package com.abzooba.sourcing.fb.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONObject;

import com.abzooba.sourcing.common.GlobalTopologyCreator;
import com.abzooba.sourcing.common.bean.AlertDTO;
import com.abzooba.sourcing.common.bean.PageInfo;
import com.abzooba.sourcing.common.bean.SocialData;
import com.abzooba.sourcing.common.bean.SourceDTO;
import com.abzooba.sourcing.common.service.DBService;
import com.abzooba.sourcing.common.util.Constants;
import com.abzooba.sourcing.common.util.Util;

public class FacebookUtil {

	private static DateFormat _dfm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static SimpleDateFormat dfm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static SimpleDateFormat _format = new SimpleDateFormat("yyyy-MM-dd");

	private static DateFormat fmtDateHH = new SimpleDateFormat("yyyy-MM-dd HH");

	private static SimpleDateFormat _hourFormat = new SimpleDateFormat("HH:00:00");

	//	public static Properties _prop = null;
	//	
	////	public static void init(Properties prop) throws Exception {
	////		_prop = prop;
	////	}

	public static String getJSONString(String file) throws IOException {

		String json = "";
		BufferedReader br = new BufferedReader(new FileReader(file));
		String str = "";
		while ((str = br.readLine()) != null) {
			json = json + str;
		}
		br.close();
		return json;
	}

	public static JSONObject getJsonFromUrl(Properties prop, String url) {
		//public static JSONObject getJsonFromUrl(String url) {

		JSONObject json = null;
		Properties prop1 = null;
		try {

			prop1 = GlobalTopologyCreator._prop;

			String jsonText = Util.getDataFromProxy(prop, url, null);
			//System.out.println("Response From Graph FB " + jsonText);
			json = new org.json.JSONObject(jsonText);
		} catch (IOException ie) {
			try {
				Thread.sleep(1000);
				String jsonText = Util.getDataFromProxy(prop, url, null);
				json = new org.json.JSONObject(jsonText);
			} catch (IOException i) {
				System.out.println("ERROR : In getJsonFromURL  for URL :: " + url);
				System.out.println("Error Message : " + i.getMessage());
				return new JSONObject("{\"error\" : \"" + i.getMessage() + "\"}");
				//i.printStackTrace();
			} catch (InterruptedException e) {
				System.out.println("Error Message : " + e.getMessage());
				e.printStackTrace();
				return new JSONObject("{\"error\" : \"" + e.getMessage() + "\"}");
			}
		}
		return json;
	}

	public static String generateAccessToken(Properties prop, int nSourceTypeId, int nSourceId) throws SQLException {
		String accessToken = null;
		Map<String, Object> tokenMap = null;

		try {
			tokenMap = DBService.getNextToken(nSourceTypeId);

			if (tokenMap.isEmpty()) {
				DBService.updateTokenMaster(0, -1, nSourceTypeId);
				tokenMap = DBService.getNextToken(nSourceTypeId);
			}

			String url = "https://graph.facebook.com/oauth/access_token?client_id=" + (String) tokenMap.get("app_key") + "&client_secret=" + (String) tokenMap.get("app_secret") + "&grant_type=client_credentials";

			String authTokenStr = null;
			authTokenStr = fetchURL(prop, url);
			String authTokenArr[] = null;
			JSONObject token = new JSONObject();
			token = getJsonFromUrl(prop, url);

			//System.out.println("*************************************************** " + authTokenStr);
			System.out.println("*************************************************** " + token.toString());
			//System.out.println("*************************************************** ");
			if (token.has("access_token") && token.getString("access_token") != null) {
				accessToken = token.getString("access_token");
			} else if (authTokenStr.contains("access_token")) {

				// OLD CASE of String Operations
				authTokenArr = authTokenStr.split("=");
				accessToken = authTokenArr[1];
			} else {
				System.out.println("FacebookUtil : Can't get access token to facebook: invalid client_id or client_secret");

				AlertDTO alertDTO = new AlertDTO();
				alertDTO.setSubject("invalid client_id or client_secret for facebook");
				alertDTO.setAlertCategoryId(1);
				alertDTO.setSourceId(nSourceId);
				alertDTO.setDateHH(fmtDateHH.format(new Date()));
				alertDTO.setDescription("Supplied client_id or client_secret in token_master table is invalid");
				alertDTO.setAlertType("Critical");
				alertDTO.setPriority(1);
				alertDTO.setActive(true);
				alertDTO.setAuthor("system");
				DBService.saveAlert(alertDTO);
			}

			DBService.updateTokenMaster(1, (Integer) tokenMap.get("token_id"), nSourceTypeId);
		} catch (SQLException e) {
			System.out.println("FacebookUtil : Token_Master table need to be populated with client_id and client_secret: " + e.getMessage());
			//accessToken = generateAccessToken(prop, nSourceTypeId, nSourceId);
			//e.printStackTrace();
			AlertDTO alertDTO = new AlertDTO();
			alertDTO.setSubject("token master table for Facebook need to be populated");
			alertDTO.setAlertCategoryId(1);
			alertDTO.setSourceId(nSourceId);
			alertDTO.setDateHH(fmtDateHH.format(new Date()));
			alertDTO.setDescription("Token_Master table need to be populated with valid client_id and client_secret. " + e.getMessage());
			alertDTO.setAlertType("Critical");
			alertDTO.setPriority(1);
			alertDTO.setActive(true);
			alertDTO.setAuthor("system");
			DBService.saveAlert(alertDTO);
		} catch (Exception e) {
			System.out.println("FacebookUtil : Can't get access token for facebook: null or invalid client_id or client_secret: " + e.getMessage());
			//e.printStackTrace();
			//DBService.updateTokenMaster(-1, (Integer) tokenMap.get("token_id"), nSourceTypeId);
			//accessToken = generateAccessToken(prop, nSourceTypeId, nSourceId);
			AlertDTO alertDTO = new AlertDTO();
			alertDTO.setSubject("token master table for Facebook need to be populated");
			alertDTO.setAlertCategoryId(1);
			alertDTO.setSourceId(nSourceId);
			alertDTO.setDateHH(fmtDateHH.format(new Date()));
			alertDTO.setDescription("Token_Master table need to be populated with valid client_id and client_secret. " + e.getMessage());
			alertDTO.setAlertType("Critical");
			alertDTO.setPriority(1);
			alertDTO.setActive(true);
			alertDTO.setAuthor("system");
			DBService.saveAlert(alertDTO);
		}
		return accessToken;
	}

	public static String generateUserAccessToken() throws SQLException {
		String accessToken = null;
		Map<String, String> tokenMap = new HashMap<String, String>();

		try {
			//accessToken = DBService.getFacebookUserToken();
			tokenMap = DBService.getFacebookUserToken();

			//if (accessToken == null) 
			if (tokenMap.isEmpty()) {
				DBService.updateUserTokenMaster(0, accessToken);
				tokenMap = DBService.getFacebookUserToken();
				//accessToken = DBService.getFacebookUserToken();
			}
			DBService.updateUserTokenMaster(1, tokenMap.get("app_key"));
		} catch (SQLException e) {
			System.err.println("FacebookUtil : Token_Master table need to be populated with more Tokens " + e.getMessage());
			tokenMap = DBService.getFacebookUserToken();
			//accessToken = DBService.getFacebookUserToken();
			//e.printStackTrace();
		}
		accessToken = tokenMap.get("app_key");
		return accessToken;
	}

	public static String fetchURL(Properties prop, String urlString) throws IOException {
		/*String str = new String();
		URL url = new URL(urlString);
		InputStream is = url.openStream();
		BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
		str = readAll(rd);
		return str;*/
		return Util.getDataFromProxy(prop, urlString, null);
	}

	public static String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}

	public static Object[] getFeedJsonArray(Properties prop, SourceDTO sourceDTO, String url, String access_token) {
		Object[] objArr = new Object[3];
		String urlPlusAccessToken = "";
		JSONObject json = new JSONObject();
		//JSONArray jsonArray = new JSONArray();
		try {

			int trycount = 1;
			if (url.lastIndexOf("?") == url.length() - 1)
				urlPlusAccessToken = url + "access_token=" + access_token;
			else
				urlPlusAccessToken = url + "&access_token=" + access_token;
			System.out.println("FINAL URL ::" + urlPlusAccessToken);
			while (trycount <= 3) {
				json = getJsonFromUrl(prop, urlPlusAccessToken);

				if (json.has("error")) {
					AlertDTO alertDTO = new AlertDTO();
					alertDTO.setSubject("Data fetching error");
					alertDTO.setAlertCategoryId(2);
					alertDTO.setSourceId(sourceDTO.getSourceId());
					alertDTO.setDateHH(fmtDateHH.format(new Date()));
					alertDTO.setDescription("May be the page is not authorized to fetch data. " + json.getString("error"));
					alertDTO.setAlertType("Major");
					alertDTO.setPriority(2);
					alertDTO.setActive(true);
					alertDTO.setAuthor("system");
					DBService.saveAlert(alertDTO);
				}

				trycount = 4;
				trycount++;
			}

			// check data array if there is any feeds
			//jsonArray = ((JSONArray) json.get("data"));
		} catch (Exception e) {
			try {
				System.out.println("FAILURE: Reading data object from json");
				access_token = FacebookUtil.generateAccessToken(prop, sourceDTO.getSourceTypeId(), sourceDTO.getSourceId());
				if (access_token == null)
					access_token = FacebookUtil.generateAccessToken(prop, sourceDTO.getSourceTypeId(), sourceDTO.getSourceId());

				if (access_token != null) {
					urlPlusAccessToken = url + "&access_token=" + access_token;
					json = FacebookUtil.getJsonFromUrl(prop, urlPlusAccessToken);
					Thread.sleep(1200);
					//jsonArray = ((JSONArray) json.get("data"));
					System.out.println("SUCCESS: Reading data object from json with new token");
				}
			} catch (Exception exe) {
				System.out.println("All tokens are used and expired.");

				AlertDTO alertDTO = new AlertDTO();
				alertDTO.setSubject("token master table for Facebook need to be populated");
				alertDTO.setAlertCategoryId(1);
				alertDTO.setSourceId(sourceDTO.getSourceId());
				alertDTO.setDateHH(fmtDateHH.format(new Date()));
				alertDTO.setDescription("Token_Master table need to be populated with valid client_id and client_secret. " + exe.getMessage());
				alertDTO.setAlertType("Critical");
				alertDTO.setPriority(1);
				alertDTO.setActive(true);
				alertDTO.setAuthor("system");

				try {
					DBService.saveAlert(alertDTO);
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}

		objArr[0] = json;
		//objArr[1] = jsonArray;
		//objArr[2] = access_token;
		objArr[1] = access_token;

		return objArr;
	}

	public static Date DateJsonParse(String input) throws java.text.ParseException {

		// NOTE: SimpleDateFormat uses GMT[-+]hh:mm for the TZ which breaks
		// things a bit. Before we go on we have to repair this.
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssz");
		df.setTimeZone(TimeZone.getTimeZone("GMT"));
		// this is zero time so we need to add that TZ indicator for
		if (input.endsWith("Z")) {
			input = input.substring(0, input.length() - 1) + "GMT-00:00";
		} else {
			int inset = "+0000".length();

			String s0 = input.substring(0, input.length() - inset);
			String s1 = input.substring(input.length() - inset);
			s1 = s1.replaceAll("\\+", "").replaceAll("-", "");

			input = s0 + "GMT" + s1;
			System.out.println(input);
		}
		Date date = df.parse(input);
		return date;
	}

	public static String filterASCII(String str) {
		if (str == null)
			return null;
		StringBuilder filtered = new StringBuilder(str.length());
		for (int i = 0; i < str.length(); i++) {
			char current = str.charAt(i);
			if (current >= 0x20 && current <= 0x7e) {
				filtered.append(current);
			}
		}

		return filtered.toString();
	}

	public static java.util.Date DateJsonParse1(String input) throws java.text.ParseException {

		// NOTE: SimpleDateFormat uses GMT[-+]hh:mm for the TZ which breaks
		// things a bit. Before we go on we have to repair this.
		// SimpleDateFormat df = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ssz"
		// );
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		// this is zero time so we need to add that TZ indicator for
		if (input.endsWith("Z")) {
			input = input.substring(0, input.length() - 1);
		} else {
			int inset = 6;

			String s0 = input.substring(0, input.length() - inset);
			String s1 = input.substring(input.length() - inset, input.length());

			input = s0 + "GMT" + s1;
		}
		//System.out.println("input:" + input);
		java.util.Date startDate = df.parse(input);
		//System.out.println("date:" + df.format(startDate));
		return startDate;
	}

	// Unix Time Converter
	public static long unixtimeConversion(Properties prop, String time) throws ParseException {
		_dfm.setTimeZone(TimeZone.getTimeZone("GMT-4"));// Specify your timezone
		//_dfm.setTimeZone(TimeZone.getTimeZone(prop.getProperty("timezone", "GMT")));
		Date date = _dfm.parse(time);
		//String date1 = _dfm.format(date);

		long unixTime = (long) date.getTime() / 1000;

		//System.out.println("unix time from util:" + unixtime);
		return unixTime;
	}

	public static PageInfo getPageInfo(Properties prop, JSONObject json, SourceDTO sourceDTO, String accessToken, List<String> searchList) {

		PageInfo pageInfo = new PageInfo();
		String strPageId = "";
		String strStartDate = "";
		String strEndDate = "";
		String strUnixStartDate = "";
		String strUnixEndDate = "";
		if (json == null) {
			pageInfo = null;
			return pageInfo;
		}

		/*Checking Search Results  for Having the Keyword of the source or as specified in SourceMaster
		 *If not present then  returning null 
		 *Moving forward to next search result*/

		if ((json.has("name")) && (searchList != null)) {
			String name = json.getString("name");
			boolean isValid = false;
			for (String search : searchList) {
				System.out.println("Search Word : " + search);
				if (name.contains(search) || name.contains(search.toLowerCase()) || name.contains(search.toUpperCase())) {
					isValid = true;
					System.out.println("Search WORD EXISTS IN PAGE NAME " + name);
				}
			}
			if (isValid == false)
				return null;
		}

		pageInfo.setSourceId(sourceDTO.getSourceId());
		pageInfo.setDateKey(_format.format(new Date()));
		pageInfo.setHourKey(_hourFormat.format(new Date()));

		if (json.has("id"))
			strPageId = json.getString("id");
		pageInfo.setPageId(json.getString("id"));

		if (json.has("category"))
			pageInfo.setCategory(json.getString("category"));

		if (json.has("fan_count"))
			pageInfo.setPageLike(json.getLong("fan_count"));

		if (json.has("link"))
			pageInfo.setPageURL(json.getString("link"));

		if (json.has("talking_about_count"))
			pageInfo.setPeopleTalkingAbout(json.getLong("talking_about_count"));

		if (json.has("were_here_count"))
			pageInfo.setPeopleWereHere(json.getLong("were_here_count"));

		if (json.has("location")) {
			JSONObject location = json.getJSONObject("location");

			if (location.has("city"))
				pageInfo.setCity(location.getString("city"));
			if (location.has("country"))
				pageInfo.setCountry(location.getString("country"));
			if (location.has("latitude")) {
				pageInfo.setLatitude(location.getDouble("latitude"));
			}

			if (location.has("longitude")) {
				pageInfo.setLongitude(location.getDouble("longitude"));
			}

		}

		//Insights for yesterday till today
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		/*cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		strStartDate = _dfm.format(cal.getTime());*/
		strStartDate = _dfm.format(cal.getTime());
		strEndDate = _dfm.format(new Date());
		try {
			if (!(strStartDate == "" && strEndDate == "")) {
				strUnixStartDate = String.valueOf(unixtimeConversion(prop, strStartDate));
				strUnixEndDate = String.valueOf(unixtimeConversion(prop, strEndDate));
			}
		} catch (ParseException e) {
			System.out.println("Error in UnixTimeConversion for PageInsights");
			//e.printStackTrace();
		}

		// Page Insights Processing 
		if (prop.getProperty("kpi_insights").equalsIgnoreCase("1")) {
			//String accessToken = "";
			try {
				accessToken = FacebookUtil.generateUserAccessToken();
			} catch (SQLException e2) {
				System.err.println("Error in KPI PAGE INSIGHTS of GET PAGE INFO .. : " + e2.getMessage());
				//e2.printStackTrace();
			}

			String insightsFanURL = "";
			String insightsStorytellerURL = "";
			try {
				//insightsFanURL = "https://graph.facebook.com/" + strPageId + "/insights/page_fans_country/lifetime?access_token=" + accessToken +"&since=" + strUnixStartDate + "&until=" + strUnixEndDate;
				insightsFanURL = "https://graph.facebook.com/" + strPageId + "/insights/page_fans_country/lifetime?access_token=" + accessToken + "&since=" + strUnixStartDate;
				JSONObject pageInsights = getJsonFromUrl(prop, insightsFanURL);

				if (pageInsights.has("error")) {
					AlertDTO alertDTO = new AlertDTO();
					alertDTO.setSubject("Data fetching error");
					alertDTO.setAlertCategoryId(2);
					alertDTO.setSourceId(sourceDTO.getSourceId());
					alertDTO.setDateHH(fmtDateHH.format(new Date()));
					alertDTO.setDescription("May be the page is not authorized to fetch data. " + json.getString("error"));
					alertDTO.setAlertType("Major");
					alertDTO.setPriority(2);
					alertDTO.setActive(true);
					alertDTO.setAuthor("system");
					DBService.saveAlert(alertDTO);
				}

				System.out.println("URL for Fans by country Insight :: " + insightsFanURL);
				parsePageFanInsights(pageInsights, pageInfo);

				//insightsStorytellerURL = "https://graph.facebook.com/" + strPageId + "/insights/page_storytellers_by_country/day?access_token=" + accessToken +"&since=" + strUnixStartDate + "&until=" + strUnixEndDate;
				insightsStorytellerURL = "https://graph.facebook.com/" + strPageId + "/insights/page_storytellers_by_country/day?access_token=" + accessToken + "&since=" + strUnixStartDate;
				pageInsights = getJsonFromUrl(prop, insightsStorytellerURL);
				System.out.println("URL for StoryTeller by country Insight :: " + insightsStorytellerURL);
				parsePageStorytellerInsights(pageInsights, pageInfo);

			} catch (SQLException e) {
				System.out.println("Error in getPageInfo while retriving Page Insights");
				System.out.println("FanURL :: " + insightsFanURL);
				System.out.println("StoryTellerURL :: " + insightsStorytellerURL);
				System.out.println("Error Message :: " + e.getMessage());
				//e.printStackTrace();

			} catch (Exception e) {
				System.out.println("Error in getPageInfo while retriving Page Insights");
				System.out.println("FanURL :: " + insightsFanURL);
				System.out.println("StoryTellerURL :: " + insightsStorytellerURL);
				System.out.println("Error Message :: " + e.getMessage());

				AlertDTO alertDTO = new AlertDTO();
				alertDTO.setSubject("token master table for Facebook need to be populated");
				alertDTO.setAlertCategoryId(1);
				alertDTO.setSourceId(sourceDTO.getSourceId());
				alertDTO.setDateHH(fmtDateHH.format(new Date()));
				alertDTO.setDescription("Token_Master table need to be populated with valid client_id and client_secret. " + e.getMessage());
				alertDTO.setAlertType("Critical");
				alertDTO.setPriority(1);
				alertDTO.setActive(true);
				alertDTO.setAuthor("system");
				try {
					DBService.saveAlert(alertDTO);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					//e1.printStackTrace();
				}
				//e.printStackTrace();
			}
		}

		return pageInfo;
	}

	public static void parsePageFanInsights(JSONObject json, PageInfo pageInfo) throws SQLException {

		int insightId = DBService.getInsightId("Fans");
		JSONArray insightsJsonArray = new JSONArray();
		JSONObject fansJson = new JSONObject();
		JSONArray fansValueJsonArray = new JSONArray();
		HashMap<String, Long> countryFans = new HashMap<String, Long>();
		long count = 0;
		try {
			insightsJsonArray = (JSONArray) json.get("data");
			fansValueJsonArray = (JSONArray) insightsJsonArray.getJSONObject(0).get("values");
			fansJson = fansValueJsonArray.getJSONObject(0).getJSONObject("value");

			for (String key : fansJson.keySet()) {
				if (count < fansJson.getLong(key)) {
					count = fansJson.getLong(key);
				}
				countryFans.put(key, fansJson.getLong(key));
			}
			pageInfo.setLikeCountry(count);
			DBService.saveKPIPageInsights(countryFans, pageInfo, insightId);

		} catch (SQLException e) {
			System.out.println("SQLError: While saving Insight Map");
			System.out.println("Error Message :: " + e.getMessage());
			//e.printStackTrace();
		}

	}

	public static void parsePageStorytellerInsights(JSONObject json, PageInfo pageInfo) throws SQLException {

		int insightId = DBService.getInsightId("Storyteller");
		JSONArray insightsJsonArray = new JSONArray();
		JSONObject storyTellerJson = new JSONObject();
		JSONArray storyTellerValueJson = new JSONArray();
		HashMap<String, Long> countryStoryteller = new HashMap<String, Long>();
		long count = 0;
		try {
			insightsJsonArray = (JSONArray) json.get("data");
			storyTellerValueJson = (JSONArray) insightsJsonArray.getJSONObject(0).get("values");
			storyTellerJson = storyTellerValueJson.getJSONObject(0).getJSONObject("value");
			count = 0;

			for (String key : storyTellerJson.keySet()) {
				if (count < storyTellerJson.getLong(key)) {
					count = storyTellerJson.getLong(key);
				}
				countryStoryteller.put(key, storyTellerJson.getLong(key));
				DBService.saveKPIPageInsights(countryStoryteller, pageInfo, insightId);
			}
			pageInfo.setPageStorytellerCountry(count);
		} catch (SQLException e) {
			System.out.println("SQLError: While saving Insight Map");
			System.out.println("Error Message :: " + e.getMessage());
			//e.printStackTrace();
		} catch (Exception e) {
			System.out.println("Error in extracting Story Teller");
			System.out.println("Error Message :: " + e.getMessage());
			//e.printStackTrace();
		}

	}

	public static void writeFile(String strFilePath, String data) {
		File file = new File(strFilePath);

		try {
			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(data);
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * Use to parse JSON reply 
	 * 1.When pagefeeds are called from  FaceBookBolt: It calls getReactions()
	 * 2.When comments are called from FacebookCommentBolt : It calls getCommentLikeReplyCount()
	 *  
	 */
	public static Set<SocialData> getPostData(Properties prop, SourceDTO sourceDTO, JSONArray postJsonArray, String strParentId, String strFacebookToken, int postTypeId1, HashMap<String, Integer> aspectMap) throws Exception {

		String postURL = "";
		String type = null;
		Set<SocialData> listPost = new LinkedHashSet<SocialData>();
		JSONObject postJson = new JSONObject();
		try {

			// Looping through all post
			for (int i = 0; i < postJsonArray.length(); i++) {
				postJson = (JSONObject) postJsonArray.get(i);

				//if (postJson.has("message") && postJson.getString("message") != null && !postJson.getString("message").equals("")) {
				if (postJson != null) {
					SocialData postData = new SocialData();
					postData.setDateKey(_format.format(new Date()));
					postData.setHourKey(_hourFormat.format(new Date()));

					String strPostId = postJson.getString("id");
					postData.setPostId(strPostId);

					Util.prepareSocialDataBean(postData, sourceDTO);
					postData.setPostTypeId(postTypeId1);
					postData.setAspectMap(aspectMap);

					if (postJson.has("type")) {
						type = postJson.getString("type");
						if (type != null) {
							postData.setType(type);
							//System.out.println("************************TYPE : " + type);
						}
					}

					Date date = FacebookUtil.DateJsonParse(postJson.getString("created_time"));
					//System.out.println("Get POST_DATETIME from FB API :: " + date);
					//System.out.println("Timezone on config.properties :: " + prop.getProperty("timezone"));
					_dfm.setTimeZone(TimeZone.getTimeZone(prop.getProperty("timezone", "GMT")));
					//System.out.println("POST_DATETIME saved in DB :: " + _dfm.format(date));
					postData.setPostDateTime(_dfm.format(date));

					if (postJson.has("message") && postJson.getString("message") != null && !postJson.getString("message").equals("")) {
						String strMessage = postJson.getString("message").replace("'", "\\'");
						postData.setPostText(strMessage);
						postData.setHashId(Util.ConvertTomd5(strMessage));
						//Default Type "TEXT" will be saved
					} else {
						if (type != null) {
							postData.setPostText(type);
							postData.setHashId(Util.ConvertTomd5(type));
						} else {
							postData.setPostText("Sticker");
							postData.setType("Sticker");
							postData.setHashId(Util.ConvertTomd5("Sticker"));
						}

						postData.setText(false);
					}

					postData.setXpressoReview(postData.getPostText());
					if (postJson.has("link") && postJson.getString("link") != null && !postJson.getString("link").equals("")) {
						postData.setLink(postJson.getString("link"));
					}

					if (postJson.has("from")) {
						JSONObject fromObj = (JSONObject) postJson.get("from");
						String id = fromObj.getString("id");
						postData.setAuthorId(id);
						if (prop.getProperty("user_url").equalsIgnoreCase("1")) {
							getOutreach(prop, id, postData, sourceDTO, strFacebookToken);
							String userURL = getUserURL(prop, id, strFacebookToken, sourceDTO);
							postData.setUserURL(userURL);
						}
						postData.setAuthorName(fromObj.getString("name").replaceAll("'", "\\'"));
					}

					//PageID does not contain '_'| So if ParentId contain '_' its a PostId other wise its PageId

					if (strParentId.contains("_")) {
						postData.setParentId(strParentId);
						//It's a comment or a reply
						int postTypeComment = DBService.getPostTypeId(sourceDTO.getSourceTypeId(), "Comment");
						//int postTypeReply = DBService.getPostTypeId(sourceDTO.getSourceTypeId(), "Reply");
						//postURL = Constants.FACEBOOK_URL + strParentId.substring(0, strParentId.indexOf("_")) + Constants.POSTS + strPostId.substring(strPostId.indexOf("_") + 1);
						if (postTypeId1 == postTypeComment) {
							//Construncting Comment Post URL
							postURL = Constants.FACEBOOK_URL + strParentId.substring(0, strParentId.indexOf("_")) + Constants.POSTS + strParentId.substring(strParentId.indexOf("_") + 1);
							String commentID = strPostId.substring(strPostId.indexOf("_") + 1);
							postURL = postURL.concat("?comment_id=").concat(commentID);
						} else {
							//Constructing ReplyPost URL
							postURL = DBService.getCommentPostURL(strParentId, sourceDTO.getSourceId());
							String replyID = strPostId.substring(strPostId.indexOf("_") + 1);
							postURL = postURL + "&reply_comment_id=" + replyID;

						}

						getCommentLikeReplyCount(postJson, postData);
					} else {
						//It's a Post
						//postURL = Constants.FACEBOOK_URL + strParentId + Constants.POSTS + strPostId.substring(strPostId.indexOf("_") + 1);
						String parentId = strPostId.substring(0, strPostId.indexOf("_"));
						postURL = Constants.FACEBOOK_URL + parentId + Constants.POSTS + strPostId.substring(strPostId.indexOf("_") + 1);

						//CHANGING PARENT ID OF TAGGED POSTS SO THAT IT'S COMMENT CAN BE FETCHED TOO

						postData.setParentId(parentId);
						getLikeShareCommentCount(postJson, postData);
						getReactions(prop, strPostId, postData, strFacebookToken, sourceDTO);
					}

					/*if (strParentId.contains("_")) {
						//It's a comment or a reply
						postURL = Constants.FACEBOOK_URL + strParentId.substring(0, strParentId.indexOf("_")) + Constants.POSTS + strPostId.substring(strPostId.indexOf("_") + 1);
						getCommentLikeReplyCount(postJson, postData);
					} else {
						//It's a Post
						postURL = Constants.FACEBOOK_URL + strParentId + Constants.POSTS + strPostId.substring(strPostId.indexOf("_") + 1);
						getLikeShareCommentCount(postJson, postData);
						getReactions(prop, strPostId, postData, strFacebookToken);
					}*/
					postData.setPostURL(postURL);
					listPost.add(postData);
				}
			}
		} catch (Exception e) {
			System.out.println("FacebookUtil: Error found in processing every post/comment:" + e.getMessage());
			e.printStackTrace();
			//System.out.println("FacebookUtil: Error found in post/comment process....continue");

			AlertDTO alertDTO = new AlertDTO();
			alertDTO.setSubject("token master table for Facebook need to be populated");
			alertDTO.setAlertCategoryId(1);
			alertDTO.setSourceId(sourceDTO.getSourceId());
			alertDTO.setDateHH(fmtDateHH.format(new Date()));
			alertDTO.setDescription("Token_Master table need to be populated with valid client_id and client_secret. " + e.getMessage());
			alertDTO.setAlertType("Critical");
			alertDTO.setPriority(1);
			alertDTO.setActive(true);
			alertDTO.setAuthor("system");
			DBService.saveAlert(alertDTO);

		}
		return listPost;
	}

	/*
	 * Get Count of Like/Share/Commnent for Posts or Comment
	 */

	public static void getLikeShareCommentCount(JSONObject postJson, SocialData postData) {

		if (postJson.has("likes") && postJson.get("likes") != null) {
			JSONObject likesObj = (JSONObject) postJson.get("likes");
			if (likesObj.has("summary") && likesObj.get("summary") != null) {
				JSONObject summaryObj = (JSONObject) likesObj.get("summary");
				if (summaryObj.has("total_count") && summaryObj.get("total_count") != null) {
					postData.setLikesCount(summaryObj.getInt("total_count"));
				}
			}

		}
		if (postJson.has("shares") && postJson.get("shares") != null) {
			JSONObject sharesObj = (JSONObject) postJson.get("shares");
			if (sharesObj.has("count") && sharesObj.get("count") != null) {
				postData.setSharesCount(sharesObj.getInt("count"));
			}
		}

		if (postJson.has("comments") && postJson.get("comments") != null) {
			JSONObject commentsObj = (JSONObject) postJson.get("comments");

			if (commentsObj.has("summary") && commentsObj.get("summary") != null) {
				JSONObject summaryObj = (JSONObject) commentsObj.get("summary");
				if (summaryObj.has("total_count") && summaryObj.get("total_count") != null) {
					postData.setCommentsCount(summaryObj.getInt("total_count"));
				}
			}
		}

	}

	/*
	 * Get friends Count for the author
	 * 
	 */

	public static void getOutreach(Properties prop, String authorId, SocialData postData, SourceDTO sourceDTO, String strFacebookToken) {

		String strOutreachURL = "https://graph.facebook.com/v2.8/" + authorId + "?fields=id,name,friends&access_token=" + strFacebookToken;

		//System.out.println("Reactions URL :: " + strReactionsURL);
		JSONObject outreachJson = FacebookUtil.getJsonFromUrl(prop, strOutreachURL);
		//System.out.println("****Outreach JSON :: " + outreachJson);

		if (outreachJson.has("error")) {
			AlertDTO alertDTO = new AlertDTO();
			alertDTO.setSubject("Data fetching error");
			alertDTO.setAlertCategoryId(2);
			alertDTO.setSourceId(sourceDTO.getSourceId());
			alertDTO.setDateHH(fmtDateHH.format(new Date()));
			alertDTO.setDescription("May be the page is not authorized to fetch data. " + outreachJson.getString("error"));
			alertDTO.setAlertType("Major");
			alertDTO.setPriority(2);
			alertDTO.setActive(true);
			alertDTO.setAuthor("system");
			try {
				DBService.saveAlert(alertDTO);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else if (outreachJson.has("friends")) {
			JSONObject outreach = outreachJson.getJSONObject("friends");
			postData.setAuthorOutreach(outreach.getJSONObject("summary").getInt("total_count"));
			System.out.println("******** OutReach :: " + postData.getAuthorOutreach());
		}

	}

	/*
	 * Get Reactions for FB Posts by the page/user 
	 * Called from getPostData
	 * Not called for comments/replies
	 */

	public static void getReactions(Properties prop, String strPostId, SocialData postData, String strFacebookToken, SourceDTO sourceDTO) {

		String strReactionsURL = "https://graph.facebook.com/v2.7/" + strPostId + "?fields=reactions.type(NONE).limit(0).summary(total_count).as(reactions_none)," + "reactions.type(LIKE).limit(0).summary(total_count).as(reactions_like),reactions.type(LOVE).limit(0).summary(total_count).as(reactions_love)," + "reactions.type(WOW).limit(0).summary(total_count).as(reactions_wow),reactions.type(HAHA).limit(0).summary(total_count).as(reactions_haha)," + "reactions.type(SAD).limit(0).summary(total_count).as(reactions_sad),reactions.type(ANGRY).limit(0).summary(total_count).as(reactions_angry)," + "reactions.type(THANKFUL).limit(0).summary(total_count).as(reactions_thankful),shares&access_token=" + strFacebookToken;

		//System.out.println("Reactions URL :: " + strReactionsURL);
		JSONObject reactionJson = FacebookUtil.getJsonFromUrl(prop, strReactionsURL);

		if (reactionJson.has("error")) {
			AlertDTO alertDTO = new AlertDTO();
			alertDTO.setSubject("Data fetching error");
			alertDTO.setAlertCategoryId(2);
			alertDTO.setSourceId(sourceDTO.getSourceId());
			alertDTO.setDateHH(fmtDateHH.format(new Date()));
			alertDTO.setDescription("May be the page is not authorized to fetch data. " + reactionJson.getString("error"));
			alertDTO.setAlertType("Major");
			alertDTO.setPriority(2);
			alertDTO.setActive(true);
			alertDTO.setAuthor("system");
			try {
				DBService.saveAlert(alertDTO);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		if (reactionJson.has("reactions_none")) {
			JSONObject reactionNone = reactionJson.getJSONObject("reactions_none");
			postData.setNoneCount(reactionNone.getJSONObject("summary").getInt("total_count"));
		}
		if (reactionJson.has("reactions_love")) {
			JSONObject reactionLove = reactionJson.getJSONObject("reactions_love");
			postData.setLoveCount(reactionLove.getJSONObject("summary").getInt("total_count"));
		}
		if (reactionJson.has("reactions_wow")) {
			JSONObject reactionWow = reactionJson.getJSONObject("reactions_wow");
			postData.setWowCount(reactionWow.getJSONObject("summary").getInt("total_count"));
		}
		if (reactionJson.has("reactions_haha")) {
			JSONObject reactionHaha = reactionJson.getJSONObject("reactions_haha");
			postData.setHahaCount(reactionHaha.getJSONObject("summary").getInt("total_count"));
		}
		if (reactionJson.has("reactions_sad")) {
			JSONObject reactionSad = reactionJson.getJSONObject("reactions_sad");
			postData.setSadCount(reactionSad.getJSONObject("summary").getInt("total_count"));
		}
		if (reactionJson.has("reactions_angry")) {
			JSONObject reactionAngry = reactionJson.getJSONObject("reactions_angry");
			postData.setAngryCount(reactionAngry.getJSONObject("summary").getInt("total_count"));
		}
		if (reactionJson.has("reactions_thankful")) {
			JSONObject reactionThankful = reactionJson.getJSONObject("reactions_thankful");
			postData.setThankfulCount(reactionThankful.getJSONObject("summary").getInt("total_count"));
		}

		if (reactionJson.has("shares")) {
			JSONObject shares = reactionJson.getJSONObject("shares");
			postData.setSharesCount(shares.getInt("count"));
		}

	}

	/*
	 * Get Likes count in Comment and Replies count
	 * Called from getPostData() when its called from CommentProcessBolt/ReplyProcessBolt
	 */

	public static void getCommentLikeReplyCount(JSONObject commentJson, SocialData commentData) {

		if (commentJson.has("like_count"))
			commentData.setLikesCount(commentJson.getInt("like_count"));

		if (commentJson.has("comment_count"))
			commentData.setCommentsCount(commentJson.getInt("comment_count"));
	}

	public static String getUserURL(Properties prop, String id, String strFacebookToken, SourceDTO sourceDTO) {

		String userURL = "";
		String profileURL = "https://graph.facebook.com/v2.7/" + id + "?fields=picture&access_token=" + strFacebookToken;
		JSONObject json = new JSONObject();
		json = getJsonFromUrl(prop, profileURL);

		if (json.has("error")) {
			AlertDTO alertDTO = new AlertDTO();
			alertDTO.setSubject("Data fetching error");
			alertDTO.setAlertCategoryId(2);
			alertDTO.setSourceId(sourceDTO.getSourceId());
			alertDTO.setDateHH(fmtDateHH.format(new Date()));
			alertDTO.setDescription("May be the page is not authorized to fetch data. " + json.getString("error"));
			alertDTO.setAlertType("Major");
			alertDTO.setPriority(2);
			alertDTO.setActive(true);
			alertDTO.setAuthor("system");
			try {
				DBService.saveAlert(alertDTO);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		if (json.has("picture") && json.get("picture") != null) {
			userURL = json.getJSONObject("picture").getJSONObject("data").getString("url");
		}
		return userURL;

	}

	public static void main(String args[]) {
		try {
			Date dt = DateJsonParse("2016-09-21T12:00:00+0000");
			System.out.println(dt);

			SimpleDateFormat _format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss ");
			_format.setTimeZone(TimeZone.getTimeZone("Asia/Singapore"));
			System.out.println(_format.format(dt) + "\n" + _format.getTimeZone());

			SimpleDateFormat _format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss ");
			_format2.setTimeZone(TimeZone.getTimeZone("IST"));
			System.out.println(_format2.format(dt) + "\n" + _format2.getTimeZone());

			dfm.setTimeZone(TimeZone.getTimeZone("GMT"));
			System.out.println(_dfm.parse("2016-09-21 17:30:00"));
			System.out.println(_format.format(dt).toString());
		} catch (ParseException e) {
			e.printStackTrace();
		}

		String next = "https://graph.facebook.com/v2.7/103737039656866/feed?fields=id,message,created_time,from,likes.limit\u0025280\u002529.summary\u002528total_count\u002529,comments.summary\u002528total_count\u002529,shares&limit=25&access_token=EAAEJlfZBvbOQBAIIigPw8SDH6kaWm0FXgBtONOZCpX0h0zEr9Ny4jXH4C9Ct4yZA2nMQqSycydli7PqlJbuYWZAGJIqNUnZCxjlZA6vl7gfSyrUuGSFSGyxBZATj3dCXgqgs1qyZCdLSECpD0vmwvdTZARExFdre4ufcZD&until=1474353235&__paging_token=enc_AdAxBkV954CNZA2OuIFyCm7yQLXVmFAMZA3oWzayX0pdnXJtQGyygVDurR6KX5y30eC8NRFhZAVzInAvaKWZC3DnA4btQb1tmL1tLPZBZAy8pFwodxugZDZD";
		String strPostURL = next.replaceAll("&oid=1&value=1&base_amount=1", "");

	}
}