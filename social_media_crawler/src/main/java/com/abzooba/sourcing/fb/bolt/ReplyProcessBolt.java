package com.abzooba.sourcing.fb.bolt;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;

/*import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;*/
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.json.JSONArray;
import org.json.JSONObject;

import com.abzooba.sourcing.common.bean.SocialData;
import com.abzooba.sourcing.common.bean.SourceDTO;
import com.abzooba.sourcing.common.service.DBService;
import com.abzooba.sourcing.common.service.DataSource;
import com.abzooba.sourcing.fb.util.FacebookUtil;

public class ReplyProcessBolt extends BaseRichBolt {

	private static final long serialVersionUID = 1L;
	private OutputCollector _collector;

	private Properties _prop = new Properties();
	private HashMap<String, Integer> aspectMap;
	private HashMap<String, Integer> postTypesMap;
	private SimpleDateFormat _format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static SimpleDateFormat _dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	private static SimpleDateFormat _hourFormat = new SimpleDateFormat("HH:00:00");

	private SimpleDateFormat fmtDateHH = new SimpleDateFormat("yyyy-MM-dd HH");

	public ReplyProcessBolt() throws ClassNotFoundException, IllegalAccessException, SQLException, InstantiationException {
		try {
			//_prop.load(new FileInputStream(Constants.PROPERTIES_PATH));
			//_prop.load(new FileInputStream(System.getProperty("propfile")));
			//_prop.load(CommentProcessBolt.class.getClassLoader().getResourceAsStream("config.properties"));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void execute(Tuple tuple) {

		SourceDTO sourceDTO = (SourceDTO) tuple.getValueByField("sourcevo");
		String strPageId = tuple.getStringByField("pageid");
		String strPostId = tuple.getStringByField("postid");
		String strMainCommentId = tuple.getStringByField("commentid");
		String strFacebookToken = tuple.getStringByField("facebooktoken");
		_format.setTimeZone(TimeZone.getTimeZone(_prop.getProperty("timezone", "GMT")));
		String strCommentURL = "";
		String strMaxCommentDate = "no-data";
		int postTypeId3 = -1;

		String strCommentMessage = null;
		boolean isEnd = false;

		JSONObject json = new JSONObject();
		JSONObject commentJsonObj = new JSONObject();
		JSONArray commentJsonArray = null;
		JSONObject commentJson = new JSONObject();

		try {

			//postTypeId3 = DBService.getPostTypeId(sourceDTO.getSourceTypeId(), "Reply");
			postTypeId3 = postTypesMap.get("Reply");

			strCommentURL = "https://graph.facebook.com/" + strMainCommentId + "?limit=25" + "&fields=comment_count,like_count,comments.order(reverse_chronological){id,message,link,from,created_time,like_count}&access_token=" + strFacebookToken;
			System.out.println("Comment URL :: " + strCommentURL);
			try {

				json = FacebookUtil.getJsonFromUrl(_prop, strCommentURL);
				if (json != null && !json.has("error")) {

					while (!isEnd) {
						if (json.has("comments")) {
							commentJsonObj = json.getJSONObject("comments");
							commentJsonArray = commentJsonObj.getJSONArray("data");

							if (commentJsonArray != null && commentJsonArray.length() > 0) {
								Set<SocialData> listReplies = new LinkedHashSet<SocialData>();
								listReplies = FacebookUtil.getPostData(_prop, sourceDTO, commentJsonArray, strMainCommentId, strFacebookToken, postTypeId3, aspectMap);
								_collector.emit(tuple, new Values(null, listReplies));
								_collector.ack(tuple);

								if (commentJsonObj.has("paging") && commentJsonObj.getJSONObject("paging").has("next")) {
									strCommentURL = commentJsonObj.getJSONObject("paging").getString("next").replaceAll("&oid=1&value=1&base_amount=1", "");
									System.out.println("ReplyProcessBolt: url for next page:" + strCommentURL);
								} else {
									isEnd = true;
								}
							}
						} else {
							System.out.println("No Replies in the current Comment :: " + strMainCommentId);
							isEnd = true;
						}
						json = FacebookUtil.getJsonFromUrl(_prop, strCommentURL);
					}

				} else {
					System.out.println("Comment Deleted");
				}

			} catch (Exception e) {
				System.out.println("ReplyProcessBolt: Error found in processing :: " + e.getMessage());
				System.out.println("ReplyProcessBolt: URL with error :: " + strCommentURL);
				e.printStackTrace();
				//System.out.println("CommentProcessBolt: Error found in comment process....continue");

			}

		} catch (Exception e) {
			System.err.println("**************************** UNHANDLED ERROR ::: " + e.getMessage());
			e.printStackTrace();
			_collector.fail(tuple);
		}
	}

	public void prepare(Map map, TopologyContext context, OutputCollector collector) {
		System.out.println("RSS Bolt :: " + map.toString());
		try {
			_prop.load(new FileInputStream((String) map.get("config.properties")));
			DataSource.setConfigPath((String) map.get("config.properties"));
			aspectMap = DBService.getAspectMap();
			postTypesMap = DBService.getPostTypeMap();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		_collector = collector;
	}

	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("pageInfo", "socialDatas"));
	}

}
