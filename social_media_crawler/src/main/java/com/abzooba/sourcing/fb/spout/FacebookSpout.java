package com.abzooba.sourcing.fb.spout;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/*import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;*/
import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;
import org.apache.storm.utils.Utils;

import com.abzooba.sourcing.common.bean.AlertDTO;
import com.abzooba.sourcing.common.bean.SourceDTO;
import com.abzooba.sourcing.common.service.DBService;
import com.abzooba.sourcing.common.service.DataSource;
import com.abzooba.sourcing.fb.util.FacebookUtil;

public class FacebookSpout extends BaseRichSpout {

	private static final long serialVersionUID = 1L;
	protected SpoutOutputCollector _collector;
	private Properties _prop = new Properties();

	private SimpleDateFormat fmtDateHH = new SimpleDateFormat("yyyy-MM-dd HH");

	public FacebookSpout() throws ClassNotFoundException, IllegalAccessException, SQLException, InstantiationException, IOException {
	}

	public void nextTuple() {
		try {
			String strFacebookToken = "";
			int iCountTokenGeneration = 0;
			int postTypeId1 = -1;
			int nMinutes = Integer.parseInt(_prop.getProperty("fb_sleep_time_in_minutes").trim());
			List<SourceDTO> sources = DBService.getSources(nMinutes, "FB", null);

			for (SourceDTO sourceDTO : sources) {

				System.out.println("Crawling for " + sourceDTO.getPageURL());
				postTypeId1 = DBService.getPostTypeId(sourceDTO.getSourceTypeId(), "Post");
				if (iCountTokenGeneration == 0) {
					//strFacebookToken = FacebookUtil.generateAccessToken(_prop);
					System.out.println("FacebookSpout: Generating Facebook token...");
					if (_prop.getProperty("user_token").equalsIgnoreCase("1")) {
						strFacebookToken = FacebookUtil.generateUserAccessToken();
					} else {
						strFacebookToken = FacebookUtil.generateAccessToken(_prop, sourceDTO.getSourceTypeId(), sourceDTO.getSourceId());
					}
				}

				if (strFacebookToken != null) {

					_collector.emit(new Values(sourceDTO, strFacebookToken, postTypeId1), sourceDTO.getSourceId());
					System.out.println("FacebookSpout: Tuple emitted successfully: " + sourceDTO.toString() + ", " + strFacebookToken);

					// update livestatus column in tbltaskinfo table

					iCountTokenGeneration++;
				} else {
					System.out.println("There is no FB token to be used to fetching data from FB");

					AlertDTO alertDTO = new AlertDTO();
					alertDTO.setSubject("No Facebook Token");
					alertDTO.setAlertCategoryId(1);
					alertDTO.setSourceId(sourceDTO.getSourceId());
					alertDTO.setDateHH(fmtDateHH.format(new Date()));
					alertDTO.setDescription("There is no FB token to be used to fetching data from FB");
					alertDTO.setAlertType("Critical");
					alertDTO.setPriority(1);
					alertDTO.setActive(true);
					alertDTO.setAuthor("system");
					DBService.saveAlert(alertDTO);
				}

				//DBService.updateSourceStatus(2, sourceDTO.getSourceId(), true, null);
			}
			Utils.sleep(nMinutes * 60 * 1000); // every two minutes
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception generted");
		}
	}

	public void open(Map map, TopologyContext context, SpoutOutputCollector collector) {
		_collector = collector;
		try {
			//_prop.load(new FileInputStream(Constants.PROPERTIES_PATH));
			//_prop.load(new FileInputStream(System.getProperty("propfile")));
			//_prop.load(FacebookSpout.class.getClassLoader().getResourceAsStream("config.properties"));
			_prop.load(new FileInputStream((String) map.get("config.properties")));
			DataSource.setConfigPath((String) map.get("config.properties"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("sourcedto", "facebooktoken", "posttypeid"));
	}

}