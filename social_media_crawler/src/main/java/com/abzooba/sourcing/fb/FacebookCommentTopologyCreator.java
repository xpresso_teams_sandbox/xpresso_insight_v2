package com.abzooba.sourcing.fb;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;
import java.util.Properties;



/*import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.utils.NimbusClient;
import backtype.storm.utils.Utils;*/
import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.StormSubmitter;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.utils.NimbusClient;
import org.apache.storm.utils.Utils;
import org.json.simple.JSONValue;

import com.abzooba.sourcing.common.ITopologyBuilder;
import com.abzooba.sourcing.common.util.Util;
import com.abzooba.sourcing.fb.bolt.CommentProcessBolt;
import com.abzooba.sourcing.fb.bolt.FBSaveBolt;
import com.abzooba.sourcing.fb.spout.FacebookCommentSpout;

public class FacebookCommentTopologyCreator implements ITopologyBuilder {
	private Properties _prop = new Properties();

	public static void main(String args[]) {

		try {
			Properties prop = new Properties();
			prop.load(new FileInputStream(System.getProperty("propfile")));
			FacebookCommentTopologyCreator fb = new FacebookCommentTopologyCreator();
			TopologyBuilder builder = fb.topologyBuilder(prop);
			LocalCluster cluster = new LocalCluster();

			Config config = new Config();
			config.setDebug(true);
			config.put(Config.TOPOLOGY_WORKERS, 4);
			config.put("config.properties", System.getProperty("propfile"));

			cluster.submitTopology("fbtopologycomment", config, builder.createTopology());
			Thread.sleep(10 * 60 * 1000);
			System.out.println("FacebookCommentTopologyCreator: Topology submitted in Local cluster");
			cluster.shutdown();
			
			FacebookTopologyCreator fbTopology = new FacebookTopologyCreator();
			fbTopology.createTopology("fbtopology");

		} catch (Exception exc) {
			System.err.println("Error: " + exc.getMessage());
			exc.printStackTrace(System.err);
		}

	}

	public TopologyBuilder topologyBuilder(Properties properties) {

		TopologyBuilder builder = null;
		try {

			builder = new TopologyBuilder();
			/*builder.setSpout("facebookspout", new FacebookSpout());
			builder.setBolt("datafetchingbolt", new FacebookBolt()).shuffleGrouping("facebookspout");
			builder.setBolt("fbsavebolt", new DBSaveBolt()).shuffleGrouping("datafetchingbolt");
			*/
			builder.setSpout("commentspout", new FacebookCommentSpout());
			//builder.setBolt("commentfetchbolt", new CommentDataFetchingBolt()).shuffleGrouping("commentspout");
			//builder.setBolt("commentprocessbolt", new CommentProcessBolt()).shuffleGrouping("commentfetchbolt");
			builder.setBolt("commentprocessbolt", new CommentProcessBolt(), Integer.parseInt(properties.getProperty("fbc_process_bolt"))).setNumTasks(Integer.parseInt(properties.getProperty("fbc_worker_number"))).shuffleGrouping("commentspout");
			builder.setBolt("commentsavebolt", new FBSaveBolt(), Integer.parseInt(properties.getProperty("fbc_save_bolt"))).setNumTasks(Integer.parseInt(properties.getProperty("fbc_worker_number"))).shuffleGrouping("commentprocessbolt");
			System.out.println("Building topology for FacebookComment");

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return builder;
	}

	public Map topologyConfBuilder(Properties properties) {
		Map topologyConf = Utils.readStormConfig();
		topologyConf.put(Config.NIMBUS_HOST, properties.getProperty("nimbus_ip").trim());
		topologyConf.put(Config.TOPOLOGY_WORKERS, Util.stringToInt(properties.getProperty("fbc_worker_number").trim()));
		topologyConf.put(Config.TOPOLOGY_TASKS, Util.stringToInt(properties.getProperty("fbc_task_number").trim()));
		topologyConf.put("config.properties", System.getProperty("propfile"));

		return topologyConf;
	}

	public void createTopology(String strTopologyName) {

		try {
			//_prop.load(new FileInputStream(Constants.PROPERTIES_PATH));
			_prop.load(new FileInputStream(System.getProperty("propfile")));
			//System.out.println("FacebookTopologyCreator: Proprties file loaded successfully");
			//_prop.load(FacebookTopologyCreator.class.getClassLoader().getResourceAsStream("config.properties"));

			TopologyBuilder builder = topologyBuilder(_prop);
			Map topologyConf = topologyConfBuilder(_prop);

			String inputJar = _prop.getProperty("jar_location").trim();
			NimbusClient nimbus = new NimbusClient(topologyConf, _prop.getProperty("nimbus_ip").trim(), 6627);

			// upload topology jar to Cluster using StormSubmitter
			String uploadedJarLocation = StormSubmitter.submitJar(topologyConf, inputJar);
			System.out.println("FacebookCommentTopologyCreator: jar uploading...");

			String jsonConf = JSONValue.toJSONString(topologyConf);

			if (builder != null) {
				nimbus.getClient().submitTopology(strTopologyName, uploadedJarLocation, jsonConf, builder.createTopology());
				System.out.println("FacebookCommentTopologyCreator: Topology submitted");
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("FacebookCommentTopologyCreator: Error :" + e.getMessage());
		}
	}

}
