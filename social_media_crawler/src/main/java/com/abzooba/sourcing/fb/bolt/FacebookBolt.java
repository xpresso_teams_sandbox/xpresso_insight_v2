package com.abzooba.sourcing.fb.bolt;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;*/
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.json.JSONArray;
import org.json.JSONObject;

import com.abzooba.sourcing.common.bean.AlertDTO;
import com.abzooba.sourcing.common.bean.PageInfo;
import com.abzooba.sourcing.common.bean.SocialData;
import com.abzooba.sourcing.common.bean.SourceDTO;
import com.abzooba.sourcing.common.service.DBService;
import com.abzooba.sourcing.common.service.DataSource;
import com.abzooba.sourcing.common.util.DateUtil;
import com.abzooba.sourcing.fb.util.FacebookUtil;

public class FacebookBolt extends BaseRichBolt {

	private static final long serialVersionUID = 1L;
	protected OutputCollector _collector;

	private String _strpreviousday;
	private int _npostlimit = 25;

	private SimpleDateFormat _format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private SimpleDateFormat fmtDateHH = new SimpleDateFormat("yyyy-MM-dd HH");

	private Properties _prop = new Properties();

	private HashMap<String, Integer> aspectMap;
	private HashMap<String, Integer> postTypesMap;

	public FacebookBolt() throws ClassNotFoundException, IllegalAccessException, SQLException, InstantiationException {
		try {
			//_prop.load(new FileInputStream(Constants.PROPERTIES_PATH));
			//_prop.load(new FileInputStream(System.getProperty("propfile")));
			//_prop.load(FacebookBolt.class.getClassLoader().getResourceAsStream("config.properties"));
			//_strpreviousday = _prop.getProperty("fb_previous_day");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void execute(Tuple tuple) {
		String strPageIdNumber = "";
		String strUnixStartDate = "";
		String strUnixEndDate = "";
		String strPostURL = "";
		String strFeedURL = "";
		String strTaggedURL = "";
		String strSearchURL = "";
		String strMaxPostDate = "no-data";
		String strStartDate = "";
		String strEndDate = "";
		boolean isEnd = false;
		int searchStatus = -1;
		JSONObject pageJson = null;

		SourceDTO sourceDTO = (SourceDTO) tuple.getValueByField("sourcedto");
		String strFacebookToken = tuple.getStringByField("facebooktoken");

		int postTypeIdTagged = postTypesMap.get("Tagged");

		int postTypeId1 = postTypesMap.get("Post");

		_format.setTimeZone(TimeZone.getTimeZone(_prop.getProperty("timezone", "GMT")));
		_strpreviousday = _prop.getProperty("fb_previous_day");

		try {
			searchStatus = sourceDTO.getSearchStatus();
			strFeedURL = "https://graph.facebook.com/v2.7/" + sourceDTO.getPageURL() + "?fields=id,name,category,fan_count,link,talking_about_count,were_here_count&access_token=" + strFacebookToken;
			System.out.println("Page URL :: " + strFeedURL);

			pageJson = FacebookUtil.getJsonFromUrl(_prop, strFeedURL);

			if (pageJson.has("error")) {
				AlertDTO alertDTO = new AlertDTO();
				alertDTO.setSubject("Data fetching error");
				alertDTO.setAlertCategoryId(2);
				alertDTO.setSourceId(sourceDTO.getSourceId());
				alertDTO.setDateHH(fmtDateHH.format(new Date()));
				alertDTO.setDescription("May be the page is not authorized to fetch data. " + pageJson.getString("error"));
				alertDTO.setAlertType("Major");
				alertDTO.setPriority(2);
				alertDTO.setActive(true);
				alertDTO.setAuthor("system");
				DBService.saveAlert(alertDTO);
			}

			if (pageJson == null) {
				DBService.updateUserTokenMaster(-1, strFacebookToken);
			} else {
				PageInfo pageInfo = FacebookUtil.getPageInfo(_prop, pageJson, sourceDTO, strFacebookToken, null);
				if (pageInfo != null) {
					strPageIdNumber = pageInfo.getPageId();
					System.out.println("FacebookBolt: Emitting PAGE_INFO data to save in database");
					_collector.emit(tuple, new Values(pageInfo, null));
				}
			}

			int nPreviousDay = Integer.parseInt(_strpreviousday);
			strStartDate = _format.format(DateUtil.getBeforeOrAfterDate(-nPreviousDay));
			strEndDate = _format.format(new Date());

			System.out.println("FacebookBolt: Process start date:" + strStartDate);
			System.out.println("FacebookBolt: Process end date:" + strEndDate);

			if (!(strStartDate == "" && strEndDate == "")) {
				strUnixStartDate = String.valueOf(FacebookUtil.unixtimeConversion(_prop, strStartDate));
				strUnixEndDate = String.valueOf(FacebookUtil.unixtimeConversion(_prop, strEndDate));
				//strPostURL = "https://graph.facebook.com/v2.7/" + strPageIdNumber + "/feed?limit=" + _npostlimit + "&since=" + strUnixStartDate + "&until=" + strUnixEndDate + "&fields=id,message,created_time,from,likes.limit(1).summary(1),comments.summary(1),shares&access_token=" + strFacebookToken;
				strPostURL = "https://graph.facebook.com/v2.7/" + strPageIdNumber + "/feed?limit=" + _npostlimit + "&since=" + strUnixStartDate + "&fields=id,type,message,link,created_time,from,likes.limit(0).summary(total_count),comments.limit(0).summary(total_count),shares&access_token=" + strFacebookToken;
			} else {
				strPostURL = "https://graph.facebook.com/v2.7/" + strPageIdNumber + "/feed?limit=" + _npostlimit + "&fields=id,type,message,link,created_time,from,likes.limit(0).summary(total_count),comments.limit(0).summary(total_count),shares&access_token=" + strFacebookToken;
			}

			System.out.println("Post URL :: " + strPostURL);
			JSONArray postJsonArray = new JSONArray();

			while (!isEnd && strPageIdNumber != "") {

				JSONObject postJson = null;
				postJson = FacebookUtil.getJsonFromUrl(_prop, strPostURL);
				if (postJson != null) {
					postJsonArray = (JSONArray) postJson.get("data");
					if (postJsonArray.length() > 0) {

						Set<SocialData> listPost = new LinkedHashSet<SocialData>();
						listPost = FacebookUtil.getPostData(_prop, sourceDTO, postJsonArray, strPageIdNumber, strFacebookToken, postTypeId1, aspectMap);
						System.out.println("FacebookBolt: Emitting SOCIAL_DATA data to save in database for Page Posts");
						_collector.emit(tuple, new Values(null, listPost));
						_collector.ack(tuple);

						if (postJson.getJSONObject("paging").has("next")) {
							//strPostURL = postJson.getJSONObject("paging").getString("next").replaceAll("&oid=1&value=1&base_amount=1", "") + "&since=" + strUnixStartDate + "&fields=id,message,created_time,likes.limit(1).summary(1),comments.summary(1),shares";
							strPostURL = postJson.getJSONObject("paging").getString("next") + "&since=" + strUnixStartDate;
							System.out.println("FacebookBolt: url for next page:" + strPostURL);
						} else {
							isEnd = true;
						}
					} else {
						isEnd = true;
						System.out.println("FacebookBolt: Crawling completed for Page Feeds");
					}
				}

			}

			/**
			 * After Completing Page Feeds
			 * Starting for Page Tagged Posts
			 * */

			isEnd = false;
			if (!(strStartDate == "" && strEndDate == "")) {
				strTaggedURL = "https://graph.facebook.com/v2.7/" + strPageIdNumber + "/tagged?&since=" + strUnixStartDate + "&fields=id,message,created_time,from,likes.limit(0).summary(total_count),comments.summary(total_count),shares&access_token=" + strFacebookToken;
			} else {
				strTaggedURL = "https://graph.facebook.com/v2.7/" + strPageIdNumber + "/tagged?fields=id,message,created_time,from,likes.limit(0).summary(total_count),comments.summary(total_count),shares&access_token=" + strFacebookToken;
			}

			while (!isEnd && strPageIdNumber != "") {

				JSONObject postJson = null;
				System.out.println("Tagged URL :: " + strTaggedURL);
				postJson = FacebookUtil.getJsonFromUrl(_prop, strTaggedURL);
				if (postJson != null) {
					postJsonArray = (JSONArray) postJson.get("data");
					if (postJsonArray.length() > 0) {

						Set<SocialData> listPost = new LinkedHashSet<SocialData>();
						listPost = FacebookUtil.getPostData(_prop, sourceDTO, postJsonArray, strPageIdNumber, strFacebookToken, postTypeIdTagged, aspectMap);
						System.out.println("FacebookBolt: Emitting SOCIAL_DATA data to save in database for Tagged Posts");
						_collector.emit(tuple, new Values(null, listPost));
						_collector.ack(tuple);

						if (postJson.getJSONObject("paging").has("next")) {
							//strPostURL = postJson.getJSONObject("paging").getString("next").replaceAll("&oid=1&value=1&base_amount=1", "") + "&since=" + strUnixStartDate + "&fields=id,message,created_time,likes.limit(1).summary(1),comments.summary(1),shares";
							strTaggedURL = postJson.getJSONObject("paging").getString("next") + "&since=" + strUnixStartDate;
							System.out.println("FacebookBolt: url for next page for tagged posts:" + strPostURL);
						} else {
							isEnd = true;
						}
					} else {
						isEnd = true;
						System.out.println("FacebookBolt: Crawling completed for Tagged Posts");
					}
				}

			}

			/*After Tagged Results Searching for keywords Search
			 *searchStatus =  0 : Don't Search
			 *searchStatus =  1 : Global Search
			 *searchStatus =  2 : Search Location Specific with Latitude & Longitude*/

			isEnd = false;
			int nSearch = Integer.parseInt(_prop.getProperty("number_of_search", "1"));
			if (searchStatus == 0) {
				System.out.println("Not Implementing Facebook Search");
			} else if (searchStatus == 1) {
				System.out.println("Implementing Global Search with PAGE URL Name");
				//KEEPING LIMIT IN MULTIPLES OF 100 FOR GLOBAL SEARCH
				strSearchURL = "https://graph.facebook.com/v2.7/search?q=" + sourceDTO.getPageURL() + "&type=page&fields=id,name,category,fan_count,link,talking_about_count,were_here_count&limit=100&access_token=" + strFacebookToken;

				System.out.println("Search URL :: " + strSearchURL);
				JSONArray searchJsonArray = new JSONArray();
				String[] searchKeywords = sourceDTO.getSearchWords().split(",");
				List<String> searchList = Arrays.asList(searchKeywords);

				while (!isEnd && nSearch != 0) {
					//nSearch Parameter for the particular Source_id represents the sum of total SearchResults that are needed
					nSearch--;
					JSONObject searchJson = null;
					searchJson = FacebookUtil.getJsonFromUrl(_prop, strSearchURL);
					if (searchJson != null) {
						searchJsonArray = (JSONArray) searchJson.get("data");
						for (Object info : searchJsonArray) {
							//System.out.println("Inside FOR LOOP OF JSON ARRAY");
							PageInfo pageInfo = FacebookUtil.getPageInfo(_prop, (JSONObject) info, sourceDTO, strFacebookToken, searchList);
							if (pageInfo == null) {
								continue;
							}

							strPageIdNumber = pageInfo.getPageId();
							strStartDate = _format.format(DateUtil.getBeforeOrAfterDate(-nPreviousDay));
							strUnixStartDate = String.valueOf(FacebookUtil.unixtimeConversion(_prop, strStartDate));

							if (_prop.getProperty("save_page_info").equalsIgnoreCase("1")) {
								System.out.println("FacebookBolt: Emitting PAGE_INFO data to save in database");
								_collector.emit(tuple, new Values(pageInfo, null));
							}

							/**
							 * Now Search Post if available on these pages
							 */

							boolean isSearchEnd = false;
							System.out.println("Extracting feed for Page : " + strPageIdNumber);
							strPostURL = "https://graph.facebook.com/v2.8/" + strPageIdNumber + "/feed?limit=" + _npostlimit + "&since=" + strUnixStartDate + "&fields=id,message,created_time,from,likes.limit(0).summary(total_count),comments.summary(total_count),shares&access_token=" + strFacebookToken;
							while (!isSearchEnd && strPageIdNumber != "") {
								System.out.println("Entering WHILE LOOP");
								JSONObject postJson = null;
								System.out.println("Post URL for Searched Page :: " + strPostURL);
								postJson = FacebookUtil.getJsonFromUrl(_prop, strPostURL);
								if (postJson != null) {
									postJsonArray = (JSONArray) postJson.get("data");
									if (postJsonArray.length() > 0) {

										Set<SocialData> listPost = new LinkedHashSet<SocialData>();
										listPost = FacebookUtil.getPostData(_prop, sourceDTO, postJsonArray, strPageIdNumber, strFacebookToken, postTypeId1, aspectMap);
										System.out.println("FacebookBolt: Emitting SOCIAL_DATA data to save in database for Searched Page Posts");
										_collector.emit(tuple, new Values(null, listPost));
										_collector.ack(tuple);

										if (postJson.getJSONObject("paging").has("next")) {
											//strPostURL = postJson.getJSONObject("paging").getString("next").replaceAll("&oid=1&value=1&base_amount=1", "") + "&since=" + strUnixStartDate + "&fields=id,message,created_time,likes.limit(1).summary(1),comments.summary(1),shares";
											strPostURL = postJson.getJSONObject("paging").getString("next") + "&since=" + strUnixStartDate;
											System.out.println("FacebookBolt: url for next page for search page posts:" + strPostURL);
										} else {
											isSearchEnd = true;
										}
									} else {
										isSearchEnd = true;
										System.out.println("FacebookBolt: Crawling completed for Searched Page Posts");
									}
								}

							}

						}
						if (searchJson.getJSONObject("paging").has("next")) {
							strSearchURL = searchJson.getJSONObject("paging").getString("next") + "&since=" + strUnixStartDate;
							System.out.println("FacebookBolt: url for next page:" + strPostURL);
						} else {
							isEnd = true;
						}
					} else {
						isEnd = true;
						System.out.println("FacebookBolt: Crawling completed for Page Feeds of Searched Page");
					}
				}

			} else if (searchStatus == 2) {
				System.out.println("Impleneting Location Specific Search on Facebook");
				System.out.println("Implementing Global Search with PAGE URL Name");
				Pattern p = Pattern.compile("\\(.*?\\)");
				Matcher m = p.matcher(sourceDTO.getCoordinates());
				List<String> centers = new ArrayList<String>();
				double southLatitude = 0;
				double northLatitude = 0;
				double eastLongitude = 0;
				double westLongitude = 0;
				if (sourceDTO.getCenterLocations() == "N") {
					int count = 0;
					while (m.find()) {
						centers.add(m.group().replace("(", "").replace(")", ""));
						if (count == 0) {
							String latitudeRange[] = m.group().replace("(", "").replace(")", "").split(",");
							southLatitude = Double.parseDouble(latitudeRange[0]);
							northLatitude = Double.parseDouble(latitudeRange[1]);
						} else {
							String longitudeRange[] = m.group().replace("(", "").replace(")", "").split(",");
							eastLongitude = Double.parseDouble(longitudeRange[0]);
							westLongitude = Double.parseDouble(longitudeRange[1]);
						}

					}
				} else {
					while (m.find()) {
						centers.add(m.group().replace("(", "").replace(")", ""));
						String cityCordinates[] = m.group().replace("(", "").replace(")", "").split(",");
						southLatitude = Double.parseDouble(cityCordinates[0]) - 1;
						northLatitude = Double.parseDouble(cityCordinates[0]) + 1;
						eastLongitude = Double.parseDouble(cityCordinates[1]) + 1;
						westLongitude = Double.parseDouble(cityCordinates[1]) - 1;
					}
				}

				for (String center : centers) {
					//KEEPING LIMIT IN MULTIPLES OF 40 FOR GLOBAL SEARCH
					strSearchURL = "https://graph.facebook.com/v2.7/search?q=" + sourceDTO.getPageURL() + "&type=page&center=" + center + "&distance=" + sourceDTO.getRange() + "&fields=id,name,category,fan_count,link,talking_about_count,were_here_count,location&limit=40&access_token=" + strFacebookToken;

					System.out.println("Search URL :: " + strSearchURL);
					JSONArray searchJsonArray = new JSONArray();
					String[] searchKeywords = sourceDTO.getSearchWords().split(",");
					List<String> searchList = Arrays.asList(searchKeywords);

					while (!isEnd && nSearch != 0) {
						//nSearch Parameter for the particular Source_id represents the sum of total SearchResults that are needed
						System.out.println("Entering WHILE LOOP of SEARCH");
						JSONObject searchJson = null;
						searchJson = FacebookUtil.getJsonFromUrl(_prop, strSearchURL);
						if (searchJson != null) {
							searchJsonArray = (JSONArray) searchJson.get("data");
							for (Object info : searchJsonArray) {
								PageInfo pageInfo = FacebookUtil.getPageInfo(_prop, (JSONObject) info, sourceDTO, strFacebookToken, searchList);
								if (pageInfo == null) {
									System.out.println("Page Info came Null");
									continue;
								}

								strPageIdNumber = pageInfo.getPageId();

								/**Checking if the Latitude & Longitude of the Page from the Search Result
								 *is in the Range of our City/Country to get Data
								 *IF IT IS IN RANGE then continue
								 *Otherwise it skips to the next page
								 */

								if ((pageInfo.getLatitude() < northLatitude) && (pageInfo.getLatitude() > southLatitude) && (pageInfo.getLongitude() > westLongitude) && (pageInfo.getLongitude() > eastLongitude)) {
									//nSearch--;
									System.out.println("Page in Scope :: " + pageInfo.getPageId());
								} else if ((pageInfo.getLatitude() == 0.0) && (pageInfo.getLongitude() == 0.0)) {
									System.out.println("Page Location Not available so crawling it");
								} else {
									System.out.println("PAGE NOT IN SCOPE *************************************************");
									System.out.println(" PARAMETERS :: " + pageInfo.getLatitude());
									System.out.println(" PARAMETERS :: " + pageInfo.getLongitude());
									//System.out.println(" PARAMETERS  :: " + );
									//System.out.println(" PARAMETERS :: " + );
									continue;
								}
								if (_prop.getProperty("save_page_info").equalsIgnoreCase("1")) {
									System.out.println("FacebookBolt: Emitting PAGE_INFO data to save in database");
									_collector.emit(tuple, new Values(pageInfo, null));
								}

								/**
								 * Now Search Post if available on these pages
								 */

								Boolean isSearchEnd = false;
								strPostURL = "https://graph.facebook.com/v2.7/" + strPageIdNumber + "/feed?limit=" + _npostlimit + "&since=" + strUnixStartDate + "&fields=id,message,created_time,from,likes.limit(0).summary(total_count),comments.summary(total_count),shares&access_token=" + strFacebookToken;
								while (!isSearchEnd && strPageIdNumber != "") {
									System.out.println("Entering WHILE LOOP of POSTS");
									JSONObject postJson = null;
									System.out.println("Post URL for Searched Page :: " + strPostURL);
									postJson = FacebookUtil.getJsonFromUrl(_prop, strPostURL);
									if (postJson != null) {
										postJsonArray = (JSONArray) postJson.get("data");
										if (postJsonArray.length() > 0) {

											Set<SocialData> listPost = new LinkedHashSet<SocialData>();
											listPost = FacebookUtil.getPostData(_prop, sourceDTO, postJsonArray, strPageIdNumber, strFacebookToken, postTypeId1, aspectMap);
											System.out.println("FacebookBolt: Emitting SOCIAL_DATA data to save in database for Tagged Posts");
											_collector.emit(tuple, new Values(null, listPost));
											_collector.ack(tuple);

											if (postJson.getJSONObject("paging").has("next")) {
												//strPostURL = postJson.getJSONObject("paging").getString("next").replaceAll("&oid=1&value=1&base_amount=1", "") + "&since=" + strUnixStartDate + "&fields=id,message,created_time,likes.limit(1).summary(1),comments.summary(1),shares";
												strPostURL = postJson.getJSONObject("paging").getString("next") + "&since=" + strUnixStartDate;
												System.out.println("FacebookBolt: url for next page for search page posts:" + strPostURL);
											} else {
												isSearchEnd = true;
											}
										} else {
											isSearchEnd = true;
											System.out.println("FacebookBolt: Crawling completed for Searched Page Posts");
										}
									}

								}

							}
							if (searchJson.getJSONObject("paging").has("next")) {
								strSearchURL = searchJson.getJSONObject("paging").getString("next") + "&since=" + strUnixStartDate;
								System.out.println("FacebookBolt: url for next page:" + strPostURL);
							} else {
								isEnd = true;
							}
						} else {
							isEnd = true;
							System.out.println("FacebookBolt: Crawling completed for Page Feeds of Searched Page");
						}
					}
				}

			}

			/**
			 * Updating Source Master for Execution of Source ID
			 */
			DBService.updateSourceStatus(1, sourceDTO.getSourceId(), false, null);

		} catch (Exception e) {
			System.err.println("**************************** UNHANDLED ERROR ::: " + e.getMessage());
			e.printStackTrace();
			_collector.fail(tuple);
		}

	}

	public void prepare(Map map, TopologyContext context, OutputCollector collector) {
		try {
			_prop.load(new FileInputStream((String) map.get("config.properties")));
			DataSource.setConfigPath((String) map.get("config.properties"));
			aspectMap = DBService.getAspectMap();
			postTypesMap = DBService.getPostTypeMap();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		_collector = collector;
	}

	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("pageInfo", "socialDatas"));
	}

}