package com.abzooba.sourcing.fb.bolt;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/*import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;*/
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.json.JSONArray;
import org.json.JSONObject;

import com.abzooba.sourcing.common.bean.AlertDTO;
import com.abzooba.sourcing.common.bean.SocialData;
import com.abzooba.sourcing.common.bean.SourceDTO;
import com.abzooba.sourcing.common.service.DBService;
import com.abzooba.sourcing.common.service.DataSource;
import com.abzooba.sourcing.fb.util.FacebookUtil;

public class CommentProcessBolt extends BaseRichBolt {

	private static final long serialVersionUID = 1L;
	private OutputCollector _collector;

	private Properties _prop = new Properties();

	private HashMap<String, Integer> aspectMap;
	private HashMap<String, Integer> postTypesMap;

	private SimpleDateFormat _format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static SimpleDateFormat _dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	private static SimpleDateFormat _hourFormat = new SimpleDateFormat("HH:00:00");

	private SimpleDateFormat fmtDateHH = new SimpleDateFormat("yyyy-MM-dd HH");

	public CommentProcessBolt() throws ClassNotFoundException, IllegalAccessException, SQLException, InstantiationException {
		try {
			//_prop.load(new FileInputStream(Constants.PROPERTIES_PATH));
			//_prop.load(new FileInputStream(System.getProperty("propfile")));
			//_prop.load(CommentProcessBolt.class.getClassLoader().getResourceAsStream("config.properties"));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void execute(Tuple tuple) {

		SourceDTO sourceDTO = (SourceDTO) tuple.getValueByField("sourcevo");
		String strPageId = tuple.getStringByField("pageid");
		String strPostId = tuple.getStringByField("postid");
		String strFacebookToken = tuple.getStringByField("facebooktoken");
		//int postTypeId2 = tuple.getIntegerByField("posttypeid");
		int postTypeId2 = postTypesMap.get("Comment");
		String strCommentURL = "";
		String strMaxCommentDate = "no-data";
		String strCommentMessage = null;
		boolean isEnd = false;

		JSONObject json = new JSONObject();
		JSONObject commentJsonObj = new JSONObject();
		JSONArray commentJsonArray = null;
		JSONObject commentJson = new JSONObject();

		try {

			strCommentURL = "https://graph.facebook.com/v2.7/" + strPostId + "?limit=25" + "&fields=likes.limit(0).summary(total_count),comments.order(reverse_chronological).summary(total_count){id,message,link,from,created_time,like_count,comment_count},shares&access_token=" + strFacebookToken;
			System.out.println("Comment URL :: " + strCommentURL);

			while (!isEnd) {
				try {
					json = FacebookUtil.getJsonFromUrl(_prop, strCommentURL);
					if (json == null) {
						isEnd = true;
						continue;
					}

					if (json.has("error")) {
						AlertDTO alertDTO = new AlertDTO();
						alertDTO.setSubject("Data fetching error");
						alertDTO.setAlertCategoryId(2);
						alertDTO.setSourceId(sourceDTO.getSourceId());
						alertDTO.setDateHH(fmtDateHH.format(new Date()));
						alertDTO.setDescription("May be the page is not authorized to fetch data. " + json.getString("error"));
						alertDTO.setAlertType("Major");
						alertDTO.setPriority(2);
						alertDTO.setActive(true);
						alertDTO.setAuthor("system");
						DBService.saveAlert(alertDTO);
						isEnd = true;
						continue;
					}

					if (json.has("comments") && json.get("comments") != null) {
						commentJsonObj = json.getJSONObject("comments");
						commentJsonArray = commentJsonObj.getJSONArray("data");
					} else if (strCommentURL.contains("comments?") && json.has("data") && json.get("data") != null) {
						commentJsonObj = json;
						commentJsonArray = commentJsonObj.getJSONArray("data");
					}

					if (commentJsonArray != null && commentJsonArray.length() > 0) {
						Set<SocialData> listComments = new LinkedHashSet<SocialData>();
						listComments = FacebookUtil.getPostData(_prop, sourceDTO, commentJsonArray, strPostId, strFacebookToken, postTypeId2, aspectMap);
						System.out.println("CommentProcessBolt: Emitting SOCIAL_DATA data to save in database");
						_collector.emit(tuple, new Values(null, listComments));
						_collector.ack(tuple);

						if (commentJsonObj.has("paging") && commentJsonObj.getJSONObject("paging").has("next")) {
							//System.out.println("PAGING OBJECT PRINT ::" + commentJsonObj.getJSONObject("paging"));
							strCommentURL = commentJsonObj.getJSONObject("paging").getString("next");
							System.out.println("CommentProcessBolt: url for next page:" + strCommentURL);
						} else {
							isEnd = true;
						}

					} else {
						isEnd = true;
						System.out.println("FacebookCommentBolt: Crawling completed");
					}
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("CommentProcessBolt: Error found in processing page wise comment:" + e.getMessage() + strCommentURL);
					System.out.println("CommentProcessBolt: Error found in comment process....continue");
					isEnd = true;
				}
			}

		} catch (Exception e) {
			System.err.println("**************************** UNHANDLED ERROR ::: " + e.getMessage());
			e.printStackTrace();
			_collector.fail(tuple);
		}
	}

	public void prepare(Map map, TopologyContext context, OutputCollector collector) {
		System.out.println("FBComment Bolt :: " + map.toString());
		try {
			_prop.load(new FileInputStream((String) map.get("config.properties")));
			DataSource.setConfigPath((String) map.get("config.properties"));
			aspectMap = DBService.getAspectMap();
			postTypesMap = DBService.getPostTypeMap();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		_collector = collector;
	}

	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("pageInfo", "socialDatas"));
	}

}
