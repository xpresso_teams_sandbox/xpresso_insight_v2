package com.abzooba.sourcing.fb.spout;

import java.io.FileInputStream;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/*import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;*/
import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;
import org.apache.storm.utils.Utils;

import com.abzooba.sourcing.common.bean.AlertDTO;
import com.abzooba.sourcing.common.bean.SourceDTO;
import com.abzooba.sourcing.common.service.DBService;
import com.abzooba.sourcing.common.service.DataSource;
import com.abzooba.sourcing.common.util.DateUtil;
import com.abzooba.sourcing.fb.util.FacebookUtil;

public class FacebookReplySpout extends BaseRichSpout {
	private static final long serialVersionUID = 1L;
	protected SpoutOutputCollector _collector;
	private Properties _prop = new Properties();

	private SimpleDateFormat _format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private SimpleDateFormat fmtDateHH = new SimpleDateFormat("yyyy-MM-dd HH");

	public FacebookReplySpout() throws ClassNotFoundException, IllegalAccessException, SQLException, InstantiationException {
	}

	public void nextTuple() {
		try {
			String strFacebookToken = "";
			int iCountTokenGeneration = 0;
			int nMinutes = Integer.parseInt(_prop.getProperty("fb_reply_sleep_time_in_minutes").trim());
			int nDaysPostbackComment = Integer.parseInt(_prop.getProperty("fb_back_reply_day"));
			List<SourceDTO> sources = DBService.getSources(nMinutes, "FB", "fb-reply");

			for (SourceDTO sourceDTO : sources) {
				System.out.println("Crawling for " + sourceDTO.getPageURL());
				if (iCountTokenGeneration == 0) {
					//System.out.println("FacebookReplySpout: Generating Facebook token...");
					if (_prop.getProperty("user_token").equalsIgnoreCase("1")) {
						strFacebookToken = FacebookUtil.generateUserAccessToken();
					} else {
						strFacebookToken = FacebookUtil.generateAccessToken(_prop, sourceDTO.getSourceTypeId(), sourceDTO.getSourceId());
					}
				}

				//_collector.emit(new Values(sourceDTO, strFacebookToken), sourceDTO.getSourceId());
				//System.out.println("FacebookCommentSpout: Tuple emitted successfully:" + sourceDTO.toString() + ", " + strFacebookToken);

				if (strFacebookToken != null) {
					String strFromDate = _format.format(DateUtil.getBeforeOrAfterDate(-nDaysPostbackComment));
					String strToDate = _format.format(new Date());

					System.out.println("FacebookReplySpout: start date:" + strFromDate);
					System.out.println("FacebookReplySpout: end date:" + strToDate);

					Map<String, String> commentMap = DBService.getPostAndComments(sourceDTO, strFromDate, strToDate);
					String strPageId = DBService.getPageId(sourceDTO.getSourceId());
					System.out.println("Outside Loop");
					for (Map.Entry<String, String> elem : commentMap.entrySet()) {
						String strCommentId = elem.getKey();
						String strPostId = elem.getValue();

						//System.out.println("Inside Loop");
						_collector.emit(new Values(sourceDTO, strPageId, strPostId, strCommentId, strFacebookToken), sourceDTO.getSourceId());
					}
				} else {
					System.out.println("There is no FB token to be used to fetching data from FB");

					AlertDTO alertDTO = new AlertDTO();
					alertDTO.setSubject("No Facebook Token");
					alertDTO.setAlertCategoryId(1);
					alertDTO.setSourceId(sourceDTO.getSourceId());
					alertDTO.setDateHH(fmtDateHH.format(new Date()));
					alertDTO.setDescription("There is no FB token to be used to fetching data from FB");
					alertDTO.setAlertType("Critical");
					alertDTO.setPriority(1);
					alertDTO.setActive(true);
					alertDTO.setAuthor("system");
					DBService.saveAlert(alertDTO);
				}

				/**
				 * Updating Source Master for Execution of Source ID
				 */
				DBService.updateSourceStatus(1, sourceDTO.getSourceId(), false, "fb-reply");
			}
			Utils.sleep(nMinutes * 60 * 1000);// wait for 5 minutes
		} catch (Exception e) {

			System.out.println("Exception generted  :: " + e.getMessage());
			e.printStackTrace();
		}

	}

	public void open(Map map, TopologyContext context, SpoutOutputCollector collector) {
		_collector = collector;
		try {
			//_prop.load(new FileInputStream(Constants.PROPERTIES_PATH));
			//_prop.load(new FileInputStream(System.getProperty("propfile")));
			//_prop.load(FacebookCommentSpout.class.getClassLoader().getResourceAsStream("config.properties"));
			_prop.load(new FileInputStream((String) map.get("config.properties")));
			DataSource.setConfigPath((String) map.get("config.properties"));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		//declarer.declare(new Fields("sourcevo", "facebooktoken"));
		declarer.declare(new Fields("sourcevo", "pageid", "postid", "commentid", "facebooktoken"));
	}
}
