package com.abzooba.sourcing.fb.bolt;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/*import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;*/
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

import com.abzooba.sourcing.common.bean.SourceDTO;
import com.abzooba.sourcing.common.service.DBService;
import com.abzooba.sourcing.common.service.DataSource;
import com.abzooba.sourcing.common.util.DateUtil;

public class CommentDataFetchingBolt extends BaseRichBolt {

	private static final long serialVersionUID = 1L;

	private OutputCollector _collector;
	
	private int _inumdayspostbackcomment;

	private Properties _prop = new Properties();
	private SimpleDateFormat _format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	public CommentDataFetchingBolt() throws ClassNotFoundException, IllegalAccessException, SQLException, InstantiationException {
		try {
			//_prop.load(new FileInputStream(Constants.PROPERTIES_PATH));
			//_prop.load(new FileInputStream(System.getProperty("propfile")));
			//_prop.load(CommentDataFetchingBolt.class.getClassLoader().getResourceAsStream("config.properties"));
			// getting how many days post back comments is required
			//_inumdayspostbackcomment = Integer.parseInt(_prop.getProperty("fb_back_comment_day"));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void execute(Tuple tuple) {
		
		SourceDTO sourceDTO = (SourceDTO)tuple.getValueByField("sourcevo");
		String strFacebookToken = tuple.getStringByField("facebooktoken");
		
		_inumdayspostbackcomment = Integer.parseInt(_prop.getProperty("fb_back_comment_day"));
		
		try {
			// calculate back date
			String strFromDate = _format.format(DateUtil.getBeforeOrAfterDate(-_inumdayspostbackcomment));
			String strToDate = _format.format(new Date());

			System.out.println("CommentDataFetchingBolt: start date:" + strFromDate);
			System.out.println("CommentDataFetchingBolt: end date:" + strToDate);

			Map<String, List<String>> postMap = DBService.getPageAndPosts(sourceDTO.getSourceId(), strFromDate, strToDate);
			
			for(Map.Entry<String, List<String>> elem : postMap.entrySet()) {
				String strPageId = elem.getKey();
				List<String> posts = elem.getValue();
				
				for (String strPostId : posts) {
					//System.out.println("CommentDataFetchingBolt: Emitting comment Data for further processing...");
					_collector.emit(tuple, new Values(sourceDTO, strPageId, strPostId, strFacebookToken));
				}
			}
			_collector.ack(tuple);

		} catch (Exception e) {
			_collector.fail(tuple);
		}
	}

	public void prepare(Map map, TopologyContext context, OutputCollector collector) {
		System.out.println("RSS Bolt :: " + map.toString());
		try {
			_prop.load(new FileInputStream((String) map.get("config.properties")));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		DataSource.setConfigPath((String) map.get("config.properties"));
		_collector = collector;
	}

	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("sourcevo", "pageid", "postid", "facebooktoken"));
	}

}