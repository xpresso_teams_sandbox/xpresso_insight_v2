package com.abzooba.sourcing.common.bean;

import java.io.Serializable;

public class SourceDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private int sourceId;
	private int sourceTypeId;
	private int shortNameId;
	private int domainId;
	private String pageName;
	private String pageURL;
	private int xpressoEnabled;
	private String xpressoDomain;
	private int searchStatus;
	private String searchWords;
	private String centerLocations;
	private String coordinates;
	private int range;

	/**
	 * @return the sourceId
	 */
	public int getSourceId() {
		return sourceId;
	}

	/**
	 * @param sourceId the sourceId to set
	 */
	public void setSourceId(int sourceId) {
		this.sourceId = sourceId;
	}

	/**
	 * @return the sourceTypeId
	 */
	public int getSourceTypeId() {
		return sourceTypeId;
	}

	/**
	 * @param sourceTypeId the sourceTypeId to set
	 */
	public void setSourceTypeId(int sourceTypeId) {
		this.sourceTypeId = sourceTypeId;
	}

	/**
	 * @return the domainId
	 */
	public int getDomainId() {
		return domainId;
	}

	/**
	 * @param domainId the domainId to set
	 */
	public void setDomainId(int domainId) {
		this.domainId = domainId;
	}

	/**
	 * @return the pageName
	 */
	public String getPageName() {
		return pageName;
	}

	/**
	 * @param pageName the pageName to set
	 */
	public void setPageName(String pageName) {
		this.pageName = pageName;
	}

	/**
	 * @return the pageURL
	 */
	public String getPageURL() {
		return pageURL;
	}

	/**
	 * @param pageURL the pageURL to set
	 */
	public void setPageURL(String pageURL) {
		this.pageURL = pageURL;
	}

	/**
	 * @return the xpressoEnabled
	 */
	public int isXpressoEnabled() {
		return xpressoEnabled;
	}

	/**
	 * @param xpressoEnabled the xpressoEnabled to set
	 */
	public void setXpressoEnabled(int xpressoEnabled) {
		this.xpressoEnabled = xpressoEnabled;
	}

	public int getSearchStatus() {
		return searchStatus;
	}

	public void setSearchStatus(int searchStatus) {
		this.searchStatus = searchStatus;
	}

	public String getSearchWords() {
		return searchWords;
	}

	public void setSearchWords(String searchWords) {
		this.searchWords = searchWords;
	}

	public String getCenterLocations() {
		return centerLocations;
	}

	public void setCenterLocations(String centerLocations) {
		this.centerLocations = centerLocations;
	}

	public String getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(String coordinates) {
		this.coordinates = coordinates;
	}

	public int getRange() {
		return range;
	}

	public void setRange(int range) {
		this.range = range;
	}

	public int getShortNameId() {
		return shortNameId;
	}

	public void setShortNameId(int shortNameId) {
		this.shortNameId = shortNameId;
	}

	public String getXpressoDomain() {
		return xpressoDomain;
	}

	public void setXpressoDomain(String xpressoDomain) {
		this.xpressoDomain = xpressoDomain;
	}

	public int getXpressoEnabled() {
		return xpressoEnabled;
	}

	@Override
	public String toString() {
		return "SourceDTO [sourceId=" + sourceId + ", sourceTypeId=" + sourceTypeId + ", domainId=" + domainId + ", pageName=" + pageName + ", pageURL=" + pageURL + ", xpressoEnabled=" + xpressoEnabled + "]";
	}

}
