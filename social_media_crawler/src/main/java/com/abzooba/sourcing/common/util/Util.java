package com.abzooba.sourcing.common.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.math.BigInteger;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.PasswordAuthentication;
import java.net.Proxy;
import java.net.Socket;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509ExtendedTrustManager;

import com.abzooba.sourcing.common.bean.PageInfo;
import com.abzooba.sourcing.common.bean.SocialData;
import com.abzooba.sourcing.common.bean.SourceDTO;
import com.abzooba.sourcing.common.bean.XpressoValues;
import com.abzooba.sourcing.common.service.DBService;

public class Util {

	public static void getXpressoAnalysis(Properties prop, SocialData socialData, Connection connection) throws SQLException {

		String responseText = "";

		try {
			URL baseURL = new URL("http://" + prop.getProperty("server") + ":" + prop.getProperty("port") + "/abzooba/engine/result/");
			HttpURLConnection httpConnection = (HttpURLConnection) baseURL.openConnection();
			httpConnection.setRequestMethod("POST");
			httpConnection.setDoOutput(true);
			httpConnection.setRequestProperty("Content-Type", "text/plain;");

			BufferedWriter httpRequestBodyWriter = new BufferedWriter(new OutputStreamWriter(httpConnection.getOutputStream()));
			String strData = "";
			if (prop.getProperty("apikey") != null)
				strData += "apikey=" + prop.getProperty("apikey") + prop.getProperty("delimeter");
			if (prop.getProperty("apisecret") != null)
				strData += "apisecret=" + prop.getProperty("apisecret") + prop.getProperty("delimeter");

			//"source=" + nSourceId + prop.getProperty("delimeter") +
			strData += "annotation=" + prop.getProperty("annotation") + prop.getProperty("delimeter") + "domain=" + socialData.getXpressoDomain() + prop.getProperty("delimeter") + "hist=" + prop.getProperty("hist") + prop.getProperty("delimeter") + "trend=" + prop.getProperty("trend") + prop.getProperty("delimeter") + "feedback=" + socialData.getXpressoReview();

			httpRequestBodyWriter.write(strData);
			httpRequestBodyWriter.close();

			BufferedReader reader = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
			String line;
			while ((line = reader.readLine()) != null)
				responseText += line + '\n';

			System.out.println("Parsing xpresso output in XpressoValues bean...");
			XpressoValues xpressoValues = new XpressoValues(responseText);
			xpressoValues.parseXpressoJson(prop);

			if (xpressoValues.isParseStatus()) {
				System.out.println("Saving data into XPRESSO_OUTPUT table...");
				DBService.saveXpressoData(connection, socialData, xpressoValues);
				socialData.setOverallAspectId(socialData.getAspectMap().get(xpressoValues.getOverallAspect()));
				socialData.setOverallSentiment(xpressoValues.getOverallSentiment());
				socialData.setOverallSentimentScore(xpressoValues.getOverallSentimentScore());
				socialData.setOverallSentiment_number(xpressoValues.getOverallSentimentNumber());
				socialData.setIsAnalyzed(2);
				//int nColumn = DBService.updateSocialData(socialData, xpressoValues);
				//System.out.println("Update SOCIAL_DATA table... column updated " + nColumn);
			} else {
				socialData.setIsAnalyzed(3);
				//DBService.updateSocialData(3, nSourceId, strPostId);
				System.out.println("Error in parsing Xpresso for the given text :: " + socialData.getXpressoReview());
			}
			/*System.out.println("Saving data into XPRESSO_OUTPUT table...");
			DBService.saveXpressoData(nSourceId, strPostId, xpressoValues);*/
			/*if(!xpressoValues.getTrendingTopics().isEmpty()) {
				System.out.println("Delete all data from TRENDING_TOPICS table...");
				DBService.deleteTrendingTopicData();
				System.out.println("Insert data to TRENDING_TOPICS table...");
				DBService.saveTrendingTopic(xpressoValues);
			}*/

			reader.close();

		} catch (IOException e) {
			socialData.setIsAnalyzed(3);
			//DBService.updateSocialData(3, nSourceId, strPostId);
			System.err.println("IO ERROR in Xpresso Parsing in getXpressoAnalysis :: " + e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			socialData.setIsAnalyzed(3);
			//DBService.updateSocialData(3, nSourceId, strPostId);
			//System.err.println(e.getMessage());
			System.err.println("ERROR in Xpresso Parsing in getXpressoAnalysis :: " + e.getMessage());
			System.err.println("XPRESSO RESPONSE ::  " + responseText);
			System.err.println("XPRESSO REVIEW TEXT :: " + socialData.getXpressoReview());
			e.printStackTrace();
		}
	}

	/*public static void initiateXpressoCall(Properties prop, SocialData socialData) throws Exception {
	
		String strDomain = null;
		int nXpressoEnable = 0;
	
		Map<String, Object> valueMap = DBService.getDomainAndXpressoStatus(nSourceId);
		if (valueMap != null && valueMap.size() > 0) {
			strDomain = (String) valueMap.get("domain");
			nXpressoEnable = (Integer) valueMap.get("call_xpresso");
		} else
			throw new Exception("Domain and Xpresso calling status is not set in db or config file...");
	
		try {
			if (nXpressoEnable == 1) {
				int nRecords = DBService.findSocialData(strPostId, strParentId, nSourceId);
				if (nRecords == 1) {
					DBService.updateSocialData(1, nSourceId, strPostId);
					getXpressoAnalysis(prop, nSourceId, strPostId, strDomain, strReview);
				} else {
					System.out.println("Record not saved and Xpresso called");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	public static String ConvertTomd5(String source) throws NoSuchAlgorithmException {
		MessageDigest m = MessageDigest.getInstance("MD5");
		m.update(source.getBytes(), 0, source.length());
		return new BigInteger(1, m.digest()).toString(16);
	}

	public static int stringToInt(String strValue) {
		try {
			return Integer.parseInt(strValue);
		} catch (Exception e) {
			System.out.println("Property is not defind in config file, return 1 as default value...");
			return 1;
		}
	}

	public static void prepareSocialDataBean(SocialData socialData, SourceDTO sourceDTO) {

		try {
			socialData.setSourceId(sourceDTO.getSourceId());
			socialData.setSourceTypeId(sourceDTO.getSourceTypeId());
			socialData.setXpressoDomain(sourceDTO.getXpressoDomain());
			socialData.setXpressoEnabled(sourceDTO.getXpressoEnabled());
			socialData.setShortnameId(sourceDTO.getShortNameId());
		} catch (Exception e) {
			System.err.println("Error occured while moving Data from SOURCE MASTER to SOCIAL DATA Bean :: " + e.getMessage());
			e.printStackTrace();
		}

	}

	public static String getDataFromProxy(Properties prop, String strURL, Map<String, Object> attributeMap) throws IOException {

		StringBuffer tmp = new StringBuffer();
		String line = null;
		HttpURLConnection uc = null;
		System.out.println("URL to connect via PROXY :: " + strURL);
		URL url = new URL(strURL);

		if (prop.getProperty("http_proxy_enable").equalsIgnoreCase("Y")) {
			System.out.println("setting up proxy server...");
			if (prop.containsKey("http_proxy_user") && prop.getProperty("http_proxy_user").trim() != null) {
				Authenticator.setDefault(new Authenticator() {
					@Override
					public PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(prop.getProperty("http_proxy_user").trim(), prop.getProperty("http_proxy_password").trim().toCharArray());
					}
				});
				System.setProperty("http.proxyUser", prop.getProperty("http_proxy_user").trim());
				System.setProperty("http.proxyPassword", prop.getProperty("http_proxy_password").trim());
			}

			Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(prop.getProperty("http_proxy_host").trim(), Integer.parseInt(prop.getProperty("http_proxy_port").trim()))); // or whatever your proxy is
			uc = (HttpURLConnection) url.openConnection(proxy);
			uc.setDoOutput(true);
			//System.out.println("Proxy CONNECTED" + uc.getResponseMessage());

		} else {
			System.out.println("No proxy setup required...");
			uc = (HttpURLConnection) url.openConnection();
			uc.setRequestMethod("GET");

			//System.out.println("RESPONSE" + uc.getResponseMessage());

		}
		//uc.setRequestMethod("GET");

		/*if(!strURL.contains("graph.facebook.com")){
			//uc.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0)");
			uc.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.152 Safari/537.36");
			uc.setConnectTimeout(20 * 1000);
		}*/

		if (attributeMap != null) {
			for (Map.Entry<String, Object> elem : attributeMap.entrySet()) {
				uc.setRequestProperty(elem.getKey(), (String) elem.getValue());
				//System.out.println(" HEADER :: " + elem.getKey() + " VALUE :: " + (String) elem.getValue());
			}
			System.out.println("RESPONSE" + uc.getResponseMessage());
		} else {
			System.out.println("RESPONSE" + uc.getResponseMessage());
		}

		uc.connect();

		BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream(), Charset.forName("UTF-8")));
		BufferedReader inCheck = new BufferedReader(new InputStreamReader(uc.getInputStream()));
		while ((line = in.readLine()) != null) {
			tmp.append(line);
		}
		String line2;
		String responseText = "";
		while ((line2 = inCheck.readLine()) != null) {
			responseText += line;
		}
		//System.out.println("***************Response using Character Set :: " + tmp.toString());
		//System.out.println("***************Response using WITHOUT Character Set :: " + responseText);
		return tmp.toString();
	}

	public static String getSSLDataFromProxy(Properties prop, String strURL, Map<String, Object> attributeMap) throws Exception {

		StringBuffer tmp = new StringBuffer();
		String connectionResponse = "";
		String line = null;
		String responseText;
		HttpURLConnection uc = null;
		HttpURLConnection huc = null;
		System.out.println("URL to connect via PROXY :: " + strURL);
		URL url = new URL(strURL);
		if (prop.getProperty("http_proxy_enable").equalsIgnoreCase("Y")) {
			System.out.println("setting up proxy server...");
			// Create a trust manager that does not validate certificate chains
			TrustManager[] trustAllCerts = new TrustManager[] { new X509ExtendedTrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				public void checkClientTrusted(X509Certificate[] certs, String authType) {
				}

				public void checkServerTrusted(X509Certificate[] certs, String authType) {
				}

				@Override
				public void checkClientTrusted(X509Certificate[] chain, String authType, Socket socket) throws CertificateException {

				}

				@Override
				public void checkClientTrusted(X509Certificate[] chain, String authType, SSLEngine engine) throws CertificateException {

				}

				@Override
				public void checkServerTrusted(X509Certificate[] chain, String authType, Socket socket) throws CertificateException {

				}

				@Override
				public void checkServerTrusted(X509Certificate[] chain, String authType, SSLEngine engine) throws CertificateException {

				}
			} };

			// Install the all-trusting trust manager
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			System.out.println("Certification of trustmanager");
			// Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {

				public boolean verify(String hostname, SSLSession session) {
					return true;
				}

			};

			// Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

			System.setProperty("http.proxyUser", prop.getProperty("http_proxy_user"));
			System.setProperty("http.proxyPassword", prop.getProperty("http_proxy_password"));
			System.setProperty("http.proxyHost", prop.getProperty("http_proxy_host"));
			System.setProperty("http.proxyPort", prop.getProperty("http_proxy_port"));
			System.setProperty("https.proxyHost", prop.getProperty("http_proxy_host"));
			System.setProperty("https.proxyPort", prop.getProperty("http_proxy_port"));
			System.setProperty("sun.security.ssl.allowUnsafeRenegotiation", "true");
			System.setProperty("com.sun.net.ssl.checkRevocation", "false");

			Util utilProxy = new Util();
			System.out.println("System Proxy Done");
			System.err.println("TRYING WITHOUT USER AGENT FOR  URL ::  " + strURL);
			uc = (HttpURLConnection) url.openConnection();
			uc.setDoOutput(true);
			//CHANGING USER AGENT JUST FOR FORUM IN MCI
			//uc.addRequestProperty("User-Agent", Constants.USER_AGENT);
			System.out.println("Proxy CONNECTED" + uc.getResponseMessage());
		} else {
			System.out.println("No proxy setup required...");
			uc = (HttpURLConnection) url.openConnection();
			uc.setDoOutput(true);
			uc.addRequestProperty("User-Agent", Constants.USER_AGENT);
		}
		//uc.setRequestMethod("GET");

		/*if (attributeMap != null) {
			for (Map.Entry<String, Object> elem : attributeMap.entrySet()) {
				uc.setRequestProperty(elem.getKey(), (String) elem.getValue());
			}
		}*/
		uc.setConnectTimeout(50 * 1000);
		//uc.connect();
		System.out.println("*********RESPONSE on CONNECTING WITH THE WEBSITE" + uc.getResponseMessage());
		//String responseText = Jsoup.parse(uc.getInputStream(), "UTF-8", "").text();
		connectionResponse = uc.getResponseMessage();
		if (connectionResponse.equalsIgnoreCase("OK")) {
			BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream(), Charset.forName("UTF-8")));
			while ((line = in.readLine()) != null) {
				tmp.append(line);
			}
		} else {
			int index = strURL.indexOf(':');
			String intialCall = strURL.substring(0, index);
			String newCall;
			if (intialCall.endsWith("s")) {
				newCall = "http";
			} else {
				newCall = "https";
			}
			System.out.println("Initial Get Request :: " + intialCall);
			System.out.println("Finally Get Request :: " + newCall);

			String newURL = newCall + strURL.substring(index, strURL.length());
			URL newUrl = new URL(newURL);

			try {
				System.err.println("TRYING WITH USER AGENT FOR NEW URL ::  " + strURL);
				huc = (HttpURLConnection) newUrl.openConnection();
				huc.setDoOutput(true);
				huc.addRequestProperty("User-Agent", Constants.USER_AGENT);
				huc.setConnectTimeout(50 * 1000);
				System.out.println("*********NEW RESPONSE on CONNECTING WITH THE WEBSITE" + huc.getResponseMessage());
				connectionResponse = huc.getResponseMessage();
			} catch (Exception e) {
				System.err.println("ERROR in connection while in HTTP/HTTPS toggle :: " + e.getMessage());
				//e.printStackTrace();
			}

			if (connectionResponse.equalsIgnoreCase("OK")) {
				BufferedReader in = new BufferedReader(new InputStreamReader(huc.getInputStream(), Charset.forName("UTF-8")));
				while ((line = in.readLine()) != null) {
					tmp.append(line);
				}
			} else {
				System.out.println("TRYING WITHOUT USER AGENT FOR NEW URL USER AGENT :: " + newUrl);

				try {
					huc = (HttpURLConnection) newUrl.openConnection();
					huc.setDoOutput(true);
					//huc.addRequestProperty("User-Agent", Constants.USER_AGENT);
					huc.setConnectTimeout(50 * 1000);
					System.out.println("*********NEW RESPONSE on CONNECTING WITH THE WEBSITE" + huc.getResponseMessage());
					connectionResponse = huc.getResponseMessage();
				} catch (Exception e) {
					System.err.println("ERROR in connection while in toggled URL without USER AGENT:: " + e.getMessage());
					//e.printStackTrace();
				}

				if (connectionResponse.equalsIgnoreCase("OK")) {
					BufferedReader in = new BufferedReader(new InputStreamReader(huc.getInputStream(), Charset.forName("UTF-8")));
					while ((line = in.readLine()) != null) {
						tmp.append(line);
					}
				} else {
					System.err.println("TRYING WITH USER AGENT FOR OLD URL ::  " + strURL);
					URL oldUrl = new URL(strURL);

					try {
						huc = (HttpURLConnection) oldUrl.openConnection();
						huc.setDoOutput(true);
						huc.addRequestProperty("User-Agent", Constants.USER_AGENT);
						huc.setConnectTimeout(50 * 1000);
						System.out.println("*********NEW RESPONSE on CONNECTING WITH THE WEBSITE" + huc.getResponseMessage());
						connectionResponse = huc.getResponseMessage();
					} catch (Exception e) {
						System.out.println("ERROR in connection while in ORIGINAL URL without USER AGENT:: " + e.getMessage());
						//e.printStackTrace();
					}

					if (connectionResponse.equalsIgnoreCase("OK")) {
						BufferedReader in = new BufferedReader(new InputStreamReader(huc.getInputStream(), Charset.forName("UTF-8")));
						while ((line = in.readLine()) != null) {
							tmp.append(line);
						}
					} else {
						System.out.println("COULD NOT SUCCESSFULLY RETRIEVE RSS DATA : " + strURL);
					}
				}

			}
		}

		//System.out.println("***************Response using  Character Set :: " + tmp.toString());
		return tmp.toString();
	}

	public static Map<String, List<String>> getResponseHeaderFromProxy(Properties prop, String strURL) throws Exception {

		HttpURLConnection uc = null;
		System.out.println("URL to connect via PROXY :: " + strURL);
		URL url = new URL(strURL);

		// Create a trust manager that does not validate certificate chains
		TrustManager[] trustAllCerts = new TrustManager[] { new X509ExtendedTrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(X509Certificate[] certs, String authType) {
			}

			public void checkServerTrusted(X509Certificate[] certs, String authType) {
			}

			@Override
			public void checkClientTrusted(X509Certificate[] chain, String authType, Socket socket) throws CertificateException {

			}

			@Override
			public void checkClientTrusted(X509Certificate[] chain, String authType, SSLEngine engine) throws CertificateException {

			}

			@Override
			public void checkServerTrusted(X509Certificate[] chain, String authType, Socket socket) throws CertificateException {

			}

			@Override
			public void checkServerTrusted(X509Certificate[] chain, String authType, SSLEngine engine) throws CertificateException {

			}
		} };

		// Install the all-trusting trust manager
		SSLContext sc = SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, new java.security.SecureRandom());
		HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		System.out.println("Certification of trustmanager");
		// Create all-trusting host name verifier
		HostnameVerifier allHostsValid = new HostnameVerifier() {

			public boolean verify(String hostname, SSLSession session) {
				return true;
			}

		};

		// Install the all-trusting host verifier
		HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

		System.setProperty("http.proxyUser", prop.getProperty("http_proxy_user"));
		System.setProperty("http.proxyPassword", prop.getProperty("http_proxy_password"));
		System.setProperty("http.proxyHost", prop.getProperty("http_proxy_host"));
		System.setProperty("http.proxyPort", prop.getProperty("http_proxy_port"));
		System.setProperty("https.proxyHost", prop.getProperty("http_proxy_host"));
		System.setProperty("https.proxyPort", prop.getProperty("http_proxy_port"));
		System.setProperty("sun.security.ssl.allowUnsafeRenegotiation", "true");
		System.setProperty("com.sun.net.ssl.checkRevocation", "false");

		Util utilProxy = new Util();
		System.out.println("System Proxy Done");

		uc = (HttpURLConnection) url.openConnection();
		//uc.setDoOutput(true);
		//uc.addRequestProperty("User-Agent", Constants.USER_AGENT);
		System.out.println("Proxy CONNECTED" + uc.getResponseMessage());

		//uc.setRequestMethod("GET");		
		//uc.setConnectTimeout(50 * 1000);
		//uc.connect();
		System.out.println("*********STATUS on CONNECTING WITH THE WEBSITE :: " + uc.getResponseCode());
		System.out.println("*********RESPONSE on CONNECTING WITH THE WEBSITE" + uc.getResponseMessage());
		uc.disconnect();
		Map<String, List<String>> rheader = uc.getRequestProperties();
		for (Map.Entry<String, List<String>> field : rheader.entrySet()) {
			System.out.println("KEY :: " + field.getKey());
			System.out.println("Value :: " + field.getValue().toString());
		}
		return rheader;
	}

	public static String removeXmlStringNamespaceAndPreamble(String xmlString) {
		return xmlString.replaceAll("(<\\?[^<]*\\?>)?", "") /* remove preamble */
				.replaceAll("xmlns.*?(\"|\').*?(\"|\')", "") /* remove xmlns declaration */
				.replaceAll("(<)(\\w+:)(.*?>)", "$1$3") /* remove opening tag prefix */
				.replaceAll("(</)(\\w+:)(.*?>)", "$1$3") /* remove closing tags prefix */
				.replaceAll("&([^;]+(?!(?:\\w|;)))", "&amp;$1"); /* remove special char & */
	}

	public static PageInfo updatePageInfoValues(PageInfo pageInfo, PageInfo pageInfoExist) {
		/*
		 * Called from DBSave Bolt & DB Service for KPI Page LEVEL
		 */
		if (pageInfo.getPageLike() == 0)
			pageInfo.setPageLike(pageInfoExist.getPageLike());
		if (pageInfo.getPeopleTalkingAbout() == 0)
			pageInfo.setPeopleTalkingAbout(pageInfoExist.getPeopleTalkingAbout());
		if (pageInfo.getTotalFollowings() == 0)
			pageInfo.setTotalFollowings(pageInfoExist.getTotalFollowings());
		if (pageInfo.getTotalFollowers() == 0)
			pageInfo.setTotalFollowers(pageInfoExist.getTotalFollowers());
		if (pageInfo.getWereHere() == 0)
			pageInfo.setWereHere(pageInfoExist.getWereHere());
		if (pageInfo.getLikeCountry() == 0)
			pageInfo.setLikeCountry(pageInfoExist.getLikeCountry());
		if (pageInfo.getPageStorytellerCountry() == 0)
			pageInfo.setPageStorytellerCountry(pageInfoExist.getPageStorytellerCountry());

		return pageInfo;

	}

	public static SocialData updateSocialDataValues(SocialData socialData, SocialData socialDataExist) {

		/*
		 * Called from DB Service for KPI Post LEVEL & saveSocialData
		 */
		/*if (socialData.getBreakingNews() == null)
			socialData.setBreakingNews(socialDataExist.getBreakingNews());*/
		if (socialData.getCommentsCount() == 0)
			socialData.setCommentsCount(socialDataExist.getCommentsCount());
		if (socialData.getLikesCount() == 0)
			socialData.setLikesCount(socialDataExist.getLikesCount());
		if (socialData.getSharesCount() == 0)
			socialData.setSharesCount(socialDataExist.getSharesCount());
		if (socialData.getRetweetsCount() == 0)
			socialData.setRetweetsCount(socialDataExist.getRetweetsCount());
		if (socialData.getReplyCount() == 0)
			socialData.setReplyCount(socialDataExist.getReplyCount());
		if (socialData.getNoneCount() == 0)
			socialData.setNoneCount(socialDataExist.getNoneCount());
		if (socialData.getLoveCount() == 0)
			socialData.setLoveCount(socialDataExist.getLoveCount());
		if (socialData.getHahaCount() == 0)
			socialData.setHahaCount(socialDataExist.getHahaCount());
		if (socialData.getWowCount() == 0)
			socialData.setWowCount(socialDataExist.getWowCount());
		if (socialData.getSadCount() == 0)
			socialData.setSadCount(socialDataExist.getSadCount());
		if (socialData.getSadCount() == 0)
			socialData.setSadCount(socialDataExist.getSadCount());
		if (socialData.getThankfulCount() == 0)
			socialData.setThankfulCount(socialDataExist.getThankfulCount());

		return socialData;
	}

	public static SocialData updateRSSSocialDataValues(SocialData socialData, SocialData socialDataExist) {

		/*
		 * Called from DB Service for  saveRSSSocialData
		 */
		/*if (socialData.getBreakingNews() == null)
			socialData.setBreakingNews(socialDataExist.getBreakingNews());*/
		if (socialData.getHeadline() == null || socialData.getHeadline() == "")
			socialData.setHeadline(socialDataExist.getHeadline());

		if (socialData.getPostText() == null || socialData.getPostText() == "")
			socialData.setPostText(socialDataExist.getPostText());

		if (socialData.getAuthorName() == null || socialData.getAuthorName() == "")
			socialData.setAuthorName(socialDataExist.getAuthorName());

		if (socialData.getAuthorId() == null || socialData.getAuthorId() == "")
			socialData.setAuthorId(socialDataExist.getAuthorId());

		return socialData;
	}

	public static PageInfo updateKPIPageValues(PageInfo pageInfo, PageInfo pageInfoExist) {

		/*
		 * Called from DB Service for KPI Post LEVEL & saveSocialData
		 */
		/*if (socialData.getBreakingNews() == null)
			socialData.setBreakingNews(socialDataExist.getBreakingNews());*/
		if (pageInfo.getPageLike() == 0)
			pageInfo.setPageLike(pageInfoExist.getPageLike());
		if (pageInfo.getPeopleTalkingAbout() == 0)
			pageInfo.setPeopleTalkingAbout(pageInfoExist.getPeopleTalkingAbout());
		if (pageInfo.getPeopleWereHere() == 0)
			pageInfo.setPeopleWereHere(pageInfoExist.getPeopleWereHere());
		if (pageInfo.getLikeCountry() == 0)
			pageInfo.setLikeCountry(pageInfoExist.getLikeCountry());
		if (pageInfo.getPageStorytellerCountry() == 0)
			pageInfo.setPageStorytellerCountry(pageInfoExist.getPageStorytellerCountry());
		if (pageInfo.getTotalFollowings() == 0)
			pageInfo.setTotalFollowings(pageInfoExist.getTotalFollowings());
		if (pageInfo.getTotalFollowers() == 0)
			pageInfo.setTotalFollowers(pageInfoExist.getTotalFollowers());

		return pageInfo;
	}
}
