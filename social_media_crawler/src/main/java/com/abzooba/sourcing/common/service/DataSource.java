package com.abzooba.sourcing.common.service;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.commons.dbcp2.BasicDataSource;

public class DataSource {

	private BasicDataSource bds = new BasicDataSource();
	private static String datasource_type = "mysql";
	private static String configPath = "./config.properties";

	private DataSource() {
		Properties prop;
		try {
			//prop = RSSTopologyCreator._prop;
			//prop.load(new FileInputStream(Constants.PROPERTIES_PATH));
			//prop.load(new FileInputStream(System.getProperty("propfile")));
			prop = new Properties();
			//prop.load(DataSource.class.getClassLoader().getResourceAsStream("config.properties"));
			System.out.println("File path :: " + configPath);
			//prop.load(new FileInputStream(System.getProperty("propfile")));
			prop.load(new FileInputStream(configPath));

			bds.setDriverClassName(prop.getProperty("db_driver").trim());
			String dbURL = prop.getProperty("db_protocol").trim() + prop.getProperty("db_host").trim() + prop.getProperty("db_port").trim() + prop.getProperty("db_name").trim();
			System.out.println("Database URL :: " + dbURL);
			if (prop.getProperty("db_protocol").trim().contains("mysql")) {
				datasource_type = "mysql";
			}
			if (prop.getProperty("db_protocol").trim().contains("db2")) {
				datasource_type = "db2";
			}
			bds.setUrl(dbURL);
			bds.setUsername(prop.getProperty("db_user").trim());
			bds.setPassword(prop.getProperty("db_password").trim());
			int noOfConnection = Integer.parseInt(prop.getProperty("db_pool").trim());
			bds.setInitialSize(noOfConnection);
			System.out.println("DataSource type :: " + datasource_type);
			System.out.println("Basic DataSource Object created successfully with number of connection pool :: " + noOfConnection);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @return the configPath
	 */
	public static String getConfigPath() {
		return configPath;
	}

	/**
	 * @param configPath the configPath to set
	 */
	public static void setConfigPath(String configPath) {
		DataSource.configPath = configPath;
	}

	private static class DataSourceHolder {
		private static final DataSource INSTANCE = new DataSource();
	}

	public static DataSource getInstance() {
		return DataSourceHolder.INSTANCE;
	}

	private static class DataSourcTypeHolder {
		private static final String DATASOURCETYPE = datasource_type;
	}

	public static String getDataSourceType() {
		return DataSourcTypeHolder.DATASOURCETYPE;
	}

	public BasicDataSource getBds() {
		return bds;
	}

	public void setBds(BasicDataSource bds) {
		this.bds = bds;
	}

	public static void main(String args[]) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			statement = connection.prepareStatement("select post_id from social_data limit 10");
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				System.out.println(resultSet.getString("post_id"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (resultSet != null)
					resultSet.close();
				if (statement != null)
					statement.close();
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}
}