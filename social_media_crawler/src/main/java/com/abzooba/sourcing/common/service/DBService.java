package com.abzooba.sourcing.common.service;

import java.net.InetAddress;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.dbcp2.BasicDataSource;

import com.abzooba.sourcing.common.bean.AlertDTO;
import com.abzooba.sourcing.common.bean.PageInfo;
import com.abzooba.sourcing.common.bean.SocialData;
import com.abzooba.sourcing.common.bean.SourceDTO;
import com.abzooba.sourcing.common.bean.XpressoAbout;
import com.abzooba.sourcing.common.bean.XpressoValues;
import com.abzooba.sourcing.common.util.Constants;
import com.abzooba.sourcing.common.util.Util;

public class DBService {

	public static Map<String, Object> getDomainAndXpressoStatus(int nSourceId) throws SQLException {

		Map<String, Object> valueMap = new HashMap<>();
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			preparedStatement = connection.prepareStatement(Constants.SELECT_DOMAIN_AND_XPRESSO_CALL);
			preparedStatement.setInt(1, nSourceId);
			resultSet = preparedStatement.executeQuery();
			if (resultSet != null) {
				while (resultSet.next()) {
					valueMap.put("domain", resultSet.getString("xpresso_domain"));
					valueMap.put("call_xpresso", resultSet.getInt("xpresso_enable"));
				}
			}
		} catch (SQLException e) {
			System.err.println("SQL Message in getDomainAndXpressoStatus:: " + e.getMessage());
			System.err.println("SQL Error occurred in getDomainAndXpressoStatus::" + preparedStatement.toString());
		} catch (Exception e) {
			System.err.println("Error Message in getDomainAndXpressoStatus:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (resultSet != null)
				resultSet.close();
			if (preparedStatement != null)
				preparedStatement.close();
			if (connection != null)
				connection.close();
		}
		return valueMap;
	}

	public static String getMaxPostDate(int nSourceId, String strPostId) throws SQLException {
		String strMaxPostDate = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			String companyId = getcompanyId(nSourceId);
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			//preparedStatement = connection.prepareStatement(Constants.SELECT_MAX_POST_DATETIME);
			preparedStatement = connection.prepareStatement(Constants.SELECT_MAX_POST_DATETIME+Constants.SOCIAL_DATA+companyId+Constants.WHERE_MAX_POST_DATETIME);
			preparedStatement.setInt(1, nSourceId);
			preparedStatement.setString(2, strPostId);

			resultSet = preparedStatement.executeQuery();
			if (resultSet != null) {
				while (resultSet.next())
					strMaxPostDate = resultSet.getString("maxdate");
			}
		} catch (SQLException e) {
			System.err.println("SQL Message in getMaxPostDate:: " + e.getMessage());
			System.err.println("SQL Error occurred in getMaxPostDate::" + preparedStatement.toString());
		} catch (Exception e) {
			System.err.println("Error Message in getMaxPostDate:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (resultSet != null)
				resultSet.close();
			if (preparedStatement != null)
				preparedStatement.close();
			if (connection != null)
				connection.close();
		}
		return strMaxPostDate;
	}

	public static List<SourceDTO> getSources(int nMinutes, String sourceType, String strFBType) throws SQLException {
		List<SourceDTO> sources = new ArrayList<>();
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			String dataSourceType = DataSource.getDataSourceType();
			String sqlQuery = null;
			
			//get the IP address of machine
			InetAddress ipAddr = InetAddress.getLocalHost();
			String processor_ip_address=ipAddr.getHostAddress();
            System.out.println("Processor IP address:"+processor_ip_address);
            //eoc 
			
			if (dataSourceType.equalsIgnoreCase("mysql")) {
				if (strFBType != null && strFBType.equalsIgnoreCase("fb-comment")){
					sqlQuery = Constants.SELECT_FB_COMMENT_SOURCE_URL_MySQL;
					//checking with ipaddress
					sqlQuery = sqlQuery + " AND processor_ip='"+processor_ip_address+"'";
					System.out.println("Source query:"+ sqlQuery);
					//eoc 
				}	
				else if (strFBType != null && strFBType.equalsIgnoreCase("fb-reply")){
					sqlQuery = Constants.SELECT_FB_REPLY_SOURCE_URL_MySQL;
					//checking with ipaddress
					sqlQuery = sqlQuery + " AND processor_ip='"+processor_ip_address+"'";
					System.out.println("Source query:"+ sqlQuery);
					//eoc
				}
				else{
					sqlQuery = Constants.SELECT_SOURCE_URL_MySQL;
					//checking with ipaddress
					sqlQuery = sqlQuery + " AND processor_ip='"+processor_ip_address+"'";
					System.out.println("Source query:"+ sqlQuery);
					//eoc
				}	
			} else {
				if (strFBType != null && strFBType.equalsIgnoreCase("fb-comment"))
					sqlQuery = Constants.SELECT_FB_COMMENT_SOURCE_URL_DB2;
				else if (strFBType != null && strFBType.equalsIgnoreCase("fb-reply"))
					sqlQuery = Constants.SELECT_FB_REPLY_SOURCE_URL_DB2;
				else
					sqlQuery = Constants.SELECT_SOURCE_URL_DB2;
			}
			preparedStatement = connection.prepareStatement(sqlQuery);
			preparedStatement.setInt(1, nMinutes);
			preparedStatement.setString(2, sourceType);

			resultSet = preparedStatement.executeQuery();
			if (resultSet != null) {
				while (resultSet.next()) {
					SourceDTO source = new SourceDTO();
					source.setSourceId(resultSet.getInt("source_id"));
					source.setPageURL(resultSet.getString("source_url"));
					source.setSourceTypeId(resultSet.getInt("source_type_id"));
					source.setDomainId(resultSet.getInt("domain_id"));
					source.setXpressoEnabled(resultSet.getInt("xpresso_enable"));
					source.setXpressoDomain(resultSet.getString("xpresso_domain"));
					source.setShortNameId(resultSet.getInt("shortname_id"));
					source.setPageName(resultSet.getString("page_name"));
					source.setSearchStatus(resultSet.getInt("search"));
					source.setCenterLocations(resultSet.getString("geo_centers"));
					source.setCoordinates(resultSet.getString("cordinates"));
					source.setSearchWords(resultSet.getString("search_words"));
					source.setRange(resultSet.getInt("range") * 1000);

					sources.add(source);
				}
			}
		} catch (SQLException e) {
			System.err.println("SQL Message in getSources:: " + e.getMessage());
			System.err.println("SQL Error occurred in getSources::" + preparedStatement.toString());
		} catch (Exception e) {
			System.err.println("Error Message in getSources:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (resultSet != null)
				resultSet.close();
			if (preparedStatement != null)
				preparedStatement.close();
			if (connection != null)
				connection.close();
		}
		return sources;
	}

	public static PageInfo getPageInfo(String strPageId) throws SQLException {
		PageInfo pageInfo = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			preparedStatement = connection.prepareStatement(Constants.SELECT_PAGE_INFO);
			if (preparedStatement != null) {
				preparedStatement.setString(1, strPageId);
				resultSet = preparedStatement.executeQuery();

				if (resultSet != null) {
					while (resultSet.next()) {
						pageInfo = new PageInfo();
						pageInfo.setPageId(resultSet.getString("page_id"));
						pageInfo.setSourceId(resultSet.getInt("source_id"));
						pageInfo.setPageLike(resultSet.getLong("page_like"));
						pageInfo.setPeopleTalkingAbout(resultSet.getLong("people_talking_about"));
						pageInfo.setPeopleTalkingAbout(resultSet.getLong("were_here_count"));
						pageInfo.setTotalFollowings(resultSet.getLong("total_followings"));
						pageInfo.setTotalFollowers(resultSet.getLong("total_follower"));
						pageInfo.setPageURL(resultSet.getString("page_url"));
					}
				}
			}
		} catch (SQLException e) {
			System.err.println("SQL Message in getPageInfo:: " + e.getMessage());
			System.err.println("SQL Error occurred in getPageInfo::" + preparedStatement.toString());
		} catch (Exception e) {
			System.err.println("Error Message in getPageInfo:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (resultSet != null)
				resultSet.close();
			if (preparedStatement != null)
				preparedStatement.close();
			if (connection != null)
				connection.close();
		}
		return pageInfo;
	}

	public static PageInfo getKPIPageLevel(String strPageId, int sourceId) throws SQLException {
		PageInfo pageInfo = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			String dataSourceType = DataSource.getDataSourceType();
			if (dataSourceType.equalsIgnoreCase("mysql")) {
				preparedStatement = connection.prepareStatement(Constants.SELECT_KPI_PAGE_LEVEL);
			} else {
				preparedStatement = connection.prepareStatement(Constants.SELECT_KPI_PAGE_LEVEL_DB2);
			}

			if (preparedStatement != null) {
				preparedStatement.setString(1, strPageId);
				preparedStatement.setInt(2, sourceId);

				resultSet = preparedStatement.executeQuery();
				if (resultSet != null) {
					while (resultSet.next()) {
						pageInfo = new PageInfo();
						pageInfo.setPageId(resultSet.getString("page_id"));
						pageInfo.setSourceId(resultSet.getInt("source_id"));
						pageInfo.setPageLike(resultSet.getLong("page_like"));
						pageInfo.setPeopleTalkingAbout(resultSet.getLong("talking_about_count"));
						pageInfo.setPeopleTalkingAbout(resultSet.getLong("were_here"));
						pageInfo.setLikeCountry(resultSet.getLong("like_by_country"));
						pageInfo.setPageStorytellerCountry(resultSet.getLong("page_storyteller_by_country"));
						pageInfo.setTotalFollowings(resultSet.getLong("total_followings"));
						pageInfo.setTotalFollowers(resultSet.getLong("total_followers"));
					}
				}
			}
		} catch (SQLException e) {
			System.err.println("SQL Message in getKPIPageLevel:: " + e.getMessage());
			System.err.println("SQL Error occurred in getKPIPageLevel::" + preparedStatement.toString());
		} catch (Exception e) {
			System.err.println("Error Message in getKPIPageLevel:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (resultSet != null)
				resultSet.close();
			if (preparedStatement != null)
				preparedStatement.close();
			if (connection != null)
				connection.close();
		}
		return pageInfo;
	}

	public static SocialData getSocialData(String strPostId, String strParentId, int nSourceId, boolean fromKPI) throws SQLException {
		SocialData socialData = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			String companyId = getcompanyId(nSourceId);
			if (fromKPI) {
				preparedStatement = connection.prepareStatement(Constants.SELECT_KPI_POST_DATA);
				if (preparedStatement != null) {
					preparedStatement.setString(1, strPostId);
					preparedStatement.setInt(2, nSourceId);
					resultSet = preparedStatement.executeQuery();
					if (resultSet != null) {
						while (resultSet.next()) {
							socialData = new SocialData();
							socialData.setPostId(resultSet.getString("post_id"));
							//socialData.setParentId(resultSet.getString("parent_id"));
							socialData.setSourceId(resultSet.getInt("source_id"));
							//socialData.setBreakingNews(resultSet.getString("breaking_news"));
							socialData.setCommentsCount(resultSet.getInt("comments_count"));
							socialData.setLikesCount(resultSet.getInt("likes_count"));
							socialData.setSharesCount(resultSet.getInt("share_count"));
							socialData.setRetweetsCount(resultSet.getInt("retweet_count"));
							socialData.setReplyCount(resultSet.getInt("reply_count"));
							socialData.setNoneCount(resultSet.getInt("none_count"));
							socialData.setLoveCount(resultSet.getInt("love_count"));
							socialData.setHahaCount(resultSet.getInt("haha_count"));
							socialData.setWowCount(resultSet.getInt("wow_count"));
							socialData.setSadCount(resultSet.getInt("sad_count"));
							socialData.setAngryCount(resultSet.getInt("angry_count"));
							socialData.setThankfulCount(resultSet.getInt("thankful_count"));
						}
					}
				}

			} else {
				//preparedStatement = connection.prepareStatement(Constants.SELECT_SOCIAL_DATA);
				preparedStatement = connection.prepareStatement(Constants.SELECT_SOCIAL_DATA+Constants.SOCIAL_DATA+companyId+Constants.WHERE_SOCIAL_DATA);
				if (preparedStatement != null) {
					preparedStatement.setString(1, strPostId);
					preparedStatement.setString(2, strParentId);
					preparedStatement.setInt(3, nSourceId);
					resultSet = preparedStatement.executeQuery();
					if (resultSet != null) {
						while (resultSet.next()) {
							socialData = new SocialData();
							socialData.setPostId(resultSet.getString("post_id"));
							socialData.setParentId(resultSet.getString("parent_id"));
							socialData.setSourceId(resultSet.getInt("source_id"));
							socialData.setPostDateTime(resultSet.getString("post_datetime"));
							socialData.setPostText(resultSet.getString("post_text"));
							socialData.setHeadline(resultSet.getString("headline"));
							socialData.setIsUpdated(resultSet.getInt("is_updated"));
							socialData.setBreakingNews(resultSet.getString("breaking_news"));
							socialData.setCommentsCount(resultSet.getInt("comment_count"));
							socialData.setLikesCount(resultSet.getInt("likes_count"));
							socialData.setSharesCount(resultSet.getInt("share_count"));
							socialData.setRetweetsCount(resultSet.getInt("retweet_count"));
							socialData.setAuthorName(resultSet.getString("author_name"));
							socialData.setAuthorId(resultSet.getString("author_id"));
						}
					}
				}
			}

		} catch (SQLException e) {
			System.err.println("SQL Message in getSocialData:: " + e.getMessage());
			System.err.println("SQL Error occurred in getSocialData::" + preparedStatement.toString());
		} catch (Exception e) {
			System.err.println("Error Message in getSocialData:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (resultSet != null)
				resultSet.close();
			if (preparedStatement != null)
				preparedStatement.close();
			if (connection != null)
				connection.close();
		}
		return socialData;
	}

	/*public static SocialData getSocialData(String strPostId, int nSourceId) throws SQLException {
		SocialData socialData = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
	
		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
	
			//System.out.println("SQL + Param :: " + Constants.SELECT_SOCIAL_DATA + " -> " + strPostId + ", " + strParentId + ", " + nSourceId);
			preparedStatement = connection.prepareStatement(Constants.SELECT_RSS_SOCIAL_DATA);
			if (preparedStatement != null) {
				preparedStatement.setString(1, strPostId);
				preparedStatement.setInt(2, nSourceId);
				resultSet = preparedStatement.executeQuery();
				if (resultSet != null) {
					while (resultSet.next()) {
						socialData = new SocialData();
						socialData.setPostId(resultSet.getString("post_id"));
						socialData.setParentId(resultSet.getString("parent_id"));
						socialData.setSourceId(resultSet.getInt("source_id"));
						socialData.setPostDateTime(resultSet.getString("post_datetime"));
						socialData.setPostText(resultSet.getString("post_text"));
						socialData.setHeadline(resultSet.getString("headline"));
						socialData.setIsUpdated(resultSet.getInt("is_updated"));
						socialData.setBreakingNews(resultSet.getString("breaking_news"));
						socialData.setCommentsCount(resultSet.getInt("comment_count"));
						socialData.setLikesCount(resultSet.getInt("likes_count"));
						socialData.setSharesCount(resultSet.getInt("share_count"));
						socialData.setRetweetsCount(resultSet.getInt("retweet_count"));
						socialData.setAuthorName(resultSet.getString("author_name"));
						socialData.setAuthorId(resultSet.getString("author_id"));
					}
				}
			}
	
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (resultSet != null)
				resultSet.close();
			if (preparedStatement != null)
				preparedStatement.close();
			if (connection != null)
				connection.close();
		}
		return socialData;
	}*/

	public static SocialData getKPIFacebookData(String strPostId, int nSourceId) throws SQLException {
		SocialData socialData = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			String companyId = getcompanyId(nSourceId);
			String dataSourceType = DataSource.getDataSourceType();
			if (dataSourceType.equalsIgnoreCase("mysql")) {
				//preparedStatement = connection.prepareStatement(Constants.SELECT_KPI_POST_FB_DATA);
				preparedStatement = connection.prepareStatement(Constants.SELECT_KPI_POST_FB_DATA+Constants.KPI_POST_LEVEL_FB+companyId+Constants.WHERE_KPI_POST_FB_DATA);
			} else {
				preparedStatement = connection.prepareStatement(Constants.SELECT_KPI_POST_FB_DATA_DB2);
			}

			if (preparedStatement != null) {
				preparedStatement.setString(1, strPostId);
				preparedStatement.setInt(2, nSourceId);
				resultSet = preparedStatement.executeQuery();
				if (resultSet != null) {
					while (resultSet.next()) {
						socialData = new SocialData();
						socialData.setPostId(resultSet.getString("post_id"));
						socialData.setSourceId(resultSet.getInt("source_id"));
						socialData.setCommentsCount(resultSet.getInt("comments_count"));
						socialData.setLikesCount(resultSet.getInt("likes_count"));
						socialData.setSharesCount(resultSet.getInt("share_count"));
						socialData.setReplyCount(resultSet.getInt("reply_count"));
						socialData.setNoneCount(resultSet.getInt("none_count"));
						socialData.setLoveCount(resultSet.getInt("love_count"));
						socialData.setHahaCount(resultSet.getInt("haha_count"));
						socialData.setWowCount(resultSet.getInt("wow_count"));
						socialData.setSadCount(resultSet.getInt("sad_count"));
						socialData.setAngryCount(resultSet.getInt("angry_count"));
						socialData.setThankfulCount(resultSet.getInt("thankful_count"));
					}
				}
			}

		} catch (SQLException e) {
			System.err.println("SQL Message in getKPIFacebookData:: " + e.getMessage());
			System.err.println("SQL Error occurred in getKPIFacebookData::" + preparedStatement.toString());
		} catch (Exception e) {
			System.err.println("Error Message in getKPIFacebookData:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (resultSet != null)
				resultSet.close();
			if (preparedStatement != null)
				preparedStatement.close();
			if (connection != null)
				connection.close();
		}
		return socialData;
	}

	public static SocialData getKPITwitterData(String strPostId, int nSourceId) throws SQLException {
		SocialData socialData = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			String companyId = getcompanyId(nSourceId);

			String dataSourceType = DataSource.getDataSourceType();
			if (dataSourceType.equalsIgnoreCase("mysql")) {
				//preparedStatement = connection.prepareStatement(Constants.SELECT_KPI_POST_TWITTER_DATA);
				preparedStatement = connection.prepareStatement(Constants.SELECT_KPI_POST_TWITTER_DATA+Constants.KPI_POST_LEVEL_TW+companyId+Constants.WHERE_KPI_POST_TWITTER_DATA);

			} else {
				preparedStatement = connection.prepareStatement(Constants.SELECT_KPI_POST_TWITTER_DATA_DB2);
			}

			if (preparedStatement != null) {
				preparedStatement.setString(1, strPostId);
				preparedStatement.setInt(2, nSourceId);
				resultSet = preparedStatement.executeQuery();
				if (resultSet != null) {
					while (resultSet.next()) {
						socialData = new SocialData();
						socialData.setPostId(resultSet.getString("post_id"));
						socialData.setSourceId(resultSet.getInt("source_id"));
						socialData.setLikesCount(resultSet.getInt("likes_count"));
						socialData.setSharesCount(resultSet.getInt("retweet_count"));
					}
				}
			}

		} catch (SQLException e) {
			System.err.println("SQL Message in getKPITwitterData:: " + e.getMessage());
			System.err.println("SQL Error occurred in getKPITwitterData::" + preparedStatement.toString());
		} catch (Exception e) {
			System.err.println("Error Message in getKPITwitterData:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (resultSet != null)
				resultSet.close();
			if (preparedStatement != null)
				preparedStatement.close();
			if (connection != null)
				connection.close();
		}
		return socialData;
	}

	private static void getXpressoAnalysis(Properties properties, SocialData socialData, Connection connection) throws SQLException {
		System.out.println("Starting Xpresso Analysis");
		try {
			Util.getXpressoAnalysis(properties, socialData, connection);
			/*
						if (socialData.getPostText().length() > 0 && socialData.getHeadline() != null && socialData.getHeadline() != "") {
							String review = socialData.getHeadline() + "." + socialData.getPostText();
							Util.initiateXpressoCall(properties, socialData);
							System.out.println("Xpresso Output for the corresponding Social Data saved.");
			
						} else if (socialData.getPostText().length() > 0) {
							Util.initiateXpressoCall(properties, socialData);
							System.out.println("Xpresso Output for the corresponding Social Data saved.");
			
						} else {
							System.out.println("Post Text size is <=0 . Hence Not calling Xpresso");
							updateSocialData(2, socialData.getSourceId(), socialData.getPostId());
							System.out.println("Updated is_analyzed for postID :: " + socialData.getPostId());
						}*/
		} catch (Exception e) {

			socialData.setIsAnalyzed(3);
			System.err.println("Error while in initiating Xpresso Call from DBService  :: " + e.getMessage());
			e.printStackTrace();

			//updateSocialData(3, socialData.getSourceId(), socialData.getPostId());
			System.err.println("Updated is_analyzed for postID :: " + socialData.getPostId());
		}
	}

	public static Map<String, List<String>> getPageAndPosts(int nSourceId, String strFromDate, String strToDate) throws SQLException {

		Map<String, List<String>> postMap = new HashMap<String, List<String>>();
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String pageId = null;
		String previousPageId = null;
		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			String companyId = getcompanyId(nSourceId);
			//preparedStatement = connection.prepareStatement(Constants.SELECT_PAGE_POST);
			preparedStatement = connection.prepareStatement(Constants.SELECT_PAGE_POST+Constants.SOCIAL_DATA+companyId+Constants.WHERE_PAGE_POST);

			preparedStatement.setInt(1, nSourceId);
			preparedStatement.setString(2, strFromDate);
			preparedStatement.setString(3, strToDate);

			resultSet = preparedStatement.executeQuery();
			List<String> posts = new ArrayList<String>();

			if (resultSet != null) {
				while (resultSet.next()) {
					pageId = resultSet.getString("page_id");
					//Only for First Run
					if (previousPageId == null) {
						previousPageId = pageId;
					}
					if (previousPageId.equalsIgnoreCase(pageId)) {
						posts.add(resultSet.getString("post_id"));
					} else {
						List<String> pagePosts = posts;
						postMap.put(previousPageId, pagePosts);

						previousPageId = pageId;
						posts.clear();
						posts.add(resultSet.getString("post_id"));
					}
				}
			}
			postMap.put(previousPageId, posts);
		} catch (SQLException e) {
			System.err.println("SQL Message in getPageAndPosts:: " + e.getMessage());
			System.err.println("SQL Error occurred in getPageAndPosts::" + preparedStatement.toString());
		} catch (Exception e) {
			System.err.println("Error Message in getPageAndPosts:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (resultSet != null)
				resultSet.close();
			if (preparedStatement != null)
				preparedStatement.close();
			if (connection != null)
				connection.close();
		}
		return postMap;
	}

	public static Map<String, String> getPageAndPosts(int nSourceId, int postTypePost, int postTypeTag, String strFromDate, String strToDate) throws SQLException {

		Map<String, String> postMap = new HashMap<String, String>();
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String pageId = null;
		String previousPageId = null;
		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			//preparedStatement = connection.prepareStatement(Constants.SELECT_PAGE_POST_FB);
			String companyId = getcompanyId(nSourceId);
			//preparedStatement = connection.prepareStatement(Constants.SELECT_PAGE_POST);
			preparedStatement = connection.prepareStatement(Constants.SELECT_PAGE_POST_FB+Constants.SOCIAL_DATA+companyId+Constants.WHERE_PAGE_POST_FB);

			preparedStatement.setInt(1, nSourceId);
			preparedStatement.setInt(2, postTypePost);
			preparedStatement.setInt(3, postTypeTag);
			preparedStatement.setString(4, strFromDate);
			preparedStatement.setString(5, strToDate);
			//System.err.println("SQL in getPageAndPosts::" + preparedStatement.toString());
			resultSet = preparedStatement.executeQuery();

			if (resultSet != null) {
				while (resultSet.next()) {
					postMap.put(resultSet.getString("post_id"), resultSet.getString("parent_id"));
				}
			}
			System.out.println("POST SIZE in FB COMMENT :: " + postMap.size());
		} catch (SQLException e) {
			System.err.println("SQL Message in getPageAndPosts:: " + e.getMessage());
			System.err.println("SQL Error occurred in getPageAndPosts::" + preparedStatement.toString());
		} catch (Exception e) {
			System.err.println("Error Message in getPageAndPosts:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (resultSet != null)
				resultSet.close();
			if (preparedStatement != null)
				preparedStatement.close();
			if (connection != null)
				connection.close();
		}
		return postMap;
	}

	public static Map<String, String> getPostAndComments(SourceDTO sourceDTO, String strFromDate, String strToDate) throws SQLException {
		Map<String, String> postMap = new HashMap<String, String>();
		int post_type_id = getPostTypeId(sourceDTO.getSourceTypeId(), "Comment");
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		/*String previousParentId = null;
		String currentParentId = null;*/
		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			String companyId = getcompanyId(sourceDTO.getSourceId());
			//preparedStatement = connection.prepareStatement(Constants.SELECT_PAGE_COMMENT);
			preparedStatement = connection.prepareStatement(Constants.SELECT_PAGE_COMMENT+Constants.SOCIAL_DATA+companyId+Constants.WHERE_PAGE_COMMENT);

			preparedStatement.setInt(1, sourceDTO.getSourceId());
			preparedStatement.setInt(2, post_type_id);
			preparedStatement.setString(3, strFromDate);
			preparedStatement.setString(4, strToDate);
			//System.err.println("SQL Error occurred in getPostAndComments::" + preparedStatement.toString());
			resultSet = preparedStatement.executeQuery();
			// List<String> posts = new ArrayList<String>();

			if (resultSet != null) {
				while (resultSet.next()) {
					postMap.put(resultSet.getString("post_id"), resultSet.getString("parent_id"));
				}
			}
			System.out.println("SIZE OF REPLY IN FB::" + postMap.size());
		} catch (SQLException e) {
			System.err.println("SQL Message in getPostAndComments:: " + e.getMessage());
			System.err.println("SQL Error occurred in getPostAndComments::" + preparedStatement.toString());
		} catch (Exception e) {
			System.err.println("Error Message in getPostAndComments:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (resultSet != null)
				resultSet.close();
			if (preparedStatement != null)
				preparedStatement.close();
			if (connection != null)
				connection.close();
		}
		return postMap;
	}

	public static Map<String, Object> getNextToken(int nSourceTypeId) throws SQLException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Map<String, Object> tokenMap = new HashMap<String, Object>();

		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();

			String dataSourceType = DataSource.getDataSourceType();
			if (dataSourceType.equalsIgnoreCase("mysql")) {
				preparedStatement = connection.prepareStatement(Constants.SELECT_APP_SECRET);
			} else {
				preparedStatement = connection.prepareStatement(Constants.SELECT_APP_SECRET_DB2);
			}
			preparedStatement.setInt(1, nSourceTypeId);
			resultSet = preparedStatement.executeQuery();

			if (resultSet != null) {
				while (resultSet.next()) {
					tokenMap.put("token_id", resultSet.getInt("token_id"));
					tokenMap.put("app_key", resultSet.getString("app_key"));
					tokenMap.put("app_secret", resultSet.getString("app_secret"));
				}
			}
		} catch (SQLException e) {
			System.err.println("SQL Message in getNextToken:: " + e.getMessage());
			System.err.println("SQL Error occurred in getNextToken::" + preparedStatement.toString());
		} catch (Exception e) {
			System.err.println("Error Message in getNextToken:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (resultSet != null)
				resultSet.close();
			if (preparedStatement != null)
				preparedStatement.close();
			if (connection != null)
				connection.close();
		}
		return tokenMap;
	}

	public static Map<String, Object> getNextTokenUserProfiling(int nSourceTypeId) throws SQLException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Map<String, Object> tokenMap = new HashMap<String, Object>();

		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			String dataSourceType = DataSource.getDataSourceType();

			if (dataSourceType.equalsIgnoreCase("mysql")) {
				preparedStatement = connection.prepareStatement(Constants.SELECT_APP_SECRET_PROFILING);
			} else {
				preparedStatement = connection.prepareStatement(Constants.SELECT_APP_SECRET_DB2_PROFILING);
			}

			preparedStatement.setInt(1, nSourceTypeId);
			resultSet = preparedStatement.executeQuery();

			if (resultSet != null) {
				while (resultSet.next()) {
					tokenMap.put("token_id", resultSet.getInt("token_id"));
					tokenMap.put("app_key", resultSet.getString("app_key"));
					tokenMap.put("app_secret", resultSet.getString("app_secret"));
				}
			}
		} catch (SQLException e) {
			System.err.println("SQL Message in getNextTokenUserProfiling:: " + e.getMessage());
			System.err.println("SQL Error occurred in getNextTokenUserProfiling::" + preparedStatement.toString());
		} catch (Exception e) {
			System.err.println("Error Message in getNextTokenUserProfiling:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (resultSet != null)
				resultSet.close();
			if (preparedStatement != null)
				preparedStatement.close();
			if (connection != null)
				connection.close();
		}
		return tokenMap;
	}

	public static int getPostTypeId(int nSourceTypeId, String postType) throws SQLException {

		int postTypeId = -1;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			preparedStatement = connection.prepareStatement(Constants.SELECT_POST_TYPE_ID);
			preparedStatement.setInt(1, nSourceTypeId);
			preparedStatement.setString(2, postType);

			resultSet = preparedStatement.executeQuery();
			if (resultSet != null) {
				while (resultSet.next())
					postTypeId = resultSet.getInt("post_type_id");
			}
		} catch (SQLException e) {
			System.err.println("SQL Message in getPostTypeId:: " + e.getMessage());
			System.err.println("SQL Error occurred in getPostTypeId::" + preparedStatement.toString());
		} catch (Exception e) {
			System.err.println("Error Message in getPostTypeId:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (resultSet != null)
				resultSet.close();
			if (preparedStatement != null)
				preparedStatement.close();
			if (connection != null)
				connection.close();
		}
		return postTypeId;
	}

	public static HashMap<String, Integer> getPostTypeMap() throws SQLException {

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		HashMap<String, Integer> postTypeMap = new HashMap<>();

		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			preparedStatement = connection.prepareStatement(Constants.SELECT_POST_TYPE_MAP);
			resultSet = preparedStatement.executeQuery();
			if (resultSet != null) {
				while (resultSet.next())
					postTypeMap.put(resultSet.getString("post_type"), resultSet.getInt("post_type_id"));
			}
		} catch (SQLException e) {
			System.err.println("SQL Message in getPostTypeId:: " + e.getMessage());
			System.err.println("SQL Error occurred in getPostTypeId::" + preparedStatement.toString());
		} catch (Exception e) {
			System.err.println("Error Message in getPostTypeId:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (resultSet != null)
				resultSet.close();
			if (preparedStatement != null)
				preparedStatement.close();
			if (connection != null)
				connection.close();
		}
		return postTypeMap;
	}

	public static String getPageId(int nSourceId) throws SQLException {

		String pageId = null;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			preparedStatement = connection.prepareStatement(Constants.SELECT_PAGE_ID);
			preparedStatement.setInt(1, nSourceId);

			resultSet = preparedStatement.executeQuery();
			if (resultSet != null) {
				while (resultSet.next())
					pageId = resultSet.getString("page_id");
			}
		} catch (SQLException e) {
			System.err.println("SQL Message in getPageId:: " + e.getMessage());
			System.err.println("SQL Error occurred in getPageId::" + preparedStatement.toString());
		} catch (Exception e) {
			System.err.println("Error Message in getPageId:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (resultSet != null)
				resultSet.close();
			if (preparedStatement != null)
				preparedStatement.close();
			if (connection != null)
				connection.close();
		}
		return pageId;
	}

	public static HashMap<Integer, String> getRssTwitterMap(String sourceTypeCode) throws SQLException {

		int sourceId = 0;
		HashMap<Integer, String> rssTwitterMap = new HashMap<>();
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			preparedStatement = connection.prepareStatement(Constants.SELECT_TWITTER_RSS_ID);
			preparedStatement.setString(1, sourceTypeCode);
			resultSet = preparedStatement.executeQuery();
			if (resultSet != null) {
				while (resultSet.next()) {
					rssTwitterMap.put(resultSet.getInt("social_source_id"), resultSet.getString("source_url"));
				}

			}
		} catch (SQLException e) {
			System.err.println("SQL Message in getPageId:: " + e.getMessage());
			System.err.println("SQL Error occurred in getPageId::" + preparedStatement.toString());
		} catch (Exception e) {
			System.err.println("Error Message in getPageId:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (resultSet != null)
				resultSet.close();
			if (preparedStatement != null)
				preparedStatement.close();
			if (connection != null)
				connection.close();
		}
		return rssTwitterMap;
	}

	public static int getInsightId(String strInsightTypeName) throws SQLException {

		int insightId = -1;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			preparedStatement = connection.prepareStatement(Constants.SELECT_INSIGHT_ID);
			preparedStatement.setString(1, strInsightTypeName);

			resultSet = preparedStatement.executeQuery();
			if (resultSet != null) {
				while (resultSet.next())
					insightId = resultSet.getInt("insight_id");
			}
		} catch (SQLException e) {
			System.err.println("SQL Message in getInsightId:: " + e.getMessage());
			System.err.println("SQL Error occurred in getInsightId::" + preparedStatement.toString());
		} catch (Exception e) {
			System.err.println("Error Message in getInsightId:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (resultSet != null)
				resultSet.close();
			if (preparedStatement != null)
				preparedStatement.close();
			if (connection != null)
				connection.close();
		}
		return insightId;

	}

	public static Map<String, String> getFacebookUserToken() throws SQLException {

		Map<String, String> tokenMap = new HashMap<String, String>();
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			String dataSourceType = DataSource.getDataSourceType();
			if (dataSourceType.equalsIgnoreCase("mysql")) {
				preparedStatement = connection.prepareStatement(Constants.SELECT_USER_TOKEN);
			} else {
				preparedStatement = connection.prepareStatement(Constants.SELECT_USER_TOKEN_DB2);
			}
			resultSet = preparedStatement.executeQuery();
			if (resultSet != null) {
				while (resultSet.next()) {
					tokenMap.put("token_id", resultSet.getString("token_id"));
					tokenMap.put("app_key", resultSet.getString("app_key"));
				}
			}
		} catch (SQLException e) {
			System.err.println("SQL Message in getFacebookUserToken:: " + e.getMessage());
			System.err.println("SQL Error occurred in getFacebookUserToken::" + preparedStatement.toString());
		} catch (Exception e) {
			System.err.println("Error Message in getFacebookUserToken:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (resultSet != null)
				resultSet.close();
			if (preparedStatement != null)
				preparedStatement.close();
			if (connection != null)
				connection.close();
		}
		return tokenMap;

	}

	private static int getAspectId(String strAspect) throws SQLException {

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		int nAspectId = 0;

		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			preparedStatement = connection.prepareStatement(Constants.SELECT_ASPECT_ID);

			preparedStatement.setString(1, strAspect);

			resultSet = preparedStatement.executeQuery();

			if (resultSet != null) {
				while (resultSet.next()) {
					nAspectId = resultSet.getInt("aspect_id");
				}
			}

		} catch (SQLException e) {
			System.err.println("SQL Message in getAspectId:: " + e.getMessage());
			System.err.println("SQL Error occurred in getAspectId::" + preparedStatement.toString());
		} catch (Exception e) {
			System.err.println("Error Message in getAspectId:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (resultSet != null)
				resultSet.close();
			if (preparedStatement != null)
				preparedStatement.close();
			if (connection != null)
				connection.close();
		}
		return nAspectId;
	}

	public static HashMap<String, Integer> getAspectMap() throws SQLException {
		HashMap<String, Integer> aspectMap = new HashMap<>();
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			preparedStatement = connection.prepareStatement(Constants.SELECT_ASPECT_MASTER);
			resultSet = preparedStatement.executeQuery();

			if (resultSet != null) {
				while (resultSet.next()) {
					aspectMap.put(resultSet.getString("aspect_name"), resultSet.getInt("aspect_id"));
				}
			}
		} catch (SQLException e) {
			System.err.println("SQL Message in getAspectMap:: " + e.getMessage());
			System.err.println("SQL Error occurred in getAspectMap::" + preparedStatement.toString());
		} catch (Exception e) {
			System.err.println("Error Message in getAspectMap:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (resultSet != null)
				resultSet.close();
			if (preparedStatement != null)
				preparedStatement.close();
			if (connection != null)
				connection.close();
		}
		return aspectMap;
	}

	public static int getSourceTypeId(String strSourceTypeCode) throws SQLException {

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		int nSourceTypeId = 0;

		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			preparedStatement = connection.prepareStatement(Constants.SELECT_SOURCE_TYPE_ID);
			preparedStatement.setString(1, strSourceTypeCode);
			resultSet = preparedStatement.executeQuery();
			if (resultSet != null) {
				while (resultSet.next()) {
					nSourceTypeId = resultSet.getInt("source_type_id");
				}
			}
		} catch (SQLException e) {
			System.err.println("SQL Message in getSourceTypeId:: " + e.getMessage());
			System.err.println("SQL Error occurred in getSourceTypeId::" + preparedStatement.toString());
		} catch (Exception e) {
			System.err.println("Error Message in getSourceTypeId:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (resultSet != null)
				resultSet.close();
			if (preparedStatement != null)
				preparedStatement.close();
			if (connection != null)
				connection.close();
		}
		return nSourceTypeId;
	}

	public static int getShortnameId(String strShortname) throws SQLException {

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		int nShortnameId = 0;

		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			preparedStatement = connection.prepareStatement(Constants.SELECT_SHORTNAME_ID);

			preparedStatement.setString(1, strShortname);

			resultSet = preparedStatement.executeQuery();

			if (resultSet != null) {
				while (resultSet.next()) {
					nShortnameId = resultSet.getInt("shortname_id");
				}
			}

		} catch (SQLException e) {
			System.err.println("SQL Message in getShortnameId:: " + e.getMessage());
			System.err.println("SQL Error occurred in getShortnameId::" + preparedStatement.toString());
		} catch (Exception e) {
			System.err.println("Error Message in getShortnameId:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (resultSet != null)
				resultSet.close();
			if (preparedStatement != null)
				preparedStatement.close();
			if (connection != null)
				connection.close();
		}
		return nShortnameId;
	}

	public static int getShortnameId(int nSourceId) throws SQLException {

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		int nShortnameId = 0;

		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			System.out.println("SQL :: " + Constants.SELECT_SHORTNAME_ID_USING_SOURCE_ID);
			preparedStatement = connection.prepareStatement(Constants.SELECT_SHORTNAME_ID_USING_SOURCE_ID);

			preparedStatement.setInt(1, nSourceId);

			resultSet = preparedStatement.executeQuery();

			if (resultSet != null) {
				while (resultSet.next()) {
					nShortnameId = resultSet.getInt("shortname_id");
				}
			}

		} catch (SQLException e) {
			System.err.println("SQL Message in getShortnameId:: " + e.getMessage());
			System.err.println("SQL Error occurred in getShortnameId::" + preparedStatement.toString());
		} catch (Exception e) {
			System.err.println("Error Message in getShortnameId:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (resultSet != null)
				resultSet.close();
			if (preparedStatement != null)
				preparedStatement.close();
			if (connection != null)
				connection.close();
		}
		return nShortnameId;
	}

	public static String getCommentPostURL(String parentId, int sourceId) throws SQLException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String postURL = "";

		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			String dataSourceType = DataSource.getDataSourceType();
			String companyId = getcompanyId(sourceId);
			if (dataSourceType.equalsIgnoreCase("mysql")) {
				//preparedStatement = connection.prepareStatement(Constants.SELECT_POST_URL);
				preparedStatement = connection.prepareStatement(Constants.SELECT_POST_URL+Constants.SOCIAL_DATA+companyId+Constants.WHERE_POST_URL);
			} else {
				preparedStatement = connection.prepareStatement(Constants.SELECT_POST_URL_DB2);
			}

			preparedStatement.setString(1, parentId);
			preparedStatement.setInt(2, sourceId);

			resultSet = preparedStatement.executeQuery();
			if (resultSet != null) {
				while (resultSet.next()) {
					postURL = resultSet.getString("post_url");
				}
			}
		} catch (SQLException e) {
			System.err.println("SQL Message in getCommentPostURL:: " + e.getMessage());
			System.err.println("SQL Error occurred in getCommentPostURL::" + preparedStatement.toString());
		} catch (Exception e) {
			System.err.println("Error Message in getCommentPostURL:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (resultSet != null)
				resultSet.close();
			if (preparedStatement != null)
				preparedStatement.close();
			if (connection != null)
				connection.close();
		}
		return postURL;
	}

	public static int findSocialData(String strPostId, String strParentId, int nSourceId) throws SQLException {
		int nRecords = -1;
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			String companyId = getcompanyId(nSourceId);
			//preparedStatement = connection.prepareStatement(Constants.SELECT_SOCIAL_DATA_COUNT);
			preparedStatement = connection.prepareStatement(Constants.SELECT_SOCIAL_DATA_COUNT+Constants.SOCIAL_DATA+companyId+Constants.WHERE_SOCIAL_DATA_COUNT);
			if (preparedStatement != null) {
				preparedStatement.setString(1, strPostId);
				preparedStatement.setString(2, strParentId);
				preparedStatement.setInt(3, nSourceId);

				resultSet = preparedStatement.executeQuery();
				if (resultSet != null) {
					while (resultSet.next()) {
						nRecords = resultSet.getInt("record");
					}
				}
			}
		} catch (SQLException e) {
			System.err.println("SQL Message in findSocialData:: " + e.getMessage());
			System.err.println("SQL Error occurred in findSocialData::" + preparedStatement.toString());
		} catch (Exception e) {
			System.err.println("Error Message in findSocialData:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (resultSet != null)
				resultSet.close();
			if (preparedStatement != null)
				preparedStatement.close();
			if (connection != null)
				connection.close();
		}
		return nRecords;
	}

	public static int updateSourceStatus(int nStatus, int nSourceId, boolean isOnlyStatus, String strType) throws SQLException {
		int nFlag = -1;
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		String strSQL = "";
		if (strType != null && strType.equalsIgnoreCase("fb-comment") && nStatus == 1)
			strSQL = Constants.UPDATE_SOURCE_FB_COMMENT_MASTER_LAST_STREAMING_AND_STATUS;
		else if (strType != null && strType.equalsIgnoreCase("fb-reply") && nStatus == 1)
			strSQL = Constants.UPDATE_SOURCE_FB_REPLY_MASTER_LAST_STREAMING_AND_STATUS;
		else if (nStatus == 1)
			strSQL = Constants.UPDATE_SOURCE_MASTER_LAST_STREAMING_AND_STATUS;
		else
			strSQL = Constants.UPDATE_SOURCE_MASTER_STATUS;
		if (isOnlyStatus)
			strSQL = Constants.UPDATE_SOURCE_MASTER_STATUS;

		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			preparedStatement = connection.prepareStatement(strSQL);
			preparedStatement.setInt(1, nStatus);
			preparedStatement.setInt(2, nSourceId);

			nFlag = preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.err.println("SQL Message in updateSourceStatus:: " + e.getMessage());
			System.err.println("SQL Error occurred in updateSourceStatus::" + preparedStatement.toString());
		} catch (Exception e) {
			System.err.println("Error Message in updateSourceStatus:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (preparedStatement != null)
				preparedStatement.close();
			if (connection != null)
				connection.close();
		}
		return nFlag;
	}

	public static int updatePageInfo(PageInfo pageInfo) throws SQLException {
		int nFlag = -1;
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			String dataSourceType = DataSource.getDataSourceType();
			if (dataSourceType.equalsIgnoreCase("mysql")) {
				preparedStatement = connection.prepareStatement(Constants.UPDATE_PAGE_INFO);
				preparedStatement.setInt(1, pageInfo.getSourceId());
				preparedStatement.setLong(2, pageInfo.getPageLike());
				preparedStatement.setLong(3, pageInfo.getPeopleTalkingAbout());
				preparedStatement.setLong(4, pageInfo.getTotalFollowings());
				preparedStatement.setLong(5, pageInfo.getTotalFollowers());
				preparedStatement.setString(6, pageInfo.getPageURL());
				preparedStatement.setLong(7, pageInfo.getWereHere());
				preparedStatement.setString(8, pageInfo.getPageId());
			} else {
				preparedStatement = connection.prepareStatement(Constants.UPDATE_PAGE_INFO_DB2);
				preparedStatement.setString(1, pageInfo.getPageId());
				preparedStatement.setInt(2, pageInfo.getSourceId());
				preparedStatement.setLong(3, pageInfo.getPageLike());
				preparedStatement.setLong(4, pageInfo.getPeopleTalkingAbout());
				preparedStatement.setLong(5, pageInfo.getWereHere());
				preparedStatement.setLong(6, pageInfo.getTotalFollowings());
				preparedStatement.setLong(7, pageInfo.getTotalFollowers());
				preparedStatement.setString(8, pageInfo.getPageURL());
				preparedStatement.setString(9, pageInfo.getCity());
				preparedStatement.setString(10, pageInfo.getCountry());
				preparedStatement.setString(11, Double.toString(pageInfo.getLatitude()));
				preparedStatement.setString(12, Double.toString(pageInfo.getLongitude()));

			}

			nFlag = preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.err.println("SQL Message in updatePageInfo:: " + e.getMessage());
			System.err.println("SQL Error occurred in updatePageInfo::" + preparedStatement.toString());
		} catch (Exception e) {
			System.err.println("Error Message in updatePageInfo:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (preparedStatement != null)
				preparedStatement.close();
			if (connection != null)
				connection.close();
		}
		return nFlag;
	}

	private static int updateSocialData(SocialData socialData) throws SQLException {
		int nFlag = -1;
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			String companyId = getcompanyId(socialData.getSourceId());
			//preparedStatement = connection.prepareStatement(Constants.UPDATE_SOCIAL_DATA);
			//preparedStatement = connection.prepareStatement(Constants.UPDATE_SOCIAL_DATA);
			preparedStatement = connection.prepareStatement(Constants.UPDATE_SOCIAL_DATA+Constants.SOCIAL_DATA+companyId+Constants.SET_SOCIAL_DATA);
			//System.out.println("SQL :: "+ Constants.UPDATE_SOCIAL_DATA + "->" +  );
			preparedStatement.setString(1, socialData.getBreakingNews());
			preparedStatement.setInt(2, socialData.getCommentsCount());
			preparedStatement.setInt(3, socialData.getLikesCount());
			preparedStatement.setInt(4, socialData.getSharesCount());
			preparedStatement.setInt(5, socialData.getRetweetsCount());
			/*preparedStatement.setString(6, socialData.getType());
			preparedStatement.setString(7, socialData.getPostId());
			preparedStatement.setString(8, socialData.getParentId());
			preparedStatement.setInt(9, socialData.getSourceId());*/

			preparedStatement.setString(6, socialData.getPostId());
			preparedStatement.setString(7, socialData.getParentId());
			preparedStatement.setInt(8, socialData.getSourceId());

			nFlag = preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.err.println("SQL Message in updateSocialData:: " + e.getMessage());
			System.err.println("SQL Error occurred in updateSocialData::" + preparedStatement.toString());
		} catch (Exception e) {
			System.err.println("Error Message in updateSocialData:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (preparedStatement != null)
				preparedStatement.close();
			if (connection != null)
				connection.close();
		}
		return nFlag;
	}

	private static int updateSocialData(SocialData socialData, int flag) throws SQLException {
		int nFlag = -1;
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			String companyId = getcompanyId(socialData.getSourceId());
			//preparedStatement = connection.prepareStatement(Constants.UPDATE_SOCIAL_DATA_WITH_FLAG);
			preparedStatement = connection.prepareStatement(Constants.UPDATE_SOCIAL_DATA_WITH_FLAG+Constants.SOCIAL_DATA+companyId+Constants.SET_SOCIAL_DATA_WITH_FLAG);
			preparedStatement.setString(1, socialData.getParentId());
			preparedStatement.setString(2, socialData.getPostText());
			preparedStatement.setString(3, socialData.getHeadline());
			preparedStatement.setInt(4, flag);
			preparedStatement.setString(5, socialData.getPostId());
			preparedStatement.setInt(6, socialData.getSourceId());

			nFlag = preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.err.println("SQL Message in updateSocialData:: " + e.getMessage());
			System.err.println("SQL Error occurred in updateSocialData::" + preparedStatement.toString());
		} catch (Exception e) {
			System.err.println("Error Message in updateSocialData:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (preparedStatement != null)
				preparedStatement.close();
			if (connection != null)
				connection.close();
		}
		return nFlag;
	}

	public static int updateTokenMaster(int nStatus, int nTokenId, int nSourceTypeId) throws SQLException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		int nFlag = 0;
		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			if (nTokenId != -1) {
				preparedStatement = connection.prepareStatement(Constants.UPDATE_APP_SECRET_SINGLE);
				preparedStatement.setInt(1, nStatus);
				preparedStatement.setInt(2, nTokenId);
				preparedStatement.setInt(3, nSourceTypeId);
			} else {
				preparedStatement = connection.prepareStatement(Constants.UPDATE_APP_SECRET_ALL);
				preparedStatement.setInt(1, nStatus);
				preparedStatement.setInt(2, nSourceTypeId);
			}
			nFlag = preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.err.println("SQL Message in updateTokenMaster:: " + e.getMessage());
			System.err.println("SQL Error occurred in updateTokenMaster::" + preparedStatement.toString());
		} catch (Exception e) {
			System.err.println("Error Message in updateTokenMaster:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (preparedStatement != null)
				preparedStatement.close();
			if (connection != null)
				connection.close();
		}
		return nFlag;
	}

	public static int updateTokenMasterProfiling(int nStatus, int nTokenId, int nSourceTypeId) throws SQLException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		int nFlag = 0;
		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();

			if (nTokenId != -1) {
				preparedStatement = connection.prepareStatement(Constants.UPDATE_APP_SECRET_SINGLE_PROFILING);
				preparedStatement.setInt(1, nStatus);
				preparedStatement.setInt(2, nTokenId);
				preparedStatement.setInt(3, nSourceTypeId);
			} else {
				preparedStatement = connection.prepareStatement(Constants.UPDATE_APP_SECRET_ALL_PROFILING);
				preparedStatement.setInt(1, nStatus);
				preparedStatement.setInt(2, nSourceTypeId);
			}
			nFlag = preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.err.println("SQL Message in updateTokenMasterProfiling:: " + e.getMessage());
			System.err.println("SQL Error occurred in updateTokenMasterProfiling::" + preparedStatement.toString());
		} catch (Exception e) {
			System.err.println("Error Message in updateTokenMasterProfiling:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (preparedStatement != null)
				preparedStatement.close();
			if (connection != null)
				connection.close();
		}
		return nFlag;
	}

	public static int updateUserTokenMaster(int nStatus, String accessToken) throws SQLException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		int nFlag = 0;
		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();

			if (nStatus != 0) {
				preparedStatement = connection.prepareStatement(Constants.UPDATE_USER_TOKEN_SINGLE);
				preparedStatement.setInt(1, nStatus);
				preparedStatement.setString(2, accessToken);
			} else {
				preparedStatement = connection.prepareStatement(Constants.UPDATE_USER_TOKEN_ALL);
				preparedStatement.setInt(1, nStatus);
			}
			nFlag = preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.err.println("SQL Message in updateUserTokenMaster:: " + e.getMessage());
			System.err.println("SQL Error occurred in updateUserTokenMaster::" + preparedStatement.toString());
		} catch (Exception e) {
			System.err.println("Error Message in updateUserTokenMaster:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (preparedStatement != null)
				preparedStatement.close();
			if (connection != null)
				connection.close();
		}
		return nFlag;
	}

	public static int updateSocialData(SocialData socialData, XpressoValues xpressoValues) throws SQLException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		int flag = -1;
		int aspectId = -1;
		HashMap<String, Integer> aspectMap = getAspectMap();
		aspectId = aspectMap.get(xpressoValues.getOverallAspect());
		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			System.out.println(xpressoValues.getOverallSentiment() + " = " + xpressoValues.getOverallSentimentScore());
			String companyId = getcompanyId(socialData.getSourceId());
			//preparedStatement = connection.prepareStatement(Constants.UPDATE_SOCIAL_DATA_WITH_FLAG);
			//preparedStatement = connection.prepareStatement(Constants.UPDATE_SOCIAL_DATA_WITH_XPRESSO_OUTPUT);
			preparedStatement = connection.prepareStatement(Constants.UPDATE_SOCIAL_DATA_WITH_XPRESSO_OUTPUT+Constants.SOCIAL_DATA+companyId+Constants.SET_SOCIAL_DATA_WITH_XPRESSO_OUTPUT);
			/*try {
				aspectId = getAspectId(xpressoValues.getOverallAspect());
			} catch (Exception e) {
				System.out.println("Error in Aspect ID ::  " + e.getMessage());
				e.printStackTrace();
			}*/
			preparedStatement.setString(1, xpressoValues.getOverallSentiment());
			preparedStatement.setDouble(2, xpressoValues.getOverallSentimentScore());
			preparedStatement.setInt(3, aspectId);
			preparedStatement.setInt(4, xpressoValues.getOverallSentimentNumber());
			preparedStatement.setString(5, socialData.getPostId());
			preparedStatement.setInt(6, socialData.getSourceId());

			flag = preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.err.println("SQL Message in updateSocialData:: " + e.getMessage());
			System.err.println("SQL Error occurred in updateSocialData::" + preparedStatement.toString());
		} catch (Exception e) {
			System.err.println("Error Message in updateSocialData:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
			if (connection != null)
				connection.close();
		}
		return flag;
	}

	public static int updateSocialData(int nStatus, int nSourceId, String strPostId) throws SQLException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		int flag = -1;

		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			String companyId = getcompanyId(nSourceId);
			//preparedStatement = connection.prepareStatement(Constants.UPDATE_SOCIAL_DATA_WITH_IS_ANALYZED);
			preparedStatement = connection.prepareStatement(Constants.UPDATE_SOCIAL_DATA_WITH_IS_ANALYZED+Constants.SOCIAL_DATA+companyId+Constants.SET_SOCIAL_DATA_WITH_IS_ANALYZED);

			preparedStatement.setInt(1, nStatus);
			preparedStatement.setString(2, strPostId);
			preparedStatement.setInt(3, nSourceId);

			flag = preparedStatement.executeUpdate();
		} catch (SQLException e) {
			System.err.println("SQL Message in updateSocialData:: " + e.getMessage());
			System.err.println("SQL Error occurred in updateSocialData::" + preparedStatement.toString());
		} catch (Exception e) {
			System.err.println("Error Message in updateSocialData:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
			if (connection != null)
				connection.close();
		}
		return flag;
	}

	/**
	 * 
	 * Saving page info into database
	 * 
	 * @throws SQLException
	 * 
	 */
	public static void savePageInfo(PageInfo pageInfo) throws SQLException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			preparedStatement = connection.prepareStatement(Constants.INSERT_PAGE_INFO);

			preparedStatement.setString(1, pageInfo.getPageId());
			preparedStatement.setInt(2, pageInfo.getSourceId());
			preparedStatement.setLong(3, pageInfo.getPageLike());
			preparedStatement.setLong(4, pageInfo.getPeopleTalkingAbout());
			preparedStatement.setLong(5, pageInfo.getPeopleWereHere());
			preparedStatement.setLong(6, pageInfo.getTotalFollowings());
			preparedStatement.setLong(7, pageInfo.getTotalFollowers());
			preparedStatement.setString(8, pageInfo.getPageURL());
			preparedStatement.setString(9, pageInfo.getCity());
			preparedStatement.setString(10, pageInfo.getCountry());
			preparedStatement.setString(11, Double.toString(pageInfo.getLatitude()));
			preparedStatement.setString(12, Double.toString(pageInfo.getLongitude()));

			preparedStatement.execute();
		} catch (SQLException e) {
			System.err.println("SQL Message in savePageInfo:: " + e.getMessage());
			System.err.println("SQL Error occurred in savePageInfo::" + preparedStatement.toString());
		} catch (Exception e) {
			System.err.println("Error Message in savePageInfo:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
			if (connection != null)
				connection.close();
		}
	}

	/**
	 * 
	 * Saving post details into database
	 * 
	 * @throws Exception
	 * 
	 */
	public static void saveSocialData(Set<SocialData> socialDatas, Properties properties) throws Exception {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		int isUpdatedFlag = 0;
		String reviewText = "";
		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			//preparedStatement = connection.prepareStatement(Constants.INSERT_SOCIAL_DATA);
			
			
			

			
			
			for (SocialData socialData : socialDatas) {
				try {
					if (socialData.getPostDateTime() == null) {
						continue;
					}
					SocialData findSocialData = getSocialData(socialData.getPostId(), socialData.getParentId(), socialData.getSourceId(), false);
					if (findSocialData == null) {
						isUpdatedFlag = 0;
						String companyId = getcompanyId(socialData.getSourceId());

						if (socialData.getXpressoEnabled() == 0 && socialData.getXpressoReview().length() > 0) {
							socialData.setIsAnalyzed(0);
						} else if (socialData.getXpressoEnabled() == 1 && socialData.getXpressoReview().length() > 0) {
							getXpressoAnalysis(properties, socialData, connection);
						} else if (socialData.getXpressoEnabled() == 1 && socialData.getXpressoReview().length() <= 0) {
							socialData.setIsAnalyzed(2);
						} else if (socialData.getXpressoEnabled() == 0 && socialData.getXpressoReview().length() <= 0) {
							// MCI Case where it is 1
							socialData.setIsAnalyzed(1);
						}
						preparedStatement = connection.prepareStatement(Constants.INSERT_SOCIAL_DATA+Constants.SOCIAL_DATA+companyId+Constants.INSERT_SOCIAL_DATA1);
						
						preparedStatement.setString(1, socialData.getPostId());
						preparedStatement.setString(2, socialData.getParentId());
						preparedStatement.setInt(3, socialData.getSourceId());
						preparedStatement.setInt(4, socialData.getPostTypeId());
						preparedStatement.setString(5, socialData.getPostDateTime());
						preparedStatement.setInt(6, '0');
						preparedStatement.setInt(7, '0');
						preparedStatement.setString(8, socialData.getPostText());
						preparedStatement.setString(9, socialData.getHeadline());
						preparedStatement.setString(10, socialData.getBreakingNews());
						preparedStatement.setInt(11, socialData.getCommentsCount());
						preparedStatement.setInt(12, socialData.getLikesCount());
						preparedStatement.setInt(13, socialData.getSharesCount());
						preparedStatement.setInt(14, socialData.getRetweetsCount());
						preparedStatement.setString(15, socialData.getAuthorId());
						preparedStatement.setString(16, socialData.getAuthorName());
						preparedStatement.setString(17, socialData.getPostURL());
						preparedStatement.setString(18, socialData.getUserURL());
						preparedStatement.setInt(19, isUpdatedFlag);
						preparedStatement.setString(20, socialData.getType());
						preparedStatement.setInt(21, socialData.getSourceTypeId());
						preparedStatement.setInt(22, socialData.getShortnameId());
						preparedStatement.setInt(23, socialData.getAuthorOutreach());

						preparedStatement.executeUpdate();
						/*if (socialData.getHeadline() != null)
							reviewText = socialData.getHeadline() + ".";
						
						reviewText += socialData.getPostText();
						if (socialData.isText() == true) {
							//Util.initiateXpressoCall(properties, socialData.getSourceId(), socialData.getPostId(), socialData.getParentId(), reviewText);
							System.out.println("Xpresso Output for the corresponding Social Data saved.");
						}*/
					}
				} catch (SQLException e) {
					System.out.println("Got insertion error :: " + e.getMessage());
					System.err.println("SQL Error occurred in saveSocialData::" + preparedStatement.toString());
					continue;
				} catch (Exception e) {
					System.out.println("Got error :: " + e.getMessage());
					e.printStackTrace();
					continue;
				}
			}
		} catch (

		SQLException e) {
			System.err.println("SQL Message in saveSocialData:: " + e.getMessage());
		} catch (Exception e) {
			System.err.println("Error Message in saveSocialData:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
			if (connection != null) {
				connection.close();
			}
		}
	}

	public static void saveForumData(Set<SocialData> socialDatas, Properties properties) throws Exception {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		int isUpdatedFlag = 0;
		String reviewText = "";
		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			//preparedStatement = connection.prepareStatement(Constants.INSERT_FORUM_SOCIAL_DATA);
			for (SocialData socialData : socialDatas) {
				try {
					/*if (socialData.getPostDateTime() == null) {
						continue;
					}*/
					SocialData findSocialData = getSocialData(socialData.getPostId(), socialData.getParentId(), socialData.getSourceId(), false);
					if (findSocialData == null) {
						isUpdatedFlag = 0;

						String companyId = getcompanyId(socialData.getSourceId());
						if (socialData.getXpressoEnabled() == 0 && socialData.getXpressoReview().length() > 0) {
							socialData.setIsAnalyzed(0);
						} else if (socialData.getXpressoEnabled() == 1 && socialData.getXpressoReview().length() > 0) {
							getXpressoAnalysis(properties, socialData, connection);
						} else if (socialData.getXpressoEnabled() == 1 && socialData.getXpressoReview().length() <= 0) {
							socialData.setIsAnalyzed(2);
						} else if (socialData.getXpressoEnabled() == 0 && socialData.getXpressoReview().length() <= 0) {
							// MCI Case where it is 1
							socialData.setIsAnalyzed(1);
						}
						preparedStatement = connection.prepareStatement(Constants.INSERT_FORUM_SOCIAL_DATA+Constants.SOCIAL_DATA+companyId+Constants.INSERT_FORUM_SOCIAL_DATA1);
						preparedStatement.setString(1, socialData.getPostId());
						preparedStatement.setString(2, socialData.getParentId());
						preparedStatement.setInt(3, socialData.getSourceId());
						preparedStatement.setInt(4, socialData.getPostTypeId());
						preparedStatement.setString(5, socialData.getPostDateTime());
						preparedStatement.setInt(6, '0');
						preparedStatement.setInt(7, '0');
						preparedStatement.setString(8, socialData.getPostText());
						preparedStatement.setString(9, socialData.getHeadline());
						preparedStatement.setInt(10, socialData.getCommentsCount()); //Replies
						preparedStatement.setInt(11, socialData.getLikesCount()); //Likes
						preparedStatement.setInt(12, socialData.getSharesCount()); //Views
						preparedStatement.setString(13, socialData.getAuthorId());
						preparedStatement.setString(14, socialData.getAuthorName());
						preparedStatement.setString(15, socialData.getPostURL());
						preparedStatement.setInt(16, isUpdatedFlag);
						preparedStatement.setString(17, socialData.getType());
						preparedStatement.setInt(18, socialData.getSourceTypeId());
						preparedStatement.setInt(19, socialData.getShortnameId());

						preparedStatement.executeUpdate();

					}
				} catch (SQLException e) {
					System.out.println("Got insertion  error in saveForumData:: " + e.getMessage());
					System.err.println("SQL Error occurred in saveForumData :: " + preparedStatement.toString());
					System.err.println("*************************************************************");
					System.err.println(socialData.toString());
					continue;
				} catch (Exception e) {
					System.out.println("Got error :: " + e.getMessage());
					e.printStackTrace();
					continue;
				}
			}
		} catch (SQLException e) {
			System.err.println("SQL Message in saveForumData:: " + e.getMessage());
		} catch (Exception e) {
			System.err.println("Error Message in saveForumData:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
			if (connection != null) {
				connection.close();
			}
		}
	}

	public static void saveFBSocialData(Set<SocialData> socialDatas, Properties properties) throws Exception {

		PreparedStatement preparedStatement = null;
		Connection connection = null;
		int isUpdatedFlag = 0;

		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			//preparedStatement = connection.prepareStatement(Constants.INSERT_FB_SOCIAL_DATA);
			for (SocialData socialData : socialDatas) {
				try {
					/*if (socialData.getPostDateTime() == null) {
						System.err.println("Incomplete Social Data bean :: " + socialData.toString());
						continue;
					}*/
					SocialData findSocialData = getSocialData(socialData.getPostId(), socialData.getParentId(), socialData.getSourceId(), false);
					if (findSocialData == null) {
						isUpdatedFlag = 0;
						String companyId = getcompanyId(socialData.getSourceId());
						if (socialData.getXpressoEnabled() == 0 && socialData.getXpressoReview().length() > 0) {
							socialData.setIsAnalyzed(0);
						} else if (socialData.getXpressoEnabled() == 1 && socialData.getXpressoReview().length() > 0) {
							getXpressoAnalysis(properties, socialData, connection);
						} else if (socialData.getXpressoEnabled() == 1 && socialData.getXpressoReview().length() <= 0) {
							socialData.setIsAnalyzed(2);
						} else if (socialData.getXpressoEnabled() == 0 && socialData.getXpressoReview().length() <= 0) {
							// MCI Case where it is 1
							socialData.setIsAnalyzed(1);
						}
						preparedStatement = connection.prepareStatement(Constants.INSERT_FB_SOCIAL_DATA+Constants.SOCIAL_DATA+companyId+Constants.INSERT_FB_SOCIAL_DATA1);
						
						preparedStatement.setString(1, socialData.getPostId());
						preparedStatement.setString(2, socialData.getParentId());
						preparedStatement.setInt(3, socialData.getSourceId());
						preparedStatement.setInt(4, socialData.getPostTypeId());
						preparedStatement.setString(5, socialData.getPostDateTime());
						preparedStatement.setInt(6, '0');
						preparedStatement.setInt(7, '0');
						preparedStatement.setString(8, socialData.getPostText());
						preparedStatement.setInt(9, socialData.getCommentsCount());
						preparedStatement.setInt(10, socialData.getLikesCount());
						preparedStatement.setInt(11, socialData.getSharesCount());
						preparedStatement.setString(12, socialData.getAuthorId());
						preparedStatement.setString(13, socialData.getAuthorName());
						preparedStatement.setString(14, socialData.getPostURL());
						preparedStatement.setString(15, socialData.getUserURL());
						preparedStatement.setInt(16, isUpdatedFlag);
						preparedStatement.setString(17, socialData.getType());
						preparedStatement.setInt(18, socialData.getSourceTypeId());
						preparedStatement.setInt(19, socialData.getShortnameId());
						preparedStatement.setInt(20, socialData.getAuthorOutreach());
						preparedStatement.setString(21, socialData.getLink());

						preparedStatement.setString(22, socialData.getOverallSentiment());
						preparedStatement.setDouble(23, socialData.getOverallSentimentScore());
						preparedStatement.setInt(24, socialData.getOverallAspectId());
						preparedStatement.setInt(25, socialData.getOverallSentimentNumber());
						preparedStatement.setInt(26, socialData.getIsAnalyzed());

						preparedStatement.executeUpdate();

					}
				} catch (SQLException e) {
					System.err.println("Got insertion error :: " + e.getMessage());
					System.err.println("SQL Error occurred in saveFBSocialData::" + preparedStatement.toString());
					continue;
				} catch (Exception e) {
					System.err.println("Got error :: " + e.getMessage());
					e.printStackTrace();
					continue;
				}
			}
		} catch (SQLException e) {
			System.err.println("SQL Message in saveFBSocialData:: " + e.getMessage());
		} catch (Exception e) {
			System.err.println("Error Message in saveFBSocialData:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
			if (connection != null) {
				connection.close();
			}
		}
	}

	public static void saveTwitterSocialData(Set<SocialData> socialDatas, Properties properties) throws Exception {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		int isUpdatedFlag = 0;

		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			//preparedStatement = connection.prepareStatement(Constants.INSERT_TWITTER_SOCIAL_DATA);
			for (SocialData socialData : socialDatas) {
				try {
					SocialData findSocialData = getSocialData(socialData.getPostId(), socialData.getParentId(), socialData.getSourceId(), false);
					/*if (socialData.getPostDateTime() == null) {
						continue;
					}*/
					if (findSocialData == null) {
						String companyId = getcompanyId(socialData.getSourceId());

						if (socialData.getXpressoEnabled() == 0 && socialData.getXpressoReview().length() > 0) {
							socialData.setIsAnalyzed(0);
						} else if (socialData.getXpressoEnabled() == 1 && socialData.getXpressoReview().length() > 0) {
							getXpressoAnalysis(properties, socialData, connection);
						} else if (socialData.getXpressoEnabled() == 1 && socialData.getXpressoReview().length() <= 0) {
							socialData.setIsAnalyzed(2);
						} else if (socialData.getXpressoEnabled() == 0 && socialData.getXpressoReview().length() <= 0) {
							// MCI Case where it is 1
							socialData.setIsAnalyzed(1);
						}
						preparedStatement = connection.prepareStatement(Constants.INSERT_TWITTER_SOCIAL_DATA+Constants.SOCIAL_DATA+companyId+Constants.INSERT_TWITTER_SOCIAL_DATA1);
						isUpdatedFlag = 0;
						preparedStatement.setString(1, socialData.getPostId());
						preparedStatement.setString(2, socialData.getParentId());
						preparedStatement.setInt(3, socialData.getSourceId());
						preparedStatement.setInt(4, socialData.getPostTypeId());
						preparedStatement.setString(5, socialData.getPostDateTime());
						preparedStatement.setInt(6, '0');
						preparedStatement.setInt(7, '0');
						preparedStatement.setString(8, socialData.getPostText());
						preparedStatement.setInt(9, socialData.getLikesCount());
						preparedStatement.setInt(10, socialData.getRetweetsCount());
						preparedStatement.setString(11, socialData.getAuthorId());
						preparedStatement.setString(12, socialData.getAuthorName());
						preparedStatement.setString(13, socialData.getPostURL());
						preparedStatement.setString(14, socialData.getUserURL());
						preparedStatement.setInt(15, isUpdatedFlag);
						preparedStatement.setString(16, socialData.getType());
						preparedStatement.setInt(17, socialData.getSourceTypeId());
						preparedStatement.setInt(18, socialData.getShortnameId());
						preparedStatement.setInt(19, socialData.getAuthorOutreach());
						preparedStatement.setInt(20, socialData.getIsMention());
						preparedStatement.setString(21, socialData.getLink());

						preparedStatement.setString(22, socialData.getOverallSentiment());
						preparedStatement.setDouble(23, socialData.getOverallSentimentScore());
						preparedStatement.setInt(24, socialData.getOverallAspectId());
						preparedStatement.setInt(25, socialData.getOverallSentimentNumber());
						preparedStatement.setInt(26, socialData.getIsAnalyzed());

						preparedStatement.executeUpdate();
						System.out.println("Inserted Social Data");

					}
				} catch (SQLException e) {
					System.out.println("Got insertion error :: " + e.getMessage());
					System.err.println("SQL Error occurred in saveTwitterSocialData::" + preparedStatement.toString());
					continue;
				} catch (Exception e) {
					System.out.println("Got error :: " + e.getMessage());
					e.printStackTrace();
					continue;
				}
			}
		} catch (SQLException e) {
			System.err.println("SQL Message in saveTwitterSocialData:: " + e.getMessage());
		} catch (Exception e) {
			System.err.println("Error Message in saveTwitterSocialData:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
			if (connection != null) {
				connection.close();
			}
		}
		System.out.println("Post info saved successfully into social_data table");
	}

	public static void saveRSSSocialData(Set<SocialData> socialDatas, Properties properties) throws Exception {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		int isUpdatedFlag = 0;

		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			preparedStatement = connection.prepareStatement(Constants.INSERT_RSS_SOCIAL_DATA);
			for (SocialData socialData : socialDatas) {
				try {
					/*if (socialData.getPostDateTime() == null) {
						continue;
					}*/
					SocialData findSocialData = getSocialData(socialData.getPostId(), socialData.getParentId(), socialData.getSourceId(), false);
					if (findSocialData == null) {

						if (socialData.getXpressoEnabled() == 0 && socialData.getXpressoReview().length() > 0) {
							socialData.setIsAnalyzed(0);
						} else if (socialData.getXpressoEnabled() == 1 && socialData.getXpressoReview().length() > 0) {
							getXpressoAnalysis(properties, socialData, connection);
						} else if (socialData.getXpressoEnabled() == 1 && socialData.getXpressoReview().length() <= 0) {
							socialData.setIsAnalyzed(2);
						} else if (socialData.getXpressoEnabled() == 0 && socialData.getXpressoReview().length() <= 0) {
							// MCI Case where it is 1
							socialData.setIsAnalyzed(1);
						}
						isUpdatedFlag = 0;
						preparedStatement.setString(1, socialData.getPostId());
						preparedStatement.setString(2, socialData.getParentId());
						preparedStatement.setInt(3, socialData.getSourceId());
						preparedStatement.setInt(4, socialData.getPostTypeId());
						preparedStatement.setString(5, socialData.getPostDateTime());
						preparedStatement.setInt(6, '0');
						preparedStatement.setInt(7, '0');
						preparedStatement.setString(8, socialData.getPostText());
						preparedStatement.setString(9, socialData.getHeadline());
						preparedStatement.setString(10, socialData.getBreakingNews());
						preparedStatement.setString(11, socialData.getAuthorId());
						preparedStatement.setString(12, socialData.getAuthorName());
						preparedStatement.setString(13, socialData.getPostURL());
						preparedStatement.setString(14, socialData.getUserURL());
						preparedStatement.setInt(15, isUpdatedFlag);
						preparedStatement.setString(16, socialData.getType());
						preparedStatement.setInt(17, socialData.getSourceTypeId());
						preparedStatement.setInt(18, socialData.getShortnameId());

						preparedStatement.setString(19, socialData.getOverallSentiment());
						preparedStatement.setDouble(20, socialData.getOverallSentimentScore());
						preparedStatement.setInt(21, socialData.getOverallAspectId());
						preparedStatement.setInt(22, socialData.getOverallSentimentNumber());
						preparedStatement.setInt(23, socialData.getIsAnalyzed());

						preparedStatement.executeUpdate();

						//getXpressoAnalysis(properties, socialData, connection);

					} else if (socialData.getPostText() != null || socialData.getHeadline() != null) {
						if (socialData.getPostText().equals(findSocialData.getPostText()) && socialData.getHeadline().equals(findSocialData.getHeadline())) {
							System.out.println("RSS News Same");
						} else {
							isUpdatedFlag = findSocialData.getIsUpdated() + 1;
							System.out.println("isUpdated Value :: " + findSocialData.getIsUpdated());
							System.out.println("isUpdated Value after addition:: " + isUpdatedFlag);
							System.out.println("OLD RSS HASH:: " + Util.ConvertTomd5(findSocialData.getPostText() + findSocialData.getHeadline()));
							System.out.println("Post _id OLD :: " + findSocialData.getPostId());
							System.out.println("OLD RSS HASH:: " + Util.ConvertTomd5(socialData.getPostText() + socialData.getHeadline()));
							System.out.println("Post _id NEW :: " + socialData.getPostId());
							System.out.println("Social Data being updated with updated post or headline");

							Util.updateRSSSocialDataValues(socialData, findSocialData);
							System.out.println("Either post text or headline is NULL");

							DBService.saveRSSArchive(findSocialData);

							if (socialData.getXpressoEnabled() == 0) {
								socialData.setIsAnalyzed(0);
							} else if (socialData.getXpressoEnabled() == 1 && socialData.getXpressoReview().length() > 0) {
								getXpressoAnalysis(properties, socialData, connection);
							} else if (socialData.getXpressoEnabled() == 1 && socialData.getXpressoReview().length() <= 0) {
								socialData.setIsAnalyzed(2);
							} else if (socialData.getXpressoEnabled() == 0 && socialData.getXpressoReview().length() <= 0) {
								// MCI Case where it is 1
								socialData.setIsAnalyzed(1);
							}
							// TODO update Xpresso Output at once
							DBService.updateSocialData(socialData, isUpdatedFlag);
						}
					} else {
						System.out.println("Both Post Text & Headline for the News Article in the current crawl is NULL");
					}
				} catch (SQLException e) {
					System.err.println("Got insertion error :: " + e.getMessage());
					e.printStackTrace();
					continue;
				} catch (Exception e) {
					System.err.println("Got error :: " + e.getMessage());
					e.printStackTrace();
					continue;
				}
			}
		} catch (SQLException e) {
			System.err.println("SQL Message in saveRSSSocialData:: " + e.getMessage());
		} catch (Exception e) {
			System.err.println("Error Message in saveRSSSocialData:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
			if (connection != null) {
				connection.close();
			}
		}
		System.out.println("Post info saved successfully into social_data table");
	}

	private static int saveRSSArchive(SocialData socialData) throws SQLException {
		int nFlag = -1;
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			preparedStatement = connection.prepareStatement(Constants.INSERT_RSS_ARCHIVE);
			preparedStatement.setString(1, socialData.getPostId());
			preparedStatement.setInt(2, socialData.getSourceId());
			preparedStatement.setString(3, socialData.getPostDateTime());
			preparedStatement.setInt(4, '0');
			preparedStatement.setInt(5, '0');
			preparedStatement.setString(6, socialData.getPostText());
			preparedStatement.setString(7, socialData.getHeadline());
			preparedStatement.setString(8, socialData.getAuthorId());
			preparedStatement.setString(9, socialData.getAuthorName());

			nFlag = preparedStatement.executeUpdate();

		} catch (SQLException e) {
			System.err.println("SQL Message in saveRSSArchive:: " + e.getMessage());
			System.err.println("SQL Error occurred in saveRSSArchive::" + preparedStatement.toString());
		} catch (Exception e) {
			System.err.println("Error Message in saveRSSArchive:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (preparedStatement != null)
				preparedStatement.close();
			if (connection != null) {
				connection.close();
			}
		}
		return nFlag;
	}

	public static void saveXpressoData(Connection connection, SocialData socialData, XpressoValues xpressoValues) throws SQLException {

		List<XpressoAbout> abouts = xpressoValues.getAbouts();
		//Connection connection = null;
		PreparedStatement preparedStatement = null;
		HashMap<String, Integer> aspectMap = socialData.getAspectMap();
		int aspectId = -1;

		try {
			/*BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();*/
			String companyId = getcompanyId(socialData.getSourceId());
			//preparedStatement = connection.prepareStatement(Constants.INSERT_XPRESSO_DATA);
			preparedStatement = connection.prepareStatement(Constants.INSERT_XPRESSO_DATA+Constants.XPRESSO_OUTPUT+companyId+Constants.INSERT_XPRESSO_DATA1);
			//connection.setAutoCommit(false);

			for (XpressoAbout xpressoAbout : abouts) {
				preparedStatement.setInt(1, socialData.getSourceId());
				preparedStatement.setString(2, socialData.getPostId());

				preparedStatement.setString(3, xpressoAbout.getAspect());
				preparedStatement.setString(4, xpressoAbout.getSentiment());
				preparedStatement.setString(5, xpressoAbout.getEntity());
				preparedStatement.setString(6, xpressoAbout.getStatementType());
				preparedStatement.setString(7, null);
				preparedStatement.setString(8, xpressoAbout.getSnippet());

				preparedStatement.setString(9, null);
				preparedStatement.setString(10, null);
				preparedStatement.setString(11, xpressoValues.getDomain());
				System.out.println("ASPECT NAME :: " + xpressoAbout.getAspect());
				aspectId = aspectMap.get(xpressoAbout.getAspect());

				System.out.println("ID :: " + aspectId);
				preparedStatement.setInt(12, aspectId);
				preparedStatement.setInt(13, xpressoAbout.getSentiment_number());
				aspectId = -1;

				preparedStatement.executeUpdate();
			}

		} catch (SQLException e) {
			System.out.println("ERROR in SAVING XPRESSO OUTPUT :: " + e.getMessage());
			e.printStackTrace();
			connection.rollback();
		} catch (Exception e) {
			System.out.println("ERROR in SAVING XPRESSO OUTPUT :: " + e.getMessage());
			e.printStackTrace();
			connection.rollback();
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
			/*if (connection != null) {
				connection.close();
			}*/
		}
		System.out.println("Xpresso output saved successfully into xpresso_output table for post id " + socialData.getPostId());
	}

	public static void saveTrendingTopic(XpressoValues xpressoValues) throws SQLException {

		Map<String, Double> trendingTopicMap = xpressoValues.getTrendingTopics();
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			preparedStatement = connection.prepareStatement(Constants.INSERT_TRENDING_DATA);
			connection.setAutoCommit(false);

			for (Map.Entry<String, Double> trend : trendingTopicMap.entrySet()) {
				preparedStatement.setString(1, trend.getKey());
				preparedStatement.setString(2, null);
				preparedStatement.setDouble(3, trend.getValue());

				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			connection.rollback();
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
			if (connection != null) {
				connection.close();
			}
		}
		System.out.println("Xpresso output saved successfully into trending_topic table");
	}

	public static void deleteTrendingTopicData() throws SQLException {

		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			preparedStatement = connection.prepareStatement(Constants.DELETE_TRENDING_TOPIC);
			preparedStatement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
			connection.rollback();
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
			if (connection != null) {
				connection.close();
			}
		}
		System.out.println("History data deleted successfully from trending_topic table");
	}

	/**
	 * 
	 * Saving kpi page level data into database
	 * @throws SQLException 
	 * 
	 */

	public static void saveKPIPageLevel(PageInfo pageInfo) throws SQLException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			String dataSourceType = DataSource.getDataSourceType();
			if (dataSourceType.equalsIgnoreCase("mysql")) {
				preparedStatement = connection.prepareStatement(Constants.INSERT_KPI_PAGE_LEVEL);
			} else {
				preparedStatement = connection.prepareStatement(Constants.INSERT_KPI_PAGE_LEVEL_DB2);
			}

			PageInfo findPageInfo = getKPIPageLevel(pageInfo.getPageId(), pageInfo.getSourceId());
			if (findPageInfo != null)
				Util.updateKPIPageValues(pageInfo, findPageInfo);

			preparedStatement.setInt(1, pageInfo.getSourceId());
			preparedStatement.setString(2, pageInfo.getPageId());
			preparedStatement.setString(3, pageInfo.getDateKey());
			preparedStatement.setString(4, pageInfo.getHourKey());
			preparedStatement.setLong(5, pageInfo.getPageLike());
			preparedStatement.setLong(6, pageInfo.getPeopleTalkingAbout());
			preparedStatement.setLong(7, pageInfo.getWereHere());
			preparedStatement.setLong(8, pageInfo.getLikeCountry());
			preparedStatement.setLong(9, pageInfo.getPageStorytellerCountry());
			preparedStatement.setString(10, pageInfo.getCategory());
			preparedStatement.setLong(11, pageInfo.getTotalFollowings());
			preparedStatement.setLong(12, pageInfo.getTotalFollowers());

			if (dataSourceType.equalsIgnoreCase("mysql")) {
				preparedStatement.setLong(13, pageInfo.getPageLike());
				preparedStatement.setLong(14, pageInfo.getPeopleTalkingAbout());
				preparedStatement.setLong(15, pageInfo.getWereHere());
				preparedStatement.setLong(16, pageInfo.getLikeCountry());
				preparedStatement.setLong(17, pageInfo.getPageStorytellerCountry());
				preparedStatement.setString(18, pageInfo.getCategory());
				preparedStatement.setLong(19, pageInfo.getTotalFollowings());
				preparedStatement.setLong(20, pageInfo.getTotalFollowers());
			}
			preparedStatement.execute();
		} catch (SQLException e) {
			System.err.println("SQL Message in saveKPIPageLevel:: " + e.getMessage());
			System.err.println("SQL Error occurred in saveKPIPageLevel::" + preparedStatement.toString());
		} catch (Exception e) {
			System.err.println("Error Message in saveKPIPageLevel:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
			if (connection != null)
				connection.close();
		}
	}

	/**
	 * 
	 * Saving kpi post level details into database
	 * 
	 * @throws Exception
	 * 
	 */
	public static void saveKPIPostLevel(Set<SocialData> socialDatas) throws Exception {
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			preparedStatement = connection.prepareStatement(Constants.INSERT_KPI_POST_LEVEL);

			for (SocialData socialData : socialDatas) {
				try {
					if (socialData.getPostTypeId() < 1)
						continue;
					SocialData findSocialData = getSocialData(socialData.getPostId(), socialData.getParentId(), socialData.getSourceId(), true);
					if (findSocialData != null)
						Util.updateSocialDataValues(socialData, findSocialData);

					preparedStatement.setString(1, socialData.getPostId());
					preparedStatement.setString(2, socialData.getHashId());
					preparedStatement.setInt(3, socialData.getSourceId());
					preparedStatement.setInt(4, socialData.getPostTypeId());
					preparedStatement.setString(5, socialData.getDateKey());
					preparedStatement.setString(6, socialData.getHourKey());
					preparedStatement.setInt(7, socialData.getCommentsCount());
					preparedStatement.setInt(8, socialData.getLikesCount());
					preparedStatement.setInt(9, socialData.getSharesCount());
					preparedStatement.setInt(10, socialData.getReplyCount());
					preparedStatement.setInt(11, socialData.getNoneCount());
					preparedStatement.setInt(12, socialData.getLoveCount());
					preparedStatement.setInt(13, socialData.getHahaCount());
					preparedStatement.setInt(14, socialData.getWowCount());
					preparedStatement.setInt(15, socialData.getSadCount());
					preparedStatement.setInt(16, socialData.getAngryCount());
					preparedStatement.setInt(17, socialData.getThankfulCount());
					preparedStatement.setInt(18, socialData.getRetweetsCount());
					preparedStatement.setString(19, socialData.getUserLoc());
					preparedStatement.setString(20, socialData.getTweetLoc());
					preparedStatement.setBoolean(21, socialData.getPossiblySensitive());

					preparedStatement.setInt(22, socialData.getCommentsCount());
					preparedStatement.setInt(23, socialData.getLikesCount());
					preparedStatement.setInt(24, socialData.getSharesCount());
					preparedStatement.setInt(25, socialData.getReplyCount());
					preparedStatement.setInt(26, socialData.getNoneCount());
					preparedStatement.setInt(27, socialData.getLoveCount());
					preparedStatement.setInt(28, socialData.getHahaCount());
					preparedStatement.setInt(29, socialData.getWowCount());
					preparedStatement.setInt(30, socialData.getSadCount());
					preparedStatement.setInt(31, socialData.getAngryCount());
					preparedStatement.setInt(32, socialData.getThankfulCount());
					preparedStatement.setInt(33, socialData.getRetweetsCount());
					preparedStatement.setString(34, socialData.getUserLoc());
					preparedStatement.setString(35, socialData.getTweetLoc());
					preparedStatement.setBoolean(36, socialData.getPossiblySensitive());
					preparedStatement.executeUpdate();

				} catch (SQLException e) {
					System.out.println("Got insertion error :: " + e.getMessage());
					System.err.println("SQL Error occurred in saveKPIPostLevel::" + preparedStatement.toString());
					continue;
				} catch (Exception e) {
					System.out.println("Got error :: " + e.getMessage());
					e.printStackTrace();
					continue;
				}
			}
		} catch (SQLException e) {
			System.err.println("SQL Message in saveKPIPostLevel:: " + e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			System.err.println("Error Message in saveKPIPostLevel:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
			if (connection != null)
				connection.close();
		}
		System.out.println("Post data saved successfully into kpi_post_level table");
	}

	public static void saveKPIPostForumLevel(Set<SocialData> socialDatas) throws Exception {
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			String dataSourceType = DataSource.getDataSourceType();
			if (dataSourceType.equalsIgnoreCase("mysql")) {
				preparedStatement = connection.prepareStatement(Constants.INSERT_KPI_FORUM_POST_LEVEL);
			} else {
				preparedStatement = connection.prepareStatement(Constants.INSERT_KPI_FORUM_POST_LEVEL_DB2);
			}
			for (SocialData socialData : socialDatas) {
				try {
					/*if (socialData.getPostTypeId() < 1)
						continue;*/
					/*SocialData findSocialData = getKPIFacebookData(socialData.getPostId(), socialData.getSourceId());
					if (findSocialData != null)
						Util.updateSocialDataValues(socialData, findSocialData);*/

					preparedStatement.setString(1, socialData.getPostId());
					preparedStatement.setString(2, socialData.getHashId());
					preparedStatement.setInt(3, socialData.getSourceId());
					preparedStatement.setInt(4, socialData.getPostTypeId());
					preparedStatement.setString(5, socialData.getDateKey());
					preparedStatement.setString(6, socialData.getHourKey());
					preparedStatement.setInt(7, socialData.getLikesCount());
					preparedStatement.setInt(8, socialData.getCommentsCount());
					preparedStatement.setInt(9, socialData.getSharesCount());

					if (dataSourceType.equalsIgnoreCase("mysql")) {
						preparedStatement.setInt(10, socialData.getLikesCount());
						preparedStatement.setInt(11, socialData.getCommentsCount());
						preparedStatement.setInt(12, socialData.getSharesCount());
					}
					preparedStatement.executeUpdate();

				} catch (SQLException e) {
					System.err.println("Got insertion error in KPI Post level FORUM:: " + e.getMessage());
					System.err.println("SQL Error occurred in saveKPIPostFOLevel::" + Constants.INSERT_KPI_FORUM_POST_LEVEL_DB2);
					System.err.println("*************************************************************");
					System.err.println(socialData.toString());
					continue;
				} catch (Exception e) {
					System.err.println("Got error :: " + e.getMessage());
					e.printStackTrace();
					continue;
				}
			}
		} catch (SQLException e) {
			System.err.println("SQL Message in saveKPIPostFBLevel:: " + e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			System.err.println("Error Message in saveKPIPostFBLevel:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
			if (connection != null)
				connection.close();
		}
		System.out.println("Post data saved successfully into kpi_post_forum_level table");
	}

	public static void saveKPIPostFBLevel(Set<SocialData> socialDatas) throws Exception {
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			String dataSourceType = DataSource.getDataSourceType();
			
			/*if (dataSourceType.equalsIgnoreCase("mysql")) {
				preparedStatement = connection.prepareStatement(Constants.INSERT_KPI_FB_POST_LEVEL);
			} else {
				preparedStatement = connection.prepareStatement(Constants.INSERT_KPI_FB_POST_LEVEL_DB2);
			}*/
			for (SocialData socialData : socialDatas) {
				try {
					/*if (socialData.getPostTypeId() < 1)
						continue;*/
					String companyId = getcompanyId(socialData.getSourceId());
					if (dataSourceType.equalsIgnoreCase("mysql")) {
						//preparedStatement = connection.prepareStatement(Constants.INSERT_KPI_FB_POST_LEVEL);
						preparedStatement = connection.prepareStatement(Constants.INSERT_KPI_FB_POST_LEVEL+Constants.KPI_POST_LEVEL_FB+companyId+Constants.INSERT_KPI_FB_POST_LEVEL1);
					} else {
						preparedStatement = connection.prepareStatement(Constants.INSERT_KPI_FB_POST_LEVEL_DB2);
					}
					SocialData findSocialData = getKPIFacebookData(socialData.getPostId(), socialData.getSourceId());
					if (findSocialData != null)
						Util.updateSocialDataValues(socialData, findSocialData);

					preparedStatement.setString(1, socialData.getPostId());
					preparedStatement.setString(2, socialData.getHashId());
					preparedStatement.setInt(3, socialData.getSourceId());
					preparedStatement.setInt(4, socialData.getPostTypeId());
					preparedStatement.setString(5, socialData.getDateKey());
					preparedStatement.setString(6, socialData.getHourKey());
					preparedStatement.setInt(7, socialData.getCommentsCount());
					preparedStatement.setInt(8, socialData.getLikesCount());
					preparedStatement.setInt(9, socialData.getSharesCount());
					preparedStatement.setInt(10, socialData.getReplyCount());
					preparedStatement.setInt(11, socialData.getNoneCount());
					preparedStatement.setInt(12, socialData.getLoveCount());
					preparedStatement.setInt(13, socialData.getHahaCount());
					preparedStatement.setInt(14, socialData.getWowCount());
					preparedStatement.setInt(15, socialData.getSadCount());
					preparedStatement.setInt(16, socialData.getAngryCount());
					preparedStatement.setInt(17, socialData.getThankfulCount());

					if (dataSourceType.equalsIgnoreCase("mysql")) {
						preparedStatement.setInt(18, socialData.getCommentsCount());
						preparedStatement.setInt(19, socialData.getLikesCount());
						preparedStatement.setInt(20, socialData.getSharesCount());
						preparedStatement.setInt(21, socialData.getReplyCount());
						preparedStatement.setInt(22, socialData.getNoneCount());
						preparedStatement.setInt(23, socialData.getLoveCount());
						preparedStatement.setInt(24, socialData.getHahaCount());
						preparedStatement.setInt(25, socialData.getWowCount());
						preparedStatement.setInt(26, socialData.getSadCount());
						preparedStatement.setInt(27, socialData.getAngryCount());
						preparedStatement.setInt(28, socialData.getThankfulCount());
					}
					preparedStatement.executeUpdate();

				} catch (SQLException e) {
					System.err.println("Got insertion error in KPI Post level Facebook:: " + e.getMessage());
					System.err.println("SQL Error occurred in saveKPIPostFBLevel::" + preparedStatement.toString());
					continue;
				} catch (Exception e) {
					System.err.println("Got error :: " + e.getMessage());
					e.printStackTrace();
					continue;
				}
			}
		} catch (SQLException e) {
			System.err.println("SQL Message in saveKPIPostFBLevel:: " + e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			System.err.println("Error Message in saveKPIPostFBLevel:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
			if (connection != null)
				connection.close();
		}
		System.out.println("Post data saved successfully into kpi_post_facebook_level table");
	}

	public static void saveKPIPostTwitterLevel(Set<SocialData> socialDatas) throws Exception {
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			String dataSourceType = DataSource.getDataSourceType();
			/*if (dataSourceType.equalsIgnoreCase("mysql")) {
				preparedStatement = connection.prepareStatement(Constants.INSERT_KPI_TWITTER_POST_LEVEL);
			} else {
				preparedStatement = connection.prepareStatement(Constants.INSERT_KPI_TWITTER_POST_LEVEL_DB2);
			}*/
			for (SocialData socialData : socialDatas) {
				try {
					/*if (socialData.getPostTypeId() < 1)
						continue;*/
					SocialData findSocialData = getKPITwitterData(socialData.getPostId(), socialData.getSourceId());
					String companyId = getcompanyId(socialData.getSourceId());
					if (dataSourceType.equalsIgnoreCase("mysql")) {
						//preparedStatement = connection.prepareStatement(Constants.INSERT_KPI_TWITTER_POST_LEVEL);
						preparedStatement = connection.prepareStatement(Constants.INSERT_KPI_TWITTER_POST_LEVEL+Constants.KPI_POST_LEVEL_TW+companyId+Constants.INSERT_KPI_TWITTER_POST_LEVEL1);
					} else {
						preparedStatement = connection.prepareStatement(Constants.INSERT_KPI_TWITTER_POST_LEVEL_DB2);
					}
					if (findSocialData != null)
						Util.updateSocialDataValues(socialData, findSocialData);

					preparedStatement.setString(1, socialData.getPostId());
					preparedStatement.setString(2, socialData.getHashId());
					preparedStatement.setInt(3, socialData.getSourceId());
					preparedStatement.setInt(4, socialData.getPostTypeId());
					preparedStatement.setString(5, socialData.getDateKey());
					preparedStatement.setString(6, socialData.getHourKey());
					preparedStatement.setInt(7, socialData.getLikesCount());
					preparedStatement.setInt(8, socialData.getRetweetsCount());
					preparedStatement.setString(9, socialData.getUserLoc());
					preparedStatement.setString(10, socialData.getTweetLoc());
					preparedStatement.setBoolean(11, socialData.getPossiblySensitive());

					if (dataSourceType.equalsIgnoreCase("mysql")) {
						preparedStatement.setInt(12, socialData.getLikesCount());
						preparedStatement.setInt(13, socialData.getRetweetsCount());
						preparedStatement.setString(14, socialData.getUserLoc());
						preparedStatement.setString(15, socialData.getTweetLoc());
						preparedStatement.setBoolean(16, socialData.getPossiblySensitive());
					}
					preparedStatement.executeUpdate();

				} catch (SQLException e) {
					System.err.println("Got insertion error in KPI Post Twitter:: " + e.getMessage());
					System.err.println("SQL Error occurred in saveKPIPostTwitterLevel::" + preparedStatement.toString());
					continue;
				} catch (Exception e) {
					System.err.println("Got error :: " + e.getMessage());
					e.printStackTrace();
					continue;
				}
			}
		} catch (SQLException e) {
			System.err.println("SQL Message in saveKPIPostTwitterLevel:: " + e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			System.err.println("Error Message in saveKPIPostTwitterLevel:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
			if (connection != null)
				connection.close();
		}

		System.out.println("Post data saved successfully into kpi_post_twitter_level table");
	}

	/*
	 * Saving Page Insights For Facebook Sources into DB
	 */

	public static void saveKPIPageInsights(HashMap<String, Long> countryMap, PageInfo pageInfo, int insight_id) throws SQLException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			preparedStatement = connection.prepareStatement(Constants.INSERT_KPI_INSIGHTS);
			for (String country : countryMap.keySet()) {
				try {
					preparedStatement.setInt(1, pageInfo.getSourceId());
					preparedStatement.setString(2, pageInfo.getPageId());
					preparedStatement.setInt(3, insight_id);
					preparedStatement.setString(4, pageInfo.getDateKey());
					preparedStatement.setString(5, pageInfo.getHourKey());
					preparedStatement.setString(6, country);
					preparedStatement.setLong(7, countryMap.get(country).longValue());
					preparedStatement.setLong(8, countryMap.get(country).longValue());
					preparedStatement.execute();

				} catch (SQLException e) {
					System.out.println("Insertion Error in Page Insights Saving :: " + e.getMessage());
					System.err.println("SQL Error occurred in saveKPIPageInsights::" + preparedStatement.toString());
					continue;

				} catch (Exception e) {
					System.out.println("Error in Page Insights Saving :: " + e.getMessage());
					e.printStackTrace();
					continue;
				}
			}
		} catch (SQLException e) {
			System.err.println("SQL Message in saveKPIPageInsights:: " + e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			System.err.println("Error Message in saveKPIPageInsights:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
			if (connection != null)
				connection.close();
		}
		//System.out.println("Page Insights saved successfully");
	}

	/**
	 * Save alert.
	 *
	 * @param alertDTO the alert DTO
	 * @throws SQLException the SQL exception
	 */
	public static void saveAlert(AlertDTO alertDTO) throws SQLException {
		Connection connection = null;
		PreparedStatement preparedStatement = null;

		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			preparedStatement = connection.prepareStatement(Constants.INSERT_ALERT);

			preparedStatement.setString(1, alertDTO.getSubject());
			preparedStatement.setInt(2, alertDTO.getAlertCategoryId());
			preparedStatement.setInt(3, alertDTO.getSourceId());
			preparedStatement.setString(4, alertDTO.getDateHH());
			preparedStatement.setString(5, alertDTO.getDescription());
			preparedStatement.setString(6, alertDTO.getAlertType());
			preparedStatement.setInt(7, alertDTO.getPriority());
			preparedStatement.setBoolean(8, alertDTO.isActive());
			preparedStatement.setString(9, alertDTO.getAuthor());

			preparedStatement.execute();
		} catch (SQLException e) {
			System.err.println("SQL Message in saveAlert:: " + e.getMessage());
			System.err.println("SQL Error occurred in saveAlert::" + preparedStatement.toString());
		} catch (Exception e) {
			System.err.println("Error Message in saveAlert:: " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (preparedStatement != null)
				preparedStatement.close();
			if (connection != null)
				connection.close();
		}
	}

	public static void main(String args[]) {
		try {
			Map<String, Object> map = getDomainAndXpressoStatus(700);
			System.out.println("Domain :: " + map.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/** shubhra 16/11
	 * code to get company Id from source id
	 */
	private static String getcompanyId(int sourceId) throws Exception{
		
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		int companyId = 0 ;
		try {
		BasicDataSource bds = DataSource.getInstance().getBds();
		connection = bds.getConnection();
		preparedStatement = connection.prepareStatement(Constants.SELECT_COMPANY_ID);
		preparedStatement.setInt(1, sourceId);
		ResultSet rs = preparedStatement.executeQuery();
		while (rs.next()) {
			companyId = rs.getInt("company_id");
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
			connection.rollback();
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
			if (connection != null)
				connection.close();
		}
		return String.valueOf(companyId);
	}


       	/** shubhra 16/11
       	 * code to get source_type_name from source id
       	 */
	private static String getSourceTypeName(int sourceId) throws Exception{ 
			
		System.out.println("Entered getSourceTypeName method");
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		String strSourceTypeName = "" ;
		try {
			BasicDataSource bds = DataSource.getInstance().getBds();
			connection = bds.getConnection();
			preparedStatement = connection.prepareStatement(Constants.SELECT_SOURCE_NAME);
			preparedStatement.setInt(1, sourceId);
			ResultSet rs = preparedStatement.executeQuery();
				while (rs.next()) {
				strSourceTypeName = rs.getString("source_type_name");
				}
		}
		catch (SQLException e) {
		System.out.println("Error in getting source type name");
		e.printStackTrace();
		connection.rollback();
		} finally {
			if (preparedStatement != null) {
			preparedStatement.close();
			}
			if (connection != null)
			connection.close();
			}
		System.out.println("Source Type Name:"+strSourceTypeName);
		return strSourceTypeName;
		}

}