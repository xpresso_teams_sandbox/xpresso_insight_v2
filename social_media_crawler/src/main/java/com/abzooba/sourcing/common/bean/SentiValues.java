package com.abzooba.sourcing.common.bean;

import java.io.Serializable;
import java.util.Comparator;

public class SentiValues implements Serializable {
	
	private static final long serialVersionUID = 1L;

	String senti = null;
	double score = 0;
	
	public SentiValues(String senti, double score){
		this.senti= senti;
		this.score = score;
	}

	public String getSenti() {
		return senti;
	}

	public void setSenti(String senti) {
		this.senti = senti;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}
}

class SentiComparer implements Comparator<SentiValues> {
	
	public int compare(SentiValues x, SentiValues y) {
		int startComparison = compare(x.getScore(), y.getScore());
		return startComparison != 0 ? startComparison : compare(x.getScore(), y.getScore());
	}

	private static int compare(double a, double b) {
		return a < b ? 1 : a > b ? -1 : 0;
	}
}
