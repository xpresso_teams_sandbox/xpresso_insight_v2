package com.abzooba.sourcing.common.bean;

import java.io.Serializable;
import java.sql.Date;

public class PageInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private String pageId;
	private int sourceId;
	private String dateKey;
	private String hourKey;
	private long pageLike = 0;
	private long peopleTalkingAbout = 0;
	private long peopleWereHere = 0;
	private long totalFollowings = 0;
	private long totalFollowers = 0;
	private double ratings = 0.0;
	private String pageURL;
	private long wereHere = 0;
	private long likeCountry = 0;
	private long pageStorytellerCountry = 0;
	private String category;
	private String city;
	private String country;
	private double latitude;
	private double longitude;
	private Date createDateTime;
	private Date updateDateTime;

	public String getPageId() {
		return pageId;
	}

	public void setPageId(String pageId) {
		this.pageId = pageId;
	}

	public int getSourceId() {
		return sourceId;
	}

	public void setSourceId(int sourceId) {
		this.sourceId = sourceId;
	}

	public String getDateKey() {
		return dateKey;
	}

	public void setDateKey(String dateKey) {
		this.dateKey = dateKey;
	}

	public String getHourKey() {
		return hourKey;
	}

	public void setHourKey(String hourKey) {
		this.hourKey = hourKey;
	}

	public long getPageLike() {
		return pageLike;
	}

	public void setPageLike(long pageLike) {
		this.pageLike = pageLike;
	}

	public long getPeopleTalkingAbout() {
		return peopleTalkingAbout;
	}

	public void setPeopleTalkingAbout(long peopleTalkingAbout) {
		this.peopleTalkingAbout = peopleTalkingAbout;
	}

	public long getPeopleWereHere() {
		return peopleWereHere;
	}

	public void setPeopleWereHere(long peopleWereHere) {
		this.peopleWereHere = peopleWereHere;
	}

	public long getTotalFollowings() {
		return totalFollowings;
	}

	public void setTotalFollowings(long totalFollowings) {
		this.totalFollowings = totalFollowings;
	}

	public long getTotalFollowers() {
		return totalFollowers;
	}

	public void setTotalFollowers(long totalFollowers) {
		this.totalFollowers = totalFollowers;
	}

	public double getRatings() {
		return ratings;
	}

	public void setRatings(double ratings) {
		this.ratings = ratings;
	}

	public String getPageURL() {
		return pageURL;
	}

	public void setPageURL(String pageURL) {
		this.pageURL = pageURL;
	}

	public long getWereHere() {
		return wereHere;
	}

	public void setWereHere(long wereHere) {
		this.wereHere = wereHere;
	}

	public long getLikeCountry() {
		return likeCountry;
	}

	public void setLikeCountry(long likeCountry) {
		this.likeCountry = likeCountry;
	}

	public long getPageStorytellerCountry() {
		return pageStorytellerCountry;
	}

	public void setPageStorytellerCountry(long pageStorytellerCountry) {
		this.pageStorytellerCountry = pageStorytellerCountry;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return the createDateTime
	 */
	public Date getCreateDateTime() {
		return createDateTime;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	/**
	 * @param createDateTime the createDateTime to set
	 */
	public void setCreateDateTime(Date createDateTime) {
		this.createDateTime = createDateTime;
	}

	/**
	 * @return the updateDateTime
	 */
	public Date getUpdateDateTime() {
		return updateDateTime;
	}

	/**
	 * @param updateDateTime the updateDateTime to set
	 */
	public void setUpdateDateTime(Date updateDateTime) {
		this.updateDateTime = updateDateTime;
	}

	@Override
	public String toString() {
		return "PageInfo [pageId=" + pageId + ", sourceId=" + sourceId + ", dateKey=" + dateKey + ", hourKey=" + hourKey + ", pageLike=" + pageLike + ", peopleTalkingAbout=" + peopleTalkingAbout + ", totalFollowings=" + totalFollowings + ", totalFollowers=" + totalFollowers + ", ratings=" + ratings + ", pageURL=" + pageURL + ", wereHere=" + wereHere + ", likeCountry=" + likeCountry + ", pageStorytellerCountry=" + pageStorytellerCountry + ", category=" + category + ", createDateTime=" + createDateTime + ", updateDateTime=" + updateDateTime + "]";
	}

}
