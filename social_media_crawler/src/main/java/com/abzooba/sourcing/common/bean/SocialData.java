package com.abzooba.sourcing.common.bean;

import java.io.Serializable;
import java.sql.Date;
import java.util.HashMap;

public class SocialData implements Serializable {

	private static final long serialVersionUID = 1L;

	private int sourceId;
	private String postId;
	private String parentId;
	private String hashId;
	private int sourceTypeId;
	private int postTypeId;
	private int shortnameId;
	private String postDateTime;
	private String dateKey;
	private String hourKey;
	private String postText;
	private String headline;
	private String breakingNews;
	private String type = "Text";
	private boolean isText = true;
	private String authorId;
	private String authorName;
	private int authorOutreach;
	private String postURL;
	private String userURL;
	private String link;
	private int isUpdated;
	private int isMention;
	private int xpressoEnabled;
	private String xpressoReview;
	private int isAnalyzed = 0;
	private HashMap<String, Integer> aspectMap;
	private String overallSentiment = "Neutral";
	private int overallSentimentNumber = 0;
	private double overallSentimentScore = 0;
	private int overallAspectId = 0;
	private String xpressoDomain;
	private int commentsCount;
	private int likesCount;
	private int replyCount;
	private int noneCount;
	private int loveCount;
	private int hahaCount;
	private int wowCount;
	private int sadCount;
	private int angryCount;
	private int thankfulCount;
	private int sharesCount;
	private int retweetsCount;
	private String userLoc;
	private String tweetLoc;
	private boolean possiblySensitive;

	private Date createDateTime;
	private Date updateDateTime;

	public String getPostId() {
		return postId;
	}

	public void setPostId(String postId) {
		this.postId = postId;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public int getSourceId() {
		return sourceId;
	}

	public void setSourceId(int sourceId) {
		this.sourceId = sourceId;
	}

	public int getSourceTypeId() {
		return sourceTypeId;
	}

	public void setSourceTypeId(int sourceTypeId) {
		this.sourceTypeId = sourceTypeId;
	}

	public int getShortnameId() {
		return shortnameId;
	}

	public void setShortnameId(int shortnameId) {
		this.shortnameId = shortnameId;
	}

	public String getHashId() {
		return hashId;
	}

	public void setHashId(String hashId) {
		this.hashId = hashId;
	}

	public int getPostTypeId() {
		return postTypeId;
	}

	public void setPostTypeId(int postTypeId) {
		this.postTypeId = postTypeId;
	}

	public String getPostDateTime() {
		return postDateTime;
	}

	public void setPostDateTime(String postDateTime) {
		this.postDateTime = postDateTime;
	}

	public String getDateKey() {
		return dateKey;
	}

	public void setDateKey(String dateKey) {
		this.dateKey = dateKey;
	}

	public String getHourKey() {
		return hourKey;
	}

	public void setHourKey(String hourKey) {
		this.hourKey = hourKey;
	}

	public String getPostText() {
		return postText;
	}

	public void setPostText(String postText) {
		this.postText = postText;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isText() {
		return isText;
	}

	public void setText(boolean isText) {
		this.isText = isText;
	}

	public String getHeadline() {
		return headline;
	}

	public void setHeadline(String headline) {
		this.headline = headline;
	}

	public String getBreakingNews() {
		return breakingNews;
	}

	public void setBreakingNews(String breakingNews) {
		this.breakingNews = breakingNews;
	}

	public int getIsUpdated() {
		return isUpdated;
	}

	public void setIsUpdated(int isUpdated) {
		this.isUpdated = isUpdated;
	}

	public int getIsMention() {
		return isMention;
	}

	public void setIsMention(int isMention) {
		this.isMention = isMention;
	}

	public int getLikesCount() {
		return likesCount;
	}

	public void setLikesCount(int likesCount) {
		this.likesCount = likesCount;
	}

	public int getReplyCount() {
		return replyCount;
	}

	public void setReplyCount(int replyCount) {
		this.replyCount = replyCount;
	}

	public int getNoneCount() {
		return noneCount;
	}

	public void setNoneCount(int noneCount) {
		this.noneCount = noneCount;
	}

	public int getLoveCount() {
		return loveCount;
	}

	public void setLoveCount(int loveCount) {
		this.loveCount = loveCount;
	}

	public int getHahaCount() {
		return hahaCount;
	}

	public void setHahaCount(int hahaCount) {
		this.hahaCount = hahaCount;
	}

	public int getWowCount() {
		return wowCount;
	}

	public void setWowCount(int wowCount) {
		this.wowCount = wowCount;
	}

	public int getSadCount() {
		return sadCount;
	}

	public void setSadCount(int sadCount) {
		this.sadCount = sadCount;
	}

	public int getAngryCount() {
		return angryCount;
	}

	public void setAngryCount(int angryCount) {
		this.angryCount = angryCount;
	}

	public int getThankfulCount() {
		return thankfulCount;
	}

	public void setThankfulCount(int thankfulCount) {
		this.thankfulCount = thankfulCount;
	}

	public String getUserLoc() {
		return userLoc;
	}

	public String getUserURL() {
		return userURL;
	}

	public void setUserURL(String userURL) {
		this.userURL = userURL;
	}

	public void setUserLoc(String userLoc) {
		this.userLoc = userLoc;
	}

	public String getTweetLoc() {
		return tweetLoc;
	}

	public void setTweetLoc(String tweetLoc) {
		this.tweetLoc = tweetLoc;
	}

	public boolean getPossiblySensitive() {
		return possiblySensitive;
	}

	public void setPossiblySensitive(boolean possiblySensitive) {
		this.possiblySensitive = possiblySensitive;
	}

	public int getCommentsCount() {
		return commentsCount;
	}

	public void setCommentsCount(int commentsCount) {
		this.commentsCount = commentsCount;
	}

	public int getRetweetsCount() {
		return retweetsCount;
	}

	public void setRetweetsCount(int retweetsCount) {
		this.retweetsCount = retweetsCount;
	}

	public int getSharesCount() {
		return sharesCount;
	}

	public void setSharesCount(int sharesCount) {
		this.sharesCount = sharesCount;
	}

	public String getAuthorId() {
		return authorId;
	}

	public void setAuthorId(String authorId) {
		this.authorId = authorId;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public String getPostURL() {
		return postURL;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public int getAuthorOutreach() {
		return authorOutreach;
	}

	public void setAuthorOutreach(int authorOutreach) {
		this.authorOutreach = authorOutreach;
	}

	public void setPostURL(String postURL) {
		this.postURL = postURL;
	}

	/**
	 * @return the createDateTime
	 */
	public Date getCreateDateTime() {
		return createDateTime;
	}

	/**
	 * @param createDateTime the createDateTime to set
	 */
	public void setCreateDateTime(Date createDateTime) {
		this.createDateTime = createDateTime;
	}

	/**
	 * @return the updateDateTime
	 */
	public Date getUpdateDateTime() {
		return updateDateTime;
	}

	/**
	 * @param updateDateTime the updateDateTime to set
	 */
	public void setUpdateDateTime(Date updateDateTime) {
		this.updateDateTime = updateDateTime;
	}

	public HashMap<String, Integer> getAspectMap() {
		return aspectMap;
	}

	public void setAspectMap(HashMap<String, Integer> aspectMap) {
		this.aspectMap = aspectMap;
	}

	public int getXpressoEnabled() {
		return xpressoEnabled;
	}

	public void setXpressoEnabled(int xpressoEnabled) {
		this.xpressoEnabled = xpressoEnabled;
	}

	public int getIsAnalyzed() {
		return isAnalyzed;
	}

	public void setIsAnalyzed(int isAnalyzed) {
		this.isAnalyzed = isAnalyzed;
	}

	public String getOverallSentiment() {
		return overallSentiment;
	}

	public void setOverallSentiment(String overallSentiment) {
		this.overallSentiment = overallSentiment;
	}

	public int getOverallSentimentNumber() {
		return overallSentimentNumber;
	}

	public void setOverallSentiment_number(int overallSentimentNumber) {
		this.overallSentimentNumber = overallSentimentNumber;
	}

	public double getOverallSentimentScore() {
		return overallSentimentScore;
	}

	public void setOverallSentimentScore(double overallSentimentScore) {
		this.overallSentimentScore = overallSentimentScore;
	}

	public int getOverallAspectId() {
		return overallAspectId;
	}

	public void setOverallAspectId(int overallAspectId) {
		this.overallAspectId = overallAspectId;
	}

	public String getXpressoDomain() {
		return xpressoDomain;
	}

	public void setXpressoDomain(String xpressoDomain) {
		this.xpressoDomain = xpressoDomain;
	}

	public String getXpressoReview() {
		return xpressoReview;
	}

	public void setXpressoReview(String xpressoReview) {
		this.xpressoReview = xpressoReview;
	}

	@Override
	public String toString() {
		return "SocialData [postId=" + postId + ", parentId=" + parentId + ", hashId=" + hashId + ", sourceId=" + sourceId + ", sourceTypeId=" + sourceTypeId + ", shortnameId=" + shortnameId + ", postTypeId=" + postTypeId + ", postDateTime=" + postDateTime + ", dateKey=" + dateKey + ", hourKey=" + hourKey + ", postText=" + postText + ", type=" + type + ", isText=" + isText + ", headline=" + headline + ", breakingNews=" + breakingNews + ", commentsCount=" + commentsCount + ", likesCount=" + likesCount + ", replyCount=" + replyCount + ", noneCount=" + noneCount + ", loveCount=" + loveCount + ", hahaCount=" + hahaCount + ", wowCount=" + wowCount + ", sadCount=" + sadCount + ", angryCount=" + angryCount + ", thankfulCount=" + thankfulCount + ", sharesCount=" + sharesCount + ", retweetsCount=" + retweetsCount + ", userLoc=" + userLoc + ", tweetLoc=" + tweetLoc + ", possiblySensitive=" + possiblySensitive + ", authorId=" + authorId + ", authorName=" + authorName + ", postURL=" + postURL + ", userURL=" + userURL + ", createDateTime=" + createDateTime + ", updateDateTime=" + updateDateTime + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof SocialData) {
			SocialData socialData = (SocialData) obj;
			return (socialData.postId.equals(this.postId) && socialData.parentId.equals(this.parentId) && socialData.sourceId == this.sourceId && socialData.postTypeId == this.postTypeId);
		} else {
			return false;
		}
	}
}
