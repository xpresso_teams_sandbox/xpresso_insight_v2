package com.abzooba.sourcing.common.util;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public interface Constants {
	public static final String LOG_FILENAME = "storm.log";
	public static final String LOGGER_NAME = "com.abzooba.storm";

	public static final String SELECT_SOURCE_URL_MySQL = "SELECT sm.source_id, sm.source_url, sm.source_type_id, sm.domain_id,dm.xpresso_domain,sm.shortname_id, sm.xpresso_enable, sm.page_name, sm.search, sm.geo_centers, sm.cordinates,sm.search_words, sm.range FROM source_master AS sm, source_types AS st ,domain_master as dm WHERE live_source = 1 AND TIMESTAMPDIFF(MINUTE, sm.last_streaming, CURRENT_TIMESTAMP) > ? AND sm.source_type_id = st.source_type_id AND st.source_type_code = ? AND sm.domain_id = dm.domain_id";

	public static final String SELECT_FB_COMMENT_SOURCE_URL_MySQL = "SELECT sm.source_id, sm.source_url, sm.source_type_id, sm.domain_id,dm.xpresso_domain,sm.shortname_id, sm.xpresso_enable, sm.page_name, sm.search, sm.geo_centers, sm.cordinates,sm.search_words, sm.range FROM source_master AS sm, source_types AS st ,domain_master as dm WHERE live_source = 1 AND TIMESTAMPDIFF(MINUTE, sm.last_streaming_comment, CURRENT_TIMESTAMP) > ? AND sm.source_type_id = st.source_type_id AND st.source_type_code = ? AND sm.domain_id = dm.domain_id";

	public static final String SELECT_FB_REPLY_SOURCE_URL_MySQL = "SELECT sm.source_id, sm.source_url, sm.source_type_id, sm.domain_id,dm.xpresso_domain,sm.shortname_id, sm.xpresso_enable, sm.page_name, sm.search, sm.geo_centers, sm.cordinates,sm.search_words, sm.range FROM source_master AS sm, source_types AS st ,domain_master as dm WHERE live_source = 1 AND TIMESTAMPDIFF(MINUTE, sm.last_streaming_reply, CURRENT_TIMESTAMP) > ? AND sm.source_type_id = st.source_type_id AND st.source_type_code = ? AND sm.domain_id = dm.domain_id";

	public static final String SELECT_SOURCE_URL_DB2 = "SELECT sm.source_id, sm.source_url, sm.source_type_id, sm.domain_id,dm.xpresso_domain,sm.shortname_id, sm.xpresso_enable, sm.page_name, sm.search, sm.geo_centers, sm.cordinates,sm.search_words, sm.range FROM source_master AS sm, source_types AS st ,domain_master as dm WHERE live_source = 1 AND TIMESTAMPDIFF(4, CURRENT TIMESTAMP - TIMESTAMP(sm.last_streaming)) > ? AND sm.source_type_id = st.source_type_id AND st.source_type_code = ? AND sm.domain_id = dm.domain_id";

	public static final String SELECT_FB_COMMENT_SOURCE_URL_DB2 = "SELECT sm.source_id, sm.source_url, sm.source_type_id, sm.domain_id,dm.xpresso_domain,sm.shortname_id, sm.xpresso_enable, sm.page_name, sm.search, sm.geo_centers, sm.cordinates,sm.search_words, sm.range FROM source_master AS sm, source_types AS st ,domain_master as dm WHERE live_source = 1 AND TIMESTAMPDIFF(4, CURRENT TIMESTAMP - TIMESTAMP(sm.last_streaming_comment)) > ? AND sm.source_type_id = st.source_type_id AND st.source_type_code = ? AND sm.domain_id = dm.domain_id";

	public static final String SELECT_FB_REPLY_SOURCE_URL_DB2 = "SELECT sm.source_id, sm.source_url, sm.source_type_id, sm.domain_id,dm.xpresso_domain,sm.shortname_id, sm.xpresso_enable, sm.page_name, sm.search, sm.geo_centers, sm.cordinates,sm.search_words, sm.range FROM source_master AS sm, source_types AS st ,domain_master as dm WHERE live_source = 1 AND TIMESTAMPDIFF(4, CURRENT TIMESTAMP - TIMESTAMP(sm.last_streaming_reply)) > ? AND sm.source_type_id = st.source_type_id AND st.source_type_code = ? AND sm.domain_id = dm.domain_id";

	public static final String SELECT_TW_USER_SOURCE_URL_MySQL = "SELECT sm.source_id, sm.source_url, sm.source_type_id, sm.domain_id, sm.xpresso_enable, sm.page_name, sm.search, sm.geo_centers, sm.cordinates, sm.search_words, sm.range FROM source_master AS sm, source_types AS st WHERE live_source = 1  AND sm.source_type_id = st.source_type_id AND st.source_type_code = ?";

	public static final String SELECT_TW_SOURCE_URL_MySQL = "SELECT sm.source_id, sm.source_url, sm.source_type_id, sm.domain_id, sm.xpresso_enable, sm.page_name, sm.search, sm.geo_centers, sm.cordinates, sm.search_words, sm.range FROM source_master AS sm, source_types AS st WHERE live_source = 1 AND TIMESTAMPDIFF(MINUTE, sm.last_streaming_comment, CURRENT_TIMESTAMP) > ? " + " AND sm.source_type_id = st.source_type_id AND st.source_type_code = ?";

	public static final String SELECT_TW_SOURCE_URL_DB2 = "SELECT sm.source_id, sm.source_url, sm.source_type_id, sm.domain_id, sm.xpresso_enable, sm.page_name, sm.search, sm.geo_centers, sm.cordinates, sm.search_words, sm.range FROM source_master AS sm, source_types AS st WHERE live_source = 1 AND TIMESTAMPDIFF(4, CURRENT TIMESTAMP - TIMESTAMP(sm.last_streaming)) > ? " + " AND sm.source_type_id = st.source_type_id AND st.source_type_code = ?";

	//public static final String SELECT_MAX_POST_DATETIME = "SELECT MAX(post_datetime) AS maxdate FROM social_data WHERE source_id = ? AND parent_id = ?";
	public static final String SELECT_MAX_POST_DATETIME = "SELECT MAX(post_datetime) AS maxdate FROM ";
	public static final String WHERE_MAX_POST_DATETIME = " WHERE source_id = ? AND parent_id = ?";


	//public static final String SELECT_MAX_COMMENT_DATETIME = "SELECT MAX(post_datetime) AS maxdate FROM social_data WHERE source_id = ? AND parent_id = ?";

	//public static final String SELECT_PAGE_POST = "SELECT p.page_id, s.post_id FROM page_info AS p, social_data AS s WHERE p.page_id = s.parent_id AND p.source_id = s.source_id AND s.source_id = ? AND post_datetime BETWEEN ? AND ? ORDER BY p.page_id DESC , s.post_datetime DESC";
	public static final String SELECT_PAGE_POST = "SELECT p.page_id, s.post_id FROM page_info AS p, ";
	public static final String WHERE_PAGE_POST	=" AS s WHERE p.page_id = s.parent_id AND p.source_id = s.source_id AND s.source_id = ? AND post_datetime BETWEEN ? AND ? ORDER BY p.page_id DESC ,s.post_datetime DESC";

	public static final String SELECT_PAGE_POST_FB = "SELECT parent_id , post_id FROM ";
	public static final String WHERE_PAGE_POST_FB = " WHERE source_id = ? AND post_type_id IN (? , ?) AND post_datetime BETWEEN ? AND ? ORDER BY post_datetime DESC";

	//public static final String SELECT_PAGE_COMMENT = "SELECT parent_id,post_id FROM social_data WHERE source_id = ? AND post_type_id = ?  AND post_datetime BETWEEN ? AND ?   ORDER BY post_datetime DESC";
	public static final String SELECT_PAGE_COMMENT = "SELECT parent_id,post_id FROM ";
	public static final String WHERE_PAGE_COMMENT  = " WHERE source_id = ? AND post_type_id = ?  AND post_datetime BETWEEN ? AND ?   ORDER BY post_datetime DESC";

	public static final String SELECT_DOMAIN_AND_XPRESSO_CALL = "SELECT d.xpresso_domain, s.xpresso_enable FROM domain_master AS d, source_master AS s WHERE d.domain_id = s.domain_id AND s.source_id = ?";

	public static final String SELECT_PAGE_INFO = "SELECT * FROM page_info WHERE page_id = ?";

	public static final String SELECT_KPI_PAGE_LEVEL = "SELECT * FROM kpi_page_level WHERE page_id = ? AND source_id = ? ORDER by date_key DESC, hour_key DESC LIMIT 1";

	public static final String SELECT_KPI_PAGE_LEVEL_DB2 = "SELECT * FROM kpi_page_level WHERE page_id = ? AND source_id = ? ORDER by date_key DESC, hour_key DESC FETCH FIRST 1 ROWS ONLY";

	public static final String SELECT_SOURCE_TYPE_ID = "SELECT source_type_id FROM source_types WHERE source_type_code = ?";

	public static final String SELECT_SHORTNAME_ID = "SELECT shortname_id FROM shortname_master WHERE shortname = ?";

	public static final String SELECT_SHORTNAME_ID_USING_SOURCE_ID = "SELECT shortname_id FROM source_master WHERE source_id = ?";

	//public static final String SELECT_SOCIAL_DATA = "SELECT * FROM social_data WHERE post_id = ? AND parent_id = ? AND source_id = ?";
	public static final String SELECT_SOCIAL_DATA = "SELECT * FROM ";
	public static final String WHERE_SOCIAL_DATA = " WHERE post_id = ? AND parent_id = ? AND source_id = ?";
	

	public static final String SELECT_RSS_SOCIAL_DATA = "SELECT * FROM social_data WHERE post_id = ? AND source_id = ?";

	public static final String SELECT_KPI_POST_DATA = "SELECT * FROM kpi_post_level WHERE post_id = ? AND source_id = ? ORDER by date_key DESC, hour_key DESC LIMIT 1";

	//public static final String SELECT_KPI_POST_FB_DATA = "SELECT * FROM kpi_post_level_fb WHERE post_id = ? AND source_id = ? ORDER by date_key DESC, hour_key DESC LIMIT 1";
	public static final String SELECT_KPI_POST_FB_DATA = "SELECT * FROM ";
	public static final String WHERE_KPI_POST_FB_DATA = " WHERE post_id = ? AND source_id = ? ORDER by date_key DESC, hour_key DESC LIMIT 1";

	public static final String SELECT_KPI_POST_FB_DATA_DB2 = "SELECT * FROM kpi_post_level_fb WHERE post_id = ? AND source_id = ? ORDER by date_key DESC, hour_key DESC  FETCH FIRST 1 ROWS ONLY";

	//public static final String SELECT_KPI_POST_TWITTER_DATA = "SELECT * FROM kpi_post_level_tw WHERE post_id = ? AND source_id = ? ORDER by date_key DESC, hour_key DESC LIMIT 1";
	public static final String SELECT_KPI_POST_TWITTER_DATA = "SELECT * FROM ";
	public static final String WHERE_KPI_POST_TWITTER_DATA = " WHERE post_id = ? AND source_id = ? ORDER by date_key DESC, hour_key DESC LIMIT 1";

	public static final String SELECT_KPI_POST_TWITTER_DATA_DB2 = "SELECT * FROM kpi_post_level_tw WHERE post_id = ? AND source_id = ? ORDER by date_key DESC, hour_key DESC  FETCH FIRST 1 ROWS ONLY";

	//public static final String SELECT_SOCIAL_DATA_COUNT = "SELECT COUNT(*) AS record FROM social_data WHERE post_id = ? and parent_id = ? and source_id = ? and is_analyzed = 0";

	//public static final String SELECT_SOCIAL_DATA_COUNT = "SELECT COUNT(*) AS record FROM social_data WHERE post_id = ? and parent_id = ? and source_id = ?";
	public static final String SELECT_SOCIAL_DATA_COUNT = "SELECT COUNT(*) AS record FROM ";
	public static final String WHERE_SOCIAL_DATA_COUNT = " WHERE post_id = ? and parent_id = ? and source_id = ?";

	public static final String SELECT_APP_SECRET = "SELECT token_id, app_key, app_secret FROM token_master WHERE status = 0 AND source_type_id = ? ORDER BY token_id LIMIT 1";

	public static final String SELECT_APP_SECRET_DB2 = "SELECT token_id, app_key, app_secret FROM token_master WHERE status = 0 AND source_type_id = ? ORDER BY token_id FETCH FIRST 1 ROWS ONLY";

	public static final String SELECT_APP_SECRET_PROFILING = "SELECT token_id, app_key, app_secret FROM token_master WHERE profiling_status = 0 AND source_type_id = ? ORDER BY token_id  LIMIT 1";

	public static final String SELECT_APP_SECRET_DB2_PROFILING = "SELECT token_id, app_key, app_secret FROM token_master WHERE profiling_status = 0 AND source_type_id = ? ORDER BY token_id  FETCH FIRST 1 ROWS ONLY";

	public static final String UPDATE_APP_SECRET_SINGLE = "UPDATE token_master set status = ? WHERE token_id = ? AND source_type_id = ?";

	public static final String UPDATE_APP_SECRET_SINGLE_PROFILING = "UPDATE token_master set profiling_status = ? WHERE token_id = ? AND source_type_id = ?";

	public static final String UPDATE_USER_TOKEN_SINGLE = "UPDATE user_token_master set status = ? WHERE app_key = ?";

	public static final String UPDATE_APP_SECRET_ALL = "UPDATE token_master set status = ? WHERE source_type_id = ?";

	public static final String UPDATE_APP_SECRET_ALL_PROFILING = "UPDATE token_master set profiling_status = ? WHERE source_type_id = ?";

	public static final String UPDATE_USER_TOKEN_ALL = "UPDATE user_token_master set status = ?";

	public static final String UPDATE_SOURCE_MASTER_LAST_STREAMING_AND_STATUS = "UPDATE source_master SET last_streaming = CURRENT_TIMESTAMP, live_status = ? WHERE source_id = ?";

	public static final String UPDATE_SOURCE_FB_COMMENT_MASTER_LAST_STREAMING_AND_STATUS = "UPDATE source_master SET last_streaming_comment = CURRENT_TIMESTAMP, live_status = ? WHERE source_id = ?";

	public static final String UPDATE_SOURCE_FB_REPLY_MASTER_LAST_STREAMING_AND_STATUS = "UPDATE source_master SET last_streaming_reply = CURRENT_TIMESTAMP, live_status = ? WHERE source_id = ?";

	public static final String UPDATE_SOURCE_MASTER_STATUS = "UPDATE source_master SET live_status = ? WHERE source_id = ?";

	public static final String UPDATE_PAGE_INFO = "UPDATE page_info SET source_id = ?, page_like = ?, people_talking_about = ?, total_followings = ?, total_follower = ?, page_url = ?, were_here_count = ? " + "WHERE page_id = ?";

	public static final String UPDATE_PAGE_INFO_DB2 = "MERGE INTO page_info AS T USING (VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)) AS DAT(page_id, source_id, page_like, people_talking_about, were_here_count, total_followings, total_follower, page_url, city, country, latitude, longitude) ON T.page_id = DAT.page_id WHEN MATCHED THEN UPDATE SET T.page_like = DAT.page_like, T.people_talking_about = DAT.people_talking_about, T.total_followings = DAT.total_followings, T.total_follower = DAT.total_follower, T.page_url = DAT.page_url WHEN NOT MATCHED THEN INSERT (page_id, source_id, page_like, people_talking_about, were_here_count, total_followings, total_follower, page_url, city, country, latitude, longitude) VALUES (DAT.page_id, DAT.source_id, DAT.page_like, DAT.people_talking_about, DAT.were_here_count, DAT.total_followings, DAT.total_follower, DAT.page_url, DAT.city, DAT.country, DAT.latitude, DAT.longitude)";

	public static final String UPDATE_SOCIAL_DATA = "UPDATE ";
	public static final String SET_SOCIAL_DATA=" SET breaking_news = ?, comment_count = ?, likes_count = ?, share_count = ?, retweet_count = ? WHERE post_id = ? AND parent_id = ? AND source_id = ?";

	//public static final String UPDATE_SOCIAL_DATA_WITH_FLAG = "UPDATE social_data SET parent_id = ? ,post_text = ?, headline = ?, is_updated = ? WHERE post_id = ? AND source_id = ?";
	public static final String UPDATE_SOCIAL_DATA_WITH_FLAG = "UPDATE ";
	public static final String SET_SOCIAL_DATA_WITH_FLAG = " SET parent_id = ? ,post_text = ?, headline = ?, is_updated = ? WHERE post_id = ? AND source_id = ?";

	//public static final String UPDATE_SOCIAL_DATA_WITH_XPRESSO_OUTPUT = "UPDATE social_data SET is_analyzed = 2, overall_sentiment = ?, overall_sentiment_score = ?, aspect_id = ?,overall_sentiment_number = ? WHERE post_id = ? AND source_id = ?";
	public static final String UPDATE_SOCIAL_DATA_WITH_XPRESSO_OUTPUT = "UPDATE ";
	public static final String SET_SOCIAL_DATA_WITH_XPRESSO_OUTPUT = " SET is_analyzed = 2, overall_sentiment = ?, overall_sentiment_score = ?, aspect_id = ?,overall_sentiment_number = ? WHERE post_id = ? AND source_id = ?";

	public static final String SELECT_ASPECT_ID = "SELECT aspect_id FROM aspect_master WHERE aspect_name = ?";

	public static final String SELECT_ASPECT_MASTER = "SELECT aspect_id,aspect_name FROM aspect_master";

	//public static final String UPDATE_SOCIAL_DATA_WITH_IS_ANALYZED = "UPDATE social_data SET is_analyzed = ? WHERE post_id = ? AND source_id = ?";
	public static final String UPDATE_SOCIAL_DATA_WITH_IS_ANALYZED = "UPDATE ";
	public static final String SET_SOCIAL_DATA_WITH_IS_ANALYZED = " SET is_analyzed = ? WHERE post_id = ? AND source_id = ?";

	public static final String INSERT_PAGE_INFO = "INSERT INTO page_info(page_id, source_id, page_like, people_talking_about, were_here_count, total_followings, total_follower, page_url, city, country, latitude, longitude) " + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

	public static final String INSERT_SOCIAL_DATA = "INSERT INTO ";
	public static final String INSERT_SOCIAL_DATA1 = " (post_id, parent_id, source_id, post_type_id, post_datetime, date_key, hour_key, " + "post_text, headline, breaking_news, comment_count, likes_count, share_count, retweet_count, author_id, author_name, post_url, user_url, is_updated, type, source_type_id, shortname_id,author_outreach) " + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)";

	//public static final String INSERT_FORUM_SOCIAL_DATA = "INSERT INTO social_data(post_id, parent_id, source_id, post_type_id, post_datetime, date_key, hour_key, " + "post_text, headline,comment_count, likes_count, share_count, author_id, author_name, post_url,  is_updated, type, source_type_id, shortname_id) " + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	public static final String INSERT_FORUM_SOCIAL_DATA = "INSERT INTO ";
	public static final String INSERT_FORUM_SOCIAL_DATA1 = " (post_id, parent_id, source_id, post_type_id, post_datetime, date_key, hour_key, " + "post_text, headline,comment_count, likes_count, share_count, author_id, author_name, post_url,  is_updated, type, source_type_id, shortname_id) " + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

	//public static final String INSERT_FB_SOCIAL_DATA = "INSERT INTO social_data(post_id, parent_id, source_id, post_type_id, post_datetime, date_key, hour_key, post_text,comment_count, likes_count, share_count,author_id, author_name, post_url, user_url, is_updated, type, source_type_id, shortname_id,author_outreach,link,overall_sentiment,overall_sentiment_score,aspect_id,overall_sentiment_number,is_analyzed) " + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?,?,?,?)";
	public static final String INSERT_FB_SOCIAL_DATA = "INSERT INTO ";
	public static final String INSERT_FB_SOCIAL_DATA1 = " (post_id, parent_id, source_id, post_type_id, post_datetime, date_key, hour_key, post_text,comment_count, likes_count, share_count,author_id, author_name, post_url, user_url, is_updated, type, source_type_id, shortname_id,author_outreach,link,overall_sentiment,overall_sentiment_score,aspect_id,overall_sentiment_number,is_analyzed) " + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?,?,?,?)";

	//public static final String INSERT_TWITTER_SOCIAL_DATA = "INSERT INTO social_data(post_id, parent_id, source_id, post_type_id, post_datetime, date_key, hour_key, post_text,likes_count, retweet_count,author_id, author_name, post_url, user_url, is_updated, type, source_type_id, shortname_id,author_outreach,is_mention,link,overall_sentiment,overall_sentiment_score,aspect_id,overall_sentiment_number,is_analyzed) VALUES (?,?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?,?,?,?)";
	public static final String INSERT_TWITTER_SOCIAL_DATA = "INSERT INTO ";
	public static final String INSERT_TWITTER_SOCIAL_DATA1 = " (post_id, parent_id, source_id, post_type_id, post_datetime, date_key, hour_key, post_text,likes_count, retweet_count,author_id, author_name, post_url, user_url, is_updated, type, source_type_id, shortname_id,author_outreach,is_mention,link,overall_sentiment,overall_sentiment_score,aspect_id,overall_sentiment_number,is_analyzed) VALUES (?,?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?,?,?,?)";

	public static final String INSERT_RSS_SOCIAL_DATA = "INSERT INTO social_data(post_id, parent_id, source_id, post_type_id, post_datetime, date_key, hour_key, " + "post_text, headline, breaking_news, author_id, author_name, post_url, user_url, is_updated, type, source_type_id, shortname_id,overall_sentiment,overall_sentiment_score,aspect_id,overall_sentiment_number,is_analyzed) " + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?,?)";

	public static final String INSERT_RSS_ARCHIVE = "INSERT INTO rss_archive(post_id, source_id,post_datetime, date_key, hour_key, " + "post_text, headline, author_id, author_name) " + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

	/*public static final String INSERT_XPRESSO_DATA = "INSERT INTO xpresso_output(source_id, post_id, aspect, sentiment, entity, statement_type, " + 
											"emotion, snippet, intent, intent_classification, overall_sentiment, overall_sentiment_score, xpresso_domain) " + 
											"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";*/
	//public static final String INSERT_XPRESSO_DATA = "INSERT INTO xpresso_output(source_id, post_id, aspect, sentiment, entity, statement_type, " + "emotion, snippet, intent, intent_classification, xpresso_domain,aspect_id,sentiment_number) " + "VALUES (?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	public static final String INSERT_XPRESSO_DATA = "INSERT INTO ";
	public static final String INSERT_XPRESSO_DATA1 = " (source_id, post_id, aspect, sentiment, entity, statement_type, " + "emotion, snippet, intent, intent_classification, xpresso_domain,aspect_id,sentiment_number) " + "VALUES (?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

	public static final String SELECT_POST_TYPE_ID = "SELECT post_type_id FROM post_types WHERE source_type_id = ? AND post_type = ?";

	public static final String SELECT_POST_TYPE_MAP = "SELECT post_type , post_type_id FROM post_types";

	public static final String SELECT_PAGE_ID = "SELECT page_id FROM page_info WHERE source_id = ?";

	public static final String SELECT_TWITTER_RSS_ID = "SELECT DISTINCT rs.social_source_id,sm.source_url FROM rss_mapping AS rs ,source_master AS sm,source_types AS st  WHERE rs.social_source_id= sm.source_id AND sm.source_type_id=st.source_type_id AND st.source_type_code=?";

	public static final String SELECT_INSIGHT_ID = "SELECT insight_id FROM insight_types WHERE insight_type_name = ?";

	public static final String SELECT_USER_TOKEN = "SELECT token_id,app_key FROM user_token_master WHERE status = 0 ORDER BY token_id LIMIT 1";

	public static final String SELECT_USER_TOKEN_DB2 = "SELECT token_id,app_key FROM user_token_master WHERE status = 0 ORDER BY token_id FETCH FIRST 1 ROWS ONLY";

	public static final String INSERT_TRENDING_DATA = "INSERT INTO trending_topics(topic, category, score) VALUES (?, ?, ?)";

	public static final String DELETE_TRENDING_TOPIC = "DELETE FROM trending_topics";

	public static final String INSERT_KPI_INSIGHTS = "INSERT INTO kpi_page_insights(source_id, page_id, insight_id, date_key, hour_key, country, count_by_country)" + "VALUES (?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE count_by_country = ?";

	public static final String INSERT_KPI_PAGE_LEVEL = "INSERT INTO kpi_page_level(source_id, page_id, date_key, hour_key, page_like, talking_about_count, were_here, like_by_country, page_storyteller_by_country, category, total_followings, total_followers) " + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE page_like = ?, talking_about_count = ?, were_here = ?, like_by_country = ?, page_storyteller_by_country = ?, category = ?, total_followings = ?, total_followers = ?";

	public static final String INSERT_KPI_PAGE_LEVEL_DB2 = "MERGE INTO kpi_page_level AS T " + "USING (VALUES(?, CAST(? AS VARCHAR(100)), CAST(? AS DATE), CAST(? AS TIME), ?, ?, ?, ?, ?, CAST(? AS VARCHAR(50)), ?, ?)) AS DAT(source_id, page_id, date_key, hour_key, page_like, talking_about_count, were_here, like_by_country, page_storyteller_by_country, category, total_followings, total_followers) " + "ON T.source_id = DAT.source_id AND T.page_id = CAST(DAT.page_id AS VARCHAR(100)) AND T.date_key = CAST(DAT.date_key AS DATE) AND T.hour_key = CAST(DAT.hour_key AS TIME) " + "WHEN MATCHED THEN UPDATE SET T.page_like = DAT.page_like, T.talking_about_count = DAT.talking_about_count, T.were_here = DAT.were_here, T.like_by_country = DAT.like_by_country, T.page_storyteller_by_country = DAT.page_storyteller_by_country, T.category = CAST(DAT.category AS VARCHAR(50)), T.total_followings = DAT.total_followings, T.total_followers = DAT.total_followers " + "WHEN NOT MATCHED THEN INSERT (source_id, page_id, date_key, hour_key, page_like, talking_about_count, were_here, like_by_country, page_storyteller_by_country, category, total_followings, total_followers) VALUES (DAT.source_id, CAST(DAT.page_id AS VARCHAR(100)), CAST(DAT.date_key AS DATE), CAST(DAT.hour_key AS TIME), DAT.page_like, DAT.talking_about_count, DAT.were_here, DAT.like_by_country, DAT.page_storyteller_by_country, CAST(DAT.category AS VARCHAR(50)), DAT.total_followings, DAT.total_followers)";

	public static final String INSERT_KPI_POST_LEVEL = "INSERT INTO kpi_post_level(post_id,hash_id, source_id, post_type_id, date_key, hour_key, comments_count, likes_count, share_count, reply_count, none_count, love_count, haha_count, wow_count, sad_count, angry_count, thankful_count, retweet_count, user_loc, tweet_loc, possibly_sensitive) " + "VALUES (?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) " + "ON DUPLICATE KEY UPDATE comments_count = ?, likes_count = ?, share_count = ?, reply_count = ?, none_count = ?, love_count =? , haha_count = ?, wow_count = ?, sad_count = ?, angry_count = ?, thankful_count = ?, retweet_count = ?, user_loc = ?, tweet_loc = ?, possibly_sensitive = ?";

	//public static final String INSERT_KPI_FB_POST_LEVEL = "INSERT INTO kpi_post_level_fb(post_id,hash_id, source_id, post_type_id, date_key, hour_key, comments_count, likes_count, share_count, reply_count, none_count, love_count, haha_count, wow_count, sad_count, angry_count, thankful_count) " + "VALUES (?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) " + "ON DUPLICATE KEY UPDATE comments_count = ?, likes_count = ?, share_count = ?, reply_count = ?, none_count = ?, love_count =? , haha_count = ?, wow_count = ?, sad_count = ?, angry_count = ?, thankful_count = ?";
	public static final String INSERT_KPI_FB_POST_LEVEL = "INSERT INTO ";
	public static final String INSERT_KPI_FB_POST_LEVEL1 = " (post_id,hash_id, source_id, post_type_id, date_key, hour_key, comments_count, likes_count, share_count, reply_count, none_count, love_count, haha_count, wow_count, sad_count, angry_count, thankful_count) " + "VALUES (?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) " + "ON DUPLICATE KEY UPDATE comments_count = ?, likes_count = ?, share_count = ?, reply_count = ?, none_count = ?, love_count =? , haha_count = ?, wow_count = ?, sad_count = ?, angry_count = ?, thankful_count = ?";

	public static final String INSERT_KPI_FB_POST_LEVEL_DB2 = "MERGE INTO kpi_post_level_fb AS T " + "USING (VALUES(CAST(? AS VARCHAR(100)), CAST(? AS VARCHAR(100)), ?, ?, CAST(? AS DATE), CAST(? AS TIME), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)) AS DAT(post_id, hash_id, source_id, post_type_id, date_key, hour_key, comments_count, likes_count, share_count, reply_count, none_count, love_count, haha_count, wow_count, sad_count, angry_count, thankful_count) " + "ON T.post_id = CAST(DAT.post_id AS VARCHAR(100)) AND T.source_id = DAT.source_id AND T.date_key = CAST(DAT.date_key AS DATE) AND T.hour_key = CAST(DAT.hour_key AS TIME) " + "WHEN MATCHED THEN UPDATE SET T.comments_count = DAT.comments_count, T.likes_count = DAT.likes_count, T.share_count = DAT.share_count, T.reply_count = DAT.reply_count, T.none_count = DAT.none_count, T.love_count = DAT.love_count , T.haha_count = DAT.haha_count, T.wow_count = DAT.wow_count, T.sad_count = DAT.sad_count, T.angry_count = DAT.angry_count, T.thankful_count = DAT.thankful_count " + "WHEN NOT MATCHED THEN INSERT (post_id, hash_id, source_id, post_type_id, date_key, hour_key, comments_count, likes_count, share_count, reply_count, none_count, love_count, haha_count, wow_count, sad_count, angry_count, thankful_count) VALUES (CAST(DAT.post_id AS VARCHAR(100)), CAST(DAT.hash_id AS VARCHAR(100)), DAT.source_id, DAT.post_type_id, CAST(DAT.date_key AS DATE), CAST(DAT.hour_key AS TIME), DAT.comments_count, DAT.likes_count, DAT.share_count, DAT.reply_count, DAT.none_count, DAT.love_count, DAT.haha_count, DAT.wow_count, DAT.sad_count, DAT.angry_count, DAT.thankful_count) ";

	//public static final String INSERT_KPI_TWITTER_POST_LEVEL = "INSERT INTO kpi_post_level_tw(post_id,hash_id, source_id, post_type_id, date_key, hour_key, likes_count, retweet_count, user_loc, tweet_loc, possibly_sensitive) " + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) " + "ON DUPLICATE KEY UPDATE  likes_count = ? , retweet_count = ?, user_loc = ?, tweet_loc = ?, possibly_sensitive = ?";
	public static final String INSERT_KPI_TWITTER_POST_LEVEL = "INSERT INTO ";
	public static final String INSERT_KPI_TWITTER_POST_LEVEL1 = " (post_id,hash_id, source_id, post_type_id, date_key, hour_key, likes_count, retweet_count, user_loc, tweet_loc, possibly_sensitive) " + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) " + "ON DUPLICATE KEY UPDATE  likes_count = ? , retweet_count = ?, user_loc = ?, tweet_loc = ?, possibly_sensitive = ?";

	public static final String INSERT_KPI_TWITTER_POST_LEVEL_DB2 = "MERGE INTO kpi_post_level_tw AS T " + "USING (VALUES(CAST(? AS VARCHAR(100)), CAST(? AS VARCHAR(100)), ?, ?, CAST(? AS DATE), CAST(? AS TIME), ?, ?, CAST(? AS VARCHAR(50)), CAST(? AS VARCHAR(50)), ?)) AS DAT(post_id, hash_id, source_id, post_type_id, date_key, hour_key, likes_count, retweet_count, user_loc, tweet_loc, possibly_sensitive) " + "ON T.post_id = CAST(DAT.post_id AS VARCHAR(100)) AND T.source_id = DAT.source_id AND T.date_key = CAST(DAT.date_key AS DATE) AND T.hour_key = CAST(DAT.hour_key AS TIME) " + "WHEN MATCHED THEN UPDATE SET T.likes_count = DAT.likes_count, T.retweet_count = DAT.retweet_count, T.user_loc = CAST(DAT.user_loc AS VARCHAR(50)), T.tweet_loc = CAST(DAT.tweet_loc AS VARCHAR(50)), T.possibly_sensitive = DAT.possibly_sensitive " + "WHEN NOT MATCHED THEN INSERT (post_id, hash_id, source_id, post_type_id, date_key, hour_key, likes_count, retweet_count, user_loc, tweet_loc, possibly_sensitive) VALUES (CAST(DAT.post_id AS VARCHAR(100)), CAST(DAT.hash_id AS VARCHAR(100)), DAT.source_id, DAT.post_type_id, CAST(DAT.date_key AS DATE), CAST(DAT.hour_key AS TIME), DAT.likes_count, DAT.retweet_count, CAST(DAT.user_loc AS VARCHAR(50)), CAST(DAT.tweet_loc AS VARCHAR(50)), DAT.possibly_sensitive)";

	public static final String INSERT_KPI_FORUM_POST_LEVEL = "INSERT INTO kpi_post_level_fo(post_id,hash_id, source_id, post_type_id, date_key, hour_key, likes_count, replies_count, views_count) " + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?) " + "ON DUPLICATE KEY UPDATE likes_count = ?, replies_count = ?, views_count = ?";

	public static final String INSERT_KPI_FORUM_POST_LEVEL_DB2 = "MERGE INTO kpi_post_level_fo AS T USING (VALUES(CAST(? AS VARCHAR(100)), CAST(? AS VARCHAR(100)), ?, ?, CAST(? AS DATE), CAST(? AS TIME), ?, ?, ?)) AS DAT(post_id, hash_id, source_id, post_type_id, date_key, hour_key, likes_count, replies_count, views_count) ON T.post_id = CAST(DAT.post_id AS VARCHAR(100)) AND T.source_id = DAT.source_id AND T.date_key = CAST(DAT.date_key AS DATE) AND T.hour_key = CAST(DAT.hour_key AS TIME) WHEN MATCHED THEN UPDATE SET T.likes_count = DAT.likes_count, T.replies_count = DAT.replies_count, T.views_count = DAT.views_count WHEN NOT MATCHED THEN INSERT (post_id, hash_id, source_id, post_type_id, date_key, hour_key, likes_count, replies_count, views_count) VALUES (CAST(DAT.post_id AS VARCHAR(100)), CAST(DAT.hash_id AS VARCHAR(100)), DAT.source_id, DAT.post_type_id, CAST(DAT.date_key AS DATE), CAST(DAT.hour_key AS TIME), DAT.likes_count, DAT.replies_count, DAT.views_count)";

	//public static final String SELECT_POST_URL = "SELECT post_url FROM social_data WHERE post_id = ? AND source_id = ? DESC LIMIT 1";
	public static final String SELECT_POST_URL = "SELECT post_url FROM ";
	public static final String WHERE_POST_URL = " WHERE post_id = ? AND source_id = ? DESC LIMIT 1";

	public static final String SELECT_POST_URL_DB2 = "SELECT post_url FROM social_data WHERE post_id = ? AND source_id = ?  DESC FETCH FIRST 1 ROWS ONLY";

	public static final String INSERT_ALERT = "INSERT INTO alert_master (subject, alert_category_id, source_id, date_hh, description, alert_type, priority, is_active, author) VALUES" + "(?, ?, ?, ?, ?, ?, ?, ?, ?)";

	public static final String FACEBOOK_URL = "https://www.facebook.com/";

	public static final String POSTS = "/posts/";

	public static final String TWITTER_URL = "https://twitter.com/";
	public static final String STATUS = "/status/";

	public static final String PROPERTIES_PATH = "./config.properties";

	public static final String ZOMATO_API_RESTAURANT_URL = "https://developers.zomato.com/api/v2.1/restaurant?res_id=";
	public static final String ZOMATO_API_REVIEW_URL = "https://developers.zomato.com/api/v2.1/reviews?count=1000&res_id=";
	public static final String ZOMATO_REVIEW_URL = "https://www.zomato.com/review/";
	public static final String HEADER_USER_KEY = "user-key";
	public static final String HEADER_CONTENT_TYPE = "Content-Type";
	public static final String CONTENT_TYPE = "application/json";
	public static final String HEADER_USER_AGENT = "User-Agent";
	//public static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0)";
	public static final String USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.152 Safari/537.36";
	public static final String GPLUS_PAGE_PREFIX = "https://www.googleapis.com/plus/v1/people/";
	public static final String GPLUS_PAGE_SUFFIX = "?fields=circledByCount,displayName,id,kind,objectType,plusOneCount,url&key=";

	public static final String GPLUS_POST_SUFFIX = "/activities/public?fields=id,items(actor(displayName,id),id,kind,object(content,id,objectType,originalContent,plusoners/totalItems,replies/totalItems,resharers/totalItems),published,title,updated,url,verb),kind,nextLink,nextPageToken,title,updated&key=";

	public static final String GPLUS_ACTIVITIES_PREFIX = "https://www.googleapis.com/plus/v1/activities/";
	public static final String GPLUS_ACTIVITIES_SUFFIX = "/comments?fields=id,items(actor(displayName,id),id,kind,object,plusoners,published,selfLink,updated,verb),kind,nextLink,nextPageToken,title,updated&key=";

	public static final String SELECT_DISTINCT_AUTHORID = "SELECT DISTINCT author_id from social_data WHERE source_id=? AND author_id <> IFNULL((SELECT page_id from page_info WHERE source_id = ?),'') AND create_datetime BETWEEN DATE_ADD(NOW(),INTERVAL -? DAY) AND NOW() ";

	public static final String SELECT_DISTINCT_AUTHORID_DB2 = "SELECT DISTINCT AUTHOR_ID from SOCIAL_DATA WHERE SOURCE_ID=? AND AUTHOR_ID <> COALESCE((SELECT PAGE_ID from PAGE_INFO WHERE SOURCE_ID = ?),'') AND create_datetime BETWEEN (CURRENT TIMESTAMP - ? DAY) AND CURRENT TIMESTAMP";

	public static final String SELECT_DISTINCT_PAGEID = "SELECT DISTINCT page_id from page_info WHERE source_id=? ";

	public static final String INSERT_USER_PROFILE = "INSERT INTO user_profile( source_id,author_id,author_name,email,date_key,hour_key,max_time,total_followings,total_follower,mention_count,tweet_count,favorite_count) " + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";

	public static final String SELECT_MAX_TIME_RECORD_USER_PROFILE = " SELECT count(*) AS record from user_profile WHERE source_id=? AND author_id=? AND max_time=? AND DAY(date_key)=? and MONTH(date_key)=? and YEAR(date_key)=? ";

	public static final String SELECT_MAX_TIME_RECORD_USER_SCORE_PROFILING = " SELECT count(*) AS record from user_score_profiling WHERE source_id=? AND author_id=? AND max_time=? AND DAY(date_key)=? and MONTH(date_key)=? and YEAR(date_key)=? ";

	public static final String SELECT_MIN_HOUR_KEY_USER_PROFILE = "SELECT MIN(hour_key) as hour FROM user_profile WHERE source_id=? AND author_id=? AND max_time=? AND DAY(date_key)=? and MONTH(date_key)=? and YEAR(date_key)=?";

	public static final String SELECT_MIN_HOUR_KEY_USER_SCORE_PROFILING = "SELECT MIN(hour_key) as hour FROM user_score_profiling WHERE source_id=? AND author_id=? AND max_time=? AND DAY(date_key)=? and MONTH(date_key)=? and YEAR(date_key)=?";

	public static final String UPDATE_MIN_HOUR_RECORD_USER_PROFILE = "UPDATE user_profile SET max_time =? WHERE source_id= ? AND author_id=? AND max_time=? AND hour_key = ? AND DAY(date_key)=? and MONTH(date_key)=? and YEAR(date_key)=?";

	public static final String UPDATE_MIN_HOUR_RECORD_USER_SCORE_PROFILING = "UPDATE user_score_profiling SET max_time =? WHERE source_id= ? AND author_id=? AND max_time=? AND hour_key = ? AND DAY(date_key)=? and MONTH(date_key)=? and YEAR(date_key)=?";

	public static final String SELECT_PREVIOUS_USER_PROFILE_RECORD = "SELECT *  FROM user_profile WHERE source_id= ? AND author_id=? AND max_time=? AND DAY(date_key)=? and MONTH(date_key)=? and YEAR(date_key)=? ";

	public static final String SELECT_COUNT_CURRENT_USER_PROFILE = "SELECT COUNT(*) AS record FROM user_profile WHERE DAY(date_key)=? and MONTH(date_key)=? and YEAR(date_key)=? AND author_id= ? AND source_id =?";

	public static final String INSERT_USER_SCORE_PROFILING = "INSERT INTO user_score_profiling( source_id,author_id,author_name,date_key,hour_key,max_time,user_activity,user_popularity,user_score) " + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?) ";

	public static List<Map.Entry<String, Integer>> sortByValue(Map<String, Integer> unsortMap) {
		// 1. Convert Map to List of Map
		List<Map.Entry<String, Integer>> list = new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());

		// 2. Sort list with Collections.sort(), provide a custom Comparator
		//    Try switch the o1 o2 position for a different order
		Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
			public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
				return (o2.getValue()).compareTo(o1.getValue());
			}
		});

		/*
		// classic iterator example
		for (Iterator<Map.Entry<String, Integer>> it = list.iterator(); it.hasNext(); ) {
		    Map.Entry<String, Integer> entry = it.next();
		    sortedMap.put(entry.getKey(), entry.getValue());
		}
		
		// 3. Loop the sorted list and put it into a new insertion order Map LinkedHashMap
		Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
		for (Map.Entry<String, Integer> entry : list) {
		    sortedMap.put(entry.getKey(), entry.getValue());
		}*/

		// return sortedMap;
		return list;
	}

	public static int getSentimentNumber(String sentiment) {

		int val = 0;

		switch (sentiment) {
			case XpressoKeys.POSITIVE:
				val = XpressoKeys.POSITIVE_NUMBER;
				break;
			case XpressoKeys.NEGATIVE:
				val = XpressoKeys.NEGATIVE_NUMBER;
				break;
			case XpressoKeys.NEUTRAL:
				val = XpressoKeys.NEUTRAL_NUMBER;
				break;

		}

		return val;
	}

	public static boolean getRootNodeKeyName(String key) {
		boolean flag = true;

		switch (key) {
			case XpressoKeys.DOMAIN:
				flag = false;
				break;

			case XpressoKeys.VERSION:
				flag = false;
				break;
			case XpressoKeys.ENTITY_LIST:
				flag = false;
				break;
			case XpressoKeys.REVIEW_SENTIMENT:
				flag = false;
				break;
			case XpressoKeys.TRENDING_TOPICS:
				flag = false;
				break;
		}

		return flag;
	}
	public static final String SELECT_COMPANY_ID = " select company_id from company_source_map where source_id  = ?";
	public static final String SELECT_SOURCE_NAME = "select st.source_type_name from source_types st,source_master sm where st.source_type_id = sm.source_type_id and sm.source_id=?";
	public static final String SOCIAL_DATA = " social_data_";
	public static final String XPRESSO_OUTPUT = " xpresso_output_";
	public static final String KPI_POST_LEVEL_FB = " kpi_post_level_fb_";
	public static final String KPI_POST_LEVEL_TW = " kpi_post_level_tw_";
}
