package com.abzooba.sourcing.common.util;

public class XpressoKeys {

	public static final String DOMAIN = "Domain";
	public static final String EXPR = "Expr";
	public static final String ABOUT = "About";
	public static final String OVERALL = "Overall";
	public static final String REVIEW_SENTIMENT = "Review Sentiment";
	public static final String TRENDING_TOPICS = "trending_topics";
	public static final String TOPIC = "topic";
	public static final String SCORE = "score";
	public static final String ASPECT = "Aspect";
	public static final String ENTITY = "Entity";
	public static final String SENTIMENT = "Sentiment";
	public static final String STATEMENT_TYPE = "StatementType";
	public static final String INDICATIVE_SNIPPET = "Indicative Snippet";
	public static final String NAMED_ENTITIES = "Named Entities";
	public static final String ENTITY_LIST = "entity_list";
	public static final String INTENT = "Intent";
	public static final String INTENT_CLASSIFICATION = "Intent Classification";
	public static final String EMOTION = "Emotion";
	public static final String ENTITIES = "entities";
	public static final String VERSION = "Version";

	public static final String POSITIVE = "Positive";
	public static final String NEGATIVE = "Negative";
	public static final String NEUTRAL = "Neutral";

	public static final int POSITIVE_NUMBER = 1;
	public static final int NEGATIVE_NUMBER = -1;
	public static final int NEUTRAL_NUMBER = 0;

}
