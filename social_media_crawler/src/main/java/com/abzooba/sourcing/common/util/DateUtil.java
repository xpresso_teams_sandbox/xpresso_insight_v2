package com.abzooba.sourcing.common.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class DateUtil {

	public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
		long diffInMillies = 0;
		if (date1 != null) {
			diffInMillies = date2.getTime() - date1.getTime();
		} else {
			diffInMillies = date2.getTime();
		}

		return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
	}

	static String[] dateFormats = { "dd-MM-yyyy HH:mm a", "yyyy-MM-dd HH:mm:ss", "EEE, dd MMM yyyy HH:mm:ss zzz", "EEE, dd MMM yyyy HH:mm:ss Z", "EEE, dd MMM yyyy HH:mm:ss z", "dd MMM yyyy HH:mm", "yyyy-MM-dd", "yyyyy/MM/dd", "dd/MM/yyyyy", "dd-MM-yyyy", "yyyy MMM dd", "yyyy dd MMM", "dd MMM yyyy", "dd MMM", "MMM dd", "dd MMM yyyy", "M/y", "M/d/y", "M-d-y" };

	public static Date convertPubdate(String date) throws ParseException {

		for (String formatString : dateFormats) {
			try {
				SimpleDateFormat sdf = new SimpleDateFormat(formatString);
				//sdf.setTimeZone(TimeZone.getTimeZone(prop.getProperty("timezone", "GMT")));
				return sdf.parse(date);
			} catch (ParseException e) {
				continue;
			}
		}
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormats[1]);
		try {
			return sdf.parse(date.replaceAll("Z$", "+0000"));
		} catch (ParseException e) {

		}
		return null;
	}

	public static Date getBeforeOrAfterDate(int nDays) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.DAY_OF_YEAR, nDays);

		return calendar.getTime();
	}

	public static Date getBeforeOrAfterDateProvided(Date date, int nDays) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_YEAR, nDays);

		return calendar.getTime();
	}

	public static void main(String[] args) {
		try {
			Properties prop = new Properties();
			prop.load(new FileInputStream("./config.properties"));
			Date dt = convertPubdate("Thu, 18 Aug 2016 17:28:55 +0000");
			SimpleDateFormat _format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			_format.setTimeZone(TimeZone.getTimeZone("Asia/Singapore"));
			System.out.println("RSSFeedProcessBolt: new pub date " + _format.format(dt));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = getBeforeOrAfterDate(-7);
		System.out.println("7 days after : " + sdf.format(getBeforeOrAfterDateProvided(date, 7)));

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(getBeforeOrAfterDate(-7));
		long diff = getDateDiff(calendar.getTime(), new Date(), TimeUnit.DAYS);
		System.out.println(diff);
	}

}
