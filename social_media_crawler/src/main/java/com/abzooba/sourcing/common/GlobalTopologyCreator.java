package com.abzooba.sourcing.common;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Map;
import java.util.Properties;

/*import backtype.storm.Config;
import backtype.storm.StormSubmitter;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.utils.NimbusClient;
import backtype.storm.utils.Utils;*/
import org.apache.storm.Config;
import org.apache.storm.StormSubmitter;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.utils.NimbusClient;
import org.apache.storm.utils.Utils;
import org.json.simple.JSONValue;

public class GlobalTopologyCreator {

	public static Properties _prop = new Properties();

	public static void main(String[] args) {
		System.out.println("Inside Main Function");
		try {
			//_prop.load(new FileInputStream(Constants.PROPERTIES_PATH));
			_prop.load(new FileInputStream(System.getProperty("propfile")));
			//_prop.load(new FileInputStream(Constants.PROPERTIES_PATH));

			System.out.println("Is this getting printed");
			//_prop.load(GlobalTopologyCreator.class.getClassLoader().getResourceAsStream("config.properties"));
			System.out.println("Properties file loaded successfully....");
			//Util.prop=_prop;
			//Util.init(_prop);
			/*FacebookUtil.init(_prop);
			TwitterUtil.init(_prop);
			GPlusUtil.init(_prop);
			ZomatoUtil.init(_prop);
			System.out.println("Properties file loaded successfully for all Util Classes....");*/

			/*Util ut = new Util();
			ut.init(_prop);
			System.out.println("Init Called");*/
			// ################## Facebook ################## //
			/*FacebookTopologyCreator fb = new FacebookTopologyCreator();
			TopologyBuilder builder1 = fb.topologyBuilder(_prop);
			Map topologyConf1 = fb.topologyConfBuilder(_prop);
			String jsonConf1 = JSONValue.toJSONString(topologyConf1);*/

			// ################## Twitter ################## //
			/*TwitterTopologyCreator twitter = new TwitterTopologyCreator();
			TopologyBuilder builder2 = twitter.topologyBuilder(_prop);
			Map topologyConf2 = twitter.topologyConfBuilder(_prop);
			String jsonConf2 = JSONValue.toJSONString(topologyConf2);*/

			// ##################### RSS ##################### //
			/*RSSTopologyCreator rss = new RSSTopologyCreator();
			TopologyBuilder builder3 = rss.topologyBuilder(_prop);
			Map topologyConf3 = rss.topologyConfBuilder(_prop);
			String jsonConf3 = JSONValue.toJSONString(topologyConf3);*/

			// ##################### FacebookComment ##################### //
			/*FacebookCommentTopologyCreator fbc = new FacebookCommentTopologyCreator();
			TopologyBuilder builder4 = fbc.topologyBuilder(_prop);
			Map topologyConf4 = fbc.topologyConfBuilder(_prop);
			String jsonConf4 = JSONValue.toJSONString(topologyConf4);*/

			// ##################### Forum ##################### //
			/*ForumTopologyCreator forum = new ForumTopologyCreator();
			TopologyBuilder builder5 = forum.topologyBuilder(_prop);
			Map topologyConf5 = forum.topologyConfBuilder(_prop);
			String jsonConf5 = JSONValue.toJSONString(topologyConf5);*/

			// ##################### Zomato ##################### //
			/*ZomatoTopologyCreator zomato = new ZomatoTopologyCreator();
			TopologyBuilder builder6 = zomato.topologyBuilder(_prop);
			Map topologyConf6 = zomato.topologyConfBuilder(_prop);
			String jsonConf6 = JSONValue.toJSONString(topologyConf6);*/

			// ##################### Facebook Reply ##################### //
			/*FacebookReplyTopologyCreator fbr = new FacebookReplyTopologyCreator();
			TopologyBuilder builder7 = fbr.topologyBuilder(_prop);
			Map topologyConf7 = fbr.topologyConfBuilder(_prop);
			String jsonConf7 = JSONValue.toJSONString(topologyConf6);*/

			Map stormConf = Utils.readStormConfig();
			stormConf.put(Config.NIMBUS_HOST, _prop.getProperty("nimbus_ip").trim());

			// ================== Submit topology from local to remote cluster ====================
			String inputJar = _prop.getProperty("jar_location").trim();
			String uploadedJarLocation = StormSubmitter.submitJar(stormConf, inputJar);
			System.out.println("Topology Jar uploaded successfully...");

			NimbusClient nimbus = new NimbusClient(stormConf, _prop.getProperty("nimbus_ip").trim(), 6627);

			/*if(builder1 != null) {
				nimbus.getClient().submitTopology("fbtopology", uploadedJarLocation, jsonConf1, builder1.createTopology());
				System.out.println("Facebook Topology submitted");
			}
			if(builder2 != null) {
				nimbus.getClient().submitTopology("twittertopology", uploadedJarLocation, jsonConf2, builder2.createTopology());
				System.out.println("Twitter Topology submitted");
			}
			if(builder3 != null) {
				nimbus.getClient().submitTopology("rsstopology", uploadedJarLocation, jsonConf3, builder3.createTopology());
				System.out.println("RSS Topology submitted");
			}
			if(builder4 != null) {
				nimbus.getClient().submitTopology("fbctopology", uploadedJarLocation, jsonConf4, builder4.createTopology());
				System.out.println("Facebook Comment Topology submitted");
			}
			if(builder5 != null) {
				nimbus.getClient().submitTopology("forumtopology", uploadedJarLocation, jsonConf5, builder5.createTopology());
				System.out.println("Forum Topology submitted");
			}
			if(builder6 != null) {
				nimbus.getClient().submitTopology("zomatotopology", uploadedJarLocation, jsonConf6, builder6.createTopology());
				System.out.println("Zomato Topology submitted");
			}*/

			String[] topologyList = _prop.getProperty("topology_list").trim().split(",");
			System.out.println("topologyList >>> " + Arrays.toString(topologyList));

			for (String topology : topologyList) {
				String topologyName = topology.split("\\.")[4].replace("Creator", "").toLowerCase();
				System.out.println(topology + " >>> " + topologyName);
				ITopologyBuilder topologyCreator = (ITopologyBuilder) Class.forName(topology).newInstance();
				TopologyBuilder builder = topologyCreator.topologyBuilder(_prop);
				Map topologyConf = topologyCreator.topologyConfBuilder(_prop);
				String jsonConf = JSONValue.toJSONString(topologyConf);
				//System.out.println("jsonConf >>> " + jsonConf);

				if (builder != null) {
					nimbus.getClient().submitTopology(topologyName, uploadedJarLocation, jsonConf, builder.createTopology());
					System.out.println(topologyName + " submitted");
					// 2 minutes sleep before submitting another Topology
					Thread.sleep(1000 * 60 * 2);
				}
			}
			/*if(builder7 != null) {
				nimbus.getClient().submitTopology("fbrtopology", uploadedJarLocation, jsonConf7, builder7.createTopology());
				System.out.println("FacebookReply Topology submitted");
			}*/

			// ================== Submit jar in remote cluster from command line ================================
			/*StormSubmitter.submitTopologyWithProgressBar("fbtopology", storm_conf1, builder1.createTopology());
			StormSubmitter.submitTopologyWithProgressBar("twittertopology", storm_conf1, builder2.createTopology());
			StormSubmitter.submitTopologyWithProgressBar("rsstopology", storm_conf1, builder3.createTopology());*/
			System.out.println("All topology submitted successfully...");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
