package com.abzooba.sourcing.common.bean;

import java.io.Serializable;

/**
 * The Class AlertDTO.
 */
public class AlertDTO implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The id. */
	private int id;
	
	/** The subject. */
	private String subject;
	
	/** The alert category id. */
	private int alertCategoryId;
	
	/** The post id. */
	private int sourceId;
	
	private String dateHH;
	
	/** The description. */
	private String description;
	
	/** The alert type. */
	private String alertType;
	
	/** The priority. */
	private int priority;
	
	/** The active. */
	private boolean active;
	
	/** The author. */
	private String author;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * Gets the subject.
	 *
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}
	
	/**
	 * Sets the subject.
	 *
	 * @param subject the new subject
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	/**
	 * Gets the alert category id.
	 *
	 * @return the alert category id
	 */
	public int getAlertCategoryId() {
		return alertCategoryId;
	}
	
	/**
	 * Sets the alert category id.
	 *
	 * @param alertCategoryId the new alert category id
	 */
	public void setAlertCategoryId(int alertCategoryId) {
		this.alertCategoryId = alertCategoryId;
	}
	
	/**
	 * Gets the post id.
	 *
	 * @return the post id
	 */
	public int getSourceId() {
		return sourceId;
	}
	
	/**
	 * Sets the post id.
	 *
	 * @param postId the new post id
	 */
	public void setSourceId(int sourceId) {
		this.sourceId = sourceId;
	}
	
	public String getDateHH() {
		return dateHH;
	}

	public void setDateHH(String dateHH) {
		this.dateHH = dateHH;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * Gets the alert type.
	 *
	 * @return the alert type
	 */
	public String getAlertType() {
		return alertType;
	}
	
	/**
	 * Sets the alert type.
	 *
	 * @param alertType the new alert type
	 */
	public void setAlertType(String alertType) {
		this.alertType = alertType;
	}
	
	/**
	 * Gets the priority.
	 *
	 * @return the priority
	 */
	public int getPriority() {
		return priority;
	}
	
	/**
	 * Sets the priority.
	 *
	 * @param priority the new priority
	 */
	public void setPriority(int priority) {
		this.priority = priority;
	}
	
	/**
	 * Checks if is active.
	 *
	 * @return true, if is active
	 */
	public boolean isActive() {
		return active;
	}
	
	/**
	 * Sets the active.
	 *
	 * @param active the new active
	 */
	public void setActive(boolean active) {
		this.active = active;
	}
	
	/**
	 * Gets the author.
	 *
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}
	
	@Override
	public String toString() {
		return "AlertDTO [id=" + id + ", subject=" + subject
				+ ", alertCategoryId=" + alertCategoryId + ", sourceId="
				+ sourceId + ", dateHH=" + dateHH + ", description="
				+ description + ", alertType=" + alertType + ", priority="
				+ priority + ", active=" + active + ", author=" + author + "]";
	}

	/**
	 * Sets the author.
	 *
	 * @param author the new author
	 */
	public void setAuthor(String author) {
		this.author = author;
	}
	
}
