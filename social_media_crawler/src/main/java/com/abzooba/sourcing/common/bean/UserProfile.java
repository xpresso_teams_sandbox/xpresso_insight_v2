package com.abzooba.sourcing.common.bean;

import java.io.Serializable;
import java.sql.Date;

public class UserProfile implements Serializable {

	private static final long serialVersionUID = 1L;

	private int sourceId;
	private String authorName;
	private long authorID;
	private String emailID;
	private String dateKey;
	private String hourKey;
	private long tweetCount;
	private long favourites;
	private long mentions;
	private long retweets;
	private long totalFollowings = 0;
	private long totalFollowers = 0;
	private Date createDateTime;
	private Date updateDateTime;

	public String getEmailID() {
		return emailID;
	}

	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public long getAuthorID() {
		return authorID;
	}

	public void setAuthorID(long authorID) {
		this.authorID = authorID;
	}

	public int getSourceId() {
		return sourceId;
	}

	public void setSourceId(int sourceId) {
		this.sourceId = sourceId;
	}

	public String getDateKey() {
		return dateKey;
	}

	public void setDateKey(String dateKey) {
		this.dateKey = dateKey;
	}

	public String getHourKey() {
		return hourKey;
	}

	public void setHourKey(String hourKey) {
		this.hourKey = hourKey;
	}

	public long getTweetCount() {
		return tweetCount;
	}

	public void setTweetCount(long tweetCount) {
		this.tweetCount = tweetCount;
	}

	public long getFavourites() {
		return favourites;
	}

	public void setFavourites(long favourites) {
		this.favourites = favourites;
	}

	public long getMentions() {
		return mentions;
	}

	public void setMentions(long mentions) {
		this.mentions = mentions;
	}

	public long getRetweets() {
		return retweets;
	}

	public void setRetweets(long retweets) {
		this.retweets = retweets;
	}

	public long getTotalFollowings() {
		return totalFollowings;
	}

	public void setTotalFollowings(long totalFollowings) {
		this.totalFollowings = totalFollowings;
	}

	public long getTotalFollowers() {
		return totalFollowers;
	}

	public void setTotalFollowers(long totalFollowers) {
		this.totalFollowers = totalFollowers;
	}

	public Date getCreateDateTime() {
		return createDateTime;
	}

	public void setCreateDateTime(Date createDateTime) {
		this.createDateTime = createDateTime;
	}

	public Date getUpdateDateTime() {
		return updateDateTime;
	}

	public void setUpdateDateTime(Date updateDateTime) {
		this.updateDateTime = updateDateTime;
	}

}
