package com.abzooba.sourcing.common.bolt;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/*import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Tuple;*/
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Tuple;

import com.abzooba.sourcing.common.bean.PageInfo;
import com.abzooba.sourcing.common.bean.SocialData;

public class XpressoCallerBolt extends BaseRichBolt {

	private static final long serialVersionUID = 1L;
	protected OutputCollector _collector;

	private Properties _prop = new Properties();

	public XpressoCallerBolt() throws IOException, InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		//_prop.load(new FileInputStream(Constants.PROPERTIES_PATH));
		//_prop.load(new FileInputStream(System.getProperty("propfile")));
		_prop.load(XpressoCallerBolt.class.getClassLoader().getResourceAsStream("config.properties"));
	}

	public void execute(Tuple tuple) {
		PageInfo pageInfo = (PageInfo) tuple.getValueByField("pageInfo");
		Set<SocialData> socialDatas = (Set<SocialData>) tuple.getValueByField("socialDatas");

		try {

			/*int nSourceId = pageInfo.getSourceId();
			String strDomain = null;
			int nXpressoEnabled = 0;
			Map<String, Object> valueMap = DBService.getDomainAndXpressoStatus(nSourceId);
			if(valueMap != null && valueMap.size() > 0) {
				strDomain = (String)valueMap.get("domain");
				nXpressoEnabled = (Integer)valueMap.get("call_xpresso");
			} else
				throw new Exception("domain and xpresso calling status is not set in db or config file...");*/

			/*for (SocialData socialData : socialDatas) {
				if(socialData.getPostText() != null && !socialData.getPostText().equalsIgnoreCase(""))
					Util.initiateXpressoCall(_prop,socialData.getSourceId(), socialData.getPostId(), socialData.getParentId(), socialData.getPostText());					
			}*/

		} catch (Exception e) {
			_collector.fail(tuple);
			e.printStackTrace();
		}

		_collector.ack(tuple);
	}

	public void declareOutputFields(OutputFieldsDeclarer declarer) {

	}

	public void prepare(Map map, TopologyContext context, OutputCollector collector) {
		_collector = collector;
	}

}
