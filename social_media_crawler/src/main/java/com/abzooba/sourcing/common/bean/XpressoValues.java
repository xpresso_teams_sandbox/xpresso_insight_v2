package com.abzooba.sourcing.common.bean;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.json.JSONArray;
import org.json.JSONObject;

import com.abzooba.sourcing.common.util.Constants;
import com.abzooba.sourcing.common.util.XpressoKeys;

public class XpressoValues implements Serializable {

	private static final long serialVersionUID = 1L;

	private String domain = null;
	private List<XpressoAbout> abouts = new ArrayList<XpressoAbout>();
	// private List<SentiValues> reviewSentiments = new ArrayList<SentiValues>();
	// private Map<String, Double> reviewSentiments =  new LinkedHashMap<String, Double>();
	private Map<String, Integer> aspectMap = new LinkedHashMap<String, Integer>();
	private Map<String, Double> trendingTopics = new LinkedHashMap<String, Double>();
	// private Map <String, Integer> trendingTopics = new LinkedHashMap<String, Integer>();
	private String xpressoJson = null;
	private String overallSentiment = "";
	private int overallSentiment_number = 0;
	private double overallSentimentScore = 0;
	private String overallAspect = "";
	private Map<String, String> namedEntities = null;
	private Map<String, Integer> entityMap = new LinkedHashMap<String, Integer>();
	private boolean parseStatus = false;

	private boolean intentClassification = false;

	public XpressoValues(String json) {
		xpressoJson = json;
	}

	public String getDomain() {
		return domain;
	}

	public List<XpressoAbout> getAbouts() {
		return abouts;
	}

	public Map<String, Double> getTrendingTopics() {
		return trendingTopics;
	}

	public String getXpressoJson() {
		return xpressoJson;
	}

	public String getOverallSentiment() {
		return overallSentiment;
	}

	public double getOverallSentimentScore() {
		return overallSentimentScore;
	}

	public int getOverallSentimentNumber() {
		return overallSentiment_number;
	}

	public void parseXpressoJson(String json, Properties prop) throws Exception {
		xpressoJson = json;
		parseXpressoJson(prop);
	}

	public Map<String, String> getNamedEntities() {
		return namedEntities;
	}

	public void setNamedEntities(Map<String, String> namedEntities) {
		this.namedEntities = namedEntities;
	}

	public boolean isIntentClassification() {
		return intentClassification;
	}

	public void setIntentClassification(boolean intentClassification) {
		this.intentClassification = intentClassification;
	}

	public String getOverallAspect() {
		return overallAspect;
	}

	public Map<String, Integer> getEntityList() {
		return entityMap;
	}

	public boolean isParseStatus() {
		return parseStatus;
	}

	@SuppressWarnings("unchecked")
	public void parseXpressoJson(Properties prop) throws Exception {

		if (xpressoJson != null && xpressoJson.trim().equalsIgnoreCase("{}") == false) {

			JSONObject jsonRoot = new org.json.JSONObject(xpressoJson);

			domain = jsonRoot.getString(XpressoKeys.DOMAIN);

			//JSONObject jsonExpr = jsonRoot.getJSONObject(XpressoKeys.EXPR);
			Iterator<String> roots = jsonRoot.keys();
			String review_key = "";
			while (roots.hasNext()) {
				String key = roots.next();
				//				 System.out.println("Key : " + key);
				if (Constants.getRootNodeKeyName(key)) {
					System.out.println("###" + key);
					review_key = key;

					// JSONObject jsonExpr = jsonRoot.getJSONObject(review_key);
					// Iterator<String> keys = jsonExpr.keys();
					// while(keys.hasNext()) {
					// String key = keys.next();
					// JSONObject aboutNode = jsonExpr.getJSONObject(key);
					// XpressoAbout about = new XpressoAbout();

					JSONObject jsonMain = jsonRoot.getJSONObject(review_key);
					JSONArray arrAbout = jsonMain.getJSONArray(XpressoKeys.ABOUT);
					int arrLen = arrAbout.length();
					//					System.out.println("review_key:" + review_key + " ## arrAbout size:" + arrLen);

					int aspectCount = 0;
					XpressoAbout aboutOverAll = null;
					for (int i = 0; i < arrLen; i++) {
						JSONObject obj = arrAbout.getJSONObject(i);
						XpressoAbout about = new XpressoAbout();
						about.parseXpressoAbout(obj);
						if (about.getAspect().equalsIgnoreCase(XpressoKeys.OVERALL)) {
							aboutOverAll = about;
						} else {
							aspectCount++;
						}

						abouts.add(about);
					}

					if (aspectCount > 0 && aboutOverAll != null) {
						abouts.remove(aboutOverAll);
					}

					// JSONObject objNamed = aboutNode.getJSONObject(XpressoKeys.NAMED_ENTITIES);
					try {
						JSONObject objNamed = jsonMain.getJSONObject(XpressoKeys.NAMED_ENTITIES);
						//					JSONObject objNamed = jsonRoot.getJSONObject(XpressoKeys.NAMED_ENTITIES);
						if (objNamed != null && objNamed.length() > 0) {
							namedEntities = new LinkedHashMap<String, String>();

							Iterator<String> keys1 = objNamed.keys();
							while (keys1.hasNext()) {
								String key1 = keys1.next();
								String value1 = objNamed.getString(key1);
								namedEntities.put(key1, value1);
							}
						}
					} catch (Exception e) {
						//System.out.println(XpressoKeys.NAMED_ENTITIES + " not found in Xpresso engine output json.");
					}

					// JSONObject objIntent = aboutNode.getJSONObject(XpressoKeys.INTENT);
					try {
						JSONObject objIntent = jsonMain.getJSONObject(XpressoKeys.INTENT);
						//					JSONObject objIntent = jsonRoot.getJSONObject(XpressoKeys.INTENT);
						if (objIntent != null && objIntent.length() > 0) {
							intentClassification = (boolean) objIntent.getBoolean(XpressoKeys.INTENT_CLASSIFICATION);
						}
					} catch (Exception e) {
						//System.out.println(XpressoKeys.INTENT + " not found in Xpresso engine output json.");
					}
				}
			}
			/**
			 * Review Sentiment	
			 */
			/*
			JSONObject jsonReviewSentiment = jsonRoot.getJSONObject(XpressoKeys.REVIEW_SENTIMENT);
			for (Object senti : jsonReviewSentiment.keySet()) {
				String sentiVal = senti.toString();  
				double score = jsonReviewSentiment.getDouble(sentiVal);
				
				//	SentiValues sentiValue = new SentiValues(senti.toString(), score);
				//	reviewSentiments.add(sentiValue);
				
				//	System.out.println(sentiVal + " = " + score);
				reviewSentiments.put(sentiVal, score);
			}*/

			/**
			 * Calculate Overall Sentiiment	
			 */
			calculateOverallSetiment();
			overallSentiment_number = Constants.getSentimentNumber(overallSentiment);

			/**
			 * Calculate Overall Aspect	
			 */
			List<Map.Entry<String, Integer>> sortedAspectList = Constants.sortByValue(aspectMap);

			/*	
			for (Map.Entry<String, Integer> entry : sortedAspectList) {
			    System.out.println(entry.getKey() + " = " + entry.getValue());
			}*/

			Map.Entry<String, Integer> topAspect1 = null;
			Map.Entry<String, Integer> topAspect2 = null;

			if (sortedAspectList.size() == 1) {
				topAspect1 = sortedAspectList.get(0);
				overallAspect = topAspect1.getKey();
			} else if (sortedAspectList.size() > 1) {
				topAspect1 = sortedAspectList.get(0);
				topAspect2 = sortedAspectList.get(1);

				//	System.out.println("Top1=" + topAspect1.toString());
				//	System.out.println("Top2=" + topAspect2.toString());

				overallAspect = topAspect1.getKey();

				if (topAspect1.getKey().equalsIgnoreCase(XpressoKeys.OVERALL)) {
					overallAspect = topAspect2.getKey();
				} else if (topAspect1.getValue() == topAspect2.getValue() && topAspect1.getKey().equalsIgnoreCase(XpressoKeys.OVERALL)) {
					overallAspect = topAspect2.getKey();
				}
			} else {
				overallAspect = "";
			}

			/**
			 * 	trending_topics
			 */
			/*JSONArray jsonTopics = jsonRoot.getJSONArray(XpressoKeys.TRENDING_TOPICS);
			int nCounter = Integer.parseInt(prop.getProperty("no_of_topic", "50").trim());
			if(jsonTopics.length() < nCounter)
				nCounter = jsonTopics.length();
			//for (int i = 0; i < jsonTopics.length(); i++) {
			for (int i = 0; i < nCounter; i++) {
				JSONObject obj = jsonTopics.getJSONObject(i);
				
				String topic = obj.getString(XpressoKeys.TOPIC);
				double score = obj.getDouble(XpressoKeys.SCORE);
				trendingTopics.put(topic, score);
			}*/

			/**
			 * Get Entity List	
			 */
			/*try{
				JSONArray jsonEntityList = jsonRoot.getJSONArray(XpressoKeys.ENTITY_LIST);
				for (int i = 0; i < jsonEntityList.length(); i++) {
					String entity = jsonEntityList.getString(i);
					entityList.add(entity);
				}
				//	System.out.println("Entity List :" + entityList);
			}
			catch(JSONException je) {}*/

			try {
				JSONObject jsonEntityList = jsonRoot.getJSONObject(XpressoKeys.ENTITY_LIST);
				for (Object key : jsonEntityList.keySet()) {
					String entity = key.toString();
					int frequency = jsonEntityList.getInt(entity);
					entityMap.put(entity, frequency);
					// trendingTopics.put(entity, frequency);
				}
			} catch (Exception e) {
				System.out.println(XpressoKeys.ENTITY_LIST + " not found in Xpresso engine output json.");
			}

			parseStatus = true;

		} else {
			// throw new Exception("Xpresso engine output json is NULL");
			System.out.println("******  Xpresso engine output json is BLANK");
		}
	}

	private void calculateOverallSetiment() throws Exception {

		//		double posVal = reviewSentiments.get(XpressoKeys.POSITIVE);
		//		double negVal = reviewSentiments.get(XpressoKeys.NEGATIVE);
		//		double neuVal = reviewSentiments.get(XpressoKeys.NEUTRAL);

		int posVal = 0;
		int negVal = 0;
		int neuVal = 0;

		for (XpressoAbout about : abouts) {
			if (about.getSentiment().equalsIgnoreCase(XpressoKeys.POSITIVE)) {
				posVal++;
			}
			if (about.getSentiment().equalsIgnoreCase(XpressoKeys.NEGATIVE)) {
				negVal++;
			}
			if (about.getSentiment().equalsIgnoreCase(XpressoKeys.NEUTRAL)) {
				neuVal++;
			}

			incrementAspect(about.getAspect(), 1);
		}

		//		double maxVal = Math.max(posVal, Math.max(negVal, neuVal));
		int maxVal = Math.max(posVal, Math.max(negVal, neuVal));

		if (posVal == maxVal && maxVal > negVal && maxVal > neuVal) {
			overallSentiment = "Positive";
			overallSentimentScore = posVal;
		} else if (maxVal == negVal && maxVal > posVal && maxVal > neuVal) {
			overallSentiment = "Negative";
			overallSentimentScore = negVal;
		} else if (maxVal == neuVal && maxVal > negVal && maxVal > posVal) {
			overallSentiment = "Neutral";
			overallSentimentScore = neuVal;
		} else if (posVal == maxVal && maxVal == negVal) {
			overallSentiment = "Neutral";
			overallSentimentScore = neuVal;
		} else if (posVal == maxVal && maxVal == neuVal) {
			overallSentiment = "Positive";
			overallSentimentScore = posVal;
		} else if (negVal == maxVal && maxVal == neuVal) {
			overallSentiment = "Negative";
			overallSentimentScore = negVal;
		}
	}

	@Override
	public String toString() {

		String str = "";

		str = "XpressoValues :\ndomain=" + domain + "\n";

		for (XpressoAbout about : abouts) {
			str = str + about.toString() + "\n";
		}

		str = str + "\nTrendingTopics=" + trendingTopics + "\nOverallSentiment=" + overallSentiment + "\nOverallSentimentScore=" + overallSentimentScore + "\n";

		return str;
	}

	public static void main(String[] args) {

		try {

			//			String file = ".\\MCI_xpresso_sample.json";
			//			String file = ".\\xpresso_sample1.json";
			//			String file = ".\\xpresso_sample2.json";
			//			String file = ".\\xpresso_sample3.json";

			String file = "D:\\Data Dumps & Sample\\Xpresso JSON Structure\\fb.txt";
			//			String file = "D:\\Abzooba\\MCI\\Phase2\\MCI_OUTPUT_post.json";
			//			String file = "D:\\Abzooba\\MCI\\Phase2\\MCI_OUTPUT_rss.json";

			//			String file = ".\\null_output.json";
			//			String file = ".\\sample_senti.json";
			//			String file = ".\\sample_expr_trend.json";			

			Properties properties = new Properties();
			properties.load(new FileInputStream(Constants.PROPERTIES_PATH));

			String json = getJSONString(file);
			JSONObject obj = new JSONObject();
			obj = new org.json.JSONObject(json);
			System.out.println("TOKEN :: " + obj.getString("access_token"));
			if (obj.has("access_token") && obj.getString("access_token") != null)
				System.out.println("TOKEN :: " + obj.getString("access_token"));
			String strDomain = "government";
			//String strReview = "http://www.straitstimes.com/singapore/important-for-dementia-patients-to-be-in-a-familiar-home-environment-indranee-rajah";
			String strReview = getJSONString(file);
			//String json = getJsonFromServer(properties, strReview, strDomain);

			System.out.println(json);
			XpressoValues xp = new XpressoValues(json);
			xp.parseXpressoJson(json, properties);

			System.out.println("================================\n" + xp.toString());
			System.out.println("================================\n" + xp.printAspectMap());
			System.out.println("================================\n" + xp.getOverallSentiment() + " = " + xp.getOverallSentimentScore());
			System.out.println("================================\n" + xp.getOverallAspect());

		} catch (Exception ex) {
			ex.printStackTrace(System.out);
		}
	}

	private void incrementAspect(String aspect, int x) {
		int i = 1;

		Integer N = aspectMap.get(aspect);
		if (N != null) {
			i = N.intValue() + x;
		}
		//		System.out.println(aspect + "  " + i);
		aspectMap.put(aspect, i);
	}

	public String printAspectMap() {
		return aspectMap.toString();

	}

	public static String getJSONString(String file) throws IOException {

		String json = "";
		BufferedReader br = new BufferedReader(new FileReader(file));
		String str = "";
		while ((str = br.readLine()) != null) {
			json = json + str;
		}
		br.close();
		return json;
	}

	private static String getJsonFromServer(Properties prop, String strReview, String strDomain) throws Exception {

		String responseText = "";

		URL baseURL = new URL("http://" + prop.getProperty("server") + ":" + prop.getProperty("port") + "/abzooba/engine/result/");
		HttpURLConnection httpConnection = (HttpURLConnection) baseURL.openConnection();
		httpConnection.setRequestMethod("POST");
		httpConnection.setDoOutput(true);
		httpConnection.setRequestProperty("Content-Type", "text/plain;");

		BufferedWriter httpRequestBodyWriter = new BufferedWriter(new OutputStreamWriter(httpConnection.getOutputStream()));
		String strData = "";
		if (prop.getProperty("apikey") != null)
			strData += "apikey=" + prop.getProperty("apikey") + prop.getProperty("delimeter");
		if (prop.getProperty("apisecret") != null)
			strData += "apisecret=" + prop.getProperty("apisecret") + prop.getProperty("delimeter");

		//"source=" + nSourceId + prop.getProperty("delimeter") +
		strData += "annotation=" + prop.getProperty("annotation") + prop.getProperty("delimeter") + "domain=" + strDomain + prop.getProperty("delimeter") + "hist=" + prop.getProperty("hist") + prop.getProperty("delimeter") + "trend=" + prop.getProperty("trend") + prop.getProperty("delimeter") + "feedback=" + strReview;

		httpRequestBodyWriter.write(strData);
		httpRequestBodyWriter.close();

		BufferedReader reader = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
		String line;
		while ((line = reader.readLine()) != null) {
			responseText += line + '\n';
		}

		return responseText;

	}
}