package com.abzooba.sourcing.common.bean;

import java.io.Serializable;

import org.json.JSONObject;

import com.abzooba.sourcing.common.util.Constants;
import com.abzooba.sourcing.common.util.XpressoKeys;

public class XpressoAbout implements Serializable {

	private static final long serialVersionUID = 1L;

	private String aspect = null;
	private String entity = null;
	private String sentiment = null;
	private int sentiment_number = 0;
	private String statementType = null;
	private String snippet = null;

	public XpressoAbout() {
	}

	public void parseXpressoAbout(JSONObject about) throws Exception {

		aspect = about.getString(XpressoKeys.ASPECT);
		entity = about.getString(XpressoKeys.ENTITY);
		sentiment = about.getString(XpressoKeys.SENTIMENT);
		statementType = about.getString(XpressoKeys.STATEMENT_TYPE);
		snippet = about.getString(XpressoKeys.INDICATIVE_SNIPPET);

		sentiment_number = Constants.getSentimentNumber(sentiment);
	}

	public String getAspect() {
		return aspect;
	}

	public String getEntity() {
		return entity;
	}

	public String getSentiment() {
		return sentiment;
	}

	public String getStatementType() {
		return statementType;
	}

	public String getSnippet() {
		return snippet;
	}

	public int getSentiment_number() {
		return sentiment_number;
	}

	public void setSentiment_number(int sentiment_number) {
		this.sentiment_number = sentiment_number;
	}

	@Override
	public String toString() {
		return "XpressoAbout [aspect=" + aspect + ", entity=" + entity + ", sentiment=" + sentiment + "(" + sentiment_number + "), statementType=" + statementType + ", snippet=" + snippet + "]";
	}

}
