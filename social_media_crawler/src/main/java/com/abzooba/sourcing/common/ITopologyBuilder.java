package com.abzooba.sourcing.common;

import java.util.Map;
import java.util.Properties;

/*import backtype.storm.topology.TopologyBuilder;*/
import org.apache.storm.topology.TopologyBuilder;

public interface ITopologyBuilder {
	TopologyBuilder topologyBuilder(Properties properties);
	Map topologyConfBuilder(Properties properties);
	void createTopology(String strTopologyName);
}
