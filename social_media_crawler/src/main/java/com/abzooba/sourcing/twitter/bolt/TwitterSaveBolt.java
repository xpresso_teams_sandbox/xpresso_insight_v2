package com.abzooba.sourcing.twitter.bolt;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/*import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Tuple;*/
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Tuple;

import com.abzooba.sourcing.common.bean.PageInfo;
import com.abzooba.sourcing.common.bean.SocialData;
import com.abzooba.sourcing.common.service.DBService;
import com.abzooba.sourcing.common.service.DataSource;
import com.abzooba.sourcing.common.util.Util;

public class TwitterSaveBolt extends BaseRichBolt {

	private static final long serialVersionUID = 1L;
	protected OutputCollector _collector;

	private Properties _prop = new Properties();

	public TwitterSaveBolt() throws IOException, InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		try {
			//_prop.load(new FileInputStream(Constants.PROPERTIES_PATH));
			//_prop.load(new FileInputStream(System.getProperty("propfile")));
			//_prop.load(DBSaveBolt.class.getClassLoader().getResourceAsStream("config.properties"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void execute(Tuple tuple) {
		/*int sourceTypeId = 0;
		int shortNameId = 0;
		try {
			sourceTypeId = DBService.getSourceTypeId("TW");
			shortNameId = DBService.getShortnameId("TW");
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		System.out.println("TwitterSaveBolt executing....");
		PageInfo pageInfo = (PageInfo) tuple.getValueByField("pageInfo");
		Set<SocialData> socialDatas = (Set<SocialData>) tuple.getValueByField("socialDatas");

		try {
			if (pageInfo != null) {
				PageInfo pageInfoExist = DBService.getPageInfo(pageInfo.getPageId());
				if (pageInfoExist == null) {
					DBService.savePageInfo(pageInfo);
				} else {
					Util.updatePageInfoValues(pageInfo, pageInfoExist);
					DBService.updatePageInfo(pageInfo);
				}
				if (_prop.getProperty("kpi_twitter").equalsIgnoreCase("1")) {
					DBService.saveKPIPageLevel(pageInfo);
				}
			}

			if (socialDatas != null && socialDatas.size() > 0) {
				DBService.saveTwitterSocialData(socialDatas, _prop);
				if (_prop.getProperty("kpi_twitter").equalsIgnoreCase("1")) {
					DBService.saveKPIPostTwitterLevel(socialDatas);
				}

			}

		} catch (Exception e) {
			_collector.fail(tuple);
			e.printStackTrace();
		}

		_collector.ack(tuple);
	}

	public void declareOutputFields(OutputFieldsDeclarer declarer) {

	}

	public void prepare(Map map, TopologyContext context, OutputCollector collector) {
		try {
			_prop.load(new FileInputStream((String) map.get("config.properties")));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		DataSource.setConfigPath((String) map.get("config.properties"));
		_collector = collector;
	}
}
