package com.abzooba.sourcing.twitter.spout;

import java.io.FileInputStream;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/*import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;*/
import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;
import org.apache.storm.utils.Utils;

import com.abzooba.sourcing.common.bean.AlertDTO;
import com.abzooba.sourcing.common.bean.SourceDTO;
import com.abzooba.sourcing.common.service.DBService;
import com.abzooba.sourcing.common.service.DataSource;
import com.abzooba.sourcing.twitter.util.TwitterUtil;

public class TwitterSpout extends BaseRichSpout {
	private static final long serialVersionUID = 1L;
	protected SpoutOutputCollector _collector;
	private Properties _prop = new Properties();

	private SimpleDateFormat fmtDateHH = new SimpleDateFormat("yyyy-MM-dd HH");

	public TwitterSpout() {
	}

	public void nextTuple() {

		int nMinutes = Integer.parseInt(_prop.getProperty("twitter_sleep_time_in_minutes").trim());
		//List<SourceDTO> sources = DBService.getSources(nMinutes, "Twitter", null);
		List<SourceDTO> sources = new ArrayList<>();
		try {
			sources = DBService.getSources(nMinutes, "TW", null);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (sources.size() > 0) {
			try {
				Map<String, Object> twitterMap = TwitterUtil.getOAuth2Token(_prop, sources.get(0));

				for (SourceDTO sourceDTO : sources) {

					if (sourceDTO.getPageURL().startsWith("@"))
						sourceDTO.setPageURL(sourceDTO.getPageURL().substring(1));

					System.out.println("Crawling for " + sourceDTO.getPageURL());

					_collector.emit(new Values(sourceDTO, twitterMap), sourceDTO.getSourceId());
					System.out.println("TwitterSpout: Tuple emitted successfully: " + sourceDTO.toString() + ", " + sourceDTO.getPageURL());

					// update livestatus column in tbltaskinfo table
					DBService.updateSourceStatus(2, sourceDTO.getSourceId(), true, null);
				}
				Utils.sleep(nMinutes * 60 * 1000); // every two minutes
			} catch (Exception e) {
				AlertDTO alertDTO = new AlertDTO();
				alertDTO.setSubject("Twitter Token Expired");
				alertDTO.setAlertCategoryId(3);
				alertDTO.setSourceId(sources.get(0).getSourceId());
				alertDTO.setDateHH(fmtDateHH.format(new Date()));
				alertDTO.setDescription("All token expired for Twitter. " + e.getMessage());
				alertDTO.setAlertType("Twitter Token");
				alertDTO.setPriority(1);
				alertDTO.setActive(true);
				alertDTO.setAuthor("system");

				try {
					DBService.saveAlert(alertDTO);
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				e.printStackTrace();
			}
		} else {
			Utils.sleep(nMinutes * 60 * 1000); // every two minutes
		}

	}

	public void open(Map map, TopologyContext context, SpoutOutputCollector collector) {
		_collector = collector;
		try {
			//_prop.load(new FileInputStream(Constants.PROPERTIES_PATH));
			//_prop.load(new FileInputStream(System.getProperty("propfile")));
			//_prop.load(TwitterSpout.class.getClassLoader().getResourceAsStream("config.properties"));
			_prop.load(new FileInputStream((String) map.get("config.properties")));
			DataSource.setConfigPath((String) map.get("config.properties"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("sourcedto", "twittermap"));
	}
}