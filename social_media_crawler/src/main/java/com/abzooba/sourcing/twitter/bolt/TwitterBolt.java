package com.abzooba.sourcing.twitter.bolt;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;

/*import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;*/
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

import com.abzooba.sourcing.common.bean.AlertDTO;
import com.abzooba.sourcing.common.bean.PageInfo;
import com.abzooba.sourcing.common.bean.SocialData;
import com.abzooba.sourcing.common.bean.SourceDTO;
import com.abzooba.sourcing.common.service.DBService;
import com.abzooba.sourcing.common.service.DataSource;
import com.abzooba.sourcing.common.util.Constants;
import com.abzooba.sourcing.common.util.DateUtil;
import com.abzooba.sourcing.common.util.Util;
import com.abzooba.sourcing.twitter.util.TwitterUtil;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.RateLimitStatus;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.URLEntity;
import twitter4j.User;

public class TwitterBolt extends BaseRichBolt {

	private static final long serialVersionUID = 1L;

	protected OutputCollector _collector;
	//Number of hit can be 400 maximum
	private int _numberofhit = 200;
	private String _strpreviousday;

	private SimpleDateFormat _format = new SimpleDateFormat("yyyy-MM-dd");

	private SimpleDateFormat fmtDateHH = new SimpleDateFormat("yyyy-MM-dd HH");

	private SimpleDateFormat _hourFormat = new SimpleDateFormat("HH:00:00");
	private Properties _prop = new Properties();
	private HashMap<String, Integer> aspectMap;
	private HashMap<String, Integer> postTypesMap;
	private HashMap<Integer, String> rssTwitterMap;

	public TwitterBolt() throws ClassNotFoundException, IllegalAccessException, SQLException, InstantiationException, IOException {
		try {
			//_prop.load(new FileInputStream(Constants.PROPERTIES_PATH));
			//_prop.load(new FileInputStream(System.getProperty("propfile")));
			//_prop.load(TwitterBolt.class.getClassLoader().getResourceAsStream("config.properties"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void execute(Tuple tuple) {

		int remainingCalls = 0;

		SourceDTO sourceDTO = (SourceDTO) tuple.getValueByField("sourcedto");
		Map<String, Object> twitterMap;
		try {
			twitterMap = TwitterUtil.getOAuth2Token(_prop, sourceDTO);
		} catch (TwitterException | SQLException e1) {
			twitterMap = (Map<String, Object>) tuple.getValueByField("twittermap");
			System.err.println("USING Twitter Map from Spout");
			//e1.printStackTrace();
		}
		int nTotalNumberOfHit = 0;
		boolean bNoTweetFound = false;
		boolean bTweetFound = false;
		String strStartDate = "";
		String strEndDate = "";
		long maxID = Long.MAX_VALUE;
		_strpreviousday = _prop.getProperty("tweet_previous_day");
		_numberofhit = Integer.parseInt(_prop.getProperty("tweet_pages"));
		int nPreviousDay = Integer.parseInt(_strpreviousday);
		int postTypeId = postTypesMap.get("Tweet");
		int postTypeId4Retweet = postTypesMap.get("Retweet");
		System.out.println("Previous Day ::" + nPreviousDay);

		try {

			Twitter twitterObj = TwitterUtil.getTwitter(_prop, sourceDTO.getSourceTypeId(), twitterMap);
			System.out.println("TwitterBolt: Twitter value:" + twitterObj);

			if (twitterObj != null) {
				Map<String, RateLimitStatus> rateLimitStatus = twitterObj.getRateLimitStatus("search");
				RateLimitStatus searchTweetsRateLimit = rateLimitStatus.get("/search/tweets");

				PageInfo pageInfo;
				User user = null;
				String strPageId = "0";
				try {
					pageInfo = new PageInfo();
					user = twitterObj.showUser(sourceDTO.getPageURL());
					strPageId = String.valueOf(user.getId());
					pageInfo.setPageId(strPageId);
					pageInfo.setSourceId(sourceDTO.getSourceId());
					pageInfo.setDateKey(_format.format(new Date()));
					pageInfo.setHourKey(_hourFormat.format(new Date()));
					pageInfo.setPageLike(user.getFavouritesCount());
					pageInfo.setTotalFollowers(user.getFollowersCount());
					pageInfo.setTotalFollowings(user.getFriendsCount());
					pageInfo.setPageURL(sourceDTO.getPageURL());
					System.out.println(pageInfo.toString());

					System.out.println("TwitterBolt: Emitting PAGE_INFO data to save in database");
					_collector.emit(tuple, new Values(pageInfo, null));
				} catch (Exception e1) {
					System.err.println("ERROR in  getting User Information  for USERNAME :: " + sourceDTO.getPageURL());
					//e1.printStackTrace();
				}

				//Search for tweets that contains this term
				Query query = new Query(sourceDTO.getPageURL());
				boolean isCrawlFromPast = false;
				System.out.println("Number of Pages:: " + _numberofhit);
				for (int queryNumber = 0; queryNumber < _numberofhit; queryNumber++) {
					Status dateStatus = null;
					int count = 0;
					System.out.println("TwitterBolt: Processing started page wise : " + queryNumber);
					bNoTweetFound = false;
					strStartDate = _format.format(DateUtil.getBeforeOrAfterDate(-nPreviousDay));
					System.out.println("TwitterBolt: crawling from " + strStartDate + " to " + strEndDate);
					try {
						query.setCount(100);
						//query.setLang("en");
						query.setSince(strStartDate);
						//query.setUntil(strEndDate);

						QueryResult result = twitterObj.search(query);
						if (result.getTweets().size() == 0) {
							bNoTweetFound = true;
							System.out.println("TwitterBolt: No tweet found:" + bNoTweetFound);
							break;
						}
						Set<SocialData> socialDatas = new LinkedHashSet<SocialData>();
						List<Status> allStatus = result.getTweets();
						try {
							if (queryNumber == 0) {
								//Adding the User's Status for the first pagination
								allStatus.add(user.getStatus());
							}
						} catch (Exception e3) {
							System.out.println("USER DOES NOT HAVE ANY STATUS CURRENTLY :: " + e3.getMessage());
							//e3.printStackTrace();
						}

						for (Status status : allStatus) {
							try {
								SocialData socialData = new SocialData();
								Util.prepareSocialDataBean(socialData, sourceDTO);
								bTweetFound = true;
								//	Increment our count of tweets retrieved 
								if (maxID == -1 || status.getId() < maxID) {
									maxID = status.getId();
								}

								long lTweetID = status.getId();
								//System.out.println("STATUS :: " + status);

								String strTweetUser = "";
								try {
									strTweetUser = status.getUser().getScreenName();
									int authorOutreach = status.getUser().getFollowersCount();
									String userURL = status.getUser().getProfileImageURL();
									String authorId = Long.toString(status.getUser().getId());
									if (authorId == strPageId) {
										authorOutreach = 0;
									}
									socialData.setAuthorId(authorId);
									socialData.setAuthorName(strTweetUser.replaceAll("'", "\\'"));
									socialData.setUserURL(userURL);
									socialData.setAuthorOutreach(authorOutreach);

								} catch (Exception e3) {
									//System.err.println("********ERROR with Twitter AUTHOR ID :: " + status );									
									//e3.printStackTrace();
								}

								//if (_prop.getProperty("expand_url").equalsIgnoreCase("1")) {
								if (rssTwitterMap.containsKey(sourceDTO.getSourceId())) {
									if (rssTwitterMap.get(sourceDTO.getSourceId()).equalsIgnoreCase(strTweetUser)) {
										URLEntity[] url = status.getURLEntities();
										for (URLEntity link : url) {
											String shortURL = link.getExpandedURL();
											//System.out.println("Expanded Link : " + shortURL);
											/**
											 * When Twitter Status is directly posted, The URL entity takes the format
											 * https://twitter.com/i/web/status/<STATUS ID>
											 * This also needs to be expanded to avoid any loss of data
											 */
											if (shortURL.length() <= 30 || shortURL.contains("https://twitter.com/i/web/status/")) {
												//System.out.println("Extending the URL via call..." + shortURL);
												String expandedURL = TwitterUtil.getExpandedURL(_prop, shortURL);
												System.out.println("******* FINAL LINK being saved :: " + expandedURL);
												socialData.setLink(expandedURL);
											} else {
												socialData.setLink(shortURL);
											}
										}
									}
								}
								//System.out.println("**************************** AUTHOR OUTREACH :: " + authorOutreach);
								String strTweetId = String.valueOf(lTweetID);
								String strTweetDate = status.getCreatedAt().toString();
								//System.out.println("twitter4j date :: " + strTweetDate);
								int nRetweetCount = status.getRetweetCount();

								/**
								 * Only taking Retweets of Posts.
								 * If the status is Retweet then we don't go further into its Retweets
								 */
								//&& (status.isRetweet() == false)

								/**
								 * /o Avoid Retweet Error as it is expired
								 */
								try {
									if ((nRetweetCount > 0) && (status.isRetweet() == false)) {
										//System.out.println("*************************************************ENTERING RETWEET*************************************************");
										Set<SocialData> retweetDatas = new LinkedHashSet<SocialData>();
										retweetDatas = TwitterUtil.getRetweets(twitterObj.getRetweets(lTweetID), sourceDTO, strTweetId, postTypeId4Retweet, aspectMap, _prop);
										_collector.emit(tuple, new Values(null, retweetDatas));
									} else {
										//System.out.println("No Retweets");
									}
								} catch (Exception e) {
								}

								int nFavoriteCount = status.getFavoriteCount();
								String pattern = "E MMM dd HH:mm:ss z yyyy";
								SimpleDateFormat format = new SimpleDateFormat(pattern);
								Date date = format.parse(strTweetDate);

								SimpleDateFormat formatdate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
								//System.out.println("Get POST_DATETIME from Twitter API :: " + date);
								//System.out.println("Timezone on config.properties :: " + _prop.getProperty("timezone"));
								//formatdate.setTimeZone(TimeZone.getTimeZone("GMT"));
								formatdate.setTimeZone(TimeZone.getTimeZone(_prop.getProperty("timezone", "GMT")));
								//System.out.println("POST_DATETIME saved in DB :: " + formatdate.format(date));
								strTweetDate = formatdate.format(date);
								//System.out.println("date save in db :: " + strTweetDate);

								java.sql.Date sqlDate = new java.sql.Date(date.getTime());
								String strTweetMessage = status.getText().replaceAll("'", "\\'");
								if (strTweetMessage.contains("@" + sourceDTO.getPageURL())) {
									socialData.setIsMention(1);
								}
								socialData.setAspectMap(aspectMap);
								socialData.setPostId(strTweetId);
								socialData.setDateKey(_format.format(new Date()));
								socialData.setHourKey(_hourFormat.format(new Date()));

								if (status.isRetweeted() || strTweetMessage.startsWith("RT")) {
									Status originalTweet = status.getRetweetedStatus();
									if (originalTweet != null) {
										String parentID = String.valueOf(originalTweet.getId());
										socialData.setParentId(parentID);
									} else {
										socialData.setParentId(strPageId);
									}
									socialData.setPostTypeId(postTypeId4Retweet);
								} else if (status.getInReplyToStatusId() > -1) {
									socialData.setParentId(String.valueOf(status.getInReplyToStatusId()));
									socialData.setPostTypeId(postTypeId);
								} else {
									socialData.setParentId(strPageId);
									socialData.setPostTypeId(postTypeId);
								}

								socialData.setPostDateTime(strTweetDate);
								socialData.setPostText(strTweetMessage);
								socialData.setXpressoReview(socialData.getPostText());
								socialData.setHashId(Util.ConvertTomd5(strTweetMessage));
								socialData.setRetweetsCount(nRetweetCount);
								socialData.setLikesCount(nFavoriteCount);
								TwitterUtil.getLocation(socialData, status);

								String postURL = Constants.TWITTER_URL + strTweetUser + Constants.STATUS + strTweetId;
								socialData.setPostURL(postURL);
								socialDatas.add(socialData);
								dateStatus = status;
								count++;

							} catch (Exception e) {
								System.out.println("TwitterBolt: getting some error to extract every tweet ....continue :: " + e.getMessage());
								e.printStackTrace();
								continue;
							}
						}
						searchTweetsRateLimit = result.getRateLimitStatus();
						remainingCalls = searchTweetsRateLimit.getRemaining();
						System.out.println("Source ID :: " + sourceDTO.getSourceId());
						System.out.println("Rate limit status for source ID :: " + searchTweetsRateLimit);
						System.out.println("TwitterBolt: Emitting SOCIAL_DATA data to save in database");

						_collector.emit(tuple, new Values(null, socialDatas));
						_collector.ack(tuple);
					} catch (Exception e) {
						System.err.println("TwitterBolt: getting some error to extrat tweet page wise ...continue");
						e.printStackTrace();
						continue;
					}
					if (remainingCalls == 3) {
						System.out.println("Getting a NEW TWITTER TOKEN");
						twitterObj = TwitterUtil.getTwitterObject(_prop, sourceDTO);
					}
					System.out.println("Date in current Page ::  " + dateStatus.getCreatedAt());
					System.out.println("Records in current Page :: " + count);
					count = 0;
					query.setMaxId(maxID - 1);
					nTotalNumberOfHit++;
				}
				//Check process completed or not
				if (nTotalNumberOfHit == _numberofhit) {
					//case for full tweet found
					System.out.println("TwitterBolt: Crawling completed successfully");
				} else if (bNoTweetFound && bTweetFound) {
					System.out.println("bNotweetfound = " + bNoTweetFound + ", btweetfound = " + bTweetFound + ", logic = " + (bNoTweetFound && bTweetFound));
				}
				//case of no tweet found
				if (bNoTweetFound) {
					if (!(bTweetFound)) {
						System.out.println("TwitterBolt: No Tweet found");
					}
				}
			} else {
				AlertDTO alertDTO = new AlertDTO();
				alertDTO.setSubject("Twitter API Error");
				alertDTO.setAlertCategoryId(4);
				alertDTO.setSourceId(sourceDTO.getSourceId());
				alertDTO.setDateHH(fmtDateHH.format(new Date()));
				alertDTO.setDescription("Twitter API is not working");
				alertDTO.setAlertType("Major");
				alertDTO.setPriority(1);
				alertDTO.setActive(true);
				alertDTO.setAuthor("system");

				DBService.saveAlert(alertDTO);
			}
			System.out.println("TwitterBolt: Completed processing");

			/**
			 * Updating Source Master for Execution of Source ID
			 */
			DBService.updateSourceStatus(1, sourceDTO.getSourceId(), false, null);

		} catch (Exception e) {

			AlertDTO alertDTO = new AlertDTO();
			alertDTO.setSubject("Twitter API Error");
			alertDTO.setAlertCategoryId(4);
			alertDTO.setSourceId(sourceDTO.getSourceId());
			alertDTO.setDateHH(fmtDateHH.format(new Date()));
			alertDTO.setDescription("Twitter API is not working. " + e.getMessage());
			alertDTO.setAlertType("Major");
			alertDTO.setPriority(1);
			alertDTO.setActive(true);
			alertDTO.setAuthor("system");

			try {
				DBService.saveAlert(alertDTO);
			} catch (SQLException sql) {
				sql.printStackTrace();
			}

			System.err.println("**************************** UNHANDLED ERROR ::: " + e.getMessage());
			e.printStackTrace();
			_collector.fail(tuple);
			System.err.println("SOURCE DTO :: " + sourceDTO.toString());
			//updating status in tbltaskinfo table
		}

	}

	public void prepare(Map map, TopologyContext context, OutputCollector collector) {
		try {
			_prop.load(new FileInputStream((String) map.get("config.properties")));
			DataSource.setConfigPath((String) map.get("config.properties"));
			aspectMap = DBService.getAspectMap();
			postTypesMap = DBService.getPostTypeMap();
			rssTwitterMap = DBService.getRssTwitterMap("TW");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		_collector = collector;
	}

	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("pageInfo", "socialDatas"));
	}
}