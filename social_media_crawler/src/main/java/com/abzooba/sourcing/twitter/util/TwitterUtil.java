package com.abzooba.sourcing.twitter.util;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;

import com.abzooba.sourcing.common.bean.AlertDTO;
import com.abzooba.sourcing.common.bean.SocialData;
import com.abzooba.sourcing.common.bean.SourceDTO;
import com.abzooba.sourcing.common.service.DBService;
import com.abzooba.sourcing.common.util.Constants;
import com.abzooba.sourcing.common.util.Util;

import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.OAuth2Token;
import twitter4j.conf.ConfigurationBuilder;

public class TwitterUtil {

	/*private static Properties prop = null;
		
		public static void init(Properties _prop) throws Exception {
			prop = _prop;
		}*/
	private static DateFormat fmtDateHH = new SimpleDateFormat("yyyy-MM-dd HH");

	public static Twitter getTwitter(Properties prop, int nSourceTypeId, Map<String, Object> twitterMap) throws TwitterException, SQLException {

		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setApplicationOnlyAuthEnabled(true);
		cb.setOAuthConsumerKey((String) twitterMap.get("app_key"));
		cb.setOAuthConsumerSecret((String) twitterMap.get("app_secret"));
		setProxyServer(prop, cb);
		OAuth2Token token = (OAuth2Token) twitterMap.get("auth_token");
		cb.setOAuth2TokenType(token.getTokenType());
		cb.setOAuth2AccessToken(token.getAccessToken());

		DBService.updateTokenMaster(1, (Integer) twitterMap.get("token_id"), nSourceTypeId);
		return new TwitterFactory(cb.build()).getInstance();
	}

	public static Twitter getTwitterProfiling(Properties prop, int nSourceTypeId, Map<String, Object> twitterMap) throws TwitterException, SQLException {

		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setApplicationOnlyAuthEnabled(true);
		cb.setOAuthConsumerKey((String) twitterMap.get("app_key"));
		cb.setOAuthConsumerSecret((String) twitterMap.get("app_secret"));
		setProxyServer(prop, cb);
		OAuth2Token token = (OAuth2Token) twitterMap.get("auth_token");
		cb.setOAuth2TokenType(token.getTokenType());
		cb.setOAuth2AccessToken(token.getAccessToken());

		DBService.updateTokenMasterProfiling(1, (Integer) twitterMap.get("token_id"), nSourceTypeId);
		return new TwitterFactory(cb.build()).getInstance();
	}

	public static Map<String, Object> getOAuth2Token(Properties prop, SourceDTO sourceDTO) throws TwitterException, SQLException {
		Map<String, Object> twitterMap = new HashMap<String, Object>();
		Map<String, Object> tokenMap = DBService.getNextToken(sourceDTO.getSourceTypeId());
		if (tokenMap.isEmpty()) {
			DBService.updateTokenMaster(0, -1, sourceDTO.getSourceTypeId());
			tokenMap = DBService.getNextToken(sourceDTO.getSourceTypeId());
		}

		if (tokenMap != null) {
			ConfigurationBuilder cb = new ConfigurationBuilder();
			cb.setApplicationOnlyAuthEnabled(true);
			cb.setOAuthConsumerKey((String) tokenMap.get("app_key"));
			cb.setOAuthConsumerSecret((String) tokenMap.get("app_secret"));
			setProxyServer(prop, cb);

			OAuth2Token token = new TwitterFactory(cb.build()).getInstance().getOAuth2Token();

			twitterMap.put("token_id", (Integer) tokenMap.get("token_id"));
			twitterMap.put("app_key", (String) tokenMap.get("app_key"));
			twitterMap.put("app_secret", (String) tokenMap.get("app_secret"));
			twitterMap.put("auth_token", token);
		} else {
			AlertDTO alertDTO = new AlertDTO();
			alertDTO.setSubject("Twitter Token Expired");
			alertDTO.setAlertCategoryId(3);
			alertDTO.setSourceId(sourceDTO.getSourceId());
			alertDTO.setDateHH(fmtDateHH.format(new Date()));
			alertDTO.setDescription("All token expired for Twitter");
			alertDTO.setAlertType("Critical");
			alertDTO.setPriority(1);
			alertDTO.setActive(true);
			alertDTO.setAuthor("system");

			try {
				DBService.saveAlert(alertDTO);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return twitterMap;
	}

	public static Map<String, Object> getOAuth2TokenProfiling(Properties prop, SourceDTO sourceDTO) throws TwitterException, SQLException {
		Map<String, Object> twitterMap = new HashMap<String, Object>();
		Map<String, Object> tokenMap = DBService.getNextTokenUserProfiling(sourceDTO.getSourceTypeId());
		if (tokenMap.isEmpty()) {
			DBService.updateTokenMasterProfiling(0, -1, sourceDTO.getSourceTypeId());
			tokenMap = DBService.getNextTokenUserProfiling(sourceDTO.getSourceTypeId());
		}

		if (tokenMap != null) {
			ConfigurationBuilder cb = new ConfigurationBuilder();
			cb.setApplicationOnlyAuthEnabled(true);
			cb.setOAuthConsumerKey((String) tokenMap.get("app_key"));
			cb.setOAuthConsumerSecret((String) tokenMap.get("app_secret"));
			setProxyServer(prop, cb);

			OAuth2Token token = new TwitterFactory(cb.build()).getInstance().getOAuth2Token();

			twitterMap.put("token_id", (Integer) tokenMap.get("token_id"));
			twitterMap.put("app_key", (String) tokenMap.get("app_key"));
			twitterMap.put("app_secret", (String) tokenMap.get("app_secret"));
			twitterMap.put("auth_token", token);
		}

		return twitterMap;
	}

	public static Set<SocialData> getRetweets(List<Status> tweets, SourceDTO sourceDTO, String parentId, int postTypeId4Retweet, HashMap<String, Integer> aspectMap, Properties _prop) {
		Set<SocialData> retweetDatas = new LinkedHashSet<SocialData>();
		SimpleDateFormat _format = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat _hourFormat = new SimpleDateFormat("HH:00:00");
		//System.out.println("Inside Twitter Util");
		for (Status status : tweets) {
			try {
				SocialData socialData = new SocialData();
				//System.out.println("INSIDE LOOP");
				long lTweetID = status.getId();
				String authorId = "";
				try {
					authorId = Long.toString(status.getUser().getId());
				} catch (Exception e3) {
					System.err.println("********ERROR with Twitter AUTHOR ID :: " + status);
					e3.printStackTrace();
				}
				String strTweetId = String.valueOf(lTweetID);
				String strTweetDate = status.getCreatedAt().toString();
				//System.out.println("twitter4j date :: " + strTweetDate);
				//int nRetweetCount = status.getRetweetCount();
				//int nFavoriteCount = status.getFavoriteCount();

				String pattern = "E MMM dd HH:mm:ss z yyyy";
				SimpleDateFormat format = new SimpleDateFormat(pattern);
				Date date = format.parse(strTweetDate);

				SimpleDateFormat formatdate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				//formatdate.setTimeZone(TimeZone.getTimeZone("GMT"));
				//System.out.println("Get POST_DATETIME from Twitter API :: " + date);
				//System.out.println("Timezone on config.properties :: " + _prop.getProperty("timezone"));
				formatdate.setTimeZone(TimeZone.getTimeZone(_prop.getProperty("timezone", "GMT")));
				//System.out.println("POST_DATETIME saved in DB :: " + formatdate.format(date));
				strTweetDate = formatdate.format(date);
				//System.out.println("date save in db :: " + strTweetDate);

				java.sql.Date sqlDate = new java.sql.Date(date.getTime());
				String strTweetMessage = status.getText().replaceAll("'", "\\'");
				//System.out.println("TWEET TEXT ::"+strTweetMessage);
				String strTweetUser = status.getUser().getScreenName();
				String userURL = status.getUser().getProfileImageURL();
				int authorOutreach = status.getUser().getFollowersCount();
				//System.out.println("**************************** AUTHOR OUTREACH IN RETWEETS:: " + authorOutreach);
				socialData.setAuthorOutreach(authorOutreach);

				socialData.setPostId(strTweetId);
				Util.prepareSocialDataBean(socialData, sourceDTO);
				//socialData.setSourceId(nSourceId);
				//socialData.setSourceTypeId(DBService.getSourceTypeId("Twitter"));
				//socialData.setShortnameId(DBService.getShortnameId("Twitter"));
				socialData.setAspectMap(aspectMap);
				socialData.setDateKey(_format.format(new Date()));
				socialData.setHourKey(_hourFormat.format(new Date()));
				socialData.setParentId(parentId);
				socialData.setPostTypeId(postTypeId4Retweet);
				socialData.setPostDateTime(strTweetDate);
				socialData.setPostText(strTweetMessage);
				socialData.setXpressoReview(socialData.getPostText());
				socialData.setHashId(Util.ConvertTomd5(strTweetMessage));
				socialData.setAuthorId(authorId);
				socialData.setAuthorName(strTweetUser.replaceAll("'", "\\'"));
				socialData.setUserURL(userURL);
				/*
				 * We are setting  as Retweet/Favourite count
				 * for a Retweet as a Retweet Reflects the Numbers of the
				 * original Tweet */

				socialData.setRetweetsCount(status.getRetweetCount());
				socialData.setLikesCount(status.getFavoriteCount());

				//Because getPlace is returning Null at times so throws Null PointerException on calling getCountry over null
				getLocation(socialData, status);

				String postURL = Constants.TWITTER_URL + strTweetUser + Constants.STATUS + strTweetId;
				socialData.setPostURL(postURL);
				retweetDatas.add(socialData);
			} catch (Exception e) {
				System.out.println("TwitterBolt: getting some error to extract every tweet ....continue");
				e.printStackTrace();
				continue;
			}
		}

		return retweetDatas;

	}

	private static void setProxyServer(Properties prop, ConfigurationBuilder cb) {
		if (prop.getProperty("http_proxy_enable").equalsIgnoreCase("Y")) {
			System.out.println("setting up proxy server...");
			cb.setHttpProxyHost(prop.getProperty("http_proxy_host"));
			cb.setHttpProxyPort(Integer.parseInt(prop.getProperty("http_proxy_port")));
			cb.setHttpProxyUser(prop.getProperty("http_proxy_user"));
			cb.setHttpProxyPassword(prop.getProperty("http_proxy_password"));
		} else {
			System.out.println("No proxy setup required...");
		}
	}

	public static void getLocation(SocialData socialData, Status status) {

		try {
			socialData.setPossiblySensitive(status.isPossiblySensitive());
			if (status.getUser() != null) {
				//Checking Author's Location
				if (status.getUser().getLocation() == null) {
					socialData.setUserLoc(status.getUser().getTimeZone());
				} else {
					socialData.setUserLoc(status.getUser().getLocation());
				}
			}
			//Because getPlace is returning Null at times so throws Null PointerException on calling getCountry over null
			if (status.getPlace() == null && status.getGeoLocation() == null && status.getUser() != null) {
				socialData.setTweetLoc(status.getUser().getTimeZone());
			} else if (status.getPlace() != null && status.getPlace().getCountry() != null) {
				socialData.setTweetLoc(status.getPlace().getCountry());
			} else if (status.getPlace() != null && status.getPlace().getCountry() == null) {
				socialData.setTweetLoc(status.getPlace().toString());
			} else if (status.getGeoLocation() != null) {
				socialData.setTweetLoc(status.getGeoLocation().toString());
			}

		} catch (NullPointerException e) {
			System.err.println("NULL POINTER ERROR in Twitter Locations ::" + e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			System.err.println("ERROR in Twitter Locations ::" + e.getMessage());
			e.printStackTrace();
		}
	}

	public static Twitter getTwitterObject(Properties _prop, SourceDTO sourceDTO) {
		Twitter obj = null;
		try {
			Map<String, Object> twitterMap = TwitterUtil.getOAuth2Token(_prop, sourceDTO);
			//twitterObj = TwitterUtil.getTwitter(_prop, sourceDTO.getSourceTypeId(), twitterMap);
			obj = TwitterUtil.getTwitter(_prop, sourceDTO.getSourceTypeId(), twitterMap);

		} catch (TwitterException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return obj;
	}

	public static String getExpandedURL(Properties prop, String shortURL) {
		String expandedURL = "";
		Map<String, List<String>> rheader;

		try {
			if (prop.getProperty("http_proxy_enable").equalsIgnoreCase("Y")) {
				System.out.println("Proxy setup to get Response Headers in Twitter");
				rheader = Util.getResponseHeaderFromProxy(prop, shortURL);

			} else {
				URL url = new URL(shortURL);
				HttpURLConnection huc = (HttpURLConnection) url.openConnection();
				huc.getResponseMessage();
				huc.getResponseCode();

				//System.out.println("*********RESPONSE on CONNECTING WITH THE WEBSITE :: " + );
				//System.out.println("*********STATUS on CONNECTING WITH THE WEBSITE :: " + huc.getResponseCode());
				huc.disconnect();

				rheader = huc.getRequestProperties();
			}

			for (Map.Entry<String, List<String>> field : rheader.entrySet()) {
				//System.out.println("KEY :: " + field.getKey());
				//System.out.println("Value :: " + field.getValue().toString());
				if (field.getKey() != null) {
					String text = field.getKey();
					if (text.contains("GET")) {
						text = text.replaceAll("GET", "");
						text = text.substring(0, text.indexOf("HTTP")).trim();
						if (text.startsWith("http")) {
							expandedURL = text;
							return expandedURL;
						} else {
							expandedURL = expandedURL + text;
						}

						System.out.println("EXPANDED URL :: " + expandedURL);
					} else if (text.contains("HOST") || text.contains("Host")) {
						String domain = field.getValue().get(0);
						System.out.println("HOST :: " + domain);
						domain = domain.trim().replace("", "").replace("]", "").trim();
						expandedURL = domain + expandedURL;
						System.out.println("********FINAL EXPANDED URL :: " + expandedURL);
					}

				}
			}
			//TODO call via Proxy to Expand the URL

		} catch (MalformedURLException e) {
			System.err.println("ERROR in short URL :: " + shortURL);
			System.err.println("Message :: " + e.getMessage());
		} catch (IOException e) {
			System.err.println("ERROR in Connecting with website .. ");
			System.err.println("Message :: " + e.getMessage());
		} catch (Exception e) {
			System.err.println("ERROR in Connecting with website .. ");
			System.err.println("Message :: " + e.getMessage());
			e.printStackTrace();
		}

		return expandedURL;
	}

}
