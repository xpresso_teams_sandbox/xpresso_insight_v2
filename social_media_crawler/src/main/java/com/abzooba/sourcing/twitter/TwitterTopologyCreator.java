package com.abzooba.sourcing.twitter;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;
import java.util.Properties;



/*import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.generated.AlreadyAliveException;
import backtype.storm.generated.InvalidTopologyException;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.utils.NimbusClient;
import backtype.storm.utils.Utils;*/
import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.StormSubmitter;
import org.apache.storm.generated.AlreadyAliveException;
import org.apache.storm.generated.InvalidTopologyException;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.utils.NimbusClient;
import org.apache.storm.utils.Utils;
import org.json.simple.JSONValue;

import com.abzooba.sourcing.common.ITopologyBuilder;
import com.abzooba.sourcing.common.util.Util;
import com.abzooba.sourcing.twitter.bolt.TwitterBolt;
import com.abzooba.sourcing.twitter.bolt.TwitterSaveBolt;
import com.abzooba.sourcing.twitter.spout.TwitterSpout;

public class TwitterTopologyCreator implements ITopologyBuilder {

	private Properties _prop = new Properties();

	public static void main(String args[]) throws ClassNotFoundException, IllegalAccessException, InstantiationException, SQLException, IOException, AlreadyAliveException, InvalidTopologyException {

		try {
			Properties prop = new Properties();
			prop.load(new FileInputStream(System.getProperty("propfile")));
			TwitterTopologyCreator twitter = new TwitterTopologyCreator();
			TopologyBuilder builder = twitter.topologyBuilder(prop);
			LocalCluster cluster = new LocalCluster();

			Config config = new Config();
			config.setDebug(true);
			config.put(Config.TOPOLOGY_WORKERS, 4);
			config.put("config.properties", System.getProperty("propfile"));

			cluster.submitTopology("tweetertopology", config,	builder.createTopology());

			Thread.sleep(30 * 60 * 1000);
			System.out.println("TwitterTopologyCreator: Topology submitted in Local cluster");
			//cluster.shutdown();
			
			TwitterTopologyCreator twitterTopology = new TwitterTopologyCreator();
			twitterTopology.createTopology("twittertopology");
			
		} catch (Exception exc) {
			exc.printStackTrace();
		}
	}

	public void localMode() {
		/*try {
			String strTopologyName = "testtopology";
			TopologyBuilder builder = topologyBuilder();
			
			Config conf = new Config();
			conf.setNumWorkers(2);
			conf.setMaxSpoutPending(5);
			StormSubmitter.submitTopology(strTopologyName, conf, builder.createTopology());

		} catch (Exception exc) {
			exc.printStackTrace();
		}*/
	}

	public TopologyBuilder topologyBuilder(Properties properties) {

		TopologyBuilder builder = null;
		try {

			builder = new TopologyBuilder();
			builder.setSpout("tweetspout", new TwitterSpout());
			builder.setBolt("twitterbolt", new TwitterBolt(), Integer.parseInt(properties.getProperty("twitter_process_bolt"))).setNumTasks(Integer.parseInt(properties.getProperty("twitter_worker_number"))).shuffleGrouping("tweetspout");
			builder.setBolt("twsavebolt", new TwitterSaveBolt(), Integer.parseInt(properties.getProperty("twitter_save_bolt"))).setNumTasks(Integer.parseInt(properties.getProperty("twitter_worker_number"))).shuffleGrouping("twitterbolt");
			System.out.println("Building topology for Twitter");

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return builder;
	}

	public Map topologyConfBuilder(Properties properties) {
		Map topologyConf = Utils.readStormConfig();
		topologyConf.put(Config.NIMBUS_HOST, properties.getProperty("nimbus_ip").trim());
		topologyConf.put(Config.TOPOLOGY_WORKERS, Util.stringToInt(properties.getProperty("twitter_worker_number").trim()));
		topologyConf.put(Config.TOPOLOGY_TASKS, Util.stringToInt(properties.getProperty("twitter_task_number").trim()));
		topologyConf.put("config.properties", System.getProperty("propfile"));

		return topologyConf;
	}

	public void createTopology(String strTopologyName) {
		try {
			//_prop.load(new FileInputStream(Constants.PROPERTIES_PATH));
			_prop.load(new FileInputStream(System.getProperty("propfile")));
			//_prop.load(TwitterTopologyCreator.class.getClassLoader().getResourceAsStream("config.properties"));
			//System.out.println("TwitterTopologyCreator: Proprties file loaded successfully");

			TopologyBuilder builder = topologyBuilder(_prop);
			Map topologyConf = topologyConfBuilder(_prop);

			String inputJar = _prop.getProperty("jar_location").trim();
			NimbusClient nimbus = new NimbusClient(topologyConf, _prop.getProperty("nimbus_ip").trim(), 6627);
			// upload topology jar to Cluster using StormSubmitter
			String uploadedJarLocation = StormSubmitter.submitJar(topologyConf, inputJar);
			System.out.println("TwitterTopologyCreator: jar uploading");

			String jsonConf = JSONValue.toJSONString(topologyConf);
			if (builder != null) {
				nimbus.getClient().submitTopology(strTopologyName, uploadedJarLocation, jsonConf, builder.createTopology());
				System.out.println("TwitterTopologyCreator: Topology submitted");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}