-- MySQL dump 10.13  Distrib 5.7.13, for Linux (x86_64)
--
-- Host: 52.23.170.75    Database: SMAx_DB
-- ------------------------------------------------------
-- Server version	5.7.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account_company_map`
--

DROP TABLE IF EXISTS `account_company_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_company_map` (
  `account_id` int(10) unsigned DEFAULT NULL,
  `company_id` int(10) unsigned DEFAULT NULL,
  `primary_company` int(1) unsigned DEFAULT NULL,
  `create_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `FK_account_map` (`account_id`),
  KEY `FK_company_map1` (`company_id`),
  CONSTRAINT `FK_account_map` FOREIGN KEY (`account_id`) REFERENCES `account_master` (`account_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_company_map1` FOREIGN KEY (`company_id`) REFERENCES `company_master` (`company_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_company_map`
--

LOCK TABLES `account_company_map` WRITE;
/*!40000 ALTER TABLE `account_company_map` DISABLE KEYS */;
INSERT INTO `account_company_map` VALUES (1,1,1,'2016-07-22 18:42:35','2016-07-22 18:42:35');
/*!40000 ALTER TABLE `account_company_map` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Table structure for table `account_master`
--

DROP TABLE IF EXISTS `account_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_master` (
  `account_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account_name` varchar(50) DEFAULT NULL,
  `plan_id` int(10) unsigned DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `expiry_date` datetime DEFAULT NULL,
  `create_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`account_id`),
  KEY `FK_plan_account` (`plan_id`),
  CONSTRAINT `FK_plan_account` FOREIGN KEY (`plan_id`) REFERENCES `plan_master` (`plan_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_master`
--

LOCK TABLES `account_master` WRITE;
/*!40000 ALTER TABLE `account_master` DISABLE KEYS */;
INSERT INTO `account_master` VALUES (1,'MCI Singapore',1,'2016-07-21 18:29:39','2017-07-22 18:29:39','2016-07-22 18:29:39','2016-07-22 18:42:05');
/*!40000 ALTER TABLE `account_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_master`
--

DROP TABLE IF EXISTS `company_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company_master` (
  `company_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_name` varchar(100) DEFAULT NULL,
  `domain_id` int(10) unsigned DEFAULT NULL,
  `create_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`company_id`),
  KEY `FK_company_domain` (`domain_id`),
  CONSTRAINT `FK_company_domain` FOREIGN KEY (`domain_id`) REFERENCES `domain_master` (`domain_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_master`
--

LOCK TABLES `company_master` WRITE;
/*!40000 ALTER TABLE `company_master` DISABLE KEYS */;
INSERT INTO `company_master` VALUES (1,'MCI Singapore',1,'2016-07-22 18:37:46','2016-07-22 18:37:46');
/*!40000 ALTER TABLE `company_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_source_map`
--

DROP TABLE IF EXISTS `company_source_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company_source_map` (
  `company_id` int(10) unsigned DEFAULT NULL,
  `source_id` int(10) unsigned DEFAULT NULL,
  `create_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `FK_company_map` (`company_id`),
  KEY `FK_source_map` (`source_id`),
  CONSTRAINT `FK_company_map` FOREIGN KEY (`company_id`) REFERENCES `company_master` (`company_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_source_map` FOREIGN KEY (`source_id`) REFERENCES `source_master` (`source_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_source_map`
--

LOCK TABLES `company_source_map` WRITE;
/*!40000 ALTER TABLE `company_source_map` DISABLE KEYS */;
INSERT INTO `company_source_map` VALUES (1,1,'2016-07-22 18:47:43','2016-07-22 18:47:43'),(1,2,'2016-07-22 18:47:43','2016-07-22 18:47:43'),(1,3,'2016-07-22 18:49:47','2016-07-22 18:49:47'),(1,4,'2016-07-22 18:49:47','2016-07-22 18:49:47'),(1,5,'2016-07-22 18:49:47','2016-07-22 18:49:47'),(1,6,'2016-07-22 18:49:47','2016-07-22 18:49:47'),(1,7,'2016-07-22 18:49:47','2016-07-22 18:49:47'),(1,8,'2016-07-22 18:49:47','2016-07-22 18:49:47'),(1,9,'2016-07-22 18:49:47','2016-07-22 18:49:47'),(1,10,'2016-07-22 18:49:47','2016-07-22 18:49:47'),(1,11,'2016-07-22 18:49:47','2016-07-22 18:49:47'),(1,12,'2016-07-22 18:49:47','2016-07-22 18:49:47'),(1,13,'2016-07-22 18:49:47','2016-07-22 18:49:47'),(1,14,'2016-07-22 18:49:47','2016-07-22 18:49:47'),(1,15,'2016-07-22 18:49:47','2016-07-22 18:49:47'),(1,16,'2016-07-22 18:49:47','2016-07-22 18:49:47'),(1,17,'2016-07-22 18:49:47','2016-07-22 18:49:47'),(1,18,'2016-07-22 18:49:47','2016-07-22 18:49:47'),(1,19,'2016-07-22 18:49:47','2016-07-22 18:49:47'),(1,20,'2016-07-22 18:49:47','2016-07-22 18:49:47'),(1,21,'2016-07-22 18:51:47','2016-07-22 18:51:47'),(1,22,'2016-07-22 18:51:47','2016-07-22 18:51:47'),(1,23,'2016-07-22 18:51:47','2016-07-22 18:51:47'),(1,24,'2016-07-22 18:51:47','2016-07-22 18:51:47'),(1,25,'2016-07-22 18:51:47','2016-07-22 18:51:47'),(1,26,'2016-07-22 18:51:47','2016-07-22 18:51:47');
/*!40000 ALTER TABLE `company_source_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `country_master`
--

DROP TABLE IF EXISTS `country_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country_master` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(2) NOT NULL DEFAULT '',
  `country_name` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `document_store`
--

DROP TABLE IF EXISTS `document_store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document_store` (
  `doc_id` varchar(100) NOT NULL,
  `source_id` int(5) unsigned DEFAULT NULL,
  `post_type_id` int(2) unsigned DEFAULT NULL,
  `doc_datetime` datetime DEFAULT NULL,
  `date_key` int(8) DEFAULT NULL,
  `hhour_key` int(4) DEFAULT NULL,
  `doc_text` longtext,
  `author_id` varchar(10) DEFAULT NULL,
  `author_name` varchar(255) DEFAULT NULL,
  `doc_location` text,
  `create_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`doc_id`),
  KEY `FK_post_type_doc` (`post_type_id`),
  KEY `FK_source_doc` (`source_id`),
  CONSTRAINT `FK_post_type_doc` FOREIGN KEY (`post_type_id`) REFERENCES `post_types` (`post_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_source_doc` FOREIGN KEY (`source_id`) REFERENCES `source_master` (`source_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `domain_master`
--

DROP TABLE IF EXISTS `domain_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `domain_master` (
  `domain_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `domain_name` varchar(100) DEFAULT NULL,
  `xpresso_domain` varchar(10) DEFAULT NULL,
  `xpresso_annotation` varchar(10) DEFAULT NULL,
  `create_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`domain_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `domain_master`
--

LOCK TABLES `domain_master` WRITE;
/*!40000 ALTER TABLE `domain_master` DISABLE KEYS */;
INSERT INTO `domain_master` VALUES (1,'GOVERNMENT','government','expr_trend','2016-07-22 15:12:03','2016-07-22 15:12:03');
/*!40000 ALTER TABLE `domain_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kpi_happiness_index`
--

DROP TABLE IF EXISTS `kpi_happiness_index`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kpi_happiness_index` (
  `source_id` int(5) DEFAULT NULL,
  `date_key` int(8) DEFAULT NULL,
  `hour_key` int(4) DEFAULT NULL,
  `month_key` int(6) DEFAULT NULL,
  `index_count` int(10) DEFAULT NULL,
  `sentiment` varchar(50) DEFAULT NULL,
  `create_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `page_info`
--

DROP TABLE IF EXISTS `page_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_info` (
  `page_id` varchar(100) NOT NULL,
  `source_id` int(10) unsigned DEFAULT NULL,
  `page_like` bigint(20) DEFAULT NULL,
  `people_talking_about` bigint(20) DEFAULT NULL,
  `total_followings` bigint(20) DEFAULT NULL,
  `total_follower` bigint(20) DEFAULT NULL,
  `page_url` varchar(255) DEFAULT NULL,
  `create_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`page_id`),
  KEY `FK_page_source` (`source_id`),
  CONSTRAINT `FK_page_source` FOREIGN KEY (`source_id`) REFERENCES `source_master` (`source_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `plan_master`
--

DROP TABLE IF EXISTS `plan_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plan_master` (
  `plan_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `plan_name` varchar(50) DEFAULT NULL,
  `max_users` int(3) DEFAULT NULL,
  `max_competitors` int(3) DEFAULT NULL,
  `max_sources` int(3) DEFAULT NULL,
  `max_keywords` int(3) DEFAULT NULL,
  `pricing` float DEFAULT NULL,
  `create_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`plan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plan_master`
--

LOCK TABLES `plan_master` WRITE;
/*!40000 ALTER TABLE `plan_master` DISABLE KEYS */;
INSERT INTO `plan_master` VALUES (1,'Gold',3,3,100,5,3000,'2016-07-22 18:28:03','2016-07-22 18:28:03');
/*!40000 ALTER TABLE `plan_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_types`
--

DROP TABLE IF EXISTS `post_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_types` (
  `post_type_id` int(2) unsigned NOT NULL AUTO_INCREMENT,
  `source_type_id` int(2) unsigned DEFAULT NULL,
  `post_type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`post_type_id`),
  KEY `FK_source_type_post_type` (`source_type_id`),
  CONSTRAINT `FK_source_type_post_type` FOREIGN KEY (`source_type_id`) REFERENCES `source_types` (`source_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_types`
--

LOCK TABLES `post_types` WRITE;
/*!40000 ALTER TABLE `post_types` DISABLE KEYS */;
INSERT INTO `post_types` VALUES (1,1,'Post'),(2,1,'Comment'),(3,2,'Tweet'),(4,3,'News');
/*!40000 ALTER TABLE `post_types` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Table structure for table `role_master`
--

DROP TABLE IF EXISTS `role_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_master` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) NOT NULL DEFAULT '',
  `description` varchar(500) NOT NULL DEFAULT '',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `social_data`
--

DROP TABLE IF EXISTS `social_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_data` (
  `post_id` varchar(100) NOT NULL DEFAULT '',
  `parent_id` varchar(100) NOT NULL DEFAULT '',
  `source_id` int(5) unsigned DEFAULT NULL,
  `post_type_id` int(2) unsigned DEFAULT NULL,
  `post_datetime` datetime DEFAULT NULL,
  `date_key` int(8) DEFAULT NULL,
  `hour_key` int(4) DEFAULT NULL,
  `post_text` longtext,
  `headline` text,
  `breaking_news` char(1) DEFAULT '0',
  `comment_count` int(10) DEFAULT NULL,
  `likes_count` int(10) DEFAULT NULL,
  `share_count` int(10) DEFAULT NULL,
  `retweet_count` int(10) DEFAULT NULL,
  `author_id` varchar(100) DEFAULT NULL,
  `author_name` varchar(255) DEFAULT NULL,
  `post_url` text,
  `is_updated` tinyint(1) DEFAULT '0',
  `is_analyzed` tinyint(1) DEFAULT '0',
  `overall_sentiment` varchar(50) DEFAULT NULL,
  `overall_sentiment_score` decimal(6,4) DEFAULT NULL,
  `create_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`post_id`,`parent_id`),
  KEY `FK_post_type_post` (`post_type_id`),
  KEY `FK_source_post` (`source_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `source_master`
--

DROP TABLE IF EXISTS `source_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `source_master` (
  `source_id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `source_type_id` int(2) unsigned DEFAULT NULL,
  `domain_id` int(10) NOT NULL,
  `page_name` varchar(50) DEFAULT NULL,
  `source_category` varchar(100) DEFAULT NULL,
  `source_url` varchar(255) DEFAULT NULL,
  `live_source` tinyint(1) NOT NULL DEFAULT '0',
  `xpresso_enable` int(1) DEFAULT NULL,
  `live_status` int(1) DEFAULT '0',
  `last_streaming` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_streaming_comment` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `create_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`source_id`),
  KEY `FK_crawl_source_type` (`source_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `source_master`
--

LOCK TABLES `source_master` WRITE;
/*!40000 ALTER TABLE `source_master` DISABLE KEYS */;
INSERT INTO `source_master` VALUES (1,3,1,'Straits Times Singapore','mci-rss-1','http://www.straitstimes.com/news/singapore/rss.xml',1,1,1,'2016-07-29 06:53:45','2016-07-22 15:47:47','2016-07-22 15:47:47','2016-07-29 06:53:45'),(2,3,1,'Straits Times Lifestyle','mci-rss-1','http://www.straitstimes.com/news/lifestyle/rss.xml',1,1,1,'2016-07-29 06:53:45','2016-07-22 15:49:27','2016-07-22 15:49:27','2016-07-29 06:53:45'),(3,3,1,'Straits Times Forum','mci-rss-1','http://www.straitstimes.com/news/forum/rss.xml',1,1,1,'2016-07-29 06:53:45','2016-07-22 15:49:27','2016-07-22 15:49:27','2016-07-29 06:53:45'),(4,3,1,'Straits Times Opinion','mci-rss-1','http://www.straitstimes.com/news/opinion/rss.xml',1,1,1,'2016-07-29 06:53:45','2016-07-22 15:49:27','2016-07-22 15:49:27','2016-07-29 06:53:45'),(5,3,1,'Straits Times Business','mci-rss-1','http://www.straitstimes.com/news/business/rss.xml',1,1,0,'2016-07-29 06:53:45','2016-07-22 15:53:41','2016-07-22 15:53:41','2016-07-29 06:53:45'),(6,3,1,'Straits Times Sport','mci-rss-1','http://www.straitstimes.com/news/sport/rss.xml',1,1,0,'2016-07-29 06:53:45','2016-07-22 15:53:41','2016-07-22 15:53:41','2016-07-29 06:53:45'),(7,3,1,'Straits Times Tech','mci-rss-1','http://www.straitstimes.com/news/tech/rss.xml',1,1,0,'2016-07-29 06:53:45','2016-07-22 15:53:41','2016-07-22 15:53:41','2016-07-29 06:53:45'),(8,1,1,'Straits Times FB','mci-fb-1','TheStraitsTimes',1,1,1,'2016-07-29 06:23:44','2016-07-27 20:13:42','2016-07-22 15:53:41','2016-07-29 06:23:44'),(9,1,1,'Channel News Asia FB','mci-fb-1','ChannelNewsAsia',1,1,1,'2016-07-29 06:23:44','2016-07-27 20:13:42','2016-07-22 15:53:41','2016-07-29 06:23:44'),(10,1,1,'Today','mci-fb-1','todayonline',1,1,1,'2016-07-29 06:23:44','2016-07-27 20:13:42','2016-07-22 15:53:41','2016-07-29 06:23:44'),(11,1,1,'Yahoo Singapore','mci-fb-1','YahooSingapore',1,1,1,'2016-07-29 06:23:44','2016-07-27 20:13:42','2016-07-22 15:53:41','2016-07-29 06:23:44'),(12,1,1,'The Online Citizen','mci-fb-1','theonlinecitizen',1,1,1,'2016-07-29 06:23:44','2016-07-27 20:13:42','2016-07-22 15:53:41','2016-07-29 06:23:44'),(13,1,1,'TR Emeritus','mci-fb-1','TREmeritus',1,1,1,'2016-07-29 06:23:44','2016-07-27 20:13:42','2016-07-22 15:53:41','2016-07-29 06:23:44'),(14,2,1,'Channel News Asia','mci-tw-1','@ChannelNewsAsia',1,1,2,'2016-07-29 06:54:01','2016-07-22 15:53:41','2016-07-22 15:53:41','2016-07-29 06:54:01'),(15,2,1,'TODAY online','mci-tw-1','@TODAYonline',1,1,2,'2016-07-29 06:54:01','2016-07-22 15:57:26','2016-07-22 15:57:26','2016-07-29 06:54:01'),(16,2,1,'Straits Times Tweet','mci-tw-1','@STcom',1,1,2,'2016-07-29 06:54:01','2016-07-22 15:57:26','2016-07-22 15:57:26','2016-07-29 06:54:01'),(17,2,1,'Peoples Power Party','mci-tw-1','@PeoplesPowerSG',1,1,2,'2016-07-29 06:54:01','2016-07-22 15:57:26','2016-07-22 15:57:26','2016-07-29 06:54:01'),(18,2,1,'Singaporeans First','mci-tw-1','@SFPSingapore',1,1,2,'2016-07-29 06:54:01','2016-07-22 15:57:26','2016-07-22 15:57:26','2016-07-29 06:54:01'),(19,2,1,'ng yi shu','mci-tw-1','@theyishusblog',1,1,2,'2016-07-29 06:54:01','2016-07-22 15:57:26','2016-07-22 15:57:26','2016-07-29 06:54:01'),(20,1,1,'The Indendent Singapore','mci-fb-1','TheIndependentSG',1,1,1,'2016-07-29 06:23:44','2016-07-27 20:13:42','2016-07-22 15:57:26','2016-07-29 06:23:44'),(21,1,1,'The Middle Ground','mci-fb-1','TheMiddleGroundSG',1,1,1,'2016-07-29 06:23:44','2016-07-27 20:13:42','2016-07-22 15:57:26','2016-07-29 06:23:44'),(22,1,1,'All Singapore Stuff','mci-fb-1','allsgstuff',1,1,1,'2016-07-29 06:23:44','2016-07-27 20:13:42','2016-07-22 15:57:26','2016-07-29 06:23:44'),(23,2,1,'Leong Sze Hian','mci-tw-1','@leongszehian',1,1,2,'2016-07-29 06:54:01','2016-07-22 15:57:26','2016-07-22 15:57:26','2016-07-29 06:54:01'),(24,2,1,'HOME Singapore','mci-tw-1','@home_org_sg',1,1,2,'2016-07-29 06:54:01','2016-07-22 15:57:26','2016-07-22 15:57:26','2016-07-29 06:54:01'),(25,3,1,'Straits Times Politics','mci-rss-1','http://www.straitstimes.com/news/politics/rss.xml',1,1,0,'2016-07-29 06:53:45','2016-07-22 15:57:26','2016-07-22 15:57:26','2016-07-29 06:53:45'),(26,2,1,'Democratic Progressive Party','mci-tw-1','@dpp_sg',1,1,2,'2016-07-29 06:54:01','2016-07-22 15:57:26','2016-07-22 15:57:26','2016-07-29 06:54:01'),(27,3,1,'Channel News Asia','mci-rss-1','http://www.channelnewsasia.com/starterkit/servlet/cna/rss/singapore.xml',1,1,0,'2016-07-29 06:53:45','2016-07-24 00:00:00','2016-07-26 09:12:47','2016-07-29 06:53:45'),(28,3,1,'Today Online Singapore','mci-rss-1','http://www.todayonline.com/feed/singapore',1,1,0,'2016-07-29 06:53:45','2016-07-24 00:00:00','2016-07-26 09:15:17','2016-07-29 06:53:45'),(29,3,1,'Today Online Commentary','mci-rss-1','http://www.todayonline.com/feed/Commentary',1,1,0,'2016-07-29 06:53:45','2016-07-24 00:00:00','2016-07-26 09:16:02','2016-07-29 06:53:45'),(30,3,1,'Today Online Voices','mci-rss-1','http://www.todayonline.com/feed/Voices',1,1,0,'2016-07-29 06:53:45','2016-07-24 00:00:00','2016-07-26 09:17:03','2016-07-29 06:53:45'),(31,3,1,'The Online Citizen','mci-rss-1','http://www.theonlinecitizen.com/feed/',1,1,0,'2016-07-29 06:53:45','2016-07-24 00:00:00','2016-07-26 09:18:55','2016-07-29 06:53:45'),(32,3,1,'Yahoo News','mci-rss-1','https://sg.news.yahoo.com/rss/singapore',1,1,0,'2016-07-29 06:53:45','2016-07-24 00:00:00','2016-07-26 09:19:42','2016-07-29 06:53:45'),(33,1,1,'States Times Review','mci-fb-1','STReview',1,1,1,'2016-07-29 06:23:44','2016-07-27 20:13:42','2016-07-26 11:20:15','2016-07-29 06:23:44'),(34,1,1,'leehsienloong','mci-fb-1','leehsienloong',1,1,1,'2016-07-29 06:23:44','2016-07-27 20:13:42','2016-07-26 11:22:11','2016-07-29 06:23:44'),(35,1,1,'wakeupSG','mci-fb-1','wakeupSG',1,1,1,'2016-07-29 06:23:44','2016-07-27 20:13:42','2016-07-26 11:22:38','2016-07-29 06:23:44'),(36,1,1,'pap.sg','mci-fb-1','pap.sg',1,1,2,'2016-07-29 06:53:44','2016-07-27 20:13:42','2016-07-26 11:23:27','2016-07-29 06:53:44'),(37,1,1,'workersparty','mci-fb-1','workersparty',1,1,2,'2016-07-29 06:53:44','2016-07-27 20:13:41','2016-07-26 11:23:51','2016-07-29 06:53:44'),(38,1,1,'yoursdp','mci-fb-1','yoursdp',1,1,2,'2016-07-29 06:53:44','2016-07-27 20:13:42','2016-07-26 11:24:16','2016-07-29 06:53:44'),(39,1,1,'thereformparty','mci-fb-1','thereformparty',1,1,1,'2016-07-29 06:23:44','2016-07-24 00:00:00','2016-07-26 11:24:34','2016-07-29 06:23:44'),(40,1,1,'SingaporePeoplesParty','mci-fb-1','SingaporePeoplesParty',1,1,1,'2016-07-29 06:23:44','2016-07-24 00:00:00','2016-07-26 11:24:51','2016-07-29 06:23:44'),(41,1,1,'cheesoonjuan','mci-fb-1','cheesoonjuan',1,1,2,'2016-07-29 06:53:44','2016-07-27 20:13:42','2016-07-26 11:25:12','2016-07-29 06:53:44'),(42,1,1,'kjeyaretnamRP','mci-fb-1','kjeyaretnamRP',1,1,1,'2016-07-29 06:23:44','2016-07-24 00:00:00','2016-07-26 11:25:31','2016-07-29 06:23:44'),(43,2,1,'MARUAH','mci-tw-1','@maruahsg',1,1,2,'2016-07-29 06:54:01','2016-07-24 00:00:00','2016-07-26 11:26:58','2016-07-29 06:54:01'),(44,2,1,'Chee Soon Juan','mci-tw-1','@CheeSoonJuan',1,1,2,'2016-07-29 06:54:01','2016-07-24 00:00:00','2016-07-26 11:27:36','2016-07-29 06:54:01'),(45,2,1,'AWARE Singapore','mci-tw-1','@awarenews',1,1,2,'2016-07-29 06:54:01','2016-07-24 00:00:00','2016-07-26 11:28:40','2016-07-29 06:54:01'),(46,2,1,'Jeannette C Aruldoss','mci-tw-1','@JCAMountbatten',1,1,2,'2016-07-29 06:54:01','2016-07-24 00:00:00','2016-07-26 11:29:53','2016-07-29 06:54:01'),(47,2,1,'Tan Kin Lian','mci-tw-1','@kinlian',1,1,2,'2016-07-29 06:54:01','2016-07-24 00:00:00','2016-07-26 11:30:36','2016-07-29 06:54:01'),(48,2,1,'Wake Up, Singapore','mci-tw-1','@wakeupsg',1,1,2,'2016-07-29 06:54:01','2016-07-24 00:00:00','2016-07-26 11:35:57','2016-07-29 06:54:01'),(49,2,1,'Kenneth Jeyaretnam','mci-tw-1','@KenJeyaretnam',1,1,2,'2016-07-29 06:54:01','2016-07-24 00:00:00','2016-07-26 11:37:05','2016-07-29 06:54:01'),(50,2,1,'Tan Jee Say','mci-tw-1','@JeeSayTan',1,1,2,'2016-07-29 06:54:01','2016-07-24 00:00:00','2016-07-26 11:38:15','2016-07-29 06:54:01'),(51,2,1,'Kirsten Han','mci-tw-1','@kixes',1,1,2,'2016-07-29 06:54:01','2016-07-24 00:00:00','2016-07-26 11:38:58','2016-07-29 06:54:01'),(52,2,1,'New Nation','mci-tw-1','@NewNationsg',1,1,2,'2016-07-29 06:54:01','2016-07-24 00:00:00','2016-07-26 11:49:43','2016-07-29 06:54:01');
/*!40000 ALTER TABLE `source_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `source_types`
--

DROP TABLE IF EXISTS `source_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `source_types` (
  `source_type_id` int(2) unsigned NOT NULL AUTO_INCREMENT,
  `source_type_code` varchar(10) DEFAULT NULL,
  `source_type_name` varchar(100) DEFAULT NULL,
  `create_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`source_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `source_types`
--

LOCK TABLES `source_types` WRITE;
/*!40000 ALTER TABLE `source_types` DISABLE KEYS */;
INSERT INTO `source_types` VALUES (1,'FB','Facebook','2016-07-22 15:02:28','2016-07-22 15:02:28'),(2,'Twitter','Twitter','2016-07-22 15:02:28','2016-07-22 15:02:28'),(3,'RSS','Straits Times','2016-07-22 15:02:28','2016-07-25 13:49:53');
/*!40000 ALTER TABLE `source_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `task_master`
--

DROP TABLE IF EXISTS `task_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `task_master` (
  `task_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `source_id` int(10) unsigned DEFAULT NULL,
  `task_config` text COMMENT 'Task config JSON',
  `crawling_status` int(1) DEFAULT '0',
  `xpresso_status` int(1) DEFAULT '0',
  `run_frequency` int(3) DEFAULT '0',
  `warehousing_status` int(1) unsigned DEFAULT '0',
  `task_status` int(1) DEFAULT '0',
  `create_time` varchar(50) DEFAULT NULL,
  `process_start_time` datetime DEFAULT NULL,
  `process_end_time` datetime DEFAULT NULL,
  `previous_run` datetime DEFAULT NULL,
  `create_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ticket_details`
--

DROP TABLE IF EXISTS `ticket_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ticket_details` (
  `ticket_id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` varchar(100) NOT NULL DEFAULT '',
  `assigner_id` varchar(10) DEFAULT NULL,
  `assignee_id` varchar(10) DEFAULT NULL,
  `comments` longtext,
  `ticket_status` varchar(10) DEFAULT NULL,
  `ticket_priority` varchar(10) DEFAULT NULL,
  `create_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `due_datetime` datetime DEFAULT NULL,
  `closed_datetime` datetime DEFAULT NULL,
  `update_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ticket_id`),
  KEY `FK_post_map` (`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `timezone_master`
--

DROP TABLE IF EXISTS `timezone_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timezone_master` (
  `timezone_id` int(11) NOT NULL AUTO_INCREMENT,
  `timezone` varchar(100) NOT NULL DEFAULT '',
  `display` varchar(500) NOT NULL DEFAULT '',
  `offset` char(8) NOT NULL DEFAULT '',
  PRIMARY KEY (`timezone_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `token_master`
--

DROP TABLE IF EXISTS `token_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `token_master` (
  `token_id` int(3) NOT NULL AUTO_INCREMENT,
  `source_type_id` int(2) NOT NULL,
  `app_key` varchar(50) NOT NULL,
  `app_secret` varchar(100) NOT NULL,
  `status` tinyint(3) NOT NULL DEFAULT '0',
  `create_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`token_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `token_master`
--

LOCK TABLES `token_master` WRITE;
/*!40000 ALTER TABLE `token_master` DISABLE KEYS */;
INSERT INTO `token_master` VALUES (1,1,'795173247244740','c4f6112c07789d662a43def6a96454b0',1,'2016-07-22 15:24:46','2016-07-29 05:53:44'),(3,2,'Kg6Km4LVRRBPGxTvMwq2dBk9u','aYuXJ3q9tiR89HUoVZ9sh0LMqvWV2RLjS19G3fouF4J4EAfKfM',1,'2016-07-22 15:24:46','2016-07-29 05:24:00'),(4,1,'333849450089382','3bb4748e735e0f992e9907e62770b08d',1,'2016-07-22 15:24:46','2016-07-29 06:53:44'),(5,1,'558376047579830','465f6410ce5cce6505b4a1cf52f93cd8',0,'2016-07-22 15:24:46','2016-07-29 05:24:00'),(6,1,'191585361047997','2ad35fe84c5ed2e8ccc8f54176d12596',0,'2016-07-22 15:24:46','2016-07-29 02:53:59'),(7,1,'602665919789200','af396a45b2ece6e7a69fbf7f5ffea0b1',0,'2016-07-22 15:24:46','2016-07-27 23:23:47'),(8,1,'395149320614557','c3ab996151253fb6174f8b6a5bf14034',0,'2016-07-22 15:24:46','2016-07-27 20:53:35'),(9,1,'900544246735430','1d7fc1fdb623c4f4ff3a618e29cd2025',0,'2016-07-22 15:24:46','2016-07-27 20:53:35'),(10,2,'ep7wLRkVyG6ugdShERWMg','BiV2AWyPjTTcBM3E4SpCpNDr9KIgU1k8G9AHQReo',1,'2016-07-22 15:24:46','2016-07-29 05:54:01'),(11,2,'QKSdyDqieDhEkrtQjQ8kkg','B1cd4G1S4XwG4d6MwchiMX1CVlAKFb65pspj8rYkA',1,'2016-07-22 15:24:46','2016-07-29 06:24:01'),(12,2,'xqLBWfArx41RNRwIrhJkwoaKF','BumhzmFwpdcg5tYFOXgiyj7fLTTMFzbwvx1bfCd2773UI4WJmE',1,'2016-07-22 15:24:46','2016-07-29 06:54:01'),(13,2,'Kg6Km4LVRRBPGxTvMwq2dBk9u','aYuXJ3q9tiR89HUoVZ9sh0LMqvWV2RLjS19G3fouF4J4EAfKfM',0,'2016-07-22 15:24:46','2016-07-29 05:24:00');
/*!40000 ALTER TABLE `token_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trending_topics`
--

DROP TABLE IF EXISTS `trending_topics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trending_topics` (
  `topic_id` int(10) NOT NULL AUTO_INCREMENT,
  `topic` varchar(250) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL,
  `score` decimal(20,15) DEFAULT NULL,
  `create_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`topic_id`)
) ENGINE=InnoDB AUTO_INCREMENT=43144456 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_master`
--

DROP TABLE IF EXISTS `user_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_master` (
  `user_id` varchar(100) NOT NULL COMMENT 'USER EMAIL',
  `user_name` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `company_name` varchar(100) DEFAULT NULL,
  `designation` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `domain_id` int(10) unsigned DEFAULT NULL,
  `timezone_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `isConfirmed` smallint(11) DEFAULT '0',
  `app_name` varchar(50) DEFAULT NULL,
  `create_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_token` varchar(20) NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `FK_country_user` (`country_id`),
  KEY `FK_domain_user` (`domain_id`),
  KEY `FK_role_user` (`role_id`),
  KEY `FK_timezone_user` (`timezone_id`),
  CONSTRAINT `FK_country_user` FOREIGN KEY (`country_id`) REFERENCES `country_master` (`country_id`),
  CONSTRAINT `FK_domain_user` FOREIGN KEY (`domain_id`) REFERENCES `domain_master` (`domain_id`),
  CONSTRAINT `FK_role_user` FOREIGN KEY (`role_id`) REFERENCES `role_master` (`role_id`),
  CONSTRAINT `FK_timezone_user` FOREIGN KEY (`timezone_id`) REFERENCES `timezone_master` (`timezone_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xpresso_output`
--

DROP TABLE IF EXISTS `xpresso_output`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xpresso_output` (
  `source_id` int(5) NOT NULL,
  `post_id` varchar(100) NOT NULL,
  `aspect` varchar(50) NOT NULL DEFAULT '',
  `sentiment` varchar(50) NOT NULL DEFAULT '',
  `entity` varchar(250) DEFAULT NULL,
  `statement_type` varchar(50) NOT NULL DEFAULT '',
  `emotion` varchar(50) DEFAULT NULL,
  `snippet` text,
  `intent` varchar(50) DEFAULT NULL,
  `intent_classification` varchar(50) DEFAULT NULL,
  `xpresso_domain` varchar(50) DEFAULT NULL,
  `create_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping events for database 'SMAx_DB'
--

CREATE TABLE post_entity_list ( 
  `post_id` varchar(100) NOT NULL, 
  `source_id` integer DEFAULT 0, 
  `entity` varchar(255) DEFAULT NULL, 
  `create_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE INDEX indx_post_id_entity ON post_entity_list (post_id);

--
-- Dumping routines for database 'SMAx_DB'
--
/*!50003 DROP PROCEDURE IF EXISTS `sp_brand_comparission_24Hour` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `sp_brand_comparission_24Hour`( 
  inAccountId int,
  currentTime datetime,
  selectedInterval varchar(255)
  
)
Begin

  declare curentdt, stdt datetime;
  
  
  declare accountName, companyName varchar(255);
  declare company_ID, primary_company_id int (10);
  declare kpi_score decimal(2,1);
  
  DECLARE v_finished INTEGER DEFAULT 0;
  
  declare v_max int unsigned default 24;
  declare v_counter int unsigned default 1;
  declare tmp_hours, tbl_brand_score text ;
  
  DEClARE cur_company CURSOR FOR
  select distinct c.company_id,c.company_name,m.primary_company
  from account_company_map m, company_master c
  where m.company_id = c.company_id and m.account_id= inAccountId;
  
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1; 
  
  if currentTime is null then
    
    set curentdt = DATE_ADD(now(),INTERVAL 5.5 Hour);
    
  else
    set curentdt = currentTime;
  end if;
  
  
  set stdt = curentdt;
  
	select account_name into accountName FROM account_master where account_id = inAccountId;
  
	set tmp_hours = CONCAT('tmp_hours_',accountName);
	DROP TEMPORARY TABLE IF EXISTS tmp_hours;
	CREATE TEMPORARY TABLE tmp_hours ENGINE=MEMORY as select curentdt current_datetime,hour(curentdt) as current_hour;
	
  
  if selectedInterval = 'Daily' then
	set v_counter = 1;
  set v_max = 25;
	myloop:while v_counter < v_max do
    
		  set stdt = DATE_ADD(stdt, INTERVAL -1 Hour);
    
      
    
   
		set v_counter=v_counter+1;
		INSERT into tmp_hours values (stdt,hour(stdt));
   
   
	end while;
  
	elseif selectedInterval = 'Weekly' then
  set v_counter = 1;
  set v_max = 7;
	myloop:while v_counter < v_max do
    
		  set stdt = DATE_ADD(stdt, INTERVAL -1 Day);
    
      
    
   
		set v_counter=v_counter+1;
		INSERT into tmp_hours values (stdt,hour(stdt));
   
   
	end while;
	
  elseif selectedInterval = 'Monthly' then
  set v_counter = 1;
  set v_max = 30;
	myloop:while v_counter < v_max do
    
		  set stdt = DATE_ADD(stdt, INTERVAL -1 Day);
    
      
    
   
		set v_counter=v_counter+1;
		INSERT into tmp_hours values (stdt,hour(stdt));
   
   
	end while;
	end if;
	set tbl_brand_score = CONCAT('tbl_brand_score_',accountName);
	DROP TEMPORARY TABLE IF EXISTS tbl_brand_score;
	CREATE TEMPORARY TABLE tbl_brand_score ENGINE=MEMORY as
	select -1 as account_id
  ,'XXXXXXXXXXXXXXXXXXX' as account_name
  ,-1 as company_id
  ,'XXXXXXXXXXXXXXXXXXX' as company_name 
  ,0 as primary_company 
  ,date_format(now(),'%d-%m-%Y %H:%i') as current_datetime
  
  ,0 as current_hour
  ,0 as Positive
	,0 as Negative
  ,0 as brand_score
  ,0 as post_count;	
	
  
  OPEN cur_company;
  get_val : LOOP 
  
    FETCH cur_company INTO company_ID,companyName,primary_company_id;
    IF v_finished = 1 THEN 
      LEAVE get_val;
    END IF;
    
	Insert into tbl_brand_score 
  (account_id,account_name,company_id,company_name,primary_company,current_datetime,current_hour,
  Positive,
	Negative,
  brand_score,post_count)
	Select Cast(inAccountId as UNSIGNED) as account_id
    ,accountName as account_name
    ,Cast(company_ID as UNSIGNED) as company_id
		,companyName as company_name
		,Cast(primary_company_id as UNSIGNED) as primary_company
		,tmp.current_datetime
    
		,Cast(tmp.current_hour as UNSIGNED) as current_hour
		,IfNull(base.Positive,0) as Positive
		,IfNull(base.Negative,0) as Negative
		,round((IfNull(base.Positive,0) - IfNull(base.Negative,0))/10,1) as brand_score
		,IfNull(base.post_count,0) as post_count
	from tmp_hours tmp 
	left outer join 
	(
		select 
   xp.post_date
		,xp.post_hr
		
	  
		,ROUND((COUNT(
					CASE 
						WHEN Rtrim(Ltrim(xp.overall_sentiment))='Positive' 
						THEN 1 
						ELSE NULL 
					END
				)/Count(xp.post_id))*100,1) AS 'Positive',
			ROUND((COUNT(
					CASE 
						WHEN Rtrim(Ltrim(xp.overall_sentiment))='Negative' 
						THEN 1 
						ELSE NULL 
					END
			)/Count(xp.post_id))*100,1) AS 'Negative',
			ROUND((COUNT(
				CASE 
					WHEN Rtrim(Ltrim(xp.overall_sentiment))='Neutral' 
					THEN 1 
					ELSE NULL 
				END
			)/Count(xp.post_id))*100,1) AS 'Neutral',
			count(xp.post_id) as post_count
			from 
		  (
		  
				select distinct s.post_id,s.source_id,s.post_datetime, s.overall_sentiment
				,date_format(s.post_datetime,'%d-%m-%Y') post_date
				,Hour(s.post_datetime) post_hr
				from social_data s 
				,xpresso_output x
				where s.post_id = x.post_id 
				and x.aspect<>'Overall'
				and s.post_datetime between stdt and curentdt    
		  ) xp
		  ,company_source_map m
		  where xp.source_id = m.source_id 
		  and m.company_id = company_ID 
		  group by 1,2
		) base
		on (date_format(tmp.current_datetime,'%d-%m-%Y') = base.post_date 
		and tmp.current_hour = base.post_hr )	
	  order by tmp.current_datetime;
	

		
  END LOOP get_val;
  CLOSE cur_company;  
		
 
 select * from tbl_brand_score ;
 
End ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_brand_comparission_monthly` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `sp_brand_comparission_monthly`(
  inAccountId int
)
Begin
  
  Select company_id,company_name, date_format(dt , '%d-%m-%Y') as post_date,
  round(avg(KPI_score),1) as monthly_score, dt
  from (
  	Select company_id,company_name,
    STR_TO_DATE(dt , '%d-%m-%Y %H:%s') as dt ,hr,
    
  	round((Positive - Negative)/10,1) as KPI_score
  	from (
  	select c.company_id, c.company_name,
    date_format(xp.post_datetime , '%d-%m-%Y') as dt ,
    date_format(xp.post_datetime,'%H') as hr ,
  	ROUND((COUNT(
  			CASE 
  				WHEN Rtrim(Ltrim(xp.overall_sentiment))='Positive' 
  				THEN 1 
  				ELSE NULL 
  			END
  		)/Count(xp.post_id))*100,1) AS 'Positive',
  	ROUND((COUNT(
  			CASE 
  				WHEN Rtrim(Ltrim(xp.overall_sentiment))='Negative' 
  				THEN 1 
  				ELSE NULL 
  			END
  	)/Count(xp.post_id))*100,1) AS 'Negative',
  	ROUND((COUNT(
  		CASE 
  			WHEN Rtrim(Ltrim(xp.overall_sentiment))='Neutral' 
  			THEN 1 
  			ELSE NULL 
  		END
  	)/Count(xp.post_id))*100,1) AS 'Neutral',
  	count(xp.post_id) as post_count
  	
    from 
    (
      select distinct s.post_id,s.source_id,s.post_datetime,s.overall_sentiment
      from social_data s  ,xpresso_output x
      where s.post_id = x.post_id 
      and x.aspect<>'Overall'
      and s.post_datetime between DATE_ADD(now(), INTERVAL -30 Day) and now()
    ) xp
    , company_master c
    ,company_source_map m
    ,account_company_map ac
  	where xp.source_id = m.source_id 
    and m.company_id = c.company_id 
    and ac.company_id = c.company_id
    and ac.account_id = inAccountId
  	group by 1,2,3,4
  	) a
  	
  ) mm
  group by 1,2,3,5
  order by 1 asc, 5 desc
  	;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_create_update_ticket` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `sp_create_update_ticket`(
  in_ticket_id INT,
  in_post_id VARCHAR(50),
  in_assigner_id VARCHAR(20),
  in_assignee_id VARCHAR(20),
  in_comments TEXT,
  in_ticket_priority VARCHAR(20),
  in_ticket_status VARCHAR(20),
  in_due_date DATE,
  in_closed_date DATE
)
BEGIN
  
  DECLARE create_date DATETIME;
  
  IF in_ticket_id IS NULL THEN
    SET create_date = now();
    INSERT INTO ticket_details(post_id,assigner_id,assignee_id,comments,ticket_priority,ticket_status,create_datetime,due_datetime,closed_datetime) 
    VALUES (in_post_id,in_assigner_id,in_assignee_id,in_comments,in_ticket_priority,in_ticket_status,create_date,in_due_date,in_closed_date);
  
  ELSE
    
    UPDATE ticket_details
    SET ticket_status = in_ticket_status
    
    WHERE ticket_id = in_ticket_id;
  
  END IF;
  
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_daily_snapshot` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `sp_daily_snapshot`(
  inCompanyid int
)
Begin
  
  declare weekly_review_count, positive_count, negative_count,cnt  int;
  declare last_week_positive_count, last_week_negative_count int;
  declare top_sources, top_entity, c_entity, most_complaint_topics text;
  DECLARE v_source varchar(100);
  declare v_complaint_topic varchar(100);
  
  declare dt, pdt  datetime;
  declare last_week_from_date, last_week_to_date  datetime;
  DECLARE v_finished INTEGER DEFAULT 0;
  
  DEClARE entity_cursor CURSOR FOR 
  select aspect  from (   
   select x.aspect,count(a.post_id)
    from(
      Select distinct mp.company_id,d.source_id,d.post_id, d.post_datetime
      from social_data d, company_source_map mp
      where d.source_id = mp.source_id
      and mp.company_id = inCompanyid 
    ) a ,
    xpresso_output x
    where a.post_id = x.post_id
    
    and a.post_datetime between DATE_ADD(now(), INTERVAL -24 Hour) and now()
    
    and x.aspect<>'Overall'
    group by x.aspect
    order by 2 desc
    limit 3 
  ) b;
  
  DECLARE source_cursor CURSOR FOR
          
				select  t.source_type_name , count(d.post_id) as cnt 
				from social_data d, source_master s, source_types t, company_source_map mp
				where d.source_id = s.source_id and s.source_type_id = t.source_type_id
				and s.source_id = mp.source_id
				and mp.company_id = inCompanyid
				
				and d.post_datetime between DATE_ADD(now(), INTERVAL -24 Hour) and now()
				group by t.source_type_name
				order by 2 desc ;
				
        
  
  DEClARE complaint_cursor CURSOR FOR 
  select aspect  from (   
   select x.aspect,count(a.post_id)
    from(
      Select distinct mp.company_id,d.source_id,d.post_id, d.post_datetime
      from social_data d, company_source_map mp
      where d.source_id = mp.source_id
      and mp.company_id = inCompanyid 
      and d.overall_sentiment = 'Negative'
    ) a ,
    xpresso_output x
    where a.post_id = x.post_id
    
    and a.post_datetime between DATE_ADD(now(), INTERVAL -24 Hour) and now()
    
    and x.aspect<>'Overall'
    group by x.aspect
    order by 2 desc
    limit 3 
  ) b;
  
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1; 
   
  set dt = now();
  set pdt = DATE_ADD(now(), INTERVAL -24 Hour);
  set last_week_to_date = DATE_ADD(now(), INTERVAL -7 Day);
  set last_week_from_date = DATE_ADD(last_week_to_date, INTERVAL -24 Hour);
  
  
  set top_entity = '';
  OPEN entity_cursor;
  get_entity : LOOP 
 
  FETCH entity_cursor INTO c_entity;
  IF v_finished = 1 THEN 
  LEAVE get_entity;
  END IF;
  
  set dt = now();
 
  
  SET top_entity = CONCAT(top_entity,c_entity,',');
  
  
  END LOOP get_entity;
  CLOSE entity_cursor;
  
  SET top_entity = SUBSTRING_INDEX(top_entity, ',', 3);
  
  select count(*) into weekly_review_count 
  from social_data d, company_source_map mp
  where d.source_id = mp.source_id
  and mp.company_id = inCompanyid 
  
  and post_datetime between pdt and dt;
  
  
SET v_finished = 0;
SET top_sources = '';
         
  OPEN source_cursor;
  get_source : LOOP
  FETCH source_cursor INTO v_source, cnt;
  IF v_finished = 1 THEN
    LEAVE get_source;
  END IF;
  
  SET top_sources = CONCAT(UPPER(v_source), ",", top_sources);
  
  
  
  END LOOP get_source;
  CLOSE source_cursor;
  SET top_sources = SUBSTRING_INDEX(top_sources, ',', 3);
    
  
  
SET v_finished = 0;
SET most_complaint_topics = '';
         
  OPEN complaint_cursor;
  get_complaint : LOOP
  FETCH complaint_cursor INTO v_complaint_topic;
  IF v_finished = 1 THEN
    LEAVE get_complaint;
  END IF;
  
  SET most_complaint_topics = CONCAT(UPPER(v_complaint_topic), ",", most_complaint_topics);
  
  END LOOP get_complaint;
  CLOSE complaint_cursor;
  SET most_complaint_topics = SUBSTRING_INDEX(most_complaint_topics, ',', 3);
    
    
  select 
  count(case WHEN Rtrim(Ltrim(xp.overall_sentiment))='Positive' 
    THEN 1 
    ELSE Null 
  END) as 'Positive' ,
  count(case WHEN Rtrim(Ltrim(xp.overall_sentiment))='Negative' 
    THEN 1
    ELSE Null 
  END) as 'Negative' into last_week_positive_count, last_week_negative_count
  from (
    select distinct d.source_id, d.post_id, d.overall_sentiment 
    from social_data d, xpresso_output x
    where d.post_id = x.post_id
    
    and d.post_datetime between last_week_from_date and last_week_to_date
    and x.aspect<>'Overall'
    and d.overall_sentiment in ('Positive','Negative') 
  ) xp, company_source_map mp
  where mp.source_id = xp.source_id
  and mp.company_id = inCompanyid;
  
  select 
  count(case WHEN Rtrim(Ltrim(xp.overall_sentiment))='Positive' 
    THEN 1 
    ELSE Null 
  END) as 'Positive' ,
  count(case WHEN Rtrim(Ltrim(xp.overall_sentiment))='Negative' 
    THEN 1
    ELSE Null 
  END) as 'Negative' into positive_count, negative_count
  from (
    select distinct d.source_id, d.post_id, d.overall_sentiment 
    from social_data d, xpresso_output x
    where d.post_id = x.post_id
    
    and d.post_datetime between pdt and dt
    and x.aspect<>'Overall'
    and d.overall_sentiment in ('Positive','Negative') 
  ) xp, company_source_map mp
  where mp.source_id = xp.source_id
  and mp.company_id = inCompanyid;



  select weekly_review_count as 'weekly_review_count',
  top_sources as 'top_source',
  top_entity as 'top_entity',
  positive_count as 'positive_count',
  negative_count as 'negative_count',
  last_week_positive_count as 'last_week_positive_count',
  last_week_negative_count as 'last_week_negative_count',
  most_complaint_topics as 'most_complaint_topics'; 
  
End ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_entity_filter_data` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `sp_entity_filter_data`(
  in_account_id INT,
  in_source INT,
  in_topic VARCHAR(255),
  in_start_dt DATETIME,
  in_end_dt DATETIME,
  in_virality VARCHAR(20),
  in_sentiment VARCHAR(20)
)
BEGIN
  
  DECLARE from_date DATETIME;
  DECLARE to_date DATETIME;
  DECLARE q_text TEXT;
  DECLARE q_where TEXT;
  
  IF in_start_dt IS NULL THEN
	  IF in_end_dt IS NULL THEN
		  SET to_date = now();
		  SET from_date = DATE_ADD(to_date, INTERVAL -24 HOUR);
	  ELSE 
		  SET to_date = in_end_dt;
		  SET from_date = DATE_ADD(to_date, INTERVAL -24 HOUR);
    END IF;
  ELSE 
    IF in_end_dt IS NULL THEN
		  SET from_date = in_start_dt;
		  SET to_date = DATE_ADD(from_date, INTERVAL 24 HOUR);
	  ELSE 
		  SET from_date = in_start_dt;
		  SET to_date = in_end_dt; 
    END IF;
  END IF;
  
  SET @sql ="select source_type_id,source_type_name,aspect,Positive,Negative,Neutral,
  CASE 
	  WHEN ROUND(((Positive - Negative)/10),1) < 0 
	  THEN 'viral'  
    ELSE ''  
  END as virality,";
  SET q_text = "from (
    SELECT t.source_type_id,t.source_type_name,xp.aspect,
    COUNT(xp.post_id) as review_count,
	  ROUND((COUNT(
			CASE 
				WHEN RTRIM(LTRIM(xp.overall_sentiment))='Positive' 
				THEN 1 
				ELSE NULL 
			END
		)/COUNT(xp.post_id))*100,2) AS 'Positive',
	  ROUND((COUNT(
			CASE 
				WHEN RTRIM(LTRIM(xp.overall_sentiment))='Negative' 
				THEN 1 
				ELSE NULL 
			END
	  )/COUNT(xp.post_id))*100,2) AS 'Negative',
	  ROUND((COUNT(
		  CASE 
			  WHEN RTRIM(LTRIM(xp.overall_sentiment))='Neutral' 
			  THEN 1 
			  ELSE NULL 
		  END
	  )/COUNT(xp.post_id))*100,2) AS 'Neutral'
	FROM (
    SELECT DISTINCT s.post_id,s.source_id,s.post_datetime,s.overall_sentiment, x.aspect 
    FROM social_data s  ,xpresso_output x
    WHERE s.post_id = x.post_id 
    AND x.aspect<>'Overall' 
    AND s.post_datetime BETWEEN ";

  SET @sql = CONCAT(@sql,"'",from_date,"' as from_date,'",to_date,"' as to_date ",q_text); 
  SET @sql = CONCAT(@sql,"'",from_date,"' and '",to_date, "'" );
  
  IF in_source IS NOT NULL THEN
    SET @sql = CONCAT(@sql," and s.source_id =",in_source);
  END IF;

  IF in_topic IS NOT NULL THEN 
    SET @sql = CONCAT(@sql, " and x.aspect='", in_topic, "' "); 
  END IF;
  
  IF in_sentiment IS NOT NULL THEN
    SET @sql = CONCAT(@sql, " and x.sentiment='", in_sentiment, "' "); 
  END IF;
  
  SET q_where = ") xp 
    ,source_master sm
    ,source_types t
    ,account_company_map ac
    ,company_source_map cs
	WHERE xp.source_id = sm.source_id 
  AND sm.source_type_id = t.source_type_id 
  AND ac.company_id = cs.company_id  ";  

  SET q_where = CONCAT(q_where, "  and ac.account_id = ",in_account_id); 
  SET q_where = CONCAT(q_where, "  and xp.source_id = cs.source_id and sm.source_id = cs.source_id "); 
  SET @sql = CONCAT(@sql, q_where);
  SET @sql = CONCAT(@sql," group by 1,2,3  order by 4 desc ) Zz  "); 
  
  
  
  PREPARE stmt FROM @sql;
  EXECUTE stmt;
  DEALLOCATE PREPARE stmt;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_sentiment_analysis_filter_data` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `sp_sentiment_analysis_filter_data`(
  in_account_id INT,
  in_source INT,
  in_topic VARCHAR(255),
  in_start_dt DATETIME,
  in_end_dt DATETIME,
  in_virality VARCHAR(20),
  in_sentiment VARCHAR(20)
)
BEGIN

	DECLARE from_date DATETIME;
	DECLARE to_date DATETIME;
	DECLARE q_text TEXT;
	DECLARE q_where TEXT;

	IF in_start_dt IS NULL THEN
		IF in_end_dt IS NULL THEN
		  SET to_date = now();
		  SET from_date = DATE_ADD(to_date, INTERVAL -24 HOUR);
		ELSE 
		  SET to_date = in_end_dt;
		  SET from_date = DATE_ADD(to_date, INTERVAL -24 HOUR);
		END IF;
	ELSE 
		IF in_end_dt IS NULL THEN
		  SET from_date = in_start_dt;
		  SET to_date = DATE_ADD(from_date, INTERVAL 24 HOUR);
		ELSE 
		  SET from_date = in_start_dt;
		  SET to_date = in_end_dt; 
		END IF;
	END IF;

	SET @sql ="select t.source_type_id,t.source_type_name,date_format(xp.post_datetime,'%d-%m-%Y') as DT,
		CAST(date_format(xp.post_datetime,'%H') as UNSIGNED) as HR,
		COUNT(xp.post_id) as post_count,
		COUNT(
			CASE 
				WHEN Rtrim(Ltrim(xp.overall_sentiment))='Positive' 
				THEN 1 
				ELSE NULL 
			END
		) AS Positive,    
		COUNT(
			CASE 
				WHEN Rtrim(Ltrim(xp.overall_sentiment))='Negative' 
				THEN 1 
				ELSE NULL 
			END
		) AS Negative,
		COUNT(
			CASE 
				WHEN Rtrim(Ltrim(xp.overall_sentiment))='Neutral' 
				THEN 1 	
				ELSE NULL 
			END
		) AS Neutral
		from (select distinct s.post_id,s.source_id,s.post_datetime,s.overall_sentiment
				from social_data s
				,xpresso_output x 
				where s.post_id = x.post_id 
				and x.aspect<>'Overall'
				and s.post_datetime between ";
  
	SET @sql = CONCAT(@sql,"'",from_date,"' and '",to_date, "'" );
  
	IF in_topic IS NOT NULL THEN
		SET @sql = CONCAT(@sql," and x.aspect='",in_topic,"'");	
	END IF;

	
  
	SET q_where = ") xp   
		,source_master m
		,source_types t
		,company_source_map ms 
		,account_company_map ac
		where xp.source_id = m.source_id
		and m.source_type_id = t.source_type_id 
		and ms.source_id = m.source_id
		and ms.company_id = ac.company_id
		";

	SET @sql = CONCAT(@sql, q_where);

	IF in_source IS NOT NULL THEN
		SET q_where = CONCAT(" and t.source_type_id=",in_source);
		SET @sql = CONCAT(@sql,q_where);	
	END IF;

	IF in_account_id IS NOT NULL THEN
		SET q_where = CONCAT(" and ac.account_id =",in_account_id);
		SET @sql = CONCAT(@sql,q_where);	
	END IF;

	SET @sql = CONCAT(@sql," group by 1,2,3,4") ;

	

	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_social_post_list` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `sp_social_post_list`(
  inAccountId int,
  inSource varchar(50),
  inTopic varchar(255),
  start_dt datetime,
  time_interval int
)
Begin

declare last_date datetime;
declare to_date datetime;

declare temp_source varchar(50);
declare select_q text;
declare q_where text;









declare span,span1,span2 decimal(4,2); 




set span= CAST(time_interval as decimal(4,2))/2.0;
set span1 = span;
set span2 = span1 *(-1);

if start_dt is NULL then
    set last_date = now();
    set to_date = DATE_ADD(now(), INTERVAL -24 Hour); 
  else 
     
      set last_date = DATE_ADD(start_dt, INTERVAL span1 Hour);  
      set to_date = DATE_ADD(start_dt, INTERVAL span2 Hour); 
  end if;

set @sql = "Select distinct
  pt.post_type_id
  ,pt.post_type
  ,pt.source_type_id
  ,t.source_type_name
  ,Date_format(xp.post_datetime,'%d-%m-%Y %H:%i') as post_datetime
  ,xp.post_text
  /*,Case
    When pt.post_type='Post'  then
      
      Concat('https://www.facebook.com/',page_id,'/posts/',SUBSTRING(post_id,INSTR(post_id,'_')+1,(LENGTH(post_id)-INSTR(post_id,'-')))) 
    when pt.post_type='Comment' then
      Concat('https://www.facebook.com/',page_id,'/posts/',SUBSTRING(parent_id,INSTR(parent_id,'_')+1,(LENGTH(parent_id)-INSTR(parent_id,'-')))) 
    When pt.post_type='Tweet' then
      Concat('https://twitter.com/',page_id,'/status/',post_id) 
    else
      'News Site'
  End as post_url
  */
  ,xp.post_url
  ,xp.post_id
  from
(  
  select distinct s.source_id, s.post_url
  ,s.post_id, parent_id, post_type_id,post_datetime,post_text
  ,s.overall_sentiment
  ,x.aspect
  from social_data s
  ,xpresso_output x 
  where s.post_id = x.post_id 
  and x.aspect<>'Overall' ";
  
  if inTopic is not NULL then
  	set q_where = CONCAT(" and x.aspect='",inTopic,"'");
  	set @sql = CONCAT(@sql,q_where);	
  end if;
  
  
  
  set @sql = CONCAT(@sql," and s.post_datetime between '",to_date,"' and '",last_date, "'" );

  set q_where =" ) xp
  ,source_master m
  ,source_types t
  ,company_source_map ms 
  ,account_company_map ac
  ,post_types pt
  where xp.source_id = m.source_id
  and m.source_type_id = t.source_type_id 
  and ms.source_id = m.source_id
  and ms.company_id = ac.company_id
  and xp.post_type_id = pt.post_type_id ";

  set @sql = CONCAT(@sql,q_where);	
  
  if inSource is NULL then 
    
      set q_where = CONCAT(" and xp.source_id in (select c.source_id from account_company_map a, company_source_map c where a.company_id = c.company_id and a.account_id=",inAccountId," and a.primary_company=1)" );
  else 
    #set q_where = CONCAT(" and xp.source_id=",inSource);
  	set q_where = CONCAT(" and t.source_type_id=",inSource);
  end if;
  set @sql = CONCAT(@sql,q_where);

  if inAccountId is not NULL then
  	set q_where = CONCAT(" and ac.account_id =",inAccountId);
  	set @sql = CONCAT(@sql,q_where);	
  end if;

  set @sql = CONCAT(@sql," order by post_datetime asc ") ;

  




PREPARE stmt FROM @sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_source_date_Sentiment_analysis` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `sp_source_date_Sentiment_analysis`(
  inAccountId int,
  inSource varchar(50),
  inTopic varchar(255),
  start_dt datetime,
  time_interval int
)
Begin

declare last_date datetime;
declare to_date datetime;

declare select_q text;
declare q_where text;
declare span,span1,span2 decimal(4,2); 




set span= CAST(time_interval as decimal(4,2))/2.0;
set span1 = span;
set span2 = span1 *(-1);

if start_dt is NULL then
    set last_date = now();
    set to_date = DATE_ADD(now(), INTERVAL -24 Hour); 
  else 
     
      set last_date = DATE_ADD(start_dt, INTERVAL span1 Hour);  
      set to_date = DATE_ADD(start_dt, INTERVAL span2 Hour); 
  end if;






Set @sql ="select t.source_type_id,t.source_type_name,date_format(xp.post_datetime,'%d-%m-%Y') as DT,
CAST(date_format(xp.post_datetime,'%H') as UNSIGNED) as HR,
Count(xp.post_id) as post_count,
#ROUND((COUNT(
COUNT(
        CASE 
            WHEN Rtrim(Ltrim(xp.overall_sentiment))='Positive' 
            THEN 1 
            ELSE NULL 
        END
#    )/Count(*))*100,1) AS Positive,
) AS Positive,    
#ROUND((COUNT(
COUNT(
        CASE 
            WHEN Rtrim(Ltrim(xp.overall_sentiment))='Negative' 
            THEN 1 
            ELSE NULL 
        END
#)/Count(*))*100,1) AS Negative,
) AS Negative,
#ROUND((COUNT(
COUNT(
    CASE 
        WHEN Rtrim(Ltrim(xp.overall_sentiment))='Neutral' 
        THEN 1 
        ELSE NULL 
    END
#)/Count(*))*100,1) AS Neutral
) AS Neutral
from (select distinct s.post_id,s.source_id,s.post_datetime,s.overall_sentiment
  from social_data s
  ,xpresso_output x 
  where s.post_id = x.post_id 
  and x.aspect<>'Overall'
  and s.post_datetime between ";
  
  
  
  
  
  set @sql = CONCAT(@sql,"'",to_date,"' and '",last_date, "'" );
  if inTopic is not NULL then
	
	set @sql = CONCAT(@sql," and x.aspect='",inTopic,"'");	
end if;

 
 
  
Set q_where = ") xp   
,source_master m
,source_types t
,company_source_map ms 
,account_company_map ac
where xp.source_id = m.source_id
and m.source_type_id = t.source_type_id 
and ms.source_id = m.source_id
and ms.company_id = ac.company_id
";

 set @sql = CONCAT(@sql, q_where);


if inSource is not NULL then
	set q_where = CONCAT(" and t.source_type_id=",inSource);
	set @sql = CONCAT(@sql,q_where);	
end if;


if inAccountId is not NULL then
	set q_where = CONCAT(" and ac.account_id =",inAccountId);
	set @sql = CONCAT(@sql,q_where);	
end if;

set @sql = CONCAT(@sql," group by 1,2,3,4") ;






PREPARE stmt FROM @sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;

End ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_source_entity_details` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `sp_source_entity_details`(
  inAccountId int,
  inSource int,
  inTopic varchar(255),
  start_dt datetime,
  end_dt datetime,
  hr int
  
)
Begin
  
   declare last_date datetime;
  declare to_date datetime;

  declare q_text text;
  declare q_where text;
  Declare span,span1,span2 decimal(4,2);  

  set span= CAST(hr as decimal(4,2))/2.0;
  set span1 = span;
  set span2 = span1 *(-1);
  
  if start_dt is NULL then
    set last_date = now();
  else 
     
      set last_date = DATE_ADD(start_dt, INTERVAL span1 Hour);  
  end if;
  
	if end_dt  is NULL then
    set to_date = DATE_ADD(now(), INTERVAL -24 Hour); 
  else
    
    set to_date = DATE_ADD(start_dt, INTERVAL span2 Hour); 
  end if;
  
  
  
  Set @sql ="Select source_type_id,source_type_name,aspect,Positive,Negative,Neutral,
CASE 
	WHEN round(((Positive - Negative)/10),1) < 0 
	THEN 'viral'
  ELSE ''  
END as virality,";


set q_text = "from (
  select t.source_type_id,t.source_type_name,
	#date_format(s.post_datetime,'%d-%m-%Y') as post_date , 
  xp.aspect,
  Count(xp.post_id) as review_count,
	ROUND((COUNT(
			CASE 
				WHEN Rtrim(Ltrim(xp.overall_sentiment))='Positive' 
				THEN 1 
				ELSE NULL 
			END
		)/Count(xp.post_id))*100,2) AS 'Positive',
	ROUND((COUNT(
			CASE 
				WHEN Rtrim(Ltrim(xp.overall_sentiment))='Negative' 
				THEN 1 
				ELSE NULL 
			END
	)/Count(xp.post_id))*100,2) AS 'Negative',
	ROUND((COUNT(
		CASE 
			WHEN Rtrim(Ltrim(xp.overall_sentiment))='Neutral' 
			THEN 1 
			ELSE NULL 
		END
	)/Count(xp.post_id))*100,2) AS 'Neutral'
	from (
    select distinct s.post_id,s.source_id,s.post_datetime,s.overall_sentiment, x.aspect 
    from social_data s  ,xpresso_output x, source_master sm
    where s.post_id = x.post_id 
    and x.aspect<>'Overall' 
    and s.source_id=sm.source_id
    and s.post_datetime between ";


  set @sql = CONCAT(@sql,"'",to_date,"' as to_date,'",last_date,"' as last_date ",q_text); 
  set @sql = CONCAT(@sql,"'",to_date,"' and '",last_date, "'" );
  
  if inSource is not null then
    set @sql = CONCAT(@sql," and sm.source_type_id =",inSource);
  end if;
  
  if inTopic is not null then
    set @sql = CONCAT(@sql, " and x.aspect='", inTopic, "' "); 
  end if;
  
  Set q_where = ") xp 
  ,source_master sm
  ,source_types t
  ,account_company_map ac
  ,company_source_map cs
	where xp.source_id = sm.source_id 
  and sm.source_type_id = t.source_type_id 
  and ac.company_id = cs.company_id  ";  

  Set q_where = CONCAT(q_where, "  and ac.account_id = ",inAccountId); 
  Set q_where = CONCAT(q_where, "  and xp.source_id = cs.source_id and sm.source_id = cs.source_id "); 
   
  set @sql = CONCAT(@sql, q_where);
   
  set @sql = CONCAT(@sql," group by 1,2,3  order by 4 desc ) Zz  "); 
   
  
  
  PREPARE stmt FROM @sql;
  EXECUTE stmt;
  DEALLOCATE PREPARE stmt;


End ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_virality_comparission` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `sp_virality_comparission`(
  inCompany int,
  start_dt datetime,
  end_dt datetime,
  hr int
)
Begin

  declare last_date datetime;
  declare to_date datetime;

  declare q_company text;
  declare q_where text;
  
  Declare span,span1,span2 decimal(4,2);  

  set span= CAST(hr as decimal(4,2))/2.0 ;
  
  
  set span1 = span;
  set span2 = span1 *(-1);

  

  if end_dt is NULL then
    set last_date = now();
  else 
     set last_date = DATE_ADD(end_dt,INTERVAL span1 Hour);  
  end if;
  
	if start_dt is NULL then
    set to_date = DATE_ADD(now(), INTERVAL -24 Hour); 
  else
    set to_date = DATE_ADD(start_dt,INTERVAL span2 Hour);
  end if;
  
  
  Set @sql ="Select company_id, company_name,source_type_id,source_type_name,aspect,review_count,
  #post_date,
  Positive,Negative,Neutral,
CASE 
	WHEN round((Positive - Negative)/10,1) < 0 
	THEN 'viral'
  ELSE ''  
END as virality
from (
  select c.company_id, c.company_name,t.source_type_id,t.source_type_name,
	#date_format(xp.post_datetime,'%d-%m-%Y') as post_date , 
  xp.aspect,
  Count(xp.post_id) as review_count,
	ROUND((COUNT(
			CASE 
				WHEN Rtrim(Ltrim(xp.overall_sentiment))='Positive' 
				THEN 1 
				ELSE NULL 
			END
		)/Count(xp.post_id))*100,2) AS 'Positive',
	ROUND((COUNT(
			CASE 
				WHEN Rtrim(Ltrim(xp.overall_sentiment))='Negative' 
				THEN 1 
				ELSE NULL 
			END
	)/Count(xp.post_id))*100,2) AS 'Negative',
	ROUND((COUNT(
		CASE 
			WHEN Rtrim(Ltrim(xp.overall_sentiment))='Neutral' 
			THEN 1 
			ELSE NULL 
		END
	)/Count(xp.post_id))*100,2) AS 'Neutral'
	from (
    select distinct s.post_id,s.source_id,s.post_datetime,s.overall_sentiment, x.aspect 
    from social_data s  ,xpresso_output x
    where s.post_id = x.post_id 
    and x.aspect<>'Overall' 
    and s.post_datetime between ";
  
  set @sql = CONCAT(@sql,"'",to_date,"' and '",last_date, "'" );
  
  
  
  Set q_where = ") xp 
 ,company_source_map m
  ,company_master c
  ,source_master sm
  ,source_types t
	where xp.source_id = m.source_id and m.company_id = c.company_id
  and sm.source_type_id = t.source_type_id
  and xp.source_id = sm.source_id ";
	
  set @sql = CONCAT(@sql, q_where);
    
  if inCompany is not NULL then
  	set q_company = CONCAT(" and c.company_id=",inCompany);
  	set @sql = CONCAT(@sql,q_company);	
  end if;
  
  set @sql = CONCAT(@sql," group by 1,2,3,4,5  order by 4 desc ) Zz  ");
	
  
  
  
  PREPARE stmt FROM @sql;
  EXECUTE stmt;
  DEALLOCATE PREPARE stmt;
 
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `UpdateSocialDataWithSentimentScore` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `UpdateSocialDataWithSentimentScore`()
BEGIN
	DECLARE bDone INT DEFAULT 0;
	DECLARE postId VARCHAR(100);
	DECLARE overallSentiment VARCHAR(50);
	DECLARE overallSentimentScore DECIMAL(6,4);
	DECLARE sourceId INT(10);

	DECLARE curs CURSOR FOR SELECT source_id, post_id, overall_sentiment, overall_sentiment_score FROM xpresso_output;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET bDone = 1;

	OPEN curs;

	SET bDone = 0;
	REPEAT
		FETCH curs INTO sourceId, postId, overallSentiment, overallSentimentScore;

		UPDATE social_data SET is_analyzed=1, overall_sentiment=overallSentiment, overall_sentiment_score=overallSentimentScore where source_id=sourceId and post_id=postId;
		
	UNTIL bDone END REPEAT;
	
	CLOSE curs;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-07-29 12:27:47
