package com.abzooba.sourcing.test;

import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import com.abzooba.sourcing.common.util.Constants;
import com.abzooba.sourcing.common.util.Util;

public class ZomatoTestFull {

	public static void main(String[] args) throws Exception {

		String strRestaurantURL = Constants.ZOMATO_API_RESTAURANT_URL + "56842";

		Map<String, Object> attributeMap = new HashMap<String, Object>();
		attributeMap.put(Constants.HEADER_CONTENT_TYPE, Constants.CONTENT_TYPE);
		//attributeMap.put(Constants.HEADER_USER_KEY, getUserKey(sourceDTO.getSourceTypeId()));
		//attributeMap.put(Constants.HEADER_USER_KEY, ZomatoUtil.getUserKey(4));
		Properties _prop = new Properties();
		_prop.load(new FileInputStream("./config.properties"));
		String json = Util.getDataFromProxy(_prop, strRestaurantURL, attributeMap);
		System.out.println("JSON :: " + json);

	}

}
