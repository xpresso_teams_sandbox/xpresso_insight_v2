package com.abzooba.sourcing.test;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.RateLimitStatus;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.URLEntity;
import twitter4j.User;
import twitter4j.auth.OAuth2Token;
import twitter4j.conf.ConfigurationBuilder;

public class TwitterTest {

	private int _numberofhit = 800;
	private SimpleDateFormat _format = new SimpleDateFormat("yyyy-MM-dd");

	private void getTweet() {

		int nTotalNumberOfHit = 0;
		boolean bNoTweetFound = false;
		boolean bTweetFound = false;
		String strStartDate = "";
		String strEndDate = "";
		long maxID = Long.MAX_VALUE;

		try {
			Map<String, Object> twitterMap = getOAuth2Token();

			Twitter twitterObj = getTwitter(twitterMap);

			if (twitterObj != null) {
				Map<String, RateLimitStatus> rateLimitStatus = twitterObj.getRateLimitStatus("search");
				RateLimitStatus searchTweetsRateLimit = rateLimitStatus.get("/search/tweets");
				//MothershipSG
				User user = twitterObj.showUser("STCom");
				System.out.println(user.getId() + " >>> " + user.getFavouritesCount() + " >>> " + user.getFollowersCount() + " >>> " + user.getFriendsCount());

				List<Status> statuses;
				String user1;
				statuses = twitterObj.getUserTimeline("ChannelNewsAsia");
				// retrieves the first page's 200 tweets
				//getUserTimeline(new Paging(1, 200));
				int count1 = 0;
				Status stat = twitterObj.showStatus(843663908535644162L);
				URLEntity[] url = stat.getURLEntities();
				System.out.println("STATUS :: " + stat);
				for (URLEntity check1 : url) {
					System.out.println("check : " + check1.getExpandedURL());
				}
				Thread.sleep(1000 * 900);
				/*for (Status check : statuses) {
					count1++;
					System.out.println("CHECKK :: " + check);
					URLEntity[] url = check.getURLEntities();
					for (URLEntity check1 : url) {
						System.out.println("check : " + check1.getExpandedURL());
					}
				}*/
				System.out.println("Count :: " + count1);
				Thread.sleep(1000 * 500);
				// Search for tweets that contains this term

				//Query query = new Query("Walmart");
				Query query = new Query("MothershipSG");
				boolean isCrawlFromPast = false;

				for (int queryNumber = 0; queryNumber < _numberofhit; queryNumber++) {
					//System.out.println("TwitterBolt: Processing started page wise : " + queryNumber);
					Status dateStatus = null;
					int count = 0;
					bNoTweetFound = false;

					/*if (isCrawlFromPast || strMaxPostDate == null) {
						isCrawlFromPast = true;
						Date dtPre = DateUtil.getBeforeOrAfterDate(-nPreviousDay);
						strStartDate = _format.format(dtPre);
						strEndDate = _format.format(DateUtil.getBeforeOrAfterDateProvided(dtPre, nPreviousDay));
						System.out.println("TwitterBolt: from " + strStartDate + " to " + strEndDate);
						--nPreviousDay;
						if (nPreviousDay == -1)
							isCrawlFromPast = false;
					} else {*/
					// old task will go for live
					//strStartDate = _format.format(_format.parse(strMaxPostDate));
					strEndDate = _format.format(new Date());
					/*System.out.println("TwitterBolt: from " + strStartDate + " to " + strEndDate);
					long dtDiff = DateUtil.getDateDiff(_format.parse(strMaxPostDate), new Date(), TimeUnit.DAYS);
					if (dtDiff > nPreviousDay)*/
					strStartDate = _format.format(getBeforeOrAfterDate(-10));
					//}

					System.out.println("TwitterBolt: crawling from " + strStartDate + " to " + strEndDate);
					try {
						query.setCount(100);
						//query.setLang("en");
						query.setSince(strStartDate);
						//query.setUntil(strEndDate);
						//q.setSinceId(id);

						QueryResult result = twitterObj.search(query);
						if (result.getTweets().size() == 0) {
							bNoTweetFound = true;
							System.out.println("TwitterBolt: No tweet found:" + bNoTweetFound);
							break;
						}

						for (Status status : result.getTweets()) {
							try {
								System.out.println("CHECKK :: " + status);
								bTweetFound = true;
								// Increment our count of tweets retrieved
								if (maxID == -1 || status.getId() < maxID) {
									maxID = status.getId();
								}

								long lTweetID = status.getId();
								String authorId = Long.toString(status.getUser().getId());
								String strTweetId = String.valueOf(lTweetID);
								String strTweetDate = status.getCreatedAt().toString();
								int nRetweetCount = status.getRetweetCount();
								int nFavoriteCount = status.getFavoriteCount();

								String pattern = "E MMM dd HH:mm:ss z yyyy";
								SimpleDateFormat format = new SimpleDateFormat(pattern);
								Date date = format.parse(strTweetDate);

								SimpleDateFormat formatdate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
								strTweetDate = formatdate.format(date);

								java.sql.Date sqlDate = new java.sql.Date(date.getTime());
								String strTweetMessage = status.getText();
								String strTweetUser = status.getUser().getScreenName();
								//System.out.println("Status ::: " +  status);
								/*System.out.println(strTweetId + " >>> " + strTweetDate);
								System.out.println(strTweetMessage.replaceAll("'", "\\'"));
								System.out.println(authorId + " >>> " + strTweetUser.replaceAll("'", "\\'"));
								System.out.println(nRetweetCount + " >>> " + nFavoriteCount);
								String postURL = "https://twitter.com/" + strTweetUser + "/status/" + strTweetId;
								System.out.println(postURL);
								System.out.println(nTotalNumberOfHit + "\n===============================================\n");*/
								dateStatus = status;
								count++;
							} catch (Exception e) {
								System.out.println("TwitterBolt: getting some error to extract every tweet ....continue");
								e.printStackTrace();
								continue;
							}
						}
						searchTweetsRateLimit = result.getRateLimitStatus();
						System.out.println("Rate limit status :: " + searchTweetsRateLimit);
						System.out.println("Rate limit status REMAINING :: " + searchTweetsRateLimit.getRemaining());
						//System.out.println("TwitterBolt: Emitting SOCIAL_DATA data to save in database");
					} catch (Exception e) {
						System.out.println("TwitterBolt: getting some error to extrat tweet page wise ...continue");
						e.printStackTrace();
						continue;
					}

					query.setMaxId(maxID - 1);
					System.out.println("===============================================PAGE NUMBER::" + nTotalNumberOfHit + "===============================================");
					System.out.println("Date in current Page ::  " + dateStatus.getCreatedAt());
					System.out.println("Records in current Page :: " + count);
					count = 0;
					nTotalNumberOfHit++;
				}
				// Check process completed or not
				if (nTotalNumberOfHit == _numberofhit) {
					// case for full tweet found
					System.out.println("TwitterBolt: Crawling completed successfully");
				} else if (bNoTweetFound && bTweetFound) {
					System.out.println("bNotweetfound = " + bNoTweetFound + ", btweetfound = " + bTweetFound + ", logic = " + (bNoTweetFound && bTweetFound));
				}
				// case of no tweet found
				if (bNoTweetFound) {
					if (!(bTweetFound)) {
						System.out.println("TwitterBolt: No Tweet found");
					}
				}
			}
			System.out.println("TwitterBolt: Completed processing");
		} catch (Exception e) {
			e.printStackTrace();
			// updating status in tbltaskinfo table
		}
	}

	private static Twitter getTwitter(Map<String, Object> twitterMap) throws TwitterException, SQLException {

		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setApplicationOnlyAuthEnabled(true);
		cb.setOAuthConsumerKey((String) twitterMap.get("app_key"));
		cb.setOAuthConsumerSecret((String) twitterMap.get("app_secret"));
		OAuth2Token token = (OAuth2Token) twitterMap.get("auth_token");
		cb.setOAuth2TokenType(token.getTokenType());
		cb.setOAuth2AccessToken(token.getAccessToken());

		return new TwitterFactory(cb.build()).getInstance();
	}

	private static Map<String, Object> getOAuth2Token() throws TwitterException, SQLException {
		Map<String, Object> twitterMap = new HashMap<String, Object>();

		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setApplicationOnlyAuthEnabled(true);
		cb.setOAuthConsumerKey("oCPLYYwy3Rb4MzBV1fIUEr0QH");
		cb.setOAuthConsumerSecret("ZLd59kDSKN6F0BQF0JK4Jw42QL0U1JMNNc5ZoTkyVlTNJCplHm");

		OAuth2Token token = new TwitterFactory(cb.build()).getInstance().getOAuth2Token();

		twitterMap.put("app_key", "oCPLYYwy3Rb4MzBV1fIUEr0QH");
		twitterMap.put("app_secret", "ZLd59kDSKN6F0BQF0JK4Jw42QL0U1JMNNc5ZoTkyVlTNJCplHm");
		twitterMap.put("auth_token", token);

		return twitterMap;
	}

	private static Date getBeforeOrAfterDate(int nDays) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.DAY_OF_YEAR, nDays);

		return calendar.getTime();
	}

	public static void main(String args[]) {
		TwitterTest tw = new TwitterTest();
		tw.getTweet();
	}

}
