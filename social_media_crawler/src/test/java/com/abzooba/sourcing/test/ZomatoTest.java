package com.abzooba.sourcing.test;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import org.apache.storm.utils.Utils;

public class ZomatoTest {

	public static void main(String[] args) throws Exception {
		long sourceStartTime = System.currentTimeMillis();
		long endTime;
		String strReviewURL = "https://developers.zomato.com/api/v2.1/reviews?count=1000&res_id=" + "56842";
		for (int i = 0; i < 100; i++) {

			Map<String, Object> attributeMap = new HashMap<String, Object>();
			attributeMap.put("Content-Type", "application/json");
			attributeMap.put("user-key", "b38eefa301d2f8d07591b3491c6956d7");
			StringBuffer tmp = new StringBuffer();
			String line = null;
			HttpURLConnection uc = null;
			System.out.println("URL to connect via PROXY :: " + strReviewURL);
			URL url = new URL(strReviewURL);

			System.out.println("No proxy setup required...");
			uc = (HttpURLConnection) url.openConnection();
			//uc.setRequestMethod("GET");

			//uc.setRequestMethod("GET");

			/*if(!strURL.contains("graph.facebook.com")){
				//uc.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0)");
				uc.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.152 Safari/537.36");
				uc.setConnectTimeout(20 * 1000);
			}*/

			if (attributeMap != null) {
				for (Map.Entry<String, Object> elem : attributeMap.entrySet()) {
					uc.setRequestProperty(elem.getKey(), (String) elem.getValue());
				}
			}
			uc.connect();
			System.out.println("RESPONSE" + uc.getResponseMessage());
			BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream(), Charset.forName("UTF-8")));
			BufferedReader inCheck = new BufferedReader(new InputStreamReader(uc.getInputStream()));
			while ((line = in.readLine()) != null) {
				tmp.append(line);
			}
			String line2;
			String responseText = "";
			while ((line2 = inCheck.readLine()) != null) {
				responseText += line;
			}
			//System.out.println("***************Response using Character Set :: " + tmp.toString());
			//System.out.println("***************Response using WITHOUT Character Set :: " + responseText);
			tmp.toString();
			System.out.println("**********************************************************************************");
			System.out.println("**********************************************************************************");
			System.out.println("**********************************************************************************");
			System.out.println("**********************************************************************************");
			System.out.println("**********************************************************************************");
			System.out.println("**********************************************************************************");
			System.out.println("**********************************************************************************");
			endTime = System.currentTimeMillis();
			System.out.println("Count :: " + i);
			System.out.println(" Time Elapsed :: " + (endTime - sourceStartTime) + "ms");
			Utils.sleep(1000 * 15);

		}

	}

}
