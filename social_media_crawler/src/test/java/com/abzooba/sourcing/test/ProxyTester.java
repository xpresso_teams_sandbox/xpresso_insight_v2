package com.abzooba.sourcing.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.PasswordAuthentication;
import java.net.Proxy;
import java.net.URL;
import java.nio.charset.Charset;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class ProxyTester {

	private final static String HTTP_PROXY_ENABLE = "N";
	private final static String HTTP_PROXY_HOST = "192.168.100.110";
	private final static int HTTP_PROXY_PORT = 3128;
	private final static String HTTP_PROXY_USER = "ramproxy";
	private final static String HTTP_PROXY_PASSWORD = "welcome123";
	private final static String URL_TO_TEST_FB_TOKEN = "https://graph.facebook.com/oauth/access_token?client_id=795173247244740&client_secret=c4f6112c07789d662a43def6a96454b0&grant_type=client_credentials";
	private final static String URL_TO_TEST_FB = "https://graph.facebook.com/TheStraitsTimes?access_token=795173247244740|sCfyNBZwBblNpr9sKGmd-80cTdQ&fields=id,likes,talking_about_count";
	private final static String URL_TO_TEST_HTML = "https://www.straitstimes.com/singapore/courts-crime/rapid-deployment-troops-begin-operation-trials-from-monday-public-advised-not";
	private final static String URL_TO_TEST_XML = "https://www.todayonline.com/feed/Voices";

	public Document getCrawledData(String strURL) throws IOException {
		HttpURLConnection huc = null;
		URL url = new URL(strURL);
		if (HTTP_PROXY_ENABLE.equalsIgnoreCase("Y")) {
			Proxy proxy = getProxyConf();
			huc = (HttpURLConnection) url.openConnection(proxy);
		} else {
			huc = (HttpURLConnection) url.openConnection();
		}
		huc.setConnectTimeout(30 * 1000);

		return Jsoup.parse(huc.getInputStream(), "UTF-8", "");
		/*HashMap<String, String> contentMap = new HashMap<String, String>();

		Elements news_title = _doc.select("h1[class = headline node-title]");
		String title = news_title.get(0).text();
		contentMap.put("title", title);

		Elements news_date = _doc.select("div[class = story-postdate]");
		String date = news_date.get(0).text().replace("Published", "");		
		contentMap.put("date", date);
		Elements news = _doc.select("p");

		String complete_news = "";
		for (int j = 0; j < news.size(); j++)
			complete_news += news.get(j).text();
		complete_news = complete_news.replaceAll("(More Deals on)(.*)(for our PDFs.)", "");

		
		contentMap.put("content", complete_news);

		return contentMap;*/
	}

	private static Proxy getProxyConf() throws IOException {
		System.out.println("setting up proxy server...");
		if (HTTP_PROXY_USER != null) {
			Authenticator.setDefault(new Authenticator() {
				@Override
				public PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(HTTP_PROXY_USER, HTTP_PROXY_PASSWORD.toCharArray());
				}
			});
			System.setProperty("http.proxyUser", HTTP_PROXY_USER);
			System.setProperty("http.proxyPassword", HTTP_PROXY_PASSWORD);
		}

		Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(HTTP_PROXY_HOST, HTTP_PROXY_PORT)); // or
		return proxy;
	}

	public String getFBData(String strURL) throws IOException {
		HttpURLConnection huc = null;
		StringBuffer tmp = new StringBuffer();
		String line = null;
		URL url = new URL(strURL);

		if (HTTP_PROXY_ENABLE.equalsIgnoreCase("Y")) {
			Proxy proxy = getProxyConf();
			huc = (HttpURLConnection) url.openConnection(proxy);
		} else {
			huc = (HttpURLConnection) url.openConnection();
		}
		BufferedReader in = new BufferedReader(new InputStreamReader(huc.getInputStream(), Charset.forName("UTF-8")));
		while ((line = in.readLine()) != null) {
			tmp.append(line);
		}

		return tmp.toString();
	}

	private void getXMLDataAsInputStream(String strURL) throws IOException, SAXException, ParserConfigurationException, XPathExpressionException {
		HttpURLConnection huc = null;
		URL url = new URL(strURL);
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		if (HTTP_PROXY_ENABLE.equalsIgnoreCase("Y")) {
			Proxy proxy = getProxyConf();
			huc = (HttpURLConnection) url.openConnection(proxy);
		} else {
			huc = (HttpURLConnection) url.openConnection();
		}

		org.w3c.dom.Document doc = factory.newDocumentBuilder().parse(huc.getInputStream());
		Element root = doc.getDocumentElement();
		/*XPath xPath = XPathFactory.newInstance().newXPath();
		XPathExpression expression = xPath.compile("//item");
		NodeList nl = (NodeList) expression.evaluate(root, XPathConstants.NODESET);
		for (int index = 0; index < nl.getLength(); index++) {
			Node node = nl.item(index);
			expression = xPath.compile("pubDate");
			Node child = (Node) expression.evaluate(node, XPathConstants.NODE);
			System.out.println(child.getTextContent());
			expression = xPath.compile("link");
			Node child1 = (Node) expression.evaluate(node, XPathConstants.NODE);
			System.out.println(child1.getTextContent());
		}*/
		System.out.println(root.getTextContent());
	}

	private void getXMLDataAsInputSource(String strURL) throws IOException, SAXException, ParserConfigurationException, XPathExpressionException {
		HttpURLConnection huc = null;
		URL url = new URL(strURL);
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		StringBuffer tmp = new StringBuffer();
		String line = null;

		if (HTTP_PROXY_ENABLE.equalsIgnoreCase("Y")) {
			Proxy proxy = getProxyConf();
			huc = (HttpURLConnection) url.openConnection(proxy);
		} else {
			huc = (HttpURLConnection) url.openConnection();
		}

		BufferedReader in = new BufferedReader(new InputStreamReader(huc.getInputStream(), Charset.forName("UTF-8")));
		while ((line = in.readLine()) != null) {
			tmp.append(line);
		}

		InputSource is = new InputSource(new StringReader(tmp.toString()));
		org.w3c.dom.Document doc = factory.newDocumentBuilder().parse(is);
		Element root = doc.getDocumentElement();
		System.out.println(root.getTextContent());
		/*XPath xPath = XPathFactory.newInstance().newXPath();
		XPathExpression expression = xPath.compile("//item");
		NodeList nl = (NodeList) expression.evaluate(root, XPathConstants.NODESET);
		for (int index = 0; index < nl.getLength(); index++) {
			Node node = nl.item(index);
			expression = xPath.compile("pubDate");
			Node child = (Node) expression.evaluate(node, XPathConstants.NODE);
			System.out.println(child.getTextContent());
			expression = xPath.compile("link");
			Node child1 = (Node) expression.evaluate(node, XPathConstants.NODE);
			System.out.println(child1.getTextContent());
		}*/
	}

	public static void main(String args[]) {
		ProxyTester proxyTester = new ProxyTester();
		try {
			//HashMap<String, String> dataMap = proxyTester.getCrawledData(URL_TO_TEST_HTML);
			//System.out.println(proxyTester.getCrawledData(URL_TO_TEST_HTML).text());

			//System.out.println(proxyTester.getFBData(URL_TO_TEST_FB_TOKEN));

			//System.out.println(proxyTester.getFBData(URL_TO_TEST_FB));

			System.out.println("Parse XML as InputStream... " + URL_TO_TEST_XML);
			proxyTester.getXMLDataAsInputStream(URL_TO_TEST_XML);

			System.out.println("Parse XML as InputSource... " + URL_TO_TEST_XML);
			proxyTester.getXMLDataAsInputSource(URL_TO_TEST_XML);

		} catch (IOException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
	}
}
