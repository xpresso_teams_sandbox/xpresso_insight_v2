#! /bin/bash
## This script is used to run the project. It shuold contain the script which will run the project
##
## DO NOT USE SUDO in the scripts. These scripts are run as sudo user


# Run the application


#TOPOLOGY_LIST = `storm list`
#echo $TOPOLOGY_LIST

topologies=(facebooktopology facebookcommenttopology facebookreplytopology twittertopology)
cd /apache-storm-1.0.5
echo "Starting Zookeeper"
cd /app/apache-zookeeper-3.5.5-bin

cat /etc/hosts
echo "127.0.0.1       zookeeper" >> /etc/hosts
cat /etc/hosts
bin/zkServer.sh start
cat /etc/hosts

ps -ef 
cd /app
nohup storm nimbus &
sleep 20
nohup storm supervisor &
sleep 20
nohup storm ui &
sleep 30

ps -ef 

echo "Starting topologies"
/apache-storm-1.0.5/bin/storm jar target/USTUSCrawler-1.0-Release.jar -Dpropfile=config.properties com.abzooba.sourcing.common.GlobalTopologyCreator | true

tail -f /etc/hosts
