social_media_crawler: Xpresso project application 
=============================


README

What is this repository for?

    This project is a Crawler for any XpressoInsights having multiple 
    branchfor multiple products/projects on Apache Storm & Java.
	
How do I get set up?

    To set-up the project , clone it to local IDE and build using "assembly:assembly". 
    The jar created in the Target folder needs to be deployed in the server which already has 
    Apache Storm configured and running
    Configuration - For the Worker Nodes & Supervisor Nodes can be provided in the config.properties file
    Dependencies - No external dependencies needed. 
    The project packs itself with all the required dependencies via POM
    Database configuration can be done in the config file
    Deployment instructions : Need to setup Apache Storm Framework to deploy the runnable jar

Main Clases

    GlobalTopology Creator - Creates N number of topologies with the 
    sources as per config.properties setting by the User
    CrawlerSpout - Crawls and emits the Website along with the redirects and 
    external link using BFS mechanism.Uses TreeUtil & CrawlerUtil as helper classes to crawl.
    CrawlerBolt - used in version 0.1 to do what the CrawlerSpout currently implements.
    DocumentHandleBolt - used to handle PDF files to download and extract text.
    HTMLHandlerBolt - used to handle any kind of HTML page, saving the page and 
    extract content in the file-disk
    StandaloneApplication - used to run the crawler as an independent Java program. 
    The script to run the same is submitCapitalStandalone.sh present in the project folder

Who do I talk to?

    Repo owner or admin
    Email : biswajit.sahal@abzooba.com


