# Developed By Nalin Ahuja, nalin.ahuja@abzooba.com
import plotly.graph_objs as go
import numpy as np

import xpresso.ai.core.data.visualization.plotly.res.utils as utils
from xpresso.ai.core.data.visualization.plotly.res.plot_types import diagram
from xpresso.ai.core.commons.utils.constants import EMPTY_STRING


# End Imports-------------------------------------------------------------------------------------------------------------------------------------------------------------------

class new_plot(diagram):
    def __init__(self, input_1, view_mode=None, filter_type=None,
                 plot_title=None, zero_filter=False, name=None,
                 output_format=utils.HTML, output_path=None, auto_render=False, filename=None, axes_labels=None):

        # Add Style to Plot
        self.style_plot()
        if axes_labels is None:
            axes_labels["x_label"] = EMPTY_STRING
            axes_labels["y_label"] = EMPTY_STRING
        if plot_title is not None or filename is not None:
            self.set_labels_manual(plot_label=plot_title, filename=filename)
            self.set_labels(plot_label=plot_title, x_axis_label=axes_labels["x_label"],
                            y_axis_label=axes_labels["y_label"])

        # Convert Input to List
        input = (np.array(input_1).tolist())
        input_1 = [input[0], input[1], input[1], input[2], input[3], input[3],
                   input[4]]
        input_1 = (np.array(input_1).tolist())

        # Check Input Type
        if (type(input_1) is list):
            # Set List Values
            self.data_in = input_1
            self.data_label = input

            # mode options ('m', 'msd')
            if (view_mode == "m"):
                self.mode = True
            elif (view_mode == 'msd'):
                self.mode = 'sd'
            elif (view_mode is None):
                self.mode = view_mode
            else:
                # Return Error on Invalid Flag
                print("ERROR: Invalid View Flag!")
                utils.halt()

            if (filter_type == "a"):
                self.filter = 'all'
            elif (filter_type == "w"):
                self.filter = False
            elif (filter_type == "s"):
                self.filter = "suspectedoutliers"
            elif (filter_type == "o"):
                self.filter = "outliers"
            elif (filter_type is None):
                self.filter = filter_type
            else:
                # Return Error on Invalid Flag
                print("ERROR: Invalid Filter Flag!")
                utils.halt()

            # Process Set Arguments
            if (not (name is None)):
                self.set_name(name)
            if (not (output_path is None)):
                self.output_path = output_path
            if (auto_render):
                self.render(output_format=output_format, auto_open=True,
                            output_path=output_path)
        else:
            # Return Error on Invalid Input
            print("ERROR: Invalid Input Type!")
            utils.halt()

    # End Object Constructor----------------------------------------------------------------------------------------------------------------------------------------------------

    def style_plot(self):
        # Apply Default Style to Plot
        self.apply_default_style()
        # Apply Custom Style to Plot
        self.marker = None
        self.filter = None
        self.mode = None

    # End Style Application Function--------------------------------------------------------------------------------------------------------------------------------------------

    def set_node_color(self, color=None):
        if (not (color is None)):
            self.marker = dict(color=color)

    # End Custom Style Functions------------------------------------------------------------------------------------------------------------------------------------------------

    def process(self, output_format):
        # Generate Plot Config
        plot_data = go.Box(y=self.data_in, hovertemplate=self.hover_tool, name=self.graph_name, marker=self.marker,
                           boxmean=self.mode, boxpoints=self.filter)

        if output_format is utils.PDF or output_format is utils.PNG:
            # Generate Plot Layout with static labels
            ann = [
                go.layout.Annotation(x=0.3, y=self.data_label[0],
                                     text='Minimum(' + str(self.data_label[0]) + ')',
                                     showarrow=False),
                go.layout.Annotation(x=0.3, y=self.data_label[1],
                                     text='1rd Quartile (' + str(self.data_label[1]) + ')',
                                     showarrow=False),
                go.layout.Annotation(x=0.3, y=self.data_label[2],
                                     text='Median(' + str(self.data_label[2]) + ')',
                                     showarrow=False),
                go.layout.Annotation(x=0.3, y=self.data_label[3],
                                     text='3rd Quartile(' + str(self.data_label[3]) + ')',
                                     showarrow=False),
                go.layout.Annotation(x=0.3, y=self.data_label[4],
                                     text='Maximum(' + str(self.data_label[4]) + ')',
                                     showarrow=False)]

            plot_layout = go.Layout(autosize=True, width=self.plot_width, height=self.plot_height,
                                    annotations=ann, paper_bgcolor=str(self.bg_color),
                                    plot_bgcolor=str(self.plot_color),
                                    title=self.plot_title, xaxis=self.x_axis_title, yaxis=self.y_axis_title,
                                    margin=go.layout.Margin(l=int(self.left), r=int(self.right), b=int(self.bottom),
                                                            t=int(self.top), pad=int(self.padding)))
        else:
            plot_layout = go.Layout(autosize=True, width=self.plot_width, height=self.plot_height,
                                    paper_bgcolor=str(self.bg_color), plot_bgcolor=str(self.plot_color),
                                    title=self.plot_title, xaxis=self.x_axis_title, yaxis=self.y_axis_title,
                                    margin=go.layout.Margin(l=int(self.left), r=int(self.right), b=int(self.bottom),
                                                            t=int(self.top), pad=int(self.padding)))

        return {'data': [plot_data], 'layout': plot_layout}

    def render(self, output_format=utils.HTML, output_path=None, auto_open=True):
        self._render_single_plot(__name__, output_format=output_format, output_path=output_path, auto_open=auto_open)

    # End Generation/Rendering Functions----------------------------------------------------------------------------------------------------------------------------------------


# End Graph Class---------------------------------------------------------------------------------------------------------------------------------------------------------------

class join_plots(diagram):
    def __init__(self, plots, output_format=utils.HTML, output_path=None, auto_render=False):
        # Apply Def Style to Plot
        self.apply_default_style()

        # Check Input Types
        if (type(plots) is list):
            self.multi_plots = []

            # Strip off styling from plots
            for plot in plots:
                self.multi_plots.append(plot.process()['data'][0])

            # Process Set Arguments
            if (not (output_path is None)):
                self.output_path = output_path
            if (auto_render):
                self.render(output_format=output_format, auto_open=True,
                            output_path=output_path)
        else:
            # Return Error on Invalid Input
            print("ERROR: Invalid Input Type!")
            utils.halt()

    # End Object Constructor----------------------------------------------------------------------------------------------------------------------------------------------------

    def generate_layout(self):
        plot_layout = go.Layout(autosize=True, width=self.plot_width, height=self.plot_height,
                                paper_bgcolor=str(self.bg_color), plot_bgcolor=str(self.plot_color),
                                title=self.plot_title, xaxis=self.x_axis_title, yaxis=self.y_axis_title,
                                margin=go.layout.Margin(l=int(self.left), r=int(self.right), b=int(self.bottom),
                                                        t=int(self.top), pad=int(self.padding)))

        return plot_layout

    def render(self, output_format=utils.HTML, output_path=None, auto_open=True):
        self._render_multi_plot(__name__, output_format=output_format, output_path=output_path, auto_open=auto_open)

    # End Generation/Rendering Functions----------------------------------------------------------------------------------------------------------------------------------------

# End Join Class----------------------------------------------------------------------------------------------------------------------------------------------------------------
