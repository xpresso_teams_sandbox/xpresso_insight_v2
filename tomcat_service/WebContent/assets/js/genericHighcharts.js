(function(){
	var custHighchart = window.custHighchart || function(useroptions){
		var defaults=
		{
			container: 'body',
			type: 'line',
			backgroundColor: 'rgba(0, 0, 0, 0)',
			colors: ["#A7D43B", "#7BCAC9", "#A473A4", "#BBD37B", "#F2566C", "#FFBF02", "#1693A5", "#00A3D8"],
			xAxis: true,
			yAxis: true,
			xAxisGridLine: 0,
			yAxisGridLine: 0,
			gridLineColor: "#ccc",
			axisColor: "#bbb",
			xAxisTitle: "",
			yAxisTitle: "",
			toolTipValueLabel : "Unique Fan",
			plotOptions:{
    			line: {
					marker: {
						fillColor: "#FFFFFF",
						radius: 4,
						lineWidth: 2,
						lineColor: null 
					},
					lineWidth: 1
				}
			},
			lineWidth: 1,
            states: {
                hover: {
                    lineWidth: 1
                }
            },
            credits: false,
	        legend: false,
	        title: "",
	        subtitle: "",
	        series: "",
	        categories: "",
	        marker: {
	        	color: "#fff",
	        	radius: 4,
	        	lineWidth: 2,
	        	lineColor: null
	        },
	        dataLabels: false,
	        marginTop: 20,
	        marginBottom: 50,
	        marginLeft: 65,
	        marginRight: '',
	        tooltipFormat: {}
		},
		options= $.extend(defaults,useroptions),
		width = options.width || "",
		height = options.height  || "",
		customCharts = {
			chartFn: function(){
				//console.log("Series::",options.type,"---",options.series);
				Highcharts.chart(options.container, {
					chart: {
				        type: options.type,
				        backgroundColor: options.backgroundColor,
				        marginTop: options.marginTop,
				        marginBottom: options.type === 'pie' ? 0 : options.marginBottom,
				        marginLeft: options.marginLeft,
				        marginRight: options.type === 'pie' ? options.marginLeft : options.marginRight
			        },
			        xAxis: {
			            categories: options.categories,
			            title: {
			            	text: options.xAxisTitle
			            }
			        },
			        yAxis: {
			        	title: {
			                text: options.yAxisTitle
			            },
			            lineColor: options.gridLineColor,
			            gridLineWidth: options.xAxisGridLine,
			            lineWidth: options.yAxis ? options.lineWidth : 0,
			            startOnTick:true
			        },
					colors: options.colors || Highcharts.getOptions().colors[0],
					tooltip: options.tooltipFormat,
			        plotOptions: options.plotOptions,
			        legend: {
			        	enabled: options.legend
			        },
			        credits: {
			            enabled: options.credits
			        },
			        title: {
			            text: options.title
			        },
			        subtitle:{
			        	text: options.subtitle
			        },
			        series: options.series
			    });
			},
			render: function(){
				
				customCharts.chartFn();
				
			}
		}
		customCharts.render();
	};
	window.custHighchart = custHighchart;
})();
