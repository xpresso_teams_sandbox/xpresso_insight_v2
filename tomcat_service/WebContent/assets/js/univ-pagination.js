/**
 * @license UniV-Pagination v3.0.0 
 * Date : 2016-07-27
 * (c) 2016 Abzooba India Infotech Pvt. Ltd. 
 * Author : Ashis Kumar Nayak
 * License: Free
 * 
 * @description
 * Plug and play 'pagination control' for table pagination using basic java script and jQuery
 * ---------------- Read Me -----------------------
 * @param tblName : Provide the Table ID where you want to show the pagination
 * @param tBodyName : Provide the Tbody ID for the same table 
 * @param maxRows : Provide max number or rows to be shown for each page
 * @param cssClass : Provide extra class for pagination DIV, you can add multiple classes by giving a space in between
 * @param uniqueIdentifier : Provide Unique Identifier (Unique ID) for elements inside the pagination
 * @param type : default / minimal (Creates the pagination according to the type)
 * 
 * NOTE : By default it will create the pagination bellow the "Table"
 */
//(function(window, document, undefined) {'use strict';

$.fn.univpagination = function( options ) {
    // default settings:
    var defaults = $.extend({
    	tblName: "tblDefault",
    	tBodyName: "tbodyDefault",
    	maxRows: 10,
    	cssClass: "no-class",
    	uniqueIdentifier: "_ABZ",
    	type:'default'
    }, options );
    
    return this.each(function() {
    	$('#paginationDiv'+defaults.uniqueIdentifier).remove();
    	var rowsTotal = $('#' + defaults.tBodyName +' tr').length;
    	
    	$('#'+ defaults.tblName).after('<input type="hidden" name="hidPagination'+defaults.uniqueIdentifier+'" id="hidPagination'+defaults.uniqueIdentifier+'" value="0" />')
    	
    	var rowsShown = defaults.maxRows;
      	var numPages = rowsTotal/rowsShown;
    	if(defaults.type == 'default'){
	    	if(rowsTotal > defaults.maxRows ){
	    		$('#'+ defaults.tblName).after('<div class="'+defaults.cssClass+' nbt-peger" id="paginationDiv'+defaults.uniqueIdentifier+'"><ul class="pagination" id="navp'+defaults.uniqueIdentifier+'"><li id="pgPrev'+defaults.uniqueIdentifier+'"><a aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li><li id="pgNext'+defaults.uniqueIdentifier+'"><a aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li></ul></div>');
	    	 
	         	for(i = 0;i < numPages;i++) {
	            	var pageNum = i + 1;
	            	$('#pgNext'+defaults.uniqueIdentifier).before('<li><a id="pgBtn'+defaults.uniqueIdentifier+'_'+i+'" rel="'+i+'" >'+pageNum+'</a></li>');
	         	}
	         	 $('#'+defaults.tBodyName+' tr').hide();
	         	 $('#'+defaults.tBodyName+ ' tr').slice(0, rowsShown+1).show();
	         	 $('#hidPagination'+defaults.uniqueIdentifier).val(0); 
	         	 $('#navp'+defaults.uniqueIdentifier+' [id^="pgBtn'+defaults.uniqueIdentifier+'_"]').bind('click', function(){
	    	         $('#navp'+defaults.uniqueIdentifier+' a').removeClass('active');
	    	         $('#navp'+defaults.uniqueIdentifier+' li').removeClass('active');
	    	         $(this).addClass('active');
	    	         $(this).parent().addClass('active');
	    	         var currPage = $(this).attr('rel');
	    	         $('#hidPagination'+defaults.uniqueIdentifier).val(currPage);
	    	         var startItem = currPage * rowsShown;
	    	         var endItem = startItem + rowsShown;
	    	         $('#'+defaults.tBodyName+' tr').css('opacity','0.0').hide().slice(startItem, endItem).css('display','table-row').animate({opacity:1}, 500);
	    	     });
	         	$('#pgBtn'+defaults.uniqueIdentifier+'_0').parent().addClass('active');
	         
	         	$('#pgNext'+defaults.uniqueIdentifier).click(function(){
	                var curClassBtn = $('#navp'+defaults.uniqueIdentifier+' li.active').find('a').attr('id');
	                var splitLiClass = curClassBtn.split(SYMBOLS.UNDERSCORE);
	                var newId = splitLiClass[2];
	                var newFinalId = parseInt(newId)+1;
	                $('#pgBtn'+defaults.uniqueIdentifier+'_' + newFinalId).click();  
	            });
	         
	         	$('#pgPrev'+defaults.uniqueIdentifier).click(function(){
	                var curClassBtn = $('#navp'+defaults.uniqueIdentifier+' li.active').find('a').attr('id');
	                var splitLiClass = curClassBtn.split(SYMBOLS.UNDERSCORE);
	                var newId = splitLiClass[2];
	                var newFinalId = parseInt(newId)-1;
	                $("#pgBtn"+defaults.uniqueIdentifier+"_" + newFinalId).click();  
	                return false;
	    		});
	    	} else {
	    		$("#paginationSpecerRow").css('display','block');
	    	} 
	    //End of Default View pagination	
    	} else if(defaults.type == 'minimal') {
    	//Start of Minimal view of pagination
    		if(rowsTotal > defaults.maxRows ){
    			$('#'+ defaults.tblName).after('<div class="'+defaults.cssClass+' nbt-peger" id="paginationDiv'+defaults.uniqueIdentifier+'"><ul class="pagination" id="navp'+defaults.uniqueIdentifier+'"><li id="pgPrev'+defaults.uniqueIdentifier+'"><a aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li><li id="pgNext'+defaults.uniqueIdentifier+'"><a aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li></ul></div>');
    	     	numPages = Math.ceil(numPages);
    	     	for(i = 0;i < numPages;i++) {
                	var pageNum = i + 1;
                	 if(pageNum < 2 || pageNum == numPages){
                		 $('#pgNext'+defaults.uniqueIdentifier).before('<li><a id="pgBtn'+defaults.uniqueIdentifier+'_'+i+'" rel="'+i+'"  class="firstBtn">'+pageNum+'</a></li>');
                	} else {
                		$('.dotPages'+defaults.uniqueIdentifier).remove();
                		var newValDotPage = parseInt($('#hidPagination'+defaults.uniqueIdentifier).val())+1;
		        		var totalPages = parseInt(rowsTotal);
		        		if(newValDotPage <= totalPages && newValDotPage != 1){
		        			$('#pgNext'+defaults.uniqueIdentifier).removeClass('liDisabled');
	                		$('#pgNext'+defaults.uniqueIdentifier).before('<li class="dotPages'+defaults.uniqueIdentifier+'"><a id="dotPagesAnchor'+defaults.uniqueIdentifier+'" class="dotPagesAnchor" >'+(parseInt($('#hidPagination'+defaults.uniqueIdentifier).val())+1)+'</a></li>');
	                		$('#pgNext'+defaults.uniqueIdentifier).before('<li class="clsHiddenPages" id="idHiddenPages'+defaults.uniqueIdentifier+'_'+i+'" style="display:none;"><a id="pgBtn'+defaults.uniqueIdentifier+'_'+i+'" rel="'+i+'">'+pageNum+'</a></li>');
	                		
	                		
	                		
	                		
                		} else{
                			if(newValDotPage <= totalPages){$('#pgNext'+defaults.uniqueIdentifier).removeClass('liDisabled')}else{$('#pgNext'+defaults.uniqueIdentifier).addClass('liDisabled')}
                			$('#pgNext'+defaults.uniqueIdentifier).before('<li class="dotPages'+defaults.uniqueIdentifier+'"><a id="dotPagesAnchor'+defaults.uniqueIdentifier+'" class="dotPagesAnchor" >1</a></li>');
	                		$('#pgNext'+defaults.uniqueIdentifier).before('<li class="clsHiddenPages" id="idHiddenPages'+defaults.uniqueIdentifier+'_'+i+'" style="display:none;"><a id="pgBtn'+defaults.uniqueIdentifier+'_'+i+'" rel="'+i+'" >'+pageNum+'</a></li>');
	                		
	                		/*$('.dotPagesAnchor').bind('click', function(){
	                			var currentValue = $(this).html();
	                			$(this).replaceWith('<a id="dotPagesAnchor'+defaults.uniqueIdentifier+'" class="dotPagesAnchor"><input type="text" name="txtdotPagesAnchor'+defaults.uniqueIdentifier+'" value="'+currentValue+'" style="width:45px;height:20px;border-radius:20%; border-color:lightgray;border-style:solid;"></a>')
	                		});*/
                		}
             		}
             	}
    	     	
    	     	 $('#'+defaults.tBodyName+' tr').hide();
	         	 $('#'+defaults.tBodyName+ ' tr').slice(0, rowsShown+1).show();
	         	 
    	     	 $('#navp'+defaults.uniqueIdentifier+' a:first').addClass('active');
    	     	 $('#hidPagination'+defaults.uniqueIdentifier).val(0);
             	 $('#navp'+defaults.uniqueIdentifier+' [id^="pgBtn_"]').bind('click', function(){
        	         $('#navp'+defaults.uniqueIdentifier+' a').removeClass('active');
        	         $('#navp'+defaults.uniqueIdentifier+' li').removeClass('active');
        	         $(this).addClass('active');
        	         $(this).parent().addClass('active');
        	         var currPage = parseInt($(this).attr('rel'));
        	         $('#hidPagination'+defaults.uniqueIdentifier).val(currPage);
        	         $('#dotPagesAnchor'+defaults.uniqueIdentifier).html(currPage+1)
        	         var startItem = currPage * rowsShown;
    		         var endItem = startItem + rowsShown;
    		         //$('#'+defaults.tBodyName+' tr').css('opacity','0.0').hide().slice(startItem, endItem).css('display','table-row').animate({opacity:1}, 300);
    		         $('#'+defaults.tBodyName+' tr').css('opacity','0.0').hide().slice(startItem, endItem).css('display','table-row').animate({opacity:1}, 500);
        	         
        	     });
    	     	 
    	     	$('#pgBtn_0').parent().addClass('active');
    	     
    	     	$('#pgNext'+defaults.uniqueIdentifier).click(function(){
    	     		var curBtn = parseInt($('#hidPagination'+defaults.uniqueIdentifier).val());
    	            var newFinalId = parseInt(curBtn)+1;
    	            $('#pgBtn'+defaults.uniqueIdentifier+'_' + newFinalId).click();  
    	            var lastBtnIndex = $('.firstBtn').eq(1).attr('id');
    	            var splitLastBtnIndex = lastBtnIndex.split(SYMBOLS.UNDERSCORE);
    	            var splitLastBtnIndex = splitLastBtnIndex[2];
    	            if(splitLastBtnIndex >= newFinalId+1){
    	            	$('#dotPagesAnchor'+defaults.uniqueIdentifier).html(newFinalId+1);
    	            	$('.dotPages'+defaults.uniqueIdentifier).addClass('active');
    	            } else{
    	            	$('#pgNext'+defaults.uniqueIdentifier+' a').addClass('liDisabled');
    	            	$('#pgBtn'+defaults.uniqueIdentifier+'_' + newFinalId).click(); 
    	            	$('.dotPages'+defaults.uniqueIdentifier).removeClass('active');
    	            }
    	        });
    	     
    	     	$('#pgPrev'+defaults.uniqueIdentifier).click(function(){
    	     		var curBtn = parseInt($('#hidPagination'+defaults.uniqueIdentifier).val());
    	            var newFinalId = parseInt(curBtn)-1;
    	            $('#pgBtn'+defaults.uniqueIdentifier+'_' + newFinalId).click();  
    	            var firstBtnIndex = $('.firstBtn').eq(0).attr('id');
    	            var splitFirstBtnIndex = firstBtnIndex.split(SYMBOLS.UNDERSCORE);
    	            var splitFirstBtnIndex = splitFirstBtnIndex[2];
    	            $('#pgNext a').removeClass('liDisabled');
    	            if(splitFirstBtnIndex <= newFinalId){
    	            	$('#dotPagesAnchor'+defaults.uniqueIdentifier).html(newFinalId+1);
    	            	if(splitFirstBtnIndex != newFinalId){
    	            		$('.dotPages'+defaults.uniqueIdentifier).addClass('active');
    	            	}
    	            } else{  
    	            	$('#pgPrev'+defaults.uniqueIdentifier+' a').addClass('liDisabled');
    	            	$('#pgBtn'+defaults.uniqueIdentifier+'_' + newFinalId).click();
    	            	$('.dotPages'+defaults.uniqueIdentifier).removeClass('active');
    	            }
    	            return false; 
    			});
    	     
    		} else {
    			$("#paginationSpecerRow").css('display','block');
    		}    		
    	//End of Minimal view of pagination	
    	} else if(defaults.type == 'minimalwithtext') {
    	//Start of Minimal view of pagination
    		if(rowsTotal > defaults.maxRows ){
    			$('#'+ defaults.tblName).parent().after('<div class="'+defaults.cssClass+' nbt-peger" id="paginationDiv'+defaults.uniqueIdentifier+'"><ul class="pagination" id="navp'+defaults.uniqueIdentifier+'"><li id="pgPrev'+defaults.uniqueIdentifier+'"><a aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li><li id="pgNext'+defaults.uniqueIdentifier+'"><a aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li></ul></div>');
    	     	numPages = Math.ceil(numPages);
    	     	for(i = 0;i < numPages;i++) {
                	var pageNum = i + 1;
                	 if(pageNum < 2 || pageNum == numPages){
                		 $('#pgNext'+defaults.uniqueIdentifier).before('<li><a id="pgBtn'+defaults.uniqueIdentifier+'_'+i+'" rel="'+i+'"  class="firstBtn">'+pageNum+'</a></li>');
                	} else {
                		$('.dotPages'+defaults.uniqueIdentifier).remove();
                		var newValDotPage = parseInt($('#hidPagination'+defaults.uniqueIdentifier).val())+1;
		        		var totalPages = parseInt(rowsTotal);
		        		if(newValDotPage <= totalPages && newValDotPage != 1){
		        			$('#pgNext'+defaults.uniqueIdentifier).removeClass('liDisabled');
	                		$('#pgNext'+defaults.uniqueIdentifier).before('<li class="dotPages'+defaults.uniqueIdentifier+'"><a id="dotPagesAnchor'+defaults.uniqueIdentifier+'" class="dotPagesAnchor" >'+(parseInt($('#hidPagination'+defaults.uniqueIdentifier).val())+1)+'</a></li>');
	                		$('#pgNext'+defaults.uniqueIdentifier).before('<li class="clsHiddenPages" id="idHiddenPages'+defaults.uniqueIdentifier+'_'+i+'" style="display:none;"><a id="pgBtn'+defaults.uniqueIdentifier+'_'+i+'" rel="'+i+'">'+pageNum+'</a></li>');
	                		
	                		
	                		
	                		
                		} else{
                			if(newValDotPage <= totalPages){$('#pgNext'+defaults.uniqueIdentifier).removeClass('liDisabled')}else{$('#pgNext'+defaults.uniqueIdentifier).addClass('liDisabled')}
                			$('#pgNext'+defaults.uniqueIdentifier).before('<li class="dotPages'+defaults.uniqueIdentifier+'"><a id="dotPagesAnchor'+defaults.uniqueIdentifier+'" class="dotPagesAnchor" >1</a></li>');
	                		$('#pgNext'+defaults.uniqueIdentifier).before('<li class="clsHiddenPages" id="idHiddenPages'+defaults.uniqueIdentifier+'_'+i+'" style="display:none;"><a id="pgBtn'+defaults.uniqueIdentifier+'_'+i+'" rel="'+i+'" >'+pageNum+'</a></li>');
	                		
	                		$('.dotPagesAnchor').bind('click', function(){
	                			var currentValue = $(this).html();
	                			$(this).replaceWith('<a id="dotPagesAnchor'+defaults.uniqueIdentifier+'" class="dotPagesAnchor"><input type="text" name="txtdotPagesAnchor'+defaults.uniqueIdentifier+'" id="txtdotPagesAnchor'+defaults.uniqueIdentifier+'" value="'+currentValue+'" style="width:45px;height:20px;border-radius:20%; border-color:lightgray;border-style:solid;"></a>')
	                			$("#txtdotPagesAnchor"+defaults.uniqueIdentifier).bind('keypress', function(e){
		                			//$(this).replaceWith('<a id="dotPagesAnchor'+defaults.uniqueIdentifier+'" class="dotPagesAnchor"><input type="text" name="txtdotPagesAnchor'+defaults.uniqueIdentifier+'" id="txtdotPagesAnchor'+defaults.uniqueIdentifier+'" value="'+currentValue+'" style="width:45px;height:20px;border-radius:20%; border-color:lightgray;border-style:solid;"></a>')
	                				if(e.which == 13) {
	                				 $('#navp'+defaults.uniqueIdentifier+' a').removeClass('active');
	                       	         $('#navp'+defaults.uniqueIdentifier+' li').removeClass('active');
	                       	         $(this).addClass('active');
	                       	         $(this).parent().addClass('active');
	                       	         var currPage = parseInt($(this).val());
	                       	         $('#hidPagination'+defaults.uniqueIdentifier).val(currPage);
	                       	         var startItem = currPage * rowsShown;
	                   		         var endItem = startItem + rowsShown;
	                   		         //$('#'+defaults.tBodyName+' tr').css('opacity','0.0').hide().slice(startItem, endItem).css('display','table-row').animate({opacity:1}, 300);
	                   		         $('#'+defaults.tBodyName+' tr').css('opacity','0.0').hide().slice(startItem, endItem).css('display','table-row').animate({opacity:1}, 500);
	                				}
		                		});
	                		});
                		}
             		}
             	}
    	     	
    	     	 $('#'+defaults.tBodyName+' tr').hide();
	         	 $('#'+defaults.tBodyName+ ' tr').slice(0, rowsShown+1).show();
	         	 
    	     	 $('#navp'+defaults.uniqueIdentifier+' a:first').addClass('active');
    	     	 $('#hidPagination'+defaults.uniqueIdentifier).val(0);
             	 $('#navp'+defaults.uniqueIdentifier+' [id^="pgBtn_"]').bind('click', function(){
        	         $('#navp'+defaults.uniqueIdentifier+' a').removeClass('active');
        	         $('#navp'+defaults.uniqueIdentifier+' li').removeClass('active');
        	         $(this).addClass('active');
        	         $(this).parent().addClass('active');
        	         var currPage = parseInt($(this).attr('rel'));
        	         $('#hidPagination'+defaults.uniqueIdentifier).val(currPage);
        	         $('#dotPagesAnchor'+defaults.uniqueIdentifier).html(currPage+1)
        	         var startItem = currPage * rowsShown;
    		         var endItem = startItem + rowsShown;
    		         //$('#'+defaults.tBodyName+' tr').css('opacity','0.0').hide().slice(startItem, endItem).css('display','table-row').animate({opacity:1}, 300);
    		         
    		         //Handeled for issue in showing one page less in case of first page when moving back
    		         var curButtonID = $(this).attr('id')
    		         if(curButtonID.indexOf("_rating_0") >= 0){
    		        	 $('#'+defaults.tBodyName+' tr').css('opacity','0.0').hide().slice(startItem, endItem+1).css('display','table-row').animate({opacity:1}, 500);
    		         } else {
    		        	 $('#'+defaults.tBodyName+' tr').css('opacity','0.0').hide().slice(startItem, endItem).css('display','table-row').animate({opacity:1}, 500);
    		         }
    		         
        	         
        	     });
    	     	 
    	     	$('#pgBtn_0').parent().addClass('active');
    	     
    	     	$('#pgNext'+defaults.uniqueIdentifier).click(function(){
    	     		var curBtn = parseInt($('#hidPagination'+defaults.uniqueIdentifier).val());
    	            var newFinalId = parseInt(curBtn)+1;
    	            $('#pgBtn'+defaults.uniqueIdentifier+'_' + newFinalId).click();  
    	            var lastBtnIndex = $('.firstBtn').eq(1).attr('id');
    	            var splitLastBtnIndex = lastBtnIndex.split(SYMBOLS.UNDERSCORE);
    	            var splitLastBtnIndex = splitLastBtnIndex[2];
    	            if(splitLastBtnIndex >= newFinalId+1){
    	            	//alert(0)
    	            	$('#dotPagesAnchor'+defaults.uniqueIdentifier).html(newFinalId+1);
    	            	$('.dotPages'+defaults.uniqueIdentifier).addClass('active');
    	            } else{
    	            	//alert(1)
    	            	$('#pgNext'+defaults.uniqueIdentifier+' a').addClass('liDisabled');
    	            	$('#pgBtn'+defaults.uniqueIdentifier+'_' + newFinalId).click(); 
    	            	$('.dotPages'+defaults.uniqueIdentifier).removeClass('active');
    	            	
    	            	
    	            	$('.dotPagesAnchor').bind('click', function(){
                			var currentValue = $(this).html();
                			$(this).replaceWith('<a id="dotPagesAnchor'+defaults.uniqueIdentifier+'" class="dotPagesAnchor"><input type="text" name="txtdotPagesAnchor'+defaults.uniqueIdentifier+'" id="txtdotPagesAnchor'+defaults.uniqueIdentifier+'" value="'+currentValue+'" style="width:45px;height:20px;border-radius:20%; border-color:lightgray;border-style:solid;"></a>')
                			$("#txtdotPagesAnchor"+defaults.uniqueIdentifier).bind('keypress', function(e){
	                			//$(this).replaceWith('<a id="dotPagesAnchor'+defaults.uniqueIdentifier+'" class="dotPagesAnchor"><input type="text" name="txtdotPagesAnchor'+defaults.uniqueIdentifier+'" id="txtdotPagesAnchor'+defaults.uniqueIdentifier+'" value="'+currentValue+'" style="width:45px;height:20px;border-radius:20%; border-color:lightgray;border-style:solid;"></a>')
                				if(e.which == 13) {
                				 $('#navp'+defaults.uniqueIdentifier+' a').removeClass('active');
                       	         $('#navp'+defaults.uniqueIdentifier+' li').removeClass('active');
                       	         $(this).addClass('active');
                       	         $(this).parent().addClass('active');
                       	         var currPage = parseInt($(this).val());
                       	         $('#hidPagination'+defaults.uniqueIdentifier).val(currPage);
                       	         //$('#dotPagesAnchor'+defaults.uniqueIdentifier).html(currPage+1)
                       	         var startItem = currPage * rowsShown;
                   		         var endItem = startItem + rowsShown;
                   		         //$('#'+defaults.tBodyName+' tr').css('opacity','0.0').hide().slice(startItem, endItem).css('display','table-row').animate({opacity:1}, 300);
                   		         $('#'+defaults.tBodyName+' tr').css('opacity','0.0').hide().slice(startItem, endItem).css('display','table-row').animate({opacity:1}, 500);
                				}
	                		});
                		});
    	            	
    	            	
    	            }
    	        });
    	     
    	     	$('#pgPrev'+defaults.uniqueIdentifier).click(function(){
    	     		var curBtn = parseInt($('#hidPagination'+defaults.uniqueIdentifier).val());
    	            var newFinalId = parseInt(curBtn)-1;
    	            $('#pgBtn'+defaults.uniqueIdentifier+'_' + newFinalId).click();  
    	            var firstBtnIndex = $('.firstBtn').eq(0).attr('id');
    	            var splitFirstBtnIndex = firstBtnIndex.split(SYMBOLS.UNDERSCORE);
    	            var splitFirstBtnIndex = splitFirstBtnIndex[2];
    	            $('#pgNext a').removeClass('liDisabled');
    	            if(splitFirstBtnIndex <= newFinalId){
    	            	$('#dotPagesAnchor'+defaults.uniqueIdentifier).html(newFinalId+1);
    	            	if(splitFirstBtnIndex != newFinalId){
    	            		$('.dotPages'+defaults.uniqueIdentifier).addClass('active');
    	            	}
    	            } else{  
    	            	if(newFinalId == -1 || newFinalId == "-1"){
    	            		newFinalId = 0;	
    	            	}
    	            	$('#pgPrev'+defaults.uniqueIdentifier+' a').addClass('liDisabled');
    	            	$('#pgBtn'+defaults.uniqueIdentifier+'_' + newFinalId).click();
    	            	$('.dotPages'+defaults.uniqueIdentifier).removeClass('active');
    	            	
    	            	
    	            	$('.dotPagesAnchor').bind('click', function(){
                			var currentValue = $(this).html();
                			$(this).replaceWith('<a id="dotPagesAnchor'+defaults.uniqueIdentifier+'" class="dotPagesAnchor"><input type="text" name="txtdotPagesAnchor'+defaults.uniqueIdentifier+'" id="txtdotPagesAnchor'+defaults.uniqueIdentifier+'" value="'+currentValue+'" style="width:45px;height:20px;border-radius:20%; border-color:lightgray;border-style:solid;"></a>')
                			$("#txtdotPagesAnchor"+defaults.uniqueIdentifier).bind('keypress', function(e){
	                			//$(this).replaceWith('<a id="dotPagesAnchor'+defaults.uniqueIdentifier+'" class="dotPagesAnchor"><input type="text" name="txtdotPagesAnchor'+defaults.uniqueIdentifier+'" id="txtdotPagesAnchor'+defaults.uniqueIdentifier+'" value="'+currentValue+'" style="width:45px;height:20px;border-radius:20%; border-color:lightgray;border-style:solid;"></a>')
                				if(e.which == 13) {
                					//alert($(this).val()) 
                				 $('#navp'+defaults.uniqueIdentifier+' a').removeClass('active');
                       	         $('#navp'+defaults.uniqueIdentifier+' li').removeClass('active');
                       	         $(this).addClass('active');
                       	         $(this).parent().addClass('active');
                       	         var currPage = parseInt($(this).val());
                       	         $('#hidPagination'+defaults.uniqueIdentifier).val(currPage);
                       	         //$('#dotPagesAnchor'+defaults.uniqueIdentifier).html(currPage+1)
                       	         var startItem = currPage * rowsShown;
                   		         var endItem = startItem + rowsShown;
                   		         //$('#'+defaults.tBodyName+' tr').css('opacity','0.0').hide().slice(startItem, endItem).css('display','table-row').animate({opacity:1}, 300);
                   		         $('#'+defaults.tBodyName+' tr').css('opacity','0.0').hide().slice(startItem, endItem+1).css('display','table-row').animate({opacity:1}, 500);
                				}
	                		});
                		});
    	            	
    	            	
    	            	
    	            }
    	            return false; 
    			});
    	     
    		} else {
    			$("#paginationSpecerRow").css('display','block');
    		}    		
    	//End of Minimal view of pagination	
    	}
    });
};