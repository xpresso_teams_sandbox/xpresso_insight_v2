function ticketSummaryDummy(authName) {
	
        // Build the chart
        $('#ticketMang').highcharts({
            colors: ['#f58548', '#7ba75c', '#f55348'],
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                valueSuffix: ' %'
            },
            plotOptions: {
                pie: {
                    innerSize: 110,
                    depth: 45,
                    point: {
                        events: {
                            click: function(e) {
                                //$('#ticketRes').show();
                                fn_click(this.name);
                            }
                        }
                    },
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            credits: {
                enabled: false
            },
			legend: {
				itemWidth: 120
			},
            series: [{
                name: 'Tickets',
                colorByPoint: true,
                data: [
                    ['Open', 40],
                    ['Closed', 35],
                    ['Reopen', 25]
                ]
            }]
        });

		$('#ticketResTime').highcharts({
            colors: ['#f9cd11', '#f55348', '#d1223b', '#ae0119'],
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                valueSuffix: ' %'
            },
            plotOptions: {
                pie: {
                    innerSize: 110,
                    depth: 45,
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            credits: {
                enabled: false
            },
			legend: {
				itemWidth: 120
			},
            series: [{
                name: 'Tickets',
                colorByPoint: true,
                data: [
                    ['<7days', 23],
                    ['7-15 days', 35],
                    ['16-30 days', 25],
					['>30 days', 17]
                ]
            }]
        });

        var optionsHDFC = {
			colors: ['#f58548', '#f5854d', '#f58552', '#f58557', '#f5855c', '#f58561', '#f58566', '#f5856b'],
            chart: {
                type: 'bar'
            },
            title: {
                text: ''
            },
            tooltip: {
                valueSuffix: ' %'
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            credits: {
                enabled: false
            },
            legend: {
                enabled: false
            },
            xAxis: {
                categories: [],
                title: {
                    text: null
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: '',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            series: [{
                name: 'Tickets',
                colorByPoint: true,
                data: [
                    ['SCHEME AND PAYMENT', 23],
                    ['MOBILE BANKING', 18],
                    ['BANKING SERVICES', 15],
                    ['LOAN SERVICES', 13],
                    ['ONLINE BANKING', 10],
                    ['SECURITY', 10],
                    ['CARD SERVICES', 6],
                    ['CUSTOMER SERVICES', 5]

                ]
            }]
        };
        
        var optionsASDA = {
    			colors: ['#f58548', '#f5854d', '#f58552', '#f58557', '#f5855c', '#f58561', '#f58566', '#f5856b'],
                chart: {
                    type: 'bar'
                },
                title: {
                    text: ''
                },
                tooltip: {
                    valueSuffix: ' %'
                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
                credits: {
                    enabled: false
                },
                legend: {
                    enabled: false
                },
                xAxis: {
                    categories: [],
                    title: {
                        text: null
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: '',
                        align: 'high'
                    },
                    labels: {
                        overflow: 'justify'
                    }
                },
                series: [{
                    name: 'Tickets',
                    colorByPoint: true,
                    data: [
                        ['FRESH FOOD', 23],
                        ['SERVICE', 18],
                        ['INVENTORY', 15],
                        ['PRICE AND OFFERS', 13],
                        ['CHECKOUT', 10],
                        ['FRIENDLINESS', 6],
                        ['DEPARTMENT', 5],
                        ['CLEANLINESS', 5]

                    ]
                }]
            };
        
        var optionsMCI = {
    			colors: ['#f58548', '#f5854d', '#f58552', '#f58557', '#f5855c', '#f58561', '#f58566', '#f5856b'],
                chart: {
                    type: 'bar'
                },
                title: {
                    text: ''
                },
                tooltip: {
                    valueSuffix: ' %'
                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
                credits: {
                    enabled: false
                },
                legend: {
                    enabled: false
                },
                xAxis: {
                    categories: [],
                    title: {
                        text: null
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: '',
                        align: 'high'
                    },
                    labels: {
                        overflow: 'justify'
                    }
                },
                series: [{
                    name: 'Tickets',
                    colorByPoint: true,
                    data: [
                        ['LAW', 23],
                        ['TRANSPORTATION', 18],
                        ['SPORTS', 15],
                        ['EDUCATION', 13],
                        ['ECONOMY', 10],
                        ['SOCIETY', 6],
                        ['HEALTHCARE', 5],
                        ['MANPOWER', 5]

                    ]
                }]
            };
        
        var optionsAirtel = {
    			colors: ['#f58548', '#f5854d', '#f58552', '#f58557', '#f5855c', '#f58561'],
                chart: {
                    type: 'bar'
                },
                title: {
                    text: ''
                },
                tooltip: {
                    valueSuffix: ' %'
                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
                credits: {
                    enabled: false
                },
                legend: {
                    enabled: false
                },
                xAxis: {
                    categories: [],
                    title: {
                        text: null
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: '',
                        align: 'high'
                    },
                    labels: {
                        overflow: 'justify'
                    }
                },
                series: [{
                    name: 'Tickets',
                    colorByPoint: true,
                    data: [
                        ['CONNECTIVITY', 23],
                        ['PLAN AND CHARGES', 18],
                        ['DATA', 15],
                        ['DEVICES', 10],
                        ['SERVICE', 6],
                        ['CAMPAIGN', 5]
                    ]
                }]
            };
        
        /*function fn_click(name){
        	
        }*/

        function fn_click(name) {
            //alert(name)
            var newoptionsHDFC1 = {
				colors: ['#f58548', '#f5854d', '#f58552', '#f58557', '#f5855c', '#f58561', '#f58566', '#f5856b'],
                chart: {
                    type: 'bar'
                },
                title: {
                    text: ''
                },
                tooltip: {
                    valueSuffix: ' %'
                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
                credits: {
                    enabled: false
                },
                legend: {
                    enabled: false
                },
                xAxis: {
                    categories: [],
                    title: {
                        text: null
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: '',
                        align: 'high'
                    },
                    labels: {
                        overflow: 'justify'
                    }
                },
                series: [{
                    name: 'Tickets',
                    colorByPoint: true,
                    data: [
                        ['SCHEME AND PAYMENT', 23],
                        ['MOBILE BANKING', 18],
                        ['BANKING SERVICES', 15],
                        ['LOAN SERVICES', 13],
                        ['ONLINE BANKING', 10],
                        ['SECURITY', 10],
                        ['CARD SERVICES', 6],
                        ['CUSTOMER SERVICES', 5]

                    ]
                }]
            };
            var newoptionsHDFC2 = {
				colors: ['#7ba75c', '#7ba761', '#7ba766', '#7ba76b', '#7ba770', '#7ba775'],
                chart: {
                    type: 'bar'
                },
                title: {
                    text: ''
                },
                tooltip: {
                    valueSuffix: ' %'
                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
                credits: {
                    enabled: false
                },
                legend: {
                    enabled: false
                },
                xAxis: {
                    categories: [],
                    title: {
                        text: null
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: '',
                        align: 'high'
                    },
                    labels: {
                        overflow: 'justify'
                    }
                },
                series: [{
                    name: 'Tickets',
                    colorByPoint: true,
                    data: [
						['BANKING SERVICES', 35],
                        ['MOBILE BANKING', 28],
						['CARD SERVICES', 25],
                        ['SCHEME AND PAYMENT', 23],
                        ['LOAN SERVICES', 23],
                        ['SECURITY', 20]

                    ]
                }]
            };

            var newoptionsHDFC3 = {
				colors: ['#f55348', '#f5534d', '#f55352', '#f55357'],
                chart: {
                    type: 'bar'
                },
                title: {
                    text: ''
                },
                tooltip: {
                    valueSuffix: ' %'
                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
                credits: {
                    enabled: false
                },
                legend: {
                    enabled: false
                },
                xAxis: {
                    categories: [],
                    title: {
                        text: null
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: '',
                        align: 'high'
                    },
                    labels: {
                        overflow: 'justify'
                    }
                },
                series: [{
                    name: 'Tickets',
                    colorByPoint: true,
                    data: [
                        ['CUSTOMER SERVICES', 55],
						['ONLINE BANKING', 40],
						['MOBILE BANKING', 18],
                        ['SCHEME AND PAYMENT', 13]
                    ]
                }]
            };
            
            var newoptionsASDA1 = {
    				colors: ['#f58548', '#f5854d', '#f58552', '#f58557', '#f5855c', '#f58561', '#f58566', '#f5856b'],
                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: ''
                    },
                    tooltip: {
                        valueSuffix: ' %'
                    },
                    plotOptions: {
                        bar: {
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    legend: {
                        enabled: false
                    },
                    xAxis: {
                        categories: [],
                        title: {
                            text: null
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: '',
                            align: 'high'
                        },
                        labels: {
                            overflow: 'justify'
                        }
                    },
                    series: [{
                        name: 'Tickets',
                        colorByPoint: true,
                        data: [
                               ['FRESH FOOD', 23],
                               ['SERVICE', 18],
                               ['INVENTORY', 15],
                               ['PRICE AND OFFERS', 13],
                               ['CHECKOUT', 10],
                               ['FRIENDLINESS', 6],
                               ['DEPARTMENT', 5],
                               ['CLEANLINESS', 5]

                           ]
                    }]
                };
                var newoptionsASDA2 = {
    				colors: ['#7ba75c', '#7ba761', '#7ba766', '#7ba76b', '#7ba770', '#7ba775'],
                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: ''
                    },
                    tooltip: {
                        valueSuffix: ' %'
                    },
                    plotOptions: {
                        bar: {
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    legend: {
                        enabled: false
                    },
                    xAxis: {
                        categories: [],
                        title: {
                            text: null
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: '',
                            align: 'high'
                        },
                        labels: {
                            overflow: 'justify'
                        }
                    },
                    series: [{
                        name: 'Tickets',
                        colorByPoint: true,
                        data: [
    						['SERVICE', 35],
                            ['FRESH FOOD', 28],
    						['INVENTORY', 25],
                            ['CHECKOUT', 23],
                            ['FRIENDLINESS', 23],
                            ['DEPARTMENT', 20]

                        ]
                    }]
                };

                var newoptionsASDA3 = {
    				colors: ['#f55348', '#f5534d', '#f55352', '#f55357'],
                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: ''
                    },
                    tooltip: {
                        valueSuffix: ' %'
                    },
                    plotOptions: {
                        bar: {
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    legend: {
                        enabled: false
                    },
                    xAxis: {
                        categories: [],
                        title: {
                            text: null
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: '',
                            align: 'high'
                        },
                        labels: {
                            overflow: 'justify'
                        }
                    },
                    series: [{
                        name: 'Tickets',
                        colorByPoint: true,
                        data: [
                            ['PARKING', 55],
    						['LAYOUT', 40],
    						['CLEANLINESS', 18],
                            ['SERVICE', 13]
                        ]
                    }]
                };
                
                var newoptionsAirtel1 = {
        				colors: ['#f58548', '#f5854d', '#f58552', '#f58557', '#f5855c', '#f58561'],
                        chart: {
                            type: 'bar'
                        },
                        title: {
                            text: ''
                        },
                        tooltip: {
                            valueSuffix: ' %'
                        },
                        plotOptions: {
                            bar: {
                                dataLabels: {
                                    enabled: true
                                }
                            }
                        },
                        credits: {
                            enabled: false
                        },
                        legend: {
                            enabled: false
                        },
                        xAxis: {
                            categories: [],
                            title: {
                                text: null
                            }
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: '',
                                align: 'high'
                            },
                            labels: {
                                overflow: 'justify'
                            }
                        },
                        series: [{
                            name: 'Tickets',
                            colorByPoint: true,
                            data: [
                                ['CONNECTIVITY', 23],
                                ['DATA', 18],
                                ['SERVICE', 15],
                                ['DEVICES', 13],
                                ['CAMPAIGN', 10],
                                ['PLAN AND CHARGES', 10]
                            ]
                        }]
                    };
                
                var newoptionsAirtel2 = {
        				colors: ['#f58548', '#f5854d', '#f58552', '#f58557', '#f5855c', '#f58561'],
                        chart: {
                            type: 'bar'
                        },
                        title: {
                            text: ''
                        },
                        tooltip: {
                            valueSuffix: ' %'
                        },
                        plotOptions: {
                            bar: {
                                dataLabels: {
                                    enabled: true
                                }
                            }
                        },
                        credits: {
                            enabled: false
                        },
                        legend: {
                            enabled: false
                        },
                        xAxis: {
                            categories: [],
                            title: {
                                text: null
                            }
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: '',
                                align: 'high'
                            },
                            labels: {
                                overflow: 'justify'
                            }
                        },
                        series: [{
                            name: 'Tickets',
                            colorByPoint: true,
                            data: [
                                ['CONNECTIVITY', 23],
                                ['DATA', 18],
                                ['SERVICE', 15],
                                ['DEVICES', 13],
                                ['CAMPAIGN', 10],
                                ['PLAN AND CHARGES', 10]
                            ]
                        }]
                    };
                
                var newoptionsAirtel3 = {
        				colors: ['#f58548', '#f5854d', '#f58552', '#f58557', '#f5855c', '#f58561', '#f58566', '#f5856b'],
                        chart: {
                            type: 'bar'
                        },
                        title: {
                            text: ''
                        },
                        tooltip: {
                            valueSuffix: ' %'
                        },
                        plotOptions: {
                            bar: {
                                dataLabels: {
                                    enabled: true
                                }
                            }
                        },
                        credits: {
                            enabled: false
                        },
                        legend: {
                            enabled: false
                        },
                        xAxis: {
                            categories: [],
                            title: {
                                text: null
                            }
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: '',
                                align: 'high'
                            },
                            labels: {
                                overflow: 'justify'
                            }
                        },
                        series: [{
                            name: 'Tickets',
                            colorByPoint: true,
                            data: [
                                ['CONNECTIVITY', 23],
                                ['DATA', 18],
                                ['SERVICE', 15],
                                ['DEVICES', 13],
                                ['CAMPAIGN', 10],
                                ['PLAN AND CHARGES', 10]
                            ]
                        }]
                    };

            
            if(authName == "1"){
            	if (name == "Open") {
                    $('#ticketRes').highcharts(newoptionsHDFC1);
                } else if (name == "Closed") {
                    $('#ticketRes').highcharts(newoptionsHDFC2);
                } else
                    $('#ticketRes').highcharts(newoptionsHDFC3);
    		}
    		else if (authName == "2"){
    			if (name == "Open") {
                    $('#ticketRes').highcharts(newoptionsASDA1);
                } else if (name == "Closed") {
                    $('#ticketRes').highcharts(newoptionsASDA2);
                } else
                    $('#ticketRes').highcharts(newoptionsASDA3);
    		}
    		else if (authName == "8"){
    			if (name == "Open") {
                    $('#ticketRes').highcharts(newoptionsAirtel1);
                } else if (name == "Closed") {
                    $('#ticketRes').highcharts(newoptionsAirtel2);
                } else
                    $('#ticketRes').highcharts(newoptionsAirtel3);
    		}
        }
        if(authName == "1")
        	$('#ticketRes').highcharts(optionsHDFC);
        else if (authName == "2")
        	$('#ticketRes').highcharts(optionsASDA);
        else if (authName == "8")
        	$('#ticketRes').highcharts(optionsAirtel);
        //$('#ticketRes').highcharts(optionsMCI);
        
}