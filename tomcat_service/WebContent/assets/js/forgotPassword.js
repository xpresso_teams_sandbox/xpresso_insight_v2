(function(angular) {
'use strict';
var rcxApp = angular.module('app', ['formly', 'formlyBootstrap', 'ngAnimate', 'ngMessages']);

rcxApp.run(function(formlyConfig, formlyValidationMessages) {
    formlyConfig.extras.ngModelAttrsManipulatorPreferBound = true;
    formlyValidationMessages.addStringMessage('maxlength', 'Your input is WAAAAY too long!');
    formlyValidationMessages.messages.pattern = function(viewValue, modelValue, scope) {
      return viewValue + ' is invalid';
    };
    formlyValidationMessages.addTemplateOptionValueMessage();
  });

rcxApp.config(function (formlyConfigProvider) {
	// set templates here
	formlyConfigProvider.setType({
	name: 'input',
	templateUrl: 'custom.html'
	});
	
	formlyConfigProvider.setWrapper({
	  name: 'validation',
	  types: ['input'],
	  templateUrl: 'error-messages.html'
	});

});

rcxApp.controller('forgotPasswordController', function () {
	
	var forgotForm = this;
	
	forgotForm.model = {};
	forgotForm.options = {};
	
	 
	forgotForm.fields = [
		 
		{
			key: 'userName',
			type: 'input',
			name: 'email',
			templateOptions: {
				type: 'email',
				label: 'Email',
				required: true
			}
		} 
	];
    
	
 
	
});


})(window.angular);

 