/*$(window).load(function() {
	// Animate loader off screen
	$(".se-pre-con").delay(1000).fadeOut("slow");;
});*/

// function to convert any time in dd-MM-yyyy HH:mm format to its GMT time same format
/*function toGMTDate(date){
	if(date == '' || date == null)
        return date;
	//PG09052016 -To Fix IE and Safari Issue
	//date = new Date(date.substring(6,10)+'-'+date.substring(3,6)+date.substring(0,2)+date.substring(10,date.length));
	alert(date + "---1")
	date = new Date((date.substring(6,10)+'-'+date.substring(3,6)+date.substring(0,2)+date.substring(10,date.length)).replace(/-/g, "/"));
	var GMTdate = (new Date(date.valueOf() - authTimezoneOffset * 60000));
	return (('0'+GMTdate.getDate()).slice(-2)+'-'+('0'+(GMTdate.getMonth()+1)).slice(-2)+'-'+GMTdate.getFullYear()+' '+('0'+GMTdate.getHours()).slice(-2)+':'+('0'+GMTdate.getMinutes()).slice(-2));
}*/
/** Function is called from modals.jspf
 * Used for send feedback
 */
function fn_sendFeedback(typeParam){
	$('#responseLoader').show();
	var feedbackUrl= "";
	var msgToPost = "";
	if(typeParam == 't1'){
		msgToPost = $("#feedbackTemplate1").text();
	} else if(typeParam == 't2'){
		msgToPost = $("#feedbackTemplate2").text();
	} else if(typeParam == 't3'){
		msgToPost = $("#feedbackTemplate3").text();
	} else if(typeParam == 't4'){
		//This part is being called from Deep listining 
		var combinedPostIDSourceType =  $("#postId").val();
		var postID = combinedPostIDSourceType.split('#@#')[0];
		var sourceType = combinedPostIDSourceType.split('#@#')[1];
		msgToPost = $("#feedbackUserInput").val();
		if(msgToPost == ''){
			$('#errFeedbackUserInput').show();
		}
		
		if($("#feedbackUrlCheckBox").prop("checked") == true){
	    	feedbackUrl = $("#feedbackUrl").val();
	    }
	} else if(typeParam == 't5'){
		//This is being used for Ticket management page, when user gives feedback for ant ticket
		var combinedPostIDSourceType =  $("#postIdTicket").val();
		var postID = combinedPostIDSourceType.split('#@#')[0];
		var sourceType = combinedPostIDSourceType.split('#@#')[1];
		msgToPost = $("#feedbackUserInputTicket").val();
		if(msgToPost == ''){
			$('#responseLoader').hide();
			alert('Please add some comment.')
			return false;
		}
	}
	
	var masterScope = angular.element($("#masterBody")).scope();
	var strPageAccessToken = masterScope.pageAccessccessToken + "";
	if(sourceType == "fb" || sourceType == "1"){
		$.ajax({
		      type: "POST",
		      url: "https://graph.facebook.com/v2.12/"+postID+"/comments",
		      data: {
		            "message": msgToPost + feedbackUrl,
		            "access_token": strPageAccessToken
	          },
	          success: function(data) {
	        	  $('#responseLoader').hide();
	        	  $('#feedbackUserInput').val('');
	              $('#alertModal').modal("show");
	              $('#alertModal .modal-content').addClass('modal-success');
	              $('#alertTxt').html("Response sent successfully");
	          },
	          failure: function(data) {
	        	  $('#responseLoader').hide();
	        	  $('#feedbackUserInput').val('');
	        	  $('#alertModal').modal("show");
	              $('#alertModal .modal-content').addClass('modal-danger');
	              $('#alertTxt').html("Failed to send response!");
	          }
		 });	

	} else {
		var combinedData = msgToPost+feedbackUrl;
		var dataLength= combinedData.length;
		if(dataLength >280){
			alert("Text length exceeds 280 characters, Can not reply!!");
			return false;
		}
		$.ajax({
		      type: "POST",
		      url: userResponse,
		      data: {
		    	  	"postID": postID,
		            "sourceType": sourceType,
		            "feedbackUrl": feedbackUrl,
		            "msgToPost": msgToPost,
		            "companyId": authPrimaryCompanyID
	          },
	          success: function(data) {
	        	  $('#responseLoader').hide();
	        	  $('#feedbackUserInput').val('');
	        	  $('#feedbackUserInputTicket').val(''); 
	              $('#alertModal').modal("show");
	              $('#ticketInfoModal').modal("show");
	              $('#alertModal .modal-content').addClass('modal-success');
	              $('#alertTxt').html("Response sent successfully");
	          },
	          failure: function(data) {
	        	  $('#responseLoader').hide();
	        	  $('#feedbackUserInput').val('');
	        	  $('#alertModal').modal("show");
	              $('#alertModal .modal-content').addClass('modal-danger');
	              $('#alertTxt').html("Failed to send response!");
	          }
		 });	

	}
    
	
} 

/**
 * 
 * For YUM Brands the date conversion needs to be suppressed. 
 * We need to uniformly keep the dates in IST (both during displaying in UI/ sending in DB).
 * In order effect the above in minimal change the offset consideration line is commented in below two methods.
 */
//function to convert any time in dd-MM-yyyy HH:mm format to its GMT time same format
function toGMTDate(date){
	if(date == '' || date == null)
        return date;
	//PG09052016 -To Fix IE and Safari Issue
	//date = new Date(date.substring(6,10)+'-'+date.substring(3,6)+date.substring(0,2)+date.substring(10,date.length));
	date = new Date((date.substring(6,10)+'-'+date.substring(3,6)+date.substring(0,2)+date.substring(10,date.length)).replace(/-/g, "/"));
	/*var GMTdate = (new Date(date.valueOf() + date.getTimezoneOffset() * 60000));*/
	var GMTdate = (new Date(date.valueOf()));
	return (('0'+GMTdate.getDate()).slice(-2)+'-'+('0'+(GMTdate.getMonth()+1)).slice(-2)+'-'+GMTdate.getFullYear()+' '+('0'+GMTdate.getHours()).slice(-2)+':'+('0'+GMTdate.getMinutes()).slice(-2));
}

function toGMTDateReviewEndDate(date){
	if(date == '' || date == null)
        return date;
	//PG09052016 -To Fix IE and Safari Issue
	//date = new Date(date.substring(6,10)+'-'+date.substring(3,6)+date.substring(0,2)+date.substring(10,date.length));
	date = new Date((date.substring(6,10)+'-'+date.substring(3,6)+date.substring(0,2)+date.substring(10,date.length)).replace(/-/g, "/"));
	/*var GMTdate = (new Date(date.valueOf() + date.getTimezoneOffset() * 60000));*/
	var GMTdate = (new Date(date.valueOf()));
	return (('0'+GMTdate.getDate()).slice(-2)+'-'+('0'+(GMTdate.getMonth()+1)).slice(-2)+'-'+GMTdate.getFullYear()+' 23:59 ');
}

function toSentimentalGMTDate(date){
	if(date == '' || date == null)
        return date;
	//PG09052016 -To Fix IE and Safari Issue
	//date = new Date(date.substring(6,10)+'-'+date.substring(3,6)+date.substring(0,2)+date.substring(10,date.length));
	date = new Date((date.substring(6,10)+'-'+date.substring(3,6)+date.substring(0,2)+date.substring(10,date.length)).replace(/-/g, "/"));
	/*var GMTdate = (new Date(date.valueOf() + date.getTimezoneOffset() * 60000));*/
	var GMTdate = (new Date(date.valueOf()));
	return (('0'+GMTdate.getDate()).slice(-2)+'-'+('0'+(GMTdate.getMonth()+1)).slice(-2)+'-'+GMTdate.getFullYear()+' '+('0'+GMTdate.getHours()).slice(-2)+':'+('0'+GMTdate.getMinutes()).slice(-2));
}
//function to convert any GMT time in dd-MM-yyyy HH:mm format to its local time same format
function toLocalDate(date){
	if(date == '' || date == null)
        return date;
	date = new Date((date.substring(6,10)+'-'+date.substring(3,6)+date.substring(0,2)+date.substring(10,date.length)).replace(/-/g, "/"));
	var localdate = new Date(date.valueOf() + authTimezoneOffset * 60000);
	return (('0'+localdate.getDate()).slice(-2)+'-'+('0'+(localdate.getMonth()+1)).slice(-2)+'-'+localdate.getFullYear()+' '+('0'+localdate.getHours()).slice(-2)+':'+('0'+localdate.getMinutes()).slice(-2));
}

// resetting object parameters to initial values
function resetObj(obj){
	for (var prop in obj){ 
		if (obj.hasOwnProperty(prop)) {
			if(typeof obj[prop] == 'number'){
				obj[prop] = -1; 
			}else{
				obj[prop] = null;
			}
		} 
	}
}

//changes space separated words to Camel Case preserving spaces
function toCamelCase(str) {
	var words = str.split(' ');
	str = '';
	for(var i=0;i<words.length;i++){
		var letter1 = words[i][0].toUpperCase();
		words[i] = letter1 + words[i].slice(1).toLowerCase();
		str += words[i];
		if(i<words.length-1)
			str += ' ';
	}
	return str;
}
//latest news ribbon on top fixed part (in index.jsp) customization parameters
var news = $('#news').newsTicker({
    row_height: 25,
    max_rows: 1,
    duration: 4000,
    pauseOnHover: 1,
    speed: 800,
});


/* ========================= Ticket Management/Act Page functions start ============================ */

function sendTicket(){
	$('#tkt-create-modal').modal('toggle');
	$.ajax({
	      type: "POST",
	      url: createTicketUrl,
	      data: {
	    	    "assignerEmail": authEmail,
	            "assigneeEmail": $("#assigneeEmail").val(),
	            "priority": $("#priority").val(),
	            "dueDate": $("input[name=dueDate]").val(),
	            "comment": $("#comment").val(),
	            "post_id": $(".modal-body #postId").val()
          },
          success: function(data) {
              $('#alertModal').modal("show");
              $('#alertModal .modal-content').addClass('modal-success');
              $('#alertTxt').html("Ticket created successfully");
          },
          failure: function(data) {
        	  $('#alertModal').modal("show");
              $('#alertModal .modal-content').addClass('modal-danger');
              $('#alertTxt').html("Ticket creation failed!");
          }
	 });	
}

//Filling up Create Ticket modal with data
$(document).on("click", ".open-tkt-create-modal", function () {
	$('#createTicket .username').html(authName);
	$('#createTicket .useremail').html(authEmail);
    var post_id = $(this).data('id');
    $(".modal-body #postId").val(post_id);
});


function getTicketDetails(rows){
    arrValsTicket = rows;
  	var dynamicHtml = '<tbody id="tableTicketManagement">';
  	for(var i=0; i<arrValsTicket.length; i++){
  		// putting social icon
  		var closedBy = '-';
      	if(arrValsTicket[i][1] == 1)
  			var sourceIcon = '<i class="fa fa-facebook-official" aria-hidden="true"></i>';

  		else if(arrValsTicket[i][1] == 2)
  			var sourceIcon = '<i class="fa fa-twitter-square" aria-hidden="true"></i>';
  		else
  			var sourceIcon = '<i class="fa fa-rss-square" aria-hidden="true"></i>';
  		
  		// putting class for ticket status
  		if(arrValsTicket[i][2] == 'OPEN'){
  			var status = '<span class="btn nbt-yellow">Open</span>';
  		}else if(arrValsTicket[i][2] == 'CLOSE'){
  			var status = '<span class="btn nbt-green">Closed</span>';
  			closedBy = authName;
  		}else{
  			var status = '<span class="btn nbt-red">Reopen</span>';
  		}
  		
  		if(arrValsTicket[i][3] == 'HIGH' || arrValsTicket[i][3] == 'High')
  			var priority = '<span class="btn Dorange">High</span>';
  		else if(arrValsTicket[i][3] == 'MEDIUM' || arrValsTicket[i][3] == 'Medium')
  			var priority = '<span class="btn Morange">Medium</span>';
  		else
  			var priority = '<span class="btn Lorange">Low</span>';
  		    
  		if(arrValsTicket[i][5] == null)
  			var comment = '-';
  		else
  			var comment = arrValsTicket[i][5];
  		
  		if(arrValsTicket[i][6] == null){
  			var closedDate = '-';
  		}else{
  			var closedDate = arrValsTicket[i][6];
  		}
  			
  		var dueDate = arrValsTicket[i][7] == null? '-' : arrValsTicket[i][7];
  		/*if(arrValsTicket[i][6] == null)
  			
  		else
  			var closedBy = authName;*/
  		
  		dynamicHtml +='<tr>';
  		dynamicHtml +='<td><label class="nbt-checkbox"><input type="checkbox"><span></span></label></td>';
  		dynamicHtml +='<td>'+sourceIcon+'</td>';
  		dynamicHtml +='<td>'+status+'</td>';
  		dynamicHtml +='<td>'+priority+'</td>';
  		dynamicHtml +='<td>'+arrValsTicket[i][4]+'</td>';
  		dynamicHtml +='<td>'+comment+'</td>';
  		dynamicHtml +='<td>'+toCamelCase(arrValsTicket[i][0])+'</td>';
  		dynamicHtml +='<td>'+dueDate+'</td>';
  		dynamicHtml +='<td>'+closedDate+'</td>';
  		dynamicHtml +='<td>'+closedBy+'</td>';
  		dynamicHtml +='<td><a class="open-tkt-mng-modal" href="javascript:void(0);" style="margin-right:10px;" data-toggle="modal" data-row_id="'+i+'" data-ticket_id="'+arrValsTicket[i][10]+'" data-target="#tkt-mng-modal"> <i class="fa fa-pencil" aria-hidden="true"></i></a> <a href="javascript:void(0);"><i class="fa fa-envelope" aria-hidden="true"></i></a></td>';
  		dynamicHtml +='</tr>';
  	
  	}    
	console.log("arrValsTicket: ",arrValsTicket);
  	if(arrValsTicket.length == 0){
		
  		dynamicHtml += '<tr><td colspan="11" class="text-center">No Data Available.</td></tr>';
		console.log("dynamicHtml:",dynamicHtml);
	}
  	dynamicHtml += '</tbody>';          	
  	$('#tableTicketManagement').replaceWith(dynamicHtml);
	
}

function sendManagedTicket(){
	$('#tkt-mng-modal').modal('toggle');
	var target = document.getElementById('tableTicketManagement');
	var spinner = new Spinner(opts).spin(target);
	$.ajax({
	      type: "POST",
	      url: manageTicketUrl,
	      data: {
	    	  	"assignerEmail": authEmail,
	            "assigneeEmail": $("#manageAssigneeEmail").val(),
	            "priority": $("#manageTicketPriority").val(),
	            "status": $("#manageTicketStatus").val(),
	            "dueDate": $("input[name=manageDueDate]").val(),
	            "closedDate": $("input[name=manageClosedDate]").val(),
	            "comment": $("#manageUserComment").val(),
	            "ticket_id": $(".modal-body #manageTicketId").val()
          },
          success: function(data) {
              $('#alertModal').modal("show");
              $('#alertModal .modal-content').addClass('modal-success');
              $('#alertTxt').html("Ticket updated successfully");
              getTicketDetails();
          },
          failure: function(data) {
        	  $('#alertModal').modal("show");
              $('#alertModal .modal-content').addClass('modal-danger');
              $('#alertTxt').html("Failed to update ticket!");
          }
	 });	
}

//Filling up Manage Ticket modal with data
$(document).on("click", ".open-tkt-mng-modal", function () {
	var ticket_id = $(this).data('ticket_id');
	var row_id = $(this).data('row_id');
	$(".modal-body #manageTicketId").val(ticket_id);
	$("#manageTicketStatus").val(arrValsTicket[row_id][2].toLowerCase());
	$("#manageTicketPriority").val(arrValsTicket[row_id][3].toLowerCase());
	$("#manageAssigneeEmail").val(arrValsTicket[row_id][4]);
	$("#mDueDate").val(arrValsTicket[row_id][7]);
	$("#mClosedDate").val(arrValsTicket[row_id][6]);
	$("#manageUserComment").val(arrValsTicket[row_id][5]);
});

/* ========================= Ticket Management/Act Page functions end ============================ */

$(".sp1 a").click(function(event){
		event.stopPropagation();
	$(".dp1").toggle();
	$(".dp2, .dp3, .user-div").hide();
});
$(".sp2 a").click(function(event){
		event.stopPropagation();
	$(".dp2").toggle();
	$(".dp1, .dp3, .user-div").hide();
});

$(".sp4 a").click(function(event){
		event.stopPropagation();
	$(".user-div").toggle();
	$(".dp1, .dp2, .dp3").hide();
});

$(".sp3 a").click(function(event){
		event.stopPropagation();
	$(".dp3").toggle();
	$(".dp1, .dp2, .user-div").hide();
});

$(document).on('click', function (e) {
	if ($(e.target).closest('.dp1, .dp2, .dp3, .user-div').length === 0) {
		$('.dp1, .dp2, .dp3, .user-div').hide();
	}
});

$(document).ready(function(){
	$('#Word_Cloud_on_Feedbac').click(function(){});
	$(".slimScrollDiv").addClass('no-scroll');
    $(".social-feeds").addClass('no-scroll');
   /* $('#commentSearch').keyup(function(){
   
	  // Search text
	  var text = $(this).val();
	  // Hide all content class element
	  $('.content').hide();

	  // Search 
	  $('.content:contains("'+text+'")').closest('.content').show();
	 
	 });
    
    $('#postSearch').keyup(function(){
    	//alert("pppooo")
  	  // Search text
  	  var text = $(this).val();
  	  // Hide all content class element
  	  $('.postContent').hide();

  	  // Search 
  	  $('.postContent:contains("'+text+'")').closest('.postContent').show();
  	 
  	 });*/
});
/*$.expr[":"].contains = $.expr.createPseudo(function(arg) {
  return function( elem ) {
   return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
  };
});*/
function generatePdfForsectionAll(sectionIDs){
	
	generatePdfForsection('page1','Overall sentiment analysis by media type');
	setTimeout(function(){
		generatePdfForsection('page2','Word Cloud on Feedback');
	},2000);
	setTimeout(function(){
		generatePdfForsection('page3','Context Wise Feedback Distribution');
	},5000);
	setTimeout(function(){
		//generatePdfForsection_special_handel('page4','Verbatism across all sources and by CHAMPS context');
		generatePdfForsection('page4','Verbatism across all sources and by CHAMPS context');
	},8000);
	setTimeout(function(){
		generatePdfForsection('page5','Context Wise Sentiment & Entity Distribution');
	},11000);
	
	//generatePdfForsection('page1');
	//generatePdfForsection('page1');
	//generatePdfForsection('page1');
}
function generatePdfForsection(sectionID,pdfName){
		$("#generatePDFspinner").show();
		$(".extra-height-indivisual").show();
	    $(".footer").hide();
	    $(".hide-for-pdf").hide();
	    $(".social-feeds").addClass('feed-pdf');
	   // $(".slimScrollDiv").addClass('no-scroll');
	   // $(".social-feeds").addClass('no-scroll');
	    
	    var svgElements = $("#"+sectionID).find('svg');
	
	    //replace all svgs with a temp canvas
	    svgElements.each(function() {
		var canvas, xml;
	
		// canvg doesn't cope very well with em font sizes so find the calculated size in pixels and replace it in the element.
		$.each($(this).find('[style*=em]'), function(index, el) {
		  $(this).css('font-size', getStyle(el, 'font-size'));
		});
	
		canvas = document.createElement("canvas");
		canvas.className = "screenShotTempCanvas";
		//convert SVG into a XML string
		xml = (new XMLSerializer()).serializeToString(this);
	
		// Removing the name space as IE throws an error
		xml = xml.replace(/xmlns=\"http:\/\/www\.w3\.org\/2000\/svg\"/, '');
	
		//draw the SVG onto a canvas
		canvg(canvas, xml);
		$(canvas).insertAfter(this);
		
		//hide the SVG element
		$(this).attr('class', 'tempHide');
		$(this).hide();
	  });
	    
	  html2canvas($("#"+sectionID), {
		  	background :'#fcc',
			onrendered: function(canvas) {
				
				var imgWidth = 210; 
				var pageHeight = 295;  
				var imgHeight = canvas.height * imgWidth / canvas.width;
				//alert("imgHeight / heightLeft : " + imgHeight);
				var heightLeft = imgHeight;
				var doc = new jsPDF('p', 'mm','a4');
				var position = 8;
				var imgData = canvas.toDataURL('image/jpeg', 1.0);
				doc.addImage(imgData, 'JPEG', 0, position, imgWidth, imgHeight);
				heightLeft -= pageHeight;
				//alert("heightLeft after sub : " + heightLeft);
				while (heightLeft >= 0) {
					  position = heightLeft - imgHeight;
					  //alert("position : " + position)
					  doc.addPage();
					  doc.addImage(imgData, 'JPEG', 0, position, imgWidth, imgHeight);
					  heightLeft -= pageHeight;
					  //alert("heightLeft inside while : " + heightLeft)
					}
				doc.save( pdfName+'.pdf');
				
			  /*var imgData = canvas.toDataURL('image/jpeg', 1.0);
			  var doc = new jsPDF("l", "mm", "a3");
			  doc.addImage(imgData, 'JPEG', 50, 30);
			  doc.save('sample-file2222.pdf');*/
			  $('.tempHide').show();
			  $("#"+sectionID).find('.tempHide').removeAttr("class");
			  $("#"+sectionID).find('canvas').remove();
			 // $(".slimScrollDiv").removeClass('no-scroll');
			 // $(".social-feeds").removeClass('no-scroll');
			  $("#generatePDFspinner").hide();
			  $(".hide-for-pdf").show();
			  $(".footer").show();
			  $(".extra-height-indivisual").hide();
			  $(".social-feeds").removeClass('feed-pdf');
			}
	  });
}

function generatePdfForsection_special_handel(sectionID,pdfName){
	setTimeout(function(){
		generateVerbatismMultiplePDF('verba_ACCUR','Verbatism across all sources and by CHAMPS context - ACCURACY');
	},2000);
	setTimeout(function(){
		generateVerbatismMultiplePDF('verba_MAINT','Verbatism across all sources and by CHAMPS context - MAINTNANCE');
	},4000);
	setTimeout(function(){
		generateVerbatismMultiplePDF('verba_SPEED','Verbatism across all sources and by CHAMPS context - SPEED & DELIVERY');
	},6000);
	setTimeout(function(){
		generateVerbatismMultiplePDF('verba_CLEAN','Verbatism across all sources and by CHAMPS context - CLEANINESS');
	},8000);
	setTimeout(function(){
		generateVerbatismMultiplePDF('verba_HOSPI','Verbatism across all sources and by CHAMPS context - HOSPITALITY');
	},10000);
	setTimeout(function(){
		generateVerbatismMultiplePDF('verba_PRICE','Verbatism across all sources and by CHAMPS context - PRICE AND OFFERS');
	},12000);
	setTimeout(function(){
		generateVerbatismMultiplePDF('verba_PRODU','Verbatism across all sources and by CHAMPS context - PRODUCT QUALITY');
	},14000);
	
	
}

function generateVerbatismMultiplePDF(selectorChartDivID,pdfName){
	//alert(sectionID);
    $(".footer").hide();
    $(".hide-for-pdf").hide();
    $(".slimScrollDiv").addClass('no-scroll');
    $(".social-feeds").addClass('no-scroll');
    
    var svgElements = $("#"+selectorChartDivID).find('svg');

    //replace all svgs with a temp canvas
    svgElements.each(function() {
	var canvas, xml;

	// canvg doesn't cope very well with em font sizes so find the calculated size in pixels and replace it in the element.
	$.each($(this).find('[style*=em]'), function(index, el) {
	  $(this).css('font-size', getStyle(el, 'font-size'));
	});

	canvas = document.createElement("canvas");
	canvas.className = "screenShotTempCanvas";
	//convert SVG into a XML string
	xml = (new XMLSerializer()).serializeToString(this);

	// Removing the name space as IE throws an error
	xml = xml.replace(/xmlns=\"http:\/\/www\.w3\.org\/2000\/svg\"/, '');

	//draw the SVG onto a canvas
	canvg(canvas, xml);
	$(canvas).insertAfter(this);
	
	//hide the SVG element
	$(this).attr('class', 'tempHide');
	$(this).hide();
  });
    
  html2canvas($("#"+selectorChartDivID), {
	  	background :'#fcc',
		onrendered: function(canvas) {
			
			var imgWidth = 210; 
			var pageHeight = 295;  
			var imgHeight = canvas.height * imgWidth / canvas.width;
			//alert("imgHeight / heightLeft : " + imgHeight);
			var heightLeft = imgHeight;
			var doc = new jsPDF('p', 'mm','a4');
			var position = 0;
			var imgData = canvas.toDataURL('image/jpeg', 1.0);
			doc.addImage(imgData, 'JPEG', 0, position, imgWidth, imgHeight);
			heightLeft -= pageHeight;
			//alert("heightLeft after sub : " + heightLeft);
			while (heightLeft >= 0) {
				  position = heightLeft - imgHeight;
				  //alert("position : " + position)
				  doc.addPage();
				  doc.addImage(imgData, 'JPEG', 0, position, imgWidth, imgHeight);
				  heightLeft -= pageHeight;
				  //alert("heightLeft inside while : " + heightLeft)
				}
			doc.save( pdfName+'.pdf');
			
		  /*var imgData = canvas.toDataURL('image/jpeg', 1.0);
		  var doc = new jsPDF("l", "mm", "a3");
		  doc.addImage(imgData, 'JPEG', 50, 30);
		  doc.save('sample-file2222.pdf');*/
		  $('.tempHide').show();
		  $("#"+sectionID).find('.tempHide').removeAttr("class");
		  $("#"+sectionID).find('canvas').remove();
		  $(".slimScrollDiv").removeClass('no-scroll');
		  $(".social-feeds").removeClass('no-scroll');
		  $(".hide-for-pdf").show();
		  $(".footer").show();
		}
	  });


}

function getTopHeaderScreenShootDashboard() {
	  // removing the footer temporarily so that it doesn't come in the middle of the page 
	  $("#dashboard").css('background','#fff');
	  $("#generatePDFspinner").show();
	  $(".footer").hide();
	  $(".extra-height").show();
	  $(".hide-for-pdf").hide();
	  $(".social-feeds").addClass('feed-pdf');
	  $('#topHearderLogoHidden').show();
	  var svgElements = $("#dashboard").find('svg');
	  //alert(svgElements.length)
	  //replace all svgs with a temp canvas
	  svgElements.each(function() {
		var canvas, xml;

		// canvg doesn't cope very well with em font sizes so find the calculated size in pixels and replace it in the element.
		$.each($(this).find('[style*=em]'), function(index, el) {
		  $(this).css('font-size', getStyle(el, 'font-size'));
		});

		canvas = document.createElement("canvas");
		canvas.className = "screenShotTempCanvas";
		//convert SVG into a XML string
		xml = (new XMLSerializer()).serializeToString(this);

		// Removing the name space as IE throws an error
		xml = xml.replace(/xmlns=\"http:\/\/www\.w3\.org\/2000\/svg\"/, '');

		//draw the SVG onto a canvas
		canvg(canvas, xml);
		$(canvas).insertAfter(this);
		
		//hide the SVG element
		$(this).attr('class', 'tempHide');
		$(this).hide();
	  });


	  html2canvas($("#dashboard"), {
		onrendered: function(canvas) {
		 /* var imgData = canvas.toDataURL('image/jpeg', 1.0);
		  var doc = new jsPDF("p", "mm", "a4");
		  doc.addImage(imgData, 'JPEG', 10, 10, 180, 220);
		  doc.save('sample-file.pdf');*/
			
			var imgWidth = 210; 
			var pageHeight = 295;  
			var imgHeight = canvas.height * imgWidth / canvas.width;
			var heightLeft = imgHeight;
			var doc = new jsPDF('p', 'mm','a4');
			var position = 15;
			var imgData = canvas.toDataURL('image/jpeg', 1.0);
			doc.addImage(imgData, 'JPEG', 0, position, imgWidth, imgHeight);
			heightLeft -= pageHeight;
			while (heightLeft >= 0) {
				  position = heightLeft - imgHeight;
				  doc.addPage();
				  doc.addImage(imgData, 'JPEG', 0, position, imgWidth, imgHeight);
				  heightLeft -= pageHeight;
				}
			var pageCount = doc.internal.getNumberOfPages();
			
			for(var i = 0; i < pageCount; i++) { 
				// value if i==1 means first page only
				if(i == 1){
					doc.setPage(i); 
					
					html2canvas(document.getElementById('topHearderLogoHidden'), {
			            onrendered: function (canvas) {
			                var imgLogo = canvas.toDataURL('image/jpeg');
			                console.log(imgLogo);//remove
			                //var doc = new jsPDF();
			                var width = (doc.internal.pageSize.width) - 20;
			                console.log(">>>>>>>" + width)
			                var height = 18;
			                doc.addImage(imgLogo, 'JPEG', 10, 1, width, height);
			                doc.save("Dashboard.pdf")
			                console.log("------------");
			               // doc.save('sample-file.pdf');
			            }
			        });
				}
			}
		  $('#topHearderLogoHidden').hide();
		  $('.tempHide').show();
		  $("#masterBody").find('.tempHide').removeAttr("class");;
		  $("#masterBody").find('canvas').remove();
		  $(".social-feeds").removeClass('feed-pdf');
		  $(".extra-height").hide();
		  $(".hide-for-pdf").show();
		  $(".footer").show();
		  $("#generatePDFspinner").hide();
		}
	  });
	};


function getTopHeaderScreenShoot() {
  // removing the footer temporarily so that it doesn't come in the middle of the page 
  $(".footer").hide();
  $(".extra-height").show();
  var svgElements = $("#masterBody").find('svg');
  //alert(svgElements.length)
  //replace all svgs with a temp canvas
  svgElements.each(function() {
	var canvas, xml;

	// canvg doesn't cope very well with em font sizes so find the calculated size in pixels and replace it in the element.
	$.each($(this).find('[style*=em]'), function(index, el) {
	  $(this).css('font-size', getStyle(el, 'font-size'));
	});

	canvas = document.createElement("canvas");
	canvas.className = "screenShotTempCanvas";
	//convert SVG into a XML string
	xml = (new XMLSerializer()).serializeToString(this);

	// Removing the name space as IE throws an error
	xml = xml.replace(/xmlns=\"http:\/\/www\.w3\.org\/2000\/svg\"/, '');

	//draw the SVG onto a canvas
	canvg(canvas, xml);
	$(canvas).insertAfter(this);
	
	//hide the SVG element
	$(this).attr('class', 'tempHide');
	$(this).hide();
  });


  html2canvas($("#masterBody"), {
	onrendered: function(canvas) {
	 /* var imgData = canvas.toDataURL('image/jpeg', 1.0);
	  var doc = new jsPDF("p", "mm", "a4");
	  doc.addImage(imgData, 'JPEG', 10, 10, 180, 220);
	  doc.save('sample-file.pdf');*/
		
		var imgWidth = 210; 
		var pageHeight = 295;  
		var imgHeight = canvas.height * imgWidth / canvas.width;
		var heightLeft = imgHeight;
		var doc = new jsPDF('p', 'mm','a4');
		var position = 0;
		var imgData = canvas.toDataURL('image/jpeg', 1.0);
		doc.addImage(imgData, 'JPEG', 0, position, imgWidth, imgHeight);
		heightLeft -= pageHeight;
		while (heightLeft >= 0) {
			  position = heightLeft - imgHeight;
			  doc.addPage();
			  doc.addImage(imgData, 'JPEG', 0, position, imgWidth, imgHeight);
			  heightLeft -= pageHeight;
			}
		doc.save( 'screenshot.pdf');
		/*var imgData = canvas.toDataURL('image/jpeg', 1.0);
		 var doc = new jsPDF("p", "mm", "a4");
		 doc.addImage(imgData, 'JPEG', 10, 10, 180, 220);
		 doc.save('screenshot.pdf');
		 $('.tempHide').show();
		 $("#masterBody").find('.tempHide').removeAttr("class");;
		 $("#masterBody").find('canvas').remove();
		 $(".footer").show();*/
		
	  $('.tempHide').show();
	  $("#masterBody").find('.tempHide').removeAttr("class");;
	  $("#masterBody").find('canvas').remove();
	  $(".extra-height").hide();
	  $(".footer").show();
	}
  });
};



function getStyle(el, styleProp) {
  var camelize = function(str) {
    return str.replace(/\-(\w)/g, function(str, letter) {
      return letter.toUpperCase();
    });
  };

  if (el.currentStyle) {
    return el.currentStyle[camelize(styleProp)];
  } else if (document.defaultView && document.defaultView.getComputedStyle) {
    return document.defaultView.getComputedStyle(el, null)
      .getPropertyValue(styleProp);
  } else {
    return el.style[camelize(styleProp)];
  }
}

/*---------------Function to round to n-th decimal points--------------*/
function decimalAdjust(type, value, exp) {
    // If the exp is undefined or zero...
    if (typeof exp === 'undefined' || +exp === 0) {
      return Math[type](value);
    }
    value = +value;
    exp = +exp;
    // If the value is not a number or the exp is not an integer...
    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
      return NaN;
    }
    // Shift
    value = value.toString().split('e');
    value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
  }

  // Decimal round
  if (!Math.roundDec) {
    Math.roundDec = function(value, exp) {
      return decimalAdjust('round', value, exp);
    };
  }
  
 
  /*-------Array Contains method-------*/
  
  function arrayContains (arr, str){
	return !!~arr.indexOf(str) 
 }
 /*---------Function to return class name for calendar chart in analyze page (Most engaging by Month)---------*/
function dateExistsInResp(arr,day) {
	var $class='', flag=0, count;
	for(var i in arr) {
		if(arr[i].day === day){
			flag = 1;
			count = arr[i].count;
			break;
		}					
	}; 
	if(flag)
	{
		return arr[i];
	}		
	else{
		return '';
	}
		
}

function getMonday() {
	var curr = new Date;
	var first = curr.getDate() - curr.getDay();
	var newFirst = first + 1
	//var last = newFirst + 6;
	var monday = new Date(curr.setDate(newFirst)).toUTCString();
	//var sunday = new Date(curr.setDate(last)).toUTCString();
	return monday;
}

function daysInMonth (month, year) {
    return new Date(year, month, 0).getDate();
}

function goBack() {
	var deepListBackButton = angular.element($("#masterBody")).scope();
	deepListBackButton.isFromBackButton  = true;
	
    window.history.back();
}
