$(function () {

    $(document).ready(function () {

        // Brand Insights - Mentions by Source
        $('#cxMentionSource').highcharts({
			colors: ['#4867AA','#5EA9DD','#0C4357'],
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
				backgroundColor: '#f1f1f1',
            },
            title: {
                text: ''
            },
            tooltip: {
				valueSuffix: ' mentions'
            },
			legend: {
				layout: 'vertical',
				align: 'right',
				verticalAlign: 'middle',
				borderWidth: 0
			},
			credits: {
			  enabled: false
		  	},
            plotOptions: {
                pie: {
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Mentions Sources',
                colorByPoint: true,
                data: [{
                    name: 'Facebook',
                    y: 478
                }, {
                    name: 'Twitter',
                    y: 240
                }, {
                    name: 'Economic Times',
                    y: 138
                }]
            }]
        });
		
		
		// Brand Insights - Topic Report
		$('#topicReport').highcharts({
        chart: {
			backgroundColor: '#f1f1f1',
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45
            }
        },
        title: {
            text: ''
        },
		credits: {
		  enabled: false
		},
        plotOptions: {
            pie: {
                innerSize: 100,
                depth: 45,
				allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
            }
        },
        series: [{
            name: '\u00a0',
            data: [
                ['MOBILE BANKING', 8],
                ['SCHEME AND PAYMENT', 3],
                ['ONLINE BANKING', 1],
				['CUSTOMER SERVICES', 5],
				['SECURITY', 10],
				['CARD SERVICES', 3],
                ['LOAN SERVICES', 1],
				['BANKING SERVICES', 5]
            ]
        }]
    });
	
	
	// Brand Insights - Complaint volume by Topic
	$('#complaintTopic').highcharts({
        colors: ['#35286c','#f55348','#e8bc00','#ad1010'],
		chart: {
            backgroundColor: '#f1f1f1',
			type: 'spline',
			zoomType: 'x'
        },
        title: {
            text: ''
        },
		credits: {
            enabled: false
        },
        xAxis: {
            type: 'datetime'            
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        tooltip: {
            valueSuffix: ''
        },
        plotOptions: {
            spline: {
                lineWidth: 1,
                states: {
                    hover: {
                        lineWidth: 2
                    }
                },
                marker: {
                    enabled: false
                },
                pointInterval: 1800000, // one hour
                pointStart: Date.UTC(2016, 7, 14, 0, 0, 0)
            }
        },
        series: [{
            name: 'BANKING SERVICES',
            data:[0.7, 0.5, 0.8, 1.8, 1, 1.3, 1.5, 2.9, 1.9, 2.6, 1.6, 3, 4, 3.6, 4.5, 4.2, 4.5, 4.5, 4, 3.1, 2.7, 4, 2.7, 2.3, 2.3, 4.1, 7.7, 7.1, 5.6, 6.1, 5.8, 8.6, 7.2, 9, 10.9, 11.5, 11.6, 11.1, 12, 12.3, 10.7, 9.4, 9.8, 9.6, 9.8, 9.5, 8.5, 7.4, 7.6]
        }, {
            name: 'LOAN SERVICES',
            data: [0, 0, 0.6, 0.9, 0.8, 0.2, 0, 0, 0, 0.1, 0.6, 0.7, 0.8, 0.6, 0.2, 0, 0.1, 0.3, 0.3, 0, 0.1, 0, 0, 0, 0.2, 0.1, 0, 0.3, 0, 0.1, 0.2, 0.1, 0.3, 0.3, 0, 3.1, 3.1, 2.5, 1.5, 1.9, 2.1, 1, 2.3, 1.9, 1.2, 0.7, 1.3, 0.4, 0.3]
			}, {
            name: 'MOBILE BANKING',
            data: [0, 0, 0.6, 1.9, 0.8, 0.2, 2, 0, 0, 0.1, 0.6, 0.7, 0.8, 0.6, 0.2, 0, 0.1, 6.3, 6.3, 6, 6.1, 6, 6, 6, 6.2, 6.1, 6, 6.3, 5, 5.1, 5.2, 5.1, 5.3, 5.3, 5, 4.1, 4.8, 2.5, 1.5, 5.9, 2.1, 1, 2.3, 1.9, 1.2, 0.7, 1.3, 0.4, 0.3]
			}, {
            name: 'CUSTOMER SERVICES',
            data: [0, 4, 5.6, 5.9, 4.8, 4.2, 4.5, 5.1, 5.5, 5.1, 5.6, 5.7, 5.8, 4.6, 4.2, 4.9, 3.1, 3.3, 3.3, 3.9, 4.1, 0, 0, 0, 4.2, 4.1, 0, 0.3, 0, 0.1, 0.2, 0.1, 0.3, 0.3, 0, 3.1, 3.1, 4.5, 4.5, 4.9, 4.1, 5, 2.3, 3.9, 3.2, 3.7, 3.3, 3.4, 3.3]
        }],
        navigation: {
            menuItemStyle: {
                fontSize: '10px'
            }

        }
    });
	
	
	// Brand Insights - Sentiment by Department
	$('#SentimentDepartment').highcharts({
		colors: ['#7ba75c','#f55348','#e8bc00'],
        chart: {
			backgroundColor: '#f1f1f1',
            type: 'column'
        },
		credits: {
            enabled: false
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: ['Banking Services', 'Loan Services', 'Card Services', 'Security', 'Customer Services']
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'percent'
            }
        },
        series: [{
            name: 'Positive',
            data: [5, 3, 4, 7, 2]
        }, {
            name: 'Negative',
            data: [2, 2, 3, 2, 1]
        }, {
            name: 'Neutral',
            data: [3, 4, 4, 2, 5]
        }]
    });
	
	
	// Competition View - Sentiment by Department
	$('#topSourceCom').highcharts({
        colors: ['#A40702','#004B8F'],
        chart: {
			backgroundColor: '#f1f1f1',
            type: 'bar'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: ['Facebook', 'Twitter', 'ET'],
            title: {
                text: ''
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: '',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ' %'
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'ICICI',
            data: [45, 35, 20]
        }, {
            name: 'HDFC',
            data: [37, 49, 14]
        }]
    });
	
	
	// Competition View - Mentions by source
	$('#mentionSourceCom').highcharts({
        chart: {
			backgroundColor: '#f1f1f1',
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: [
                'Facebook',
                'Twitter',
                'ET'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mentions</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
		credits: {
            enabled: false
        },
        series: [{
            name: 'ICICI',
            data: [49.9, 71.5, 106.4]
		}, {
            name: 'HDFC',
            data: [42.4, 33.2, 34.5]

        }]
    });
	
	
	$('#topicReportCom').highcharts({

        chart: {
			backgroundColor: '#f1f1f1',
            type: 'bubble'
        },

        title: {
            text: ''
        },
		credits: {
            enabled: false
        },

        xAxis: {
            gridLineWidth: 1
        },

        yAxis: {
            startOnTick: false,
            endOnTick: false,
			min: 0,
            title: {
                text: ''
            }
        },
		tooltip: {
            useHTML: true,
            headerFormat: '<table>',
            pointFormat: '<tr><th colspan="2"><h3>{point.topic}</h3></th></tr>' +
                '<tr><th>Fat intake:</th><td>{point.x}g</td></tr>' +
                '<tr><th>Sugar intake:</th><td>{point.y}g</td></tr>' +
                '<tr><th>Obesity (adults):</th><td>{point.z}%</td></tr>',
            footerFormat: '</table>',
            followPointer: true
        },

        series: [{
			name: 'ICICI',
            data: [
                { x: 5, y: 5, z: 2, name: 'BE', topic: 'Mobile Banking' },
				{ x: 12, y: 7, z: 5, name: 'BE', topic: 'Belgium' }
            ],
            marker: {
                fillColor: {
                    radialGradient: { cx: 0.4, cy: 0.3, r: 0.7 },
                    stops: [
                        [0, 'rgba(255,255,255,0.5)'],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0.5).get('rgba')]
                    ]
                }
            }
        }, {
			name: 'HDFC',
            data: [
                { x: 7, y: 7, z: 1, name: 'BE', topic: 'Mobile Banking' },
				{ x: 5, y: 12, z: 3, name: 'BE', topic: 'Belgium' }
				
            ],
            marker: {
                fillColor: {
                    radialGradient: { cx: 0.4, cy: 0.3, r: 0.7 },
                    stops: [
                        [0, 'rgba(255,255,255,0.5)'],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[1]).setOpacity(0.5).get('rgba')]
                    ]
                }
            }
        }]

    });
	
	
	
	
	
	
	
	
	
	
	
	
		
		
    });
});