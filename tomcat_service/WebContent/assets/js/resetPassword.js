(function(angular) {
'use strict';
var rcxApp = angular.module('app', ['formly', 'formlyBootstrap', 'ngAnimate', 'ngMessages']);

rcxApp.constant('formlyExampleApiCheck', apiCheck());

rcxApp.config(function config(formlyConfigProvider, formlyExampleApiCheck) {
    // set templates here
    formlyConfigProvider.setType({
      name: 'matchField',
      apiCheck: function() {
        return {
          data: {
            fieldToMatch: formlyExampleApiCheck.string
          }
        };
      },
      apiCheckOptions: {
        prefix: 'matchField type'
      },
      defaultOptions: function matchFieldDefaultOptions(options) {
        return {
          extras: {
            validateOnModelChange: true
          },
          expressionProperties: {
            'templateOptions.disabled': function(viewValue, modelValue, scope) {
              var matchField = find(scope.fields, 'key', options.data.fieldToMatch);
              if (!matchField) {
                throw new Error('Could not find a field for the key ' + options.data.fieldToMatch);
              }
              var model = options.data.modelToMatch || scope.model;
              var originalValue = model[options.data.fieldToMatch];
              var invalidOriginal = matchField.formControl && matchField.formControl.$invalid;
              return !originalValue || invalidOriginal;
            }
          },
          validators: {
            fieldMatch: {
              expression: function(viewValue, modelValue, fieldScope) {
                var value = modelValue || viewValue;
                var model = options.data.modelToMatch || fieldScope.model;
                return value === model[options.data.fieldToMatch];
              },
              message: options.data.matchFieldMessage || '"Password doesnot match"'
            }
          }
        };
        
        function find(array, prop, value) {
          var foundItem;
          array.some(function(item) {
            if (item[prop] === value) {
              foundItem = item;
            }
            return !!foundItem;
          });
          return foundItem;
        }
      }
    });
  });

rcxApp.run(function(formlyConfig, formlyValidationMessages) {
    formlyConfig.extras.ngModelAttrsManipulatorPreferBound = true;
    formlyValidationMessages.addStringMessage('maxlength', 'Your input is WAAAAY too long!');
    formlyValidationMessages.messages.pattern = function(viewValue, modelValue, scope) {
      return viewValue + ' is invalid';
    };
    formlyValidationMessages.addTemplateOptionValueMessage();
  });
  
rcxApp.config(function (formlyConfigProvider) {
	// set templates here
	formlyConfigProvider.setType({
	name: 'input',
	templateUrl: 'custom.html'
	});
	
	formlyConfigProvider.setWrapper({
	  name: 'validation',
	  types: ['input'],
	  templateUrl: 'error-messages.html'
	});

});

rcxApp.controller('resetPasswordController', function () {
	
	var resetForm = this;
	
	resetForm.model = {};
	resetForm.options = {};
	
	 
	resetForm.fields = [
 
	  {
			key: 'oldpassword',
			type: 'input',
			name: 'oldpassword',
			templateOptions: {
				type: 'password',
				label: 'Old Password',
				required: true
			}
		},
		{
			key: 'newpassword',
			type: 'input',
			name: 'newpassword',
			templateOptions: {
				type: 'password',
				label: 'New Password',
				required: true,
				minlength: 8
			}
		},
		{
			key: 'confirmnewpassword',
			type: 'input',
			name: 'confirmnewpassword',
			optionsTypes: ['matchField'],
        	model: resetForm.confirmationModel,
			templateOptions: {
				type: 'password',
				label: 'Confirm Password',
				required: true
			},
			data: {
			  fieldToMatch: 'newpassword',
			  modelToMatch: resetForm.model
			}
		},
	];
    
	
 
	
});


})(window.angular);

 