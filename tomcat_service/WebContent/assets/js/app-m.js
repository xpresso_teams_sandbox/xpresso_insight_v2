(function(angular) {
'use strict';
var rcxApp = angular.module('app', ['ngAnimate', 'ui.bootstrap']);

rcxApp.controller('addDetailsController', function ($scope, $http){

	//Add users 
	$scope.users = [];
	$scope.noUser = 0;
	
	$scope.addUser = function(){
		
		var user = {
			username: $scope.username,
			useremail: $scope.useremail
		};
	
		$scope.users.push(user); //Added new user
		
		$scope.noUser++;
		if ($scope.noUser >= 3){ //Disabled click if exceed limits 
			$('#addUserbtn').attr('disabled', '');
		}
	
	};
	
	$scope.deleteUser = function(index){ //Delete existing user
		$scope.users.splice(index, 1);
		$('#addUserbtn').removeAttr('disabled');
		$scope.noUser--;
	};
	
	//Add keywords
	$scope.keywords = [];
	$scope.noKeyword = 0;
	
	$scope.addKeywords = function () {
		var keyword = {
			keyword: $scope.keyword
		};
	
		$scope.keywords.push(keyword); //Added new keywords
		
		$scope.noKeyword++;
		if ($scope.noKeyword >= 3){ //Disabled click if exceed limits 
			$('#addKeywordbtn').attr('disabled', '');
		}
	};
	
	$scope.deleteKeyword = function(index){ //Delete existing keywords
		$scope.keywords.splice(index, 1);
		$('#addKeywordbtn').removeAttr('disabled');
		$scope.noKeyword--;
	};
	
	$scope.socialProfiles = [
		{icon:'fa-facebook', name: 'Facebook', value: '1'},
		{icon:'fa-twitter', name: 'Twitter', value: '2'},
		{icon:'fa-linkedin', name: 'Linkedin', value: '3'},
		{icon:'fa-youtube', name: 'Youtube', value: '4'}
	];
	$scope.newsocialProfiles = [];

	$scope.socialListing = false;
	
	$scope.addMSMedia = function () {
		$scope.socialListing = true;
		$scope.socialProfile = {
			name: $scope.selectedSocialProfile.name,
			value: $scope.selectedSocialProfile.value
		};
		$scope.newsocialProfiles.push($scope.socialProfile);
	};
	
	$scope.addSocProfile = function () {
		alert('Added all profiles');
	};
	
	$scope.mainstreamMedias = ['Economic Times', 'Hindustan Times', 'Financial Times', 'Business Standard'];
	
	$scope.selectMainstreamMedia = function () {
		alert('selected');
	};
	
	$scope.competitorNames = [];
	
	$scope.competitorProfile = [
		{name: 'Facebook', value: '1'},
		{name: 'Twitter', value: '2'},
		{name: 'Linkedin', value: '3'},
		{name: 'Youtube', value: '4'},
		{name: 'Economic Times', value: '5'},
		{name: 'Hindustan Times', value: '6'},
		{name: 'Financial Times', value: '7'},
		{name: 'Business Standard', value: '8'}
	];
	
	$scope.competitorsDetails = true;
	
	$scope.addCompetitorname = function () {
		var competitorn = {
			cname: $scope.cname
		};
	
		$scope.competitorNames.push(competitorn);
		
	};
	$scope.newCompetitorProfile = [];
	$scope.addComProfile = function () {
		$scope.cProfile = {
			name: $scope.selectedComProfile.name
		};
		$scope.newCompetitorProfile.push($scope.cProfile);
	};

	$scope.ncp = [];
	$scope.addCompetitors = function () {
		alert($scope.cprofilename);
		var cp = {
			cprofilename: $scope.cprofilename,
			cprofileUrl: $scope.cprofileUrl
		};
	
		$scope.ncp.push(cp); 
	};

});

rcxApp.controller('profileController', function ($scope) {

	$scope.topicProfiles = [
		{
			profileType : 'Social Profile',
			profileDetails : [
				{icon:'fa-facebook', name: 'Facebook', url:'http://www.facebook.com'},
				{icon:'fa-twitter', name: 'Twitter', url:'http://www.twitter.com'},
				{icon:'fa-linkedin', name: 'Linkedin', url:'http://www.linkedin.com'},
				{icon:'fa-youtube', name: 'Youtube', url:'http://www.youtube.com'}
			] 
		},
		{
			profileType : 'Mainstream Media',
			profileDetails : [
				{icon:'fa-newspaper-o', name: 'Economic Times', url:'Economic Times'},
				{icon:'fa-newspaper-o', name: 'Hindustan Times', url:'Hindustan Times'},
				{icon:'fa-newspaper-o', name: 'Financial Times', url:'Financial Times'},
				{icon:'fa-newspaper-o', name: 'Business Standard', url:'Business Standard'}
			] 
		},
		{
			profileType : 'Other Relevant Source',
			profileDetails : [
				{icon:'fa-database', name: 'Source 1', url:'Source 1'},
				{icon:'fa-database', name: 'Source 2', url:'Source 2'},
				{icon:'fa-database', name: 'Source 3', url:'Source 3'},
				{icon:'fa-database', name: 'Source 4', url:'Source 4'}
			] 
		},
	];
	
	$scope.submit = function () {
		$('.modal').modal('hide');
		alert('Submited');
	};
	
	$scope.users = [
		{username: 'Walton Simek', useremail: 'walton.simek@domainname.com'},
		{username: 'Oliva Mayne', useremail: 'oliva.mayne@domainname.com'},
		{username: 'Joseph Ceaser', useremail: 'joseph.ceaser@domainname.com'},
		{username: 'Ken Giltner', useremail: 'ken.giltner@domainname.com'}
	]
	
	$scope.keywords = ['Keyword 1', 'Keyword 2', 'Keyword 3', 'Keyword 4'];
	
	$scope.competitors = [
		{
			competitorName: 'ICICI',
			socialMediaDetails: [
				{icon:'fa-facebook', smediaName: 'Facebook', smediaUrl:'http://www.facebook.com/icici'},
				{icon:'fa-twitter', smediaName: 'Twitter', smediaUrl:'http://www.twitter.com/icici'},
				{icon:'fa-linkedin', smediaName: 'Linkedin', smediaUrl:'http://www.linkedin.com/icici'},
				{icon:'fa-youtube', smediaName: 'Youtube', smediaUrl:'http://www.youtube.com/icici'}
			]
		},
		{
			competitorName: 'AXIS',
			socialMediaDetails: [
				{icon:'fa-facebook', smediaName: 'Facebook', smediaUrl:'http://www.facebook.com/axis'},
				{icon:'fa-twitter', smediaName: 'Twitter', smediaUrl:'http://www.twitter.com/axis'},
				{icon:'fa-linkedin', smediaName: 'Linkedin', smediaUrl:'http://www.linkedin.com/axis'},
				{icon:'fa-youtube', smediaName: 'Youtube', smediaUrl:'http://www.youtube.com/axis'}
			]
		},
		{
			competitorName: 'SBI',
			socialMediaDetails: [
				{icon:'fa-facebook', smediaName: 'Facebook', smediaUrl:'http://www.facebook.com/sbi'},
				{icon:'fa-twitter', smediaName: 'Twitter', smediaUrl:'http://www.twitter.com/sbi'},
				{icon:'fa-linkedin', smediaName: 'Linkedin', smediaUrl:'http://www.linkedin.com/sbi'},
				{icon:'fa-youtube', smediaName: 'Youtube', smediaUrl:'http://www.youtube.com/sbi'}
			]
		},
		{
			competitorName: 'Standard Chartered',
			socialMediaDetails: [
				{icon:'fa-facebook', smediaName: 'Facebook', smediaUrl:'http://www.facebook.com/StandardChartered'},
				{icon:'fa-twitter', smediaName: 'Twitter', smediaUrl:'http://www.twitter.com/StandardChartered'},
				{icon:'fa-linkedin', smediaName: 'Linkedin', smediaUrl:'http://www.linkedin.com/StandardChartered'},
				{icon:'fa-youtube', smediaName: 'Youtube', smediaUrl:'http://www.youtube.com/StandardChartered'}
			]
		}
	];
});




})(window.angular);
