/* this file contains all global constants used in all js files */


// User data returned on successful authentication for cross-page use
var authData = $('#authData').val().trim();
var authId = authData.split('$')[0];  // !!!!!! IMPORTANT - This is actually account id. !!!!!!!
var authLogo = authData.split('$')[1];
var authEmail = authData.split('$')[2];
var companyId = authData.split('$')[0];
var authIsReady = authData.split('$')[4];
var authIsConfiguredName = authData.split('$')[5];
var authName = authData.split('$')[6];
var authRole = authData.split('$')[7];
var authPrimaryCompanyID = authData.split('$')[3];
var defaultCompetitor = authData.split('$')[8];
var customerFeedbackURL = authData.split('$')[9];
var authTimezoneOffset = 0;

var DAILY_TF = '1';
var SORTBY_TF='1';
var EVERY3DAYA_TF = '3';
var EVERY7DAYS_TF = '7';
var EVERY30DAYA_TF = '30';
var DEFAULT_SOURCE_FB = '1';
var DEFAULT_SOURCE_TW = '2';

//setting the company id and company logo
$('#txtCompId').val(authId);
$('#company_logo').attr('src', img+authLogo);
$('#company_logo_hidden').attr('src', img+authLogo);


// ticket details data made global to populate values of manage modal
var arrValsTicket;

//object for table in deep listening section in analytics/Explore page
var twitterFacebookEngagementSentimentObj = {
	accountId: null,
	selectedDuration: 1,
	selectedSource: 1
};
/* parameter objects for functions with multiple parameters */
var viralityComparissionObj = {
	accountId: null,
	acctId: null,
	startDt: null,
	endDt: null,
	hR: -1,
	selectedInterval: null
};

var analyticsDetailsObj = {
	accountId: null,
	sourceId: -1,
	sourceTopic: null,
	startDt: null,
	endDt: null,
	timeInterval: -1, 
	detailsTabName: null
}

var filteredAnalyticsDetailsObj = {
	accountId: null,
	rowSourceId: -1,
	rowSourceTopic: null, 
	startDt: null, 
	endDt: null, 
	detailsTabName: null,
	virality: null,
	sentiment: null
};

var filteredSocialListeningObj = {
	accountId: null,
	rowSourceId: null,
	rowSourceTopic: null, 
	startDt: null, 
	endDt: null, 
	detailsTabName: null,
	virality: null,
	sentiment: null
};

var socialListeningObj = {
	accountId: null,
	sourceId: -1,
	sourceTopic: null,
	startDt: null,
	endDt: null,
	timeInterval: -1, 
	sentiment:null,
	detailsTabName: null
}

var entityDetailsObj = {
	accountId: null,
	rowSourceId: -1,
	rowSourceTopic: null, 
	startDt: null, 
	endDt: null,
	hr: -1,
	selectedInterval: null
}

var topAdvocatesObj = {
		authorName: null
	}

var filteredEntityDetailsObj = {
	accountId: null,
	rowSourceId: -1,
	rowSourceTopic: null, 
	startDt: null, 
	endDt: null,
	hr: -1,
	virality:null,
	sentiment:null
}

//object for Review Analysis Tab
var reviewAnalysisObj = {
	accountId: null,
	sourceId: -1,
	cityId:0,
	locality:ALL,
	sentiment:ALL,
	startDt: null,
	endDt: null
}


/* parameter objects for functions with multiple parameters end */

//js constants variables
//var user_welcome_message = 'Welcome '.concat(authName).concat(' | ').concat('Admin');

var user_welcome_message = 'Welcome';

var high_severity = 'HIGH';
var medium_severity = 'MEDIUM';
var low_severity = 'LOW';
var daily = 'Daily';
var monthly = 'Monthly';
var filter = 'Filter';

var context_details = 'context_details';
var social_listening = 'social_listening';
var WORD_CLOUD = 'word_cloud';
var SOCIAL_MEDIA = 'Social Media';
var SOCIAL_LISTENING = 'social_listening';
var REVIEW_SITES = 'Review Sites';
var ENTERPRISE = 'Enterprise';

var POSITIVE = 'Positive';
var NEGATIVE = 'Negative';
var NEUTRAL = 'Neutral';
var GREEN = '#8DC153';
var RED = '#F55348';
var YELLOW = '#F6BC41';
var GRAY = '#888';

var BLANK_STRING = '';
var NULL_VALUE = -1;
var ALL = 'All';
var ALL_CONTEXTS = 'All Contexts';
var ALL_SENTIMENTS = 'ALL SENTIMENTS';
var ALL_SENTIMENTS_100 = '100';
var ALL_SOURCES = 'All Sources';
var ALL_CITIES = 'All Cities';
var ALL_SOURCES = 'All Sources';
var ALL_REGIONS = 'All Regions';
var ALL_ENTITIES = 'All Entities';
var ALL_LOCALITIES = 'All Localities';
var OTHERS = 'Others';

var EMPTY_ARRAY = [];
var OTHERS = 'Others';

var DAILY_TF = '1';
var SORTBY_TF='1';
var EVERY3DAYA_TF = '3';
var EVERY7DAYS_TF = '7';
var EVERY30DAYA_TF = '30';
var DEFAULT_SOURCE_FB = '1';
var DEFAULT_SOURCE_TW = '2';

var DAILY = '0';
var WEEKLY = '1';
var MONTHLY = '2';
var FILTER = '99';

