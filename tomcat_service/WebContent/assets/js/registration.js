(function(angular) {
'use strict';
var rcxApp = angular.module('app', ['formly', 'formlyBootstrap', 'ngAnimate', 'ngMessages']);

var contextPath = $('#contextPathReg').val().trim();
var rootUrl = contextPath+'/services';

rcxApp.constant('formlyExampleApiCheck', apiCheck());

rcxApp.config(function config(formlyConfigProvider, formlyExampleApiCheck) {
    // set templates here
    formlyConfigProvider.setType({
      name: 'matchField',
      apiCheck: function() {
        return {
          data: {
            fieldToMatch: formlyExampleApiCheck.string
          }
        };
      },
      apiCheckOptions: {
        prefix: 'matchField type'
      },
      defaultOptions: function matchFieldDefaultOptions(options) {
        return {
          extras: {
            validateOnModelChange: true
          },
          expressionProperties: {
            'templateOptions.disabled': function(viewValue, modelValue, scope) {
              var matchField = find(scope.fields, 'key', options.data.fieldToMatch);
              if (!matchField) {
                throw new Error('Could not find a field for the key ' + options.data.fieldToMatch);
              }
              var model = options.data.modelToMatch || scope.model;
              var originalValue = model[options.data.fieldToMatch];
              var invalidOriginal = matchField.formControl && matchField.formControl.$invalid;
              return !originalValue || invalidOriginal;
            }
          },
          validators: {
            fieldMatch: {
              expression: function(viewValue, modelValue, fieldScope) {
                var value = modelValue || viewValue;
                var model = options.data.modelToMatch || fieldScope.model;
                return value === model[options.data.fieldToMatch];
              },
              message: options.data.matchFieldMessage || '"Password does not match"'
            }
          }
        };
        
        function find(array, prop, value) {
          var foundItem;
          array.some(function(item) {
            if (item[prop] === value) {
              foundItem = item;
            }
            return !!foundItem;
          });
          return foundItem;
        }
      }
    });
  });

rcxApp.run(function(formlyConfig, formlyValidationMessages) {
    formlyConfig.extras.ngModelAttrsManipulatorPreferBound = true;
    formlyValidationMessages.addStringMessage('maxlength', 'Your input is WAAAAY too long!');
    formlyValidationMessages.messages.pattern = function(viewValue, modelValue, scope) {
      return viewValue + ' is invalid';
    };
    formlyValidationMessages.addTemplateOptionValueMessage();
  });

rcxApp.config(function (formlyConfigProvider) {
	// set templates here
	formlyConfigProvider.setType({
	name: 'input',
	templateUrl: 'custom.html'
	});
	
	formlyConfigProvider.setWrapper({
	  name: 'validation',
	  types: ['input'],
	  templateUrl: 'error-messages.html'
	});

});

function jsonService($http){
    var jsonSVC = {
        getJSON: function ($url){
            var promise = $http.get($url).then(function(response){
                console.log(response);
			var	return_data = response.data.results.rows;
			var obj = [];
			for(var arr in return_data){
				obj.push({
					"value": return_data[arr][0],
					"name": return_data[arr][1]
					})
			}
			    return obj;
            });
            return promise;
        }
    };
    return jsonSVC;
}
rcxApp.factory('jsonService', jsonService);
rcxApp.controller('accountDetailsController', function (jsonService) {
	
	var regForm = this;
	
	regForm.model = {};
	regForm.options = {};
	
	
	regForm.fields = [
		{
        	template: '<h4>Personal Details</h4>'
      	},
		{
			key: 'userName',
			type: 'input',
			name: 'userName',
			templateOptions: {
				label: 'Name',
				required: true
			}
		},
		{
			key: 'userId',
			type: 'input',
			name: 'userId',
			templateOptions: {
				type: 'email',
				label: 'Email',
				onChange: 'regForm.emailcheck()',
				required: true
			}
		},
		{
			key: 'password',
			type: 'input',
			name: 'userPassword',
			templateOptions: {
				type: 'password',
				label: 'Password',
				required: true,
				minlength: 8
			}
		},
		{
			key: 'confirmPassword',
			type: 'input',
			name: 'userPasswordConfirm',
			optionsTypes: ['matchField'],
        	model: regForm.confirmationModel,
			templateOptions: {
				type: 'password',
				label: 'Confirm Password',
				required: true
			},
			data: {
			  fieldToMatch: 'password',
			  modelToMatch: regForm.model
			}
		},
		{
        	template: '<h4>Other Details</h4>'
      	},
		{
			key: 'userCompany',
			type: 'input',
			name: 'userCompany',
			templateOptions: {
				label: 'Company Name',
				required: true
			}
		},
		{
			key: 'accountName',
			type: 'input',
			name: 'accountName',
			templateOptions: {
				label: 'Brand Name',
				required: true
			}
		},
		{
			key: 'userDesignation',
			type: 'input',
			name: 'userDesignation',
			templateOptions: {
				label: 'Designation',
				options: []
			}
		},
		{
			className: 'row',
			fieldGroup: [
				{
					className: 'col-sm-offset-2 col-sm-8',
					key: 'domain',
					type: 'select',
					name: 'selectdomain',
					id: 'selectdomain',
					templateOptions: {
						label: 'Domain',
						"ngOptions": "option.name for option in to.options track by option.value"
					},
					controller: function($scope, jsonService) {
						jsonService.getJSON(rootUrl+"/user/getStaticDetails?type=Domain").then(function(data){
							
							$scope.model[$scope.options.key] = data[0];
							$scope.options.templateOptions.options = data;
							$scope.model[$scope.options.key] = $scope.options.templateOptions.options[0];
							return data;
						});
						
					}
				}
			]
      },
	  {
			className: 'row',
			fieldGroup: [
				{
					className: 'col-sm-offset-2 col-sm-8',
					key: 'country',
					type: 'select',
					name: 'SelectCountry',
					id: 'SelectCountry',
					templateOptions: {
						label: 'Country',
						"ngOptions": "option.name for option in to.options track by option.value"
					},
					controller: function($scope, jsonService) {
						jsonService.getJSON(rootUrl+"/user/getStaticDetails?type=Country").then(function(data){
							
							$scope.model[$scope.options.key] = data[0];
							$scope.options.templateOptions.options = data;
							$scope.model[$scope.options.key] = $scope.options.templateOptions.options[0];
							return data;
						});
						
					}
				}
			]
      },
	  {
			className: 'row',
			fieldGroup: [
				{
					className: 'col-sm-offset-2 col-sm-8',
					key: 'timezone',
					type: 'select',
					name: 'timezone',
					id: 'timezone',
					templateOptions: {
						label: 'Timezone',
						"ngOptions": "option.name for option in to.options track by option.value"
					},
					controller: function($scope, jsonService) {
						jsonService.getJSON(rootUrl+"/user/getStaticDetails?type=Timezone").then(function(data){
							
							$scope.model[$scope.options.key] = data[0];
							$scope.options.templateOptions.options = data;
							$scope.model[$scope.options.key] = $scope.options.templateOptions.options[0];
							return data;
						});
						
					}
				}
			]
      }
	  
	];
    
	
	function onSubmit() {
	  	if (rcxApp.form.$valid) {
			rcxApp.options.updateInitialValue();
			alert(JSON.stringify(rcxApp.model), null, 2);
			
			var url = rootUrl+"/user/inserUserSignUpDetails";
		
			http({
				method: 'POST',
				url: url,
				headers: {
					'Content-Type': 'application/json'
				}
				
			})
		  }
		
    }
	
	function emailcheck(){
		alert('test');
	}
	
});


})(window.angular);

var contextPath = $('#contextPathReg').val().trim();
var rootUrl = contextPath+'/services';

