 // this file contains controller for analyze functionalities
(function(angular) {
    /*
     * Author Name: Prasanta Gorai
     * Create Date: 10/10/2016
     * Last Modified: 02/11/2016
     * Input: various dependency injection like "$http, $location, $interval, $window, $timeout"
     * Output: As this is Angular controller, it returns a scope of 'AnalyzeController' to control data flow from model to view
     * Method Insight: 'AnalyzeController' is the angular controller for 'Analyze' page. All ajax requests in 'Analyze' are handled from this controller 
     *
     */
    angular.module('app').controller('AnalyzeController', function($scope, $http, $location, $interval, $window, $timeout) {
    	console.log('AnalyzeController');
        $scope.$parent.activeMenu = 'analyze';
        $scope.$parent.selectedSource = '';
        $scope.$parent.selectedTopic = '';
        $scope.follower_growth_rate_interval = "1";//'Daily';
        $scope.fan_growth_rate_interval = "1";//'Daily';
        $scope.per_tweet_interaction_interval = 1;//'Daily';
        $scope.per_fb_interaction_interval = 1;//'Daily';
        $scope.most_engaging_tweets;
        $scope.most_engaging_fb_posts;
        $scope.socialKPITweeter;
        $scope.socialKPIFb;
        $scope.accordian_expand_tw = 'angle-up';
        $scope.accordian_expand_fb = 'angle-down';
        $scope.page_load = true;
        
        $scope.$parent.fn_snapshot_filter_new(0); // calick on daily snapshot on page load

        var followerGrothChartContainer = '#twitter_analyze-chart1';
        var fanGrothChartContainer = '#fb_analyze-chart1';
       // var twPerIntByType = "#twitter_analyze-chart4_1";
        var twPerIntByPlace = "#twitter_per_interaction";
        var postPerIntByPlace = "#post_per_interaction";
        var headData = $.param({
            "accountId": authId
        });

        var mostEngagingData = $.param({
            "accountId": authId 
        });
        var mostEngagingDataClockTwitter = $.param({
            "accountId": authId,
            "selectedDays": 14,
            "type":"tweet"
        });
        var mostEngagingDataClockFacebook = $.param({
            "accountId": authId,
            "selectedDays": 14,
            "type":"post"
        });
       /* var mostEngagingDataCalenderTwitter = $.param({
            "accountId": authId,
            "selectedInterval": "Monthly",
            "selectedDays":10,
            "type":"tweet"
        });
        var mostEngagingDataCalenderFacebook = $.param({
            "accountId": authId,
            "selectedInterval": "Monthly",
            "selectedDays":10,
            "type":"post"
        });*/
        
        setTimeout(function(){
			var SlideW = $('#dailySnapshot').width()-30;
			  slider = $('.dashboard-carousel').bxSlider({
			 /* slideWidth: 395,*/
				slideWidth: 593,
			    minSlides: 2,
			    maxSlides: 4,
			    slideMargin: 30,
			    //moveSlides: 1,
			    infiniteLoop: false,
			  });
		},100);
        
        
        

        if (authData.split('$')[5] == 0) {
            $location.path("/addinfo");
        } else if (authData.split('$')[4] == 0)
            $location.path("/gettingdata");

        $scope.check = function() {
            if (Event.prototype.initEvent) {
                $timeout(function() {
                    var evt = $window.document.createEvent('UIEvents');
                    evt.initUIEvent('resize', true, false, $window, 0);
                    $window.dispatchEvent(evt);
                });
            } else {
                $interval(function() {
                    window.dispatchEvent(new Event('resize'));
                }, 10)
            }
        }
        
        /*
         * Author Name: Prasanta Gorai
         * Create Date: 17/10/2016
         * Last Modified: 19/11/2016
         * Input: service url, interval (Daily/Weekly), container (where to render the chart)
         * Output: returns result set to 'chart_growthrate' function in 'generatechart.js' file 
         * Method Insight: This function is called to render chart to show growth rate for Facebook and Twitter posts.
         *       This sends an ajax call with passed parameters and render the chart to passed 'container'
         *
         */
        $scope.fn_follower_growth_rate = function(url,type, selectedInterval, container) {
            var data = $.param({
                "accountId": authId,
                "selectedInterval": selectedInterval,
                "type":type
            });

            var chartData = {};
            var categories = [];
            var series = [];
            var respData;
            chartData['selectedInterval'] = selectedInterval;

            $http.post(url, data, $scope.config).then(function(response) {

                if (type=="tweet") {
                    respData = response.data.kpiFollowerFanGrowthRateBeanList;
                    chartData['title'] = 'Follower growth rate.';
                }

                if (type=="post") {
                    respData = response.data.kpiFollowerFanGrowthRateBeanList;
                    chartData['title'] = 'Fan growth rate.';
                }

                for (item in respData) {
                    categories.push(respData[item].kpi_date);
                    series.push(Math.roundDec(respData[item].growth_rate, -4));
                }

                chartData['categories'] = categories.reverse();
                chartData['series'] = series.reverse();

                for (key in chartData.categories) {
                    if (selectedInterval === 'Daily') {
                        chartData.categories[key] = chartData.categories[key] + ' ( Day' + (parseInt(key) + 1) + ' ) ';
                    } else if (selectedInterval === 'Weekly') {
                        chartData.categories[key] = chartData.categories[key] + ' ( Week' + (parseInt(key) + 1) + ' ) ';
                    }
                }
                chart_growthrate(chartData, container);
            });
        }

      //To manage blank list in the select list of selectFollowerGrowth
		//$('#selectFollowerGrowth').selectpicker('refresh');
        /*-----------------------End fn_follower_growth_rate--------------------------*/

        /*
         * Author Name: Prasanta Gorai
         * Create Date: 17/10/2016
         * Last Modified: 27/11/2016
         * Input: '$view' the name of the view to be rendered 
         *  '$dates' a (possibly empty) array of DateObject's (see source) that the user can select in the view. 
         *  '$leftDate' the DateObject selected if the user clicks the left arrow. 
         *  '$upDate' the DateObject selected if the user clicks the text between the arrows. 
         *  '$rightDate' the DateObject selected if the user clicks the right arrow,
         *  '$url' the service url, 
         *  '$container' this is the id of the container in jsp
         *  
         * Output: N/A 
         * Method Insight: This function is to render calender for 'Most Engaging tweets/posts'
         */
        /*---------------Function to get most engaging data for twweter and facebook to calendar chart----------------*/
       /* $scope.fn_render_calender = function($view, $dates, $leftDate, $upDate, $rightDate, $url,type, $container) {
            $scope.kpiMostEngagingTweetsPosts = false;
            $scope.dateRangeEnd = new Date();
            $scope.dateRangeStart = new Date();
            $scope.dateRangeStart.setMonth($scope.dateRangeStart.getMonth() - 1);

            $leftDate.selectable = false;
            $rightDate.selectable = false;
            $upDate.selectable = false;

            var $indx = 0;
            var currentDate = new Date();
            
            if(type=="tweet"){
            	mostEngagingDataCalender=mostEngagingDataCalenderTwitter;
            }
            else if(type=="post"){
            	mostEngagingDataCalender=mostEngagingDataCalenderFacebook;
            }

            $http.post($url, mostEngagingDataCalender, $scope.config).success(function(resp) {
                var results = resp.kpiMostEngagingTweetsPostsList;
                $scope.kpiMostEngagingTweetsPosts = results;
                console.log( !$scope.kpiMostEngagingTweetsPosts);
                results && results.sort(function(a, b) {
                    return parseFloat(b.count) - parseFloat(a.count);
                });
                results[0]['iclass'] = 'max0';

                for (var key in results) {
                    if (key > 0 && (results[key].count !== 0)) {
                        if (results[key].count !== results[key - 1].count) {
                            $indx += 1;
                            results[key]['iclass'] = 'max' + $indx;
                        } else {
                            results[key]['iclass'] = 'max' + $indx;
                        }
                    }
                    if (results[key].count === 0) {
                        results[key]['iclass'] = 'max9';
                    }
                    results[key]['day'] = parseInt(results[key].date.substr(0, (results[key].date.indexOf('-'))));
                }
                for (var i = 0; i < $dates.length; i++) {
                    $dates[i].selectable = false;
                    if ($dates[i].past || $dates[i].future) {
                        $dates[i].hidden = true;
                    } else {
                        $dates[i]["result"] = dateExistsInResp(results, parseInt($dates[i].display));
                    }
                    if ($dates[i].localDateValue() >= currentDate.valueOf()) {
                        $dates[i].future_date = true;
                    }
                }
            }).then(function(response) {
                setTimeout(function() {
                    $($container + ' [rel="tooltip"]').each(function() {
                        $(this).tooltip({
                            html: true,
                            container: 'body'
                        });
                    });
                }, 100);
            });
        }*/
        /*-------End of fn_render_calender-------*/
        
        /*
         * Author Name: Prasanta Gorai
         * Create Date: 22/11/2016
         * Last Modified: 22/11/2016
         * Input: 'url'the Service url,
         * 		'type' is type of the service for 'POST or TWEET',  
         * 		'container' is id of the div/section wher chart will render 
         * Output: N/A
         * Method Insight: 'fn_most_engaging_data_avg' sends POST request to 'url' with parameters 'accountId,selectedDays,selectedInterval,type' and after getting the results set, it renders area chart to 'container'
         *
         */
        /*-------Getting per interaction data for both Tweeter and Facebook and passing to chart function------*/
        $scope.fn_most_engaging_data_avg = function(url, type, container) {
        	 var data = $.param({
                 "accountId": authId,
                 "type": type
             });
        	 var shortWeek = {
        				"1": "Sun",
        				"2": "Mon",
        				"3": "Tue",
        				"4": "Wed",
        				"5": "Thu",
        				"6": "Fri",
        				"7": "Sat" 
        			};
        	 var chartData = {};
             var categories = [];
             var series = [];
             var respData;
        	 
        	$http.post(url, data, $scope.config).then(function(response) {
                 
                 respData = response.data.kpiTopEngagingMonthlyBeanList;
        		 
        		/* respData = [{
        			    "date": "Monday",
        			    "count": 45
        			}, {
        			    "date": "Tuesday",
        			    "count": 37
        			}, {
        			    "date": "Wednesday",
        			    "count": 50
        			}, {
        			    "date": "Thursday",
        			    "count": 70
        			}, {
        			    "date": "Friday",
        			    "count": 20
        			}, {
        			    "date": "Saterday",
        			    "count": 23
        			}, {
        			    "date": "Sunday",
        			    "count": 5
        			}];*/
              
                 if (type=="post") {
                     chartData['title'] = "Most engaging posts for last month.";
                 }
                 if (type=="tweet") {
                     chartData['title'] = "Most engaging tweets for last month.";
                 }
                 console.log('respdata: ',respData);
                 for (item in respData) {
                     categories.push(shortWeek[respData[item].weekCount]);
                     series.push(respData[item].avgCount);
                 }
                 //chartData['categories'] = categories.reverse();
                // chartData['series'] = series.reverse();
                
                 chartData['series'] = series;
                 chartData['categories'] = categories;
                 
                 
                 area_chart(chartData, container);
            });
        };
        
        /*
         * Author Name: Prasanta Gorai
         * Create Date: 18/10/2016
         * Last Modified: 28/10/2016
         * Input: 'url'the Service url,
         * 		'type' is type of the service 'By Content / By Place', 
         * 		'selectedInterval' is selected interval from user interface('Daily/Weekly'), 
         * 		'container' is id of the div/section wher chart will render 
         * Output: N/A
         * Method Insight: 'fn_per_interaction' sends POST request to 'url' with parameters 'accountId,selectedInterval,type' and after getting it renders stacked bar chart to 'container'
         *
         */
        /*-------Getting per interaction data for both Tweeter and Facebook and passing to chart function------*/
        $scope.fn_per_interaction = function(url, type, container) {
                var data = $.param({
                    "accountId": authId,
                    "type": type
                });

                var chartData = {};
                var categories = [];
                var series = [];
                var respData;

               // chartData['selectedInterval'] = selectedInterval;
                /*===Sending POST request and after getting data modifying as per requirement ===*/
                $http.post(url, data, $scope.config).then(function(response) {
                   
                    respData = response.data.kpiPerTweetPostInteractionBeanList;
                 
                    if (type=="post") {
                        chartData['title'] = "Per Post Interaction.";
                    }
                    if (type=="tweet") {
                        chartData['title'] = "Per Tweet Interaction.";
                    }
                    
                    for (item in respData) {
                        categories.push(respData[item].count_date);
                        series.push(Math.roundDec(respData[item].average_count, -4));
                    }
                    chartData['categories'] = categories.reverse();
                    chartData['series'] = series.reverse();
                   
                    chartData['series'] = series;

                    for (key in chartData.categories) {
                       
                            chartData.categories[key] = chartData.categories[key] + ' ( Day' + (parseInt(key) + 1) + ' ) ';
                       
                    }
                   
                    chart_perInteraction(chartData, container);
                });
            }
            /*--------------------------------------------*/

        /*--------Method to change the time interval between 'Daily, Weekly' for Tweeter follower growth rate-------------*/
        $scope.fn_follower_growth_rate_interval_change = function() {
        	//alert(0);
            $scope.fn_follower_growth_rate(followerFanGrowthRateChartUrl,"tweet", $scope.follower_growth_rate_interval, followerGrothChartContainer);
        };

        /*--------Method to change the time interval between 'Daily, Weekly' for Facebook fan growth rate-------------*/
        $scope.fn_fan_growth_rate_interval_change = function() {
            $scope.fn_follower_growth_rate(followerFanGrowthRateChartUrl,"post", $scope.fan_growth_rate_interval, fanGrothChartContainer);
        }

        
        /*
         * Author Name: Prasanta Gorai
         * Create Date: 27/10/2016
         * Last Modified: 28/10/2016
         * Input: N/A
         * Output: N/A
         * Method Insight: 'fn_analyze_fb' is to contain all function calls related to facebook and will get called once user clicks on 'Ribbon' for Facebook 
         *
         */
        $scope.fn_analyze_fb = function() {
            /*-----------fn_follower_growth_rate calling for facebook---------------*/
            $scope.fn_follower_growth_rate(followerFanGrowthRateChartUrl,"post", $scope.fan_growth_rate_interval, fanGrothChartContainer);

            /*-----------Service call to get "Top 5 Most engaging Posts" data------------*/
            $http.post(topEngagingPostsUrl, mostEngagingData, $scope.config).success(function(resp) {
                $scope.most_engaging_fb_posts = resp.kpiTopEngagingPostsBeanList;
            }).then(function(response) {
                $("#facebook_listing").slimScroll({
                    destroy: true
                });

                $('#facebook_listing').slimScroll({
                    height: '239px',
                    color: '#000',
                    size: '4px',
                    alwaysVisible: true,
                    wheelStep: 10,
                    distance: '2px',
                });
            }); /*---------End of Service call----------*/


            //To manage blank list in the select list of SENTIMENT INDEX LIVE 
    		//$('#selectFanGrowth').selectpicker('refresh');
    		
            /*===============Service call to get "MOST ENGAGING POSTS" data==============*/
            /*---------Services to get 'Clock Chart' data----------*/
            //$scope.no_data_clock_fb = false;
           
            $http.post(topEngagingTweetPostDistributionDailyUrl, mostEngagingDataClockFacebook, $scope.config).success(function(resp) {
            	var results = resp.kpiTopEngagingDailyBeanList;
               
                $scope.no_data_clock_fb = results;
                
                if( results)
            	{ 
                	for (obj in results) {
                		results[obj]["to_hour"] = parseInt(results[obj].updateHour) - 3;                		 
                    }
                	
                    results.sort(function(a, b) {
                        return parseFloat(b.avgCount) - parseFloat(a.avgCount);
                    });
                    
                    chart_clock(results, {
                        id: "#fb_analyze-chart5"
                    });

            	}
                
                
            }); /*---------End of Service call----------*/

            /*---------Services to get 'Calendar' data----------*/
            
            $scope.fn_most_engaging_data_avg(topEngagingTweetPostDistributionMonthlyUrl, "post", "#fb_analyze-calender");
            /*$scope.selectableFalseFb = function($view, $dates, $leftDate, $upDate, $rightDate) {
                var container = '#fb_analyze-calender';
                $scope.fn_render_calender($view, $dates, $leftDate, $upDate, $rightDate, topEngagingTweetPostDistributionUrl,"post", container); // calling 'fn_render_calsender' and passing url for facebook
            } */
            /*---------End of Service call----------*/

            /*=============End of "MOST ENGAGING TWEETS" service calls=================*/

           //$scope.fn_per_interaction('assets/JSON/perInteraction.json', 'content', $scope.per_fb_interaction_interval, postPerIntByType);
            $scope.fn_per_interaction(perTweetPostInteractionUrl, 'post', postPerIntByPlace);
        }
        
        /*
         * Author Name: Prasanta Gorai
         * Create Date: 27/10/2016
         * Last Modified: 28/10/2016
         * Input: N/A
         * Output: N/A
         * Method Insight: 'fn_analyze_tw' is to contain all function calls those will get called on pageload 
         *
         */
        $scope.fn_analyze_tw = function() {
            /*-----------fn_follower_growth_rate calling for Tweeter---------------*/
            $scope.fn_follower_growth_rate(followerFanGrowthRateChartUrl,"tweet", $scope.follower_growth_rate_interval, followerGrothChartContainer);

            /*-----------Service call to get "Top 5 Most engaging Tweets" data------------*/
            $http.post(topEngagingTweetsUrl, mostEngagingData, $scope.config).success(function(resp) {
                $scope.most_engaging_tweets = resp.kpiTopEngagingTweetsBeanList;
            }).then(function(response) {
                $("#twitter_listing").slimScroll({
                    destroy: true
                });

                $('#twitter_listing').slimScroll({
                    height: '239px',
                    color: '#000',
                    size: '4px',
                    alwaysVisible: true,
                    wheelStep: 10,
                    distance: '2px',
                });
            }); /*---------End of Service call----------*/


            /*===============Service call to get "MOST ENGAGING TWEETS" data==============*/
            /*---------Services to get 'Clock Chart' data----------*/
            $http.post(topEngagingTweetPostDistributionDailyUrl, mostEngagingDataClockTwitter, $scope.config).success(function(resp) {
                var results = resp.kpiTopEngagingDailyBeanList;
                $scope.no_data_clock_tw = results;
                
                if( results)
            	{ 
                	for (obj in results) {
                		results[obj]["to_hour"] = parseInt(results[obj].updateHour) - 3;
                        
                    }
                	
                    results.sort(function(a, b) {
                        return parseFloat(b.avgCount) - parseFloat(a.avgCount);
                    });
                    
                    chart_clock(results, {
                        id: "#twitter_analyze-chart5"
                    });

            	}
               
            }); /*---------End of Service call----------*/

            /*---------Services to get 'Calendar' data----------*/
            $scope.fn_most_engaging_data_avg(topEngagingTweetPostDistributionMonthlyUrl, "tweet", "#twitter_analyze-calender");
            /*$scope.selectableFalseTwitter = function($view, $dates, $leftDate, $upDate, $rightDate) {
                var container = '#twitter_analyze-calender';
                $scope.fn_render_calender($view, $dates, $leftDate, $upDate, $rightDate, topEngagingTweetPostDistributionUrl,"tweet", container); 
				// calling 'fn_render_calsender' and passing url for tweeter		
            } */
            /*---------End of Service call----------*/

            /*=============End of "MOST ENGAGING TWEETS" service calls=================*/

            //$scope.fn_per_interaction('assets/JSON/perInteraction.json', 'content', $scope.per_tweet_interaction_interval, twPerIntByType);
            $scope.fn_per_interaction(perTweetPostInteractionUrl, 'tweet', twPerIntByPlace);
        }

        /*=============On page load calling==============*/
        /*-------------POST request to get Tweeter KPI Ribbon data----------------*/
        $http.post(socialKPITweeterHeadUrl, headData, $scope.config).then(function(response) {
            $scope.socialKPITweeter = response.data.kpiTwitterBean;

            /*------------Making all accordion tab expandable ----------*/
            $('#accordion .panel-heading').on('click', function() {
                console.log("sdfsda:", $(this).next());
                $(this).next().collapse('toggle');
            });
        }); /*--------------End of Tweeter KPI call-----------------*/

        /*-------------POST request to get Facebook KPI Ribbon data----------------*/
        $http.post(socialKPIFbHeadUrl, headData, $scope.config).then(function(response) {
            $scope.socialKPIFb = response.data.kpiFacebookBean;
            console.log('$scope.socialKPIFb', $scope.socialKPIFb);
            $scope.socialKPIFb.rate_of_fan_growth = ($scope.socialKPIFb.rate_of_fan_growth).toFixed(2); 
        }); /*--------------End of Facebook KPI call-----------------*/

        $scope.fn_analyze_tw();

        /*
         * Author Name: Prasanta Gorai
         * Create Date: 27/10/2016
         * Last Modified: 27/10/2016
         * Input: N/A
         * Output: N/A
         * Method Insight: 'customTrigger' will be tiggred once user clicks on 'Ribbon' for Facebook.
         *   When user clicks first time on 'Facebook Ribbon' 'renderCalendar' will set to 'true' and call function 'fn_analyze_fb'.
         *   After 'fn_analyze_fb' function executes 'page_load' will set to 'false', and 'fn_analyze_fb' will not execute again until user move to some other tab or reload the page
         *
         */
        $scope.customTrigger = function() {
            if ($scope.page_load) {
                $scope.renderCalendar = true; // setting flag to 
                $scope.fn_analyze_fb();

                $scope.page_load = false;
            }
        }
        
        /*
         * Author Name: Urmi Saha
         * Create Date: 01/10/2016
         * Last Modified: 01/10/2016
         * Input: N/A
         * Output: N/A
         * Method Insight: window resize for reforming the charts inside accordion on expanse of accordion tabs
         *
         */
        $scope.check = function() {
            if (Event.prototype.initEvent) {
                $timeout(function() {
                    var evt = $window.document.createEvent('UIEvents');
                    evt.initUIEvent('resize', true, false, $window, 0);
                    $window.dispatchEvent(evt);
                });
            } else {
                $interval(function() {
                    window.dispatchEvent(new Event('resize'));
                }, 10);
            }
        }
    });

})(window.angular);