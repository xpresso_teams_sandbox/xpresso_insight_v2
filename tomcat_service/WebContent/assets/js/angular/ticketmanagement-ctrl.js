// this file contains controller for settings functionalities
(function(angular) {
	
	angular.module('app').controller('TicketManagementController', function ($scope,$window, $http, $filter, $routeParams, $location,$sce){
		$scope.$parent.activeMenu = 'ticketmanagement/0'
		$scope.$parent.sentimentalSource = NULL_VALUE;	
		$scope.userOriginalPost = NULL_VALUE;
		$scope.ticketSource = "2";
		$scope.statusFilter = "All";
		$scope.sortType = '-1';						// angular sort value ticket table initialized on feedback column
		$scope.sortReverse  = false;				// angular sort for ticket table to toggle between columns
		
		$scope.routPostID = "0";
		$scope.isRouted = "0";
		$scope.$parent.curTabID =100; 				// Currently not in use so its has been set to 100 
		$scope.ticketCurPage = 1;
		$scope.ticketDetailsCurPage = 1;
		$scope.ticketFrom = $scope.$parent.ticketYesterday;
		$scope.ticketTo = $scope.$parent.ticketToday;	
		
		$scope.labelVal = "0";
		
		if(authData.split('$')[5] == 0){
			$scope.addinfo = true;
			$location.path( "/addinfo" );
		}
		else if(authData.split('$')[4] == 0){
			$scope.gettingdata = true;
			$location.path( "/gettingdata" );
		}else{
			
			$scope.getTicketList = function(){
				var data = $.param({
					"inAccountId": authId,
			        "inSource": $scope.ticketSource,
			        "inStatus": $scope.statusFilter, 
			        "start_dt": toGMTDate($scope.ticketFrom),
			        "end_dt": toGMTDate($scope.ticketTo),
		        });
				$scope.entities = null;
				$http.post(getTicketListUrl, data, $scope.config).then(function(response){
					var data = response.data.results.rows;
				    $scope.tickets = data;
				    if($scope.statusFilter == "All"){
				    	$scope.labelVal = "Total " + $scope.tickets.length + " " + " Tickets";
				    } else {
				    	$scope.labelVal = $scope.tickets.length + " " + $scope.statusFilter + " Tickets";
				    }
				});	
			}
			
			$scope.getTicketListWithRoutParam = function(){
				var data = $.param({
					"inAccountId": authId,
			        "inSource": $scope.ticketSource,
			        "inStatus": $scope.statusFilter, 
			        "inPostId": $scope.routPostID,
			        "start_dt": toGMTDate($scope.ticketFrom),
			        "end_dt": toGMTDate($scope.ticketTo),
		        });
				$scope.entities = null;
				$http.post(getTicketListRoutParamUrl, data, $scope.config).then(function(response){
					var data = response.data.results.rows;
				    $scope.tickets = data;
				});	
			}
			
			if($routeParams.postid != "0"){
				$scope.routPostID = $routeParams.postid;
				$scope.getTicketListWithRoutParam();
				$scope.isRouted = "1";
			} else {
				//Calling backend to get data for ticket details table 
				$scope.getTicketList();
				$scope.isRouted = "0";
			}
			
			
		}
		
		
		
		$scope.showTicketDetails = function (ticketID,ticketStatus,ticketDate,ticketText){
			$scope.ticketDetails = "";
			var data = $.param({
				"ticketID": ticketID
	        });
			$scope.entities = null;
			$http.post(getTicketDetailsUrl, data, $scope.config).then(function(response){
				var data = response.data.results.rows;
				$scope.userPostText = "";
			    $scope.userPoststatus = "";
			    $scope.userPostDate = "";
				if(data.length == 0){
				    $scope.userPostText = "NA";
				    $scope.userPoststatus = "NEW";
				    $scope.userPostDate = "";
				} else {
					$scope.ticketDetails = data;
				    $scope.userPostText = data[0][4];
				    $scope.userPoststatus = data[0][5];
				    $scope.userPostDate = data[0][6];
				} 
					
			    
			});
		}
		
		$scope.setCommentText = function (originalPost,sourceType,postID){
			$scope.userOriginalPost = originalPost;
			if(sourceType =="1"){
				$scope.sourceTypeName = "Facebook";
				$scope.sourceTypeShortName = "fb";
			} else if(sourceType =="2"){
				$scope.sourceTypeName = "Twitter";
				$scope.sourceTypeShortName = "tw";
			} else {
				$scope.sourceTypeShortName = "Unknown";
				$scope.sourceTypeName = "Unknown";
			}
			
			$scope.selectedSourceName = sourceType;
			
			$scope.hidPostTypeID = postID+'#@#'+$scope.sourceTypeShortName;
		}
		
	});

})(window.angular);