// this file contains controller for brand functionalities
(function(angular) {
	
	angular.module('app').controller('CampaignController', function ($scope, $http, $filter, $routeParams, $location){
		$scope.$parent.activeMenu = 'campaign';
		$scope.$parent.latestNewsSection = "0";
		$scope.$parent.initialPageload = true;
		$scope.curTabID = 5;

		
	

		if(authData.split('$')[5] == 0){
			$location.path( "/addinfo" );
		}
		
		
		//alert($scope.activeMenu);
		if(authData.split('$')[5] == 0){
			$scope.addinfo = true;
			$location.path( "/addinfo" );
		}
		else if(authData.split('$')[4] == 0)
			$location.path( "/gettingdata" );
		else {
			
			//
			
		}

		
		Highcharts.chart('campaignTrendLineGraph', {
			colors: ['#00a3d8', '#7b26af', '#8ec741'],
		    chart: {
		        zoomType: 'xy'
		    },
		    title: {
		        text: null
		    },
		    credits: {
				enabled: false
			},
		    navigation: {
		        buttonOptions: {
		            enabled: false
		        }
		    },
		    xAxis: [{
		        categories: ['1 Nov', '2 Nov', '3 Nov', '4 Nov', '4 Nov', '6 Nov', '7 Nov'],
		        crosshair: true
		    }],
		    yAxis: [{ // Primary yAxis
		        labels: {
		            //format: '{value} M',
		        	formatter: function () {
		        		if(this.value >= 1000000000){
		                 	return this.value / 1000000000 + ' B'
		                 }
		               	else if(this.value >= 1000000){
		                 	return this.value / 1000000 + ' M'
		                 }
		               	else if(this.value >= 1000){
		                 	return this.value/1000 + ' K'
		                 }
		               	else {
		               		return this.value
		               	}
		           },
		            style: {
		                color: ['#999999']
		            }
		        },
		        title: {
		            text: 'Potential Impression',
		            style: {
		            	color: ['#999999']
		            }
		        },
		        opposite: true

		    }, { // Secondary yAxis
		        gridLineWidth: 0,
		        title: {
		            text: 'Mentions',
		            style: {
		            	color: ['#999999']
		            }
		        },
		        labels: {
		            //format: '{value} mm',
		            style: {
		            	color: ['#999999']
		            }
		        }

		    }, { // Tertiary yAxis
		        gridLineWidth: 0,
		        title: {
		            text: 'Net Sentiments',
		            style: {
		            	color: ['#999999']
		            }
		        },
		        labels: {
		            //format: '{value} mb',
		            style: {
		            	color: ['#999999']
		            }
		        },
		        opposite: true
		    }],
		    tooltip: {
		        shared: true
		    },
		    /*legend: {
		        layout: 'vertical',
		        align: 'left',
		        x: 80,
		        verticalAlign: 'top',
		        y: 55,
		        floating: true,
		        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
		    },*/
		    series: [{
		        name: 'Mentions',
		        type: 'area',
		        yAxis: 1,
		        fillOpacity: 0.2,
		        data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6],
		        marker: {
		        	fillColor: "#FFFFFF",
		        	symbol: 'circle',
					radius: 4,
					lineWidth: 2,
					lineColor: null
		        },
		        tooltip: {
		            //valueSuffix: ' mm'
		        }

		    }, {
		        name: 'Net Sentiments',
		        type: 'spline',
		        yAxis: 2,
		        data: [1016, 1016, 1015.9, 1015.5, 1012.3, 1009.5, 1009.6],
		        marker: {
		        	fillColor: "#FFFFFF",
		        	symbol: 'circle',
					radius: 4,
					lineWidth: 2,
					lineColor: null
		        },
		        //dashStyle: 'shortdot',
		        tooltip: {
		            //valueSuffix: ' mb'
		        }

		    }, {
		        name: 'Potential Impression',
		        type: 'spline',
		        data: [7.0, 6000.9, 900.5, 1400000.5, 1413548.2, 21.5, 268745.2],
		        marker: {
		        	fillColor: "#FFFFFF",
		        	symbol: 'circle',
					radius: 4,
					lineWidth: 2,
					lineColor: null
		        },
		        tooltip: {
		           // valueSuffix: ' °C'
		        }
		    }]
		});
		
		
		/*Metrics Mentions Graph*/		
		/*Highcharts.chart('kpiMentions', {
		    chart: {
		        type: 'pie',
		        inverted: true
		    },
		    colors: ['#ff9b47', '#dfdfdf'],
		    credits: false,
		    navigation: {
		        buttonOptions: {
		            enabled: false
		        }
		    },
		    title: {
		        text: null
		    },
		    dataLabels: false,
		    plotOptions: {
		        pie: {
		            innerSize: 200,
		            depth: 20,
		            cursor: '',
					dataLabels: {
						enabled: false
					},
		        }
		    },
		    series: [{
		        name: 'Mentions',
		        data: [
		            ['Users', 210],
		            ['KFC', 30]
		        ]
		    }]
		});*/
		

		
		var wordcloudArray = [
			  {
				    "text": "OTHERS",
				    "size": 94,
				    "sentiment": "Negative",
				    "type": "context",
				    "color": "#F55348"
				  },
				  {
				    "text": "Garbage",
				    "size": 82,
				    "sentiment": "Neutral",
				    "type": "context",
				    "color": "#F6BC41"
				  },
				  {
				    "text": "PRICES AND OFFERS",
				    "size": 35,
				    "sentiment": "Negative",
				    "type": "context",
				    "color": "#F55348"
				  },
				  {
				    "text": "PRODUCT QUALITY",
				    "size": 21,
				    "sentiment": "Neutral",
				    "type": "context",
				    "color": "#F6BC41"
				  },
				  {
				    "text": "HOSPITALITY",
				    "size": 14,
				    "sentiment": "Negative",
				    "type": "context",
				    "color": "#F55348"
				  },
				  {
				    "text": "SPEED & DELIVERY",
				    "size": 4,
				    "sentiment": "Negative",
				    "type": "context",
				    "color": "#F55348"
				  },
				  {
				    "text": "ACCURACY",
				    "size": 3,
				    "sentiment": "Neutral",
				    "type": "context",
				    "color": "#F6BC41"
				  }
				];
		
		var entityArray = [
			  {
				    "text": "Garbage",
				    "size": 82,
				    "type": "entity",
				    "color": "#888"
				  },
				  {
				    "text": "pay",
				    "size": 11,
				    "type": "entity",
				    "color": "#888"
				  },
				  {
				    "text": "refund",
				    "size": 10,
				    "type": "entity",
				    "color": "#888"
				  },
				  {
				    "text": "e-mail",
				    "size": 10,
				    "type": "entity",
				    "color": "#888"
				  },
				  {
				    "text": "payment",
				    "size": 10,
				    "type": "entity",
				    "color": "#888"
				  },
				  {
				    "text": "goodness",
				    "size": 10,
				    "type": "entity",
				    "color": "#888"
				  },
				  {
				    "text": "dear concern",
				    "size": 10,
				    "type": "entity",
				    "color": "#888"
				  },
				  {
				    "text": "paid",
				    "size": 10,
				    "type": "entity",
				    "color": "#888"
				  },
				  {
				    "text": "chicken",
				    "size": 10,
				    "type": "entity",
				    "color": "#888"
				  },
				  {
				    "text": "gst",
				    "size": 5,
				    "type": "entity",
				    "color": "#888"
				  },
				  {
				    "text": "order",
				    "size": 4,
				    "type": "entity",
				    "color": "#888"
				  },
				  {
				    "text": "people",
				    "size": 3,
				    "type": "entity",
				    "color": "#888"
				  },
				  {
				    "text": "water",
				    "size": 3,
				    "type": "entity",
				    "color": "#888"
				  },
				  {
				    "text": "common people",
				    "size": 3,
				    "type": "entity",
				    "color": "#888"
				  },
				  {
				    "text": "store",
				    "size": 3,
				    "type": "entity",
				    "color": "#888"
				  },
				  {
				    "text": "gst rates",
				    "size": 3,
				    "type": "entity",
				    "color": "#888"
				  },
				  {
				    "text": "bottle",
				    "size": 3,
				    "type": "entity",
				    "color": "#888"
				  },
				  {
				    "text": "standard",
				    "size": 3,
				    "type": "entity",
				    "color": "#888"
				  },
				  {
				    "text": "phone",
				    "size": 3,
				    "type": "entity",
				    "color": "#888"
				  },
				  {
				    "text": "prices",
				    "size": 3,
				    "type": "entity",
				    "color": "#888"
				  },
				  {
				    "text": "whats",
				    "size": 2,
				    "type": "entity",
				    "color": "#888"
				  },
				  {
				    "text": "kfc nuggets",
				    "size": 2,
				    "type": "entity",
				    "color": "#888"
				  },
				  {
				    "text": "effort mcd",
				    "size": 2,
				    "type": "entity",
				    "color": "#888"
				  },
				  {
				    "text": "delivery time",
				    "size": 2,
				    "type": "entity",
				    "color": "#888"
				  },
				  {
				    "text": "delivery system",
				    "size": 2,
				    "type": "entity",
				    "color": "#888"
				  },
				  {
				    "text": "customer executives",
				    "size": 2,
				    "type": "entity",
				    "color": "#888"
				  },
				  {
				    "text": "shame",
				    "size": 2,
				    "type": "entity",
				    "color": "#888"
				  },
				  {
				    "text": "price",
				    "size": 2,
				    "type": "entity",
				    "color": "#888"
				  },
				  {
				    "text": "replies",
				    "size": 2,
				    "type": "entity",
				    "color": "#888"
				  },
				  {
				    "text": "technical difficulties",
				    "size": 2,
				    "type": "entity",
				    "color": "#888"
				  }
				];
		
		generateWordcloud(wordcloudArray,entityArray);
	});


})(window.angular);
