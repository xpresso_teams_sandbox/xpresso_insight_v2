// this file contains controller for ticket management functionalities
(function(angular) {
	
	angular.module('app').controller('TicketController', function($scope, $http, $location){
		$scope.$parent.activeMenu = 'ticket';
		$scope.$parent.selectedSource = '';
		$scope.$parent.selectedTopic = '';
		
		if(authData.split('$')[5] == 0){
			$location.path( "/addinfo" );
		}
		else if(authData.split('$')[4] == 0)
			$location.path( "/gettingdata" );
		
		$scope.fn_ticket = function (){
			ticketSummaryDummy(authName);
			var data = $.param({
				"assigner_id": authEmail
	        });
			$http.post(ticketDetailsUrl, data, $scope.$parent.config).then(function(response){
				getTicketDetails(response.data.results.rows);
			});
		}
		$scope.fn_ticket();
	});

})(window.angular);