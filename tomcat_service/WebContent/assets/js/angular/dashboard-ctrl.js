// this file contains controller for settings functionalities
(function(angular) {
	
	angular.module('app').controller('DashboardController', function ($scope,$window, $http, $filter, $routeParams, $location,$sce){
		$scope.$parent.activeMenu = 'dashboard';				// holds the current tab of the navigation menu for keeping the tab highlighted with the active class, value is 'analytics when analytics/Explore page is shown as this controller is called'
		$scope.$parent.curTabID = 9;
		$scope.$parent.latestNewsSection = "0";
		$scope.selectedDashboardTopics = [];			// holds the aspect/topic values of Overall Sentiment section page filter
		$scope.selectedDashboardTopics = ($scope.selectedDashboardTopics  == 'All Contexts' || $scope.selectedDashboardTopics  == '') ? "-1" : $scope.selectedDashboardTopics;
		$scope.selectedDashboardSource=-1;
		$scope.dashboardFilteredStartDate = $scope.dashboardYesterday;
		$scope.dashboarFilteredEndDate = $scope.dashboardToday;
		$scope.sourcesName = [];
		
		//onclick of Go button this function is fired
		$scope.fn_dashboard_kpi_filter = function(){
			$scope.fn_dashboard_overall_sentiment_distribution();
			$scope.fn_dashboard_overall_sentiment_distribution_daywise();
			$scope.fn_dashboard_wordcloud();
			$scope.fn_dashboard_aspect_wise_pie();
			//$scope.fn_dashboard_getEntities();
			//$scope.fn_dashboard_getSentimentDistributionForAspect();
			$scope.fn_contextWiseSentimentEntityDistribution();
			$scope.fn_dashboard_source_aspect_wise_sentiment_distribution();
		}
		
		//Function is called for overall sentiment chart 
		$scope.fn_dashboard_overall_sentiment_distribution = function(){
			var data = $.param({
				"accountId": authId,
				"selectedSource": $scope.selectedDashboardSource,
				"selectedTopics":$scope.selectedDashboardTopics.toString(),
				"startDate": $scope.dashboardFilteredStartDate,
				"endDate": $scope.dashboarFilteredEndDate
			});

			$http.post(filterOverallSentimentDistributionUrlForDashboard, data, $scope.config).then(function(response){
				var data = response.data.results;	
				chart_overall_sentiment_bar_dashboard(data);
				//chart_overall_sentiment_line_dashboard(data);

			});
		}
		
		
		//Function is called for overall sentiment chart 
		$scope.fn_dashboard_overall_sentiment_distribution_daywise = function(){
			var data = $.param({
				"accountId": authId,
				"selectedSource": $scope.selectedDashboardSource,
				"startDate": $scope.dashboardFilteredStartDate,
				"endDate": $scope.dashboarFilteredEndDate
			});

			$http.post(getOverallSentimentDistributionDataDayWiseForDashboard, data, $scope.config).then(function(response){
				var data = response.data.results;	
				chart_daywise_overall_sentiment_line_for_dashboard(data);
			});
		}
		
		

		
		//Function For WordCloud
		$scope.fn_dashboard_wordcloud= function(){
			var dataForOverallSentiment = $.param({
				"accountId": authId,
				"sourceId": $scope.selectedDashboardSource,
				"startDate": $scope.dashboardFilteredStartDate,
				"endDate": $scope.dashboarFilteredEndDate,
				"sentiment": null
	        });
			
			var dataForPositiveSentiment = $.param({
				"accountId": authId,
				"sourceId": $scope.selectedDashboardSource,
				"startDate": $scope.dashboardFilteredStartDate,
				"endDate": $scope.dashboarFilteredEndDate,
				"sentiment": 1
	        });
			
			var dataForNegativeSentiment = $.param({
				"accountId": authId,
				"sourceId": $scope.selectedDashboardSource,
				"startDate": $scope.dashboardFilteredStartDate,
				"endDate": $scope.dashboarFilteredEndDate,
				"sentiment": -1
	        });
			
			$scope.fn_dashboard_populate_wordcloud(dataForOverallSentiment,"overallWordCloud");
			$scope.fn_dashboard_populate_wordcloud(dataForPositiveSentiment,"positiveWordCloud");
			$scope.fn_dashboard_populate_wordcloud(dataForNegativeSentiment,"negativeWordCloud");
		}
		
		// function for onload word cloud in trending topics / wordcloud post list section
		$scope.fn_dashboard_populate_wordcloud = function(data,sentimentDiv){
			var wordcloudArray = [];
		    var entityArray = [];
			$http.post(getWordCloudDataUrlForDashboard, data, $scope.config).then(function(response){
				$scope.fn_word_cloud_array_population(wordcloudArray,entityArray,response, 0,sentimentDiv);
			});
		}
		
		$scope.fn_word_cloud_array_population = function(wordcloudArray,entityArray,response, entityIndex,sentimentDiv){
			var wordcloudentity = response.data[0].rows;				// entities can be present in 0th or 1st index of response.data
			var wordcloudentityHead = response.data[0].columnNames;

		    // entity objects are formed with properties and pushed in entityArray
		    for(var i=0;i<wordcloudentity.length;i++){
		    	var entity = {};
				entity.text = wordcloudentity[i][0];
				entity.size = wordcloudentity[i][1];
				entity.sentiment = wordcloudentity[i][2];
				entity.type = wordcloudentityHead[0];
			
				
				/**
				 * this condition puts the required colors on entities depending on:
				 * if only entities are fetched, they are assigned colors depending on their respective sentiments
				 * if both entities and aspects are fetched, they are assigned a common color
				 */ 
				//alert(entity.size);
				if(wordcloudentity[i][2]=='Positive'){
					entity.color = '#3D8B37';
				}else if(wordcloudentity[i][2]=='Negative'){
					entity.color = '#C80000';
				}else{
					entity.color = '#ffa500';
				}
				
				entityArray.push(entity);
		    }
		    
		    // the entities filter list in the Trending Topics table (entities column) is populated 
			$scope.entitiesForFilter = [];
    		for(i=0;i<wordcloudentity.length;i++){
    			if($scope.entitiesForFilter.indexOf(wordcloudentity[i][0]) == -1){
    				$scope.entitiesForFilter.push(wordcloudentity[i][0].trim());
    			}
    		}
    		$scope.entitiesForFilter.sort();					// sorts the entities in the list in ascending order
    		$scope.entitiesForFilter.unshift("All Entities");
    		
    		/**
    		 * if entityIndex is 1, then response has fetched both aspects and entities
    		 * with aspects in 0th index and entities in 1st index
    		 */
    		/*if(entityIndex){
    			var wordcloudaspect = response.data[0].rows;
				for(i=0;i<wordcloudaspect.length;i++){
			    	var aspect = {};
					aspect.text = wordcloudaspect[i][0];
					aspect.size = wordcloudaspect[i][1];
					aspect.sentiment = wordcloudaspect[i][2];
					aspect.type = "context";
					
					// aspects are assigned colors depending on their respective sentiments
					if(wordcloudaspect[i][2]=='Positive'){
						aspect.color = 'rgba(102,212,78,0.8)';
					}else if(wordcloudaspect[i][2]=='Negative'){
						aspect.color = 'rgba(254,11,47,0.8)';
					}else{
						aspect.color = 'rgba(9,184,240,0.8)';
					}
					wordcloudArray.push(aspect);
				}
    		}*/
    		//generateDashboardWordcloud(wordcloudArray, entityArray,sentimentDiv);
    		generateDashboardWordcloud(entityArray,wordcloudArray, sentimentDiv);
		}

		//Function For Aspect wise PIE Chart
		$scope.fn_dashboard_aspect_wise_pie = function(){
			var data = $.param({
				"accountId": authId,
				"selectedSource": $scope.selectedDashboardSource,
				"selectedTopics":$scope.selectedDashboardTopics.toString(),
				"startDate": $scope.dashboardFilteredStartDate,
				"endDate": $scope.dashboarFilteredEndDate
			});

			$http.post(aspectWiseDistributionForDashboard, data, $scope.config).then(function(response){
				var data = response.data.results.rows;	
				var arr = [];
				var colorObj = {"ACCURACY":"#001f3f","CLEANLINESS":"#0074D9","HOSPITALITY":"#2ECC40","MAINTENANCE":"#3D9970", "PRICES AND OFFERS":"#FFDC00","PRODUCT QUALITY":"#FF851B","SPEED & DELIVERY":"#7FDBFF","OTHERS":"#fe91aa","Overall":"#fe91cc"};
				for (var inc = 0 ; inc< data.length ; inc ++){
					var obj = {name:"",y:"",color:""};
					obj["name"] = data[inc][1];
					obj["y"] = data[inc][0];
					obj["color"] = colorObj[data[inc][1]];
					arr.push(obj)
				}
				chart_aspect_wise_pie_dashboard(arr);
			});
		}
		
		//Source and aspect wise sentiment distribution chart
		$scope.fn_dashboard_source_aspect_wise_sentiment_distribution = function(){
			setTimeout(function(){
				angular.forEach($scope.$parent.dashboardTopicsForDiv, function (value, key) {
					//Calling backend for bar chart populate
					var paramObject = $.param({
							"accountId": authId,
							"selectedSource": $scope.selectedDashboardSource,
							"selectedTopics":$scope.selectedDashboardTopics.toString(),
							"startDate": $scope.dashboardFilteredStartDate,
							"endDate": $scope.dashboarFilteredEndDate,
							"aspect" : value[0]
						});
					
					$http.post(sourceAspectWiseSentimentDistributionForDashboard, paramObject, $scope.config).then(function(response){
						var dataChart = response.data.results.rows;	
						source_aspect_wise_sentiment_distribution_dashbaord(value[0].substr(0,5),dataChart);
					});
					//
					
					// Calling backend for populate top key words 
					var paramObjectEntity = $.param({
						"accountId": authId,
						"selectedSource": $scope.selectedDashboardSource,
						"selectedTopics":$scope.selectedDashboardTopics.toString(),
						"startDate": $scope.dashboardFilteredStartDate,
						"endDate": $scope.dashboarFilteredEndDate,
						"aspect" : value[0]
					});
					
					$http.post(sourceAspectWiseSentimentTopEntityForDashboard, paramObjectEntity, $scope.config).then(function(response){
					var dataEntity = response.data.results.rows;
					var dynamicHTMLPositive = '';
					var dynamicHTMLNegative = '';
					for(var incEntity = 0 ; incEntity<dataEntity.length; incEntity ++){
						if(dataEntity[incEntity][2] == "Positive"){
							dynamicHTMLPositive+="<li class='positiveText'>"+dataEntity[incEntity][1]+"</li>";
						} else {
							dynamicHTMLNegative+="<li class='negativeText'>"+dataEntity[incEntity][1]+"</li>";
						}
					}
					$("#entityText_"+ value[0].substr(0,5)).html(dynamicHTMLPositive+dynamicHTMLNegative);
						
					//Top posetive and negative feedback
					var paramObjectFeedback = $.param({
						"accountId": authId,
						"selectedSource": $scope.selectedDashboardSource,
						"selectedTopics":$scope.selectedDashboardTopics.toString(),
						"startDate": $scope.dashboardFilteredStartDate,
						"endDate": $scope.dashboarFilteredEndDate,
						"aspect" : value[0]
					});
					
					$scope.tempSources = angular.copy($scope.$parent.dashboardSources);
					$scope.tempSources.splice($scope.tempSources.map(function(el){return el[0];}).indexOf('All Sources'),1)
					$http.post(sourceAspectWiseSentimentTopFeedbackForDashboard, paramObjectFeedback, $scope.config).then(function(response){
						var dataFeedback = response.data.results.rows[0][0];
						var dynamicFeedbackHtml = '';
						for (var i = 0; i < $scope.tempSources.length; i++){
							var currentSourceName = $scope.tempSources[i][0];
							if(dataFeedback[currentSourceName] != undefined){
								dynamicFeedbackHtml += '<h4><span class="left">'+currentSourceName+'</span></h4>';
								for(var x= 0; x < dataFeedback[$scope.tempSources[i][0]].length; x++){
									/*if(dataFeedback[$scope.tempSources[i][0]][x].length > 64){
										//dynamicFeedbackHtml+='<div class="positive-feed all-fed"><p>'+dataFeedback[$scope.tempSources[i][0]][x]+'</p><a class="clsMore">Read More</a><div class="clearfix"></div></div>';
										//console.log(dataFeedback[$scope.tempSources[i][0]][x][0] + '--' + dataFeedback[$scope.tempSources[i][0]][x][1])
										dynamicFeedbackHtml+='<div class="'+dataFeedback[$scope.tempSources[i][0]][x][1]+'-feed "><p>'+dataFeedback[$scope.tempSources[i][0]][x][0]+ '</p><div class="clearfix"></div></div>';
										
										$(".clsMore").bind('click',function(){
											$(this).text("").parent().children('p').css({
										        'white-space': 'normal'
										    });
										});
										
									} else {
										//console.log(dataFeedback[$scope.tempSources[i][0]][x][0] + '--' + dataFeedback[$scope.tempSources[i][0]][x][1])
										dynamicFeedbackHtml+='<div class="'+dataFeedback[$scope.tempSources[i][0]][x][1]+'-feed ">'+dataFeedback[$scope.tempSources[i][0]][x][0]+'</div>';
									}*/
									
									/*$(".clsMore").bind('click',function(){
										alert($(this).text())
										$(this).text("").parent().children('p').css({
									        'white-space': 'normal'
									    });
									});*/
									dynamicFeedbackHtml+='<div class="'+dataFeedback[$scope.tempSources[i][0]][x][1]+'-feed ">'+dataFeedback[$scope.tempSources[i][0]][x][0]+'</div>';
									
									/*if(dataFeedback[$scope.tempSources[i][0]][x][1] == 'Positive'){
										dynamicFeedbackHtml+='<div class="Positive-feed ">'+dataFeedback[$scope.tempSources[i][0]][x][0]+'</div>';
									} else if(dataFeedback[$scope.tempSources[i][0]][x][1] == 'Negative'){
										dynamicFeedbackHtml+='<div class="Negative-feed ">'+dataFeedback[$scope.tempSources[i][0]][x][0]+'</div>';
									}*/
								}
								
								if(dataFeedback[$scope.tempSources[i][0]].length == 1){
									if(dataFeedback[$scope.tempSources[i][0]][0][1] == 'Positive' ){
										dynamicFeedbackHtml+='<div class="Negative-feed">No data found</div>';
									} else {
										dynamicFeedbackHtml+='<div class="Positive-feed">No data found</div>';
									}
								}
								
							} else {
								dynamicFeedbackHtml += '<h4><span class="left">'+currentSourceName+'</span></h4>';
								dynamicFeedbackHtml+='<div class="Positive-feed">No data found</div>';
								dynamicFeedbackHtml+='<div class="Negative-feed">No data found</div>';
							}
						}
						$('#feedbackText_'+value[0].substr(0,5)).html(dynamicFeedbackHtml);
						// scroll for feeds
					    
						    $('.social-feeds').slimScroll({
									height: '490px',
									color: '#000',
									size : '5px',
									wheelStep : 10,
									distance : '0',
									alwaysVisible : true,
							});
					    
					});
				});
				}, 3000);
			});
		}
		
		
		//Function For Aspect wise PIE Chart
		$scope.fn_dashboard_getEntities = function(aspectID){
			var data = $.param({
				"accountId": authId,
				"selectedSource": $scope.selectedDashboardSource,
				"selectedTopics":aspectID,
				"startDate": $scope.dashboardFilteredStartDate,
				"endDate": $scope.dashboarFilteredEndDate
			});

			$http.post(entitiesForAspectDistributionForDashboard, data, $scope.config).then(function(response){
				var data = response.data.results.rows;
				var arr = [];
				var colorObj = {"0":"   #f44336    ","1":"   #03a9f4    ","2":"   #ffe082    ","3":"   #43a047    ", "4":"    #f44336    ","5":"   #ff5722    ","6":"    #8bc34a    ","7":"#fe91aa","8":"#fe91cc"};
				for (var inc = 0 ; inc< data.length ; inc ++){
					var obj = {name:"",y:"",color:""};
					obj["name"] = data[inc][1];
					obj["y"] = data[inc][0];
					obj["color"] = colorObj[data[inc][1]];
					arr.push(obj)
				}
				generateChartForAspectWiseEntityDistribution(arr,aspectID);
			});
		}
		
		//Function For Aspect wise PIE Chart
		$scope.fn_dashboard_getSentimentDistributionForAspect = function(aspectID){
			var data = $.param({
				"accountId": authId,
				"selectedSource": $scope.selectedDashboardSource,
				"selectedTopics":aspectID,
				"startDate": $scope.dashboardFilteredStartDate,
				"endDate": $scope.dashboarFilteredEndDate
			});

			$http.post(sentimentDistributionForAspectForDashboard, data, $scope.config).then(function(response){
			
				generateChartForAspectWiseSentimentDistribution(response.data.results.rows,aspectID);
			});
		}
		
		$scope.fn_contextWiseSentimentEntityDistribution = function(){
			setTimeout(function(){
				angular.forEach($scope.$parent.dashboardTopicsForDiv2, function (value, key) {
					$scope.fn_dashboard_getSentimentDistributionForAspect(value[1]);
					$scope.fn_dashboard_getEntities(value[1]);
				});
			}, 2000);
		}
		
		
		//All onload functions are called from here
		$scope.fn_dashboard_overall_sentiment_distribution();
		$scope.fn_dashboard_overall_sentiment_distribution_daywise();
		$scope.fn_dashboard_wordcloud();
		$scope.fn_dashboard_aspect_wise_pie();
		//$scope.fn_dashboard_getEntities();
		//$scope.fn_dashboard_getSentimentDistributionForAspect();
		$scope.fn_contextWiseSentimentEntityDistribution();
		$scope.fn_dashboard_source_aspect_wise_sentiment_distribution();
		
		 $scope.fn_dashboard_source_onChange_dateSet = function(){
	        if($scope.selectedDashboardSource == "5"){
	        		$scope.dashboardFilteredStartDate = moment().subtract(8, "days").format("DD-MM-YYYY");
	        		$scope.dashboardFilteredStartDate = $scope.dashboardFilteredStartDate + " 00:01"
	        		$scope.dashboarFilteredEndDate = moment().subtract(2, "days").format("DD-MM-YYYY");
	        		$scope.dashboarFilteredEndDate = $scope.dashboarFilteredEndDate + " 23:59"
			} else {
				$scope.dashboardFilteredStartDate = moment().subtract(6, "days").format("DD-MM-YYYY");
				$scope.dashboardFilteredStartDate = $scope.dashboardFilteredStartDate + " 00:01";
        		$scope.dashboarFilteredEndDate = moment().format("DD-MM-YYYY");
        		$scope.dashboarFilteredEndDate = $scope.dashboarFilteredEndDate + " 23:59";
			}
		 }
		 
		// this function selects/deselects all options of the aspect list corresponding to the value of bool: true/false
		$scope.checkAll = function(bool,section){
			$scope.selectedTopics = [];
			$scope.selectedSentimentalTopics = [];
			for(var i=0 ; i < $scope.$parent.dashboardTopics.length; i++) {
		        $scope.$parent.dashboardTopics[i][2] = bool;
		        if(bool && i>0){
	        		$scope.selectedDashboardTopics.push( $scope.$parent.dashboardTopics[i][1]);
		        }
		    } 
		}
		
		// this function shows selected/deselected options in checkbox list and puts selected values in $scope.selectedTopics
		$scope.sync = function(bool, item, section){
			$scope.$parent.dashboardTopics[$scope.$parent.dashboardTopics.map(function(el){return el[1];}).indexOf(Number(item[1]))][2] = bool;	// check/uncheck option in checkbox list
			 
			/*
			 * bool == true: option got checked
			 * bool == false: option got unchecked
			 */
		    if(bool){
			    // add item
		    	if(item[1] == -1){									// checkbox against 'All Contexts' option got checked
	    			$scope.selectedDashboardTopics = [];
		    		$scope.checkAll(bool,section);
		    	}else{
	    			$scope.selectedDashboardTopics.push(item[1]);
		    	}
		    } else {
		        // remove item
		    	if(item[1] == -1){									// checkbox against 'All Contexts' option got unchecked
	    			$scope.selectedDashboardTopics = [];
		    		$scope.checkAll(bool,section);
		    	}else{
		    		$scope.$parent.dashboardTopics[0][2] = bool;				// anything gets deselected necessarily means "All Contexts" option is not selected
	    			$scope.selectedDashboardTopics.splice($scope.selectedDashboardTopics.map(function(el){return el;}).indexOf(Number(item[1])),1);	// remove unchecked item from selected list of items
		    	}
		    }
		};
		
		//bar
		/*Highcharts.chart('hospitality', {
			chart: {
                type: 'column'
            },
		    title: {
		        text: 'Sentiment Distribution'
		    },
		    xAxis: {
		        categories: ['Positive', 'Neutral', 'Negative']
		    },
		    colors: ['green', 'orange', 'red'],
		    plotOptions: {
                column: {
                    grouping: false,
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            credits: {
                enabled: false
            },
		    series: [{
                name: 'Positive',
                data: [[0, 10]]
    
            }, {
                name: 'Neutral',
                data: [[2, 15]]
            }, {
                name: 'Negative',
                data: [[1, 7]]
    
            }]
		});
*/		//pie

		/*Highcharts.chart('hospitalityPie', {
		    chart: {
		        plotBackgroundColor: null,
		        plotBorderWidth: null,
		        plotShadow: false,
		        type: 'pie'
		    },
		    title: {
		        text: 'Entity Distribution'
		    },
		    tooltip: {
		        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		    },
		    plotOptions: {
		        pie: {
		            allowPointSelect: true,
		            cursor: 'pointer',
		            dataLabels: {
		                enabled: true,
		                format: '<b>{point.name}</b><br>{point.percentage:.1f} %',
		                style: {
		                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
		                }
		            }
		        }
		    },
		    credits: {
	            enabled: false
	        },
		    series: [{
		        name: 'Entity',
		        colorByPoint: true,
		        data: [{
		            name: 'Gifts',
		            y: 20,
		            color:'#7cb5ec'
		        }, {
		            name: 'Money',
		            y: 10,
		            color:'#444349'
		        }, {
		            name: 'Pay',
		            y: 15,
		            color:'#90ec7d'
		        }, {
		            name: 'Cost',
		            y: 12,
		             color:'#f7a35b'
		        }, {
		            name: 'Tax',
		            y: 15,
		            color:'#8085e9'
		        }, {
		            name: 'Offers',
		            y: 8,
		            color:'#f25c81'
		        }, {
		            name: 'Compensation',
		            y: 11,
		            color:'#f4affc'
		        }, {
		            name: 'Gst',
		            y: 9,
		            color:'#fcd0af'
		        }]
		    }]
		});*/
		
		//hospitality chart
		//bar
	/*	Highcharts.chart('price', {
			chart: {
                type: 'column'
            },
		    title: {
		        text: 'Sentiment Distribution'
		    },
		    xAxis: {
		        categories: ['Positive', 'Neutral', 'Negative']
		    },
		    colors: ['green', 'orange', 'red'],
		    plotOptions: {
                column: {
                    grouping: false,
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            credits: {
                enabled: false
            },
		    series: [{
                name: 'Positive',
                data: [[0, 15]]
    
            }, {
                name: 'Neutral',
                data: [[2, 16]]
            }, {
                name: 'Negative',
                data: [[1, 18]]
    
            }]
		});
*/		//pie

/*		Highcharts.chart('pricePie', {
		    chart: {
		        plotBackgroundColor: null,
		        plotBorderWidth: null,
		        plotShadow: false,
		        type: 'pie'
		    },
		    title: {
		        text: 'Entity Distribution'
		    },
		    tooltip: {
		        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		    },
		    plotOptions: {
		        pie: {
		            allowPointSelect: true,
		            cursor: 'pointer',
		            dataLabels: {
		                enabled: true,
		                format: '<b>{point.name}</b><br>{point.percentage:.1f} %',
		                style: {
		                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
		                }
		            }
		        }
		    },
		    credits: {
	            enabled: false
	        },
		    series: [{
		        name: 'Entity',
		        colorByPoint: true,
		        data: [{
		            name: 'Complaint',
		            y: 27,
		            color:'#7cb5ec'
		        }, {
		            name: 'Call Order',
		            y: 23,
		            color:'#444349'
		        }, {
		            name: 'Service',
		            y: 10,
		            color:'#90ec7d'
		        }, {
		            name: 'Response',
		            y: 7,
		             color:'#f7a35b'
		        }, {
		            name: 'Msg',
		            y: 13,
		            color:'#8085e9'
		        }, {
		            name: 'Staff',
		            y: 10,
		            color:'#f25c81'
		        }, {
		            name: 'Action Serving',
		            y: 10,
		            color:'#fcd0af'
		        }]
		    }]
		});*/
	});
	
    function generateChartForAspectWiseSentimentDistribution(data,aspectID){
    	
    	var positiveVal,negativeVal,neutralVal;
    	var positiveVal = data[0][0];
    	var negativeVal = data[0][1];
    	var neutralVal = data[0][2];
    		
    		/*for (var inc = 0 ; inc< data.length ; inc ++){
				if(data[inc][1] =='Positive')
					positiveVal = data[inc][0];
				else if (data[inc][1]=='Negative')
					negativeVal = data[inc][0];
				else if (data[inc][1] =='Neutral')
					neutralVal = data[inc][0];
			}*/
    		
    	Highcharts.chart('quality_'+aspectID, {
			chart: {
                type: 'column'
            },
		    title: {
		        text: 'Sentiment Distribution'
		    },
		    xAxis: {
		        categories: ['Positive', 'Neutral', 'Negative']
		    },
		    scrollbar: {
	            barBackgroundColor: 'gray',
	            barBorderRadius: 7,
	            barBorderWidth: 0,
	            buttonBackgroundColor: 'gray',
	            buttonBorderWidth: 0,
	            buttonBorderRadius: 7,
	            trackBackgroundColor: 'none',
	            trackBorderWidth: 1,
	            trackBorderRadius: 8,
	            trackBorderColor: '#CCC'
	        },
		    yAxis: {
	            min: 0,
	            title: {
	                text: ''
	            },
	            stackLabels: {
	                enabled: true,
	                style: {
	                    fontWeight: 'bold',
	                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
	                }
	            }
	        },
		    colors: ['green', 'orange', 'red'],
		    plotOptions: {
                column: {
                    grouping: false,
                    pointPadding: 0.2,
                    borderWidth: 0
                },
                series: {
    	            dataLabels: {
    	                enabled: true
    	            }
    	        }
            },
            credits: {
	             enabled: false
	         },
	         exporting: {
	         	enabled: false
	         },
		    series: [{
                name: 'Positive',
                data: [[0, positiveVal]]
    
            }, {
                name: 'Neutral',
                data: [[1, neutralVal]]
            }, {
                name: 'Negative',
                data: [[2, negativeVal]]
    
            }]
		});
    }
    
    
    
    function generateChartForAspectWiseEntityDistribution(data,aspectID){
		Highcharts.chart('qualityPie_'+aspectID, {
		    chart: {
		        plotBackgroundColor: null,
		        plotBorderWidth: null,
		        plotShadow: false,
		        type: 'pie'
		    },
		    title: {
		        text: 'Entity Distribution'
		    },
		    tooltip: {
		        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
		    },
		    plotOptions: {
		        pie: {
		            allowPointSelect: true,
		            cursor: 'pointer',
		            dataLabels: {
		                enabled: true,
		                //format: '<b>{point.name}</b>',
		                //format: '<b>{point.name}</b><br>{point.percentage:.1f} %',
			            useHTML: true,
		                formatter: function () {
			            	// return '<div style="position: relative; top: -15px;">' + this.point.name + '<br>' + this.y + '&nbsp;Entity' + '<br>' + Math.round(this.percentage*100)/100 + ' %' +  '</div>'
		                	return '<div style="position: relative; top: -15px;">' + this.point.name + '' +'('+ this.y + ')' + '<br>' + Math.round(this.percentage*100)/100 + ' %' +  '</div>'
		                },
		                
		                style: {
		                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
		                }
		            }
		        }
		    },
		    credits: {
	             enabled: false
	         },
	         exporting: {
	         	enabled: false
	         },
		    series: [{
		        name: 'Entity',
		        colorByPoint: true,
		        data: data
		        /*data: [{
		            name: 'Burger',
		            y: 15,
		            color:'#7cb5ec'
		        }, {
		            name: 'Chicken',
		            y: 15,
		            color:'#444349'
		        }, {
		            name: 'Taste',
		            y: 12,
		            color:'#90ec7d'
		        }, {
		            name: 'Grilled',
		            y: 8,
		             color:'#f7a35b'
		        }, {
		            name: 'Mayonnaise',
		            y: 13,
		            color:'#8085e9'
		        }, {
		            name: 'Food',
		            y: 17,
		            color:'#b8f25c'
		        }, {
		            name: 'Delicious',
		            y: 10,
		            color:'#f4affc'
		        }, {
		            name: 'Meat',
		            y: 10,
		            color:'#fcd0af'
		        }]*/
		    }]
		});
		//context last div UI change
		var div7 = $('#divContext').find('.item:eq(6)').removeClass('col-md-4').addClass('col-md-12');
		$(div7).find('.contextBar').addClass('col-md-6 pull-left mt-3');
    	$(div7).find('.contextPie').addClass('col-md-6 pull-left mt-3').removeClass('mt-10');
    }

    // scroll for feeds
    /*setTimeout(function(){
	    $('.social-feeds').slimScroll({
				height: '400px',
				color: '#000',
				size : '5px',
				wheelStep : 10,
				distance : '0',
				alwaysVisible : true,
		});
    },4000);*/
    
})(window.angular);