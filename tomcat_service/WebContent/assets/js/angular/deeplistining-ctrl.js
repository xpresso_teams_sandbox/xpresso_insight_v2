// this file contains controller for settings functionalities
(function(angular) {
	
	angular.module('app').controller('DeepListiningController', function ($scope,$window, $http, $filter, $routeParams, $location,$sce){
		$scope.$parent.activeMenu = 'deeplistining'
		$scope.$parent.latestNewsSection = "0";
		$scope.detailsTabName = "SOCIAL_LISTENING";
		$scope.authRole = authRole;

		$scope.selectedDeepListSource =-1;
		
		$scope.selectedDeepListStartDate = $scope.$parent.newDeepListingYesterday;
		$scope.selectedDeepListEndDate = $scope.$parent.newDeepListingToday;
		$scope.deepListSourceCategory = SOCIAL_MEDIA;
		$scope.selectedDeepListSentiment="100";
		$scope.selectedDeepListTopics = $scope.$parent.deepListTopics;
		$scope.deepListSocialEntries = 10;	
		$scope.deepListTotalSocialCount = 0;
		
		//Summart KPI related declarations
		$scope.summarySource = [];
		$scope.summarySource.unshift(['Facebook', 1],['Twitter', 2]);
		$scope.summaryHourlySource = [];
		$scope.summaryHourlySource.unshift(['All Sources', 0],['Facebook', 1],['Twitter', 2]);
		$scope.selectedSummarySource =1;
		$scope.selectedSummaryHourlySource = 0;
		$scope.selectedSummaryTopNegativeAspectSource=1;
		$scope.selectedDMSource=2;
		$scope.selectedSummaryPostType="0";
		$scope.selectedSummarySortby = "1";
		
		$scope.selectedSummaryStartDate = $scope.$parent.newDeepListingYesterday;
		$scope.selectedSummaryEndDate = $scope.$parent.newDeepListingToday;
		
		$scope.selectedSummaryTopNegativeAspectStartDate = $scope.$parent.newDeepListingTopNegativeYesterday;
		$scope.selectedSummaryTopNegativeAspectEndDate = $scope.$parent.newDeepListingTopNegativeToday;
		
		$scope.selectedDMStartDate = $scope.$parent.newDeepListingTopNegativeYesterday;
		$scope.selectedDMEndDate = $scope.$parent.newDeepListingTopNegativeToday;
		$scope.summarypostslists = [];
		$scope.summarypostsDetails = [];
		$scope.summarypostsComments=[]
		$scope.summarypostsReplies=[]
		$scope.checksource = 1
		$scope.searchPostList = "";
		$scope.modalPositiveValue = "";
		$scope.modalNegativeValue = "";
		$scope.modalNeutralValue = "";
		$scope.selectedPostDivID = "";
		$scope.selectedDMDivID = "";
		$scope.adminUserName="KFC_India";
		$scope.commentTweetOffSet = 0;
		
		// object for onload Deep Listening table table
		$scope.deepListSocialPagination = {
				deepListSocialCurrentPage: 1,
				deepListSocialNewPage: 1
		};
		
		$scope.deepListSocialPageChanged = function(newPage) {
			$scope.$parent.deepListSocialNewPage = newPage;
			$scope.fn_deep_list($scope.$parent.deepListSocialNewPage);
	    };
		
	    
		$scope.fn_deep_list = function(newPage){
			console.log("---->>" + $scope.selectedDeepListTopics)
			console.log($scope.$parent.deepListTabFilterdata);
			
			if(!$scope.$parent.isFromBackButton){
				//alert("Normal loading")
				//alert($scope.selectedDeepListTopics.length)
				if($scope.selectedDeepListTopics.length >=7){
					$scope.selectedDeepListTopics = "";
				}
				$scope.$parent.deepListTabFilterdata = {
					"accountId": authId, 
			        "sourceId": $scope.selectedDeepListSource, 
			        "searchKey": "",
			        "sourceCategory": $scope.deepListSourceCategory , 
			        //"sourceTopic": ($scope.selectedDeepListTopics.length+1 == $scope.$parent.deepListTopics.length)?null:$scope.selectedDeepListTopics.toString(),
			        "sourceTopic":($scope.selectedDeepListTopics == null)?"":$scope.selectedDeepListTopics.toString(),
			        "startDate": $scope.selectedDeepListStartDate,
			        "endDate" :  $scope.selectedDeepListEndDate,
			        "virality" :"",
		            "sentiment":($scope.selectedDeepListSentiment  == 'All' || $scope.selectedDeepListSentiment  == '') ? null : $scope.selectedDeepListSentiment,
		            "city": "",
			        "tabName" :$scope.detailsTabName  ,
		            "perPageEntries" : $scope.deepListSocialEntries,
		            "newPage" :newPage,
		            "interval":1
				};
				$scope.deepListSocialPagination = {
						deepListSocialCurrentPage: newPage
				};
				$scope.$parent.deepListSocialNewPage = newPage;
				
			} else {
				//alert("Back button")
				
				console.log("===>>"+$scope.selectedDeepListTopics);
				$scope.selectedDeepListSource=$scope.$parent.deepListTabFilterdata.sourceId;
				//$scope.selectedDeepListTopics=($scope.$parent.deepListTabFilterdata.sourceTopic+1 == $scope.$parent.deepListTopics.length)?null:$scope.$parent.deepListTabFilterdata.sourceTopic.toString();
				$scope.selectedDeepListTopics=($scope.$parent.deepListTabFilterdata.sourceTopic == null)?"":$scope.$parent.deepListTabFilterdata.sourceTopic.toString();
				//alert($scope.selectedDeepListTopics.length);
				if($scope.selectedDeepListTopics.length >=7){
					$scope.selectedDeepListTopics = "";
				}
				//$scope.selectedDeepListTopics= $scope.selectedDeepListTopics;// ($scope.selectedDeepListTopics.length+1 == $scope.$parent.deepListTopics.length)?null:$scope.selectedDeepListTopics.toString(),
				$scope.selectedDeepListStartDate=$scope.$parent.deepListTabFilterdata.startDate;
				$scope.selectedDeepListEndDate=$scope.$parent.deepListTabFilterdata.endDate;
				$scope.selectedDeepListSentiment=$scope.$parent.deepListTabFilterdata.sentiment;
				$scope.newPage=$scope.$parent.deepListSocialNewPage;
				$scope.deepListSocialPagination = {
						deepListSocialCurrentPage: $scope.$parent.deepListSocialNewPage
				};
				
				//alert($scope.selectedDeepListTopics.length)
				$scope.$parent.deepListTabFilterdata = {
					"accountId": authId, 
			        "sourceId": $scope.selectedDeepListSource, 
			        "searchKey": "",
			        "sourceCategory": $scope.deepListSourceCategory , 
			        "sourceTopic": $scope.selectedDeepListTopics, 
			        "startDate": $scope.selectedDeepListStartDate,
			        "endDate" :  $scope.selectedDeepListEndDate,
			        "virality" :"",
		            "sentiment":($scope.selectedDeepListSentiment  == 'All' || $scope.selectedDeepListSentiment  == '') ? null : $scope.selectedDeepListSentiment,
		            "city": "",
			        "tabName" :$scope.detailsTabName  ,
		            "perPageEntries" : $scope.deepListSocialEntries,
		            "newPage" :$scope.$parent.deepListSocialNewPage,
		            "interval":1
				};
			}
			
			var deepListDataObj = JSON.stringify($scope.$parent.deepListTabFilterdata);
			
			
			$scope.deepListSocLists = null;
			$scope.deepListNonSocLists = null;
			$scope.deepListTotalSocialCount = 0;
			$scope.$parent.isFromBackButton = false;
			$http.post(deepListeningUrlForDeepListTab, deepListDataObj, $scope.config).then(function(response){
				
				$scope.deepListSocLists = response.data.output.socialPostListObj;
				
				if($scope.deepListSocLists.length == 0){
					$scope.deepListTotalSocialCount = 0;
				} else {
					$scope.deepListTotalSocialCount = $scope.deepListSocLists[0].totalCount;
				}
				
				$scope.deepListNonSocLists = response.data.output.nonSocialPostListObj;
				$scope.filtered = false;
		
			});

		}
		
		$scope.fn_deep_list(1);
		
		//Code for summary KPI
		
		//Getting data label counts on page load as well as when click GO button
		$scope.fn_generate_data_counts = function(){
			$scope.overallSentimentDataCount = [];
			var data = $.param({
				"inAccountId" : authId,
				"posttype":$scope.selectedSummaryPostType,
				"sourceType":$scope.selectedSummarySource,
				"sortby": $scope.selectedSummarySortby,
				"start_dt":toGMTDate($scope.selectedSummaryStartDate),
				"end_dt":toGMTDate($scope.selectedSummaryEndDate)
	        });
			
			$http.post(getSummaryOverallDataCount, data, $scope.config).then(function(response){
				$scope.overallSentimentDataCount = response.data.results.rows;
			});
		}

		//Overall sentiment  top chart
		$scope.fn_generate_overall_sentiment_chart = function(){
			$scope.overallSentiment = [];
			var data = $.param({
				"inAccountId" : authId,
				"postType":$scope.selectedSummaryPostType,
				"sourceType":$scope.selectedSummarySource,
				"start_dt":toGMTDate($scope.selectedSummaryStartDate),
				"end_dt":toGMTDate($scope.selectedSummaryEndDate)
	        });
			
			$http.post(getSummaryOverallSentimentChart, data, $scope.config).then(function(response){
				$scope.overallSentiment = response.data.results;
				chart_post_level_chain($scope.overallSentiment);
			});
		}
		
		//Overall hourly sentiment chart
		$scope.fn_generate_hourly_sentiment_chart = function(){
			$scope.overallHourlySentiment = [];
			var data = $.param({
				"inAccountId" : authId,
				"sourceType":$scope.selectedSummaryHourlySource
	        });
			
			$http.post(getSummaryOverallSentimentHourlyChart, data, $scope.config).then(function(response){
				$scope.overallHourlySentiment = response.data.results;
				chart_generate_hourly_sentiment_chart($scope.overallHourlySentiment);
			});
		}
		
		$scope.fn_generate_top_negative_aspect_chart = function(){
			$scope.topNegativeAspectSentiment = [];
			var data = $.param({
				"inAccountId" : authId,
				"sourceType":$scope.selectedSummaryTopNegativeAspectSource,
				"start_dt":toGMTDate($scope.selectedSummaryTopNegativeAspectStartDate),
				"end_dt":toGMTDate($scope.selectedSummaryTopNegativeAspectEndDate)
	        });
			
			$http.post(getSummaryTopNegativeAspectChart, data, $scope.config).then(function(response){
				$scope.topNegativeAspectSentiment = response.data.results;
				chart_generate_top_negative_aspect_chart($scope.topNegativeAspectSentiment);
			});
		}
		

		$scope.fn_showTopNegativeAspectDeatilsModal = function(selectedBarValue){
			var splitParam = selectedBarValue.split('<br/>');
			$scope.topTopNegativeAspectDeatilsModal = [];

			var data = $.param({
				"selected_dt":splitParam[0],
				"sourceType":$scope.selectedSummaryTopNegativeAspectSource,
				"selected_aspect":splitParam[1]
	        });
			
			$http.post(getTopNegativeAspectDeatilsModal, data, $scope.config).then(function(response){
				$scope.topTopNegativeAspectDeatilsModal = response.data.results.rows;
				$('#topNegativeAspectDetailsModal').modal("show");
				//chart_generate_top_negative_aspect_chart($scope.topNegativeAspectSentiment);
			});
		}
		
		// Get conversation chain gainst post id
		$scope.fn_get_replies = function(post_id){
			var ownSocialPostDetailsReplies = [];
			var data = $.param({
				"inAccountId" : authId,
				"postId":post_id,
				"sourceType":$scope.selectedSummarySource
	        });
			
			$http.post(getSummaryReplies, data, $scope.config).then(function(response){
				ownSocialPostDetailsReplies = response.data.results.rows;
				$scope.summarypostsReplies = ownSocialPostDetailsReplies;
				
				//Get replies against each comment
				for(var inc = 0;inc<$scope.summarypostsReplies.length;inc++){
					var sentimentClass = 'text-green';
					if($scope.summarypostsReplies[inc][3] == 'Negative'){
						sentimentClass = 'text-red';
					} else if($scope.summarypostsReplies[inc][3] == 'Neutral'){
						sentimentClass = 'text-yellow';
					} 
					if($scope.summarypostsReplies[inc][2] == 'null' || $scope.summarypostsReplies[inc][2] == null){
						$scope.summarypostsReplies[inc][2] = '';
					}
					$("#reply_"+post_id).append('<li><div><span class="username_label"><i class="fa fa-user"></i>'+$scope.summarypostsReplies[inc][2]+'</span> '+$scope.summarypostsReplies[inc][1]+' <span class="comment-date">'+$scope.summarypostsReplies[inc][4]+'</span></div> <strong class="'+sentimentClass+'">'+$scope.summarypostsReplies[inc][3]+'</strong> </li>');
					//$("#reply_"+post_id).append('<li><div><span class="username_label"><i class="fa fa-user"></i>'+$scope.summarypostsReplies[inc][2]+'</span> '+$scope.summarypostsReplies[inc][1]+'<a title="Click for Invalidate Post pull-left" ng-click="fn_invalidate_entry('+$scope.summarypostsReplies[inc][0]+')" > <span class="label label-danger pointer" id="btnInvalid_'+$scope.summarypostsReplies[inc][0]+'">Invalidate</span> </a> <span class="comment-date">'+$scope.summarypostsReplies[inc][4]+'</span></div> <strong class="'+sentimentClass+'">'+$scope.summarypostsReplies[inc][3]+'</strong> </li>')
				}
				
			});
		}
		
		// Get conversation chain gainst post id
		$scope.fn_get_conversation_chain_comments = function(post_id){
			$scope.localSelectedPostID = post_id;
			$scope.commentTweetOffSet = 0;
			var ownSocialPostDetailsComments = [];
			var data = $.param({
				"inAccountId" : authId,
				"postId":post_id,
				"sourceType":$scope.selectedSummarySource,
				"offSet" : $scope.commentTweetOffSet,
				"limit" : 10
	        });
			
			$http.post(getSummaryComments, data, $scope.config).then(function(response){
				ownSocialPostDetailsComments = response.data.results.rows;
				$scope.summarypostsComments = ownSocialPostDetailsComments;
				
				//Get replies against each comment
				for(var inc = 0;inc<$scope.summarypostsComments.length;inc++){
					$scope.fn_get_replies($scope.summarypostsComments[inc][0]);
				}
				
				$scope.varTotalCount = $scope.summarypostsComments[0][5];
				
			});
			
			setTimeout(function(){
				$('.user-social-post-wapper').slimScroll({
						height: '400px',
						color: '#000',
						size : '5px',
						wheelStep : 10,
						distance : '0',
						alwaysVisible : true,
				});
				
			},4000);
		}
		
		$scope.loadMoreComments = function(){
			$scope.commentTweetOffSet = parseInt($scope.commentTweetOffSet+10);
			var data = $.param({
				"inAccountId" : authId,
				"postId":$scope.localSelectedPostID,
				"sourceType":$scope.selectedSummarySource,
				"offset" : $scope.commentTweetOffSet,
				"limit" : 10
	        });
			
			$http.post(getSummaryComments, data, $scope.config).then(function(response){
				$scope.summarypostsCommentsLimit = response.data.results.rows;
				$scope.summarypostsComments = $scope.summarypostsComments.concat($scope.summarypostsCommentsLimit)
				
				//Get replies against each comment
				for(var inc = 0;inc<$scope.summarypostsComments.length;inc++){
					$scope.fn_get_replies($scope.summarypostsComments[inc][0]);
				}
				$scope.varTotalCount = $scope.summarypostsComments[0][5];
				
			});
			
			setTimeout(function(){
				$('.user-social-post-wapper').slimScroll({
						height: '400px',
						color: '#000',
						size : '5px',
						wheelStep : 10,
						distance : '0',
						alwaysVisible : true,
				});
				
			},4000);
		}
		//Get post reply.tweet/lime/mentioned etc count against post id
		$scope.fn_get_post_details = function(post_id){
			$scope.selectedPostDivID = post_id;
			var ownSocialPostDetailsData = [];
			var data = $.param({
				"inAccountId" : authId,
				"postId":post_id,
				"sourceType":$scope.selectedSummarySource
	        });
			
			$http.post(getSummaryDetailsPostTweet, data, $scope.config).then(function(response){
				ownSocialPostDetailsData = response.data.results.rows;
				$scope.summarypostsDetails = ownSocialPostDetailsData;
				
				//Get comment/reply chain for first post
				$scope.fn_get_conversation_chain_comments(post_id);
			});
		}
		
		//
		$scope.fn_get_DM_details = function(DM_user_name){
			$scope.selectedDMDivID = DM_user_name;
			var DMDetailsData = [];
			var data = $.param({
				"inAccountId" : authId,
				"userName":DM_user_name,
				"start_dt":toGMTDate($scope.selectedDMStartDate),
				"end_dt":toGMTDate($scope.selectedDMEndDate),
				"sourceType":$scope.selectedDMSource
	        });
			
			$http.post(getDMDetailedChain, data, $scope.config).then(function(response){
				DMDetailsData = response.data.results.rows;
				$scope.dmDetailedChain = DMDetailsData;
			});
		}

		// Social Posts List
		$scope.fn_own_social_post_list = function(){
			//var ownSocialPostCols = [];
			var ownSocialPostData = [];
			var data = $.param({
				"inAccountId" : authId,
				"sourceType":$scope.selectedSummarySource,
				"postType":$scope.selectedSummaryPostType,
				"start_dt":toGMTDate($scope.selectedSummaryStartDate),
				"end_dt":toGMTDate($scope.selectedSummaryEndDate),
				"strSearchString" : $scope.searchPostList,
				"sortby": $scope.selectedSummarySortby
	        });
			
			$scope.checksource = $scope.selectedSummarySource;
			
			$http.post(getSummaryOwnSocialPostTweet, data, $scope.config).then(function(response){
				//ownSocialPostCols = response.data.results.column_names;
				ownSocialPostData = response.data.results.rows;
				
				$scope.summarypostslists = ownSocialPostData;

				//Get post details against first post only
				if($scope.summarypostslists.length !=0){
					$scope.selectedPostDivID = $scope.summarypostslists[0][0];
					$scope.fn_get_post_details($scope.summarypostslists[0][0]);
				} else {
					$scope.fn_get_post_details(1);
				}
				
				setTimeout(function(){
					$('.user-social-post-wapper').slimScroll({
							height: '400px',
							color: '#000',
							size : '5px',
							wheelStep : 10,
							distance : '0',
							alwaysVisible : true,
					});
					
					var a = new Array();

					$('.post_list').each(function(index) {
					    var text = $(this).text();
					    if($.inArray(text, a)!=-1){
					        $(this).closest('.post_list').hide();
					    }else{
					        a.push(text);
					    }
					});
					
				},4000);
				
			});
		}
		
		$scope.fn_showSentimentDetailsModal = function(selectedSentiment){
			$("[id^='chk']").prop('checked',false);
			
			$scope.overallSentimentDetails = [];
			var data = $.param({
				"inAccountId" : authId,
				"postType":$scope.selectedSummaryPostType,
				"sourceType":$scope.selectedSummarySource,
				"selectedSentiment":selectedSentiment,
				"start_dt":toGMTDate($scope.selectedSummaryStartDate),
				"end_dt":toGMTDate($scope.selectedSummaryEndDate)
	        });
			
			$http.post(getSummaryOverallSentimentDetails, data, $scope.config).then(function(response){
				$scope.overallSentimentDetails = response.data.results.rows;
				$('#sentimentDetailsModal').modal("show");
				
				var checkSelectedSentiment = selectedSentiment.split(",");
				for(var inc = 0; inc <checkSelectedSentiment.length; inc++){
					$('#chk'+checkSelectedSentiment[inc]).prop('checked',true);
				}
				
			});
		}
		
		$("[id^='chk']").click(function() {
			var checkedVals = $('.chk-hidden:checkbox:checked').map(function() {
			    return this.value;
			}).get();
			
			$scope.fn_showSentimentDetailsModal(checkedVals.toString());
		});

		$scope.fn_user_list_direct_message = function(){
			var dMUserList = [];
			var data = $.param({
				"inAccountId" : authId,
				"sourceType":$scope.selectedDMSource,
				"start_dt":toGMTDate($scope.selectedDMStartDate),
				"end_dt":toGMTDate($scope.selectedDMEndDate)
	        });
			
			$scope.checksource = $scope.selectedSummarySource;
			
			$http.post(getDMUserList, data, $scope.config).then(function(response){
				dMUserList = response.data.results.rows;
				
				$scope.dMUserListData = dMUserList;
				
				var a = new Array();
				setTimeout(function(){
					$('.dmUserList').each(function(index) {
					    var text = $(this).text();
					    if($.inArray(text, a)!=-1){
					        //$(this).closest('.dmUserList').hide();
					    	$(this).closest('.dmUserList').remove();
					    }else{
					        a.push(text);
					    }
					});
				},400);

				//Get post details against first post only
				if($scope.dMUserListData.length !=0){
					$scope.selectedDMDivID = $scope.dMUserListData[0][0];
					$scope.fn_get_DM_details($scope.dMUserListData[0][0]);
				} else {
					$scope.fn_get_DM_details(1);
				}
				
			});
		}
		
		
		//Calling Summart KPI methods
		$scope.fn_own_social_post_list();
		$scope.fn_generate_overall_sentiment_chart();
		$scope.fn_generate_data_counts();
		$scope.fn_generate_hourly_sentiment_chart();
		$scope.fn_generate_top_negative_aspect_chart();
		
		//Userlevel DM ldisplay KPI
		$scope.fn_user_list_direct_message();
		
		$scope.fn_change_sorting = function(sortByVal){
			$scope.selectedSummarySortby = sortByVal;
			$scope.fn_own_social_post_list();
			$scope.fn_generate_overall_sentiment_chart();
			$scope.fn_generate_data_counts();
		}
		
		// this function selects/deselects all options of the aspect list corresponding to the value of bool: true/false
		$scope.checkAll = function(bool,section){
			$scope.selectedDeepListTopics = [];
			$scope.selectedSentimentalTopics = [];
			for(var i=0 ; i < $scope.$parent.deepListTopics.length; i++) {
		        $scope.$parent.deepListTopics[i][2] = bool;
		        if(bool && i>0){
		        	if(section == 'explore'){
		        		$scope.selectedDeepListTopics.push( $scope.$parent.deepListTopics[i][1]);
		        	} else{
		        		$scope.selectedSentimentalTopics.push( $scope.$parent.deepListTopics[i][1]);
		        	}
		        }
		    } 
		}

		// This function shows selected/deselected options in checkbox list and puts selected values in $scope.selectedDeepListTopics
		$scope.sync = function(bool, item, section){
			//alert("check all hit" + bool + "--" + item + "--" + section)
			$scope.$parent.deepListTopics[$scope.$parent.deepListTopics.map(function(el){return el[1];}).indexOf(Number(item[1]))][2] = bool;	// check/uncheck option in checkbox list
			 
			/*
			 * bool == true: option got checked
			 * bool == false: option got unchecked
			 */
			
		    if(bool){
			    // add item
		    	if(item[1] == -1){									// checkbox against 'All Contexts' option got checked
		    		if(section == 'explore'){
		    			$scope.selectedDeepListTopics = [];
		    		}else{
		    			$scope.selectedSentimentalTopics = [];
		    		}
		    		$scope.checkAll(bool,section);
		    	}else{
		    		if(section == 'explore'){
		    			$scope.selectedDeepListTopics.push(item[1]);
		    		} else{
		    			$scope.selectedSentimentalTopics.push(item[1]);
		    		}
		    	}
		    } else {
		        // remove item
		    	if(item[1] == -1){									// checkbox against 'All Contexts' option got unchecked
		    		if(section == 'explore'){
		    			$scope.selectedDeepListTopics = [];
		    		}else{
		    			$scope.selectedSentimentalTopics = [];
		    		}
		    		$scope.checkAll(bool,section);
		    	}else{
		    		$scope.$parent.deepListTopics[0][2] = bool;				// anything gets deselected necessarily means "All Contexts" option is not selected
		    		if(section == 'explore'){
		    			$scope.selectedDeepListTopics.splice($scope.selectedDeepListTopics.map(function(el){return el;}).indexOf(Number(item[1])),1);	// remove unchecked item from selected list of items
		    		}else{
		    			$scope.selectedSentimentalTopics.splice($scope.selectedSentimentalTopics.map(function(el){return el;}).indexOf(Number(item[1])),1);
		    		}
		    	}
		    }
		};
		
		$scope.checkKeyPress = function(keyEvent){
			if (keyEvent.which === 13)
				$scope.fn_own_social_post_list();
		}
		
		$scope.changePostTypeSelection = function(){
			if($scope.selectedSummarySource == 2){
				$scope.selectedSummaryPostType = "3";
				$scope.selectedSummarySortby = "3";
			} else {
				$scope.selectedSummaryPostType = "0";
				$scope.selectedSummarySortby = "1";
			}
		}
		
		setTimeout(function(){
			$('.user-social-post-wapper').slimScroll({
					height: '400px',
					color: '#000',
					size : '5px',
					wheelStep : 10,
					distance : '0',
					alwaysVisible : true,
			});
			
		},4000);
		
			
		 $('#commentSearch').keyup(function(){
			  // Search text
			  var text = $(this).val();
			  // Hide all content class element
			  $('.content').hide();

			  // Search 
			  $('.content:contains("'+text+'")').closest('.content').show();
			 
			 });
		    
		    $('#postSearch').keyup(function(){
		  	  // Search text
		  	  var text = $(this).val();
		  	  // Hide all content class element
		  	  $('.postContent').hide();

		  	  // Search 
		  	  $('.postContent:contains("'+text+'")').closest('.postContent').show();
		  	 
		  	 });
		    
		    $('#tweetUserSearch').keyup(function(){
		  	  // Search text
		  	  var text = $(this).val();
		  	  // Hide all content class element
		  	  $('.tweetUsers').hide();

		  	  // Search 
		  	  $('.tweetUsers:contains("'+text+'")').closest('.tweetUsers').show();
		  	 
		  	 });
		    
		    $.expr[":"].contains = $.expr.createPseudo(function(arg) {
			  return function( elem ) {
			   return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
			  };
			});
	});
})(window.angular);