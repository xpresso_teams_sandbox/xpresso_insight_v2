// this file contains controller to populate the select boxes and to call filtered functions of all pages
(function(angular) {
	'use strict';
	var rcxApp = angular.module('app', ['ngAnimate', 'ui.bootstrap', 'angularUtils.directives.dirPagination', 'ngLoadingSpinner']);
	
	rcxApp.controller('SelectBoxController', function ($scope, $http) {
	    $scope.sources = [];
	    $scope.topics = [];
		$scope.selectedSource = '';
	    $http.get(getSourcesTopicsUrl+'?companyId='+authId+'&type='+"source").success( function(response) {
	    	$scope.sources = response.results.rows;
	    	$scope.sources.unshift(["All", 'All']);
	    });
	    $scope.selectedTopic = '';
	    $http.get(getSourcesTopicsUrl+'?companyId='+authId+'&type='+"topics").success( function(response) {
	    	$scope.topics = response.results.rows;
	    	$scope.topics.unshift("All");
	    });
	    $scope.$watch(function() {
	        $('.selectpicker').selectpicker('refresh');
	    });
	    
	    //function for date filter validation
		$scope.dateRange = function(section){
			if(section == "dashboard"){
				var datestart = $('#datepicker_start').find('input').val();	
				var dateend = $('#datepicker_end').find('input').val();
			} else if(section == "analytics"){
				var datestart = $('#datepicker_start_entity').find('input').val();	
				var dateend = $('#datepicker_end_entity').find('input').val();
			}
			var start = new Date(datestart.substring(6,10)+'-'+datestart.substring(3,6)+datestart.substring(0,2)+datestart.substring(10,datestart.length));
			var end = new Date(dateend.substring(6,10)+'-'+dateend.substring(3,6)+dateend.substring(0,2)+dateend.substring(10,dateend.length));
			
			if(start>end){
				$('#alertModal').modal("show");
		        $('#alertModal .modal-content').addClass('modal-danger');
		        $('#alertTxt').html("End date should not be behind Start date");
			}
			else{
				if(section == "dashboard"){
					viralityComparissionObj.accountId = authId;
					viralityComparissionObj.startDt = datestart;
					viralityComparissionObj.endDt = dateend;
					viralityComparissionObj.hR = 1;
					viralityComparissionObj.selectedInterval = 'Filter';
					$scope.$parent.fn_virality_comparission(viralityComparissionObj);
				}else if(section == "analytics"){
					filteredEntityDetailsObj.accountId = authId; 
					filteredEntityDetailsObj.rowSourceId = ($('#source').val() == 'All' || $('#source').val() == '') ? -1 : $('#source').val();
					filteredEntityDetailsObj.rowSourceTopic = ($('#topics').val()  == 'All' || $('#topics').val()  == '') ? null : $('#topics').val() ;
					filteredEntityDetailsObj.startDt = $('#datepicker_start_entity').find('input').val();
					filteredEntityDetailsObj.endDt = $('#datepicker_end_entity').find('input').val();
					filteredEntityDetailsObj.sentiment = ($('#sentiment').val()  == 'All' || $('#sentiment').val()  == '') ? null : $('#sentiment').val() ;
					$scope.$parent.fn_filtered_entity_details(filteredEntityDetailsObj);
					 
					filteredAnalyticsDetailsObj.accountId = authId; 
					filteredAnalyticsDetailsObj.rowSourceId = ($('#source').val()  == 'All' || $('#source').val()  == '') ? -1 : $('#source').val() ;
					filteredAnalyticsDetailsObj.rowSourceTopic = ($('#topics').val()  == 'All' || $('#topics').val()  == '') ? null : $('#topics').val() ;
					filteredAnalyticsDetailsObj.startDt = $('#datepicker_start_entity').find('input').val();
					filteredAnalyticsDetailsObj.endDt = $('#datepicker_end_entity').find('input').val();
					filteredAnalyticsDetailsObj.sentiment = ($('#sentiment').val()  == 'All' || $('#sentiment').val()  == '') ? null : $('#sentiment').val() ;
					filteredAnalyticsDetailsObj.detailsTabName = 'sentiment_analysis';
					$scope.$parent.fn_filtered_source_date_analytics_details(filteredAnalyticsDetailsObj);
					 
					filteredSocialListeningObj.accountId = authId; 
					filteredSocialListeningObj.rowSourceId = ($('#source').val()  == 'All' || $('#source').val()  == '') ? -1 : $('#source').val() ;
					filteredSocialListeningObj.rowSourceTopic = ($('#topics').val()  == 'All' || $('#topics').val()  == '') ? null : $('#topics').val() ;
					filteredSocialListeningObj.startDt = $('#datepicker_start_entity').find('input').val();
					filteredSocialListeningObj.endDt = $('#datepicker_end_entity').find('input').val();
					filteredSocialListeningObj.sentiment = ($('#sentiment').val()  == 'All' || $('#sentiment').val()  == '') ? null : $('#sentiment').val() ;
					filteredSocialListeningObj.detailsTabName = 'social_listening';
					$scope.$parent.fn_filtered_source_date_analytics_details(filteredSocialListeningObj);
				}
				
			}	
		}
	});

})(window.angular);