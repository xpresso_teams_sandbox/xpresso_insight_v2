angular.module('myApp')
.directive('download', function(){
return {
restrict: 'E',
controller: 'DownloadCtrl',
templateUrl: 'myApp/Download.html'
};
});