// this file contains controller for functionalities of adding details in addinfo page
(function(angular) {
	
	angular.module('app').controller('AddDetailsController', function ($scope, $http, $location){
		$scope.$parent.pageLoad = false;
		$scope.$emit('eventName', { message: "Please complete the configuration!" });
		
		if(authData.split('$')[5] == 1){
			$location.path( "/" );
		}
		$scope.userInfo = {};
		$scope.topicProfiles = [
			{
				profileType : 'Social Profile',
				profileDetails : [1,2] 
			},
			{
				profileType : 'Mainstream Media',
				profileDetails : [3]
			}/*,
			{
				profileType : 'Other Relevant Source',
				profileDetails : [9,10,11,12] 
			},*/
		];
		
		$scope.sourceFilter = function(topicProfile) {
			return function(detail) {
				return topicProfile.profileDetails.indexOf(Number(detail.sourceTypeId)) != -1?true:false;
			}
		}
		
		$scope.socialProfiles = [
			{name: 'Facebook', sourceTypeId: '1'},
			{name: 'Twitter', sourceTypeId: '2'} 
		];
		
		$scope.mainstreamMedias = [
			{name: 'Economic Times', sourceTypeId: '3'} 
		];
		
		$scope.otherSources = [
			{name: 'Source 1', sourceTypeId: '9'},
			{name: 'Source 2', sourceTypeId: '10'},
			{name: 'Source 3', sourceTypeId: '11'},
			{name: 'Source 4', sourceTypeId: '12'}
		];
		
		
		
		$scope.tempCompanysocialProfiles = angular.copy($scope.socialProfiles);
		$scope.tempCompanymainstreamMedias = angular.copy($scope.mainstreamMedias);
		$scope.tempCompanyotherSources = angular.copy($scope.otherSources);
		
		$scope.tempComsocialProfiles = angular.copy($scope.socialProfiles);
		$scope.tempCommainstreamMedias = angular.copy($scope.mainstreamMedias);
		$scope.tempComotherSources = angular.copy($scope.otherSources);
		
		//Add Users
		$scope.userInfo.usersList = [];
		$scope.noUser = 0;
		
		$scope.userListing = false;
		$scope.addUser = function(){
			
			var ud = {
				userName: $scope.userName,
				userEmail: $scope.userEmail
			};
			
			
		
			if (!$scope.userName && !$scope.userEmail){
				$window.alert ("Please enter User's Name & Email");
			} else if (!$scope.userName){
				$window.alert ("Please enter User's Name");
			} else if (!$scope.userEmail){
				$window.alert ("Please enter User's Email");
			} else {
				
				$scope.userListing = true;
				$scope.userInfo.usersList.push(ud);
				
				$scope.noUser++;
				if ($scope.noUser >= 3){ //Disabled click if exceed limits 
					$('#addUserbtn').attr('disabled', '');
				}
				
				/*$scope.addUserInfo.userName.$setPristine(true);
				$scope.addUserInfo.userEmail.$setPristine(true);*/
				$scope.userName = null;
				$scope.userEmail = null;
			}
		};
		
		//Delete Users
		$scope.deleteUser = function(index){ 
			$scope.userInfo.usersList.splice(index, 1);
			$('#addUserbtn').removeAttr('disabled');
			$scope.noUser--;
		};
		
		
		//Add Keywords
		/*$scope.userInfo.keywords = [];
		$scope.noKeyword = 0;
		
		$scope.addKeywords = function () {
			var keyword = {
				keyword: $scope.keyword
			};
			
			if (!$scope.keyword){
				alert ("Please enter Keyword");
			}else {
				$scope.userInfo.keywords.push(keyword);
				
				$scope.noKeyword++;
				if ($scope.noKeyword >= 3){ //Disabled click if exceed limits 
					$('#addKeywordbtn').attr('disabled', '');
				}
				
				$scope.addUserInfo.keyword.$setPristine(true);
				$scope.keyword = null;
			}
		};*/
		
		//Delete Keywords
		$scope.deleteKeyword = function(index){ //Delete existing keywords
			$scope.userInfo.keywords.splice(index, 1);
			$('#addKeywordbtn').removeAttr('disabled');
			$scope.noKeyword--;
		};
		
		
		//Add Social Platform
		$scope.usersocialProfiles = [];

		$scope.socialListing = false;
		
		
		$scope.addSocialProfile = function () {
			$scope.socialListing = true;
			$scope.socialProfile = {
				name: $scope.selectedSocialProfile.name,
				sourceTypeId: $scope.selectedSocialProfile.sourceTypeId
			};
			
			$scope.usersocialProfiles.push($scope.socialProfile);
			$scope.tempCompanysocialProfiles.splice($scope.tempCompanysocialProfiles.indexOf($scope.selectedSocialProfile), 1);
			$scope.selectedSocialProfile = '';
			
		};
		
		
		//Add Mainstream Media
		$scope.usermainstreamMedias = [];
		
		$scope.mainstreamListing = false;
		
		$scope.addMainstreamMedia = function () {
			$scope.mainstreamListing = true;
			$scope.mainstream = {
				name: $scope.selectedMainstreamMedia.name,
				sourceTypeId: $scope.selectedMainstreamMedia.sourceTypeId
			};
			
			$scope.usermainstreamMedias.push($scope.mainstream);
			$scope.tempCompanymainstreamMedias.splice($scope.tempCompanymainstreamMedias.indexOf($scope.selectedMainstreamMedia), 1);
			$scope.selectedMainstreamMedia = '';
		};
		
		
		//Add Other Relevant Source
		$scope.userotherSources = [];
		
		$scope.othersourcesListing = false;
		
		$scope.addOtherSources = function () {
			$scope.othersourcesListing = true;
			$scope.othersource = {
				name: $scope.selectedOtherSource.name,
				sourceTypeId: $scope.selectedOtherSource.sourceTypeId
			};
			
			$scope.userotherSources.push($scope.othersource);
			$scope.tempCompanyotherSources.splice($scope.tempCompanyotherSources.indexOf($scope.selectedOtherSource), 1);
			$scope.selectedOtherSource = '';
		};
		
		
		//Add Competitors
		$scope.userInfo.competitorsList = [];
		$scope.ComsocialProfiles = [];
		$scope.CommainstreamMedias = [];
		$scope.ComotherSourceslist = [];
		
		$scope.addCSocialProfile = function () {
			$scope.ComsocialProfile = {
				name: $scope.selectedComSocialProfile.name,
				sourceTypeId: $scope.selectedComSocialProfile.sourceTypeId
			};
			$scope.ComsocialProfiles.push($scope.ComsocialProfile);
			$scope.tempComsocialProfiles.splice($scope.tempComsocialProfiles.indexOf($scope.selectedComSocialProfile), 1);
			$scope.selectedComSocialProfile = '';
		};
		
		$scope.addCMainstreamMedia = function () {
			$scope.Commainstream = {
				name: $scope.selectedComMainstreamMedia.name,
				sourceTypeId: $scope.selectedComMainstreamMedia.sourceTypeId
			};
			$scope.CommainstreamMedias.push($scope.Commainstream);
			$scope.tempCommainstreamMedias.splice($scope.tempComsocialProfiles.indexOf($scope.tempCommainstreamMedias), 1);
			$scope.selectedComMainstreamMedia = '';
		};
		
		$scope.addCOtherSources = function () {
			$scope.ComotherSources = {
				name: $scope.selectedComOtherSource.name,
				sourceTypeId: $scope.selectedComOtherSource.sourceTypeId
			};
			$scope.ComotherSourceslist.push($scope.ComotherSources);
			$scope.tempComotherSources.splice($scope.tempComotherSources.indexOf($scope.selectedComOtherSource), 1);
			$scope.selectedComOtherSource = '';
		};
		
		$scope.noCompetitor = 0;
		$scope.comListing = false;
		
		$scope.addCompetitors = function(){
			
			if(!$scope.competitorName){
				$window.alert ('Please enter Competitor Details');
			} else {
				$scope.comListing = true;
				
				var competitor = {
					competitorName: $scope.competitorName
				};
				
				
				competitor.competitorSources = [];
				
				competitor.competitorSources = competitor.competitorSources.concat($scope.ComsocialProfiles,$scope.CommainstreamMedias,$scope.ComotherSourceslist);
				$scope.userInfo.competitorsList.push(competitor);
				
				
				$scope.tempComsocialProfiles.splice($scope.tempComsocialProfiles.indexOf($scope.selectedComSocialProfile), 1);
				$scope.selectedComSocialProfile = '';
				
				$scope.tempCommainstreamMedias.splice($scope.tempCommainstreamMedias.indexOf($scope.selectedComMainstreamMedia), 1);
				$scope.selectedComMainstreamMedia = '';
				
				$scope.tempComotherSources.splice($scope.tempComotherSources.indexOf($scope.selectedComOtherSource), 1);
				$scope.selectedComOtherSource = '';
					
				$scope.noCompetitor++;
				if ($scope.noCompetitor >= 3){ //Disabled click if exceed limits 
					$('#addCompetitorbtn').attr('disabled', '');
				}
					
				$scope.competitorName = null;
				$scope.ComsocialProfiles = [];
				$scope.CommainstreamMedias = [];
				$scope.ComotherSourceslist = [];
				$scope.tempComsocialProfiles = angular.copy($scope.socialProfiles);
				$scope.tempCommainstreamMedias = angular.copy($scope.mainstreamMedias);
				$scope.tempComotherSources = angular.copy($scope.otherSources);
			}
			
		};
		
		//Delete Competitors
		$scope.deleteCompetitor = function(index){ 
			$scope.userInfo.competitorsList.splice(index, 1);
			$('#addCompetitorbtn').removeAttr('disabled');
			$scope.noCompetitor--;
		};
		//console.log($('#authEmail').val().trim());
		$scope.userInfo.adminEmail = $('#authEmail').val().trim();
		
		$scope.userInfo.sourcesDetailsList = [];
		$scope.addAllUsers = function(){  
			$scope.userInfo.sourcesDetailsList = $scope.userInfo.sourcesDetailsList.concat($scope.usersocialProfiles,$scope.usermainstreamMedias,$scope.userotherSources);
			console.log($scope.userInfo.sourcesDetailsList);
			var hasSource = 0;
			for(var i=0; i<$scope.userInfo.sourcesDetailsList.length;i++){
				if($scope.userInfo.sourcesDetailsList[i].sourceURL){
					console.log("url="+$scope.userInfo.sourcesDetailsList[i].sourceURL)
					hasSource = 1;
					break;
				}
			}
			if(!hasSource){
				$('#alertModal').modal("show");
		        $('#alertModal .modal-content').addClass('modal-warning');
		        $('#alertTxt').html("Please add atleast one source");
			}

			else{
				//var url = $('#addInfoUrl').val().trim();
				var data = $scope.userInfo;
			 	$http.post(addInfoUrl, data, $scope.configJSON).then(function(){
					console.log('Success');
					$scope.addinfosuccess = true;
					$scope.$emit('saveDetails', { message: "" });
				}, function(){
					console.log('Error');
				});
			}
		};
	});

})(window.angular);