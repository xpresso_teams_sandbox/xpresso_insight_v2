// this file contains controller for settings functionalities
(function(angular) {
	
	angular.module('app').controller('ReviewAnalysis', function ($scope,$window, $http, $filter, $routeParams, $location,$sce){
		$scope.$parent.activeMenu = 'reviewanalysis';
		$scope.$parent.latestNewsSection = "0";
		$scope.currentPage = 1;
		$scope.curTabID = 7;
		$scope.$parent.initialPageload = true;
		$scope.totalRecordCount = 0 ;
		$scope.selectedSource=1; // Default value for source
		$scope.showHide=1;
		$scope.selectedSentiment="100"; // Default value for Sentiment
		$scope.selectedCity=-1; // Default value for City
		// $scope.selectedLocality="ALL"; //Default value for Locality
		$scope.selectedLocality="All Localities"; // Default value for
													// Locality
		$scope.sortType = '-5';						// angular sort value for
													// Context Details table
													// initialized on feedback
													// column
		$scope.sortReverse  = false;				// angular sort for Context
													// Details table to toggle
													// between columns feedback
													// and complaint
		$scope.selectedStartDate="";
		$scope.selectedEndDate="";
		$scope.array_cities = [];
		
		$scope.reviewEntries = 10;
		$scope.authRole = authRole;
		// object for onload Deep Listening table table
		$scope.reviewPagination = {
				reviewCurrentPage: 1,
				reviewNewPage: 1
		};
		$scope.totalReviewCount = 0 ;
		
		$scope.mondayDate = moment(getMonday()).format("DD-MM-YYYY");
		$scope.LastPullingCurDate = moment(new Date()).format("DD-MM-YYYY");
		$scope.LastPullingDateforAppStore = moment(new Date()).subtract(1, 'days').format("DD-MM-YYYY");
		
		
		// getting list of sources at Loading time from tblrating_source_master
		// table
		$http.get(reviewAnalysisSourcesUrl+'?accountId='+authId).success( function(response) {
			$scope.review_analysis_sources = response.results.rows;
	    });
		
		
		//code for getting all ownership from db
		//$scope.array_ownerships=[[1,"Equity"],[2,"SFIPL"],[3,"DIL"],[4,"All"]];
		/*$http.get(getOwnershipsUrl,$scope.config).success( function(response) {
			alert(response);
			$scope.array_ownerships = response.results.rows;
	    });*/
		
	    // Daily snapshot code starts here
		$scope.sourceCrawlingStatus = "Live";
        $scope.legendTitleDateTimePrevious = $scope.$parent.yesterday.substring(0,10) +" 00:01 To "+ $scope.$parent.yesterday.substring(0,10) +" 23:59";
        $scope.legendTitleDateTimeCurrent = $scope.$parent.today.substring(0,10) +" 00:01 To "+ $scope.$parent.today;
        $scope.selectedSourceForSnapshot=1;
		$scope.sourceCrawlingStatus='Once in a Day';
        /* Date range selector starts */
        $scope.periodicPrevious = {
               startDate: moment().subtract(1, "days"),
               endDate: moment().subtract(1, "days"),
               format: "DD-MM-YYYY"
           };
        $scope.periodicCurrent = {
               startDate: moment(),
               endDate: moment(),
               format: "DD-MM-YYYY"
           };
        
        $scope.opts = {
            locale: {
                applyClass: 'btn-green',
                applyLabel: "Apply",
                fromLabel: "From",
                format: "DD-MM-YYYY",
                toLabel: "To",
                cancelLabel: 'Cancel',
                customRangeLabel: 'Custom range'
            },
            ranges: {
                'Last 1 Days': [moment().subtract(1, 'days'), moment()],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()]
            }
        };
        
/*
 * $('input[name="datefilter"]').daterangepicker({ autoUpdateInput: false,
 * locale: { cancelLabel: 'Clear' } });
 * 
 * $('input[name="datefilter"]').on('apply.daterangepicker', function(ev,
 * picker) { $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' +
 * picker.endDate.format('MM/DD/YYYY')); });
 * 
 * $('input[name="datefilter"]').on('cancel.daterangepicker', function(ev,
 * picker) { $(this).val(''); });
 */
        
        
        // Method for Snapshot display in Analytics Page.
        $scope.snapshotPeriodicSourceClick = function(selectedSnapSource,crawlingStatus,clickIdentifier){
			// Setting the source selection, it binding the source id into the
			// variable.
        	$scope.sourceCrawlingStatus = crawlingStatus;
        	$scope.selectedSourceForSnapshot = selectedSnapSource;
        	// Checking if selected source is GES, tehn reinitilize the date
			// range to 2 days back.
        	if(clickIdentifier == 'sourceClicked'){
        		if(selectedSnapSource == "1"){
            		$scope.periodicPrevious = {
    	                   startDate: moment().subtract(1, "days"),
    	                   endDate: moment().subtract(1, "days"),
    	                   format: "DD-MM-YYYY"
    	               };
    		        $scope.periodicCurrent = {
    	                   startDate: moment(),// .subtract(1, "days"),
    	                   endDate: moment(),// .subtract(1, "days"),
    	                   format: "DD-MM-YYYY"
    	               };
            	} 
        		else if(selectedSnapSource == "5"){ // This is for handling App
													// Store data which is 1 day
													// lag
        			$scope.periodicPrevious = {
     	                   startDate: moment().subtract(2, "days"),
     	                   endDate: moment().subtract(2, "days"),
     	                   format: "DD-MM-YYYY"
     	               };
     		        $scope.periodicCurrent = {
     		        		startDate: moment().subtract(1, "days"),
      	                   endDate: moment().subtract(1, "days"),
     	                   format: "DD-MM-YYYY"
     	               };
        		}
        		else {
            		$scope.periodicPrevious = {
    	                   startDate: moment().subtract(7, "days"),
    	                   endDate: moment().subtract(7, "days"),
    	                   format: "DD-MM-YYYY"
    	               };
    		        $scope.periodicCurrent = {
    	                   startDate: moment().subtract(1, "days"),
    	                   endDate: moment().subtract(1, "days"),
    	                   format: "DD-MM-YYYY"
    	               };
            	}
        	}
        	
        	
        	// Logic for display selected date range on top of snapshot boxes
        	setTimeout(function(){ 
				var strDate = new Date(); 
				$scope.legendTitleDateTimeCurrent = $('#periodicCurrent').val().split(" - ")[0].substring(0,10) +" 00:01 To "+ $('#periodicCurrent').val().split(" - ")[1].substring(0,10) + " " + strDate.getHours() + ":" + strDate.getMinutes() ;
		        $scope.legendTitleDateTimePrevious = $('#periodicPrevious').val().split(" - ")[0].substring(0,10) +" 00:01 To "+ $('#periodicPrevious').val().split(" - ")[1].substring(0,10) +" 23:59";
				
				var data = $.param({
					"selectedSourceId":$scope.selectedSourceForSnapshot,
					"accountId": authId,
					"inCurrentStartEndDt" : $('#periodicCurrent').val(),
					"inPreviousStartEndDt": $('#periodicPrevious').val()
		        });
				$http.post(socialReviewSnapshotdataURL, data, $scope.config).then(function(response){
			    	$scope.snapshotPeriodicResponse = response;
			    	$scope.snapshotPeriodicData = $scope.snapshotPeriodicResponse.data.results.rows;
			    	
			    	if($scope.snapshotPeriodicData.length == 2){
			    		$scope.feedbackPrevious = $scope.snapshotPeriodicData[0][0];
				    	$scope.complainPrevious = $scope.snapshotPeriodicData[0][1];
				    	$scope.feedbackCurrent = $scope.snapshotPeriodicData[1][0];
				    	$scope.complainCurrent = $scope.snapshotPeriodicData[1][1];
			    	} else if($scope.snapshotPeriodicData.length == 0){
			    		$scope.feedbackPrevious = 0;
				    	$scope.complainPrevious = 0;
				    	$scope.feedbackCurrent = 0;
				    	$scope.complainCurrent = 0;
			    	} else if($scope.snapshotPeriodicData.length == 1){
			    		if($scope.snapshotPeriodicData[0][4] == "previous"){
			    			$scope.feedbackPrevious = $scope.snapshotPeriodicData[0][0];
					    	$scope.complainPrevious = $scope.snapshotPeriodicData[0][1];
					    	$scope.feedbackCurrent = 0;
					    	$scope.complainCurrent = 0;
			    		} else if($scope.snapshotPeriodicData[0][4] == "current"){
			    			$scope.feedbackPrevious = 0;
					    	$scope.complainPrevious = 0;
					    	$scope.feedbackCurrent = $scope.snapshotPeriodicData[0][0];
					    	$scope.complainCurrent = $scope.snapshotPeriodicData[0][1];
				    	}
			    	}
					$scope.snapshotPeriodicDataLabels = $scope.snapshotPeriodicResponse.data.results.column_names;
					$scope.ifSnapshotLoaded = true;
				});
        	}, 500);
		}
		
        // Calling snapshot on page load with a time delay.
        setTimeout(function(){ 
			$scope.snapshotPeriodicSourceClick($scope.selectedSourceForSnapshot,$scope.sourceCrawlingStatus);
		}, 2000);
        // Snapshot view code completed.
        
        
        // function for disabling dates in angular datetimepicker - set date
		// picker date for last 30 days.
		$scope.dateRangeValidationReviewAnalysis = function($view, $dates, $leftDate, $upDate, $rightDate) {
		  var activeDate;
		  $scope.dateRangeEnd = new Date();
		  $scope.dateRangeStart = new Date();
		  if ($scope.dateRangeStart) {
			  activeDate = moment($scope.dateRangeStart).subtract(30, $view).add(1, 'minute');
		    for (var i = 0; i < $dates.length; i++) {
		      if ($dates[i].localDateValue() <= activeDate.valueOf())
		        $dates[i].selectable = false;            
		    } 
		  }
		  if ($scope.dateRangeEnd) {
		    activeDate = moment($scope.dateRangeEnd).add(0, 'day');
		    for (var incDates = 0; incDates < $dates.length; incDates++) {
		      if ($dates[incDates].localDateValue() >= activeDate.valueOf())
		    	  $dates[incDates].selectable = false;
		    }
		  }
		}
	    
	    
		// Get selected tab id
		var dataKpi = $.param({
			"inTabId": $scope.curTabID
        });
		
		$scope.tabMetaData = null;
		// Loading list of kpis in Review Analysis Tab
		$http.post(kpinameUrl, dataKpi, $scope.$parent.config).then(function(response){
			var tabResponse = response.data.results.rows;
			
			// Looping through all kpis from kpi_metadata table and displaying
			// it in the page.
			for (var i=0; i<tabResponse.length;i++) {
				if(tabResponse[i][0] == "fn_Review_Sentiment_Distribution" && tabResponse[i][1] == "y") {
					// calculating start date and end date on page load
					var date = new Date();
					date = new Date(date.valueOf() + (date.getTimezoneOffset() + authTimezoneOffset) * 60000);
					var end_date = $filter('date')(date, 'dd-MM-yyyy');
					date.setDate(date.getDate() - 1);
					var start_date = $filter('date')(date, 'dd-MM-yyyy');
					console.log(start_date+'||||||'+end_date);
					
					$scope.selectedStartDate=toGMTDate(start_date);
					$scope.selectedEndDate=toGMTDateReviewEndDate(end_date);
					$scope.fn_Review_Sentiment_Distribution($scope.reviewPagination.reviewNewPage,'from-others');
				}
				else if(tabResponse[i][1]== "n") {
					$('#'+tabResponse[i][2]).hide();
				}
		    }
		});
		
		// Function to load Review Analysis data into table at loading time and
		// GO click
		
		$scope.selectedOwnership = "4";
		$scope.fn_Review_Sentiment_Distribution = function(reviewNewPage,clickIdentifier){
			$scope.showHide=$scope.selectedSource;
			
			if(clickIdentifier!='from-pagination-click'){
				$scope.reviewPagination = {
						reviewCurrentPage: 1
				};
			}
			//$scope.currentPage = 1;
			
			if ($scope.selectedLocality instanceof Array) {
				var sLocality = $scope.selectedLocality[0];
				} else {
					var sLocality =$scope.selectedLocality;
				}
			
			
			reviewAnalysisObj.accountId=authId;
			reviewAnalysisObj.sourceId=$scope.selectedSource;
			reviewAnalysisObj.sentiment = $scope.selectedSentiment;
			reviewAnalysisObj.startDt =  $scope.selectedStartDate;
			reviewAnalysisObj.endDt = $scope.selectedEndDate;
			reviewAnalysisObj.cityId = ($scope.selectedCity == "" || $scope.selectedCity == null) ? -1 : $scope.selectedCity;
			//reviewAnalysisObj.locality = ($scope.selectedLocality == "All Localities") ? -1 : $scope.selectedLocality; //
			reviewAnalysisObj.locality = (sLocality == "All Localities" || sLocality == "" || sLocality == null) ? -1 : sLocality ; //
			reviewAnalysisObj.ownershipId=$scope.selectedOwnership;
			
			var arrangedData = [];
						
		/*	var data = $.param({
				"accountId": reviewAnalysisObj.accountId, 
		        "sourceId": reviewAnalysisObj.sourceId,
		        "cityId":reviewAnalysisObj.cityId,
		        "locality":reviewAnalysisObj.locality,
	            "sentiment":reviewAnalysisObj.sentiment,
		        "startDt": reviewAnalysisObj.startDt,
		        "endDt" : reviewAnalysisObj.endDt,
		        "perPageEntries" : $scope.reviewEntries,
	            "newPage" : reviewNewPage,
	            "ownershipId" : reviewAnalysisObj.ownershipId
	        });*/
			
			
			
			var data = {
				"accountId": reviewAnalysisObj.accountId, 
		        "sourceId": reviewAnalysisObj.sourceId,
		        "cityId":reviewAnalysisObj.cityId,
		        "locality":reviewAnalysisObj.locality,
	            "sentiment":reviewAnalysisObj.sentiment,
		        "startDt": reviewAnalysisObj.startDt,
		        "endDt" : reviewAnalysisObj.endDt,
		        "perPageEntries" : $scope.reviewEntries,
	            "newPage" : reviewNewPage,
	            "ownershipId" : reviewAnalysisObj.ownershipId
	        };
			
			
			
			
			$http.post(reviewAnalysisDataUrl, JSON.stringify(data), $scope.$parent.config).then(function(response){
				
				var data = response.data.results.rows;
				
				for(var inc = 0; inc < data.length; inc++){
					var ratingValue = data[inc][5];
					// Used for CHAMPSPRO
					var aspectMap = data[inc][6]; 
					var sentiment = data[inc][3].toLowerCase();
					
					if(ratingValue === null || ratingValue == 'NA'){
						ratingValue = "0";
					}
					/*
					 * As the count present in 8th index in every resultset,
					 * Here we are getting the total pages from resultset and
					 * initilazing the variable
					 */
					if(inc == 0){
						$scope.totalReviewCount = data[inc][8];  
					}
					// Preparing HTML for show Star,Half star nad full start
					// using rating int val
					var starHTML = $scope.fn_convert_rating_into_star(ratingValue);
					data[inc].push(starHTML);
					// Added for handel black rating value in Green yellow box
					data[inc].push(ratingValue); 
					
					if(aspectMap === null || aspectMap == 'NA' || aspectMap.length < 1){
						aspectMap = "na";
					}
					
					// Preparing HTML for display of Business Aspect(champspro)
					var strAspectHTML = $scope.fn_convert_aspectlist_into_CHAMPSPRO(aspectMap,sentiment);
					data[inc].push(strAspectHTML);
					
					arrangedData.push(data[inc]);
				}
				$scope.review_analysis = arrangedData;
			});
		}
		
		// for getting cities against source at loading time
		// $scope.array_cities = [];
	    $http.get(cityagainstsourceURL+'?accountId='+authId+'&selectedsource='+$scope.selectedSource).success( function(response) {
	    	$scope.array_cities = response.results.rows;
	    	$scope.array_cities.unshift([-1,"All Cities"]);
	    });
	    
	    // for getting locality against source at loading time
	    $scope.localities = [];
	    $http.get(localityagainstsourceURL+'?accountId='+authId+'&selectedsource='+$scope.selectedSource).success( function(response) {
	    	$scope.localities = response.results.rows;
	    	$scope.localities.unshift(["All Localities"]);
	    });
	    
	    
	    $scope.resetAllSelectors=function(){
	    	$scope.selectedCity=-1; // Default value for City
			$scope.selectedLocality="All Localities";
			$scope.getCities();
			$scope.selectedSentiment="100";
			
			// Change date according to source selection
			var date = new Date();
			var mp_ff_date = new Date(($scope.mondayDate.substring(6,10)+'-'+$scope.mondayDate.substring(3,6)+$scope.mondayDate.substring(0,2)+$scope.mondayDate.substring(10,$scope.mondayDate.length)).replace(/-/g, "/"));
			var appStoreEnddate = new Date();
			if($scope.selectedSource == "1"){
				date = new Date(date.valueOf() + (date.getTimezoneOffset() + authTimezoneOffset) * 60000);
				var end_date = $filter('date')(date, 'dd-MM-yyyy');
				date.setDate(date.getDate() - 1);
				var start_date = $filter('date')(date, 'dd-MM-yyyy');
			} else if($scope.selectedSource == "2" || $scope.selectedSource == "3"){
				
				mp_ff_date = new Date(mp_ff_date.valueOf() + (mp_ff_date.getTimezoneOffset() + authTimezoneOffset) * 60000);
				mp_ff_date.setDate(mp_ff_date.getDate());
				var end_date = $filter('date')(mp_ff_date, 'dd-MM-yyyy');
				mp_ff_date.setDate(mp_ff_date.getDate() - 6);
				var start_date = $filter('date')(mp_ff_date, 'dd-MM-yyyy');
			} else if($scope.selectedSource == "5"){
				appStoreEnddate = new Date(date.valueOf() + (date.getTimezoneOffset() + authTimezoneOffset) * 60000);
				appStoreEnddate.setDate(date.getDate() - 1);
				var end_date = $filter('date')(appStoreEnddate, 'dd-MM-yyyy');
				date.setDate(date.getDate() - 2);
				var start_date = $filter('date')(date, 'dd-MM-yyyy');
			}
			
			$scope.selectedStartDate=toGMTDate(start_date);
			$scope.selectedEndDate=toGMTDateReviewEndDate(end_date);
	    }
	    
		// Function for getting list of cities against source selection
		$scope.getCities = function(){
			$scope.selectedCity=NULL_VALUE; 
			$scope.array_cities = [];
	    	
	    	$http.get(cityagainstsourceURL+'?accountId='+authId+'&selectedsource='+$scope.selectedSource).success( function(response) {
		    	$scope.array_cities = response.results.rows;
		    	$scope.array_cities.unshift([-1,"All Cities"]);
		    });
	    }
		
		// Function to update overall sentiment when user changes it from deep
		// listing table
		$scope.updateReviewAnalysisOverallSentiment = function(sentimentID,reviewID,originalSentiment,postText,postDateTime){
			swal("Are you sure you want to update the displayed sentiment ?", {
				  buttons: {
				    cancel: "No",
				    confirm: {
				      text: "Yes",
				      value: "confirm",
				    }			  
				  },
				})
				.then((value) => {
					if(value == "confirm"){
	            		$scope.overAllSentiment = "Neutral";
	        			$scope.TxtColor = "#F6BC41";
	        			if(sentimentID == "1"){
	        				$scope.overAllSentiment = "Positive";
	        				$scope.TxtColor = "#8DC153";
	        			}else if(sentimentID == "-1"){
	        				$scope.overAllSentiment = "Negative";
	        				$scope.TxtColor = "#f55348";
	        			}
	        			
	        			if ($scope.selectedcompany instanceof Array) {
	        				  var cid = $scope.selectedcompany[0];
	        				} else {
	        				  var cid =$scope.selectedcompany;
	        				}
	        			
	        			var dataSentimentUpdater = $.param({
	        				"company_Id": authPrimaryCompanyID,
	        				"sentimentID": sentimentID,
	        				"reviewID":reviewID
	        	        });
	        			
        			$http.post(sentimentUpdaterUrlForReviewAnalysis, dataSentimentUpdater, $scope.config).then(function(response){
    					if(response.data == "1"){
    						$("#sentiment_"+reviewID).text($scope.overAllSentiment);
    						$("#sentiment_"+reviewID).css('color',$scope.TxtColor);
    						$("#tt_"+reviewID).css('color',$scope.TxtColor);

    						var requestResponse = $scope.fn_insert_update_sentiment(authPrimaryCompanyID,sentimentID,reviewID,originalSentiment,postText,originalSentiment,postText,postDateTime,authId);
    						if(requestResponse == "1"){
    							swal("", "Sentiment updated successfully", "success");
    						} else {
    							swal("", "Request Failed...Please try again", "error");
    						}
    					} 
	        		});
	             }
			});
		}

		$scope.fn_insert_update_sentiment = function (authPrimaryCompanyID,sentimentID,reviewID,originalSentiment,postText,originalSentiment,postText,postDateTime,authId){
			var dataSentimentUpdater = $.param({
				"company_Id": authPrimaryCompanyID,
				"sentimentID": sentimentID,
				"postID":reviewID,
				"originalSentiment": originalSentiment,
				"postText":postText,
				"postDateTime":postDateTime,
				"inAccountId":authId,
				"tabName":"ReviewAnalysis"
	        });
			
			var responseVal = 1;
			
			$http.post(sentimentInsertUpdaterUrl, dataSentimentUpdater, $scope.config).then(function(response){
					responseVal = response.data;
					if(responseVal == "1"){
						$("#sentiment_"+reviewID).text($scope.overAllSentiment);
						$("#sentiment_"+reviewID).css('color',$scope.TxtColor);
						$("#tt_"+reviewID).css('color',$scope.TxtColor);
					} 
			});
			
			return responseVal;
		}
		// Function for getting list of locality against source and city
		// selection
		$scope.getLocalities = function(){
		    $scope.localities = [];
		    if($scope.selectedCity > 0){ // when source changed but city is
											// not selected yet
			    $http.get(localityagainstsourcecityURL+'?accountId='+authId+'&selectedsource='+$scope.selectedSource+'&selectedcity='+$scope.selectedCity).success( function(response) {
			    	$scope.localities = response.results.rows;
			    	if($scope.localities.length == 0){
			    		$scope.localities = 0;
					}
			    	$scope.localities.unshift(["All Localities"]);
			    });
		    }
		    else{ // when source changed and city changed
			    $http.get(localityagainstsourceURL+'?accountId='+authId+'&selectedsource='+$scope.selectedSource).success( function(response) {
			    	$scope.localities = response.results.rows;
			    	$scope.localities.unshift(["All Localities"]);
			    });   
		    }    
	    }
		
		// Parsing HTML to render
		$scope.toTrustedHTML = function (html) {
			return $sce.trustAsHtml(html);
		};
		
		$scope.toTrustedHTML2 = function (html) {
			return $sce.trustAsHtml(html);
		};
		
		// Convert Rating value into Stars
		$scope.fn_convert_rating_into_star = function(ratingValue){
			var arrRatingValue = ratingValue.toString().split(".");
			var dynamicHTML = "";
			if(arrRatingValue.length == 2){
				// Adding Full stars
				for(var incFullStart = 0; incFullStart<parseInt(arrRatingValue[0]); incFullStart++){
					dynamicHTML +='<i class="fa fa-star" aria-hidden="true"></i>';
				}
				
				// Adding half blank stars
				dynamicHTML +='<i class="fa fa-star-half-o" aria-hidden="true"></i>';
				
				var halfStar = 4 - parseInt(arrRatingValue[0]);
				
				// Adding blank stars
				for(var incBlankStart = 0; incBlankStart<halfStar; incBlankStart++){
					dynamicHTML +='<i class="fa fa-star-o" aria-hidden="true"></i>';
				}
				
			} else {
				// Adding Full stars
				for(var incFullStart2 = 0; incFullStart2<parseInt(arrRatingValue); incFullStart2++){
					dynamicHTML +='<i class="fa fa-star" aria-hidden="true"></i>';
				}
				
				var halfStar2 = 5 - parseInt(arrRatingValue);
				
				// Adding blank stars
				for(var incBlankStart2 = 0; incBlankStart2<halfStar2; incBlankStart2++){
					dynamicHTML +='<i class="fa fa-star-o" aria-hidden="true"></i>';
				}
			}
			return dynamicHTML;
		}
		
		// Convert Rating value into Stars
		$scope.fn_convert_aspectlist_into_CHAMPSPRO = function(aspectMap,sentiment){
			var dynamicHTML = "";
			var arrAspectValue = aspectMap.toString().split(",");
			var aspectObj = {
					CLE: "",
					HOS: "",
					ACC: "", 
					MAI: "", 
					PRO: "",
					SPE: "",
					PRI: "",
					OTH: "",
					NON: "",
					OVE: ""
				}
			
			for(var i = 0 ; i < arrAspectValue.length; i++){
				var firstThreeLetter = arrAspectValue[i].substr(0,3);
				if(firstThreeLetter == "CLE"){aspectObj.CLE = sentiment;}
				if(firstThreeLetter == "HOS"){aspectObj.HOS = sentiment;}
				if(firstThreeLetter == "ACC"){aspectObj.ACC = sentiment;}
				if(firstThreeLetter == "MAI"){aspectObj.MAI = sentiment;}
				if(firstThreeLetter == "HOS"){aspectObj.HOS = sentiment;}
				if(firstThreeLetter == "PRO"){aspectObj.PRO = sentiment;}
				if(firstThreeLetter == "SPE"){aspectObj.SPE = sentiment;}
				if(firstThreeLetter == "PRI"){aspectObj.PRI = sentiment;}
				if(firstThreeLetter == "OTH"){aspectObj.OTH = sentiment;}
				if(firstThreeLetter == "NON"){aspectObj.NON = sentiment;}
				if(firstThreeLetter == "Ove"){aspectObj.OVE = sentiment;}
			}

			dynamicHTML+= '<span title="Cleanliness" class="badge background-'+aspectObj.CLE+'">C</span> ';
			dynamicHTML+= '<span title="Hospitality" class="badge background-'+aspectObj.HOS+'">H</span> ';
			dynamicHTML+= '<span title="Accuracy" class="badge background-'+aspectObj.ACC+'">A</span> ';
			dynamicHTML+= '<span title="Maintenance" class="badge background-'+aspectObj.MAI+'">M</span> ';
			dynamicHTML+= '<span title="Product Quality" class="badge background-'+aspectObj.PRO+'">P</span> ';
			dynamicHTML+= '<span title="Speed & Delivery" class="badge background-'+aspectObj.SPE+'">S</span> ';
			dynamicHTML+= '<span title="Prices and Offers" class="badge background-'+aspectObj.PRI+'">PR</span> ';
			dynamicHTML+= '<span title="Others" class="badge background-'+aspectObj.OTH+'">Ot</span> ';
			dynamicHTML+= '<span title="Non-Relevant" class="badge background-'+aspectObj.NON+'">N</span> ';
			dynamicHTML+= '<span title="Overall" class="badge background-'+aspectObj.OVE+'">Ov</span> ';
			
			return dynamicHTML;
		}
		
		 // Asynchronous Setup for pagination
		$scope.reviewPageChanged = function(reviewNewPage) {
			$scope.fn_Review_Sentiment_Distribution(reviewNewPage,'from-pagination-click');
	    };
	    
		$scope.resetPage = function(){
		    $scope.currentPage = 1;
		}
		
		
	});

})(window.angular);