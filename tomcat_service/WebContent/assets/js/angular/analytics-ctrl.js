// this file contains controller for analytics functionalities
(function(angular) {
	
	/**
	 * Author Name: Urmi Saha
	 * Create Date: 02-11-2016
	 * Last Modified: Ashis Nayak | 03-11-2017
	 * Input: 
	 * Output: 
	 * Method Insight: controller for analytics functionalities
	 */
	angular.module('app').controller('AnalyticsController', function($scope, $http, $routeParams, $location, $filter){
		//$scope.intervalValue = "Daily";						// holds the interval value of Happiness Index Live initiated with 'Daily'
		$scope.$parent.activeMenu = 'analytics';				// holds the current tab of the navigation menu for keeping the tab highlighted with the active class, value is 'analytics when analytics/Explore page is shown as this controller is called'
		$scope.$parent.curTabID = "1";
		$scope.$parent.latestNewsSection = "0";
		
		$scope.$parent.snapshotActiveTab = "Daily-snapshot-wapper";
		$scope.$parent.fn_snapshot_filter_new(0); 				// calick on daily snapshot on page load
		$scope.selectedContextMonthly = NULL_VALUE;				// holds the selected context in Competitive Happiness Index initiated with 'All Contexts'
		$scope.$parent.sentimentalSource = NULL_VALUE;			// INITIALIZATION in master-ctrl.js : initiated with 'All Sources'
		$scope.$parent.advocateSource = ALL_SOURCES;			// INITIALIZATION in master-ctrl.js : initiated with 'All Sources'
		$scope.$parent.selectedContextMonthly = NULL_VALUE;
		$scope.$parent.selectedSource = NULL_VALUE;	
		$scope.$parent.selectedSentiment="All";
		$scope.$parent.sentimentalRegion = ALL_REGIONS;			// INITIALIZATION in master-ctrl.js : repeatedly made blank so that value is not retained when transversed from other page
		$scope.$parent.sentimentalCity = BLANK_STRING;			// INITIALIZATION in master-ctrl.js : repeatedly made blank so that value is not retained when transversed from other page
		$scope.$parent.sentimentalStore = NULL_VALUE;			// INITIALIZATION in master-ctrl.js : repeatedly made blank so that value is not retained when transversed from other page
		$scope.$parent.sentimentalTopic = NULL_VALUE;			// INITIALIZATION in master-ctrl.js : repeatedly made blank so that value is not retained when transversed from other page
		
		/**
		 * The following block of code determines the state of the application
		 * and redirects the application to the required page:
		 * if user has not yet filled the addinfo page: redirect to addinfo page,
		 * if user has filled addinfo page, but data is not yet ready: redirect to gettingdata page
		 * if user has filled the details and data is ready, call functions of the present page (Analytics/Explore page).
		 */
		if(authData.split('$')[5] == 0){
			$scope.addinfo = true;
			$location.path( "/addinfo" );
		}
		else if(authData.split('$')[4] == 0)
			$location.path( "/gettingdata" );
		else{
			socialListeningObj.accountId = authId;
			socialListeningObj.detailsTabName = 'social_listening';
			
			filteredAnalyticsDetailsObj.accountId = authId;
			filteredAnalyticsDetailsObj.detailsTabName = WORD_CLOUD;
			
			/**
			 * This conditional statement determines whether analytics page is hit by
			 * View button in dashboard
			 * or by
			 * clicking on the navigation bar
			 */
			if($routeParams.source && $routeParams.topic){
				$scope.authRole = authRole;
				$scope.selectedTopic = $routeParams.topic;
				$scope.selectedSource = Number($routeParams.source);
				$scope.analyticsFrom = $scope.$parent.analyticsFrom;
				$scope.analyticsTo = $scope.$parent.analyticsTo;
				
				entityDetailsObj.rowSourceId = Number($routeParams.source);
				entityDetailsObj.rowSourceTopic = $routeParams.topic;
				entityDetailsObj.timeInterval = 1;
				entityDetailsObj.selectedInterval = viralityComparissionObj.selectedInterval;
				entityDetailsObj.startDt = toGMTDate($scope.analyticsFrom);
				entityDetailsObj.endDt = toGMTDate($scope.analyticsTo);
				$scope.fn_source_entity_details(entityDetailsObj);
							
				socialListeningObj.sourceId = Number($routeParams.source); 
				socialListeningObj.sourceTopic = $routeParams.topic;
				socialListeningObj.timeInterval = 1;
				socialListeningObj.startDt = toGMTDate($scope.analyticsFrom);
				socialListeningObj.endDt = toGMTDate($scope.analyticsTo);
				$scope.fn_deep_listening(socialListeningObj);
				
				$scope.fn_word_cloud_filtered();
				
				filteredAnalyticsDetailsObj.rowSourceId = ($scope.selectedSource == 'All' || $scope.selectedSource == '') ? -1 : $scope.selectedSource;
				filteredAnalyticsDetailsObj.rowSourceTopic = ($scope.selectedTopic  == 'All Contexts' || $scope.selectedTopic  == '') ? null : String ($scope.selectedTopic);
				filteredAnalyticsDetailsObj.startDt =  toGMTDate($scope.analyticsFrom);
				filteredAnalyticsDetailsObj.endDt = toGMTDate($scope.analyticsTo);
				$scope.fn_filtered_source_date_analytics_details(filteredAnalyticsDetailsObj);
			}else{
				// Daily snapshot code starts here 
				 $scope.selectedSourceForSnapshot=1;
				$scope.sourceCrawlingStatus = "Live";
		        $scope.legendTitleDateTimePrevious = $scope.$parent.yesterday.substring(0,10) +" 00:01 To "+ $scope.$parent.yesterday.substring(0,10) +" 23:59";
		        $scope.legendTitleDateTimeCurrent = $scope.$parent.today.substring(0,10) +" 00:01 To "+ $scope.$parent.today;
		        
		        /* Date range selector starts */
		        $scope.periodicPrevious = {
	                   startDate: moment().subtract(1, "days"),
	                   endDate: moment().subtract(1, "days"),
	                   format: "DD-MM-YYYY"
	               };
		        $scope.periodicCurrent = {
	                   startDate: moment(),
	                   endDate: moment(),
	                   format: "DD-MM-YYYY"
	               };
		        
		        $scope.opts = {
		            locale: {
		                applyClass: 'btn-green',
		                applyLabel: "Apply",
		                fromLabel: "From",
		                format: "DD-MM-YYYY",
		                toLabel: "To",
		                cancelLabel: 'Cancel',
		                customRangeLabel: 'Custom range',
		                startdate:moment().subtract(6, 'days'),
		                enddate: moment(),
		            },
		            ranges: {
		                'Last 1 Days': [moment().subtract(1, 'days'), moment()],
		                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		                'Last 30 Days': [moment().subtract(29, 'days'), moment()]
		            }
		        };

		        
		        //Method for Snapshot display in Analytics Page.
		        $scope.snapshotPeriodicSourceClick = function(selectedSnapSource,crawlingStatus,clickIdentifier){
					//Setting the source selection, it binding the source id  into the variable.
		        	//fn_periodic_onChange_date_Set(selectedSnapSource,clickIdentifier);
		        	$scope.sourceCrawlingStatus = crawlingStatus;
		        	$scope.selectedSourceForSnapshot = selectedSnapSource;
		        	//Checking if selected source is GES, tehn reinitilize the date range to 2 days back.
		        	if(clickIdentifier == 'sourceClicked'){
			        	if(selectedSnapSource == "5"){
			        		$scope.periodicPrevious = {
				                   startDate: moment().subtract(3, "days"),
				                   endDate: moment().subtract(3, "days"),
				                   format: "DD-MM-YYYY"
				               };
					        $scope.periodicCurrent = {
				                   startDate: moment().subtract(2, "days"),
				                   endDate: moment().subtract(2, "days"),
				                   format: "DD-MM-YYYY"
				               };
			        	} else {
			        		$scope.periodicPrevious = {
				                   startDate: moment().subtract(1, "days"),
				                   endDate: moment().subtract(1, "days"),
				                   format: "DD-MM-YYYY"
				               };
					        $scope.periodicCurrent = {
				                   startDate: moment(),
				                   endDate: moment(),
				                   format: "DD-MM-YYYY"
				               };
			        	}
		        	}
		        	
		        	//Logic for display selected date range on top of  snapshot boxes
		        	setTimeout(function(){ 
						var strDate = new Date(); 
						$scope.legendTitleDateTimeCurrent = $('#periodicCurrent').val().split(" - ")[0].substring(0,10) +" 00:01 To "+ $('#periodicCurrent').val().split(" - ")[1].substring(0,10) + " " + strDate.getHours() + ":" + strDate.getMinutes() ;
				        $scope.legendTitleDateTimePrevious = $('#periodicPrevious').val().split(" - ")[0].substring(0,10) +" 00:01 To "+ $('#periodicPrevious').val().split(" - ")[1].substring(0,10) +" 23:59";
						
						//$scope.selectedSourceForSnapshot = selectedSnapSource;
						var data = $.param({
							"selectedSourceId":$scope.selectedSourceForSnapshot,
							"accountId": authId,
							"inCurrentStartEndDt" : $('#periodicCurrent').val(),
							"inPreviousStartEndDt": $('#periodicPrevious').val()
				        });
						$http.post(socialPeriodicSnapshotdataURL, data, $scope.config).then(function(response){
					    	$scope.snapshotPeriodicResponse = response;
					    	$scope.snapshotPeriodicData = $scope.snapshotPeriodicResponse.data.results.rows;
					    	
					    	if($scope.snapshotPeriodicData.length == 2){
					    		$scope.feedbackPrevious = $scope.snapshotPeriodicData[0][0];
						    	$scope.complainPrevious = $scope.snapshotPeriodicData[0][1];
						    	$scope.feedbackCurrent = $scope.snapshotPeriodicData[1][0];
						    	$scope.complainCurrent = $scope.snapshotPeriodicData[1][1];
					    	} else if($scope.snapshotPeriodicData.length == 0){
					    		$scope.feedbackPrevious = 0;
						    	$scope.complainPrevious = 0;
						    	$scope.feedbackCurrent = 0;
						    	$scope.complainCurrent = 0;
					    	} else if($scope.snapshotPeriodicData.length == 1){
					    		if($scope.snapshotPeriodicData[0][4] == "previous"){
					    			$scope.feedbackPrevious = $scope.snapshotPeriodicData[0][0];
							    	$scope.complainPrevious = $scope.snapshotPeriodicData[0][1];
							    	$scope.feedbackCurrent = 0;
							    	$scope.complainCurrent = 0;
					    		} else if($scope.snapshotPeriodicData[0][4] == "current"){
					    			$scope.feedbackPrevious = 0;
							    	$scope.complainPrevious = 0;
							    	$scope.feedbackCurrent = $scope.snapshotPeriodicData[0][0];
							    	$scope.complainCurrent = $scope.snapshotPeriodicData[0][1];
						    	}
					    	}
							$scope.snapshotPeriodicDataLabels = $scope.snapshotPeriodicResponse.data.results.column_names;
							$scope.ifSnapshotLoaded = true;
						});
		        	}, 500);
				}
				
		        //Calling snapshot on page load with a time delay.
		        setTimeout(function(){ 
		        	/*$scope.selectedSourceForSnapshot = $scope.$parent.topSnapshotSources[0][1];
		        	if($scope.selectedSourceForSnapshot == undefined || $scope.selectedSourceForSnapshot == "" || $scope.selectedSourceForSnapshot == null){
		        		$scope.selectedSourceForSnapshot=1;
		        	}
					$scope.snapshotPeriodicSourceClick($scope.selectedSourceForSnapshot,$scope.sourceCrawlingStatus);*/
		        	$scope.snapshotPeriodicSourceClick($scope.selectedSourceForSnapshot,$scope.sourceCrawlingStatus);
					
				}, 3000);
		        //Method for Snapshot display in Analytics Page Ends...
		        
		        
				// Getting tab details from table against current tab ID.
				var dataKpi = $.param({
					"inTabId": $scope.curTabID
		        });
				
				$scope.tabMetaData = null;
				$http.post(kpiDataUrl, dataKpi, $scope.$parent.config).then(function(response){
						var tabResponse = response.data.results.rows;
						
						$scope.authRole = authRole;
						var srcCategory = SOCIAL_MEDIA;	
						
						/*The bellow part was responsible for time change into UST by substracting 330 min*/
						/*var date = new Date();
						date = new Date(date.valueOf() + (date.getTimezoneOffset() + authTimezoneOffset) * 60000);
						var today = $filter('date')(date, 'dd-MM-yyyy HH:mm');
						date.setDate(date.getDate() - 1);
						var yesterday = $filter('date')(date, 'dd-MM-yyyy HH:mm');
						console.log(today+'||||||'+yesterday);
						$scope.analyticsFrom = yesterday;
						$scope.analyticsTo = today;*/
						resetObj(filteredAnalyticsDetailsObj);
						resetObj(entityDetailsObj);
						resetObj(analyticsDetailsObj);
						resetObj(socialListeningObj);
						
						$scope.$parent.checkAll(true,'overall_sentiment');
						
					    if($scope.$parent.initialPageload){
					    	setTimeout(function(){
								var SlideW = $('#dailySnapshot').width()-30;
								slider = $('.dashboard-carousel-daily').bxSlider({
								    slideWidth: 395,
									slideWidth: 593,
								    minSlides: 2,
								    maxSlides: 8,
								    slideMargin: 30,
								    //moveSlides: 1,
								    infiniteLoop: false,
								  });
							
								var SlideW = $('#dailySnapshot').width()-30;
								slider = $('.dashboard-carousel-weekly').bxSlider({
								    slideWidth: 395,
									slideWidth: 593,
								    minSlides: 2,
								    maxSlides: 8,
								    slideMargin: 30,
								    //moveSlides: 1,
								    infiniteLoop: false,
								  });
							
								var SlideW = $('#dailySnapshot').width()-30;
								slider = $('.dashboard-carousel-monthly').bxSlider({
								    slideWidth: 395,
									slideWidth: 593,
								    minSlides: 2,
								    maxSlides: 8,
								    slideMargin: 30,
								    //moveSlides: 1,
								    infiniteLoop: false,
								  });
					    	},10);
					    }
					    
					    
			           
						for (var i=0; i<tabResponse.length;i++) {
							 if(tabResponse[i][0] == "fn_brand_comparission_24Hour" && tabResponse[i][1] == "y") {
								$scope.$parent.fn_brand_comparission_24Hour();
							} else if(tabResponse[i][0] == "fn_brand_comparission_monthly" && tabResponse[i][1] == "y") {
								$scope.$parent.fn_brand_comparission_monthly();
							}  else if(tabResponse[i][0] == "fn_sentimentDistributionMonthlyGraphDiv" && tabResponse[i][1] == "y") {
								$scope.$parent.fn_contextwise_sentiment_distribution();
								$scope.$parent.fn_contextwise_sentiment_distribution_percentile();
							} else if(tabResponse[i][0] == "fn_overall_sentiment_distribution" && tabResponse[i][1]== "y") {
								//$scope.$parent.fn_overall_sentiment_distribution();
								$scope.$parent.fn_overall_sentiment_distribution_filter();
							} else if(tabResponse[i][0] == "fn_top_advocates" && tabResponse[i][1]== "y") {
								$scope.$parent.fn_filter_top_advocates();
							} else if(tabResponse[i][0] == "fn_post_tweet_wise_engagement_score" && tabResponse[i][1]== "y") {
								twitterFacebookEngagementSentimentObj.selectedDuration = $scope.selectedDurationTF;
								twitterFacebookEngagementSentimentObj.selectedSource = $scope.selectedSourceTF;
								$scope.$parent.fn_post_tweet_wise_engagement_score(twitterFacebookEngagementSentimentObj,$scope.socialPaginationTF.socialNewPageTF);
							} else if(tabResponse[i][0] == "fn_deep_listening_YUM" && tabResponse[i][1]== "y") {
								socialListeningObj.accountId = authId;
								socialListeningObj.rowSourceId = ($scope.selectedSource == 'All' || $scope.selectedSource == '') ? -1 : $scope.selectedSource;
								socialListeningObj.rowSourceTopic = ($scope.selectedTopics.length+1 == $scope.topics.length)?null:$scope.selectedTopics.toString();
								socialListeningObj.sourceCategory = srcCategory;
								socialListeningObj.sentiment = Number(ALL_SENTIMENTS_100);
								socialListeningObj.detailsTabName = SOCIAL_LISTENING;
								socialListeningObj.startDt =  toGMTDate($scope.analyticsFrom);
								//socialListeningObj.startDt =  null;
								socialListeningObj.endDt = toGMTDate($scope.analyticsTo);
								$scope.$parent.fn_deep_listening_YUM(socialListeningObj,$scope.socialPagination.socialNewPage);
								
							} else if(tabResponse[i][0] == "fn_fetch_analytics_onLoad_data(context_details)" && tabResponse[i][1]== "y") {
								$scope.$parent.fn_fetch_analytics_onLoad_data(context_details);
							} else if(tabResponse[i][0] == "fn_fetch_analytics_onLoad_data(WORD_CLOUD)" && tabResponse[i][1]== "y") {
								$scope.$parent.fn_fetch_analytics_onLoad_data(WORD_CLOUD);
							} else if(tabResponse[i][0] == "fn_fetch_analytics_onLoad_data(social_listening)" && tabResponse[i][1]== "y") {
								$scope.$parent.fn_fetch_analytics_onLoad_data(social_listening);
							} else if(tabResponse[i][0] == "fn_fetch_word_cloud_onLoad_data" && tabResponse[i][1]== "y") {
								$scope.$parent.fn_fetch_word_cloud_onLoad_data();
							} else if(tabResponse[i][1]== "n") {
								$('#'+tabResponse[i][2]).hide();
							}
					    }
				});
			}
		}
		
	});
	
	

})(window.angular);