// this file contains controller for dashboard functionalities
(function(angular) {
	
	/**
	 * Author Name: Urmi Saha
	 * Create Date: 03-11-2016
	 * Last Modified: Ashis Nayak: 12-07-2017
	 * Input: 
	 * Output: 
	 * Method Insight: controller for dashboard functionalities
	 */
	angular.module('app').controller('DashboardController', function($scope, $http, $filter, $interval, $location){
		$location.path( "/analytics" );
		$scope.$parent.activeMenu = 'dashboard';		// holds the current tab of the navigation menu for keeping the tab highlighted with the active class, value is 'dashboard when dashboard/View page is shown as this controller is called'
		//$scope.intervalValue = "Daily";					// holds the interval value of Happiness Index Live initiated with 'Daily'
		$scope.datePopulationOnloadFlag = true;			// this flag is used to check whether the Happiness Index Live is called onload (so, date filters need to be populated)
		$scope.selectedContextMonthly = 'All Contexts';	// holds the selected context in Competitive Happiness Index initiated with 'All Contexts'
		$scope.entries = "10";							// holds the value for number of entries to be shown in virality table (table in View section)
		$scope.currentPage = 1;							// holds the current page number of the virality table (table in View section)
		$scope.sortTypeEntity = '-5';							// angular sort value for Virality table initialized on feedback column
		$scope.sortReverseEntity  = false;					// angular sort for Virality table to toggle between columns feedback and complaint
		$scope.sentiment = "Negative";					// holds the selected sentiment in Monthly Contextwise Sentiment initiated with 'Negative'
		
		/* For Deep Listening */
		$scope.activeTab = 'social';				// selected tab to be shown among Deep Listening and Trending Topics
		$scope.entityList = false;					// toggle behaviour of entity list dropdown in word cloud table initiated with false(dropdown closed)
		$scope.contextList = false;					// toggle behaviour of context list dropdown in word cloud table initiated with false(dropdown closed)
		$scope.filterContext = '';					// holds the selected context from word cloud table context list dropdown
		$scope.filterEntity = '';					// holds the selected enitity from word cloud table entity list dropdown
		$scope.wordcloudContext = "All Contexts"	// holds the selected context from word cloud
		$scope.more_less = 'more';
		//$scope.sentimentalTo = $scope.$parent.today;			// datepicker 'To date' in Overall Sentiment Distribution initially kept today's date
		//$scope.sentimentalFrom = $scope.$parent.yesterday		// datepicker 'From date' in Overall Sentiment Distribution initially kept yesterday's date
		$scope.showSentimentalContexts = false;
		
		
		
		
		// function for filtering wordcloud data as per entity/context selected from entityList/contextList except when value of entity/context is empty
		$scope.exceptEmptyComparator = function (actual, expected) {
			if (!expected) {
		       return true;
		    }
		    return angular.equals(expected.trim(), actual.trim());
		}
		
		
		// function for onload Happiness Index Live chart
		/*$scope.fn_brand_comparission_24Hour = function(){
			var data = $.param({
				"account_Id": authId, 
	            "current_Time": null,
	            "selectedInterval": $scope.intervalValue
	        });
			$http.post(brand24HourUrl, data, $scope.config).then(function(response){
				*//**
				 * the following condition check is written to check
				 * if the Happiness Index Live is called onload (datePopulationOnloadFlag=true):
				 * 		then date filters are populated by the first and last data points of the chart
				 * else the Happiness Index Live is called after changing interval (datePopulationOnloadFlag=false):
				 * 		then date filters retain the value it contains
				 * 
				 *//*
				if($scope.datePopulationOnloadFlag){
					$scope.$parent.dashboardTo = toLocalDate(response.data.results.rows[response.data.results.rows.length-1][2]);
					var date = response.data.results.rows[response.data.results.rows.length-1][2];
					date = new Date((date.substring(6,10)+'-'+date.substring(3,6)+date.substring(0,2)+date.substring(10,date.length)).replace(/-/g, "/"));
					date.setDate(date.getDate() - 1);
					$scope.$parent.dashboardFrom = toLocalDate($filter('date')(date, 'dd-MM-yyyy HH:mm'));
				}
				chart_brand_comparission_24Hour(response.data.results, $scope.intervalValue);
			});
		}
		*/
		// function for Daily/Weekly interval of spline chart
		$scope.intervalChange = function(){
			$scope.datePopulationOnloadFlag = false;
			$scope.fn_brand_comparission_24Hour(null);
		}
		
		// function for onload Competitive Happiness Index (two spline charts)
		$scope.fn_brand_comparission_monthly = function(){
			var data = $.param({
				"account_Id": authId,
				"topic" : $scope.selectedContextMonthly == 'All Contexts'? '': String ($scope.selectedContextMonthly)
	        });
			$http.post(brandMonthlyUrl, data, $scope.$parent.config).then(function(response){
				chart_brand_comparission_monthly(response.data.results.rows, $scope.selectedContextMonthly);
			});
		}
		
		// function for onload virality table
		$scope.fn_virality_comparission = function (viralityComparissionObj){
			viralityComparissionObj.accountId = authId;
			
			/**
			 * date filters in dashboard/view page populated depending on filter/interval value
			 * Daily interval of Happiness Index (3hrs behind chosen time to the chosen time) :
			 * virality object start and end date properties filled with same(end date) value
			 * Weekly interval of Happiness Index and Filter of virality table (filtered start and end dates filled in virality start and end date properties
			 */
			if(viralityComparissionObj.startDt && (viralityComparissionObj.selectedInterval == "Daily")){ 
				var endDt = viralityComparissionObj.endDt;
				var date = new Date((endDt.substring(6,10)+'-'+endDt.substring(3,6)+endDt.substring(0,2)+endDt.substring(10,endDt.length)).replace(/-/g, "/"));
				date.setHours(date.getHours() - 3);
				$scope.$parent.dashboardFrom = toLocalDate(('0'+date.getDate()).slice(-2)+'-'+('0'+(date.getMonth()+1)).slice(-2)+'-'+date.getFullYear()+' '+('0'+date.getHours()).slice(-2)+':'+('0'+date.getMinutes()).slice(-2));
				$scope.$parent.dashboardTo = toLocalDate(viralityComparissionObj.endDt);
			}else if(viralityComparissionObj.selectedInterval == "Weekly" /*|| viralityComparissionObj.selectedInterval == "Filter"*/){
				$scope.$parent.dashboardFrom = toLocalDate(viralityComparissionObj.startDt);
				$scope.$parent.dashboardTo = toLocalDate(viralityComparissionObj.endDt);
			}
			
			var data = $.param({
				"accountId": viralityComparissionObj.acctId == null ? viralityComparissionObj.accountId : viralityComparissionObj.acctId, 
	            "startDt": viralityComparissionObj.startDt, 
	            "endDt": viralityComparissionObj.endDt,
	            "hR" : viralityComparissionObj.hR,
	            "selectedInterval" : viralityComparissionObj.selectedInterval
	        });
			$http.post(viralityComparisonUrl, data, $scope.config).then(function(response){
				$scope.viralities = response.data.results.rows;
				for(var i=0;i<$scope.viralities.length;i++){
					$scope.viralities[i].push(Math.round($scope.viralities[i][5]*$scope.viralities[i][7]/100))
				}
			});
		}
		
		// function for monthly contextwise sentiment by count
		$scope.fn_contextwise_sentiment_distribution = function(){
			var data = $.param({
				"account_Id": authId,
				"sentiment" : $scope.sentiment
	        });
			$http.post(contextwiseSentimentDistributionURL, data, $scope.config).then(function(response){
				chart_contextwise_sentiment_distribution(response.data.results.rows, 'value', $scope.sentiment);
			});
		}
		
		// function for monthly contextwise sentiment by percent
		$scope.fn_contextwise_sentiment_distribution_percentile = function(){
			var data = $.param({
				"account_Id": authId,
				"sentiment" : $scope.sentiment
	        });
			$http.post(contextwiseSentimentDistributionPercentageURL, data, $scope.config).then(function(response){
				chart_contextwise_sentiment_distribution(response.data.results.rows, 'percentage', $scope.sentiment);
			});
		}
		
		/**
		 * the following function is called when the Go button of the filter
		 * in dashboard/View page is clicked
		 */
		$scope.fn_filter = function(){
			var start = new Date(($scope.$parent.dashboardFrom.substring(6,10)+'-'+$scope.$parent.dashboardFrom.substring(3,6)+$scope.$parent.dashboardFrom.substring(0,2)+$scope.$parent.dashboardFrom.substring(10,$scope.$parent.dashboardFrom.length)).replace(/-/g, "/"));
			var end = new Date(($scope.$parent.dashboardTo.substring(6,10)+'-'+$scope.$parent.dashboardTo.substring(3,6)+$scope.$parent.dashboardTo.substring(0,2)+$scope.$parent.dashboardTo.substring(10,$scope.$parent.dashboardTo.length)).replace(/-/g, "/"));
			if(start > end){
				$('#alertModal').modal("show");
				$('#alertModal .modal-content').addClass('modal-danger');
		        $('#alertTxt').html("End date should not be behind Start date");
			}else{
				viralityComparissionObj.accountId = authId;
				viralityComparissionObj.startDt = toGMTDate($scope.$parent.dashboardFrom);
				viralityComparissionObj.endDt = toGMTDate($scope.$parent.dashboardTo);
				viralityComparissionObj.hR = 1;
				viralityComparissionObj.selectedInterval = 'Filter';
				$scope.fn_virality_comparission(viralityComparissionObj);
			}
		}
		
		
		// function for onload deep listening
		$scope.fn_deep_listening = function(socialListeningObj){
			var data = $.param({
				"inAccountId": socialListeningObj.accountId, 
		        "inSource": socialListeningObj.sourceId, 
		        "inTopic": socialListeningObj.sourceTopic, 
		        "start_dt": socialListeningObj.startDt,
		        "end_dt": socialListeningObj.endDt,
		        "sentiment":socialListeningObj.sentiment,
 		        "details_tab_name" : socialListeningObj.detailsTabName
	        });
			$scope.soclists = null;
			$http.post(analyticsDetailsUrl, data, $scope.config).then(function(response){
					var data = response.data.results.rows;
					for (var i=0; i<data.length;i++) {
				    	data[i][4] = toLocalDate(data[i][4]);
				    }
					$scope.soclists = data;
			});
		}
		// function deep listening and trending topics tables when filtered with the filter options
		$scope.fn_filtered_source_date_analytics_details = function(filteredAnalyticsDetailsObj){
			var data = $.param({
				"inAccountId": filteredAnalyticsDetailsObj.accountId, 
		        "inSource": filteredAnalyticsDetailsObj.rowSourceId, 
		        "inTopic": filteredAnalyticsDetailsObj.rowSourceTopic, 
		        "start_dt": filteredAnalyticsDetailsObj.startDt,
		        "end_dt" : filteredAnalyticsDetailsObj.endDt,
	            "sentiment":filteredAnalyticsDetailsObj.sentiment,
		        "details_tab_name" : filteredAnalyticsDetailsObj.detailsTabName
	        });
			$scope.soclists = null;
			$http.post(filteredAnalyticsDetailsUrl, data, $scope.config).then(function(response){
				var i, data;
				if(filteredAnalyticsDetailsObj.detailsTabName == 'social_listening'){
		    		data = response.data.results.rows;
		    		for (i=0; i<data.length;i++) {
				    	data[i][4] = toLocalDate(data[i][4]);
				    }
					$scope.soclists = data;
		    	}else if(filteredAnalyticsDetailsObj.detailsTabName == WORD_CLOUD){
		    		$scope.wordCloudTableData = [];
		    		var keysArray = response.data.results.column_names;
		    		var valuesArray = response.data.results.rows;
		    		for(i=0;i<response.data.results.rows.length;i++){
		    			data = {};
		    			for(var j=0;j<keysArray.length;j++){
		    				data[keysArray[j]] = valuesArray[i][j];
		    			}
		    			$scope.wordCloudTableData.push(data);
		    		}
		    		$scope.entitiesForFilter = [];
		    		for(i=0;i<$scope.wordCloudTableData.length;i++){
		    			if($scope.entitiesForFilter.indexOf($scope.wordCloudTableData[i].entity) == -1){
		    				$scope.entitiesForFilter.push($scope.wordCloudTableData[i].entity.trim());
		    			}
		    		}
		    		$scope.entitiesForFilter.sort();
		    		$scope.entitiesForFilter.unshift("All Entities");
		    	}	
			});
		}
		
		/**
		 * This function populated the wordcloudaspect and wordcloudentity arrays
		 * which are sent to the generateWordcloud fucntion for creation of the wordcloud
		 * @parameter: response- contains the total data of entities(and aspects)
		 * @parameter: entityIndex- this contains the index of response where entities are returned,
		 * 			   0: when only entities are returned,
		 * 			   1: when both aspects and entities are returned
		 */
		$scope.fn_word_cloud_array_population = function(response, entityIndex){
			var wordcloudentity = response.data[entityIndex].rows;				// entities can be present in 0th or 1st index of response.data
			var wordcloudentityHead = response.data[entityIndex].columnNames;
			var wordcloudArray = [];
		    var entityArray = [];
			
		    // entity objects are formed with properties and pushed in entityArray
		    for(var i=0;i<wordcloudentity.length;i++){
		    	var entity = {};
				entity.text = wordcloudentity[i][0];
				entity.size = wordcloudentity[i][1];
				entity.sentiment = wordcloudentity[i][2];
				entity.type = wordcloudentityHead[0];
				
				/**
				 * this condition puts the required colors on entities depending on:
				 * if only entities are fetched, they are assigned colors depending on their respective sentiments
				 * if both entities and aspects are fetched, they are assigned a common color
				 */ 
				if(!entityIndex){
					if(wordcloudentity[i][2]=='Positive'){
						entity.color = GREEN;
					}else if(wordcloudentity[i][2]=='Negative'){
						entity.color = RED;
					}else{
						entity.color = YELLOW;
					}
				}else{
					entity.color = GRAY;
				}
				entityArray.push(entity);
		    }
		    
		    // the entities filter list in the Trending Topics table (entities column) is populated 
			$scope.entitiesForFilter = [];
    		for(i=0;i<wordcloudentity.length;i++){
    			if($scope.entitiesForFilter.indexOf(wordcloudentity[i][0]) == -1){
    				$scope.entitiesForFilter.push(wordcloudentity[i][0].trim());
    			}
    		}
    		$scope.entitiesForFilter.sort();					// sorts the entities in the list in ascending order
    		$scope.entitiesForFilter.unshift("All Entities");
    		
    		/**
    		 * if entityIndex is 1, then response has fetched both aspects and entities
    		 * with aspects in 0th index and entities in 1st index
    		 */
    		if(entityIndex){
    			var wordcloudaspect = response.data[0].rows;
				var wordcloudaspectHead = response.data[0].columnNames;
				for(i=0;i<wordcloudaspect.length;i++){
			    	var aspect = {};
					aspect.text = wordcloudaspect[i][0];
					aspect.size = wordcloudaspect[i][1];
					aspect.sentiment = wordcloudaspect[i][2];
					aspect.type = "context";
					
					// aspects are assigned colors depending on their respective sentiments
					if(wordcloudaspect[i][2]=='Positive'){
						aspect.color = GREEN;
					}else if(wordcloudaspect[i][2]=='Negative'){
						aspect.color = RED;
					}else{
						aspect.color = YELLOW;
					}
					wordcloudArray.push(aspect);
				}
    		}
    		
			generateWordcloud(wordcloudArray,entityArray);			
		}
		
		// function for onload word cloud in trending topics section
		$scope.fn_word_cloud = function(topic){
			$scope.contextList = false;
			$scope.filterEntity = '';
			var data = $.param({
				"accountId": authId,
				"selectedContext": topic == null ? null : String (topic),
				"sourceId": ($scope.selectedSource == 'All' || $scope.selectedSource == '') ? -1 : $scope.selectedSource,
				"startDate": toGMTDate($scope.analyticsFrom),
				"endDate": toGMTDate($scope.analyticsTo),
				"sentiment": ($scope.selectedSentiment  == 'All' || $scope.selectedSentiment  == '') ? null : $scope.selectedSentiment
	        });
			if(topic!=null && topic!="All Contexts"){
				$scope.filterContext = String (topic);
				$http.post(getEntitiesForSelectedContextUrl, data, $scope.config).then(function(response){
					$scope.wordcloudContext = topic;
					$scope.fn_word_cloud_array_population(response, 0);
				});
			}else{
				$scope.filterContext = '';
				if($scope.selectedTopic != "All Contexts" && $scope.selectedTopic != "" && $scope.selectedTopic != topic){
					filteredAnalyticsDetailsObj.accountId = authId;
					filteredAnalyticsDetailsObj.rowSourceTopic = topic;
					filteredAnalyticsDetailsObj.detailsTabName = WORD_CLOUD;
					$scope.fn_filtered_source_date_analytics_details(filteredAnalyticsDetailsObj);
	    		}
				$http.post(getWordCloudDataUrl, data, $scope.config).then(function(response){					
					$scope.wordcloudContext = "All Contexts";
					$scope.fn_word_cloud_array_population(response, 1);
				});
			}
		}
		
		// function for filtered word cloud in trending topics section
		$scope.fn_word_cloud_filtered = function(){
			$scope.filterEntity = '';
			var data = $.param({
				"accountId": authId,
				"selectedContext": ($scope.selectedTopic  == 'All Contexts' || $scope.selectedTopic  == '') ? null : String ($scope.selectedTopic),
				"sourceId": ($scope.selectedSource == 'All' || $scope.selectedSource == '') ? -1 : $scope.selectedSource,
				"startDate": toGMTDate($scope.analyticsFrom),
				"endDate": toGMTDate($scope.analyticsTo),
				"sentiment": ($scope.selectedSentiment  == 'All' || $scope.selectedSentiment  == '') ? null : $scope.selectedSentiment
	        });
			if($scope.selectedTopic !='All Contexts' && $scope.selectedTopic  != ''){
				$http.post(getFilteredWordCloudDataUrl, data, $scope.config).then(function(response){
					$scope.filterContext = String($scope.selectedTopic);
					$scope.wordcloudContext = String ($scope.selectedTopic);
					$scope.fn_word_cloud_array_population(response, 0);
				});
			}else{
				$scope.filterContext = '';
				$http.post(getFilteredWordCloudDataUrl, data, $scope.config).then(function(response){
					$scope.wordcloudContext = "All Contexts";
					$scope.fn_word_cloud_array_population(response, 1);
				});
			}
		}
		
		
		$scope.fn_filterByEntity = function(selectedEntity){
			if(selectedEntity == 'All Entities'){
				selectedEntity = '';
			}
			if($scope.entityList){
				$scope.entityList=!$scope.entityList;
				$scope.filterContext = '';
			}
			$scope.filterEntity = selectedEntity;
		}
		
		/**
		 * This function is called for populating data of Explore page during on-Load for sections - 
		 * Context Details, Deep Listening and Word Cloud Deep Listening Tables.
		 */
		$scope.fn_fetch_analytics_onLoad_data = function (sectionName){
			var data = $.param({
				"accountId": authId,
				"sectionName":sectionName
	        });
			$http.post(getAnalyticsOnLoadDataUrl, data, $scope.config).then(function(response){
				switch (sectionName){
					case 'social_listening':
						$scope.fn_populate_deep_listening(response);
						break;
					case 'word_cloud':
						$scope.fn_populate_word_cloud_deep_listening(response);
						break;
					default:
				}
			});
		}
		
		/**
		 * This function is called for populating data of the wordcloud onload.
		 */
		$scope.fn_fetch_word_cloud_onLoad_data = function(){
			var data = $.param({
				"accountId": authId
	        });
			$http.post(getWordCloudOnLoadDataUrl, data, $scope.config).then(function(response){
				$scope.wordcloudContext = "All Contexts";
				$scope.fn_word_cloud_array_population(response, 1);
			});
		}
		
		/**
		 * This function populates the Social Post List Table of Word Cloud with the response data.
		 */
		$scope.fn_populate_word_cloud_deep_listening = function(response){
    		$scope.wordCloudTableData = [];
    		var keysArray = response.data.results.column_names;
    		var valuesArray = response.data.results.rows;
    		for(var i=0;i<response.data.results.rows.length;i++){
    			var data = {};
    			for(var j=0;j<keysArray.length;j++){
    				data[keysArray[j]] = valuesArray[i][j];
    			}
    			$scope.wordCloudTableData.push(data);
    		}
    		$scope.entitiesForFilter = [];
    		for(i=0;i<$scope.wordCloudTableData.length;i++){
    			if($scope.entitiesForFilter.indexOf($scope.wordCloudTableData[i].entity) == -1){
    				$scope.entitiesForFilter.push($scope.wordCloudTableData[i].entity.trim());
    			}
    		}
    		$scope.entitiesForFilter.sort();
    		$scope.entitiesForFilter.unshift("All Entities");
		}
		
		/**
		 * This function populates the Deep Listening Table with the response data.
		 */
		$scope.fn_populate_deep_listening = function(response){
			var data = response.data.results.rows;
			for (var i=0; i<data.length;i++) {
		    	data[i][4] = toLocalDate(data[i][4]);
		    }
			$scope.soclists = data;
		}
		
		

		resetObj(viralityComparissionObj);
		
		/**
		 * The following block of code determines the state of the application
		 * and redirects the application to the required page:
		 * if user has not yet filled the addinfo page: redirect to addinfo page,
		 * if user has filled addinfo page, but data is not yet ready: redirect to gettingdata page
		 * if user has filled the details and data is ready, call functions of the present page (Dashboard/View page).
		 */
		if(authData.split('$')[5] == 0){
			$scope.addinfo = true;
			$location.path( "/addinfo" );
		}
		else if(authData.split('$')[4] == 0)
			$location.path( "/gettingdata" );
		else{
			
			$scope.fn_brand_comparission_24Hour();
			$scope.fn_brand_comparission_monthly();
			$scope.fn_virality_comparission(viralityComparissionObj);
			$scope.fn_contextwise_sentiment_distribution();
			$scope.fn_contextwise_sentiment_distribution_percentile();
			
			
			
			socialListeningObj.accountId = authId;
			socialListeningObj.detailsTabName = 'social_listening';
			
			filteredAnalyticsDetailsObj.accountId = authId;
			filteredAnalyticsDetailsObj.detailsTabName = WORD_CLOUD;
			
			resetObj(filteredAnalyticsDetailsObj);
			resetObj(analyticsDetailsObj);
			resetObj(socialListeningObj);
			
			/* To increase the Explore Page onload performance calling SPs are different*/
			$scope.fn_fetch_analytics_onLoad_data(WORD_CLOUD);
			$scope.fn_fetch_analytics_onLoad_data(social_listening);
			$scope.fn_fetch_word_cloud_onLoad_data();
		}
	});

})(window.angular);