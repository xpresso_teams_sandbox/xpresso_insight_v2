(function(angular) {
/*'use strict';*/
	var rcxApp = angular.module('app', ['ngRoute', 'ngAnimate', 'ui.bootstrap', 'ui.bootstrap.datetimepicker','angularUtils.directives.dirPagination', 'daterangepicker']);
	
	rcxApp.value('loadingService', {
		  loadingCount: 0,
		  isLoading: function() { return this.loadingCount > 0; },
		  requested: function() { this.loadingCount += 1; },
		  responded: function() {  this.loadingCount -= 1; }
		})
		.factory('loadingInterceptor', function(loadingService) {
		  return {
		    request: function(config) {
		      loadingService.requested();
		      return config;
		    },
		    response: function(response) {
		      loadingService.responded();
		      return response;
		    },
		    responseError: function(error){
		     console.log("error:", error);
		     loadingService.responded();
		     return error;

		    }
		  }
		});

	
	rcxApp.config(['$httpProvider', function($httpProvider) {  
		$httpProvider.interceptors.push('loadingInterceptor');
	}]);
	
	
	//options for loading spinner
	/*rcxApp.config(['usSpinnerConfigProvider', function (usSpinnerConfigProvider) {
	    usSpinnerConfigProvider.setDefaults({
	    	lines: 12 				// The number of lines to draw
			, length: 15 			// The length of each line
			, width: 4 				// The line thickness
			, radius: 5				// The radius of the inner circle
			, scale: 1 				// Scales overall size of the spinner
			, corners: 1 			// Corner roundness (0..1)
			, color: '#000' 		// #rgb or #rrggbb or array of colors
			, opacity: 0.25			// Opacity of the lines
			, rotate: 0 			// The rotation offset
			, direction: 1 			// 1: clockwise, -1: counterclockwise
			, speed: 1 				// Rounds per second
			, trail: 60 			// Afterglow percentage
			, fps: 20 				// Frames per second when using setTimeout() as a fallback for CSS
			, zIndex: 2e9 			// The z-index (defaults to 2000000000)
			, className: 'spinner' 	// The CSS class to assign to the spinner
			, top:'50%' 			// Top position relative to parent
			, left: '50%' 			// Left position relative to parent
			, shadow: false 		// Whether to render a shadow
			, hwaccel: false 		// Whether to use hardware acceleration
			, position: 'fixed' 	// Element positioning
	    });
	}]);*/
	
	// Created directive to disable the 'Left panel' menus while user is in 'Add Info' page.
	rcxApp.directive('customClick',function(){
		 return {
	        restrict: 'A',
	        link: function(scope, elem, attrs) {
				setTimeout(function(){	
					if(!!scope.isAddinfo){
						elem.on('click', function(e){
							e.preventDefault();					
						});
						
						$(document).on('mouseover',$(elem),function(){
							$(elem).attr('title', scope.addInfo_message);
							$(elem).tooltip({
									container: 'body',
									placement: 'right'
								});
							if(!scope.isAddinfo)
							{
								$(elem).attr('title', scope.addInfo_message);
								$(elem).tooltip('destroy');
							}
						});
					}
				},0);
	        }
			
	   };
	})
	
	// Created default controller for redirection of pages
	rcxApp.controller('EmptyController', function($scope, $http, $location){
		$scope.$parent.gettingdata = true;
		$scope.$parent.pageLoad = false;
		if(authData.split('$')[4] != 0){
			$location.path( "/" );
		}
	});
	
	rcxApp.directive('ddTextCollapse', ['$compile', function($compile) {
	    return {
	        restrict: 'A',
	        scope: true,
	        link: function(scope, element, attrs) {

	            // start collapsed
	            scope.collapsed = false;

	            // create the function to toggle the collapse
	            scope.toggle = function() {
	                scope.collapsed = !scope.collapsed;
	            };

	            // wait for changes on the text
	            attrs.$observe('ddTextCollapseText', function(text) {

	                // get the length from the attributes
	                var maxLength = scope.$eval(attrs.ddTextCollapseMaxLength);

	                if (text.length > maxLength) {
	                    // split the text in two parts, the first always showing
	                    var firstPart = String(text).substring(0, maxLength);
	                    var secondPart = String(text).substring(maxLength, text.length);

	                    // create some new html elements to hold the separate info
	                    var firstSpan = $compile('<span>' + firstPart + '</span>')(scope);
	                    var secondSpan = $compile('<span ng-if="collapsed">' + secondPart + '</span>')(scope);
	                    var moreIndicatorSpan = $compile('<span ng-if="!collapsed">... </span>')(scope);
	                    var lineBreak = $compile('<br ng-if="collapsed">')(scope);
	                    var toggleButton = $compile('<span class="collapse-text-toggle pointer" ng-click="toggle();$event.stopPropagation();">{{collapsed ? "Read Less" : "Read More"}}</span>')(scope);

	                    // remove the current contents of the element
	                    // and add the new ones we created
	                    element.empty();
	                    element.append(firstSpan);
	                    element.append(secondSpan);
	                    element.append(moreIndicatorSpan);
	                    element.append(lineBreak);
	                    element.append(toggleButton);
	                }
	                else {
	                    element.empty();
	                    element.append(text);
	                }
	            });
	        }
	    };
	}]);
	
})(window.angular);

