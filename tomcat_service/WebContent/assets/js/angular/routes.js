// this file contains the route-page mapping
(function(angular) {

	angular.module('app').config(function($routeProvider) {
	$routeProvider
	        
		 	// route for the gettingdata page
		    .when('/gettingdata', {
		        templateUrl : 'pages/pre-active.jsp',
		        controller  : 'EmptyController'
		    })
		    
		    // route for the add-info page
		    .when('/addinfo', {
		        templateUrl : 'pages/add-info.jsp',
		        controller  : 'AddDetailsController'
		    })
		    
		    // route for the add-info-summary page
		    .when('/addinfosummary', {
		        templateUrl : 'pages/add-info-summary.jsp',
		        controller  : ''
		    })
		    
	    	// route for the dashboard page
	        /*.when('/', {
	        	templateUrl : 'pages/dashboard.jsp',
	            controller  : 'DashboardController'
	        })*/
		    
		
		   .when('/', {
	        	templateUrl : 'pages/analytics.jsp',
	            controller  : 'AnalyticsController'
	        })
	        
	        
	        .when('/dashboard', {
	        	templateUrl : 'pages/dashboard.jsp',
	            controller  : 'DashboardController'
	        })
	
	        // route for the analytics page
	        /*.when('/analytics/:source?/:topic?', {
	            templateUrl : 'pages/analytics.jsp',
	            controller  : 'AnalyticsController'
	        })*/
	        .when('/analytics/', {
	            templateUrl : 'pages/analytics.jsp',
	            controller  : 'AnalyticsController'
	        })
	
	        // route for the ticket page
	        .when('/ticket', {
	            templateUrl : 'pages/ticket.jsp',
	            controller  : 'TicketController'
	        })
	        
	        // route for the TicketManagement page
	        .when('/ticketmanagement/:postid', {
	            templateUrl : 'pages/ticketmanagement.jsp',
	            controller  : 'TicketManagementController'
	        })
	        .when('/deeplistining', {
	            templateUrl : 'pages/deeplistining.jsp',
	            controller  : 'DeepListiningController'
	        })
	        
	        // route for the analyze page
	        .when('/analyze', {
	            templateUrl : 'pages/analyze.jsp',
	            controller  : 'AnalyzeController'
	        })
			
			// route for the settings page
	        .when('/settings/:reset?', {
	            templateUrl : 'pages/settings.jsp',
	            controller  : 'SettingsController'
	        })
	        
	        // route for the campaign page
	        .when('/campaign', {
	            templateUrl : 'pages/campaign.jsp',
	            controller  : 'CampaignController'
	        })
	        
	         // route for the campaign page
	        .when('/ratings', {
	            templateUrl : 'pages/ratings.jsp',
	            controller  : 'RatingsController'
	        })
	        
	         // route for the campaign page
	        .when('/brand', {
	            templateUrl : 'pages/brand.jsp',
	            controller  : 'BrandController'
	        })
	        
	         // route for the campaign page
	        .when('/reviewanalysis', {
	            templateUrl : 'pages/reviewanalysis.jsp',
	            controller  : 'ReviewAnalysis'
	        })
	        
	        
	        
	        // any invalid route
	    	.otherwise({ redirectTo: '/'})
		});

})(window.angular);

