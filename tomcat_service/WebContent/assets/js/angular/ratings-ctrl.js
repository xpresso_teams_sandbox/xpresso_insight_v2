// this file contains controller for settings functionalities
(function(angular) {
	
	angular.module('app').controller('RatingsController', function ($scope,$window, $http, $filter, $routeParams, $location,$sce){
		$scope.$parent.activeMenu = 'ratings';
		$scope.$parent.latestNewsSection = "0";
		$scope.currentPage = 1;
		$scope.topRatedStoreCurrentPage = 1;
		$scope.curTabID = 4;
		$scope.ownerShipId = 1;
		$scope.$parent.initialPageload = true;
		
		$scope.topRatingCities = [];
		$scope.topRatingsSources = [];	
		//$scope.topRatingsSelectedSource = NULL_VALUE;  
		//$scope.topRatingsSelectedSource = "1";
		$scope.topRatingsSelectedSource = NULL_VALUE;
		$scope.topRatingsSelectedCity = NULL_VALUE; 
		$scope.topRatingSource = NULL_VALUE;
		$scope.selectedRatingsTopratedCity = NULL_VALUE;	
		$scope.selectedRatingsTopratedSource = NULL_VALUE;	
		$scope.topratedstores=[];
		
		$scope.sourceWiseStoreListCities = [];
		$scope.sourceWiseStoreListSelectedCity = NULL_VALUE; 
		console.log("default::", (-1 === NULL_VALUE));
		$scope.sourcewisestorelists=[];
		$scope.sourcewisestorelistsColName=[];
		
		
		$scope.competitorComparisionCities = [];
		$scope.competitorComparisionSources = [];	
		$scope.competitorComparisionSelectedSource = NULL_VALUE;  // Default 2 is set for Zomato
		//$scope.competitorComparisionSelectedSource = "1"; 
		$scope.competitorComparisionSelectedCity = NULL_VALUE; // Default is 1 now for kolkata
		$scope.competitorComparisionSource = NULL_VALUE;
		//$scope.selectedRatingscompetitorComparisionCity = BLANK_STRING;	
		//$scope.selectedRatingscompetitorComparisionSource = BLANK_STRING;	
		$scope.competitorcomparisionstores=[];
		
		
		$scope.bottomRatingCities = [];
		$scope.chartRatingCities= [];
		$scope.bottomRatingsSources = [];
		$scope.chartRatingsSources = [];	
		//$scope.cities = [];	
		$scope.bottomRatingsSelectedSource = NULL_VALUE;  // Default 2 is set for Zomato
		//$scope.bottomRatingsSelectedSource = "1";
		$scope.bottomRatingsSelectedCity = NULL_VALUE; // Default is 1 now for kolkata
		$scope.bottomRatingSource = BLANK_STRING;
		$scope.selectedRatingsbottomratedCity = NULL_VALUE;	
		$scope.selectedRatingsbottomratedSource = NULL_VALUE;	
		$scope.bottomratedstores=[];
		
		$scope.chartRatingsSelectedSource = 1;  // Default 2 is set for Zomato
		$scope.chartRatingsSelectedCity = NULL_VALUE; // Default is 1 now for kolkata
		
		//$scope.overAllRatingsSelectedCity = 1; // Default is 1 now for kolkata
		$scope.overAllRatingsCities = [];
		$scope.overallratingsstores=[];
		$scope.overAllRatingsSelectedCity = NULL_VALUE;
		
		$scope.ownershipMetaData = [];
		
		$scope.searchText = "";
		
		$scope.ratingCitySourceWiseStoreFrom = $scope.$parent.ratingsYesterday;
		$scope.ratingCitySourceWiseStoreTo = $scope.$parent.ratingsToday;
		
		if(authData.split('$')[5] == 0){
			$location.path( "/addinfo" );
		}
		
		if(authData.split('$')[5] == 0){
			$scope.addinfo = true;
			$location.path( "/addinfo" );
		}
		else if(authData.split('$')[4] == 0)
			$location.path( "/gettingdata" );
		else {
			//Gettings Ownership Tabs 
			// Get tab details from table 
			var dataWonership = $.param({
				"inAccountId": authId
	        });
			$scope.ownershipMetaData = null;
			$http.post(tabWonershipUrl, dataWonership, $scope.config).then(function(response){
					var ownershipData = response.data.results.rows;
					$scope.ownershipMetaData = ownershipData;
			});
			
			//Get latest crawling date time
			$http.get(getLatestCrawlingTime+'?accountId='+authId).success( function(response) {
				$scope.crawlingDateTime = response.results.rows[0][0];
		    });
			
			
			//Get Top and Bottom and competitior Rating Sources Onload
			$http.get(getSourcesForRatingTabUrl+'?accountId='+authId+'&type='+"topRatingSources").success( function(response) {
				var topRatingsSources = response.results.rows;
				var chartRatingsSources = response.results.rows;
				
				angular.copy(chartRatingsSources,$scope.chartRatingsSources);

				topRatingsSources.unshift([NULL_VALUE,ALL_SOURCES]);
				$scope.topRatingsSources = topRatingsSources;
				$scope.bottomRatingsSources = topRatingsSources;
				$scope.competitorComparisionSources = topRatingsSources;
		    });
			
			
			//Get Cities Onload
			$http.get(getTopRatedCitiesOnSourceSelectionUrl+'?accountId='+authId+'&sourceId='+$scope.topRatingsSelectedSource+'&inOwnerShipId='+$scope.ownerShipId).success( function(response) {
				$scope.topRatingCities = [];
				$scope.bottomRatingCities = [];
				topRatingCities = response.results.rows;
				if(topRatingCities.length == 0){
					$scope.topRatingsSelectedCity = NULL_VALUE;
				}
				$scope.topRatingCities = topRatingCities;
				$scope.topRatingCities.unshift([NULL_VALUE,ALL_CITIES]);
				$scope.bottomRatingCities = $scope.topRatingCities;
		    });
			
			//Get Cities for competitor comparision Onload
			$http.get(getCompetitorComparisionCitiesOnLoadUrl+'?accountId='+authId+'&sourceId=-1&inOwnerShipId=0').success( function(response) {
				var competitorComparisionCities = response.results.rows;
				if(competitorComparisionCities.length == 0){
					$scope.competitorComparisionCities = NULL_VALUE;
				}
				$scope.competitorComparisionCities = competitorComparisionCities;
				$scope.competitorComparisionCities.unshift([NULL_VALUE,ALL_CITIES]);
				
		    });
			
			//Get overAllRatingsCities and sourceWiseStoreListCities Cities Onload
			$http.get(getTopRatedCitiesOnSourceSelectionUrl+'?accountId='+authId+'&sourceId=-1&inOwnerShipId='+$scope.ownerShipId).success( function(response) {
				$scope.overAllRatingsCities = response.results.rows;
				$scope.overAllRatingsCities.unshift([NULL_VALUE,ALL_CITIES]);
				$scope.sourceWiseStoreListCities = $scope.overAllRatingsCities;
		    });
			
			// Get tab details from table 
			var dataKpi = $.param({
				"inTabId": $scope.curTabID
	        });
			
			$scope.authRole = authRole;
			var date = new Date();
			date = new Date(date.valueOf() + (date.getTimezoneOffset() + authTimezoneOffset) * 60000);
			var today = $filter('date')(date, 'dd-MM-yyyy HH:mm');
			date.setDate(date.getDate() - 1);
			var yesterday = $filter('date')(date, 'dd-MM-yyyy HH:mm');
			console.log(today+'||||||'+yesterday);
			$scope.analyticsFrom = yesterday;
			$scope.analyticsTo = today;
			
			$http.post(kpiDataUrl, dataKpi, $scope.$parent.config).then(function(response){
				var tabResponse = response.data.results.rows;
				
				for (var i=0; i<tabResponse.length;i++) {
					//alert(tabResponse[i][0] +"||||"+ tabResponse[i][1]);
					 if(tabResponse[i][0] == "fn_top_rated_stores" && tabResponse[i][1] == "y") {
						$scope.fn_top_rated_stores();
					} else if(tabResponse[i][0] == "fn_bottom_rated_stores" && tabResponse[i][1] == "y") {
						$scope.fn_bottom_rated_stores();
						$scope.fn_CitySourceWiseHistoricRating();
						$scope.getChartCities();
					}  else if(tabResponse[i][0] == "fn_overall_rating_across_stores" && tabResponse[i][1]== "y") {
						$scope.fn_overall_rating_across_stores();
					} else if(tabResponse[i][0] == "fn_competitor_comparison" && tabResponse[i][1]== "y") {
						//$scope.fn_competitor_comparison();
					} else if(tabResponse[i][0] == "fn_source_wise_store_list" && tabResponse[i][1]== "y") {
						$scope.fn_source_wise_store_list();
					} else if(tabResponse[i][1]== "n") {
						$('#'+tabResponse[i][2]).hide();
					}
			    }
			});
			
		}
		
		//Filter Top rated ratings
		$scope.filterTopratedStores = function(){
			$scope.fn_top_rated_stores();
		}
		
		//Filter Competitor Comparision ratings
		$scope.filterCompetitorComparision = function(){
			$scope.fn_competitor_comparison();
		}
		
		$scope.filterOverAllRatings = function(){
			$scope.fn_overall_rating_across_stores();
		}
		
		//Filter Store List ratings
		$scope.filterSourceWiseStoreList = function(){
			$scope.searchText="";
			$scope.sourcewisestorelistsColName=[];
			$scope.fn_source_wise_store_list();
		}
		
		//Filter Bottom  rated ratings
		$scope.filterBottomratedStores = function(){
			$scope.fn_bottom_rated_stores();
		}
		
		//Filter Bottom  rated ratings
		$scope.filterCitySourceWiseHistoricRatingStores = function(){
			$scope.fn_CitySourceWiseHistoricRating();
		}
		
		
		//Get Top rating cities when source is changed
		$scope.getTopRatingCities = function(){
			$http.get(getTopRatedCitiesOnSourceSelectionUrl+'?accountId='+authId+'&sourceId='+$scope.topRatingsSelectedSource+'&inOwnerShipId='+$scope.ownerShipId).success( function(response) {
				var topRatingCities = response.results.rows;
				
				if(topRatingCities.length == 0){
					$scope.topRatingsSelectedCity = -1;
				}
				$scope.topRatingCities = topRatingCities;
				$scope.topRatingCities.unshift([NULL_VALUE,ALL_CITIES]);
		    	
		    });
			
		}
		
		//Get Competitor Comparision cities when source is changed
		$scope.getCompetitorComparisionCities = function(){
			$http.get(getCompetitorComparisionCitiesOnSourceSelectionUrl+'?accountId='+authId+'&sourceId='+$scope.competitorComparisionSelectedSource+'&inOwnerShipId=0').success( function(response) {
				var competitorComparisionCities = response.results.rows;
				if(competitorComparisionCities.length == 0){
					$scope.competitorComparisionSelectedCity = NULL_VALUE;
				}
				$scope.competitorComparisionCities = competitorComparisionCities;
				$scope.competitorComparisionCities.unshift([NULL_VALUE,ALL_CITIES]);
		    });
		}
		
		//Get Bottom rating cities when source is changed
		$scope.getBottomRatingCities = function(){
			$http.get(getBottomRatedCitiesOnSourceSelectionUrl+'?accountId='+authId+'&sourceId='+$scope.bottomRatingsSelectedSource+'&inOwnerShipId='+$scope.ownerShipId).success( function(response) {
				var bottomRatingCities = response.results.rows;
				if(bottomRatingCities.length == 0){
					$scope.bottomRatingsSelectedCity = NULL_VALUE;
				}
				$scope.bottomRatingCities = bottomRatingCities;
				$scope.bottomRatingCities.unshift([NULL_VALUE,ALL_CITIES]);
		    });
			
		}
		
		//Get Bottom rating cities when source is changed
		$scope.getChartCities = function(){
			$http.get(getBottomRatedCitiesOnSourceSelectionUrl+'?accountId='+authId+'&sourceId='+$scope.chartRatingsSelectedSource+'&inOwnerShipId='+$scope.ownerShipId).success( function(response) {
				var chartRatingCities = response.results.rows;
				if(chartRatingCities.length == 0){
					$scope.chartRatingsSelectedCity = NULL_VALUE;
				}
				$scope.chartRatingCities = chartRatingCities;
				$scope.chartRatingCities.unshift([NULL_VALUE,ALL_CITIES]);
		    });
			
		}
		
		//Populate Top Rated Store on page load and filter
		$scope.fn_top_rated_stores = function(){
			$scope.topratedstores = null;
			var data = $.param({
				"inSourceId": $scope.topRatingsSelectedSource,
				"inCityId":$scope.topRatingsSelectedCity,
				"inAccountId":authId,
				"inOwnerShipId":$scope.ownerShipId
	        });
			$http.post(getTopRatedStoresUrl, data, $scope.config).then(function(response){
				var dataTopRated = response.data.results.rows;
				var arrangedData = [];
				for(var inc = 0; inc < dataTopRated.length; inc++){
					var ratingValue = dataTopRated[inc][1];
					if(ratingValue == null || ratingValue == 'NA'){
						ratingValue = "0";
					}
					var starHTML = $scope.fn_convert_rating_into_star(ratingValue);
					dataTopRated[inc].push(starHTML);
					arrangedData.push(dataTopRated[inc]);
				}
				$scope.topratedstores = arrangedData;
				
				//Calling univ-pagination library to generate pagination 
				setTimeout(function(){ 
					$('#topRatedStoreTable').univpagination({
						tblName: 'topRatedStoreTable',
						tBodyName: 'topRatedStoreTableBody',
						maxRows: 10,
						cssClass: 'pull-right',
						uniqueIdentifier: '_top_rating',
						type:'minimalwithtext'
					});
				}, 1500);
				
			});
		}
		
		//Populate Competitor Comparision Store on page load and filter
		$scope.fn_competitor_comparison = function(){
			$scope.competitorcomparisionstores = null;
			var data = $.param({
				"inSourceId": $scope.competitorComparisionSelectedSource,
				"inCityId":$scope.competitorComparisionSelectedCity,
				"inAccountId":authId
	        });
			$http.post(getCompetitorComparisionStoresUrl, data, $scope.config).then(function(response){
				var dataCompetitorComparision = response.data.results.rows;
				var arrangedData = [];
				//Generating the STAR rating html
				for(var inc = 0; inc < dataCompetitorComparision.length; inc++){
					var ratingValue = dataCompetitorComparision[inc][2];
					if(ratingValue == null || ratingValue == 'NA'){
						ratingValue = "0";
					}
					var starHTML = $scope.fn_convert_rating_into_star(ratingValue);
					dataCompetitorComparision[inc].push(starHTML);
					dataCompetitorComparision[inc].push(ratingValue);
					arrangedData.push(dataCompetitorComparision[inc]);
				}
				$scope.competitorcomparisionstores = arrangedData;
			});
		}
		
		//Populate Overall ratings stores on page load and filter
		$scope.fn_overall_rating_across_stores = function(){
			$scope.overallratingsstores = null;
			var dataOverAll = null;
			var arrangedData = [];
			
			var dataOverAllRatings = $.param({
				"inCityId":$scope.overAllRatingsSelectedCity,
				"inAccountId":authId,
				"inOwnerShipId":$scope.ownerShipId
	        });
			$http.post(getOverAllRatingsStoresUrl, dataOverAllRatings, $scope.config).then(function(response){
				dataOverAll = response.data.results.rows;
				//Generating the STAR rating html
				for(var inc = 0; inc < dataOverAll.length; inc++){
					var ratingValue = dataOverAll[inc][2];
					var noOfStores = dataOverAll[inc][3];
					var eachSourceId = dataOverAll[inc][4];
					if(ratingValue === null || ratingValue == 'NA'){
						ratingValue = "0";
					}
					var starHTML = $scope.fn_convert_rating_into_star(ratingValue);
					dataOverAll[inc].push(starHTML);
					dataOverAll[inc].push(ratingValue); // Added for handel black rating value in Green yellow box
					dataOverAll[inc].push(noOfStores);
					dataOverAll[inc].push(eachSourceId);
					
					arrangedData.push(dataOverAll[inc]);
				}
				$scope.overallratingsstores = arrangedData;
			});
		}

		//SourceWiseStoreList
		//Populate Store List on page load and filter
		$scope.fn_source_wise_store_list = function(){
			$scope.resetPage();
			$scope.sourcewisestorelists = null;
			var dataStoreList = null;
			var colNames =null;
			var dataStorelists = $.param({
				"inCityId":$scope.sourceWiseStoreListSelectedCity,
				"inAccountId":authId,
				"inOwnerShipId":$scope.ownerShipId,
				"inSearchText":($scope.searchText  == '' || $scope.selectedTopic  == ' ') ? -1 : String ("%"+$scope.searchText+"%")
	        });
			$http.post(getSourcewiseStoreListsUrl, dataStorelists, $scope.config).then(function(response){
				dataStoreList = response.data.results.rows;
				colNames = response.data.results.column_names;
				
				var storestatus = colNames.indexOf("store_status");
				var indexOfStoreNameColumn = colNames.indexOf("store_name");
				if (indexOfStoreNameColumn !== -1) {
					colNames[indexOfStoreNameColumn] = "Store Name";
				}
				
				//Removing "entity" column from the heading array.
				var indexofOwnershipName = colNames.indexOf("Entity");
				if (indexofOwnershipName != -1) {
					colNames.splice("Entity", 1);
				}
				
				for(var inc = 0; inc < dataStoreList.length; inc++){
					var nullValue1 = dataStoreList[inc][1];
					var nullValue2 = dataStoreList[inc][2];
					var nullValue3 = dataStoreList[inc][3];
					var nullValue4 = dataStoreList[inc][4];
					
					var storeUrl = dataStoreList[inc][10];
					
					var nullLastValue1 = dataStoreList[inc][11];
					var nullLastValue2 = dataStoreList[inc][12];
					var nullLastValue3 = dataStoreList[inc][14];
					
					var status = dataStoreList[inc][storestatus];
			
					if(nullValue1 == null || nullValue1 == 'NA'){
						nullValue1 = "NA";
						if(status=='closed')
							nullValue1 = "Store Closed";
					}
					if(nullValue2 == null || nullValue2 == 'NA'){
						nullValue2 = "NA";
						if(status=='closed')
							nullValue1 = "Store Closed";
					}
					if(nullValue3 == null || nullValue3 == 'NA'){
						nullValue3 = "NA";
						if(status=='closed')
							nullValue1 = "Store Closed";
					}
					if(nullValue4 == null || nullValue4 == 'NA'){
						nullValue4 = "NA";
						if(status=='closed')
							nullValue1 = "Store Closed";
					}
					
					if(nullLastValue1 == null || nullLastValue1 == 'NA'){
						nullLastValue1 = "NA";
						if(status=='closed')
							nullLastValue1 = "Store Closed";
					}
					if(nullLastValue2 == null || nullLastValue2 == 'NA'){
						nullLastValue2 = "NA";
						if(status=='closed')
							nullLastValue2 = "Store Closed";
					}
					if(nullLastValue3 == null || nullLastValue3 == 'NA'){
						nullLastValue3 = "NA";
						if(status=='closed')
							nullLastValue3 = "Store Closed";
					}
					
					dataStoreList[inc].push(nullValue1);
					dataStoreList[inc].push(nullValue2);
					dataStoreList[inc].push(nullValue3);
					dataStoreList[inc].push(nullValue4);
					dataStoreList[inc].push(nullLastValue1);
					dataStoreList[inc].push(nullLastValue2);
					dataStoreList[inc].push(nullLastValue3);
					dataStoreList[inc].push(storeUrl);
				}
				$scope.sourcewisestorelists = dataStoreList;
				//$scope.sourcewisestorelistsColName = colNames;
				$scope.sourcewisestorelistsColName.push('Store Name');
				$scope.sourcewisestorelistsColName.push('Zomato');
				$scope.sourcewisestorelistsColName.push('Food Panda');
				//$scope.sourcewisestorelistsColName.push('Magicpin');
				$scope.sourcewisestorelistsColName.push('Swiggy');
			});
		}
		
		//Populate Bottom Rated Store on page load and filter
		$scope.fn_bottom_rated_stores = function(){
			$scope.bottomratedstores = null;
			var data = $.param({
				"inSourceId": $scope.bottomRatingsSelectedSource,
				"inCityId":$scope.bottomRatingsSelectedCity,
				"inAccountId":authId,
				"inOwnerShipId":$scope.ownerShipId
	        });
			$http.post(getBottomRatedStoresUrl, data, $scope.config).then(function(response){
				
				var dataBotRated = response.data.results.rows;
				var arrangedData = [];
				//Generating the STAR rating html
				for(var inc = 0; inc < dataBotRated.length; inc++){
					var ratingValue = dataBotRated[inc][1];
					if(ratingValue == null || ratingValue == 'NA'){
						ratingValue = "0";
					}
					var starHTML = $scope.fn_convert_rating_into_star(ratingValue);
					dataBotRated[inc].push(starHTML);
					arrangedData.push(dataBotRated[inc]);
				}
				$scope.bottomratedstores = arrangedData;
				
				//Calling univ-pagination library to generate pagination 
				setTimeout(function(){ 
					$('#botRatedStoreTable').univpagination({
						tblName: 'botRatedStoreTable',
						tBodyName: 'botRatedStoreTableBody',
						maxRows: 10,
						cssClass: 'pull-right',
						uniqueIdentifier: '_bot_rating',
						type:'minimalwithtext'
					});
				}, 1500);
			});
		}
		
		//Populate City,source wise store rating chart historic
		$scope.fn_CitySourceWiseHistoricRating = function(){
			$scope.citySourceWiseStores = null;
			var data = $.param({
				"inSourceId": $scope.chartRatingsSelectedSource,
				"inCityId":$scope.chartRatingsSelectedCity,
				"inAccountId":authId,
				"inOwnerShipId":$scope.ownerShipId,
				"start_date": $scope.ratingCitySourceWiseStoreFrom,
				"end_date": $scope.ratingCitySourceWiseStoreTo
	        });
			$http.post(getCityWiseSourceWiseStoreHistoryUrl, data, $scope.config).then(function(response){
				var dataCitySourceWiseStore = response.data.results.rows;
				var categoryDataArray = new Array();
			    var values = dataCitySourceWiseStore;
			    console.log("Values:" + values)
			    for (var i = 0; i < values.length; i++) {
			    	if (categoryDataArray.indexOf(values[i][1]) === -1) {
			    		categoryDataArray.push("NEW");
			    		categoryDataArray.push(values[i][1]);
			    		categoryDataArray.push(values[i][3]);
			    		categoryDataArray.push(values[i][0]);
			    		categoryDataArray.push(values[i][2]);
			    		
			    	} else {
			    		categoryDataArray.push(values[i][3]);
			    		categoryDataArray.push(values[i][0]);
			    		categoryDataArray.push(values[i][2]);
			    	}
			    }
			    
			    var  myString = categoryDataArray.toString(); 
			    var splitArrayData = myString.split("NEW");
			    
			    console.log("splitArrayData :" + splitArrayData)

			    var ratingStores=[];
			    for(var len = 1 ; len < splitArrayData.length; len ++){
			    	console.log("splitArrayData[len] : " + splitArrayData[len])
			    	ratingStores.push(splitArrayData[len].split(","));
			    }
			    console.log("ratingStores :" + ratingStores)
			    $scope.ratingHistoricValues = ratingStores;
			    console.log($scope.ratingHistoricValues)
			});
		}
		
		$scope.fn_top_rated_avg_rating = function(index,selectedCityID,selectedSourceID){
			$scope.topratedstoresavg = null;
			var dataTopRated = "0";
			var data = $.param({
				"inSourceId": selectedSourceID,
				"inCityId":selectedCityID,
				"inAccountId":authId,
				"inOwnerShipId":$scope.ownerShipId
	        });
			$http.post(getTopRatedStoresAvgUrl, data, $scope.config).then(function(response){
			    dataTopRated = response.data.results.rows;
			    $("#topAvg_"+index).text(dataTopRated);
			});
			
			
		}
		
		$scope.fn_bottom_rated_avg_rating = function(index,selectedCityID,selectedSourceID){
			$scope.topratedstoresavg = null;
			var dataBotRated = "0";
			var data = $.param({
				"inSourceId": selectedSourceID,
				"inCityId":selectedCityID,
				"inAccountId":authId,
				"inOwnerShipId":$scope.ownerShipId
	        });
			$http.post(getBottomRatedStoresAvgUrl, data, $scope.config).then(function(response){
				 dataBotRated = response.data.results.rows;
				 $("#botAvg_"+index).text(dataBotRated);
			});
			
		}
		
		$scope.getTopAvgRating = function(index,selectedCity,sourceID){
			$scope.fn_top_rated_avg_rating(index,selectedCity,sourceID);
			$scope.fn_bottom_rated_avg_rating(index,selectedCity,sourceID);
		}
		
		$scope.getRatingChart = function(divID,val1,val2,val3,val4,category1,category2,category3,category4) {
			
			if($scope.chartRatingsSelectedSource == 1){
				$scope.selectedSourceValue = "Zomato"; 
			} else if($scope.chartRatingsSelectedSource ==2){
				$scope.selectedSourceValue = "Food Panda";
			} else if($scope.chartRatingsSelectedSource ==4){
				$scope.selectedSourceValue = "Swiggy";
			}
			
		    var dataArrayList = new Array();
		    var categoryList = new Array();
		    if (typeof(val1) != "undefined"){
		    	dataArrayList.push(val1);
		    	categoryList.push(category1);
		    }
		    if (typeof(val2) != "undefined"){
		    	dataArrayList.push(val2);
		    	categoryList.push(category2);
		    }
		    if (typeof(val3) != "undefined"){
		    	dataArrayList.push(val3);
		    	categoryList.push(category3);
		    }
		    if (typeof(val4) != "undefined"){
		    	dataArrayList.push(val4);
		    	categoryList.push(category4);
		    }
		    
		    setTimeout(function(){ fn_generate_ratings_chart(dataArrayList,categoryList,divID,$scope.selectedSourceValue); }, 500);
		    
		};
		
		// Chenge Tab in Ratings / Ownership
		$scope.changeOwnership = function(tabId){
			$scope.sourcewisestorelistsColName=[];
			$scope.resetPage();
			$scope.ownerShipId = tabId;
			$scope.topRatingsSelectedSource = NULL_VALUE; 
			$scope.topRatingsSelectedCity = NULL_VALUE; 
			$scope.bottomRatingsSelectedSource = NULL_VALUE;  // Default 2 is set for Zomato
			$scope.bottomRatingsSelectedCity = NULL_VALUE; // Default is 1 now for kolkata
			$scope.overAllRatingsSelectedCity = NULL_VALUE; // Default is -1 now for all
			$scope.sourceWiseStoreListSelectedCity = NULL_VALUE; // Default is 1 now for kolkata
			$scope.searchText="";
			
			//Get Top and Bottom and competitior Rating Sources Onload
			$http.get(getSourcesForRatingTabUrl+'?accountId='+authId+'&type='+"topRatingSources").success( function(response) {
				var topRatingsSources = response.results.rows;
				
				topRatingsSources.unshift([NULL_VALUE,ALL_SOURCES]);
				
				$scope.topRatingsSources = topRatingsSources;
				$scope.bottomRatingsSources = topRatingsSources;
		    });
			
			
			//Get Cities Onload
			$http.get(getTopRatedCitiesOnSourceSelectionUrl+'?accountId='+authId+'&sourceId='+$scope.topRatingsSelectedSource+'&inOwnerShipId='+$scope.ownerShipId).success( function(response) {
				$scope.topRatingCities = [];
				$scope.bottomRatingCities = [];
				var topRatingCities = response.results.rows;
				if(topRatingCities.length == 0){
					$scope.topRatingsSelectedCity = NULL_VALUE;
				}
				
				$scope.topRatingCities = topRatingCities;
				$scope.topRatingCities.unshift([NULL_VALUE,ALL_CITIES]);
				
				$scope.bottomRatingCities = $scope.topRatingCities;
		    });
			
			//Get overAllRatingsCities and sourceWiseStoreListCities Cities Onload
			$http.get(getTopRatedCitiesOnSourceSelectionUrl+'?accountId='+authId+'&sourceId=-1&inOwnerShipId='+$scope.ownerShipId).success( function(response) {
				$scope.overAllRatingsCities = response.results.rows;
				$scope.overAllRatingsCities.unshift([NULL_VALUE,ALL_CITIES]);
				$scope.sourceWiseStoreListCities = $scope.overAllRatingsCities;
		    });

			//Calling methodes on page load
			$scope.fn_source_wise_store_list();
			$scope.fn_bottom_rated_stores();
			$scope.fn_overall_rating_across_stores();
			$scope.fn_top_rated_stores();
			$scope.fn_CitySourceWiseHistoricRating();
		}
		
		// Convert Rating value into Stars
		$scope.fn_convert_rating_into_star = function(ratingValue){
			var arrRatingValue = ratingValue.toString().split(".");
			var dynamicHTML = "";
			if(arrRatingValue.length == 2){
				//Adding Full stars
				for(var xa = 0; xa<parseInt(arrRatingValue[0]); xa++){
					dynamicHTML +='<i class="fa fa-star" aria-hidden="true"></i>';
				}
				
				// Adding half blank stars
				dynamicHTML +='<i class="fa fa-star-half-o" aria-hidden="true"></i>';
				
				var halfStarB = 4 - parseInt(arrRatingValue[0]);
				
				// Adding blank stars
				for(var xb = 0; xb<halfStarB; xb++){
					dynamicHTML +='<i class="fa fa-star-o" aria-hidden="true"></i>';
				}
				
			} else {
				//Adding Full stars
				for(var xc = 0; xc<parseInt(arrRatingValue); xc++){
					dynamicHTML +='<i class="fa fa-star" aria-hidden="true"></i>';
				}
				
				var halfStarD = 5 - parseInt(arrRatingValue);
				
				// Adding blank stars
				for(var xd = 0; xd<halfStarD; xd++){
					dynamicHTML +='<i class="fa fa-star-o" aria-hidden="true"></i>';
				}
			}
			return dynamicHTML;
		}
		
		//Download CSV
		$scope.fn_download_csv = function() {
			var url = $location.absUrl().split('?')[0];
			url = url.replace('index.jsp','');
			var finalUrl = url.trim();
			var strSearchText = ($scope.searchText  == '' || $scope.selectedTopic  == ' ') ? -1 : String ($scope.searchText);
			$window.open(finalUrl+'GetDownloadservlate?inCityId='+$scope.sourceWiseStoreListSelectedCity+'&inAccountId='+authId+'&inOwnerShipId='+$scope.ownerShipId+'&inSearchText='+strSearchText, '_blank');
		}
		
		//Reset the search text box on changing cities
		$scope.resetSearchTextBox = function(){
			$scope.searchText="";
		}
		
		// Search Store list 
		$scope.fn_search_stores = function() {
			if($scope.searchText.trim() != ""){
				$scope.sourcewisestorelistsColName=[];
				$scope.sourceWiseStoreListSelectedCity = $scope.sourceWiseStoreListSelectedCity;
				$scope.fn_source_wise_store_list();
			}
		}
		
		//Parsing HTML to render
		$scope.toTrustedHTML = function (html) {
		    return $sce.trustAsHtml(html);
		  };
		  
   	   $scope.resetPage = function(){
		    $scope.currentPage = 1;
		    $scope.topRatedStoreCurrentPage = 1;
		}
		
	});
})(window.angular);