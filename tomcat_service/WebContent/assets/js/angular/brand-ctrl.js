// this file contains controller for brand functionalities
(function(angular) {
	
	angular.module('app').controller('BrandController', function ($scope, $http, $filter, $routeParams, $location){
		$scope.$parent.activeMenu = 'brand';
		$scope.$parent.latestNewsSection = "0";
		$scope.curTabID = 5;
		$scope.$parent.initialPageload = true;
		
		$scope.selectedBrandDuration = 0;
		
		$scope.summarymatrics = {};
		$scope.selectedSentimentMatricsSource = 1;
		//$scope.selectedCompetitiveComparisonSource = 1;
		$scope.wordcloudSource = 1;
		$scope.brandTimelineSource = 1;
		$scope.competitivecomparison = [];
		$scope.ownsocialposts = [];
		$scope.ownsocialtweets = [];
		$scope.usersocialposts = [];
		$scope.usersocialtweets = [];
		$scope.topSentimentAttributeArray = [];
		
		$scope.noDataCheckTopSentimentAttr = '';
		$scope.noDataCheckTopHashTag = '';
		
		$scope.brandPageSources = [];
		$scope.brandPageSources.unshift(['Facebook', 1],['Twitter', 2]);
		
		$scope.selectedDurationName = "Day";

		
		if(authData.split('$')[5] == 0){
			$location.path( "/addinfo" );
		}
		
		
		//alert($scope.activeMenu);
		if(authData.split('$')[5] == 0){
			$scope.addinfo = true;
			$location.path( "/addinfo" );
		}
		else if(authData.split('$')[4] == 0)
			$location.path( "/gettingdata" );
		else {
			
			// Get tab details from table 
			var dataKpi = $.param({
				"inTabId": $scope.curTabID
	        });
			
			$http.post(kpiDataUrl, dataKpi, $scope.$parent.config).then(function(response){
				var tabResponse = response.data.results.rows;
				
				for (var i=0; i<tabResponse.length;i++) {
					 if(tabResponse[i][0] == "fn_populate_summary_matrics" && tabResponse[i][1] == "y") {
						$scope.fn_populate_summary_matrics();
					} else if(tabResponse[i][0] == "fn_populate_competitive_comparison" && tabResponse[i][1] == "y") {
						$scope.fn_populate_competitive_comparison();
					} else if(tabResponse[i][0] == "fn_own_social_post_tweet" && tabResponse[i][1] == "y") {
						$scope.fn_own_social_post();
						$scope.fn_own_social_tweet();
					} else if(tabResponse[i][0] == "fn_user_social_post_tweet" && tabResponse[i][1] == "y") {
						$scope.fn_user_social_post();
						$scope.fn_user_social_tweet();
					} else if(tabResponse[i][0] == "fn_brand_wordcloud" && tabResponse[i][1] == "y") {
						$scope.fn_generateWordcloudSentimentAttribute();
						$scope.fn_generateWordcloudTopHashTag();
					} else if(tabResponse[i][0] == "fn_brand_timeline" && tabResponse[i][1] == "y") {
						$scope.fn_brand_timeline();
					} else if(tabResponse[i][1]== "n") {
						$('#'+tabResponse[i][2]).hide();
					}
			    }
			});
			
		}
		
		// Function for generating SUMMARY MATRICS Kpi.
		$scope.fn_populate_summary_matrics = function(){
			
			if($scope.selectedBrandDuration == "0"){
				$scope.selectedDurationName = "Day";
			} else if ($scope.selectedBrandDuration == "1"){
				$scope.selectedDurationName = "Week";
			} else {
				$scope.selectedDurationName = "Month";
			}
			
			var summaryMatricsCols = [];
			var summaryMatricsData = [];
			var data = $.param({
				"inSourceId": $scope.selectedSentimentMatricsSource,
				"inDuration":$scope.selectedBrandDuration,
				"inAccountId" : authId
	        });
			$http.post(getSummaryMatricsUrl, data, $scope.config).then(function(response){
				 summaryMatricsCols = response.data.results.column_names;
				 summaryMatricsData = response.data.results.rows[0];
				
				 for (var inc = 0; inc < summaryMatricsCols.length; inc++){
					 $scope.summarymatrics[summaryMatricsCols[inc]] = parseInt(summaryMatricsData[inc]);
				 }
			});
			
		}
		
		// Function for Competitive Comparison Kpi.
		$scope.fn_populate_competitive_comparison = function(){
			//alert($scope.selectedCompetitiveComparisonSource);
			var competitiveComparisonCols = [];
			var competitiveComparisonData = [];
			var data = $.param({
				"inDuration" : $scope.selectedBrandDuration,
				//"inSourceId" : $scope.selectedCompetitiveComparisonSource,
				"inSourceId" : $scope.selectedSentimentMatricsSource,
				"inAccountId" : authId
	        });
			
			$http.post(getCompetitiveComparisonUrl, data, $scope.config).then(function(response){
				competitiveComparisonCols = response.data.results.column_names;
				competitiveComparisonData = response.data.results.rows;
				$scope.competitivecomparison = competitiveComparisonData;
			});
		}
		
		// Own Social Posts
		$scope.fn_own_social_post = function(){
			var ownSocialPostCols = [];
			var ownSocialPostData = [];
			var data = $.param({
				"inDuration" : 1,
				"inAccountId" : authId,
				"posttype":"post"
	        });
			
			$http.post(getOwnSocialPostUrl, data, $scope.config).then(function(response){
				ownSocialPostCols = response.data.results.column_names;
				ownSocialPostData = response.data.results.rows;
				$scope.ownsocialposts = ownSocialPostData;
			});
		}
		
		//Own social tweets
		$scope.fn_own_social_tweet = function(){
			var ownSocialTweetCols = [];
			var ownSocialTweetData = [];
			var data = $.param({
				"inDuration" : 1,
				"inAccountId" : authId,
				"posttype":"tweet"
	        });
			
			$http.post(getOwnSocialTweetUrl, data, $scope.config).then(function(response){
				ownSocialTweetCols = response.data.results.column_names;
				ownSocialTweetData = response.data.results.rows;
				$scope.ownsocialtweets = ownSocialTweetData;
			});
		}
		
		// User Social Posts
		$scope.fn_user_social_post = function(){
			var userSocialPostCols = [];
			var userSocialPostData = [];
			var data = $.param({
				"inDuration" : 1,
				"inAccountId" : authId,
				"posttype":"post"
	        });
			
			$http.post(getUserSocialPostUrl, data, $scope.config).then(function(response){
				userSocialPostCols = response.data.results.column_names;
				userSocialPostData = response.data.results.rows;
				$scope.usersocialposts = userSocialPostData;
			});
		}
		
		//user social tweets
		$scope.fn_user_social_tweet = function(){
			var userSocialTweetCols = [];
			var userSocialTweetData = [];
			var data = $.param({
				"inDuration" : 1,
				"inAccountId" : authId,
				"posttype":"tweet"
	        });
			
			$http.post(getUserSocialTweetUrl, data, $scope.config).then(function(response){
				userSocialTweetCols = response.data.results.column_names;
				userSocialTweetData = response.data.results.rows;
				$scope.usersocialtweets = userSocialTweetData;
			});
		}
		
		var blankArray = [];
		// Function for Wordcloud sentiment attribute
		$scope.fn_generateWordcloudSentimentAttribute = function(){
			var wordcloudSentimentAttrCols = [];
			var wordcloudSentimentAttrData = [];
			var data = $.param({
				"inDuration" : 1,
				"inAccountId" : authId,
				"inSourceId" : $scope.wordcloudSource,
				"inChartName" : "WordcloudSentimentAttribute"
	        });
			
			$http.post(getWordcloudUrl, data, $scope.config).then(function(response){
				
				var wordcloudSentimentAttrentity = response.data[0].rows;				
				var wordcloudSentimentAttrentityHead = response.data[0].columnNames;
				var wordcloudSentimentAttrArray = [];
			    var entitySentimentAttrArray = [];
				
			    // entity objects are formed with properties and pushed in entityArray
			    for(var i=0;i<wordcloudSentimentAttrentity.length;i++){
			    	var entity = {};
					entity.text = wordcloudSentimentAttrentity[i][0];
					entity.size = wordcloudSentimentAttrentity[i][2];
					entity.color = wordcloudSentimentAttrentity[i][1];
					if(wordcloudSentimentAttrentity[i][1]=='#679f02'){
						entity.sentiment='Positive';
					}else if(wordcloudSentimentAttrentity[i][1]=='#FF5733'){
						entity.sentiment='Negative';
					}else{
						entity.sentiment='Neutral';
					}
					
					entity.type = "context";
					
					entitySentimentAttrArray.push(entity);
			    }
				
			    $scope.noDataCheckTopSentimentAttr = entitySentimentAttrArray;
			    console.log('---------------SentimentAttr----------->');
			    console.log(entitySentimentAttrArray);
				generateWordcloudSentimentAttribute(entitySentimentAttrArray,blankArray);
			});
		}
		
		//Function for Wordcloud top has tags
		$scope.fn_generateWordcloudTopHashTag = function(){
			var data = $.param({
				"inDuration" : 1,
				"inAccountId" : authId,
				"inSourceId" : $scope.wordcloudSource,
				"inChartName" : "TopHashTag"
	        });
			
			$http.post(getWordcloudUrl, data, $scope.config).then(function(response){
				
				var wordcloudHashTagentity = response.data[0].rows;				
				var wordcloudHashTagentityHead = response.data[0].columnNames;
				var wordcloudHashTagArray = [];
			    var topHashTagArray = [];
				
			    // entity objects are formed with properties and pushed in entityArray
			    for(var i=0;i<wordcloudHashTagentity.length;i++){
			    	var entity = {};
					entity.text = wordcloudHashTagentity[i][0];
					entity.size = wordcloudHashTagentity[i][2];
					entity.color = wordcloudHashTagentity[i][1];
					if(wordcloudHashTagentity[i][1]=='#679f02'){
						entity.sentiment='Positive';
					}else if(wordcloudHashTagentity[i][1]=='#FF5733'){
						entity.sentiment='Negative';
					}else{
						entity.sentiment='Neutral';
					}
					
					entity.type = "context";
					topHashTagArray.push(entity);
			    }
				$scope.noDataCheckTopHashTag = topHashTagArray;
			    generateWordcloudTopHashTag(topHashTagArray,blankArray);
			});
		}
		
		//Function for Brand Timeline graph.
		$scope.fn_brand_timeline = function(){
			var data = $.param({
				"inSourceId" : $scope.brandTimelineSource,
				"inAccountId" : authId
	        });
			
			$http.post(getBrandTimelineUrl, data, $scope.config).then(function(response){
				
				var mention = [];
				var netsentiment = [];
				var potentialimpression = [];
				var dateRange = [];
				
				var brandTimeLineCols = response.data.results.column_names;
				var brandTimeLineData = response.data.results.rows;
				
				for(var inc = 0; inc <brandTimeLineData.length; inc ++){
					mention.push(brandTimeLineData[inc][1]);
					netsentiment.push(Math.round(brandTimeLineData[inc][2]));
					potentialimpression.push(brandTimeLineData[inc][3]);
					dateRange.push(brandTimeLineData[inc][0]);
				}
				
				generateBrandTimelineGraph(mention,netsentiment,potentialimpression,dateRange);
			});
		}
		
		// Page load events
		
		setTimeout(function(){
			$('.user-social-post-wapper').slimScroll({
					height: '400px',
					color: '#000',
					size : '5px',
					wheelStep : 10,
					distance : '0',
					alwaysVisible : true,
			});
			
		},4000);
				
	});

})(window.angular);