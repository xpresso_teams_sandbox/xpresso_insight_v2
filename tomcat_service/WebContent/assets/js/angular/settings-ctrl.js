// this file contains controller for settings functionalities
(function(angular) {
	
	angular.module('app').controller('SettingsController', function ($scope, $http, $filter, $routeParams, $location){
		$scope.$parent.activeMenu = 'settings';
		$scope.$parent.latestNewsSection = "0";
		$scope.$parent.initialPageload = true;
		$scope.wrongpassword = false;
		$('#adminEmail').val(authEmail);
		$('#adminDetails').val(authData);
		if(authData.split('$')[5] == 0){
			$location.path( "/addinfo" );
		}
		
		if($routeParams.reset == 0){
			$scope.wrongpassword = true;
		}else{
			$http.post(getUserDetailsUrl, authEmail, $scope.config).then(function(response){
				$scope.userInfo = response.data;
			}, function(){console.log('Error');});
		}
		
		$scope.sourceFilter = function(topicProfile) {
			return function(detail) {
				return topicProfile.profileDetails.indexOf(Number(detail.sourceTypeId)) != -1?true:false;
			}
		}
		
		$scope.submit = function () {
			$('.modal').modal('hide');
			alert('Submited');
		};
		
		$scope.changePrimComp = function(value) {
		    //alert(value)
		    $scope.$parent.primCompany = value;
		};
		
		// sql for getting Companies
		//alert("AUTH ID : " + authId);
		/*$scope.companies = [
			                {id: 7, text: 'McDonalds'},
			                {id: 8, text: 'Walmart'}
			              ];*/
		var data = $.param({
			"inAccountId": authId,
        });
		$http.post(getCompaniesUrl, data, $scope.config).then(function(response){
			//alert(response.data.results.rows);
			$scope.companies = response.data.results.rows;
		    /*for(var i=0;i<$scope.entitiesTF.length;i++){
				$scope.entitiesTF[i].complaint = Math.round($scope.entities[i].negative*$scope.entities[i].reviewCount/100);
		    }*/
		});
		
		/*$http.get(getCompaniesUrl).success( function(response) {
			$scope.companies = response;
			// formatting the published datetime as dd-MM-yyyy HH:mm
			$scope.headlinesData.forEach( function (headlineData){
				headlineData.publishDatetime = headlineData.publishDatetime.substring(8,10)+headlineData.publishDatetime.substring(4,8)+headlineData.publishDatetime.substring(0,4)+headlineData.publishDatetime.substring(10,16);
			});

		});*/
		
			
		    // Update the selection when a checkbox is clicked.
		    $scope.updateSecondarySelection = function($event, id) {
		    	//alert($scope.$parent.selectedSecondaryCompany.length - 1)
		    	
		    	var checkbox = $event.target;
		    	if($scope.$parent.selectedSecondaryCompany.length - 1 >=4 ){
		           
		            var action = 'remove';
			        if (action == 'remove' && $scope.$parent.selectedSecondaryCompany.indexOf(id) != -1) $scope.$parent.selectedSecondaryCompany.splice($scope.$parent.selectedSecondaryCompany.indexOf(id), 1);
		            $(checkbox).prop('checked', false); 
		            
		            /*$('#alertModal').modal("show");
		            $('#alertModal .modal-content').addClass('modal-danger');
		            $('#alertTxt').html("More than 5 selection is not allowed !!!");*/
		            
		            return false;
		    	}
		        
		        var action = (checkbox.checked ? 'add' : 'remove');
		        if (action == 'add' & $scope.$parent.selectedSecondaryCompany.indexOf(id) == -1) $scope.$parent.selectedSecondaryCompany.push(id);
		        if (action == 'remove' && $scope.$parent.selectedSecondaryCompany.indexOf(id) != -1) $scope.$parent.selectedSecondaryCompany.splice($scope.$parent.selectedSecondaryCompany.indexOf(id), 1);
		    };

		    $scope.isCheckedSelectedCompany = function(id){
		        return $scope.$parent.selectedSecondaryCompany.indexOf(id);
		    }
		    

		
		setTimeout(function(){
			var conH = $('.setting-wrapper-content > .block-body').height();
			//$('.admin-summary').css('height', conH);
			console.log(conH);
			$('.admin-summary').slimScroll({
					height: conH,
					color: '#000',
					size : '4px',
					wheelStep : 10,
					distance : '2px',
			});
			
		},4000);
	});

})(window.angular);