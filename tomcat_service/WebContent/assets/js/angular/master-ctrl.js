// this file contains controller attached to the whole body
(function(angular) {
	
	/**
	 * Author Name: Urmi Saha
	 * Create Date: 03-11-2016
	 * Last Modified: Ashis Nayak: 14-12-2017
	 * Input: 
	 * Output: 
	 * Method Insight: controller(Master Controller) attached to the whole body,
	 * parent controller of all other controllers attached to each page,
	 * deals with functions common to all pages
	 */
	angular.module('app').controller('MasterController', function($scope, $http,$window,$interval,$routeParams, $filter, $location, loadingService,$sce){
		$scope.loadingService = loadingService;
		$scope.pageAccessccessToken = "";
		$scope.latestNewsSection = "1";
		$scope.wordCloudAspectListCurPage = 1;
		$scope.contextDetailsCurPage = 1;
		$scope.Math = window.Math;
		$scope.pageLoad = true;
		$scope.addInfo_message;
		$scope.$on('eventName', function(event, data) {
			$scope.addInfo_message = data.message;
			$scope.isAddinfo = true;
		});
		$scope.$on('saveDetails', function(event, data) {
			$scope.addInfo_message = data.message;
			$scope.isAddinfo = false;
		});
		$scope.addInfoCheck = function($event){
			return !$scope.isAddinfo;
		}
		
		$scope.initialPageload = false;
		$scope.curTabID = 1;
		
		$('.showHide').css('visibility', 'hidden');
		/**
		 * The following block of constants
		 * initialize the variables which holds constant values for display throughout the application
		 */
		$scope.activeMenu = '';
		$scope.radianCXHeader = 'Omni-Channel Customer Experience Management Suite';
		$scope.authName = authName;
		$scope.authEmail = authEmail;		
		$scope.navbarWelcomeUser = user_welcome_message + " " + authName;
		
		
		$scope.sources = [];							// contains the sources
		$scope.source_categories = [];					// contains the source categories
	    $scope.topics = [];								// contains the contexts
	    $scope.feedbacktopics = [];
	    $scope.feedbackQuestionArr=[];
	    $scope.feedbackTextArr=[];
	    $scope.topicsFetched = false;					// for checking whether topics have been fetched
	    $scope.sourcesFetched = false;					// for checking whether sources have been fetched
	    $scope.zomatoCities = [];						// contains the exhaustive list of cities from zomato table
	    $scope.sentimentalTopics = [];					// list of topics to be shown for pie chart
	    $scope.sentimentalCities = [];					// list of cities to be shown for pie chart
	    $scope.sentimentalSources = [];					// list of sources to be shown for pie chart
	    $scope.topSnapshotSources = [];
	    $scope.sentimentalSource = BLANK_STRING;		// holds the source value of Overall Sentiment Distribution initiated with BLANK
	    $scope.advocateSource = ALL_SOURCES;					// holds the source value of Top Advocates initiated with 'All Sources'
		$scope.sentimentalRegion = ALL_REGIONS;		// holds the region value of Overall Sentiment Distribution initiated with BLANK
		$scope.ratingsTopRatedSource = BLANK_STRING;
		$scope.sentimentalCity = BLANK_STRING;			// holds the city value of Overall Sentiment Distribution initiated with BLANK
		$scope.sentimentalStore = BLANK_STRING;			// holds the store value of Overall Sentiment Distribution initiated with BLANK
		$scope.sentimentalTopic = NULL_VALUE;			// holds the aspect/topic value of Overall Sentiment Distribution initiated with BLANK
		$scope.selectedContextMonthly = NULL_VALUE;				// holds the selected context in Competitive Happiness Index initiated with 'All Contexts'
		$scope.selectedSentiment="All";
	    //$scope.snapshotDateFilter = false;				// dropdown containing datepickers for custom filter in snapshot, initially kept closed
	    $scope.snapshotInterval = MONTHLY;				// holds the snapshot filter value initiated with Monthly value
		$scope.selectedSource = BLANK_STRING;			// holds the source value of Explore page filter initiated with BLANK
		$scope.selectedSourceCategory = SOCIAL_MEDIA;	// holds the source value of Explore page filter initiated with 'Social Media'
		$scope.selectedTopics = [];						// holds the aspect/topic values of Explore page filter
		$scope.selectedSentimentalTopics = [];			// holds the aspect/topic values of Overall Sentiment section page filter
		$scope.dashboardFrom = $scope.yesterday;		// datepicker 'From date' in Explore page filter initially kept blank
		$scope.dashboardTo = $scope.today;				// datepicker 'To date' in Explore page filter initially kept blank
		$scope.snapshotFrom = BLANK_STRING;				// datepicker 'From date' in snapshot initially kept blank
		$scope.snapshotTo = BLANK_STRING;				// datepicker 'To date' in snapshot initially kept blank
		$scope.alertType = BLANK_STRING;				// holds the type of alert to be show
		$scope.alertText = BLANK_STRING;				// holds the text to be shown in alert box
		$scope.snapshotTitle = 'Month';					// holds the text to be shown in snapshot title initially kept "month"
		$scope.snapshotData = [];
		$scope.snapshotDataDaily = [];
		$scope.snapshotDataWeekly = [];
		$scope.snapshotDataMonthly = [];
		$scope.dashboardTopicsForDiv = [];
		$scope.dashboardTopicsForDiv2 = [];
		$scope.deepListTopics=[];
		/*$scope.periodicPrevious = $scope.yesterday +' - '+ $scope.today;
		$scope.periodicCurrent = $scope.yesterday +' - '+ $scope.today;*/
		
		$scope.selectedSummaryCategory = "0";
		$scope.selectedSummaryQuestion = "0";
		
		$scope.snapshotDurations = [];
		$scope.tabMetaData = [];
		$scope.snapshotDateFilter = 0;
		$scope.snapshotActiveTab = "Daily-snapshot-wapper";
		
		$scope.deepListTabFilterdata = {};
		$scope.$parent.deepListSocialNewPage = 1;
		$scope.isFromBackButton = false;
		
		$scope.LoggedInPrimaryCompanyID = authPrimaryCompanyID;
		
		// Here we can set default secondary company selection
		// Currently its blank
		$scope.selectedSecondaryCompany = [];
		$scope.selectedSecondaryCompany.push(authPrimaryCompanyID);
		
		$scope.selectedSecondaryCompany.push(parseInt(defaultCompetitor)); // Adding one default secondary company
		if(companyId == 2){
			$scope.domainName = 'Modern Retail Domain V.2.1';
		}else if(companyId == 1){
			$scope.domainName = 'Retail Banking Domain V.0.3';
		}else if(companyId == 8){
			$scope.domainName = 'Telecom Domain V.3.0';
		}
		
		//Current datetime in navbar
		$interval(function() {
			//var datetime = new Date().toString().split('GMT');
			var date = new Date();
			var datetime = new Date(date.valueOf() + (date.getTimezoneOffset() + authTimezoneOffset) * 60000).toString().split('GMT');
			$scope.navbardate = datetime[0].substring(0, datetime[0].length-4);
		}, 1000)
		
		// sql for getting headlines
		/*$http.get(headlinesUrl).success( function(response) {
			$scope.headlinesData = response;
			// formatting the published datetime as dd-MM-yyyy HH:mm
			$scope.headlinesData.forEach( function (headlineData){
				headlineData.publishDatetime = headlineData.publishDatetime.substring(8,10)+headlineData.publishDatetime.substring(4,8)+headlineData.publishDatetime.substring(0,4)+headlineData.publishDatetime.substring(10,16);
			});

		});
		$interval(function() {
			$http.get(headlinesUrl).success( function(response) {
				$scope.headlinesData = response;
		    });
		}, 3600000)*/
		
		$scope.sentiment = "Negative";					// holds the selected sentiment in Monthly Contextwise Sentiment initiated with 'Negative'
		
		$scope.showHideSnapshot = function(param){
			$scope.snapshotActiveTab = param;
			$('.showHide').css('visibility', 'hidden');
			$('.showHide').css('height', 0);
			$('.showHide').css('zIndex', '9999');
			setTimeout(function(){
				$('.'+param).css('visibility', 'visible');
				$('.'+param).css('height', 165 );
				$('.'+param).css('zIndex', '99999');
			},10);
		}
		
		// function for monthly contextwise sentiment by count
		$scope.fn_contextwise_sentiment_distribution = function(){
			var data = $.param({
				"account_Id": authId,
				"sentiment" : $scope.sentiment,
				"company_Id": $scope.selectedSecondaryCompany.toString()
	        });
			$http.post(contextwiseSentimentDistributionURL, data, $scope.config).then(function(response){
				chart_contextwise_sentiment_distribution(response.data.results.rows, 'value', $scope.sentiment);
			});
		}
		
		// function for monthly contextwise sentiment by percent
		$scope.fn_contextwise_sentiment_distribution_percentile = function(){
			var data = $.param({
				"account_Id": authId,
				"sentiment" : $scope.sentiment,
				"company_Id": $scope.selectedSecondaryCompany.toString()
	        });
			$http.post(contextwiseSentimentDistributionPercentageURL, data, $scope.config).then(function(response){
				chart_contextwise_sentiment_distribution(response.data.results.rows, 'percentage', $scope.sentiment);
			});
		}
		
		/**
		 * The following block of constants
		 * initialize the variables which get populated for static details
		 * corresponding to company login (authId), for use throughout the application
		 */
		$scope.sources = [];
	    $scope.topics = [];
		$scope.selectedSource = BLANK_STRING;
		$scope.selectedTopic = BLANK_STRING;
		$scope.snapshotFrom = BLANK_STRING;				// datepicker 'From date' in snapshot initially kept blank
		$scope.snapshotTo = BLANK_STRING;				// datepicker 'To date' in snapshot initially kept blank
		
		/**
		 * The following constants initialize the variables
		 * with different content type for use throughout the application
		 */
		$scope.config = {
	            headers : {
	                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
	            }
	        }
		$scope.configJSON = {
				headers : {
					'Content-Type': 'application/json'
				}
			};
		
		// get duration metadata
		$http.get(durationUrl).success( function(response) {
			//alert(response.results.rows[0][1]);
			$scope.snapshotDurations = response.results.rows;
			for (var i=0; i<$scope.snapshotDurations.length;i++) {
				//tabResponse[i][4] = toLocalDate(data[i][4]);
				//alert($scope.snapshotDurations[i][0])
				if($scope.snapshotDurations[i][0] == 0){
					$scope.fn_generate_snapshot_0($scope.snapshotDurations[i][0]);
				}
				if($scope.snapshotDurations[i][0] == 1){
					$scope.fn_generate_snapshot_1($scope.snapshotDurations[i][0]);				
				}
				if($scope.snapshotDurations[i][0] == 2){
					$scope.fn_generate_snapshot_2($scope.snapshotDurations[i][0]);
				}
		    }
	    });
		
		// Get tab details from table 
		var dataTabs = $.param({
			"inAccountId": authId
        });
		
		$scope.tabMetaData = null;
		$http.post(tabDataUrl, dataTabs, $scope.config).then(function(response){
				var tabResponse = response.data.results.rows;
				$scope.tabMetaData = tabResponse;
				$('#tab_settings').before('<li ng-click="fn_download_invalid_csv();"><i class="fa" aria-hidden="true"></i><b>Reports</b></li>');
		});
		
		$scope.updateDatetimeOfDays = function(){
			$scope.date = new Date();
			$scope.today = $filter('date')($scope.date, 'dd-MM-yyyy HH:mm');
			$scope.date.setDate($scope.date.getDate() - 1);
			$scope.yesterday = $filter('date')($scope.date, 'dd-MM-yyyy HH:mm');
			
			$scope.date.setDate($scope.date.getDate() - 5);
			$scope.lastWeek = $filter('date')($scope.date, 'dd-MM-yyyy HH:mm');
			$scope.date = new Date();
			$scope.date.setMonth($scope.date.getMonth() - 1);
			$scope.lastMonth = $filter('date')($scope.date, 'dd-MM-yyyy HH:mm');
			
			$scope.ticketDate = new Date();
			$scope.ticketToday = $filter('date')($scope.ticketDate, 'dd-MM-yyyy');
			$scope.ticketToday = $scope.ticketToday.toString() + " 23:59"
			$scope.ticketDate.setDate($scope.ticketDate.getDate());
			$scope.ticketYesterday = $filter('date')($scope.ticketDate, 'dd-MM-yyyy');
			$scope.ticketYesterday = $scope.ticketYesterday.toString() + " 00:01";
			
			$scope.newDeepListingDate = new Date();
			$scope.newDeepListingToday = $filter('date')($scope.newDeepListingDate, 'dd-MM-yyyy');
			$scope.newDeepListingToday = $scope.newDeepListingToday.toString() + " 23:59"
			// Made 100 days back to show in dev release version only
			$scope.newDeepListingDate.setDate($scope.newDeepListingDate.getDate());
			//
			$scope.newDeepListingYesterday = $filter('date')($scope.newDeepListingDate, 'dd-MM-yyyy');
			$scope.newDeepListingYesterday = $scope.newDeepListingYesterday.toString() + " 00:01";
			
			$scope.dashboardDate = new Date();
			$scope.dashboardToday = $filter('date')($scope.dashboardDate, 'dd-MM-yyyy');
			$scope.dashboardToday = $scope.dashboardToday.toString() + " 23:59"
			$scope.dashboardDate.setDate($scope.dashboardDate.getDate() - 6);
			$scope.dashboardYesterday = $filter('date')($scope.dashboardDate, 'dd-MM-yyyy');
			$scope.dashboardYesterday = $scope.dashboardYesterday.toString() + " 00:01";
			
			$scope.ratingsDate = new Date();
			$scope.ratingsToday = $filter('date')($scope.ratingsDate, 'dd-MM-yyyy');
			$scope.ratingsToday = $scope.ratingsToday.toString() + " 23:59"
			$scope.ratingsDate.setDate($scope.ratingsDate.getDate() - 14);
			$scope.ratingsYesterday = $filter('date')($scope.ratingsDate, 'dd-MM-yyyy');
			$scope.ratingsYesterday = $scope.ratingsYesterday.toString() + " 00:01";
			
			$scope.newDeepListingTopNegativeDate = new Date();
			$scope.newDeepListingTopNegativeToday = $filter('date')($scope.newDeepListingTopNegativeDate, 'dd-MM-yyyy');
			$scope.newDeepListingTopNegativeToday = $scope.newDeepListingTopNegativeToday.toString() + " 23:59"
			$scope.newDeepListingTopNegativeDate.setDate($scope.newDeepListingTopNegativeDate.getDate() - 6);
			$scope.newDeepListingTopNegativeYesterday = $filter('date')($scope.newDeepListingTopNegativeDate, 'dd-MM-yyyy');
			$scope.newDeepListingTopNegativeYesterday = $scope.newDeepListingTopNegativeYesterday.toString() + " 00:01";
			
			/*$('#ratingCitySourceWiseStoreTo').datepicker({
			    daysOfWeekDisabled: [0,6]
			});*/
		}
		$scope.updateDatetimeOfDays();
		
		$scope.fn_snapshot_filter_new = function(selection) {
			$scope.snapshotDateFilter = selection;
		}
		
		// For Daily Snapshot
		$scope.fn_generate_snapshot_0 = function(duration){
			var slider ;
			var data = $.param({
				"inAccountId": authId, 
		        "inDuration": duration
	        });
			$scope.snapshotData = null;
			$scope.Daily=null;
			$http.post(socialSnapshotdata, data, $scope.config).then(function(response){
				var data = response.data.results.rows;
				$scope.snapshotDataDaily = data;

				setTimeout(function(){
					var SlideW = $('#dailySnapshot').width()-30;
					slider = $('.dashboard-carousel-daily').bxSlider({
						slideWidth: 593,
					    minSlides: 2,
					    maxSlides: 8,
					    slideMargin: 30,
					    //moveSlides: 1,
					    infiniteLoop: false,
					  });
				},1);
			});
		}
		
		// For Weekly Snapshot
		$scope.fn_generate_snapshot_1 = function(duration){
			var slider ;
			var data = $.param({
				"inAccountId": authId, 
		        "inDuration": duration
	        });
			$scope.snapshotData = null;
			$scope.Daily=null;
			$http.post(socialSnapshotdata, data, $scope.config).then(function(response){
				var data = response.data.results.rows;
				$scope.snapshotDataWeekly = data;	
				
				//slider && slider.destroySlider();

				setTimeout(function(){
					var SlideW = $('#dailySnapshot').width()-30;
					slider = $('.dashboard-carousel-weekly').bxSlider({
						slideWidth: 593,
					    minSlides: 2,
					    maxSlides: 8,
					    slideMargin: 30,
					    //moveSlides: 1,
					    infiniteLoop: false,
					  });
				},1);
			});
		}
		
		// For Monthly Snapshot
		$scope.fn_generate_snapshot_2 = function(duration){
			var slider ;
			var data = $.param({
				"inAccountId": authId, 
		        "inDuration": duration
	        });
			$scope.snapshotData = null;
			$scope.Daily=null;
			$http.post(socialSnapshotdata, data, $scope.config).then(function(response){
				var data = response.data.results.rows;
				$scope.snapshotDataMonthly = data;
				setTimeout(function(){
					var SlideW = $('#dailySnapshot').width()-30;
					slider = $('.dashboard-carousel-monthly').bxSlider({
						slideWidth: 593,
					    minSlides: 2,
					    maxSlides: 8,
					    slideMargin: 30,
					    //moveSlides: 1,
					    infiniteLoop: false,
					  });
				},1);
			});
		}
		
		
		// sql for getting headlines
		$http.get(headlinesUrl).success( function(response) {
			$scope.headlinesData = response;
			// formatting the published datetime as dd-MM-yyyy HH:mm
			$scope.headlinesData.forEach( function (headlineData){
				headlineData.publishDatetime = headlineData.publishDatetime.substring(8,10)+headlineData.publishDatetime.substring(4,8)+headlineData.publishDatetime.substring(0,4)+headlineData.publishDatetime.substring(10,16);
			});

		});
		$interval(function() {
			$http.get(headlinesUrl).success( function(response) {
				$scope.headlinesData = response;
		    });
		}, 3600000)
		
		/*
		 * The following block of constants hold
		 * today's datetime, yesterday's datetime (exactly 24 hrs back from current datetime)
		 * and last month's datetime (exactly 1 month back from current datetime),
		 * depending on timezone offset of logged in user
		 * for use throughout the application
		 */
		
		/*// sql for getting timezone offset of user
		var data = $.param({
			"accountId": authId
		});
		$http.post(getUserTimezoneOffsetUrl, data, $scope.config).then(function(response){
			authTimezoneOffset = response.data.results.rows[0][0];
			$scope.authTimezoneOffset = authTimezoneOffset;
		
			
			var date = new Date();
			$scope.date = new Date(date.valueOf() + (date.getTimezoneOffset() + authTimezoneOffset) * 60000);
			$scope.today = $filter('date')($scope.date, 'dd-MM-yyyy HH:mm');
			$scope.date.setDate($scope.date.getDate() - 1);
			$scope.yesterday = $filter('date')($scope.date, 'dd-MM-yyyy HH:mm');
			$scope.date = new Date();
			$scope.date.setMonth($scope.date.getMonth() - 1);
			$scope.dateRangeStart = $scope.date;
			$scope.dashboardTo = '';											// value to show in date filter 'To' in dashboard 
			$scope.dashboardFrom = '';											// value to show in date filter 'From' in dashboard
		});*/
		
		
		// this function selects/deselects all options of the aspect list corresponding to the value of bool: true/false
		$scope.checkAll = function(bool,section){
			$scope.selectedTopics = [];
			$scope.selectedSentimentalTopics = [];
			for(var i=0 ; i < $scope.topics.length; i++) {
		        $scope.topics[i][2] = bool;
		        if(bool && i>0){
		        	if(section == 'explore'){
		        		$scope.selectedTopics.push( $scope.topics[i][1]);
		        	} else{
		        		$scope.selectedSentimentalTopics.push( $scope.topics[i][1]);
		        	}
		        }
		    } 
		}

		// this function shows selected/deselected options in checkbox list and puts selected values in $scope.selectedTopics
		$scope.sync = function(bool, item, section){
			//alert("check all hit" + bool + "--" + item + "--" + section)
			$scope.topics[$scope.topics.map(function(el){return el[1];}).indexOf(Number(item[1]))][2] = bool;	// check/uncheck option in checkbox list
			 
			/*
			 * bool == true: option got checked
			 * bool == false: option got unchecked
			 */
			/*if(item[1] == -1){									// checkbox against 'All Contexts' option got checked
	    		if(section == 'explore'){
	    			$scope.selectedTopics = [];
	    		}else{
	    			$scope.selectedSentimentalTopics = [];
	    		}
	    		$scope.checkAll(bool,section);
	    	}*/
			
		    if(bool){
			    // add item
		    	if(item[1] == -1){									// checkbox against 'All Contexts' option got checked
		    		if(section == 'explore'){
		    			$scope.selectedTopics = [];
		    		}else{
		    			$scope.selectedSentimentalTopics = [];
		    		}
		    		$scope.checkAll(bool,section);
		    	}else{
		    		if(section == 'explore'){
		    			$scope.selectedTopics.push(item[1]);
		    		} else{
		    			$scope.selectedSentimentalTopics.push(item[1]);
		    		}
		    	}
		    } else {
		        // remove item
		    	if(item[1] == -1){									// checkbox against 'All Contexts' option got unchecked
		    		if(section == 'explore'){
		    			$scope.selectedTopics = [];
		    		}else{
		    			$scope.selectedSentimentalTopics = [];
		    		}
		    		$scope.checkAll(bool,section);
		    	}else{
		    		$scope.topics[0][2] = bool;				// anything gets deselected necessarily means "All Contexts" option is not selected
		    		if(section == 'explore'){
		    			$scope.selectedTopics.splice($scope.selectedTopics.map(function(el){return el;}).indexOf(Number(item[1])),1);	// remove unchecked item from selected list of items
		    		}else{
		    			$scope.selectedSentimentalTopics.splice($scope.selectedSentimentalTopics.map(function(el){return el;}).indexOf(Number(item[1])),1);	// remove unchecked item from selected list of items
		    		}
		    	}
		    }
		};
		
		// function for disabling dates in angular datetimepicker for custom filter
		$scope.dateRangeValidationCustomFilter = function($view, $dates, $leftDate, $upDate, $rightDate) {
		  var activeDate,i;
		  $scope.dateRangeEnd = new Date();
		  $scope.dateRangeStart = new Date();
		  $scope.dateRangeStart.setDate($scope.dateRangeStart.getDate() - 1);
		  if ($scope.dateRangeStart) {
		    activeDate = moment($scope.dateRangeStart).subtract(1, $view).add(1, 'minute');
			  //activeDate = moment($scope.dateRangeStart).subtract(10, $view).add(1, 'minute');
		    for (i = 0; i < $dates.length; i++) {
		      if ($dates[i].localDateValue() <= activeDate.valueOf())
		        $dates[i].selectable = false;            
		    } 
		  }
		  if ($scope.dateRangeEnd) {
		    activeDate = moment($scope.dateRangeEnd).add(0, 'day');
		    for (i = 0; i < $dates.length; i++) {
		      if ($dates[i].localDateValue() >= activeDate.valueOf())
		    	  $dates[i].selectable = false;
		    }
		  }
		}

		// function for disabling dates in angular datetimepicker
		$scope.dateRangeValidation = function($view, $dates, $leftDate, $upDate, $rightDate) {
		  var activeDate;
		  $scope.dateRangeEnd = new Date();
		  $scope.dateRangeStart = new Date();
		  //$scope.dateRangeStart.setMonth($scope.dateRangeStart.getMonth() - 1);
		  if ($scope.dateRangeStart) {
		    //activeDate = moment($scope.dateRangeStart).subtract(1, $view).add(1, 'minute');
			//Calculating number of days in  last month first date and today, adding this into the calendar date range 
		  	var dateA = new Date();
		  	var currentDate = dateA.getDate();
		  	var currentMonth = dateA.getMonth();
		  	var currentYear = dateA.getFullYear();
		  	currentMonth = parseInt(currentMonth)-1;

		  	var previousMonthToBe = [];
		  	if(parseInt(currentMonth) == -1){
		  		currentMonth = 0;
		  	}
		  	if(parseInt(currentMonth) == 0){
		  		previousMonthToBe = [9,10,11];
		  	} else if(parseInt(currentMonth) == 1){
		  		previousMonthToBe = [10,11,0];
		  	} else if(parseInt(currentMonth) == 2){
		  		previousMonthToBe = [11,0,1];
		  	} else if(parseInt(currentMonth) == 3){
		  		previousMonthToBe = [0,1,2];
		  	} else if(parseInt(currentMonth) == 4){
		  		previousMonthToBe = [1,2,3];
		  	} else if(parseInt(currentMonth) == 5){
		  		previousMonthToBe = [2,3,4];
		  	} else if(parseInt(currentMonth) == 6){
		  		previousMonthToBe = [3,4,5];
		  	} else if(parseInt(currentMonth) == 7){
		  		previousMonthToBe = [4,5,6];
		  	} else if(parseInt(currentMonth) == 8){
		  		previousMonthToBe = [5,6,7];
		  	} else if(parseInt(currentMonth) == 9){
		  		previousMonthToBe = [6,7,8];
		  	} else if(parseInt(currentMonth) == 10){
		  		previousMonthToBe = [7,8,9];
		  	} else if(parseInt(currentMonth) == 11){
		  		previousMonthToBe = [8,9,10];
		  	}
		  	
		  	var numdays = 0;
		  	for(var xx = 0; xx < previousMonthToBe.length; xx++){
		  		numdays += daysInMonth(previousMonthToBe[xx],currentYear);
		  	}
		  	
		  	var totalDaysToEnabled = numdays+currentDate;
		    //activeDate = moment($scope.dateRangeStart).subtract(30, $view).add(1, 'minute');
		  	activeDate = moment($scope.dateRangeStart).subtract(totalDaysToEnabled, $view).add(1, 'minute');
		    for (var i = 0; i < $dates.length; i++) {
		      if ($dates[i].localDateValue() <= activeDate.valueOf())
		        $dates[i].selectable = false;            
		    } 
		  }
		  if ($scope.dateRangeEnd) {
		    activeDate = moment($scope.dateRangeEnd).add(0, 'day');
		    for (var i = 0; i < $dates.length; i++) {
		      if ($dates[i].localDateValue() >= activeDate.valueOf())
		    	  $dates[i].selectable = false;
		    }
		  }
		}
		
		/**
		 * the following function is used whenever an alert needs to be shown.
		 * The type of alert (success/warning/danger)-[type],
		 * and the text to be shown as the alert message-[message]
		 * need to be sent as parameters.
		 */
		$scope.callAlert = function(type, message){
			$scope.alertType = type;
			$scope.alertText = message;
            $('#alertModal').modal("show");
        }
		
		// For filter by interval starts..
		// static part
		/*$http.post('assets/JSON/filter_interval.json', data, $scope.config).success(function(resp){
			$scope.intervals = resp;
			
		}).then(function(resp){
			$scope.categories = $scope.intervals.dropdown_options[0];
			$scope.interval = $scope.intervals.filters[0].value;
			
			$scope.getSnapshotsData();
			
			$scope.activeInterval = $scope.intervals.filters[0];
		});*/
		
		// dynamic part
		var  dataSnap = $.param({
			"accountId": authId
		})
		
		/*$http.post(snapshotDropdownUrl, dataSnap, $scope.config).success(function(resp){
			$scope.intervals = JSON.parse(resp.results);
			
		}).then(function(resp){
			$scope.categories = $scope.intervals.dropdown_options[0];
			$scope.interval = $scope.intervals.filters[0].value;
			
			$scope.getSnapshotsData();
			
			$scope.activeInterval = $scope.intervals.filters[0];
		});*/
		
		//For filter by interval
		$scope.isActive = function(options, from_date, to_date){
			$scope.activeInterval = options;
			$scope.interval = options.value;
			if(!options.isCustom){
				$scope.from_date = "";
				$scope.to_date = "";
				$scope.getSnapshotsData();
			}
			$scope.show_filter = true;
		}
		
		$scope.fn_snapshot_filter = function(from_date, to_date){
			//$scope.from_date = from_date;
			//$scope.to_date = to_date;
			console.log("from_date, to_date::",from_date, to_date, $scope);
		};
		
		// For snapshot blocks
		$scope.responded = false;
		$scope.from_date = "";
		$scope.to_date = "";
		$scope.getSnapshotsData  = function(){
			var data = $.param({
				"accountId": authId,
				"selectedInterval": $scope.interval,
				"categories": $scope.categories.value,
				"from_date": $scope.from_date ? $scope.from_date : ($scope.to_date ? $scope.to_date : ""),
				"to_date": $scope.to_date ? $scope.to_date : ($scope.from_date ? $scope.from_date : "")
			});
			$http.post(snapshotUrlNew, data, $scope.config).success(function(resp){
				var results = JSON.parse(resp.results);
				$scope.overallSnapshot = results.overall_snapshots;
				$scope.snapshot = results.all_snapshots;
				console.log($scope.snapshot,$scope.overallSnapshot);
				console.log("response:: ",resp);
				$scope.snapshot_length = Object.keys($scope.snapshot).length;
				$scope.snap_block = ($scope.snapshot_length == 1) ? 12 : (($scope.snapshot_length == 2) ? 6 : 4);
				
				$scope.full_block = ($scope.snap_block == 12) ? 'full-width' : '';
				$scope.full_inner_block = ($scope.full_block == 'full-width') ? 6 : 12;
				$scope.col_block = ($scope.full_inner_block == 6) ? 4 : 6;
				$scope.col_block_row = ($scope.full_inner_block == 6) ? 8 : 6;
				

			}).then(function(resp) {
                $('.snapshot-scroll').slimScroll({
                    destroy: true
                });
                setTimeout(function(){
                $('.snapshot-scroll').slimScroll({
                    height: '262px',
                    color: '#000',
                    size: '4px',
                    alwaysVisible: true,
                    wheelStep: 10,
                    distance: '5px',
                });
                
                $('.full-width .snapshot-scroll').slimScroll({
                    height: '202px',
                    color: '#000',
                    size: '4px',
                    alwaysVisible: true,
                    wheelStep: 10,
                    distance: '5px',
                });
                
                },500);
                
               
            });
			$scope.show_filter = false;
		}
		$scope.openPopup = function(modelId, obj, snapshotId){
			var contextPath = $('#contextPath').val().trim();
			var rootUrl = contextPath+'/services';
			
			$(modelId).modal('show');
			var data = $.param({
				"accountId": authId,
				"selectedInterval": $scope.interval,
				"categories": $scope.categories.value,
				"from_date": $scope.from_date ? $scope.from_date : ($scope.to_date ? $scope.to_date : ""),
				"to_date": $scope.to_date ? $scope.to_date : ($scope.from_date ? $scope.from_date : ""),
				"snapshotId": snapshotId,
				"sourceTypeId": obj.graph_id
			});
			console.log("obj::", obj,data);
			$http.post(rootUrl + '/dashboard/' + obj.graph_url, data, $scope.config).success(function(resp){
				console.log("resp:: ",resp);
				var parsedData = JSON.parse(resp.results);
				for(var key in parsedData){
					var dataset = parsedData[key].charts;
					console.log("dataset::", dataset, key);
					var modifiedData = dataset.json.data;
					if((dataset.json.data[0] instanceof Array) || (typeof dataset.json.data[0]).toLowerCase() !== 'object')
					{
						//console.log("dataset.json.data:",dataset.json.data,typeof dataset.json.data[0]);
						modifiedData = [{"name": dataset.tool_tip_text,"data": dataset.json.data}]
						console.log("if");
					}else{
						console.log("else")
					}
					
					var chart = new custHighchart({
						container: 'sentiment_' + key,
						type: dataset.type,
						xAxisTitle: dataset.x_label,
						yAxisTitle: dataset.y_label,
						categories: dataset.json.categories,
						series: modifiedData,
						toolTipValueLabel : dataset.tool_tip_text,
						plotOptions: dataset.plotOptions,
						colors: key === 'positive' ? ["#3EBEC1", "#8edee0"] : ( key === 'negative' ? ["#eb7069", "#FFB0AA"] : ["#3EBEC1","#8edee0"]),
						tooltipFormat: dataset.tooltipFormat,
						legend: dataset.legend,
						marginBottom: 100,
					});
				}
				setTimeout(function(){
					window.dispatchEvent(new Event('resize'));
				});
				
			});
		}
		
		
		/* Bindable functions
		 -----------------------------------------------*/
		$scope.endDateBeforeRender = endDateBeforeRender
		$scope.endDateOnSetTime = endDateOnSetTime
		$scope.startDateBeforeRender = startDateBeforeRender
		$scope.startDateOnSetTime = startDateOnSetTime

		function startDateOnSetTime (newDate) {
			var arr = newDate.split('-');
			//$scope.dateSnapRangeStart = new Date(arr[1]+ '-' + arr[0] + '-' +arr[2]);
			$scope.dateSnapRangeStart = new Date(arr[2], arr[1]-1, arr[0], 0, 0, 0);
			
			console.log("newDate:: ",newDate);
			//console.log("oldDate:: ",oldDate);
			$scope.$broadcast('start-date-changed');
			$scope.from_date = newDate;
			$scope.to_date = '';
		  
		}

		function endDateOnSetTime (newDate) {
			 var arr = newDate.split('-');
			 //$scope.dateSnapRangeEnd = new Date(arr[1]+ '-' + arr[0] + '-' +arr[2]);
			 $scope.dateSnapRangeEnd = new Date(arr[2], arr[1]-1, arr[0], 0, 0, 0);
			 $scope.to_date = newDate;
			 $scope.$broadcast('end-date-changed');
		 
		}

		function startDateBeforeRender ($dates, newDate) {
			$scope.dateSnapRangeEnd = $scope.dateSnapRangeEnd || new Date();
		  if ($scope.dateSnapRangeEnd) {
		    var activeDate = moment($scope.dateSnapRangeEnd);
		    $dates.filter(function (date) {
		      return date.localDateValue() >= activeDate.valueOf();
		    }).forEach(function (date) {
		      date.selectable = false;
		    })
		  }
		}

		function endDateBeforeRender ($view, $dates, newDate) {
		    var activeDate = moment($scope.dateSnapRangeStart).subtract(1, $view).add(1, 'minute');
		    var toDay = moment(new Date());
		    //$scope.to_date = (activeDate.valueOf() + (90 * 24 * 60 * 60 * 1000));
		    $dates.filter(function (date) {
		    	return date.localDateValue() <= activeDate.valueOf() || date.localDateValue() >= toDay.valueOf() || date.localDateValue() >= (activeDate.valueOf() + (90 * 24 * 60 * 60 * 1000));
		    }).forEach(function (date) {
		      date.selectable = false;
		    })
		// }
	}
		
		/*------Function for expand and collapse-----*/
		$scope.expandCollapse= function(expand){
			console.log("expand:: ",expand);
			return !expand;
		}
		/*-----------Function for Custom ExpandAll-----------*/
		$scope.expandCollapseAll= function(expandAll){
			$scope.expand = !expandAll;
			console.log("expandAll:: ",expandAll);
			return !expandAll;
		}
		
		
		// sp for getting daily snapshot data
		/*$scope.fn_feedback_snapshot = function(interval){
			var data = $.param({
				"accountId": authId,
				"selectedInterval": interval,
				"type": "F"
	        });
						
			$http.post(SnapshotUrl, data, $scope.config).then(function(response){
				var i;
				if(interval == 'Daily'){
					$scope.dailyFeedbackSnapshot = response.data.results;
					$scope.dailyFeedbackSnapshot.current_total = $scope.dailyFeedbackSnapshot.rows[0][1];
					$scope.dailyFeedbackSnapshot.lweek_total = $scope.dailyFeedbackSnapshot.rows[0][2];
					$scope.dailyFeedbackSnapshot.percent_change = Math.abs($scope.dailyFeedbackSnapshot.rows[0][3]).toFixed(2);
					if($scope.dailyFeedbackSnapshot.percent_change > 999){
						$scope.dailyFeedbackSnapshot.percent_change = 'NA';
					}else{
						$scope.dailyFeedbackSnapshot.percent_change += '%';
					}
					$scope.dailyFeedbackSnapshot.rows.shift();
					$scope.dailySourceColorFeedback = [];
					for(i=0; i<$scope.dailyFeedbackSnapshot.rows.length; i++){
						$scope.dailyFeedbackSnapshot.rows[i][1] = Number($scope.dailyFeedbackSnapshot.rows[i][1]);
						$scope.dailyFeedbackSnapshot.rows[i][2] = Number($scope.dailyFeedbackSnapshot.rows[i][2]);
						$scope.dailyFeedbackSnapshot.rows[i][3] = Math.abs($scope.dailyFeedbackSnapshot.rows[i][3]).toFixed(2);
						if($scope.dailyFeedbackSnapshot.rows[i][3] > 999){
							$scope.dailyFeedbackSnapshot.rows[i][3] = 'NA';
						}else{
							$scope.dailyFeedbackSnapshot.rows[i][3] += '%';
						}
						if($scope.dailyFeedbackSnapshot.rows[i][0] == 'Facebook'){
							$scope.dailySourceColorFeedback[i] = 'facebook';
						}else if($scope.dailyFeedbackSnapshot.rows[i][0] == 'Twitter'){
							$scope.dailySourceColorFeedback[i] = 'twitter';
						}else{
							$scope.dailySourceColorFeedback[i] = 'rss';
						}
					}
				}else{
					$scope.monthlyFeedbackSnapshot = response.data.results;
					$scope.monthlyFeedbackSnapshot.current_total = $scope.monthlyFeedbackSnapshot.rows[0][1];
					$scope.monthlyFeedbackSnapshot.lweek_total = $scope.monthlyFeedbackSnapshot.rows[0][2];
					$scope.monthlyFeedbackSnapshot.percent_change = Math.abs($scope.monthlyFeedbackSnapshot.rows[0][3]).toFixed(2);
					if($scope.monthlyFeedbackSnapshot.percent_change > 999){
						$scope.monthlyFeedbackSnapshot.percent_change = 'NA';
					}else{
						$scope.monthlyFeedbackSnapshot.percent_change += '%';
					}
					$scope.monthlyFeedbackSnapshot.rows.shift();
					$scope.monthlySourceColorFeedback = [];
					for(i=0; i<$scope.monthlyFeedbackSnapshot.rows.length; i++){
						$scope.monthlyFeedbackSnapshot.rows[i][1] = Number($scope.monthlyFeedbackSnapshot.rows[i][1]);
						$scope.monthlyFeedbackSnapshot.rows[i][2] = Number($scope.monthlyFeedbackSnapshot.rows[i][2]);
						$scope.monthlyFeedbackSnapshot.rows[i][3] = Math.abs($scope.monthlyFeedbackSnapshot.rows[i][3]).toFixed(2);
						if($scope.monthlyFeedbackSnapshot.rows[i][3] > 999){
							$scope.monthlyFeedbackSnapshot.rows[i][3] = 'NA';
						}else{
							$scope.monthlyFeedbackSnapshot.rows[i][3] += '%';
						}
						if($scope.monthlyFeedbackSnapshot.rows[i][0] == 'Facebook'){
							$scope.monthlySourceColorFeedback[i] = 'facebook';
						}else if($scope.monthlyFeedbackSnapshot.rows[i][0] == 'Twitter'){
							$scope.monthlySourceColorFeedback[i] = 'twitter';
						}else{
							$scope.monthlySourceColorFeedback[i] = 'rss';
						}
					}
				}
			});
		}*/
		
		// sp for getting complaint daily snapshot
		/*$scope.fn_complaint_snapshot = function(interval){
			var data = $.param({
				"accountId": authId,
				"selectedInterval": interval,
				"type": "C"
	        });
			$scope.sourceColorComplaint = [];
			$http.post(SnapshotUrl, data, $scope.config).then(function(response){
				var i;
				if(interval == 'Daily'){
					$scope.dailyComplaintSnapshot = response.data.results;
					$scope.dailyComplaintSnapshot.current_total = $scope.dailyComplaintSnapshot.rows[0][1];
					$scope.dailyComplaintSnapshot.lweek_total = $scope.dailyComplaintSnapshot.rows[0][2];
					$scope.dailyComplaintSnapshot.percent_change = Math.abs($scope.dailyComplaintSnapshot.rows[0][3]).toFixed(2);
					if($scope.dailyComplaintSnapshot.percent_change > 999){
						$scope.dailyComplaintSnapshot.percent_change = 'NA';
					}else{
						$scope.dailyComplaintSnapshot.percent_change += '%';
					}
				    $scope.dailySourceColorComplaint=[];
					for(i=0; i<$scope.dailyComplaintSnapshot.rows.length; i++){
						$scope.dailyComplaintSnapshot.rows[i][1] = Number($scope.dailyComplaintSnapshot.rows[i][1]);
						$scope.dailyComplaintSnapshot.rows[i][2] = Number($scope.dailyComplaintSnapshot.rows[i][2]);
						$scope.dailyComplaintSnapshot.rows[i][3] = Math.abs($scope.dailyComplaintSnapshot.rows[i][3]).toFixed(2);
						if($scope.dailyComplaintSnapshot.rows[i][3] > 999){
							$scope.dailyComplaintSnapshot.rows[i][3] = 'NA';
						}else{
							$scope.dailyComplaintSnapshot.rows[i][3] += '%';
						}
						if($scope.dailyComplaintSnapshot.rows[i][0] == 'Facebook'){
							$scope.dailySourceColorComplaint[i] = 'facebook';
						}else if($scope.dailyComplaintSnapshot.rows[i][0] == 'Twitter'){
							$scope.dailySourceColorComplaint[i] = 'twitter';
						}else{
							$scope.dailySourceColorComplaint[i] = 'rss';
						}
					}
				}else{
					$scope.monthlySourceColorComplaint=[];
					$scope.monthlyComplaintSnapshot = response.data.results;
					$scope.monthlyComplaintSnapshot.current_total = $scope.monthlyComplaintSnapshot.rows[0][1];
					$scope.monthlyComplaintSnapshot.lweek_total = $scope.monthlyComplaintSnapshot.rows[0][2];
					$scope.monthlyComplaintSnapshot.percent_change = Math.abs($scope.monthlyComplaintSnapshot.rows[0][3]).toFixed(2);
					if($scope.monthlyComplaintSnapshot.percent_change > 999){
						$scope.monthlyComplaintSnapshot.percent_change = 'NA';
					}else{
						$scope.monthlyComplaintSnapshot.percent_change += '%';
					}
					for(i=0; i<$scope.monthlyComplaintSnapshot.rows.length; i++){
						$scope.monthlyComplaintSnapshot.rows[i][1] = Number($scope.monthlyComplaintSnapshot.rows[i][1]);
						$scope.monthlyComplaintSnapshot.rows[i][2] = Number($scope.monthlyComplaintSnapshot.rows[i][2]);
						$scope.monthlyComplaintSnapshot.rows[i][3] = Math.abs($scope.monthlyComplaintSnapshot.rows[i][3]).toFixed(2);
						if($scope.monthlyComplaintSnapshot.rows[i][3] > 999){
							$scope.monthlyComplaintSnapshot.rows[i][3] = 'NA';
						}else{
							$scope.monthlyComplaintSnapshot.rows[i][3] += '%';
						}
						if($scope.monthlyComplaintSnapshot.rows[i][0] == 'Facebook'){
							$scope.monthlySourceColorComplaint[i] = 'facebook';
						}else if($scope.monthlyComplaintSnapshot.rows[i][0] == 'Twitter'){
							$scope.monthlySourceColorComplaint[i] = 'twitter';
						}else{
							$scope.monthlySourceColorComplaint[i] = 'rss';
						}
					}
				}
			});
		} */
		
		// sp for getting entity with sentiment in daily snapshot
		/*$scope.fn_entity_snapshot = function(interval){
			var data = $.param({
				"accountId": authId,
				"selectedInterval": interval,
				"type": "E"
	        });
			$http.post(SnapshotUrl, data, $scope.config).then(function(response){
				var i;
				if(interval == 'Daily'){
					$scope.dailyEntitySnapshot = response.data.results.rows;
					$scope.dailyContextColor = [];
					for(i=0;i<$scope.dailyEntitySnapshot.length;i++){
						if($scope.dailyEntitySnapshot[i][1] == 'Positive')
							$scope.dailyContextColor[i] = 'text-green';
						else if($scope.dailyEntitySnapshot[i][1] == 'Negative')
							$scope.dailyContextColor[i] = 'text-red';
						else if($scope.dailyEntitySnapshot[i][1] == 'Neutral')
							$scope.dailyContextColor[i] = 'text-yellow';
					}
				}else{
					$scope.monthlyEntitySnapshot = response.data.results.rows;
					$scope.monthlyContextColor = [];
					for(i=0;i<$scope.monthlyEntitySnapshot.length;i++){
						if($scope.monthlyEntitySnapshot[i][1] == 'Positive')
							$scope.monthlyContextColor[i] = 'text-green';
						else if($scope.monthlyEntitySnapshot[i][1] == 'Negative')
							$scope.monthlyContextColor[i] = 'text-red';
						else if($scope.monthlyEntitySnapshot[i][1] == 'Neutral')
							$scope.monthlyContextColor[i] = 'text-yellow';
					}
				}
			});
		}*/
		
		// sp for getting daily top complaint
		/*$scope.fn_topcomplaint_snapshot = function(interval){
			var data = $.param({
				"accountId": authId,
				"selectedInterval": interval,
				"type": "TC"
	        });
			$http.post(SnapshotUrl, data, $scope.config).then(function(response){
				var data = response.data.results.rows[0];
				if(interval == 'Daily'){
					$scope.dailyTopComplaintSnapshot = data;
					$scope.dailyComplaintLoaded = true;
				}else{
					$scope.monthlyTopComplaintSnapshot = data;
					$scope.monthlyComplaintLoaded = true;
				}
			});
		}*/
		
		/**
		 * The following block of code determines the state of the application
		 * and redirects the application to the required page:
		 * if user has not yet filled the addinfo page: redirect to addinfo page,
		 * if user has filled addinfo page, but data is not yet ready: redirect to gettingdata page
		 * if user has filled the details and data is ready, call functions which are common to all pages.
		 */
		if(authData.split('$')[5] == 0){
			$scope.addinfo = true;
			$location.path( "/addinfo" );
		}
		else if(authData.split('$')[4] == 0){
			$scope.gettingdata = true;
			$location.path( "/gettingdata" );
		}else{
			$scope.gettingdata = false;
			
			$scope.pageLoad = false;
			
		    
		 // sql for getting sources along with categories
		    $http.get(getSourcesTopicsUrl+'?accountId='+authId+'&type='+"sources").success( function(response) {
		    	$scope.sources = response.results.rows;
		    	for(var i=0; i<$scope.sources.length; i++){
		    		$scope.sentimentalSources.push($scope.sources[i]);
		    		$scope.topSnapshotSources.push($scope.sources[i]); 
		    	}
		    	
		    	$scope.advocateSources = angular.copy($scope.sentimentalSources);
		    	//$scope.brandPageSources = angular.copy($scope.sentimentalSources);
		    	$scope.contextDetailsSources = angular.copy($scope.sentimentalSources);
		    	$scope.contextDetailsSources.unshift([ALL_SOURCES, 'All']);
		    	$scope.sentimentalSources.unshift([ALL_SOURCES, NULL_VALUE]);
		    	$scope.advocateSources.unshift([ALL_SOURCES, ALL_SOURCES]);
		    	$scope.sourcesFetched = true;
		    	
		    	$scope.dashboardSources= angular.copy($scope.sentimentalSources);
		    	$scope.deepListSources= angular.copy($scope.sentimentalSources);
		    	//$scope.dashboardSourcesForAspectWiseDist.unshift([ALL_SOURCES]);
		    	
		    	// getting the unique categories
		    	for(i=0; i<$scope.sources.length; i++){
		    		if($scope.source_categories.indexOf($scope.sources[i][2]) == -1)
		    			$scope.source_categories.push($scope.sources[i][2]);
		    	}
		    	$scope.source_categories.sort();
		    	$scope.getSources();
		    });
		    
		    // getting the sources for the selected source category
		    $scope.getSources = function(){
		    	$scope.selectedSource = BLANK_STRING;
		    	$scope.filtered_sources = [];
		    	for(var i=0; i<$scope.sources.length; i++){
		    		if($scope.sources[i][2] == $scope.selectedSourceCategory)
		    			$scope.filtered_sources.push($scope.sources[i]);
		    	}
		    	$scope.filtered_sources.sort();
		    }
		    
		 // sp for getting cities
		    var zomatoCities = [], twitterCities = [], regionsZomato = [];
		    $http.get(getSourcesTopicsUrl+'?accountId='+authId+'&type='+"citiesZomato").success( function(response) {
		    	zomatoCities = response.results.rows;
		    	zomatoCities.unshift(ALL_CITIES);
		    });
		    $http.get(getSourcesTopicsUrl+'?accountId='+authId+'&type='+"citiesTwitter").success( function(response) {
		    	twitterCities = response.results.rows;
		    	 //alert("twitterCities : "  + twitterCities)
		    	twitterCities.unshift(ALL_CITIES);
		    });
		    $http.get(getSourcesTopicsUrl+'?accountId='+authId+'&type='+"regionsZomato").success( function(response) {
		    	$scope.regionsWithCities = response.results.rows;
		    	for(var i=0;i<response.results.rows.length;i++){
		    		if(regionsZomato.indexOf(response.results.rows[i][0]) == -1){
		    			regionsZomato.push(response.results.rows[i][0]);
		    		}
		    	}
		    	regionsZomato.sort();
		    	
		    });
		    
		    
		    $http.get(getPageAccessTokenUrl+'?accountId='+authId).success( function(response) {
		    	$scope.pageAccessccessToken = response.results.rows[0];
		    });
		    
		    //////////////////////////////////////////////////////////////////////////
		   /* var ratingsCities = [], sourceRatings = [];
		    $http.get(getSourcesTopicsUrl+'?accountId='+authId+'&type='+"sourceRatings").success( function(response) {
		    	$scope.sourceWithCities = response.results.rows;
		    	for(var i=0;i<response.results.rows.length;i++){
		    		if(sourceRatings.indexOf(response.results.rows[i][0]) == -1){
		    			sourceRatings.push(response.results.rows[i][0]);
		    		}
		    	}
		    	sourceRatings.sort();
		    	
		    });
		    
		    $http.get(getSourcesTopicsUrl+'?accountId='+authId+'&type='+"citiesRatings").success( function(response) {
		    	ratingsCities = response.results.rows;
		    	ratingsCities.unshift(ALL_CITIES);
		    });
		    
		    $scope.getCitiesForRatingsSource = function(){
		    	$scope.ratingsCity = BLANK_STRING;
		    	$scope.ratingcities = [];
		    	*//**
		    	 * the following if condition checks
		    	 * IF option "All Regions" is selected from sentimental region filter dropdown:
		    	 * 		then sentimental cities dropdown will be populated with all zomato cities,
		    	 * ELSE: list of cities will be filtered against the selected region
		    	 *//*
		    	if($scope.ratingsSource == ALL_CITIES){
		    		$scope.ratingcities = $scope.ratingsCities;
		    	} else{
		    		for(var i=0; i<$scope.regionsWithCities.length; i++){
			    		if($scope.sourceWithCities[i][0] == $scope.ratingsSource){
			    			$scope.ratingcities.push($scope.sourceWithCities[i][1]);
			    		}
			    	}
		    	}
		    }*/
		    
		 // populate with cities according to source selected
		    $scope.getCities = function(){
		    	$scope.cities = [];
		    	if($scope.selectedSource == 4 || $scope.sentimentalSource == 4){
		    		$scope.zomatoCities = zomatoCities;
		    		$scope.cities = zomatoCities;
		    	}
		    	else if($scope.selectedSource == 2 || $scope.sentimentalSource == 2){
		    		$scope.cities = twitterCities;
		    	}
		    	$scope.cities.sort();
		    }
		    
		    // populate with regions according to source selected
		    $scope.getRegions = function(){
		    	$scope.sentimentalRegion = BLANK_STRING;
		    	if($scope.sentimentalSource == 4){
		    		$scope.zomatoRegions = angular.copy(regionsZomato);
		    		$scope.zomatoRegions.sort();
			    	$scope.zomatoRegions.unshift(ALL_REGIONS);
		    	}
		    }
		    
		    
		    // sp for getting entities
		    $http.get(getSourcesTopicsUrl+'?accountId='+authId+'&type='+"topics").success( function(response) {
		    	$scope.topics = response.results.rows;
		    	$scope.dashboardTopics = response.results.rows;
		    	$scope.dashboardTopicsForDiv = response.results.rows;
		    	//$scope.dashboardTopicsForDiv2 = response.results.rows;
		    	//$scope.dashboardTopicsForDiv = $scope.dashboardTopicsForDiv.splice(0,1);
		    	$scope.dashboardTopicsForDiv.splice($scope.dashboardTopicsForDiv.map(function(el){return el[0];}).indexOf('All Contexts'),1);
		    	$scope.dashboardTopicsForDiv.splice($scope.dashboardTopicsForDiv.map(function(el){return el[0];}).indexOf('OTHERS'),1);
		    	
		    	$scope.dashboardTopicsForDiv2 = angular.copy($scope.dashboardTopicsForDiv);
		    	//$scope.dashboardTopicsForDiv.splice($scope.dashboardTopicsForDiv.map(function(el){return el[0];}).indexOf('NON-RELEVANT'),1);
		    	/*$scope.dashboardTopicsForDiv2.splice($scope.dashboardTopicsForDiv2.map(function(el){return el[0];}).indexOf('All Contexts'),1);
		    	$scope.dashboardTopicsForDiv2.splice($scope.dashboardTopicsForDiv2.map(function(el){return el[0];}).indexOf('OTHERS'),1);*/
		    	
		    	for(var i=0;i<response.results.rows.length;i++){
		    		$scope.topics[i].push(true);
		    		$scope.selectedTopics.push($scope.topics[i][1]);
		    		$scope.selectedSentimentalTopics.push($scope.topics[i][1]);
		    	}
		    	$scope.topicsFetched = true;
		    	
		    	$scope.topics.unshift([ALL_CONTEXTS, NULL_VALUE, true]);
		    	$scope.sentimentalTopics = angular.copy($scope.topics);
		    	$scope.deepListTopics =angular.copy($scope.topics);
		    	$scope.trendingTopicAspects = angular.copy($scope.topics);
		    	$scope.trendingTopicAspects.splice($scope.trendingTopicAspects.map(function(el){return el[0];}).indexOf('OTHERS'),1);	// remove 'OTHERS' option from trendingTopicAspects list
		    	$scope.trendingTopicAspects.splice($scope.trendingTopicAspects.map(function(el){return el[0];}).indexOf('NON-RELEVANT'),1);	// remove 'NON-RELEVANT'/'Garbage' option from trendingTopicAspects list
		    });
		    
		    // sp for getting feedback aspects/categories
		    $http.get(getSourcesTopicsUrl+'?accountId='+authId+'&type='+"feedbackcategory").success( function(response) {
		    	$scope.feedbacktopics = response.results.rows;
		    	
		    	for(var i=0;i<response.results.rows.length;i++){
		    		$scope.feedbacktopics[i].push(true);
		    	}
		    	
		    	$scope.feedbacktopics.unshift(["0", "Select Category", true]);
		    });
		    
		   
		    // refreshing selectpicker after population with data
		    $scope.$watch(function() {
		        $('.selectpicker').selectpicker('refresh');
		    });
		    
		 // sql for getting domain name
			$http.get(getSourcesTopicsUrl+'?accountId='+authId+'&type='+"domainDisplayName").success( function(response) {
				$scope.domainName = response.results.rows;
		    });
		}
		
		// ******************************************** Analytics Page Related Code ******************************************
		$scope.intervalValue = "1";
		$scope.datePopulationOnloadFlag = true;			// this flag is used to check whether the Happiness Index Live is called onload (so, date filters need to be populated)
		//$scope.selectedContextMonthly = 'All Contexts';	// holds the selected context in Competitive Happiness Index initiated with 'All Contexts'
		//$scope.selectedContextMonthly = NULL_VALUE;				// holds the selected context in Competitive Happiness Index initiated with 'All Contexts'
		
		$scope.sortType = '-6';						// angular sort value for Context Details table initialized on feedback column
		$scope.sortReverse  = false;				// angular sort for Context Details table to toggle between columns feedback and complaint
		$scope.activeTab = 'social';				// selected tab to be shown among Deep Listening and Trending Topics
		$scope.entityList = false;					// toggle behaviour of entity list dropdown in word cloud table initiated with false(dropdown closed)
		$scope.contextList = false;					// toggle behaviour of context list dropdown in word cloud table initiated with false(dropdown closed)
		$scope.filterContext = '';					// holds the selected context from word cloud table context list dropdown
		$scope.filterEntity = '';					// holds the selected enitity from word cloud table entity list dropdown
		$scope.wordcloudContext = "All Contexts"	// holds the selected context from word cloud
		$scope.more_less = 'more';
		$scope.overallSentimentChartTab = 'OverallSentimentBarChart';
		/*$scope.sentimentalSource = NULL_VALUE;			// INITIALIZATION in master-ctrl.js : initiated with 'All Sources'
		$scope.sentimentalRegion = BLANK_STRING;		// INITIALIZATION in master-ctrl.js : repeatedly made blank so that value is not retained when transversed from other page
		$scope.sentimentalCity = BLANK_STRING;			// INITIALIZATION in master-ctrl.js : repeatedly made blank so that value is not retained when transversed from other page
		$scope.sentimentalStore = BLANK_STRING;			// INITIALIZATION in master-ctrl.js : repeatedly made blank so that value is not retained when transversed from other page
		$scope.sentimentalTopic = NULL_VALUE;*/			// INITIALIZATION in master-ctrl.js : repeatedly made blank so that value is not retained when transversed from other page
		
		//$scope.advocateSource = ALL_SOURCES;					// holds the source value of Top Advocates initiated with 'All Sources'
		//$scope.advocateFrom = $scope.yesterday;			// datepicker 'From date' in Top Advocates initially kept yesterday's date
		//$scope.advocateTo = $scope.today;				// datepicker 'To date' in Top Advocates initially kept today's date
		
		$scope.selectedSourceTF = DEFAULT_SOURCE_FB;
		$scope.contextEntriesTF = 10;
		$scope.selectedDurationTF = DAILY_TF;
		$scope.selectedSortByTF = SORTBY_TF;
		$scope.sortTypeTF = ['sourceTypeName','-reviewCount'];
		
		$scope.advocateFrom = $scope.yesterday;			// datepicker 'From date' in Top Advocates initially kept yesterday's date
		$scope.advocateTo = $scope.today;				// datepicker 'To date' in Top Advocates initially kept today's date
		$scope.trendingTopicFrom = $scope.yesterday;	// datepicker 'From date' in Trending Topics initially kept yesterday's date as selected trendingTopicInterval is 'Daily'
		$scope.trendingTopicTo = $scope.today;			// datepicker 'To date' in Trending Topics initially kept today's date as selected trendingTopicInterval is 'Daily'
		/*$scope.sentimentalTo = $scope.today +' - '+ $scope.today;			// datepicker 'To date' in Overall Sentiment Distribution initially kept today's date
		$scope.sentimentalFrom = $scope.yesterday +' - '+ $scope.yesterday;*/		// datepicker 'From date' in Overall Sentiment Distribution initially kept yesterday's date
		
		
		/*$scope.dashboardOverallSentimentFrom = $scope.yesterday;
		$scope.dashboardOverallSentimentTo = $scope.today;
		$scope.dashboardAspectDistributionFrom = $scope.yesterday;
		$scope.dashboardAspectDistributionTo = $scope.today;
		$scope.dashboardAspectDistributionDetailsFrom = $scope.yesterday;
		$scope.dashboardAspectDistributionDetailsTo = $scope.today;*/
		
		
		/*$scope.analyticsFrom = $scope.yesterday;
		$scope.analyticsTo = $scope.today;*/
		$scope.analyticsFrom = $scope.newDeepListingYesterday;
		$scope.analyticsTo = $scope.newDeepListingToday;
		
		$scope.contextEntries = 10;							// holds the value for number of entries to be shown in Context Details table (table in Explore section)
		$scope.socialEntries = 10;							// holds the value for number of entries to be shown in Context Details table (table in Explore section)
		$scope.authRole = "0";
		
		$scope.sourceIDFromRout= $routeParams.source;
		$scope.topicIDFromRout = $routeParams.topic;
		
		//Refreshing the selected value Select Interval selectbox
		//To manage blank list in the select list of SENTIMENT INDEX LIVE 
		$('#setRange').selectpicker('refresh');
	    
		// function for filtering wordcloud data as per entity/context selected from entityList/contextList except when value of entity/context is empty
		$scope.exceptEmptyComparator = function (actual, expected) {
			if (!expected) {
		       return true;
		    }
		    return angular.equals(expected.trim(), actual.trim());
		}
		
		// object for onload Deep Listening table table
		$scope.socialPagination = {
				socialCurrentPage: 1,
				socialNewPage: 1
		};
		$scope.totalSocialCount = 0 ;
		
		// object for onload Deep Listening table table
		$scope.socialPaginationTF = {
				socialCurrentPageTF: 1,
				socialNewPageTF: 1
		};
		$scope.totalSocialCountTF = 0 ;
		
		// object for onload Deep Listening table table
		$scope.socialPaginationTF = {
				socialCurrentPageTF: 1,
				socialNewPageTF: 1
		};
		$scope.totalSocialCountTF = 0 ;
		
		 // Asynchronous Setup for pagination
		$scope.socialPageChanged = function(socialNewPage) {
			//alert(socialNewPage);
			$scope.fn_deep_listening_YUM_pag_change(socialListeningObj,socialNewPage);
	    };
		
		$scope.postTweetWiseEngagementScorePageChange = function(newPageTF) {
			$scope.fn_post_tweet_wise_engagement_score(twitterFacebookEngagementSentimentObj,newPageTF);
	    };
	    
	    $scope.fn_post_tweet_wise_engagement_score = function (twitterFacebookEngagementSentimentObj,newPageTF){
			twitterFacebookEngagementSentimentObj.accountId = authId;
			var data = $.param({
				"inAccountId": twitterFacebookEngagementSentimentObj.accountId,
				"inDurationTF":twitterFacebookEngagementSentimentObj.selectedDuration,
		        "inSourceTF": twitterFacebookEngagementSentimentObj.selectedSource, 
		        "perPageEntries" : $scope.contextEntriesTF,
	            "newPage" : newPageTF,
	            "sortBy" : $scope.selectedSortByTF
	        });
			$http.post(getPostTweetWiseEngagementScoreUrl, data, $scope.config).then(function(response){
				var data = response.data.output;
				$scope.totalSocialCountTF = response.data.count;
				$scope.entitiesTF = data;
			    /*for(var i=0;i<$scope.entitiesTF.length;i++){
					$scope.entitiesTF[i].complaint = Math.round($scope.entities[i].negative*$scope.entities[i].reviewCount/100);
			    }*/
			});
		}
	    
	    $scope.fn_filter_PostTweetWiseEngagementScore = function(){
			// Reset current page after filter click.
			$scope.socialPaginationTF.socialCurrentPageTF = 1;
			$scope.socialPaginationTF.socialNewPageTF = 1;
			
			$scope.filteredTW = true;
			twitterFacebookEngagementSentimentObj.selectedDuration = $scope.selectedDurationTF;
			twitterFacebookEngagementSentimentObj.selectedSource = $scope.selectedSourceTF;
			
			$scope.fn_post_tweet_wise_engagement_score(twitterFacebookEngagementSentimentObj,$scope.socialPaginationTF.socialNewPageTF);
		}
	    
	    //On load calling Overall sentiment chat
		/*$scope.fn_overall_sentiment_distribution = function(){			
			var data = $.param({
				"accountId": authId
	        });
			$http.post(overallSentimentDistributionUrl, data, $scope.config).then(function(response){
				var data = response.data.results;
					var selectedSentimentalStartDuration = $('#sentimentalFrom').val();
					var selectedSentimentalEndtDuration = $('#sentimentalTo').val();
					chart_overall_sentiment(data,selectedSentimentalStartDuration,selectedSentimentalEndtDuration);	
			});
		}*/
		
		//function for filter Overall Weekly Sentiment Distribution 
		 $scope.sentimentalFrom = {
            startDate: moment().subtract(1, "days"),
            endDate: moment().subtract(1, "days"),//moment(),
            format: "DD-MM-YYYY"
        };
        $scope.sentimentalTo = {
            startDate: moment(),//moment().subtract(1, "days"),
            endDate: moment(),
            format: "DD-MM-YYYY"
        };
       
       //Checking if selected source is GES, tehn reinitilize the date range to 2 days back.
       $scope.fn_overall_sentiment_onChange_dateSet = function(){
        	if($scope.sentimentalSource == "5"){
        		$scope.sentimentalFrom = {
	                   startDate: moment().subtract(3, "days"),
	                   endDate: moment().subtract(3, "days"),
	                   format: "DD-MM-YYYY"
	               };
		        $scope.sentimentalTo = {
	                   startDate: moment().subtract(2, "days"),
	                   endDate: moment().subtract(2, "days"),
	                   format: "DD-MM-YYYY"
	               };
        	} else {
        		$scope.sentimentalFrom = {
	                   startDate: moment().subtract(1, "days"),
	                   endDate: moment().subtract(1, "days"),
	                   format: "DD-MM-YYYY"
	               };
		        $scope.sentimentalTo = {
	                   startDate: moment(),
	                   endDate: moment(),
	                   format: "DD-MM-YYYY"
	               };
        	}
		}
		
		$scope.fn_overall_sentiment_distribution_filter = function(){
			//debugger;
			
			var selectedSentimentalStartDuration = $('#sentimentalFrom').val();
			var selectedSentimentalEndtDuration = $('#sentimentalTo').val();
			if($scope.sentimentalFrom)
				var start = new Date((selectedSentimentalStartDuration.substring(6,10)+'-'+selectedSentimentalStartDuration.substring(3,6)+selectedSentimentalStartDuration.substring(0,2)+selectedSentimentalStartDuration.substring(10,selectedSentimentalStartDuration.length)).replace(/-/g, "/"));
			if($scope.sentimentalTo)
				var end = new Date((selectedSentimentalEndtDuration.substring(6,10)+'-'+selectedSentimentalEndtDuration.substring(3,6)+selectedSentimentalEndtDuration.substring(0,2)+selectedSentimentalEndtDuration.substring(10,selectedSentimentalEndtDuration.length)).replace(/-/g, "/"));
			if(start && end && start>end){
				$scope.callAlert('danger','End date should not be behind Start date');
			}else{
				console.log("$scope.selectedSentimentalTopics.length : " + $scope.selectedSentimentalTopics.length + "--- $scope.topics.length :" + $scope.topics.length );
				var data = $.param({
					"accountId": authId, 
					"sourceId": ($scope.sentimentalSource == ALL_SOURCES || $scope.sentimentalSource == BLANK_STRING || $scope.sentimentalSource == 9999) ? -1 : $scope.sentimentalSource,
					"topic": ($scope.selectedSentimentalTopics.length+1 == $scope.topics.length)?'ALL':$scope.selectedSentimentalTopics.toString(),
					"city": $scope.sentimentalCity == ALL_CITIES || $scope.sentimentalCity == null ? BLANK_STRING: String ($scope.sentimentalCity),
					//"region": $scope.sentimentalRegion == ALL_REGIONS || $scope.sentimentalRegion == null ? BLANK_STRING: String ($scope.sentimentalRegion),
					//"startDate": toSentimentalGMTDate($scope.sentimentalFrom),
					//"endDate": toSentimentalGMTDate($scope.sentimentalTo)
					"startDate": selectedSentimentalStartDuration,
					"endDate": selectedSentimentalEndtDuration
		        });

				$http.post(filterOverallSentimentDistributionUrl, data, $scope.config).then(function(response){
			
					var data = response.data.results;
					/*var data = response.data.results.rows;
					var percentage_data = [];
					if(data!=BLANK_STRING){
						percentage_data = [0,0,0];
						for(var i=0; i<data.length; i++){
							if(data[i][0] == 'Positive'){
								percentage_data[0] = Math.round((data[i][1]*100)/data[data.length-1][1]);
							}else if(data[i][0] == 'Negative'){
								percentage_data[1] = Math.round((data[i][1]*100)/data[data.length-1][1]);
							}else if(data[i][0] == 'Neutral'){
								percentage_data[2] = Math.round((data[i][1]*100)/data[data.length-1][1]);
							}
						}
						percentage_data[2] = 100-(percentage_data[0]+percentage_data[1]);
					}*/
					
					chart_overall_sentiment(data,selectedSentimentalStartDuration,selectedSentimentalEndtDuration);		// this function contains the code for chart formation using highcharts
					chart_overall_sentiment_line(data,selectedSentimentalStartDuration,selectedSentimentalEndtDuration);
				});
			}
		}
		
		// function for onload Top Advocates (bar chart)
		$scope.fn_top_advocates = function(){
			var data = $.param({
				"accountId": authId
	        });
			$http.post(topAdvocatesUrl, data, $scope.config).then(function(response){
				chart_top_advocates(response.data.results.rows);	// this function contains the code for chart formation using highcharts
			});
		}
		
		// function for filter Top Advocates (bar chart)
		$scope.fn_filter_top_advocates = function(){
			//alert(toGMTDate($scope.advocateFrom));
			var data = $.param({
				"accountId": authId, 
				"sourceId": ($scope.advocateSource == ALL_SOURCES || $scope.advocateSource == BLANK_STRING) ? -1 : $scope.advocateSource,
				"startDate": toGMTDate($scope.advocateFrom),
				"endDate": toGMTDate($scope.advocateTo)
	        });
			$http.post(topFilterAdvocatesUrl, data, $scope.config).then(function(response){
				chart_top_advocates(response.data.results.rows);	// this function contains the code for chart formation using highcharts
			});
			/*if($scope.advocateFrom)
				var start = new Date(($scope.advocateFrom.substring(6,10)+'-'+$scope.advocateFrom.substring(3,6)+$scope.advocateFrom.substring(0,2)+$scope.advocateFrom.substring(10,$scope.advocateFrom.length)).replace(/-/g, "/"));
			if($scope.advocateTo)
				var end = new Date(($scope.advocateTo.substring(6,10)+'-'+$scope.advocateTo.substring(3,6)+$scope.advocateTo.substring(0,2)+$scope.advocateTo.substring(10,$scope.advocateTo.length)).replace(/-/g, "/"));
			if(start && end && start>end){
				$scope.callAlert('danger','End date should not be behind Start date');
			}else{
				var data = $.param({
					"accountId": authId, 
					"sourceId": ($scope.advocateSource == ALL_SOURCES || $scope.advocateSource == BLANK_STRING) ? -1 : $scope.advocateSource,
					"startDate": toGMTDate($scope.advocateFrom),
					"endDate": toGMTDate($scope.advocateTo)
		        });
				$http.post(topFilterAdvocatesUrl, data, $scope.config).then(function(response){
					chart_top_advocates(response.data.results.rows);	// this function contains the code for chart formation using highcharts
				});
			}*/
		}
		
		// function for deep listening table on filtering
		$scope.fn_deep_listening = function(socialListeningObj,socialNewPage){
			var data = $.param({
				"inAccountId": socialListeningObj.accountId, 
		        //"inSource": socialListeningObj.rowSourceId, 
				"inSource": 1,
		        "inSearchKeyword": $scope.searchByText,
		        "inSourceCategory": socialListeningObj.sourceCategory,
		        "inTopic": socialListeningObj.rowSourceTopic, 
		        "start_dt": socialListeningObj.startDt,
		        "end_dt" : socialListeningObj.endDt,
		        "virality" :socialListeningObj.virality,
	            "sentiment":socialListeningObj.sentiment,
	            "city":socialListeningObj.city,
		        "details_tab_name" : socialListeningObj.detailsTabName,
	            "perPageEntries" : $scope.socialEntries,
	            "newPage" : socialNewPage
	        });
			$scope.deepListSocLists = null;
			$scope.deepListNonSocLists = null;
			$http.post(deepListeningUrl, data, $scope.config).then(function(response){
				$scope.totalSocialCount = response.data.count;
				//alert($scope.totalSocialCount);
				$scope.deepListSocLists = response.data.output.socialPostListObj;
				$scope.deepListNonSocLists = response.data.output.nonSocialPostListObj;
				$scope.filtered = false;
			});
		}
		
		// function for onload Happiness Index Live chart
		$scope.fn_brand_comparission_24Hour = function(){
			var data = $.param({
				"account_Id": authId, 
	            "current_Time": null,
	            "selectedInterval": $scope.intervalValue
	        });
			//$http.post(brand24HourUrl, data, $scope.config).then(function(response){
				/**
				 * the following condition check is written to check
				 * if the Happiness Index Live is called onload (datePopulationOnloadFlag=true):
				 * 		then date filters are populated by the first and last data points of the chart
				 * else the Happiness Index Live is called after changing interval (datePopulationOnloadFlag=false):
				 * 		then date filters retain the value it contains
				 * 
				 */
				/*if($scope.datePopulationOnloadFlag){
					$scope.analyticsTo = toLocalDate(response.data.results.rows[response.data.results.rows.length-1][2]);
					var date = response.data.results.rows[response.data.results.rows.length-1][2];
					date = new Date((date.substring(6,10)+'-'+date.substring(3,6)+date.substring(0,2)+date.substring(10,date.length)).replace(/-/g, "/"));
					date.setDate(date.getDate() - 1);
					$scope.analyticsFrom = toLocalDate($filter('date')(date, 'dd-MM-yyyy HH:mm'));
				}
				chart_brand_comparission_24Hour(response.data.results, $scope.intervalValue);
			});*/
				$http.post(brand24HourUrl, data, $scope.config).then(function(response){
					/**
					 * the following condition check is written to check
					 * if the Happiness Index Live is called onload (datePopulationOnloadFlag=true):
					 * 		then date filters are populated by the first and last data points of the chart
					 * else the Happiness Index Live is called after changing interval (datePopulationOnloadFlag=false):
					 * 		then date filters retain the value it contains
					 * 
					 */
					if($scope.datePopulationOnloadFlag && response.data.results.rows.length > 0){
						$scope.dashboardTo = response.data.results.rows[response.data.results.rows.length-1][2];
						var date = response.data.results.rows[response.data.results.rows.length-1][2];
						date = new Date((date.substring(6,10)+'-'+date.substring(3,6)+date.substring(0,2)+date.substring(10,date.length)).replace(/-/g, "/"));
						date.setDate(date.getDate() - 1);
						$scope.dashboardFrom = $filter('date')(date, 'dd-MM-yyyy HH:mm');
						$scope.trendingTopicFrom = $scope.dashboardFrom;
						$scope.trendingTopicTo = $scope.dashboardTo;
					}
					chart_brand_comparission_24Hour(response.data.results, $scope.intervalValue);	// this function contains the code for chart formation using highcharts
				});
		}
		
		// function for Daily/Weekly interval of spline chart
		$scope.intervalChange = function(){
			$scope.datePopulationOnloadFlag = false;
			$scope.fn_brand_comparission_24Hour(null);
		}
		
		// function for onload Competitive Happiness Index (two spline charts)
		/*$scope.fn_brand_comparission_monthly = function(){
			console.log("====================>" + $scope.selectedContextMonthly);
			var data = $.param({
				"account_Id": authId,
				"topic" : $scope.selectedContextMonthly == 'All Contexts'? '': String ($scope.selectedContextMonthly)
	        });
			$http.post(brandMonthlyUrl, data, $scope.config).then(function(response){
				chart_brand_comparission_monthly(response.data.results.rows, $scope.selectedContextMonthly);
			});
		}*/
		//alert("--- : " + $scope.selectedSecondaryCompany.toString())
		// function for onload Competitive Happiness Index (two spline charts)
		$scope.fn_brand_comparission_monthly = function(){
			var data = $.param({
				"account_Id":  authId,
				"company_Id": $scope.selectedSecondaryCompany.toString(),
				"topic" : $scope.selectedContextMonthly
	        });
			$http.post(brandMonthlyUrl, data, $scope.config).then(function(response){
				chart_brand_comparission_monthly(response.data.results.rows, $scope.selectedContextMonthly);
			});
		}
		
		
		// function for onload context details table
		$scope.fn_source_entity_details = function (entityDetailsObj){
			entityDetailsObj.accountId = authId;
			var data = $.param({
				"inAccountId": entityDetailsObj.accountId,
		        "inSource": entityDetailsObj.rowSourceId, 
		        "inTopic": entityDetailsObj.rowSourceTopic, 
		        "start_dt": entityDetailsObj.startDt,
		        "end_dt": entityDetailsObj.endDt,
		        "time_interval" : entityDetailsObj.hr,
		        "selectedInterval" : entityDetailsObj.selectedInterval
	        });
			$scope.entities = null;
			$http.post(entityDetailsUrl, data, $scope.config).then(function(response){
				var data = response.data.results.rows;
			    $scope.entities = data;
			    for(var i=0;i<$scope.entities.length;i++){
					$scope.entities[i].push(Math.round($scope.entities[i][4]*$scope.entities[i][6]/100))
			    }
			});
		}
		
		// function for onload deep listening
		/*$scope.fn_deep_listening = function(socialListeningObj){
			var data = $.param({
				"inAccountId": socialListeningObj.accountId, 
		        "inSource": socialListeningObj.sourceId, 
		        "inTopic": socialListeningObj.sourceTopic, 
		        "start_dt": socialListeningObj.startDt,
		        "end_dt": socialListeningObj.endDt,
		        "sentiment":socialListeningObj.sentiment,
 		        "details_tab_name" : socialListeningObj.detailsTabName
	        });
			$scope.soclists = null;
			$http.post(analyticsDetailsUrl, data, $scope.config).then(function(response){
					var data = response.data.results.rows;
					for (var i=0; i<data.length;i++) {
				    	data[i][4] = toLocalDate(data[i][4]);
				    }
					$scope.soclists = data;
			});
		}*/
		
		// function context details table when filtered with the filter options
		$scope.fn_filtered_entity_details = function(filteredEntityDetailsObj){
			//alert("()" + filteredEntityDetailsObj.startDt)
			var data = $.param({
				"inAccountId": filteredEntityDetailsObj.accountId,
	            "inSource": filteredEntityDetailsObj.rowSourceId, 
	            "inTopic": filteredEntityDetailsObj.rowSourceTopic, 
	            "start_dt": filteredEntityDetailsObj.startDt,
	            "end_dt": filteredEntityDetailsObj.endDt,
	            "virality" :filteredEntityDetailsObj.virality,
	            "sentiment":filteredEntityDetailsObj.sentiment
	        });
			$scope.entities = null;
			$http.post(filteredEntityDetailsUrl, data, $scope.config).then(function(response){
				var data = response.data.results.rows;
			    $scope.entities = data;
			    for(var i=0;i<$scope.entities.length;i++){
					$scope.entities[i].push(Math.round($scope.entities[i][4]*$scope.entities[i][6]/100))
			    }
			});
		}
		
		// function deep listening and trending topics tables when filtered with the filter options
		$scope.fn_filtered_source_date_analytics_details = function(filteredAnalyticsDetailsObj){
			var data = $.param({
				"inAccountId": filteredAnalyticsDetailsObj.accountId, 
		        "inSource": filteredAnalyticsDetailsObj.rowSourceId, 
		        "inTopic": filteredAnalyticsDetailsObj.rowSourceTopic, 
		        "start_dt": filteredAnalyticsDetailsObj.startDt,
		        "end_dt" : filteredAnalyticsDetailsObj.endDt,
	            "sentiment":filteredAnalyticsDetailsObj.sentiment,
		        "details_tab_name" : filteredAnalyticsDetailsObj.detailsTabName
	        });
			$scope.soclists = null;
			$http.post(filteredAnalyticsDetailsUrl, data, $scope.config).then(function(response){
				var i, data;
				//alert(filteredAnalyticsDetailsObj.detailsTabName);
				if(filteredAnalyticsDetailsObj.detailsTabName == 'social_listening'){
		    		data = response.data.results.rows;
		    		for (i=0; i<data.length;i++) {
				    	data[i][4] = toLocalDate(data[i][4]);
				    }
					$scope.soclists = data;
					//alert($scope.soclists)
		    	}else if(filteredAnalyticsDetailsObj.detailsTabName == WORD_CLOUD){
		    		$scope.wordCloudTableData = [];
		    		var keysArray = response.data.results.column_names;
		    		var valuesArray = response.data.results.rows;
		    		for(i=0;i<response.data.results.rows.length;i++){
		    			data = {};
		    			for(var j=0;j<keysArray.length;j++){
		    				data[keysArray[j]] = valuesArray[i][j];
		    			}
		    			$scope.wordCloudTableData.push(data);
		    		}
		    		$scope.entitiesForFilter = [];
		    		for(i=0;i<$scope.wordCloudTableData.length;i++){
		    			if($scope.entitiesForFilter.indexOf($scope.wordCloudTableData[i].entity) == -1){
		    				$scope.entitiesForFilter.push($scope.wordCloudTableData[i].entity.trim());
		    			}
		    		}
		    		$scope.entitiesForFilter.sort();
		    		$scope.entitiesForFilter.unshift("All Entities");
		    	}	
			});
		}
		
		
		// used for wordcloud click of context  to filter post list
		$scope.fn_filtered_source_date_analytics_details_click = function(filteredAnalyticsDetailsObj){
			//alert("reached")
			var data = $.param({
				"inAccountId": filteredAnalyticsDetailsObj.accountId, 
		        "inSource": filteredAnalyticsDetailsObj.rowSourceId, 
		        "inTopic": filteredAnalyticsDetailsObj.rowSourceTopic, 
		        "start_dt": filteredAnalyticsDetailsObj.startDt,
		        "end_dt" : filteredAnalyticsDetailsObj.endDt,
	            "sentiment":filteredAnalyticsDetailsObj.sentiment,
		        "details_tab_name" : filteredAnalyticsDetailsObj.detailsTabName
	        });
			$scope.soclists = null;
			$http.post(filteredAnalyticsDetailsUrlClick, data, $scope.config).then(function(response){
				var i, data;
				//alert(filteredAnalyticsDetailsObj.detailsTabName);
				if(filteredAnalyticsDetailsObj.detailsTabName == 'social_listening'){
		    		data = response.data.results.rows;
		    		for (i=0; i<data.length;i++) {
				    	data[i][4] = toLocalDate(data[i][4]);
				    }
					$scope.soclists = data;
					//alert($scope.soclists)
		    	} else if(filteredAnalyticsDetailsObj.detailsTabName == WORD_CLOUD || filteredAnalyticsDetailsObj.detailsTabName == "Entity_click"){
		    		//alert("in now");
		    		$scope.wordCloudTableData = [];
		    		var keysArray = response.data.results.column_names;
		    		var valuesArray = response.data.results.rows;
		    		for(i=0;i<response.data.results.rows.length;i++){
		    			data = {};
		    			for(var j=0;j<keysArray.length;j++){
		    				data[keysArray[j]] = valuesArray[i][j];
		    			}
		    			$scope.wordCloudTableData.push(data);
		    		}
		    		$scope.entitiesForFilter = [];
		    		for(i=0;i<$scope.wordCloudTableData.length;i++){
		    			if($scope.entitiesForFilter.indexOf($scope.wordCloudTableData[i].entity) == -1){
		    				$scope.entitiesForFilter.push($scope.wordCloudTableData[i].entity.trim());
		    			}
		    		}
		    		$scope.entitiesForFilter.sort();
		    		$scope.entitiesForFilter.unshift("All Entities");
		    	} 	
			});
		}
		/**
		 * This function populated the wordcloudaspect and wordcloudentity arrays
		 * which are sent to the generateWordcloud fucntion for creation of the wordcloud
		 * @parameter: response- contains the total data of entities(and aspects)
		 * @parameter: entityIndex- this contains the index of response where entities are returned,
		 * 			   0: when only entities are returned,
		 * 			   1: when both aspects and entities are returned
		 */
		$scope.fn_word_cloud_array_population = function(response, entityIndex){
			var wordcloudentity = response.data[entityIndex].rows;				// entities can be present in 0th or 1st index of response.data
			var wordcloudentityHead = response.data[entityIndex].columnNames;
			var wordcloudArray = [];
		    var entityArray = [];
			
		    // entity objects are formed with properties and pushed in entityArray
		    for(var i=0;i<wordcloudentity.length;i++){
		    	var entity = {};
				entity.text = wordcloudentity[i][0];
				entity.size = wordcloudentity[i][1];
				entity.sentiment = wordcloudentity[i][2];
				entity.type = wordcloudentityHead[0];
				
				/**
				 * this condition puts the required colors on entities depending on:
				 * if only entities are fetched, they are assigned colors depending on their respective sentiments
				 * if both entities and aspects are fetched, they are assigned a common color
				 */ 
				if(!entityIndex){
					if(wordcloudentity[i][2]=='Positive'){
						entity.color = GREEN;
					}else if(wordcloudentity[i][2]=='Negative'){
						entity.color = RED;
					}else{
						entity.color = YELLOW;
					}
				}else{
					entity.color = GRAY;
				}
				entityArray.push(entity);
		    }
		    
		    // the entities filter list in the Trending Topics table (entities column) is populated 
			$scope.entitiesForFilter = [];
    		for(i=0;i<wordcloudentity.length;i++){
    			if($scope.entitiesForFilter.indexOf(wordcloudentity[i][0]) == -1){
    				$scope.entitiesForFilter.push(wordcloudentity[i][0].trim());
    			}
    		}
    		$scope.entitiesForFilter.sort();					// sorts the entities in the list in ascending order
    		$scope.entitiesForFilter.unshift("All Entities");
    		
    		/**
    		 * if entityIndex is 1, then response has fetched both aspects and entities
    		 * with aspects in 0th index and entities in 1st index
    		 */
    		if(entityIndex){
    			var wordcloudaspect = response.data[0].rows;
				var wordcloudaspectHead = response.data[0].columnNames;
				for(i=0;i<wordcloudaspect.length;i++){
			    	var aspect = {};
					aspect.text = wordcloudaspect[i][0];
					aspect.size = wordcloudaspect[i][1];
					aspect.sentiment = wordcloudaspect[i][2];
					aspect.type = "context";
					
					// aspects are assigned colors depending on their respective sentiments
					if(wordcloudaspect[i][2]=='Positive'){
						aspect.color = GREEN;
					}else if(wordcloudaspect[i][2]=='Negative'){
						aspect.color = RED;
					}else{
						aspect.color = YELLOW;
					}
					wordcloudArray.push(aspect);
				}
    		}
    		
			generateWordcloud(wordcloudArray,entityArray);			
		}
		
		// function for onload word cloud in trending topics / wordcloud post list section
		$scope.fn_word_cloud = function(topic){
			
			//alert($scope.selectedSentiment)
			$scope.contextList = false;
			//alert($scope.filterEntity);
			//alert(topic)
			$scope.wordCloudAspectListCurPage = 1;
			$scope.filterEntity = '';
			var data = $.param({
				"accountId": authId,
				"selectedContext": topic == null ? null : String (topic),
				"sourceId": ($scope.selectedSource == 'All' || $scope.selectedSource == '') ? -1 : $scope.selectedSource,
				"startDate": toGMTDate($scope.analyticsFrom),
				"endDate": toGMTDate($scope.analyticsTo),
				"sentiment": ($scope.selectedSentiment  == 'All' || $scope.selectedSentiment  == '') ? null : $scope.selectedSentiment
	        });
			if(topic!=null && topic!="All Contexts"){
				//alert(7)
				// Need to call social post list filter here.
				
				$scope.filterContext = String (topic);
				$http.post(getEntitiesForSelectedContextUrlClick, data, $scope.config).then(function(response){
					$scope.wordcloudContext = topic;
					$scope.fn_word_cloud_array_population(response, 0);
				});
				
				filteredAnalyticsDetailsObj.accountId = authId;
				filteredAnalyticsDetailsObj.rowSourceTopic = topic;
				filteredAnalyticsDetailsObj.detailsTabName = WORD_CLOUD;
				//alert(1);
				// This is called for wordcloud post list filter on wordcloud click
				$scope.fn_filtered_source_date_analytics_details_click(filteredAnalyticsDetailsObj);
				
			}else{
				$scope.filterContext = '';
				if($scope.selectedTopic != "All Contexts" && $scope.selectedTopic != "" && $scope.selectedTopic != topic){
					filteredAnalyticsDetailsObj.accountId = authId;
					filteredAnalyticsDetailsObj.rowSourceTopic = topic;
					filteredAnalyticsDetailsObj.detailsTabName = WORD_CLOUD;
					$scope.fn_filtered_source_date_analytics_details(filteredAnalyticsDetailsObj);
	    		} else {
	    			filteredAnalyticsDetailsObj.accountId = authId;
					filteredAnalyticsDetailsObj.rowSourceTopic = null;
					filteredAnalyticsDetailsObj.detailsTabName = WORD_CLOUD;
					$scope.fn_filtered_source_date_analytics_details(filteredAnalyticsDetailsObj);
	    		}
				$http.post(getWordCloudDataUrl, data, $scope.config).then(function(response){
					$scope.wordcloudContext = "All Contexts";
					$scope.fn_word_cloud_array_population(response, 1);
				});
			}
		}
		
		// function for filtered word cloud in trending topics section
		$scope.fn_word_cloud_filtered = function(){
			//alert(3)
			$scope.filterEntity = '';
			var data = $.param({
				"accountId": authId,
				"selectedContext": ($scope.selectedTopic  == 'All Contexts' || $scope.selectedTopic  == '') ? null : String ($scope.selectedTopic),
				"sourceId": ($scope.selectedSource == 'All' || $scope.selectedSource == '') ? -1 : $scope.selectedSource,
				"startDate": toGMTDate($scope.analyticsFrom),
				//"startDate": null,
				"endDate": toGMTDate($scope.analyticsTo),
				//"endDate": null,
				"sentiment": ($scope.selectedSentiment  == 'All' || $scope.selectedSentiment  == '') ? null : $scope.selectedSentiment
	        });
			if($scope.selectedTopic !='All Contexts' && $scope.selectedTopic  != ''){
				$http.post(getFilteredWordCloudDataUrl, data, $scope.config).then(function(response){
					$scope.filterContext = String($scope.selectedTopic);
					$scope.wordcloudContext = String ($scope.selectedTopic);
					$scope.fn_word_cloud_array_population(response, 0);
				});
			}else{
				$scope.filterContext = '';
				$http.post(getFilteredWordCloudDataUrl, data, $scope.config).then(function(response){
					$scope.wordcloudContext = "All Contexts";
					$scope.fn_word_cloud_array_population(response, 1);
				});
			}
		}
		
		// function for deep listening table on filtering
		$scope.fn_deep_listening_YUM = function(socialListeningObj,socialNewPage){
			console.log(socialListeningObj.rowSourceId + "====================================================");
			var data = $.param({
				"inAccountId": socialListeningObj.accountId, 
		        "inSource": socialListeningObj.rowSourceId, 
		        "inSearchKeyword": $scope.searchByText,
		        "inSourceCategory": socialListeningObj.sourceCategory,
		        "inTopic": socialListeningObj.rowSourceTopic, 
		        "start_dt": socialListeningObj.startDt,
		        "end_dt" : socialListeningObj.endDt,
		        "virality" :socialListeningObj.virality,
	            "sentiment":socialListeningObj.sentiment,
	            "city":socialListeningObj.city,
		        "details_tab_name" : socialListeningObj.detailsTabName,
	            "perPageEntries" : $scope.socialEntries,
	            "newPage" : socialNewPage
	        });
			$scope.deepListSocLists = null;
			$scope.deepListNonSocLists = null;
			$http.post(deepListeningUrlFilter, data, $scope.config).then(function(response){
				//$scope.totalSocialCount = response.data.count;
				//As the count comes with adding upper result.
				//$scope.totalSocialCount = $scope.totalSocialCount/2;
				$scope.deepListSocLists = response.data.output.socialPostListObj;
				$scope.totalSocialCount = $scope.deepListSocLists[0].totalCount;
				$scope.deepListNonSocLists = response.data.output.nonSocialPostListObj;
				$scope.filtered = false;
			});
		}
		
		// function for deep listening table on filtering
		$scope.fn_deep_listening_YUM_pag_change = function(socialListeningObj,socialNewPage){
			console.log(socialListeningObj.rowSourceId + "====================================================");
			var data = $.param({
				"inAccountId": socialListeningObj.accountId, 
		        "inSource": socialListeningObj.rowSourceId, 
		        "inSearchKeyword": $scope.searchByText,
		        "inSourceCategory": socialListeningObj.sourceCategory,
		        "inTopic": socialListeningObj.rowSourceTopic, 
		        "start_dt": socialListeningObj.startDt,
		        "end_dt" : socialListeningObj.endDt,
		        "virality" :socialListeningObj.virality,
	            "sentiment":socialListeningObj.sentiment,
	            "city":socialListeningObj.city,
		        "details_tab_name" : socialListeningObj.detailsTabName,
	            "perPageEntries" : $scope.socialEntries,
	            "newPage" : socialNewPage
	        });
			$scope.deepListSocLists = null;
			$scope.deepListNonSocLists = null;
			$http.post(deepListeningUrlFilter, data, $scope.config).then(function(response){
				//$scope.totalSocialCount = response.data.count;
				//$scope.totalSocialCount = $scope.totalSocialCount/2;
				//alert($scope.totalSocialCount)
				$scope.deepListSocLists = response.data.output.socialPostListObj;
				//alert($scope.deepListSocLists)
				$scope.deepListNonSocLists = response.data.output.nonSocialPostListObj;
				$scope.filtered = false;
			});
		}
		
		
		/**
		 * the following function is called when the Go button of the filter
		 * in analytics/Explore page is clicked
		 */
		$scope.fn_filter = function(){
			$scope.wordCloudAspectListCurPage = 1;
			$scope.contextDetailsCurPage = 1;
			
			$scope.activeTab = 'social';
			filteredEntityDetailsObj.accountId = authId;
			filteredEntityDetailsObj.rowSourceId = ($scope.selectedSource == 'All' || $scope.selectedSource == '') ? -1 : $scope.selectedSource;
			filteredEntityDetailsObj.rowSourceTopic = ($scope.selectedTopics  == 'All Contexts' || $scope.selectedTopics  == '') ? null : String ($scope.selectedTopics);
			filteredEntityDetailsObj.startDt = toGMTDate($scope.analyticsFrom);
			filteredEntityDetailsObj.endDt = toGMTDate($scope.analyticsTo);
			filteredEntityDetailsObj.sentiment = ($scope.selectedSentiment  == 'All' || $scope.selectedSentiment  == '') ? null : $scope.selectedSentiment;
			$scope.fn_filtered_entity_details(filteredEntityDetailsObj);

			// copied from yum as discussed today evening
			socialListeningObj.accountId = authId; 
			socialListeningObj.rowSourceId = ($scope.selectedSource == 'All' || $scope.selectedSource == '') ? -1 : $scope.selectedSource;
			socialListeningObj.rowSourceTopic = ($scope.selectedTopics.length+1 == $scope.topics.length)?null:$scope.selectedTopics.toString();
			socialListeningObj.sourceCategory = String ($scope.selectedSourceCategory);
			socialListeningObj.startDt =  toGMTDate($scope.analyticsFrom);
			socialListeningObj.endDt = toGMTDate($scope.analyticsTo);
			socialListeningObj.city = null;
			socialListeningObj.sentiment = ($scope.selectedSentiment  == 'All' || $scope.selectedSentiment  == '') ? Number(ALL_SENTIMENTS_100) : $scope.selectedSentiment;//($scope.selectedSentiment);
			socialListeningObj.detailsTabName = SOCIAL_LISTENING;

			// object for onload Deep Listening table table
			$scope.socialPagination = {
					socialCurrentPage: 1,
					socialNewPage: 1
			};
			$scope.totalSocialCount = 0 ;
			
			$scope.fn_deep_listening_YUM(socialListeningObj,1);
			
			
			$scope.fn_word_cloud_filtered();
			filteredAnalyticsDetailsObj.accountId = authId; 
			filteredAnalyticsDetailsObj.rowSourceId = ($scope.selectedSource == 'All' || $scope.selectedSource == '') ? -1 : $scope.selectedSource;
			filteredAnalyticsDetailsObj.rowSourceTopic = ($scope.selectedTopics  == 'All Contexts' || $scope.selectedTopics  == '') ? null : String ($scope.selectedTopics);
			filteredAnalyticsDetailsObj.startDt =  toGMTDate($scope.analyticsFrom);
			filteredAnalyticsDetailsObj.endDt = toGMTDate($scope.analyticsTo);
			filteredAnalyticsDetailsObj.sentiment = ($scope.selectedSentiment  == 'All' || $scope.selectedSentiment  == '') ? null : $scope.selectedSentiment;
			filteredAnalyticsDetailsObj.detailsTabName = WORD_CLOUD;
			$scope.fn_filtered_source_date_analytics_details(filteredAnalyticsDetailsObj);
			
		}
		
		$scope.fn_view_filter = function(source, entity){
			$scope.wordCloudAspectListCurPage = 1;
			$scope.contextDetailsCurPage = 1;
			/*$scope.selectedSource = source;
			$scope.selectedTopics = entity;
			$scope.fn_filter();
			$scope.selectedSource = BLANK_STRING;
			$scope.selectedTopics = BLANK_STRING;*/
			$scope.activeTab = 'social';
			filteredEntityDetailsObj.accountId = authId;
			filteredEntityDetailsObj.rowSourceId = (source == 'All' || source == '') ? -1 : source;
			filteredEntityDetailsObj.rowSourceTopic = (entity  == 'All Contexts' || entity  == '') ? null : String (entity);
			filteredEntityDetailsObj.startDt = toGMTDate($scope.analyticsFrom);
			filteredEntityDetailsObj.endDt = toGMTDate($scope.analyticsTo);
			filteredEntityDetailsObj.sentiment = ($scope.selectedSentiment  == 'All' || $scope.selectedSentiment  == '') ? null : $scope.selectedSentiment;
			$scope.fn_filtered_entity_details(filteredEntityDetailsObj);

			// copied from yum as discussed today evening
			socialListeningObj.accountId = authId; 
			socialListeningObj.rowSourceId = (source == 'All' || source == '') ? -1 : source;
			socialListeningObj.rowSourceTopic = (entity.length+1 == $scope.topics.length)?null:entity.toString();
			socialListeningObj.sourceCategory = String ($scope.selectedSourceCategory);
			socialListeningObj.startDt =  toGMTDate($scope.analyticsFrom);
			socialListeningObj.endDt = toGMTDate($scope.analyticsTo);
			socialListeningObj.city = null;
			socialListeningObj.sentiment = ($scope.selectedSentiment  == 'All' || $scope.selectedSentiment  == '') ? Number(ALL_SENTIMENTS_100) : $scope.selectedSentiment;//($scope.selectedSentiment);
			socialListeningObj.detailsTabName = SOCIAL_LISTENING;

			// object for onload Deep Listening table table
			$scope.socialPagination = {
					socialCurrentPage: 1,
					socialNewPage: 1
			};
			$scope.totalSocialCount = 0 ;
			$scope.fn_deep_listening_YUM(socialListeningObj,1);
			
			
			$scope.fn_word_cloud_filtered();
			filteredAnalyticsDetailsObj.accountId = authId; 
			filteredAnalyticsDetailsObj.rowSourceId = (source == 'All' || source == '') ? -1 : source;
			filteredAnalyticsDetailsObj.rowSourceTopic = (entity  == 'All Contexts' || entity  == '') ? null : String (entity);
			filteredAnalyticsDetailsObj.startDt =  toGMTDate($scope.analyticsFrom);
			filteredAnalyticsDetailsObj.endDt = toGMTDate($scope.analyticsTo);
			filteredAnalyticsDetailsObj.sentiment = ($scope.selectedSentiment  == 'All' || $scope.selectedSentiment  == '') ? null : $scope.selectedSentiment;
			filteredAnalyticsDetailsObj.detailsTabName = WORD_CLOUD;
			$scope.fn_filtered_source_date_analytics_details(filteredAnalyticsDetailsObj);

			
		}
		
		// Click on entity from wordcloud, it filters the entity list
		$scope.fn_filterByEntity = function(selectedEntity){
			//alert("Entity : " + selectedEntity);
			if(selectedEntity == 'All Entities'){
				selectedEntity = '';
			}
			if($scope.entityList){
				$scope.entityList=!$scope.entityList;
				$scope.filterContext = '';
			}
			$scope.filterEntity = selectedEntity;
			
			// NOTE : need to call to get post list against entity , not by aspect. Need to change in same sp or write new sp by coping the same and 
			// changing the aspect filter into entity. 
			// Bellow code is ok but its filtering against aspect ,
			/*filteredAnalyticsDetailsObj.accountId = authId;
			filteredAnalyticsDetailsObj.rowSourceTopic = selectedEntity;
			filteredAnalyticsDetailsObj.detailsTabName = "Entity_click";
			$scope.fn_filtered_source_date_analytics_details(filteredAnalyticsDetailsObj);*/
			
			filteredAnalyticsDetailsObj.accountId = authId;
			filteredAnalyticsDetailsObj.rowSourceTopic = selectedEntity;
			filteredAnalyticsDetailsObj.detailsTabName = "Entity_click";
			//alert(1);
			// This is called for wordcloud post list filter on wordcloud click
			$scope.fn_filtered_source_date_analytics_details_click(filteredAnalyticsDetailsObj);

		}
		
		/**
		 * This function is called for populating data of Explore page during on-Load for sections - 
		 * Context Details, Deep Listening and Word Cloud Deep Listening Tables.
		 */
		$scope.fn_fetch_analytics_onLoad_data = function (sectionName){
			var data = $.param({
				"accountId": authId,
				"sectionName":sectionName
	        });
			$http.post(getAnalyticsOnLoadDataUrl, data, $scope.config).then(function(response){
				switch (sectionName){
					case 'context_details':
						$scope.fn_populate_context_details(response);
						break;
					case 'social_listening':
						$scope.fn_populate_deep_listening(response);
						break;
					case 'word_cloud':
						$scope.fn_populate_word_cloud_deep_listening(response);
						break;
					default:
				}
			});
		}
		
		/**
		 * This function is called for populating data of the wordcloud onload.
		 */
		$scope.fn_fetch_word_cloud_onLoad_data = function(){
			var data = $.param({
				"accountId": authId
	        });
			$http.post(getWordCloudOnLoadDataUrl, data, $scope.config).then(function(response){
				$scope.wordcloudContext = "All Contexts";
				$scope.fn_word_cloud_array_population(response, 1);
			});
		}
		
		/**
		 * This function populates the Context Details table with the response data.
		 */
		$scope.fn_populate_context_details = function(response){
			var data = response.data.results.rows;
			// date strings are converted from GMT date objects to local date objects for applying date format while HTML binding
		    /*for (var i=0; i<data.length;i++) {
		    	//PG09052016 -To Fix IE and Safari Issue
		    	data[i][7] = new Date((data[i][7]).replace(/-/g, "/"));
		    	data[i][8] = new Date((data[i][8]).replace(/-/g, "/"));
		    	data[i][7] = new Date(data[i][7].valueOf() - data[i][7].getTimezoneOffset() * 60000);
		    	data[i][8] = new Date(data[i][8].valueOf() - data[i][8].getTimezoneOffset() * 60000);
		    }*/
		    $scope.entities = data;
		    for(var i=0;i<$scope.entities.length;i++){
				$scope.entities[i].push(Math.round($scope.entities[i][4]*$scope.entities[i][6]/100))
		    }
		}
		
		/**
		 * This function populates the Social Post List Table of Word Cloud with the response data.
		 */
		$scope.fn_populate_word_cloud_deep_listening = function(response){
    		$scope.wordCloudTableData = [];
    		var keysArray = response.data.results.column_names;
    		var valuesArray = response.data.results.rows;
    		for(var i=0;i<response.data.results.rows.length;i++){
    			var data = {};
    			for(var j=0;j<keysArray.length;j++){
    				data[keysArray[j]] = valuesArray[i][j];
    			}
    			$scope.wordCloudTableData.push(data);
    		}
    		$scope.entitiesForFilter = [];
    		for(i=0;i<$scope.wordCloudTableData.length;i++){
    			if($scope.entitiesForFilter.indexOf($scope.wordCloudTableData[i].entity) == -1){
    				$scope.entitiesForFilter.push($scope.wordCloudTableData[i].entity.trim());
    			}
    		}
    		$scope.entitiesForFilter.sort();
    		$scope.entitiesForFilter.unshift("All Entities");
		}
		
		/**
		 * This function populates the social list near wordcloud Table with the response data.
		 */
		$scope.fn_populate_deep_listening = function(response){
			var data = response.data.results.rows;
			for (var i=0; i<data.length;i++) {
		    	data[i][4] = toLocalDate(data[i][4]);
		    }
			$scope.soclists = data;
		}
		
		//Function to update overall sentiment when user changes it from deep listing table 
		$scope.updateOverallSentiment = function(sentimentID,postID,originalSentiment,postText,postDateTime){
			swal("Are you sure you want to update the displayed sentiment ?", {
				  className: "update-overall-sentiment-modal",
				  buttons: {
				    cancel: "No",
				    confirm: {
				      text: "Yes",
				      value: "confirm",
				    }			  
				  },
				})
				.then((value) => {
					if(value == "confirm"){
	            		$scope.overAllSentiment = "Neutral";
	        			$scope.TxtColor = "#F6BC41";
	        			if(sentimentID == "1"){
	        				$scope.overAllSentiment = "Positive";
	        				$scope.TxtColor = "#8DC153";
	        			}else if(sentimentID == "-1"){
	        				$scope.overAllSentiment = "Negative";
	        				$scope.TxtColor = "#f55348";
	        			}
	        			
	        			if ($scope.selectedcompany instanceof Array) {
	        				  var cid = $scope.selectedcompany[0];
	        				} else {
	        				  var cid =$scope.selectedcompany;
	        				}
	        			
	        			var dataSentimentUpdater = $.param({
	        				"company_Id": authPrimaryCompanyID,
	        				"sentimentID": sentimentID,
	        				"postID":postID,
	        				"originalSentiment": originalSentiment,
	        				"postText":postText,
	        				"postDateTime" :postDateTime,
	        				"inAccountId":authId,
	        				"tabName":"View"
	        	        });
	        			
	        			
	        			$http.post(sentimentInsertUpdaterUrl, dataSentimentUpdater, $scope.config).then(function(response){
        					if(response.data == "1"){
        						$("#sentiment_"+postID).text($scope.overAllSentiment);
        						$("#sentiment_"+postID).css('color',$scope.TxtColor);
        						$("#tt_"+postID).css('color',$scope.TxtColor);
        						swal("", "Thank you for your feedback.we will train our xpresso engine accordingly.", "success");
        					} else {
        						 swal("", "Request Failed...Please try again", "error");
        					}
	        			});
					}
				});

		}
		
		$scope.fn_invalidate_entry = function(postID,selection){
			if($("#btnInvalid_"+postID).text() != 'Marked as Invalid')
			swal("Are you sure you want to invalidate data ?", {
				  className: "update-overall-sentiment-modal",
				  buttons: {
				    cancel: "No",
				    confirm: {
				      text: "Yes",
				      value: "confirm",
				    }			  
				  },
				})
				.then((value) => {
					if(value == "confirm"){
	        			
	        			var dataInvalidUpdater = $.param({
	        				"postID":postID
	        	        });
	        			
	        			$http.post(sentimentInsertUpdaterUrl, dataInvalidUpdater, $scope.config).then(function(response){
	        					if(response.data == "1"){
	        						if(selection == 'comment'){
	        							$("#btnInvalid_"+postID).addClass('label-danger label-invalid_comment pointer').text('Marked as Invalid');
	        						} else {
	        							$("#btnInvalid_"+postID).addClass('label-danger label-invalid pointer').text('Marked as Invalid');
	        						}
	        						
	        						$("#btnInvalid_"+postID).parent().addClass('disabled').removeAttr("ng-click");
	        						swal("", "The selection made invalid.", "success");
	        					} else {
	        						 swal("", "Request Failed...Please try again", "error");
	        					}
	        			});
	             }
			});
		}
		

		//Function to update overall sentiment when user changes it from deep listing table 
		$scope.updateReviewAnalysisOverallSentiment = function(sentimentID,reviewID){
		swal("Are you sure you want to update the displayed sentiment ?", {
			  buttons: {
			    cancel: "No",
			    confirm: {
			      text: "Yes",
			      value: "confirm",
			    }			  
			  },
			})
			.then((value) => {
				if(value == "confirm"){
            		$scope.overAllSentiment = "Neutral";
        			$scope.TxtColor = "#F6BC41";
        			if(sentimentID == "1"){
        				$scope.overAllSentiment = "Positive";
        				$scope.TxtColor = "#8DC153";
        			}else if(sentimentID == "-1"){
        				$scope.overAllSentiment = "Negative";
        				$scope.TxtColor = "#f55348";
        			}
        			
        			if ($scope.selectedcompany instanceof Array) {
        				  var cid = $scope.selectedcompany[0];
        				} else {
        				  var cid =$scope.selectedcompany;
        				}
        			
        			var dataSentimentUpdater = $.param({
        				"company_Id": authPrimaryCompanyID,
        				"sentimentID": sentimentID,
        				"reviewID":reviewID
        	        });
        			
        			
        			$http.post(sentimentUpdaterUrlForReviewAnalysis, dataSentimentUpdater, $scope.config).then(function(response){
    					if(response.data == "1"){
    						$("#sentiment_"+reviewID).text($scope.overAllSentiment);
    						$("#sentiment_"+reviewID).css('color',$scope.TxtColor);
    						$("#tt_"+reviewID).css('color',$scope.TxtColor);

    						swal("", "Thank you for your feedback.we will train our xpresso engine accordingly.", "success");
    					} else{
    						swal("", "Request Failed...Please try again", "error");
						}
        			});
				}
			});
		}
		
		/**
		 * This function populates the snippet level post details when user click on view more in deeplistining.
		 */
		$scope.fn_showPostDetails = function(postId){
			$scope.postSnippets = [];
			
			var data = $.param({
				"postId": postId,
				"companyId": authPrimaryCompanyID
	        });
			
			$http.post(getPostDetailsSnipperwiseDataUrl, data, $scope.config).then(function(response){
				var dataSnippet = response.data.results.rows;
				//Highlight the entity
				for(var inc = 0; inc < dataSnippet.length; inc++){
					var snipetValue = dataSnippet[inc][0];
					var sentimentValue = dataSnippet[inc][2];
					var entityValue = dataSnippet[inc][3];
					var starHTML = $scope.fn_hilight_snippet_entity(snipetValue,entityValue,sentimentValue);
					dataSnippet[inc].push(starHTML);
				}
				$scope.postSnippets = dataSnippet;
			});
		}
		
		
		/**
		 * This function populates the snippet level post details when user click on view more in deeplistining.
		 */
		$scope.fn_showPostDetailsForReviewAnalysisTab = function(reviewId){
			$scope.postSnippets = [];
			
			var data = $.param({
				"reviewId": reviewId,
				"companyId": authPrimaryCompanyID
	        });
			
			$http.post(getPostDetailsSnipperwiseDataUrlForReviewAnalysis, data, $scope.config).then(function(response){
				var dataSnippet = response.data.results.rows;
				//Highlight the entity
				for(var inc = 0; inc < dataSnippet.length; inc++){
					var snipetValue = dataSnippet[inc][0];
					var sentimentValue = dataSnippet[inc][2];
					var entityValue = dataSnippet[inc][3];
					var starHTML = $scope.fn_hilight_snippet_entity(snipetValue,entityValue,sentimentValue);
					dataSnippet[inc].push(starHTML);
				}
				$scope.postSnippets = dataSnippet;
			});
			
		}
		
		 $scope.fn_showFeedbacksDetails = function(source,day,val){

         	$scope.postSnippets=[];
         	$scope.feedbackOrComplaint="Feedback";
         	$scope.noSnippets="";
 			 if(val ==0 || val == "0"){
 				 $scope.noSnippets ="No data found.";
 					 return false;
 			 }
         	
         	if(day=="PREVIOUS"){
     			var dataDay = $.param({
     				"selectedSourceId":source,
     				"companyId": authPrimaryCompanyID,
     				"inStartDt" : $('#periodicPrevious').val().split(" - ")[0],//$scope.$parent.periodicCurrent,
     				"inEndDt": $('#periodicPrevious').val().split(" - ")[1]//$scope.$parent.periodicPrevious
     	        });
     			
     			$http.post(getFeedbackData, dataDay, $scope.config).then(function(response){
     				
     				$scope.postSnippets = response.data.results.rows;
     			});
     		  }	else {
     			var dataNotDay = $.param({
     				"selectedSourceId":source,
     				"companyId": authPrimaryCompanyID,
     				"inStartDt" :$('#periodicCurrent').val().split(" - ")[0],//$scope.$parent.periodicCurrent,
     				"inEndDt":$('#periodicCurrent').val().split(" - ")[1]//$scope.$parent.periodicPrevious
     	        });
     			$http.post(getFeedbackData, dataNotDay, $scope.config).then(function(response){
     				$scope.postSnippets = response.data.results.rows;
     			});
         	}
         } 
         
         $scope.fn_showComplaintsDetails = function(source,day,val){
        	 $scope.postSnippets=[];
        	 $scope.noSnippets="";
        	 $scope.feedbackOrComplaint="Complaint";
 			 if(val ==0 || val == "0"){
 				 $scope.noSnippets ="No data found.";
 					 return false;
 			 }
			 
         	if(day=="PREVIOUS"){
     			var data = $.param({
     				"selectedSourceId":source,
     				"companyId": authPrimaryCompanyID,
     				"inStartDt" : $('#periodicPrevious').val().split(" - ")[0],//$scope.$parent.periodicCurrent,
     				"inEndDt": $('#periodicPrevious').val().split(" - ")[1]//$scope.$parent.periodicPrevious
     	        });
     			
     			$http.post(getComplaintData, data, $scope.config).then(function(response){
     				$scope.postSnippets = response.data.results.rows;
     			});
         	}
         	else{

     			var data = $.param({
     				"selectedSourceId":source,
     				"companyId": authPrimaryCompanyID,
     				"inStartDt" :$('#periodicCurrent').val().split(" - ")[0],//$scope.$parent.periodicCurrent,
     				"inEndDt":$('#periodicCurrent').val().split(" - ")[1]//$scope.$parent.periodicPrevious
     	        });
     			
     			$http.post(getComplaintData, data, $scope.config).then(function(response){
     				$scope.postSnippets = response.data.results.rows;
     			});	
         	}
         } 
         
         $scope.fn_showFeedbacksDetailsForReviewAnalysis = function(source,day,val){
        	 $scope.postSnippets=[];
        	 $scope.noSnippets="";
        	 $scope.feedbackOrComplaint="Feedback";
 			 if(val ==0 || val == "0"){
 				 $scope.noSnippets ="No data found.";
 					 return false;
 			 }
			 
          	if(day=="PREVIOUS"){
      			var data = $.param({
      				"selectedSourceId":source,
      				"inAccountId": authId,
      				"companyId": authPrimaryCompanyID,
      				"inStartDt" : $('#periodicPrevious').val().split(" - ")[0],//$scope.$parent.periodicCurrent,
      				"inEndDt": $('#periodicPrevious').val().split(" - ")[1]//$scope.$parent.periodicPrevious
      	        });
      			
      			$http.post(getFeedbackDataForReviewAnalysis, data, $scope.config).then(function(response){
      				$scope.postSnippets = response.data.results.rows;
      			});
          	} else {
      			var data = $.param({
      				"selectedSourceId":source,
      				"companyId": authPrimaryCompanyID,
      				"inAccountId": authId,
      				"inStartDt" :$('#periodicCurrent').val().split(" - ")[0],//$scope.$parent.periodicCurrent,
      				"inEndDt":$('#periodicCurrent').val().split(" - ")[1]//$scope.$parent.periodicPrevious
      	        });
      			$http.post(getFeedbackDataForReviewAnalysis, data, $scope.config).then(function(response){
      				$scope.postSnippets = response.data.results.rows;
      			});
          	}
          } 
          
          
          $scope.fn_showComplaintsDetailsForReviewAnalysis = function(source,day,val){
        	  
        	 $scope.postSnippets=[];
        	$scope.feedbackOrComplaint="Complaint";
        	 $scope.noSnippets="";
 			 if(val ==0 || val == "0"){
 				 $scope.noSnippets ="No data found.";
 					 return false;
 			 }
          
          	if(day=="PREVIOUS"){
      			var data = $.param({
      				"selectedSourceId":source,
      				"companyId": authPrimaryCompanyID,
      				"inStartDt" : $('#periodicPrevious').val().split(" - ")[0],//$scope.$parent.periodicCurrent,
      				"inEndDt": $('#periodicPrevious').val().split(" - ")[1]//$scope.$parent.periodicPrevious
      	        });
      			
      			$http.post(getComplaintDataForReviewAnalysis, data, $scope.config).then(function(response){
      				$scope.postSnippets = response.data.results.rows;
      			});
             } else {
      			var data = $.param({
      				"selectedSourceId":source,
      				"companyId": authPrimaryCompanyID,
      				"inStartDt" :$('#periodicCurrent').val().split(" - ")[0],//$scope.$parent.periodicCurrent,
      				"inEndDt":$('#periodicCurrent').val().split(" - ")[1]//$scope.$parent.periodicPrevious
      	        });
      			$http.post(getComplaintDataForReviewAnalysis, data, $scope.config).then(function(response){
      				$scope.postSnippets = response.data.results.rows;
      			});
          	}
          } 
         
          
          $scope.fn_showPostsForTopAdvocates = function(authorName){
        	  $scope.postSnippets=[];
  	
  			 var data = $.param({
    				"selectedSourceId":($scope.advocateSource == ALL_SOURCES || $scope.advocateSource == BLANK_STRING) ? -1 : $scope.advocateSource,
    				"companyId": authPrimaryCompanyID,
    				"inAuthorName":authorName,
    				"inStartDt": toGMTDate($scope.advocateFrom),
    				"inEndDt": toGMTDate($scope.advocateTo),
        	        });
    			
    			$http.post(getTopAdvocatesData, data, $scope.config).then(function(response){
    				$scope.postSnippets  = response.data.results.rows;
    				$('#TopAdvocatesModal').modal('show');
    			});
            } 
           
									
		//Highlighting entity in the snippet and adding background color as per sentiment.
		$scope.fn_hilight_snippet_entity = function(snipetValue,entityValue,sentimentValue){
			if(snipetValue.toLowerCase().indexOf(entityValue.toLowerCase())){
				return snipetValue.toLowerCase().replace(entityValue.toLowerCase(), "<span class='highlight-txt bg-"+sentimentValue+"'>"+entityValue+"</span>");
			} else {
				return snipetValue;
			}
		}
		
		$scope.aspectDistributionPieChart = function(){
			aspectDistributionPieChart()
		}
		
		
		
		/**
		 * the following function is called when the Go button of the filter
		 * in analytics/Explore page is clicked
		 */
		$scope.saveAsExcel = function(){
			
			socialListeningObj.accountId = authId; 
			socialListeningObj.rowSourceId = ($scope.selectedSource == 'All' || $scope.selectedSource == '') ? -1 : $scope.selectedSource;
			socialListeningObj.rowSourceTopic = ($scope.selectedTopics.length+1 == $scope.topics.length)?null:$scope.selectedTopics.toString();
			socialListeningObj.sourceCategory = String ($scope.selectedSourceCategory);
			socialListeningObj.startDt =  toGMTDate($scope.analyticsFrom);
			socialListeningObj.endDt = toGMTDate($scope.analyticsTo);
			socialListeningObj.city = null;
			socialListeningObj.sentiment = ($scope.selectedSentiment  == 'All' || $scope.selectedSentiment  == '') ? Number(ALL_SENTIMENTS_100) : $scope.selectedSentiment;//($scope.selectedSentiment);
			socialListeningObj.detailsTabName = SOCIAL_LISTENING;

			// object for onload Deep Listening table table
			$scope.socialPagination = {
					socialCurrentPage: 1,
					socialNewPage: 1
			};
			$scope.totalSocialCount = 0 ;
			
			$scope.fn_saveDataInExcelFormat(socialListeningObj,1);
			
		}
		
		
		$scope.fn_saveDataInExcelFormat = function(socialListeningObj,socialNewPage){
			var data = $.param({
				"inAccountId": socialListeningObj.accountId, 
		        "inSource": socialListeningObj.rowSourceId, 
		        "inSearchKeyword": $scope.searchByText,
		        "inSourceCategory": socialListeningObj.sourceCategory,
		        "inTopic": socialListeningObj.rowSourceTopic, 
		        "start_dt": socialListeningObj.startDt,
		        "end_dt" : socialListeningObj.endDt,
		        "virality" :socialListeningObj.virality,
	            "sentiment":socialListeningObj.sentiment,
	            "city":socialListeningObj.city,
		        "details_tab_name" : socialListeningObj.detailsTabName,
	            "perPageEntries" : $scope.socialEntries,
	            "newPage" : socialNewPage
	        });
			$scope.deepListSocLists = null;
			$scope.deepListNonSocLists = null;
			$http.post(deepListUrlToSaveData, data, $scope.config).then(function(response){
				$scope.deepListSocLists = response.data.output.socialPostListObj;
				$scope.totalSocialCount = $scope.deepListSocLists[0].totalCount;
				$scope.deepListNonSocLists = response.data.output.nonSocialPostListObj;
				$scope.filtered = false;
			});
		}
		
		
		//Parsing HTML to render
		$scope.toTrustedHTML = function (html) {
		   return $sce.trustAsHtml(html);
		 };
		 
		 //Adding post id into feedback url from deep listining table when user click on action button.
		 $scope.setFeedbackURL = function(postID,sourceTypeId){
			 $("#postId").val(postID+'#@#'+sourceTypeId);
			 var radioValue = $("input[name='feedbackPriority']:checked").val();
			 $('#feedbackUrl').val(customerFeedbackURL+'?'+ encodeURIComponent("x="+postID)+'&'+ encodeURIComponent("s="+sourceTypeId)+'&'+ encodeURIComponent("p="+radioValue));
			 $('#lblFedbackURL').attr('href',customerFeedbackURL+'?'+ encodeURIComponent("x="+postID)+'&'+ encodeURIComponent("s="+sourceTypeId)+'&'+ encodeURIComponent("p="+radioValue));
			 
			 //Get status if any ticket has already been created against this post id :
			 var dataTicketStatus = $.param({
   				"postID":postID
   	        });
   			$http.post(checkIfTicketAvailable, dataTicketStatus, $scope.config).then(function(response){
   				if(response.data.results.rows == 1){
   					$scope.ticketAvalibilityStatus = "Ticket has already been generated against this feedback! "
   				} else {
   					$scope.ticketAvalibilityStatus = "";
   				}
   			});
   			
   			
   			$scope.selectedSummaryCategory = "0";
   			$scope.feedbackQuestionArr =[];
   			$scope.feedbackQuestionArr.unshift(["0", "Select Question", true]);
   			$scope.feedbackTextArr =[];
   			$("#feedbackUserInput").val('');
		 };
		 
		 $scope.loadQuestions = function(){
	    	$http.get(getSourcesTopicsUrl+'?accountId='+$scope.selectedSummaryCategory+'&type='+"feedbackquestions").success( function(response) {
		    	$scope.feedbackQuestionArr = response.results.rows;
		    	
		    	for(var i=0;i<response.results.rows.length;i++){
		    		$scope.feedbackQuestionArr[i].push(true);
		    	}
		    	
		    	$scope.feedbackQuestionArr.unshift(["0", "Select Question", true]);
		    });
		 };
		 $scope.loadFeedbacks = function(){
			$('#feedbackUserInput').val('');
	    	$http.get(getSourcesTopicsUrl+'?accountId='+$scope.selectedSummaryQuestion+'&type='+"feedbacktext").success( function(response) {
		    	$scope.feedbackTextArr = response.results.rows;
		    	
		    	for(var i=0;i<response.results.rows.length;i++){
		    		$scope.feedbackTextArr[i].push(true);
		    	}
		    });
		 };
		 
		 
		$scope.reFormFeedbackURL = function(selectedPriority){
			 $('#feedbackUrl').val($('#feedbackUrl').val().slice(0, -1) + selectedPriority);
			 $('#lblFedbackURL').attr('href',$('#feedbackUrl').val().slice(0, -1) + selectedPriority);
		};
		//********************************************* End Analytics *************************************************//
		
		//Download CSV for Invalid data 
		$scope.fn_download_invalid_csv = function() {
			var url = $location.absUrl().split('?')[0];
			url = url.replace('index.jsp','');
			var finalUrl = url.trim();
			$window.open(finalUrl+'DownloadInvalidReport?inAccountId='+authId, '_blank');
		};
	});
})(window.angular);