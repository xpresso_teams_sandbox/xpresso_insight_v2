// This file contains all the service urls used throughout the project

// contextPath of the project for all paths in service_url.js
var contextPath = $('#contextPath').val().trim();
var rootUrl = contextPath+'/services';

var login = '/user/login';
var img = 'assets/img/';
var forgotpassword = '/RadianCX/services/user/forgotpassword'
	

// dashboard/view urls
var overallSentimentDistributionUrl = rootUrl+'/dashboard/getOverallSentimentDistribution';
var getPostDetailsSnipperwiseDataUrl = rootUrl+'/analytics/getPostDetailsSnipperwiseData';
var headlinesUrl= rootUrl+'/analytics/getHeadlines';
var durationUrl= rootUrl+'/analytics/getDurations';
var deepListUrlToSaveData = rootUrl+'/analytics/saveDeepListDataForExcel';

var topAdvocatesUrl = rootUrl+'/dashboard/getTopAdvocates';
var SnapshotUrl= rootUrl+'/dashboard/getSnapshot';
var snapshotUrlNew= rootUrl+'/dashboard/getSnapshotNew';  /*--------Updated Snapshots-----------*/
var snapshotDropdownUrl= rootUrl+'/dashboard/getSnapshotDropdown'; 
var snapshotSentimentChartUrl= rootUrl+'/dashboard/getSnapshotSentimentChartData'; 
var brandMonthlyUrl = rootUrl+'/dashboard/getBrandComparisonMonthly';
var viralityComparisonUrl = rootUrl+'/dashboard/getViralityComparison';
var getWordCloudDataUrlForDashboard = rootUrl+'/dashboard/getWordCloudData';
var entitiesForAspectDistributionForDashboard = rootUrl+'/dashboard/getEntitiesForAspect';
var sentimentDistributionForAspectForDashboard = rootUrl+'/dashboard/getSentimentDistributionForAspect';
var checkIfTicketAvailable = rootUrl+'/analytics/checkIfTicketAvailable';

var contextwiseSentimentDistributionURL = rootUrl+'/dashboard/getContextwiseSentimentDistribution';
var contextwiseSentimentDistributionPercentageURL = rootUrl+'/dashboard/getContextwiseSentimentDistributionPercentile';

//Ratings Page
// var brandMonthlyUrl = rootUrl+'/ratings/getTopratedStores';
var getTopRatedCitiesOnSourceSelectionUrl = rootUrl+'/ratings/getCitiesOnSourceSelection';
var getCompetitorComparisionCitiesOnLoadUrl = rootUrl+'/ratings/getCitiesOnSourceSelection';
var getCompetitorComparisionCitiesOnSourceSelectionUrl = rootUrl+'/ratings/getCitiesOnSourceSelection';
var getTopRatedStoresUrl=rootUrl+'/ratings/getTopratedStore';
var getTopRatedStoresAvgUrl=rootUrl+'/ratings/getTopratedStoreAvg';
var getBottomRatedStoresAvgUrl=rootUrl+'/ratings/getBottomratedStoreAvg';

var getCompetitorComparisionStoresUrl=rootUrl+'/ratings/getCompetitorComparisionStore';
var getOverAllRatingsStoresUrl=rootUrl+'/ratings/getOverAllRatings';
var getSourcewiseStoreListsUrl=rootUrl+'/ratings/getSourcewisestorelists';
var getBottomRatedCitiesOnSourceSelectionUrl = rootUrl+'/ratings/getCitiesOnSourceSelection';
var getBottomRatedStoresUrl=rootUrl+'/ratings/getBottomratedStore';
var getCityWiseSourceWiseStoreHistoryUrl=rootUrl+'/ratings/getCityWiseSourceWiseStoreHistory';
var getSignificantRatedStoresUrl=rootUrl+'/ratings/getSignificantratedStore';
var getSignificantRatedCitiesOnSourceSelectionUrl = rootUrl+'/ratings/getCitiesOnSourceSelection';
var getAvgRatedStoresUrl=rootUrl+'/ratings/getAvgRatedStore';
var tabWonershipUrl = rootUrl + '/ratings/getWonershipMetaData';

// analytics/explore urls
var getPageAccessTokenUrl = rootUrl + '/analytics/getPageAccessToken';
var filteredAnalyticsDetailsUrl = rootUrl+'/analytics/getFilterAnalyticsDetails';
var filteredAnalyticsDetailsUrlClick = rootUrl+'/analytics/getFilterAnalyticsDetailsClick';
var topFilterAdvocatesUrl = rootUrl+'/analytics/getFilterTopAdvocates';
var filterOverallSentimentDistributionUrl = rootUrl+'/analytics/getFilterOverallSentimentDistribution';
var brand24HourUrl = rootUrl+'/analytics/getBrandComparison24Hour';
var getFeedbackData = rootUrl + '/analytics/getFeedbackdata';
var getComplaintData = rootUrl + '/analytics/getComplaintData';
var getTopAdvocatesData = rootUrl+'/analytics/getTopAdvocatesData';

//var brandMonthlyUrl = rootUrl+'/analytics/getBrandComparisonMonthly';
var analyticsDetailsUrl = rootUrl+'/analytics/getAnalyticsDetails';
var filteredEntityDetailsUrl = rootUrl+'/analytics/getFilterEntityDetails';
var socialPeriodicSnapshotdataURL = rootUrl+'/analytics/getPeriodicSnapshotData';
var socialReviewSnapshotdataURL = rootUrl+'/reviewanalysis/getReviewSnapshotData';
var sentimentUpdaterUrlForReviewAnalysis = rootUrl + '/reviewanalysis/overallSentimentUpdaterForReviewAnalysis';


var sentimentInsertUpdaterUrl = rootUrl + '/common/overallSentimentUpdater';
var sentimentInsertUpdaterUrl = rootUrl + '/common/invalidDataUpdater';


var entityDetailsUrl = rootUrl+'/analytics/getEntityDetails';
var getSourcesTopicsUrl = rootUrl+'/analytics/getSourcesTopics';
var getReviewSourcesUrl = rootUrl+'/analytics/getSourcesTopics';
var getLatestCrawlingTime = rootUrl+'/ratings/getCrawlingTime';
var getSourcesForRatingTabUrl = rootUrl+'/ratings/getSourcesForRating';
var getStoreListInExcelFormat = rootUrl+'/ratings/getStoreListInExcelFormat';
var getSourcesTopicsUrlwithCities = rootUrl+'/common/getSourcesTopicsCities';
var getWordCloudDataUrl = rootUrl+'/analytics/getWordCloudData';
var getEntitiesForSelectedContextUrl = rootUrl+'/analytics/getEntitiesForSelectedContext';
var getEntitiesForSelectedContextUrlClick = rootUrl+'/analytics/getEntitiesForSelectedContextClick';
var getFilteredWordCloudDataUrl = rootUrl+'/analytics/getFilteredWordCloudData';
var getAnalyticsOnLoadDataUrl = rootUrl+'/analytics/getAnalyticsDataOnLoad';
var getWordCloudOnLoadDataUrl = rootUrl+'/analytics/getWordCloudDataOnLoad';
//analytics/explore urls
var contextDetailsUrl = rootUrl+'/analytics/getContextDetails';
var socialSnapshotdata = rootUrl + '/analytics/getSnapshotData';
var tabDataUrl = rootUrl + '/analytics/getTabMetaData';
var kpiDataUrl = rootUrl + '/analytics/getKpiMetaData';


var deepListeningUrl = rootUrl+'/analytics/getDeepListening';
var deepListeningUrlFilter = rootUrl+'/analytics/getDeepListeningfilter';
var deepListeningUrlForDeepListTab = rootUrl+'/analytics/getDataForDeepListTab';
var getPostTweetWiseEngagementScoreUrl = rootUrl+'/analytics/getPostTweetWiseEngagementScore';

//Brand Page
var getSummaryMatricsUrl = rootUrl+'/brand/getSummaryMatrics';
var getCompetitiveComparisonUrl = rootUrl+'/brand/getCompetitiveComparison';
var getOwnSocialPostUrl = rootUrl+'/brand/getOwnSocialPostTweet';
var getOwnSocialTweetUrl = rootUrl+'/brand/getOwnSocialPostTweet';
var getUserSocialPostUrl = rootUrl+'/brand/getUserSocialPostTweet';
var getUserSocialTweetUrl = rootUrl+'/brand/getUserSocialPostTweet';
var getWordcloudUrl = rootUrl+'/brand/getBrandsWordcloud';
var getBrandTimelineUrl = rootUrl+'/brand/getBrandTimeline';

var getSummaryOwnSocialPostTweet = rootUrl+'/brand/getSummaryOwnSocialPostTweet';
var getDMUserList = rootUrl+'/brand/getDMUserList';
var getSummaryDetailsPostTweet = rootUrl+'/brand/getSummaryDetailsPostTweet';
var getDMDetailedChain = rootUrl+'/brand/getDMDetailedChain';
var getSummaryComments = rootUrl+'/brand/getSummaryComments';
var getSummaryReplies = rootUrl+'/brand/getSummaryreplies';
var getSummaryOverallSentimentChart = rootUrl+'/brand/getSummarysentimentChart';
var getSummaryOverallSentimentDetails = rootUrl+'/brand/getSummarysentimentDetails';
var getSummaryOverallDataCount = rootUrl+'/brand/getSummaryOverallDataCount';
var getSummaryOverallSentimentHourlyChart = rootUrl+'/brand/getSummarysentimentHourlyChart';
var getSummaryTopNegativeAspectChart = rootUrl+'/brand/getSummaryTopNegativeAspectChart';
var getTopNegativeAspectDeatilsModal = rootUrl+'/brand/getTopNegativeAspectDeatilsModal';
// ticket management/act urls
var createTicketUrl = rootUrl+'/ticket/createTicket';
var ticketDetailsUrl = rootUrl+'/ticket/getTicketDetails';
var manageTicketUrl = rootUrl+'/ticket/manageTicket';
var userResponse = rootUrl+'/ticket/sendUserFeedBackForTwitter';

//New ticket management page
var getTicketListUrl = rootUrl+'/ticket/getTicketList';
var getTicketListRoutParamUrl = rootUrl+'/ticket/getTicketListRoutParam';
var getTicketDetailsUrl = rootUrl+'/ticket/getTicketDetails';

// user details
var getUserTimezoneOffsetUrl = rootUrl + '/dashboard/getTimezoneOffset';
var getUserDetailsUrl = rootUrl + '/user/fetchUserInfo';
var resetPasswordUrl = rootUrl + '/user/updatepassword';
var testUrl = rootUrl+'/dashboard/getObjectFromJson';
var filterOverallSentimentDistributionUrlForDashboard = rootUrl+'/dashboard/getOverallSentimentDistributionData';
var aspectWiseDistributionForDashboard = rootUrl+'/dashboard/getAspectWiseDistributionForForDashboard';
var sourceAspectWiseSentimentDistributionForDashboard = rootUrl+'/dashboard/getSourceAspectWiseSentimentDistribution';
var sourceAspectWiseSentimentTopEntityForDashboard = rootUrl+'/dashboard/getSourceAspectWiseSentimenTopEntity';
var sourceAspectWiseSentimentTopFeedbackForDashboard = rootUrl+'/dashboard/getSourceAspectWiseSentimenTopFeedback';
var getOverallSentimentDistributionDataDayWiseForDashboard = rootUrl+ '/dashboard/getOverallSentimentDistributionDataDayWise';

// add info urls
var addInfoUrl = rootUrl+'/user/adduser';

// Analyze page urls
var socialKPITweeterHeadUrl = rootUrl + '/kpi/getTwitterKPI';

var socialKPIFbHeadUrl = rootUrl + '/kpi/getFacebookKPI';
var followerFanGrowthRateChartUrl = rootUrl + '/kpi/getFollowerFanGrowthRate';
var topEngagingTweetsUrl = rootUrl + '/kpi/getTopEngagingTweets';
var topEngagingPostsUrl = rootUrl + '/kpi/getTopEngagingPosts';
var topEngagingTweetPostDistributionMonthlyUrl = rootUrl + '/kpi/getTopEngagingTweetsPostsDistributionMonthly';
var topEngagingTweetPostDistributionDailyUrl = rootUrl + '/kpi/getTopEngagingTweetsPostsDistributionDaily';
var perTweetPostInteractionUrl = rootUrl + '/kpi/getPerTweetPostInteraction';


var getCompaniesUrl= rootUrl+'/analytics/getCompanies';

//for ReviewAnalytics Service
var kpinameUrl = rootUrl + '/reviewanalysis/getKpiMetaData';
var reviewAnalysisDataUrl = rootUrl+'/reviewanalysis/getReviewAnalysisData';
var reviewAnalysisSourcesUrl = rootUrl+'/reviewanalysis/getReviewAnalysisSources';
var reviewAnalysisSentimentUrl= rootUrl+'/reviewanalysis/getReviewAnalysisSentiments';
var cityagainstsourceURL=rootUrl+'/reviewanalysis/getCitiesAgainstSource';
var localityagainstsourceURL=rootUrl+'/reviewanalysis/getLocalitiesAgainstSource';
var localityagainstsourcecityURL=rootUrl+'/reviewanalysis/getLocalitiesAgainstSourceCity';
var getPostDetailsSnipperwiseDataUrlForReviewAnalysis = rootUrl+'/reviewanalysis/getPostDetailsSnippetwiseDataForReviewAnalysis';
var getFeedbackDataForReviewAnalysis = rootUrl + '/reviewanalysis/getFeedbackdataForReviewAnalysis';
var getComplaintDataForReviewAnalysis = rootUrl + '/reviewanalysis/getComplaintDataForReviewAnalysis';
var getOwnershipsUrl=rootUrl+'/reviewanalysis/getOwnerships';


var downloadUrl=rootUrl