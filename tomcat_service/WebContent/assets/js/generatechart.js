/* ========================= Dashboard/View charts start ============================ */
//bar chart for top advocates
function chart_top_advocates(data){
	 var categoryDataArray = [];
	 var dataArray = [];
	 for(var i=0; i<data.length; i++){
		 categoryDataArray.push(data[i][0]);
		 dataArray.push(data[i][1]);
	 }
	 var chartOptions = {
   		 colors: ['#7cb5ec'],
	         chart: {
	             type: 'bar',
	             marginTop: 30
	         },
	         title: {
	             text: ''
	         },
	         subtitle: {
	             text: ''
	         },
	         xAxis: {
	             categories: categoryDataArray,
	             title: {
	                 text: null
	             }
	         },
	         yAxis: {
	             min: 0,
	             title: {
	                 text: '',
	                 align: 'high'
	             },
	             labels: {
	                 overflow: 'justify'
	             }
	         },
	         tooltip: {
	             valueSuffix: ''
	         },
	         plotOptions: {
	        	 bar: {
	                 dataLabels: {
	                     enabled: true
	                 }
	             }
	            /*, series: {
	                 cursor: 'pointer',
	                 point: {
	                     events: {
	                         click: function () {
	                        	 var analyticsScopeForTopAdvocates = angular.element($("#analytics")).scope();
	                        	 topAdvocatesObj = this.category;
	                        	 analyticsScopeForTopAdvocates.$parent.fn_showPostsForTopAdvocates(topAdvocatesObj);
	                             //alert('Category: ' + this.category + ', value: ' + this.y);
	                         }
	                     }
	                 }
	             }*/
	         },
	         tooltip: {
		  	      formatter: function() {
		  	    	var s = this.x;
	                s += ':<b> ' + this.y + '</b>';
	                return s;
		  	      },
		  	    events: {
		  	    	
		  	    click: function(e) {
		  			 }
		  	      }
		  	 },
	         credits: {
	             enabled: false
	         },
	         series: [{
	        	 showInLegend: false,
	             data: dataArray
	         }]
   	  };    
   
	  var chart = $('#topAdvocatesBarChart').highcharts(chartOptions)
}

// pie chart for overall sentiment for context
function chart_overall_sentiment(results,selectedSentimentalStartDuration,selectedSentimentalEndtDuration) {
	//var results = {"results":{"rows":[[20,25,44],[30,523,88]]}};
    var positiveSentiment = [];
    var negativeSentiment = [];
    var neutralSentiment = [];
    var xCategories = [];
    for (var i = 0; i < results.rows.length; i++) {
    	var row = results.rows[i];
       
    	positiveSentiment.push(row[1]);
    	negativeSentiment.push(row[2]);
    	neutralSentiment.push(row[3]);
    }
    
    xCategories.push(selectedSentimentalStartDuration);
    xCategories.push(selectedSentimentalEndtDuration);
    var sentimentSeriesData = [{
        name: 'Positive',
        data: positiveSentiment
    }, {
        name: 'Negative',
        data: negativeSentiment
    },{
        name: 'Neutral',
        data: neutralSentiment
    }];
    $('#sentimentalSourceChart').highcharts({
        colors: ['green', 'red', 'orange'],
        chart: {
            type: 'column',
            //width: $('#sentimentalSourceChart').parents('.tab-content').eq(0).innerWidth() * 0.95,
            marginTop: 50
        },
        xAxis: {
           /* min: 0,
            max: i <= 24 ? i - 1 : 24,*/
            categories: xCategories,
            title: {
                //text: "Comparision chat"
            	text: ""
            }
        },
        scrollbar: {
            barBackgroundColor: 'gray',
            barBorderRadius: 7,
            barBorderWidth: 0,
            buttonBackgroundColor: 'gray',
            buttonBorderWidth: 0,
            buttonBorderRadius: 7,
            trackBackgroundColor: 'none',
            trackBorderWidth: 1,
            trackBorderRadius: 8,
            trackBorderColor: '#CCC'
        },
        credits: {
            enabled: false,
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Sentiment'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        tooltip: {
            shared: true
        },
        plotOptions: {
            column: {
                //stacking: 'normal',
                dataLabels: {
                    enabled: false
                }
            },
            series: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        title: {
            text: ''
        },
        series: sentimentSeriesData,
        lang: {
            noData: "No Data Found"
        }
    });
    Highcharts.setOptions({
        lang: {
            noData: "No Data Found"
        }
    });
}

function chart_generate_hourly_sentiment_chart(results) {
	//var results = {"results":{"rows":[[20,25,44],[30,523,88]]}};
    var positiveSentiment = [];
    var negativeSentiment = [];
    var neutralSentiment = [];
    var xCategories = [];
    for (var i = 0; i < results.rows.length; i++) {
    	var row = results.rows[i];
    	var positiveCount =row[2];
    	var negativeCount =row[4];
    	var neutralCount =row[3];
    	
    	if(positiveCount == null || positiveCount == 'null'){
    		positiveCount = 0;
    	}
    	if(negativeCount == null || negativeCount == 'null'){
    		negativeCount = 0;
    	}
    	if(neutralCount == null || neutralCount == 'null'){
    		neutralCount = 0;
    	}
    	
    	xCategories.push(row[1])
    	positiveSentiment.push(positiveCount);
    	negativeSentiment.push(negativeCount);
    	neutralSentiment.push(neutralCount);
    }
    
    //xCategories.push(selectedSentimentalStartDuration);
    //xCategories.push(selectedSentimentalEndtDuration);
    var sentimentSeriesData = [{
        name: 'Positive',
        data: positiveSentiment
    }, {
        name: 'Negative',
        data: negativeSentiment
    },{
        name: 'Neutral',
        data: neutralSentiment
    }];
    $('#hourlySentiment').highcharts({
        colors: ['green', 'red', 'orange'],
        chart: {
            type: 'column',
            //width: $('#sentimentalSourceChart').parents('.tab-content').eq(0).innerWidth() * 0.95,
            marginTop: 50
        },
        xAxis: {
           /* min: 0,
            max: i <= 24 ? i - 1 : 24,*/
            categories: xCategories,
            title: {
                //text: "Comparision chat"
            	text: ""
            }
        },
        scrollbar: {
            barBackgroundColor: 'gray',
            barBorderRadius: 7,
            barBorderWidth: 0,
            buttonBackgroundColor: 'gray',
            buttonBorderWidth: 0,
            buttonBorderRadius: 7,
            trackBackgroundColor: 'none',
            trackBorderWidth: 1,
            trackBorderRadius: 8,
            trackBorderColor: '#CCC'
        },
        credits: {
            enabled: false,
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Sentiment'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        tooltip: {
            shared: true
        },
        exporting:{
        	enabled:false
        },
        plotOptions: {
            column: {
                //stacking: 'normal',
                dataLabels: {
                    enabled: false
                }
            },
            series: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        title: {
            text: ''
        },
        series: sentimentSeriesData,
        lang: {
            noData: "No Data Found"
        }
    });
    Highcharts.setOptions({
        lang: {
            noData: "No Data Found"
        }
    });
}

function chart_generate_top_negative_aspect_chart(results) {
    //var positiveSentiment = [];
    var negativeSentiment = [];
    //var neutralSentiment = [];
    var xCategories = [];
    for (var i = 0; i < results.rows.length; i++) {
    	var row = results.rows[i];
    	var aspectName = row[3];
    	var aspectCount = row[4];
    	if(aspectName == null || aspectName == 'null'){
    		aspectName = 'N/A';
    		aspectCount = 0;
    	}
    	xCategories.push(row[0] + '<br/>' + aspectName)
    	//positiveSentiment.push(row[2]);
    	negativeSentiment.push(aspectCount);
    	//neutralSentiment.push(row[3]);
    }
    //xCategories.push(selectedSentimentalStartDuration);
    //xCategories.push(selectedSentimentalEndtDuration);
    var sentimentSeriesData = [{
        name: 'Negative',
        data: negativeSentiment
    }];
    $('#topNegativeAspect').highcharts({
        colors: ['red'],
        chart: {
            type: 'column',
            //width: $('#sentimentalSourceChart').parents('.tab-content').eq(0).innerWidth() * 0.95,
            marginTop: 50
        },
        xAxis: {
           /* min: 0,
            max: i <= 24 ? i - 1 : 24,*/
            categories: xCategories,
            title: {
                //text: "Comparision chat"
            	text: ""
            }
        },
        scrollbar: {
            barBackgroundColor: 'gray',
            barBorderRadius: 7,
            barBorderWidth: 0,
            buttonBackgroundColor: 'gray',
            buttonBorderWidth: 0,
            buttonBorderRadius: 7,
            trackBackgroundColor: 'none',
            trackBorderWidth: 1,
            trackBorderRadius: 8,
            trackBorderColor: '#CCC'
        },
        credits: {
            enabled: false,
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Sentiment'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        tooltip: {
            shared: true
        },
        exporting:{
        	enabled:false
        },
        plotOptions: {
            column: {
                //stacking: 'normal',
                dataLabels: {
                    enabled: false
                }
            },
            series: {
                dataLabels: {
                    enabled: true
                },
                events: {
                    click: function(event) {
                    	var deeplistScope = angular.element($("#page-deelistining")).scope();
                    	deeplistScope.fn_showTopNegativeAspectDeatilsModal(event.point.category);
                    	
                    }
                },
                point: {
                    events: {
                        click: function () {
                        	//deeplistScope.fn_showSentimentDetailsModal(this.series.name);
                        }
                    }
                }
            }
        },
        title: {
            text: ''
        },
        series: sentimentSeriesData,
        lang: {
            noData: "No Data Found"
        }
    });
    Highcharts.setOptions({
        lang: {
            noData: "No Data Found"
        }
    });
}



function chart_daywise_overall_sentiment_line_for_dashboard(results,selectedSentimentalStartDuration,selectedSentimentalEndtDuration) {
	//var results = {"results":{"rows":[[20,25,44],[30,523,88]]}};
    var positiveSentiment = [];
    var negativeSentiment = [];
    var neutralSentiment = [];
    var xCategories = [];
    for (var i = 0; i < results.rows.length; i++) {
    	var row = results.rows[i];
    	xCategories.push(row[0]);
    	positiveSentiment.push(row[1]);
    	negativeSentiment.push(row[2]);
    	neutralSentiment.push(row[3]);
    }
    
   /* xCategories.push(selectedSentimentalStartDuration); find
    xCategories.push(selectedSentimentalEndtDuration);*/
    var sentimentSeriesData = [{
        name: 'Positive',
        data: positiveSentiment
    },{
        name: 'Neutral',
        data: neutralSentiment
    }, {
        name: 'Negative',
        data: negativeSentiment
    }];
    $('#overallSentimentLine').highcharts({
        colors: ['green', 'orange', 'red'],
        chart: {
            //type: 'column',
        	type: 'line',
            //width: $('#sentimentalSourceChart').parents('.tab-content').eq(0).innerWidth() * 0.95,
            marginTop: 50
        },
        xAxis: {
           /* min: 0,
            max: i <= 24 ? i - 1 : 24,*/
            categories: xCategories,
            title: {
                //text: "Comparision chat"
            	text: ""
            }
        },
        scrollbar: {
            barBackgroundColor: 'gray',
            barBorderRadius: 7,
            barBorderWidth: 0,
            buttonBackgroundColor: 'gray',
            buttonBorderWidth: 0,
            buttonBorderRadius: 7,
            trackBackgroundColor: 'none',
            trackBorderWidth: 1,
            trackBorderRadius: 8,
            trackBorderColor: '#CCC'
        },
        credits: {
            enabled: false,
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Sentiment'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        tooltip: {
            shared: true
        },
        exporting:{
        	enabled:false
        },
        plotOptions: {
            column: {
                //stacking: 'normal',
                dataLabels: {
                    enabled: false
                }
            },
            series: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        title: {
            text: ''
        },
        series: sentimentSeriesData,
        lang: {
            noData: "No Data Found"
        }
    });
    Highcharts.setOptions({
        lang: {
            noData: "No Data Found",
            thousandsSep: ''
        }
    });
}



//function chart_overall_sentiment_line(results,selectedSentimentalStartDuration,selectedSentimentalEndtDuration) {



function chart_overall_sentiment_line_dashboard(data){
	
	var results = data.rows;
	var sources = [];
	var positiveSentiment = [];
	var negativeSentiment = [];
	var neutralSentiment = [];
	
	for(var i=0; i<results.length; i++){
		sources.push(results[i][0]);
		positiveSentiment.push(results[i][1]);
		negativeSentiment.push(results[i][2]);
		neutralSentiment.push(results[i][3]);
	 }
	
	
	Highcharts.chart('overallSentimentLine', {

	    title: {
	        //text: 'Overall Sentiment Analysis'
	    	text: ''
	    },

	    legend: {
	        layout: 'vertical',
	        align: 'right',
	        verticalAlign: 'middle'
	    },
	    xAxis: {
	        categories: sources
	    	
	    },
	    yAxis: {
	        min: 0
	        //max: 100
	    },
	    plotOptions: {
	        series: {
	            label: {
	                connectorAllowed: false
	            }
	        }
	    },
	    credits: {
            enabled: false
        },
        exporting: {
        	enabled: false
        },
	    series: [{
	        name: 'Positive',
	        data: positiveSentiment,
	        color:'green'
	    }, {
	        name: 'Neutral',
	        data: neutralSentiment,
	        color:'orange'
	    }, {
	        name: 'Negative',
	        data: negativeSentiment,
	        color:'red'
	    }],

	    responsive: {
	        rules: [{
	            condition: {
	                maxWidth: 500
	            },
	            chartOptions: {
	                legend: {
	                    layout: 'horizontal',
	                    align: 'center',
	                    verticalAlign: 'bottom'
	                }
	            }
	        }]
	    }

	});
}



function chart_overall_sentiment_bar_dashboard(data) {
	
	console.log(data);
	
	var results = data.rows;
	var sources = [];
	var positiveSentiment = [];
	var negativeSentiment = [];
	var neutralSentiment = [];
	
	for(var i=0; i<results.length; i++){
		sources.push(results[i][0]);
		positiveSentiment.push(results[i][1]);
		negativeSentiment.push(results[i][2]);
		neutralSentiment.push(results[i][3]);
	 }
	
	Highcharts.chart('overallSentiment', {
	    chart: {
	        type: 'column'
	    },
	    title: {
	        //text: 'Overall Sentiment Analysis'
	    	text: ''
	    },
	    xAxis: {
	        //categories: ['News', 'Blogs', 'Consumer Forums', 'Other Web', 'Twitter', 'Facebook', 'Google+', 'YouTube']
	    	categories : sources
	    },
	    scrollbar: {
            barBackgroundColor: 'gray',
            barBorderRadius: 7,
            barBorderWidth: 0,
            buttonBackgroundColor: 'gray',
            buttonBorderWidth: 0,
            buttonBorderRadius: 7,
            trackBackgroundColor: 'none',
            trackBorderWidth: 1,
            trackBorderRadius: 8,
            trackBorderColor: '#CCC'
        },
	    yAxis: {
	        min: 0,
	        title: {
                text: 'Sentiment'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
	    },
	    tooltip: {
	        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
	        shared: true
	    },
	    plotOptions: {
	        column: {
	            stacking: 'normal',
	            dataLabels: {
                    enabled: false
                }
		    ,
	        series: {
	            dataLabels: {
	                enabled: true
	            }
	        }
	            //stacking: 'percent'	
	        }
	    },
	    credits: {
            enabled: false
        },
        exporting : {
        	enabled: false
        },
        lang: {
            noData: "No Data Found"
        },
        colors: ['green', 'orange', 'red'],
	    series: [{
	    	name: 'Positive',
	        data: positiveSentiment
	    },{
	        name: 'Neutral',
	        data: neutralSentiment
	    }, {
	        name: 'Negative',
	        data: negativeSentiment
	    }]
	});
}

function chart_aspect_wise_pie_dashboard(arrData){
	// Create the chart
	Highcharts.chart('aspectRatio', {
	    chart: {
	        type: 'pie'
	    },
	    title: {
	        text: ''
	    },
	    yAxis: {
	        title: {
	            text: 'Total percent Tweets'
	        }
	    },
	    plotOptions: {
	        pie: {
	            shadow: false,
	            center: ['50%', '50%'],
		        dataLabels: {
		        	enabled: true,
		            useHTML: true,
		            formatter: function () {
		            	return '<div style="position: relative; top: -15px;">' + this.point.name + '&nbsp;(' + this.y + ')' + '<br>' + Math.round(this.percentage*100)/100 + ' %' +  '</div>'
		            }
		        },
			    size: '60%'
	        }
	    },
	    /*tooltip: {
	        valueSuffix: '%'
	    },*/
	    credits: {
            enabled: false
        },
        exporting: {
        	enabled:false
        },
	    series: [{
	        name: 'Feeds',
	        data: arrData
	        
	    }],
	    
	});
}

function source_aspect_wise_sentiment_distribution_dashbaord(chartID,dataSet){
	 	var positiveSentiment = [];
	    var negativeSentiment = [];
	    var neutralSentiment = [];
	    var xCategories = [];
	    for (var i = 0; i < dataSet.length; i++) {
	    	var row = dataSet[i];
	    	xCategories.push(row[0])
	    	positiveSentiment.push(row[1]);
	    	negativeSentiment.push(row[2]);
	    	neutralSentiment.push(row[3]);
	    }
	    /*xCategories.push(selectedSentimentalStartDuration);
	    xCategories.push(selectedSentimentalEndtDuration);*/
	    var sentimentSeriesData = [{
	        name: 'Positive',
	        data: positiveSentiment
	    },{
	        name: 'Neutral',
	        data: neutralSentiment
	    }, {
	        name: 'Negative',
	        data: negativeSentiment
	    }];
	    $('#'+chartID).highcharts({
	        colors: ['green', 'orange', 'red'],
	        chart: {
	            type: 'column',
	            //width: $('#sentimentalSourceChart').parents('.tab-content').eq(0).innerWidth() * 0.95,
	            marginTop: 50
	        },
	        xAxis: {
	           /* min: 0,
	            max: i <= 24 ? i - 1 : 24,*/
	            categories: xCategories,
	            title: {
	                //text: "Comparision chat"
	            	text: ""
	            }
	        },
	        scrollbar: {
	            barBackgroundColor: 'gray',
	            barBorderRadius: 7,
	            barBorderWidth: 0,
	            buttonBackgroundColor: 'gray',
	            buttonBorderWidth: 0,
	            buttonBorderRadius: 7,
	            trackBackgroundColor: 'none',
	            trackBorderWidth: 1,
	            trackBorderRadius: 8,
	            trackBorderColor: '#CCC'
	        },
	        credits: {
	            enabled: false,
	        },
	        exporting: {
	        	enabled:false
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: 'Sentiment'
	            },
	            stackLabels: {
	                enabled: true,
	                style: {
	                    fontWeight: 'bold',
	                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
	                }
	            }
	        },
	        tooltip: {
	            shared: true
	        },
	        plotOptions: {
	            column: {
	                stacking: 'normal',
	                dataLabels: {
	                    enabled: false
	                }
	            },
	            series: {
	                dataLabels: {
	                    enabled: true
	                }
	            }
	        },
	        title: {
	            text: ''
	        },
	        credits: {
	             enabled: false
	         },
	        series: sentimentSeriesData,
	        lang: {
	            noData: "No Data Found"
	        }
	    });
	    Highcharts.setOptions({
	        lang: {
	            noData: "No Data Found"
	        }
	    });

   
}


function aspectDistributionPieChart() {
	Highcharts.chart('aspectDistributionPieChart', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Aspect percentage distribution'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            }
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [{
            name: 'CLEANLINESS',
            y: 16.41,
        }, {
            name: 'ACCURACY',
            y: 10.84
        }, {
            name: 'HOSPITALITY',
            y: 6.85
        }, {
            name: 'MAINTENANCE',
            y: 16.67
        }, {
            name: 'PRODUCT QUALITY',
            y: 11.18
        }, {
            name: 'SPEED & DELIVERY',
            y: 20.64
        }, {
            name: 'PRICES AND OFFERS',
            y: 5.6
        }, {
            name: 'OTHERS',
            y: 5.2
        }, {
            name: 'NON-RELEVANT',
            y: 10.61
        }]
    }]
});
}



/*function chart_overall_sentiment(values){
	var data = [];
	var pW = $('.graph-ar').width();
	if(values.length){
		data = [['Positive', values[0]], ['Negative', values[1]], ['Neutral', values[2]]];
	}
	$('#sentimentalSourceChart').highcharts({
		colors: ['#8dc153', '#f55348', '#f3ba49'],	
		chart: {
			backgroundColor: '#f1f1f1',
			type: 'pie',
			width: pW,
			height: 300,
			options3d: {
				enabled: true,
				alpha: 45
			}
		},
		title: {
			text: null
		},
		credits: {
			enabled: false
		},
		plotOptions: {
			pie: {
				innerSize: 85,
				depth: 45,
				cursor: '',
				dataLabels: {
					enabled: false
				},
			}
		},
		tooltip: {
	  	      formatter: function() {
	  	    	var s = this.point.name;
	  	    	s += ':<b> ' + this.point.y + '%</b>';
	  	    	return s;
	  	      }
	  	},
		series: [{
			name: '\u00a0',
			data: data
		}],
		exporting: {
            chartOptions: {
            	chart: {
    				backgroundColor: '#f1f1f1',
    				type: 'pie',
    				width: 400,
    				height: 400	    				
    			},
                title: {
                    text: 'Overall sentiment destribution',
                    //y: 7,
                    style: {
                        color: '#798693',
                        fontSize: '13px'
                    }
                },
                plotOptions: {
    				pie: {
    					innerSize: 150,
    					dataLabels: {
    						enabled: true
    					},
    				}
    			},
            },
            filename: 'Overall sentiment destribution'
  	     }
	})
}
*/
//function fn_brand_comparission_24Hour(acct_Id, currentTime, selectedInterval){
function chart_brand_comparission_24Hour(results, selectedInterval) {
    var categoryDataArray = new Array();
    var values = results.rows.length;
    /*----Setting x-axis tick interval----*/
    var $tickInterval = 1;
    if (selectedInterval == 0) {
        $tickInterval = 3;
    }
    /*------------------------------------*/
    var numberOfCompanies = 1;
    var dataObjList = new Array();
    for (var i = 0; i < values; i++) {
        if (selectedInterval == 0) {
            results.rows[i][2] = toLocalDate(results.rows[i][2]);
            //categoryDataArray.push(results.rows[i][2].substring(0,10)+':'+results.rows[i][2].substring(11,results.rows[i][2].length));
        }
        categoryDataArray.push(results.rows[i][2]);
    }
    for (var x = 0; x < numberOfCompanies; x++) {
        var dataObj = {
            companyName: null,
            brandScore: []
        };
        for (var i = values * x; i < values * (x + 1); i++) {
            dataObj.brandScore.push(results.rows[i][5]);
        }
        dataObjList.push(dataObj);
    }

    // refreshing the selected value Select Interval selectbox
    $('#setRange').val(selectedInterval);
    $('#setRange').selectpicker('refresh');

    var chartOptions = {
        chart: {
            type: 'areaspline',
            zoomType: 'x',
            marginTop: 50
        },
        title: {
            text: '\u00a0'
        },
        xAxis: {
            categories: categoryDataArray,
            tickInterval: $tickInterval, /*---Applied tick interval---*/
            labels: {
                formatter: function() {
                    var text = this.value;
                    var idx = text.indexOf(' ');
                    return idx > 0 ? '<div class="js-ellipse" style="width: 70px; text-align: center;" title="' + text + '"><span  title="' + text + '">' + text.substr(0, idx) + '</span><br/><span  title="' + text + '">' + text.substr(idx) + '</span></div>' : text;
                },
                style: {
                	fontSize: '10px'
                }
            }
        },
        yAxis: {
            title: {
                text: ' '
            },
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            areaspline: {
                fillOpacity: 0.2,
                marker: {
                    enabled: false
                }
            },
            series: {
                cursor: 'pointer',
                events: {
                    click: function(event) {
                        /**
                         * for virality function call:
                         * if last point of HAPPINESS INDEX graph is chosen, end date becomes equal to current time,
                         * if not, then end date becomes:
                         * 			for Daily: 59th minute of chosen hour,
                         * 			for Weekly: time 23:59 of chosen date
                         */ 
                    	var chosenPoint = event.point.category;
                        var isLastPoint = (dataObjList[0].brandScore.length - 1) == event.point.index;
                        var convertedStartDate, convertedEndDate;
                       
                        if(isLastPoint){
                        	/*var date = new Date();
                			date = new Date(date.valueOf() + (date.getTimezoneOffset() + authTimezoneOffset) * 60000);
							var currentHour = date.getHours();
							var currentMinute = date.getMinutes();
                    		convertedEndDate = ('0' + date.getDate()).slice(-2) + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + date.getFullYear() + ' '+ currentHour + ':' + currentMinute;*/
                        	var date = new Date();
                			date = new Date(date.valueOf() + (date.getTimezoneOffset() + authTimezoneOffset) * 60000);
							var currentHour = date.getHours();
							var currentMinute = date.getMinutes();
                    		convertedEndDate = ('0' + date.getDate()).slice(-2) + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + date.getFullYear() + ' '+ currentHour + ':' + currentMinute;
						}else{
							/*if (selectedInterval == 0) {
								convertedEndDate = chosenPoint.substring(0,chosenPoint.length-2) + "59";
							}else{
								convertedEndDate = chosenPoint + ' 23:59';
							}*/
							if (selectedInterval == 0) {
								convertedEndDate = chosenPoint;//chosenPoint.substring(0,chosenPoint.length-2) + "00";
							}else{
								convertedEndDate = chosenPoint + ' 23:59';
							}
						}
                        /**
                         * for virality function call:
                         * start date becomes:
                         * 			for Daily: equal to end date
                         * 			for Weekly: 00:00 of chosen date
                         */
                        if (selectedInterval == 0) {
                        	//convertedStartDate = convertedEndDate;
                        	var getSubStringPart = chosenPoint.substr(11,2);
                        	var newStartDateSubString = '00';
                        	var newStartDateString = '00';
                        	if(getSubStringPart == '00'){
                        		newStartDateSubString = '23';
                        		var myval = chosenPoint.substr(0,2);
                        		var cVal = parseInt(myval)-1;
                        		chosenPoint = chosenPoint.replace(myval+'-',cVal+'-');
                        	} else if(getSubStringPart == '01'){
                        		newStartDateSubString = '00';
                        	} else {
                        		getSubStringPart = parseInt(getSubStringPart);
                        		newStartDateSubString = getSubStringPart - 1;
                        	}
                        	if(newStartDateSubString.toString().length == 1){
                        		newStartDateSubString = '0'+newStartDateSubString;
                        		getSubStringPart = '0'+getSubStringPart;
                        	}

                        	newStartDateString = chosenPoint.replace(' '+getSubStringPart,' '+newStartDateSubString);
                        	convertedStartDate = newStartDateString;//chosenPoint.substring(0,chosenPoint.length-2) + "00";//convertedEndDate;
						}else{
							convertedStartDate = chosenPoint + ' 00:00';
						}

                        //viralityComparissionObj.accountId = authId;
                        //viralityComparissionObj.acctId = authId;
                        //viralityComparissionObj.hR = -1;
                        //viralityComparissionObj.startDt = toGMTDate(convertedStartDate);
                        //viralityComparissionObj.endDt = toGMTDate(convertedEndDate);
                        //viralityComparissionObj.selectedInterval = selectedInterval;
                        //alert(authId +"--"+ toGMTDate(convertedStartDate) +"--"+toGMTDate(convertedEndDate) +"--"+ selectedInterval)
                        
                        var analyticsScope = angular.element($("#analytics")).scope();
                        
                        entityDetailsObj.rowSourceId = -1;
        				entityDetailsObj.rowSourceTopic = analyticsScope.topicIDFromRout;
        				entityDetailsObj.timeInterval = 1;
        				entityDetailsObj.selectedInterval = selectedInterval;
        				entityDetailsObj.startDt = toGMTDate(convertedStartDate);
        				entityDetailsObj.endDt = toGMTDate(convertedEndDate);
        				
        				analyticsScope.$parent.analyticsFrom=convertedStartDate;
            			analyticsScope.$parent.analyticsTo=convertedEndDate;
            			
        				analyticsScope.fn_source_entity_details(entityDetailsObj);
            			

                        $('html, body, #topFixedArea').animate({
                            scrollTop: 900
                        }, 1000);
                    }
                }
            }
        },
        tooltip: {
            formatter: function() {
                var s = this.x;
                s += '<br/><b>Score: ' + this.y + '</b>';
                return s;
            }
        },
        series: [{
            showInLegend: false,
            //name: dataObjList[0].companyName,
            data: dataObjList[0].brandScore
        }],
        exporting: {
            chartOptions: {
                title: {
                    text: 'Sentiment index live',
                    y: 7,
                    style: {
                        color: '#798693',
                        fontSize: '13px'
                    }
                },
                subtitle: {
                    //text: 'Interval: ' + (selectedInterval == 0 ? 'Hourly' : 'Daily') + '.',
                    y: 25,
                    style: {
                        color: '#aaa',
                        fontSize: '10px'
                    }
                }
            },
            filename: 'Happiness index live'
        },
        lang: {
            noData: "No Data Found"
        }
    };
    var chart = $('#chart1').highcharts(chartOptions);
}


//bar chart for brand comparisson monthly
function chart_brand_comparission_monthly(results, selectedContext) {
    var brandComparisonDataMap = {};
    var categoryDataArray = new Array();
    var companyNamesArray = new Array();
    var companyIdCompNameMap = {};
    //alert(results)
    var numberOfCompanies = Math.round(results.length / 10);
    for (var x = 0; x < numberOfCompanies; x++) {
        var companyName = null;
        var accountId = null;
        var tempBrandScoreDataArray = new Array();
        var dataCount = results.length / numberOfCompanies;
        var length = dataCount * (x + 1);
        for (var i = dataCount * (x + 1) - 1; i >= dataCount * x; i--) {
            if (categoryDataArray.indexOf(results[i][2]) == -1) {
                categoryDataArray.push(results[i][2]);
            }
            companyName = results[i][1];
            companyId = results[i][0];
            tempBrandScoreDataArray.push(results[i][3]);
        }
        companyNamesArray.push(companyName);
        brandComparisonDataMap[companyName] = tempBrandScoreDataArray;
        companyIdCompNameMap[companyName] = companyId;
    }
    var dataObjList = new Array();
    for (var j = 0; j < companyNamesArray.length; j++) {
        var dataObj = {};
        dataObj.name = companyNamesArray[j];
        dataObj.data = brandComparisonDataMap[companyNamesArray[j]];
        dataObjList.push(dataObj);
    }

    var chartOptions = {
        chart: {
            type: 'spline',
            zoomType: 'x',
            marginTop: 50
        },
        //colors: ['#C0C0C0', '#000000', '#808080'],
        title: {
            text: '\u00a0'
        },
        xAxis: {
            type: 'datetime',
            categories: categoryDataArray,
            labels:{
            	style: {
                	fontSize: '10px'
                },
                rotation: -45        		
            }
        },
        yAxis: {
            title: {
                text: ''
            },
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            spline: {
                marker: {
                    enabled: false
                }
            }
        },
        legend: {
            labelFormatter: function() {
                return '<span style="color:' + this.color + ';">' + this.name + '</span>';
            },
            itemHiddenStyle: {
                color: '#ddd'
            }
        },

        series: dataObjList,
        exporting: {
            chartOptions: {
                title: {
                    text: 'Competitive sentiment index',
                    y: 7,
                    style: {
                        color: '#798693',
                        fontSize: '13px'
                    }
                },
                subtitle: {
                    //text: 'Context: ' + selectedContext + '.',
                    y: 25,
                    style: {
                        color: '#aaa',
                        fontSize: '10px'
                    }
                }
            },
            filename: 'Competitive happiness index'
        },
        lang: {
            noData: "No Data Found"
        }
    };
    var chart = $('#chart2').highcharts(chartOptions);
}

function chart_contextwise_sentiment_distribution(results, key, sentiment) {
    var sentimentCountComparisonDataMap = {};
    var categoryDataArray = new Array();
    var companyNamesArray = new Array();
    for (var i = 0; i < results.length; i++) {
        if (companyNamesArray.indexOf(results[i][1]) == -1) {
            companyNamesArray.push(results[i][1]);
        }
    }
    var numberOfCompanies = companyNamesArray.length;
    for (var x = 0; x < numberOfCompanies; x++) {
        var companyName = null;
        var accountId = null;
        var sentimentCountDataArray = new Array();
        var dataCount = results.length / numberOfCompanies;
        var length = dataCount * (x + 1);
        for (var i = dataCount * (x + 1) - 1; i >= dataCount * x; i--) {
            if (categoryDataArray.indexOf(results[i][2]) == -1) {
                categoryDataArray.push(results[i][2]);
            }
            companyName = results[i][1];
            sentimentCountDataArray.push(results[i][3]);
        }
        sentimentCountComparisonDataMap[companyName] = sentimentCountDataArray;
    }
    var dataObjList = new Array();
    for (var j = 0; j < companyNamesArray.length; j++) {
        var dataObj = {};
        dataObj.name = companyNamesArray[j];
        dataObj.data = sentimentCountComparisonDataMap[companyNamesArray[j]];
        dataObjList.push(dataObj);
    }
    if (sentiment == 'Negative')
        var colors = ['#f55348', '#CC0000','#FF0000','#FF3300','#FF6600'];
    else if (sentiment == 'Positive')
        var colors = ['#66CC00	', '#669900','#66FF00','#99CC00','#999900'];
    else if (sentiment == 'Neutral')
        var colors = ['#cccccc', '#888888'];

    var chartOptions = {
        colors: colors,
        chart: {
            type: 'bar',
            marginTop: 20
        },
        title: {
            text: '\u00a0'
        },
        xAxis: {
            categories: categoryDataArray
        },
        yAxis: {
            gridLineWidth: 0,
            title: {
                text: ''
            },
        },
        credits: {
            enabled: false
        },
        series: dataObjList,
        exporting: {
            chartOptions: {
                title: {
                    text: 'Monthly contextwise sentiment by ' + (key == 'value' ? 'count' : '%'),
                    y: 7,
                    style: {
                        color: '#798693',
                        fontSize: '13px'
                    }
                },
                subtitle: {
                    //text: 'Sentiment: ' + sentiment + '.',
                    y: 20,
                    style: {
                        color: '#aaa',
                        fontSize: '10px'
                    }
                },
                chart: {
                    marginTop: 50
                }
            },
            filename: 'Monthly contextwise sentiment'
        },
        lang: {
            noData: "No Data Found"
        }
    };
    if (key == 'value') {
        var chart = $('#contextWiseChart').highcharts(chartOptions);
        Highcharts.setOptions({
            lang: {
                noData: "No Data Found",
                decimalPoint: '.',
                thousandsSep: ','
            }
        });
    } else if (key == 'percentage') {
        var chart = $('#contextWisePercentChart').highcharts(chartOptions);
        Highcharts.setOptions({
            lang: {
                noData: "No Data Found",
                decimalPoint: '.',
                thousandsSep: ','
            }
        });
    }
}


/* ========================= Dashboard/View charts end ============================ */

/* ========================= Analytics/Explore charts start ============================ */

function chart_sentiment_analysis(results) {
    var positiveSentiment = [];
    var negativeSentiment = [];
    var neutralSentiment = [];
    var datetime = [];
    for (var i = 0; i < results.rows.length; i++) {
        positiveSentiment.push(results.rows[i][3]);
        negativeSentiment.push(results.rows[i][4]);
        neutralSentiment.push(results.rows[i][5]);
        //datetime.push(toLocalDate(results.rows[i][0]+' '+('0'+results.rows[i][1]).slice(-2)+':00'));
        datetime.push(toLocalDate(results.rows[i][0] + ' ' + results.rows[i][1]));
    }
    var data2 = [{
        name: 'Positive',
        data: positiveSentiment
    }, {
        name: 'Neutral',
        data: neutralSentiment
    }, {
        name: 'Negative',
        data: negativeSentiment
    }];
    $('#Sentiment').highcharts({
        colors: ['green', 'orange', 'red'],
        chart: {
            type: 'column',
            width: $('#Sentiment').parents('.tab-content').eq(0).innerWidth() * 0.95,
            marginTop: 50
        },
        xAxis: {
            min: 0,
            max: i <= 24 ? i - 1 : 24,
            categories: datetime
        },
        scrollbar: {
            enabled: i <= 25 ? false : true,
            barBackgroundColor: 'gray',
            barBorderRadius: 7,
            barBorderWidth: 0,
            buttonBackgroundColor: 'gray',
            buttonBorderWidth: 0,
            buttonBorderRadius: 7,
            trackBackgroundColor: 'none',
            trackBorderWidth: 1,
            trackBorderRadius: 8,
            trackBorderColor: '#CCC'
        },
        credits: {
            enabled: false,
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Sentiment'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        tooltip: {
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: false
                }
            },
            series: {
                dataLabels: {
                    enabled: false
                }
            }
        },
        title: {
            text: ''
        },
        series: data2,
        lang: {
            noData: "No Data Found"
        }
    });
    Highcharts.setOptions({
        lang: {
            noData: "No Data Found"
        }
    });
}

/* ========================= Analytics/Explore charts end ============================ */


/* ========================= Analyze charts start ============================ */

function chart_growthrate(data, container) {

    var chartOptions = {
        chart: {
            type: 'spline',
            zoomType: 'x',
            marginTop: 50
        },
        title: {
            text: '\u00a0'
        },
        xAxis: {
            categories: data.categories,
            labels: {
                formatter: function() {
                    var text = this.value;
                    var idx = text.indexOf('(');
                    return idx > 0 ? '<div class="js-ellipse" style="width: 100px;" title="' + text + '"><span  title="' + text.substr(0, (idx - 1)) + '">' + text.substr(0, (idx - 1)) + '</span><br/><span  title="' + text.substr(idx) + '">' + text.substr(idx) + '</span></div>' : text;
                }
            }
        },
        yAxis: {
            title: {
                text: ' '
            },
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            areaspline: {
                fillOpacity: 0.2,
                marker: {
                    enabled: false
                }
            }
        },
        tooltip: {
            formatter: function() {
                var s = this.x;
                s += '<br/><b>' + (this.y).toFixed(2) + '</b>';
                return s;
            }
        },
        exporting: {
            chartOptions: {
                title: {
                    text: data.title,
                    y: 7,
                    style: {
                        color: '#798693',
                        fontSize: '13px'
                    }
                },
                subtitle: {
                    //text: 'Interval: ' + data.selectedInterval + '.',
                    y: 25,
                    style: {
                        color: '#aaa',
                        fontSize: '10px'
                    }
                }
            },
            filename: data.title
        },
        series: [{
            showInLegend: false,
            //name: dataObjList[0].companyName,
            data: data.series
        }],
        lang: {
            noData: "No Data Found"
        }
    };
    var chart = $(container).highcharts(chartOptions);
    //var chart = $('#fb_analyze-chart1').highcharts(chartOptions);
}

function chart_perInteraction(results, container) {
    console.log("results123: ", results,container);
    var chartOptions = {
    		chart: {
                type: 'column',
                zoomType: 'x',
                marginTop: 50
            },
            title: {
                text: '\u00a0'
            },
            xAxis: {
                categories: results.categories,
                labels: {
                    formatter: function() {
                        var text = this.value;
                        var idx = text.indexOf('(');
                        return idx > 0 ? '<div class="js-ellipse" style="width: 100px;" title="' + text + '"><span  title="' + text.substr(0, (idx - 1)) + '">' + text.substr(0, (idx - 1)) + '</span><br/><span  title="' + text.substr(idx) + '">' + text.substr(idx) + '</span></div>' : text;
                    }
                }
            },
            yAxis: {
                title: {
                    text: ' '
                },
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                areaspline: {
                    fillOpacity: 0.2,
                    marker: {
                        enabled: false
                    }
                }
            },
            tooltip: {
                formatter: function() {
                    var s = this.x;
                    s += '<br/><b>' + (this.y).toFixed(2) + '</b>';
                    return s;
                }
            },
            exporting: {
                chartOptions: {
                    title: {
                        text: results.title,
                        y: 7,
                        style: {
                            color: '#798693',
                            fontSize: '13px'
                        }
                    },
                    subtitle: {
                       // text: 'Interval.',
                        y: 25,
                        style: {
                            color: '#aaa',
                            fontSize: '10px'
                        }
                    }
                },
                filename: results.title
            },
            series: [{
                showInLegend: false,
                //name: dataObjList[0].companyName,
                data: results.series
            }],
            lang: {
                noData: "No Data Found"
            }
    };
    var chart = $(container).highcharts(chartOptions);
}

function chart_clock(results, param) {
    var clockGroup, height, offSetX,
        offSetY, 
		pi, 
		render, 
		scaleHours, 
		scaleSecsMins, 
		vis, 
		width, 
		idx = 0,
		radius = 70,
		tickLength = 5,
		fromClock = 9,
		toClock = 6,
		circleDegree = 360,
		radians = 0.0174532925,
		clockDivision = 24,
		arcData = results,
		colors = ['#22adca', '#35b3ce', '#50bcd3', '#6ec6d8', '#8dd0de', '#a7d8e3', '#b8dee6', '#EBF4FD'];

    function degToRad(degrees) {
        return degrees * Math.PI / 180;
    }

    function clockToRad(clock, direction) {
        var unit = circleDegree / clockDivision;
        var degree = direction > 0 ? unit * clock : unit * clock - circleDegree;
        return degToRad(unit * clock);
    }

    function getCoordFromCircle(deg, cx, cy, r) {
        var rad = degToRad(deg);
        var x = cx + r * Math.cos(rad);
        var y = cy + r * Math.sin(rad);
        return [x, y];
    }

    function splitDegrees(num) {
        var angle = circleDegree / num;
        var clockInterval = clockDivision / num;
        var degrees = [];
        var clockHour = 0;
        for (var ang = 0; ang < circleDegree; ang += angle) {
            if (clockHour == 0) {
                degrees.push({
                    angle: ang,
                    text: 24
                });
                clockHour += clockInterval;
            } 
			else {
                degrees.push({
                    angle: ang,
                    text: clockHour
                });
                clockHour += clockInterval;
            }
        }
        return degrees;
    }

    offSetX = radius + 20;
    offSetY = radius + 20;
    pi = Math.PI;

   /* scaleSecsMins = d3.scale.linear()
        .domain([0, 59 + 59 / 60])
        .range([0, 2 * pi]);

    scaleHours = d3.scale.linear()
        .domain([0, 11 + 59 / 60])
        .range([0, 2 * pi]);
	*/
    vis = d3.select(param.id)
        .append("svg:svg")
        .attr("class", "clock-svg")
        .attr("width", radius * 2.6)
        .attr("height", radius * 2.6);

    clockGroup = vis.append("svg:g")
        .attr("transform", "translate(" + offSetX + "," + offSetY + ")");


    clockGroup.append("svg:circle")
        .attr("r", radius)
        .attr("fill", "#EBF4FD")
        .attr("class", "clock outercircle")
        .attr("stroke", "#EBF4FD")
        .attr("stroke-width", 0);



    var arc = d3.svg.arc()
        .innerRadius(0)
        .outerRadius(radius)
        .startAngle(function(d, i) {
            return clockToRad(d.updateHour, -1)
        })
        .endAngle(function(d, i) {
            return clockToRad(d.to_hour, 1)
        });
    //console.log('arcData: ',arcData);
    clockGroup.selectAll("path")
        .data(arcData)
        .enter()
        .append("path")
        .attr("d", arc)
        .attr("class", "arc")
        .attr("stroke", "#fff")
        .attr("stroke-width", 0)
        .style('fill', function(d, i) {
            if (d.count == 0) {
                return colors[arcData.length - 1];
            }
            if (i > 0) {
                if (d.avgCount !== arcData[i - 1].avgCount) {
                    idx += 1;
                }
            }
            return colors[idx];
        });


    d3.selectAll(".d3-tooltip").remove();
    var tooltip = d3.select("body")
        .append("div")
        .attr("class", "d3-tooltip");

    d3.selectAll(".arc")
        .on("mouseover", function(d, i) {
            return tooltip.style("visibility", "visible")
                .style("border-color", colors[i])
                .style("color", d.color)
                .html("<strong class='data-type'>Time Interval: </strong><span class='data-type'>" + d.to_hour + " - " + d.updateHour + " o`clock</span><br/><strong>Count: </strong><span>" + d.avgCount + "</span>");
        })
        .on("mousemove", function(d) {
            return tooltip.style("top", (d3.event.pageY - 10) + "px").style("left", (d3.event.pageX + 10) + "px");
        })
        .on("mouseout", function(d) {
            return tooltip.style("visibility", "hidden");
        });

    clockGroup.append('g')
        .attr('class', 'ticks')
        .selectAll('path')
        .data(splitDegrees(8))
        .enter()
        .append('path')
        .attr('d', function(d) {
            var coord = {
                outer: getCoordFromCircle(d.angle, 0, 0, radius),
                inner: getCoordFromCircle(d.angle, 0, 0, radius - tickLength)
            };
            return 'M' + coord.outer[0] + ' ' + coord.outer[1] + 'L' + coord.inner[0] + ' ' + coord.inner[1] + 'Z';
        });

    var hourScale = d3.scale.linear()
        .range([0, 330])
        .domain([0, 11]);
    var i = j = -5;

    clockGroup.selectAll('.hour-label')
        .data(splitDegrees(8))
        .enter()
        .append('text')
        .attr('class', 'hour-label')
        .attr('text-anchor', 'middle')
        .attr("fill", "#000")
        .style('font-size', '10px')
        .attr('x', function(d, i) {
            if (i == 0) {
                return (radius + 5) * Math.sin(hourScale(d.text) * radians / 2);
            }
            if (i > 4) {
                return (radius + (i - (i - 4) * 2) * 5) * Math.sin(hourScale(d.text) * radians / 2);
            }
            return (radius + (5 * i)) * Math.sin(hourScale(d.text) * radians / 2);
        })
        .attr('y', function(d, i) {
            if (i == 0) {
                return -(radius + 5) * Math.cos(hourScale(d.text) * radians / 2);
            }
            if (i > 4) {
                return -(radius + (i - (i - 4) * 2) * 5) * Math.cos(hourScale(d.text) * radians / 2);
            }
            return -(radius + (5 * i)) * Math.cos(hourScale(d.text) * radians / 2);
        })
        .text(function(d) {
            return d.text;
        });

    clockGroup.append("svg:circle")
        .attr("r", 5)
        .attr("fill", "#EBF4FD")
        .attr("class", "clock innercircle");

  /*render = function(data) {
	  var hourArc, minuteArc, secondArc;
	  clockGroup.selectAll(".clockhand").remove();
	  secondArc = d3.svg.arc()
		  .innerRadius(0)
		  .outerRadius(70)
		  .startAngle(function(d) {
			  return scaleSecsMins(d.numeric);
		  }).endAngle(function(d) {
			  return scaleSecsMins(d.numeric);
		  });

	  minuteArc = d3.svg.arc()
		  .innerRadius(0)
		  .outerRadius(70)
		  .startAngle(function(d) {
			  return scaleSecsMins(d.numeric);
		  })
		  .endAngle(function(d) {
			  return scaleSecsMins(d.numeric);
		  });

	  hourArc = d3.svg.arc()
		  .innerRadius(0)
		  .outerRadius(50)
		  .startAngle(function(d) {
			  return scaleHours(d.numeric % clockDivision);
		  }).endAngle(function(d) {
			  return scaleHours(d.numeric % clockDivision);
		  });

	  clockGroup.selectAll(".clockhand")
		  .data(data)
		  .enter()
		  .append("svg:path")
		  .attr("d", function(d) {
			  if (d.unit === "seconds") {
				  return secondArc(d);
			  } else if (d.unit === "minutes") {
				  return minuteArc(d);
			  } else if (d.unit === "hours") {
				  return hourArc(d);
			  }
		  })
		  .attr("class", "clockhand")
		  .attr("stroke", "#fff")
		  .attr("stroke-width", function(d) {
			  if (d.unit === "seconds") {
				  return 2;
			  } else if (d.unit === "minutes") {
				  return 3;
			  } else if (d.unit === "hours") {
				  return 3;
			  }
		  })
		  .attr("fill", "none");
	};

	setInterval(function() {
	  var data;
	  data = fields();
	  return render(data);
	}, 1000);*/

}

function generateWordcloud(wordcloudArray, entityArray) {
    $(".wordcloud").remove();
    var entitySelected = '';
    var minSize, maxSize, sizeFactor;
    var frequency_list = wordcloudArray.concat(entityArray);
    if (wordcloudArray.length > 0) {

        minSize = Math.min.apply(null, wordcloudArray.map(function(a) {
            return a.size;
        }));
        maxSize = Math.max.apply(null, wordcloudArray.map(function(a) {
            return a.size;
        }))
        for (var i = 0; i < frequency_list.length; i++) {
            frequency_list[i].id = 'word' + i;
            frequency_list[i].freq = frequency_list[i].size;
            if (i < wordcloudArray.length && maxSize != minSize) {
                sizeFactor = 10 / (maxSize - minSize);
                frequency_list[i].size = 15 + ((frequency_list[i].size - minSize) * sizeFactor);
            } else {
                frequency_list[i].size = 14;
            }
        }
    } else {
        minSize = Math.min.apply(null, frequency_list.map(function(a) {
            return a.size;
        }));
        maxSize = Math.max.apply(null, frequency_list.map(function(a) {
            return a.size;
        }))
        for (var i = 0; i < frequency_list.length; i++) {
            frequency_list[i].id = 'word' + i;
            frequency_list[i].freq = frequency_list[i].size;
            if (maxSize != minSize) {
                sizeFactor = 10 / (maxSize - minSize);
                frequency_list[i].size = 18 + ((frequency_list[i].size - minSize) * sizeFactor);
            } else {
                frequency_list[i].size = 16;
            }

        }
    }
    var width = 450;
    var height = 364;

    d3.layout.cloud().size([width, height])
        /*var color = d3.scale.linear()
            .domain([0,1,2,3,4,5,6,10,15,20,100])
            .range(["#ccc", "#bbb", "#aaa", "#999", "#888", "#777", "#666", "#555", "#444", "#333", "#222", '#111']);*/

    var texts = d3.layout.cloud().size([width, height])
        .words(frequency_list)
        .padding(5)
        .rotate(0)
        .text(function(d) {
            return d.text;
        })
        .fontSize(function(d) {
            return d.size;
        })
        .on("end", draw)
        .start();
    d3.selectAll(".d3-tooltip").remove();
    var tooltip = d3.select("body")
        .append("div")
        .style("position", "absolute")
        .style("z-index", "99999")
        .attr("class", "d3-tooltip");
    console.log("-------------------> 1");
    d3.selectAll(".cloud-text")
        .on("mouseover", function(d, i) {
        	console.log("data : " + d.sentiment);
        	var sentimentText = "";
        	if(d.sentiment !== undefined){
        		sentimentText = "<strong>Sentiment: </strong><span>" + d.sentiment + "</span>";
        		console.log('Sentiment is not undefined --> Sentiment : '+d.sentiment + ' : Type : ' + d.type + ' : Text : ' + d.text + ' : Freq : ' + d.freq + ' : Sentiment Text : ' + sentimentText + '--->' + d.color)
        	}
            return tooltip.style("visibility", "visible")
                .style("border-color", d.color)
                .style("color", d.color)
                .style("border", "1px solid")
                .style("width", "auto")
                .style("background-color", "#fff")
				.style("padding", "0 5px")
                .html("<strong class='data-type'>" + d.type + ": </strong><span class='data-type'>" + d.text + "</span><br/><strong>Review Count: </strong><span>" + d.freq + "</span><br/>" + sentimentText);
        })
        .on("mousemove", function(d) {
            return tooltip.style("top", (d3.event.pageY - 10) + "px").style("left", (d3.event.pageX + 10) + "px");
        })
        .on("mouseout", function(d) {
            return tooltip.style("visibility", "hidden");
        });

    function draw(words) {
        /*------(viewBox, perserveAspectRatio, id) these attributes are added for resize functionality------*/
        d3.select("#wordcloud").append("svg")
            .attr("width", width)
            .attr("height", height)
            .attr("viewBox", "0 0 " + width + " " + height)
            .attr("perserveAspectRatio", "xMinYMid")
            .attr("id", "wordcloud_chart")
            .attr("class", "wordcloud")
            .append("g")
            // without the transform, words words would get cutoff to the left and top, they would
            // appear outside of the SVG area
            .attr("transform", "translate(" + width / 2 + "," + ((height / 2) + 5) + ")")
            .selectAll("text")
            .data(words)
            .enter()
            .append("text")
            .attr("text-anchor", "middle")
            .attr("id", function(d) {
                return d.id;
            })
            .attr("class", "cloud-text")
            .style("font-size", function(d) {
                return d.size + "px";
            })
            .style("font-family", "Impact")
            .style("cursor", "pointer")
            .style("font-family", function(d, i) {
                var font = i >= wordcloudArray.length ? 'PT Sans' : 'Impact';
                return font;
            })
            .style("fill", function(d, i) {
                return d.color;
            })
            .attr("transform", function(d) {
                return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
            })
            .text(function(d) {
                return d.text;
            })
            .transition()
            .each(function() {
                d3.select(this).on("click", function(d) {
                    var analyticsScope = angular.element($("#analytics")).scope();
                    if (wordcloudArray.length > 0 && (wordcloudArray.find(function(word) {
                            return word.text == d.text;
                        }))) {
                        analyticsScope.filterEntity = '';
                        analyticsScope.filterContext = d.text;
                        analyticsScope.fn_word_cloud(d.text);
                        /*if(analyticsScope.selectedTopic != "All Contexts" && analyticsScope.selectedTopic != "All Contexts" && analyticsScope.selectedTopic != d.text){
                        	analyticsScope.fn_word_cloud_filtered(d.text);
                        }*/
                    } else {
                        $('#' + entitySelected).css('font-weight', 'normal');
                        if (entitySelected != d.id) {
                            entitySelected = d.id
                            $('#' + entitySelected).css('font-weight', 'bold');
                            //alert(0);
                            //alert(d.text)
                            analyticsScope.fn_filterByEntity(d.text);
                        } else {
                            entitySelected = '';
                            analyticsScope.filterEntity = '';
                        }
                    }
                });
            });
    }

    /*------Word Cloud Chart Resize Start------*/

    var chart = $("#wordcloud_chart"),
        aspect = chart.width() / chart.height(),
        container = chart.parent();

    $(window).on("resize", function() {
        var targetWidth = container.width();
        if (targetWidth > 0) {
            chart.attr("width", targetWidth);
            chart.attr("height", Math.round(targetWidth / aspect));
        }

    }).trigger("resize");

    /*------Word Cloud Chart Resize End------*/
}

function generateWordcloudSentimentAttribute(wordcloudArray, blankArray) {
    $(".wordcloud1").remove();
    var entitySelected = '';
    var minSize, maxSize, sizeFactor;
    var frequency_list = wordcloudArray.concat(blankArray);
    //console.log(wordcloudArray);
    if (wordcloudArray.length > 0) {

        minSize = Math.min.apply(null, wordcloudArray.map(function(a) {
            return a.size;
        }));
        maxSize = Math.max.apply(null, wordcloudArray.map(function(a) {
            return a.size;
        }))
        for (var i = 0; i < frequency_list.length; i++) {
            frequency_list[i].id = 'word' + i;
            frequency_list[i].freq = frequency_list[i].size;
            if (i < wordcloudArray.length && maxSize != minSize) {
                sizeFactor = 10 / (maxSize - minSize);
                frequency_list[i].size = 15 + ((frequency_list[i].size - minSize) * sizeFactor);
            } else {
                frequency_list[i].size = 14;
            }
        }
    } else {
        minSize = Math.min.apply(null, frequency_list.map(function(a) {
            return a.size;
        }));
        maxSize = Math.max.apply(null, frequency_list.map(function(a) {
            return a.size;
        }))
        for (var i = 0; i < frequency_list.length; i++) {
            frequency_list[i].id = 'word' + i;
            frequency_list[i].freq = frequency_list[i].size;
            if (maxSize != minSize) {
                sizeFactor = 10 / (maxSize - minSize);
                frequency_list[i].size = 18 + ((frequency_list[i].size - minSize) * sizeFactor);
            } else {
                frequency_list[i].size = 16;
            }

        }
    }
    var width = 450;
    var height = 364;

    d3.layout.cloud().size([width, height])
        /*var color = d3.scale.linear()
            .domain([0,1,2,3,4,5,6,10,15,20,100])
            .range(["#ccc", "#bbb", "#aaa", "#999", "#888", "#777", "#666", "#555", "#444", "#333", "#222", '#111']);*/

    var texts = d3.layout.cloud().size([width, height])
        .words(frequency_list)
        .padding(5)
        .timeInterval(20)
        .rotate(0)
        .text(function(d) {
            return d.text;
        })
        .fontSize(function(d) {
            return d.size;
        })
        .on("end", draw)
        .start();
    
    d3.selectAll(".d3-tooltipSentimentAttr").remove();
    var tooltip = d3.select("body")
			    .append("div")
			    .style("position", "absolute")
			    .style("z-index", "99999")
			    .attr("class", "d3-tooltipSentimentAttr");
        
    
    /*d3.selectAll(".cloud-text")
        .on("mouseover", function(d, i) {
        	console.log('----------------->>>>>>>>');
        	console.log('-------------->>>'+d.text);
        	var sentimentText = "";
        	if(d.text !== undefined){
        		sentimentText = "<strong>Sentiment: </strong><span>" + d.text + "</span>";
        	}
            return tooltip.style("visibility", "visible")
                .style("border-color", d.color)
                .style("color", d.color)
                .html("<strong class='data-type'>" + d.size + ": </strong><span class='data-type'>" + d.text + "</span><br/><strong>Review Count: </strong><span>" + d.size + "</span><br/>" + sentimentText);
        })
        .on("mousemove", function(d) {
    		console.log('Top sentiment attr -->  : Text : ' + d.text + ' : Freq : ' + d.size + ' : Sentiment Text : ' + sentimentText + '--->' + d.color)

            return tooltip.style("top", (d3.event.pageY - 10) + "px").style("left", (d3.event.pageX + 10) + "px");
        })
        .on("mouseout", function(d) {
            return tooltip.style("visibility", "hidden");
        });*/
    /*d3.selectAll(".cloud-text")
    .on("mouseover", function(d, i) {
    	console.log("data Sentiment : " + d.sentiment);
    	var sentimentText = "";
    	if(d.sentiment !== undefined){
    		sentimentText = "<strong>Sentiment: </strong><span>" + d.sentiment + "</span>";
    		console.log('Sentiment is not undefined --> Sentiment : '+d.sentiment + ' : Type : ' + d.type + ' : Text : ' + d.text + ' : Freq : ' + d.freq + ' : Sentiment Text : ' + sentimentText + '--->' + d.color)
    	}
        return tooltip.style("visibility", "visible")
            .style("border-color", d.color)
            .style("color", d.color)
            .style("border", "1px solid")
            .style("width", "auto")
            .style("background-color", "#fff")
			.style("padding", "0 5px")
            .html("<strong class='data-type'>" + d.type + ": </strong><span class='data-type'>" + d.text + "</span><br/><strong>Review Count: </strong><span>" + d.freq + "</span><br/>" + sentimentText);
    })
    .on("mousemove", function(d) {
        return tooltip.style("top", (d3.event.pageY - 10) + "px").style("left", (d3.event.pageX + 10) + "px");
    })
    .on("mouseout", function(d) {
        return tooltip.style("visibility", "hidden");
    });*/


    function draw(words) {
        /*------(viewBox, perserveAspectRatio, id) these attributes are added for resize functionality------*/
        d3.select("#wordcloudtopsentimentattribute").append("svg")
            .attr("width", width)
            .attr("height", height)
            .attr("viewBox", "0 0 " + width + " " + height)
            .attr("perserveAspectRatio", "xMinYMid")
            .attr("id", "wordcloud_chart1")
            .attr("class", "wordcloud1")
            .append("g")
            // without the transform, words words would get cutoff to the left and top, they would
            // appear outside of the SVG area
            .attr("transform", "translate(" + width / 2 + "," + ((height / 2) + 5) + ")")
            .selectAll("text")
            .data(words)
            .enter()
            .append("text")
            .attr("text-anchor", "middle")
            .attr("id", function(d) {
                return d.id;
            })
            .attr("class", "cloud-text")
            .style("font-size", function(d) {
                return d.size + "px";
            })
            .style("font-family", "Impact")
            .style("cursor", "pointer")
            .style("font-family", function(d, i) {
                var font = i >= wordcloudArray.length ? 'PT Sans' : 'Impact';
                return font;
            })
            .style("fill", function(d, i) {
                return d.color;
            })
            .attr("transform", function(d) {
                return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
            })
            .text(function(d) {
                return d.text;
            })
            .transition()
            .each(function() {
                d3.select(this).on("mouseover", function(d) {
                    /*var analyticsScope = angular.element($("#analytics")).scope();
                    if (wordcloudArray.length > 0 && (wordcloudArray.find(function(word) {
                            return word.text == d.text;
                        }))) {
                        analyticsScope.filterEntity = '';
                        analyticsScope.filterContext = d.text;
                        analyticsScope.fn_word_cloud(d.text);
                        if(analyticsScope.selectedTopic != "All Contexts" && analyticsScope.selectedTopic != "All Contexts" && analyticsScope.selectedTopic != d.text){
                        	analyticsScope.fn_word_cloud_filtered(d.text);
                        }
                    } else {
                        $('#' + entitySelected).css('font-weight', 'normal');
                        if (entitySelected != d.id) {
                            entitySelected = d.id
                            $('#' + entitySelected).css('font-weight', 'bold');
                            //alert(0);
                            //alert(d.text)
                            analyticsScope.fn_filterByEntity(d.text);
                        } else {
                            entitySelected = '';
                            analyticsScope.filterEntity = '';
                        }
                    }*/
                	
                	console.log("data Sentiment : " + d.sentiment);
                	var sentimentText = "";
                	if(d.sentiment !== undefined){
                		sentimentText = "<strong>Sentiment: </strong><span>" + d.sentiment + "</span>";
                		console.log('Sentiment is not undefined --> Sentiment : '+d.sentiment + ' : Type : ' + d.type + ' : Text : ' + d.text + ' : Freq : ' + d.freq + ' : Sentiment Text : ' + sentimentText + '--->' + d.color)
                	}
                    return tooltip.style("visibility", "visible")
                        .style("border-color", d.color)
                        .style("color", d.color)
                        .style("border", "1px solid")
                        .style("width", "auto")
                        .style("background-color", "#fff")
            			.style("padding", "0 5px")
                        .html("<strong class='data-type'>" + d.type + ": </strong><span class='data-type'>" + d.text + "</span><br/><strong>Review Count: </strong><span>" + d.freq + "</span><br/>" + sentimentText);
                }).on("mouseout", function(d) {
                    return tooltip.style("visibility", "hidden");
                }).on("mousemove", function(d) {
                    return tooltip.style("top", (d3.event.pageY - 10) + "px").style("left", (d3.event.pageX + 10) + "px");
                });
            });
    }

    /*------Word Cloud Chart Resize Start------*/

    var chart = $("#wordcloud_chart1"),
        aspect = chart.width() / chart.height(),
        container = chart.parent();

    $(window).on("resize", function() {
        var targetWidth = container.width();
        if (targetWidth > 0) {
            chart.attr("width", targetWidth);
            chart.attr("height", Math.round(targetWidth / aspect));
        }

    }).trigger("resize");

    /*------Word Cloud Chart Resize End------*/
}

function generateWordcloudTopHashTag(wordcloudArray, entityArray) {
    $(".wordcloud2").remove();
    var entitySelected = '';
    var minSize, maxSize, sizeFactor;
    var frequency_list = wordcloudArray.concat(entityArray);
    if (wordcloudArray.length > 0) {

        minSize = Math.min.apply(null, wordcloudArray.map(function(a) {
            return a.size;
        }));
        maxSize = Math.max.apply(null, wordcloudArray.map(function(a) {
            return a.size;
        }))
        for (var i = 0; i < frequency_list.length; i++) {
            frequency_list[i].id = 'word' + i;
            frequency_list[i].freq = frequency_list[i].size;
            if (i < wordcloudArray.length && maxSize != minSize) {
                sizeFactor = 10 / (maxSize - minSize);
                frequency_list[i].size = 15 + ((frequency_list[i].size - minSize) * sizeFactor);
            } else {
                frequency_list[i].size = 14;
            }
        }
    } else {
        minSize = Math.min.apply(null, frequency_list.map(function(a) {
            return a.size;
        }));
        maxSize = Math.max.apply(null, frequency_list.map(function(a) {
            return a.size;
        }))
        for (var i = 0; i < frequency_list.length; i++) {
            frequency_list[i].id = 'word' + i;
            frequency_list[i].freq = frequency_list[i].size;
            if (maxSize != minSize) {
                sizeFactor = 10 / (maxSize - minSize);
                frequency_list[i].size = 18 + ((frequency_list[i].size - minSize) * sizeFactor);
            } else {
                frequency_list[i].size = 16;
            }

        }
    }
    var width = 450;
    var height = 364;

    d3.layout.cloud().size([width, height])
        var color = d3.scale.linear()
            .domain([0,1,2,3,4,5,6,10,15,20,100])
            .range(["#d47033","#3f5b45","#00a752","#8f268d","#ea5b23","#25acdb","#f05f2a","#ef1827"]);

    var texts = d3.layout.cloud().size([width, height])
        .words(frequency_list)
        .padding(5)
        //.rotate(0)
        .spiral("rectangular")
        .timeInterval(20)
        .rotate(function() { return ~~(Math.random() * 2) * 90; })
        .text(function(d) {
            return d.text;
        })
        .fontSize(function(d) {
            return d.size;
        })
        .on("end", draw)
        .start();
    
    d3.selectAll(".d3-tooltipTophashTags").remove();
    var tooltip = d3.select("body")
			    .append("div")
			    .style("position", "absolute")
			    .style("z-index", "99999")
			    .attr("class", "d3-tooltipTophashTags");

    /*d3.selectAll(".cloud-text")
        .on("mouseover", function(d, i) {
        	var sentimentText = "";
        	if(d.sentiment !== undefined){
        		sentimentText = "<strong>Sentiment: </strong><span>" + d.sentiment + "</span>";
        	}
            return tooltip.style("visibility", "visible")
                .style("border-color", d.color)
                .style("color", d.color)
                .html("<strong class='data-type'>" + d.type + ": </strong><span class='data-type'>" + d.text + "</span><br/><strong>Review Count: </strong><span>" + d.freq + "</span><br/>" + sentimentText);
        })
        .on("mousemove", function(d) {
            return tooltip.style("top", (d3.event.pageY - 10) + "px").style("left", (d3.event.pageX + 10) + "px");
        })
        .on("mouseout", function(d) {
            return tooltip.style("visibility", "hidden");
        });*/

    function draw(words) {
        /*------(viewBox, perserveAspectRatio, id) these attributes are added for resize functionality------*/
        d3.select("#wordcloudTopHasTag").append("svg")
            .attr("width", width)
            .attr("height", height)
            .attr("viewBox", "0 0 " + width + " " + height)
            .attr("perserveAspectRatio", "xMinYMid")
            .attr("id", "wordcloud_chart2")
            .attr("class", "wordcloud2")
            .append("g")
            // without the transform, words words would get cutoff to the left and top, they would
            // appear outside of the SVG area
            .attr("transform", "translate(" + width / 2 + "," + ((height / 2) + 5) + ")")
            .selectAll("text")
            .data(words)
            .enter()
            .append("text")
            .attr("text-anchor", "middle")
            .attr("id", function(d) {
                return d.id;
            })
            .attr("class", "cloud-text")
            .style("font-size", function(d) {
                return d.size + "px";
            })
            .style("font-family", "Impact")
            //.style("cursor", "pointer")
            .style("font-family", function(d, i) {
                var font = i >= wordcloudArray.length ? 'PT Sans' : 'Impact';
                return font;
            })
            /*.style("fill", function(d, i) {
                return d.color;
            })*/
            //.style('fill',function(d) { return color(d); })
            .style("fill", function(d, i) { return color(i); })
            .attr("transform", function(d) {
                return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
            })
            .text(function(d) {
                return d.text;
            })
            .transition()
            .each(function() {
                d3.select(this).on("mouseover", function(d) {
                    /*var analyticsScope = angular.element($("#analytics")).scope();
                    if (wordcloudArray.length > 0 && (wordcloudArray.find(function(word) {
                            return word.text == d.text;
                        }))) {
                        analyticsScope.filterEntity = '';
                        analyticsScope.filterContext = d.text;
                        analyticsScope.fn_word_cloud(d.text);
                        if(analyticsScope.selectedTopic != "All Contexts" && analyticsScope.selectedTopic != "All Contexts" && analyticsScope.selectedTopic != d.text){
                        	analyticsScope.fn_word_cloud_filtered(d.text);
                        }
                    } else {
                        $('#' + entitySelected).css('font-weight', 'normal');
                        if (entitySelected != d.id) {
                            entitySelected = d.id
                            $('#' + entitySelected).css('font-weight', 'bold');
                            //alert(0);
                            //alert(d.text)
                            analyticsScope.fn_filterByEntity(d.text);
                        } else {
                            entitySelected = '';
                            analyticsScope.filterEntity = '';
                        }
                    }*/
                	console.log("data Sentiment : " + d.sentiment);
                	var sentimentText = "";
                	if(d.sentiment !== undefined){
                		sentimentText = "<strong>Sentiment: </strong><span>" + d.sentiment + "</span>";
                		console.log('Sentiment is not undefined --> Sentiment : '+d.sentiment + ' : Type : ' + d.type + ' : Text : ' + d.text + ' : Freq : ' + d.freq + ' : Sentiment Text : ' + sentimentText + '--->' + d.color)
                	}
                    return tooltip.style("visibility", "visible")
                        .style("border-color", d.color)
                        .style("color", d.color)
                        .style("border", "1px solid")
                        .style("width", "auto")
                        .style("background-color", "#fff")
            			.style("padding", "0 5px")
                        .html("<strong class='data-type'>" + d.type + ": </strong><span class='data-type'>" + d.text + "</span><br/><strong>Review Count: </strong><span>" + d.freq + "</span><br/>" + sentimentText);
                }).on("mouseout", function(d) {
                    return tooltip.style("visibility", "hidden");
                }).on("mousemove", function(d) {
                    return tooltip.style("top", (d3.event.pageY - 10) + "px").style("left", (d3.event.pageX + 10) + "px");
                });
              });
            //});
    }

    /*------Word Cloud Chart Resize Start------*/

    var chart = $("#wordcloud_chart2"),
        aspect = chart.width() / chart.height(),
        container = chart.parent();

    $(window).on("resize", function() {
        var targetWidth = container.width();
        if (targetWidth > 0) {
            chart.attr("width", targetWidth);
            chart.attr("height", Math.round(targetWidth / aspect));
        }

    }).trigger("resize");

    /*------Word Cloud Chart Resize End------*/
}


function area_chart(results, container) {
	var weekdays = {
			"Sun": "Sunday",
			"Mon": "Monday",
			"Tue": "Tuesday",
			"Wed": "Wednesday",
			"Thu": "Thursday",
			"Fri": "Friday",
			"Sat": "Saturday" 
		};
    var chartOptions = {
        chart: {
            type: 'area',
            zoomType: 'x'
        },
        title: {
            text: '\u00a0'
        },
        xAxis: {
        	categories: results.categories,
            labels: {
                style: {
                	fontSize: '10px'
                }
            }
        },
        yAxis: {
            title: {
                text: ' '
            },
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            areaspline: {
                fillOpacity: 0.2,
                marker: {
                    enabled: false
                }
            }
        },
        tooltip: {
            formatter: function(d,i) {
            	console.log(this.x);
                var s = weekdays[this.x];
                s += '<br/><b>Average Count: ' + this.y + '</b>';
                return s;
            }
        },
        series: [{
            showInLegend: false,
            data: results.series
        }],
        exporting: {
        	enabled: false,
            chartOptions: {
                title: {
                    text: 'Most Engaging Tweets',
                    y: 7,
                    style: {
                        color: '#798693',
                        fontSize: '13px'
                    }
                }
            },
            filename: 'MOST ENGAGING TWEETS'
        },
        lang: {
        	shortWeekdays: [ 'Sun','Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
            noData: "No Data Found"
        }
    };
    console.log("results.series:: ",results.series);
    var chart = $(container).highcharts(chartOptions);
}


function generateBrandTimelineGraph(mention,netsentiment,potentialimpression,dateRange){
	//alert(mention[2]);
	/*var arrPotentialimpression = new Array();
	var arrMention = Object.keys(mention).map(function (key) { return mention[key]; });
	var arrNetsentiment = Object.keys(netsentiment).map(function (key) { return netsentiment[key]; });
	arrPotentialimpression = Object.keys(potentialimpression).map(function (key) { return potentialimpression[key]; });*/
	
	Highcharts.chart('brandTimelineGraph', {
		colors: ['#00a3d8', '#7b26af', '#8ec741'],
	    chart: {
	        zoomType: 'xy'
	    },
	    title: {
	        text: null
	    },
	    credits: {
			enabled: false
		},
	    navigation: {
	        buttonOptions: {
	            enabled: false
	        }
	    },
	    xAxis: [{
	        categories: dateRange,
	        crosshair: true
	    }],
	    yAxis: [{ // Primary yAxis
	        labels: {
	            //format: '{value} M',
	            style: {
	                color: ['#999999']
	            }
	        },
	        title: {
	            text: 'Potential Impression',
	            style: {
	            	color: ['#999999']
	            }
	        },
	        opposite: true

	    }, { // Secondary yAxis
	        gridLineWidth: 0,
	        title: {
	            text: 'Mentions',
	            style: {
	            	color: ['#999999']
	            }
	        },
	        labels: {
	            //format: '{value} mm',
	            style: {
	            	color: ['#999999']
	            }
	        }

	    }, { // Tertiary yAxis
	        gridLineWidth: 0,
	        title: {
	            text: 'Net Sentiments',
	            style: {
	            	color: ['#999999']
	            }
	        },
	        labels: {
	            //format: '{value} mb',
	            style: {
	            	color: ['#999999']
	            }
	        },
	        opposite: true
	    }],
	    tooltip: {
	        shared: true
	    },
	    /*legend: {
	        layout: 'vertical',
	        align: 'left',
	        x: 80,
	        verticalAlign: 'top',
	        y: 55,
	        floating: true,
	        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
	    },*/
	    series: [{
	        name: 'Mention',
	        type: 'area',
	        yAxis: 1,
	        fillOpacity: 0.2,
	        data: mention,
	        marker: {
	        	fillColor: "#FFFFFF",
	        	symbol: 'circle',
				radius: 4,
				lineWidth: 2,
				lineColor: null
	        },
	        tooltip: {
	            //valueSuffix: ' mm'
	        }

	    }, {
	        name: 'Net Sentiment',
	        type: 'spline',
	        yAxis: 2,
	        data: netsentiment,
	        marker: {
	        	fillColor: "#FFFFFF",
	        	symbol: 'circle',
				radius: 4,
				lineWidth: 2,
				lineColor: null
	        },
	        //dashStyle: 'shortdot',
	        tooltip: {
	            //valueSuffix: ' mb'
	        }

	    }, {
	        name: 'Potential Impression',
	        type: 'spline',
	        //yAxis: 2,
	        data: potentialimpression,//[86152,40,40,40,40,40,40],// arrPotentialimpression
	        marker: {
	        	fillColor: "#FFFFFF",
	        	symbol: 'circle',
				radius: 4,
				lineWidth: 2,
				lineColor: null
	        },
	        tooltip: {
	           // valueSuffix: ' °C'
	        }
	    }]
	});

}

function generateDashboardWordcloud(wordcloudArray, entityArray,sentimentDiv) {
	   // $(".wordcloud").remove();
		$("."+sentimentDiv).remove();
		
	    var entitySelected = '';
	    var minSize, maxSize, sizeFactor;
	    var frequency_list = wordcloudArray.concat(entityArray);
	    
	    if (wordcloudArray.length > 0) {

	        minSize = Math.min.apply(null, wordcloudArray.map(function(a) {
	            return a.size;
	        }));
	        maxSize = Math.max.apply(null, wordcloudArray.map(function(a) {
	            return a.size;
	        }))
	        for (var i = 0; i < frequency_list.length; i++) {
	            frequency_list[i].id = 'word' + i;
	            frequency_list[i].freq = frequency_list[i].size;
	            if (i < wordcloudArray.length && maxSize != minSize) {
	                sizeFactor = 10 / (maxSize - minSize);
	                frequency_list[i].size = 15 + ((frequency_list[i].size - minSize) * sizeFactor);
	            } else {
	                frequency_list[i].size = 14;
	            }
	        }
	    } else {
	        minSize = Math.min.apply(null, frequency_list.map(function(a) {
	            return a.size;
	        }));
	        maxSize = Math.max.apply(null, frequency_list.map(function(a) {
	            return a.size;
	        }))
	        for (var i = 0; i < frequency_list.length; i++) {
	            frequency_list[i].id = 'word' + i;
	            frequency_list[i].freq = frequency_list[i].size;
	            if (maxSize != minSize) {
	                sizeFactor = 10 / (maxSize - minSize);
	                frequency_list[i].size = 18 + ((frequency_list[i].size - minSize) * sizeFactor);
	            } else {
	                frequency_list[i].size = 16;
	            }

	        }
	    }
	    var width = 320;
	    var height = 364;

	    d3.layout.cloud().size([width, height])
	        /*var color = d3.scale.linear()
	            .domain([0,1,2,3,4,5,6,10,15,20,100])
	            .range(["#ccc", "#bbb", "#aaa", "#999", "#888", "#777", "#666", "#555", "#444", "#333", "#222", '#111']);*/

	    /*var maxValue = d3.max(tags,function(d){ return +d.value});
		var minValue = d3.min(tags,function(d){ return +d.value});
		var centerValue = (maxValue + minValue)/2;
	    var colorScale = d3.scale.linear().domain([minValue, centerValue, maxValue])
	    .range(["red", "white", "green"]);;*/
	    
	    var texts = d3.layout.cloud().size([width, height])
	        .words(frequency_list)
	        .padding(5)
	        .rotate(0)
	        .text(function(d) {
	            return d.text;
	        })
	        .fontSize(function(d) {
	            return d.size;
	        })
	        .on("end", draw)
	        .start();
	    d3.selectAll(".d3-tooltip").remove();
	    var tooltip = d3.select("body")
	        .append("div")
	        .style("position", "absolute")
	        .style("z-index", "99999")
	        .attr("class", "d3-tooltip");
	    console.log("-------------------> 1");
	    d3.selectAll(".cloud-text")
	        .on("mouseover", function(d, i) {
	        	console.log("data : " + d.sentiment);
	        	var sentimentText = "";
	        	if(d.sentiment !== undefined){
	        		sentimentText = "<strong>Sentiment: </strong><span>" + d.sentiment + "</span>";
	        		console.log('Sentiment is not undefined --> Sentiment : '+d.sentiment + ' : Type : ' + d.type + ' : Text : ' + d.text + ' : Freq : ' + d.freq + ' : Sentiment Text : ' + sentimentText + '--->' + d.color)
	        	}
	            return tooltip.style("visibility", "visible")
	                .style("border-color", d.color)
	                .style("color", d.color)
	                .style("border", "1px solid")
	                .style("width", "auto")
	                .style("background-color", "#fff")
					.style("padding", "0 5px")
	                .html("<strong class='data-type'>" + d.type + ": </strong><span class='data-type'>" + d.text + "</span><br/><strong>Review Count: </strong><span>" + d.freq + "</span><br/>" + sentimentText);
	        })
	        .on("mousemove", function(d) {
	            return tooltip.style("top", (d3.event.pageY - 10) + "px").style("left", (d3.event.pageX + 10) + "px");
	        })
	        .on("mouseout", function(d) {
	            return tooltip.style("visibility", "hidden");
	        });

	    function draw(words) {
	        /*------(viewBox, perserveAspectRatio, id) these attributes are added for resize functionality------*/
	       // d3.select("#overallWordcloud").append("svg")
	    	d3.select("#"+sentimentDiv).append("svg")
	            .attr("width", width)
	            .attr("height", height)
	            .attr("viewBox", "0 0 " + width + " " + height)
	            .attr("perserveAspectRatio", "xMinYMid")
	            .attr("id", "wordcloud_chart")
	            .attr("class", sentimentDiv)
	            //.attr("class", "wordcloud")
	            .append("g")
	            // without the transform, words words would get cutoff to the left and top, they would
	            // appear outside of the SVG area
	            .attr("transform", "translate(" + width / 2 + "," + ((height / 2) + 5) + ")")
	            .selectAll("text")
	            .data(words)
	            .enter()
	            .append("text")
	            .attr("text-anchor", "middle")
	            .attr("id", function(d) {
	                return d.id;
	            })
	            .attr("class", "cloud-text")
	            .style("font-size", function(d) {
	                return d.size + "px";
	            })
	            .style("font-family", "Impact")
	            .style("cursor", "pointer")
	            .style("font-family", function(d, i) {
	                var font = i >= wordcloudArray.length ? 'PT Sans' : 'Impact';
	                return font;
	            })
	            .style("fill", function(d, i) {
	            	//alert(d.color +"--"+ d.size);
	                return d.color;
	            })
	            .style("opacity", function(d) { return (d.size / 100)+0.4 })
	            .attr("transform", function(d) {
	                return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
	            })
	            .text(function(d) {
	                return d.text;
	            })
	            .transition()
	            .each(function() {
	                d3.select(this).on("click", function(d) {
	                    var analyticsScope = angular.element($("#analytics")).scope();
	                    if (wordcloudArray.length > 0 && (wordcloudArray.find(function(word) {
	                            return word.text == d.text;
	                        }))) {
	                        analyticsScope.filterEntity = '';
	                        analyticsScope.filterContext = d.text;
	                        analyticsScope.fn_word_cloud(d.text);
	                        /*if(analyticsScope.selectedTopic != "All Contexts" && analyticsScope.selectedTopic != "All Contexts" && analyticsScope.selectedTopic != d.text){
	                        	analyticsScope.fn_word_cloud_filtered(d.text);
	                        }*/
	                    } else {
	                        $('#' + entitySelected).css('font-weight', 'normal');
	                        if (entitySelected != d.id) {
	                            entitySelected = d.id
	                            $('#' + entitySelected).css('font-weight', 'bold');
	                            //alert(0);
	                            //alert(d.text)
	                            analyticsScope.fn_filterByEntity(d.text);
	                        } else {
	                            entitySelected = '';
	                            analyticsScope.filterEntity = '';
	                        }
	                    }
	                });
	            });
	    }

	    /*------Word Cloud Chart Resize Start------*/

	    var chart = $("#wordcloud_chart"),
	        aspect = chart.width() / chart.height(),
	        container = chart.parent();

	    $(window).on("resize", function() {
	        var targetWidth = container.width();
	        if (targetWidth > 0) {
	            chart.attr("width", targetWidth);
	            chart.attr("height", Math.round(targetWidth / aspect));
	        }

	    }).trigger("resize");

	    /*------Word Cloud Chart Resize End------*/
	    
	    

	}

//Chart for Post level detailed chain
function chart_post_level_chain(resultset) {
	//var results = {"result":{"rows":[[2,"Neutral"],[3,"Positive"]]}};
	var positiveSentiment = [];
    var negativeSentiment = [];
    var neutralSentiment = [];
    var xCategories = [];
    
    var deeplistScope = angular.element($("#page-deelistining")).scope();
    deeplistScope.modalPositiveValue = "N/A";
    deeplistScope.modalNegativeValue = "N/A";
    deeplistScope.modalNeutralValue = "N/A";
	for(var i=0; i<resultset.rows.length; i++){
		if(resultset.rows[i][1] == 'Positive'){
			positiveSentiment.push(resultset.rows[i][0]);
			deeplistScope.modalPositiveValue = resultset.rows[i][0];
		}
		if(resultset.rows[i][1] == 'Negative'){
			negativeSentiment.push(resultset.rows[i][0]);
			deeplistScope.modalNegativeValue = resultset.rows[i][0];
		}
		if(resultset.rows[i][1] == 'Neutral'){
			neutralSentiment.push(resultset.rows[i][0]);
			deeplistScope.modalNeutralValue = resultset.rows[i][0];
		}
	}
    
	xCategories.push("Overall sentiment analysis");
	
    var sentimentSeriesData = [{
        name: 'Positive',
        data: positiveSentiment
    },{
        name: 'Neutral',
        data: neutralSentiment
    }, {
        name: 'Negative',
        data: negativeSentiment
    }];
    $('#summaryChart').highcharts({
        colors: ['green', 'orange', 'red'],
        chart: {
            type: 'column',
        	//type: 'line',
            //width: $('#sentimentalSourceChart').parents('.tab-content').eq(0).innerWidth() * 0.95,
            marginTop: 50
        },
        xAxis: {
           /* min: 0,
            max: i <= 24 ? i - 1 : 24,*/
            categories: xCategories,
            title: {
                //text: "Comparision chat"
            	text: ""
            }
        },
        scrollbar: {
            barBackgroundColor: 'gray',
            barBorderRadius: 7,
            barBorderWidth: 0,
            buttonBackgroundColor: 'gray',
            buttonBorderWidth: 0,
            buttonBorderRadius: 7,
            trackBackgroundColor: 'none',
            trackBorderWidth: 1,
            trackBorderRadius: 8,
            trackBorderColor: '#CCC'
        },
        credits: {
            enabled: false,
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Sentiment'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        tooltip: {
            shared: true
        },
        exporting:{
        	enabled:false
        },
        plotOptions: {
            column: {
                //stacking: 'normal',
                dataLabels: {
                    enabled: false
                }
            },
            series: {
                dataLabels: {
                    enabled: true
                },
                cursor: 'pointer',
                point: {
                    events: {
                        click: function () {
                        	deeplistScope.fn_showSentimentDetailsModal(this.series.name);
                        }
                    }
                }
            }
        },
        title: {
            text: ''
        },
        series: sentimentSeriesData,
        lang: {
            noData: "No Data Found"
        }
    });
    Highcharts.setOptions({
        lang: {
            noData: "No Data Found",
            thousandsSep: ''
        }
    });
}

function fn_generate_ratings_chart(dataArrayList,categoryList,divID,chartRatingsSelectedSource) {
	//var results = {"results":{"rows":[[20,25,44],[30,523,88]]}};
	var dataArray = [];
	for(var xx=0;xx<dataArrayList.length;xx++){
		dataArray.push(parseFloat(dataArrayList[xx]));
	}


	var sentimentSeriesData = [{
        showInLegend: false,
        name: chartRatingsSelectedSource,
        data: dataArray                
    }];
	
    $('#ratingChart_'+divID).highcharts({
        //colors: ['green'],
        chart: {
            type: 'areaspline',
            spacingBottom: 30,
        	//type: 'line',
            //width: $('#sentimentalSourceChart').parents('.tab-content').eq(0).innerWidth() * 0.95,
            margin: [0, 0, 0, 0],
            backgroundColor:'transparent'
        },
        xAxis: {
           /* min: 0,
            max: i <= 24 ? i - 1 : 24,*/
            categories: categoryList,
            title: {
                //text: "Comparision chat"
            	text: ""
            }
        },
        scrollbar: {
            barBackgroundColor: 'gray',
            barBorderRadius: 7,
            barBorderWidth: 0,
            buttonBackgroundColor: 'gray',
            buttonBorderWidth: 0,
            buttonBorderRadius: 7,
            trackBackgroundColor: 'none',
            trackBorderWidth: 1,
            trackBorderRadius: 8,
            trackBorderColor: '#CCC'
        },
        credits: {
            enabled: false,
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        tooltip: {
            shared: true
        },
        exporting:{
        	enabled:false
        },
        plotOptions: {
            column: {
                //stacking: 'normal',
                dataLabels: {
                    enabled: false
                }
            },
            series: {
                dataLabels: {
                    enabled: true,
                    name:""
                },
                cursor: 'pointer',
                point: {
                    events: {
                        click: function () {
                        	
                        	fn_generate_ratings_chart_full_width(dataArrayList,categoryList,chartRatingsSelectedSource);
                        }
                    }
                }
            }
        },
        title: {
            text: ''
        },
        series: sentimentSeriesData,
        /*series: [{
            showInLegend: false,
            name: 'Series',
            data: sentimentSeriesData                
        }],*/
        lang: {
            noData: "No Data Found"
        }
    });
    Highcharts.setOptions({
        lang: {
            noData: "No Data Found",
            thousandsSep: ''
        }
    });
}


function fn_generate_ratings_chart_full_width(dataArrayList,categoryList,chartRatingsSelectedSource) {
	$('#ratingModal').modal("show");
	setTimeout(function(){ 
		var dataArray = [];
		for(var xx=0;xx<dataArrayList.length;xx++){
			dataArray.push(parseFloat(dataArrayList[xx]));
		}
	
	
		var sentimentSeriesData = [{
	        //showInLegend: false,
	        name: chartRatingsSelectedSource,
	        data: dataArray                
	    }];
		
	    $('#ratingChartModal').highcharts({
	        //colors: ['green'],
	        chart: {
	            type: 'spline'
	        	//type: 'line',
	            //width: $('#sentimentalSourceChart').parents('.tab-content').eq(0).innerWidth() * 0.95,
	            //margin: [0, 0, 0, 0],
	            //backgroundColor:'transparent'
	        },
	        xAxis: {
	           /* min: 0,
	            max: i <= 24 ? i - 1 : 24,*/
	            categories: categoryList,
	            title: {
	                text: "Historic Details"
	            }
	        },
	        scrollbar: {
	            barBackgroundColor: 'gray',
	            barBorderRadius: 7,
	            barBorderWidth: 0,
	            buttonBackgroundColor: 'gray',
	            buttonBorderWidth: 0,
	            buttonBorderRadius: 7,
	            trackBackgroundColor: 'none',
	            trackBorderWidth: 1,
	            trackBorderRadius: 8,
	            trackBorderColor: '#CCC'
	        },
	        credits: {
	            enabled: false,
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: ''
	            },
	            stackLabels: {
	                enabled: true,
	                style: {
	                    fontWeight: 'bold',
	                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
	                }
	            }
	        },
	        tooltip: {
	            shared: true
	        },
	        exporting:{
	        	enabled:false
	        },
	        plotOptions: {
	            column: {
	                //stacking: 'normal',
	                dataLabels: {
	                    enabled: false
	                }
	            },
	            series: {
	                dataLabels: {
	                    enabled: true,
	                    name:""
	                }
	            }
	        },
	        title: {
	            text: ''
	        },
	        series: sentimentSeriesData,
	        /*series: [{
	            showInLegend: false,
	            name: 'Series',
	            data: sentimentSeriesData                
	        }],*/
	        lang: {
	            noData: "No Data Found"
	        }
	    });
	    Highcharts.setOptions({
	        lang: {
	            noData: "No Data Found",
	            thousandsSep: ''
	        }
	    });
	 }, 500);
}
