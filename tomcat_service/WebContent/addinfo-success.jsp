<!DOCTYPE html>
<!--[if IE 7]> 
<html lang="en-US" class="no-js IE7 IE">
	<![endif]-->
	<!--[if IE 8]> 
	<html lang="en-US" class="no-js IE8 IE">
		<![endif]-->
		<!--[if IE 9]> 
		<html lang="en-US" class="no-js IE9 IE">
			<![endif]-->
			<!--[if gt IE 9]><!-->
			<html lang="en-US" class="no-js">
				<!--<![endif]-->
				<head>
					<!-- title -->
					<title>Omni-Channel Customer Experience Management Suite | XpressoInsights</title>
					<!-- Main_meta_&_mobile_meta -->
					<meta charset="utf-8">
					<meta name="description" content="">
					<meta name="HandheldFriendly" content="true" />
					<meta name="apple-touch-fullscreen" content="yes" />
					<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
					<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
					<link rel="shortcut icon" href="assets/img/favicon.png" type="image/png"/>
					<!-- favicon -->
					<link rel="stylesheet" type="text/css" href="assets/css/font-awesome.css">
					<!-- font awesome css -->
					<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
					<!-- bootstrap css -->
					<link rel="stylesheet" type="text/css" href="assets/css/bootstrap-theme.css">
					<!-- bootstrap supporting css -->
					<link rel="stylesheet" type="text/css" href="assets/css/style.css">
					<!-- common css -->
					<link rel="stylesheet" type="text/css" href="assets/css/style-forms.css">
					<!-- common css -->
					<!--[if lt IE 9]>
					<script type="text/javascript" src="assets/js/html5shiv.js"></script>
					<script type="text/javascript" src="assets/js/respond.min.js"></script>
					<![endif]-->
				</head>
				<body>
					<!--<section class="main-body-withoutmenu" style="height:100px;">
						<div class="col-md-12">
							<div class="logo-wrapper">
								<img src="assets/img/logo1.png" alt="RadianCX" class="login-logo"/>
								<h3 class="login-logotxt">Unified Customer Experience Management Suite</h3>
							</div>
						</div>
					</section>-->
					<section class="main-body-withoutmenu">
						<div class="col-md-5 col-sm-10 loginform-wrapper">
							<div class="gradiant-line"></div>
							<div class="block-cover">
								<div class="logo-wrapper">
									<img src="assets/img/logo_new.png" alt="" class="login-logo" />
								</div>
								<div class="block-body">
									<div class="row">
										<div class="col-md-12">
											<h4>Thanks for choosing XpressoInsights. We are always here to help you.</h4>
											<p>All Users successfully added. <br/>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
					<script data-require="angular.js@*" data-semver="1.2.5" src="assets/js/angular.min.js"></script> 
					<script type="text/javascript" src="assets/js/jquery.min.js"></script><!-- Main_lib_script --> 
					<script type="text/javascript" src="assets/js/bootstrap.js"></script><!-- bootstrap js -->
					<script type="text/javascript" src="assets/js/validator.js"></script>
				</body>
			</html>