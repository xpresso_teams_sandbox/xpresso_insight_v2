<%@ include file="header.jsp" %>
<%! String userimg = "assets/img/user.jpg"; %>

<body ng-app="app" id="masterBody" ng-controller="MasterController" class="ustus-wapper">
<!-- <div class="se-pre-con"></div> -->
	<div id="container-wrapper">
		
		<!--==============Hidden files==============-->
		<%@include file="/WEB-INF/jspf/hiddenFiles.jspf" %>
		
		<!--Don't remove this checkbox-->
		<!-- <input type="checkbox" id="sidbar"> -->
		
		<!--==============top navigation==============-->
		<%@include file="/WEB-INF/jspf/topNav.jspf" %>
		 
		<!--==============sidebar start==============-->
		<aside class="side_bar">
		  <div class="navigation">
		    <ul>
		        <li ng-repeat="tabs in tabMetaData" id="tab_{{tabs[1]}}"><a ng-href="{{!isAddinfo ? '#'+tabs[1] : ''}}" custom-click="{{!!isAddinfo}}" ng-class="!!isAddinfo ? 'disabled':((activeMenu == tabs[1]) ? 'active': '')" ><i class="fa {{tabs[2]}}" aria-hidden="true"></i><b ng-bind="tabs[0]"></b></a></li>
		        <li><a  ng-class="!!isAddinfo ? 'disabled':((activeMenu == tabs[1]) ? 'active': '')" ><i class="fa fa-download "></i><b ng-click="fn_download_invalid_csv();">Reports</b></a></li>
		        <!-- <li ng-click="fn_download_invalid_csv();"><i class="fa" aria-hidden="true"></i><b>Reports</b></li> -->
		    </ul>
		    
		  </div>
		</aside>
		<section class="main-body" id="main-body"> 
		
			<!-- ==============Fixed area for all page==============-->
			<%-- <%@include file="/WEB-INF/jspf/topFixedDailySnapshot.jspf" %> --%>
		  	
		  	
		  	<!-- ==============Dynamic page depending on route==============-->
		  	<div ng-view></div>
			
		    
		    <!--==============Modals start==============-->
		    <%@include file="/WEB-INF/jspf/modals.jspf" %>
		    
			<!-- <div class="footer-top"></div> -->
			<div class="clearfix"></div>
			<div class="footer">
		  <div class="row">
		    <div class="col-md-4 col-sm-4 col-xs-4 radiancx-logo"></div>
		    <div class="col-md-4 col-sm-4 col-xs-4 version" id="domain_name"><span ng-bind="domainName"></span></div>
		    <div class="col-md-4 col-sm-4 col-xs-4 powered-by"></div>
		  </div>
		</div>
		</section>
		
		
	</div>
</body>

<%@ include file="footer.jsp" %>