	<div id="spinner" data-ng-show="loadingService.isLoading()"><div class="loader">Loading...</div></div>
	<div id="generatePDFspinner" style="display:none;" ><div class="loader">Loading...</div></div>	
	
	<div class="col-md-5 col-sm-6 col-xs-6" id="topHearderLogoHidden" style="background:#fff;display:none;">
      <div class="logo-section">
        <div class="logo"><img id="company_logo_hidden" src="" alt="" />      
      </div>
      </div>
      <div class="header-txt" id="radianCX_header_text">
      	<span ng-bind="radianCXHeader"></span>
      </div>
      <span ng-bind="navbardate" class="current-time pull-right" style="background-color:#fff; color:#000;"></span>
    </div>
	
	
    <!-- Angular js files -->
	<script src="assets/js/angular.min.js"></script>
	<script src="assets/js/angular-route.min.js"></script>
    <script src="assets/js/angular-animate.js"></script> 
	<script src="assets/js/ui-bootstrap-tpls-2.0.0.js"></script>
	<script src="assets/js/dirPagination.js"></script>
	<!-- <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular-sanitize.js"></script> -->
	
	
	<!-- Custom Angular js controller files -->
    <script src="assets/js/angular/app.js"></script>
    <!-- <script src="assets/js/angular/angular-loading-spinner.js"></script> -->
    <script src="assets/js/angular/master-ctrl.js"></script>
    <script src="assets/js/angular/add-details-ctrl.js"></script>
    <script src="assets/js/angular/dashboard-ctrl.js"></script>
    <script src="assets/js/angular/analytics-ctrl.js"></script>
    <script src="assets/js/angular/ticket-ctrl.js"></script>
    <script src="assets/js/angular/deeplistining-ctrl.js"></script>
    <script src="assets/js/angular/analyze-ctrl.js"></script>
    <script src="assets/js/angular/settings-ctrl.js"></script>
    <script src="assets/js/angular/ratings-ctrl.js"></script>
    <script src="assets/js/angular/ticketmanagement-ctrl.js"></script>
    <script src="assets/js/angular/brand-ctrl.js"></script>
    <script src="assets/js/angular/reviewanalysis-ctrl.js"></script>
    <script src="assets/js/angular/campaign-ctrl.js"></script>
	<script src="assets/js/angular/routes.js"></script>
	<!-- <script src="assets/js/angular/DownloadCtrl.js"></script>
	<script src="assets/js/angular/DownloadDirective.js"></script> -->
    
    <!-- js plugins -->
	<script src="assets/js/jquery.min.js"></script><!-- Main_lib_script -->
	<script src="assets/js/bootstrap.js"></script><!-- bootstrap js -->
	<script src="assets/js/bootstrap-select.js"></script>
	<!-- <script src="assets/js/moment.js"></script> -->
	<script src="assets/js/moment.min.js"></script>
	<script src="assets/js/datetimepicker.js"></script>
	<script src="assets/js/datetimepicker.templates.js"></script>
	<script src="assets/js/html2canvas.js"></script>
	<script src="assets/js/html2canvas.svg.js"></script>
	<script src="assets/js/highstock.js"></script>
	<!-- <script src="assets/js/highcharts.js"></script> -->	
	<script src="assets/js/highcharts-more.js"></script>
	<script src="assets/js/d3.min.js"></script>
	<script src="assets/js/exporting.js"></script>
	<script src="assets/js/no-data-to-display.js"></script>
<!--<script src="assets/js/html-table-search.js"></script> -->	
	<script src="assets/js/utility.js"></script>
	<script src="assets/js/rgbcolor.js"></script>
	<script src="assets/js/canvg.js"></script>
	<!--<script src="assets/js/StackBlur.js"></script>-->
	<script src="assets/js/jspdf.js"></script>
	<script src="assets/js/jspdf.plugin.addimage.js"></script>
	<script src="assets/js/FileSaver.js"></script>
    <script src="assets/js/d3.v3.min.js"></script>
    <script src="assets/js/d3.layout.cloud.js"></script>
    <script src="assets/js/jquery.newsTicker.js"></script>
    <script type="text/javascript"  src="assets/js/univ-pagination.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.13/daterangepicker.min.js"></script>
    <script src="https://fragaria.github.io/angular-daterangepicker/js/angular-daterangepicker.min.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	
	<!--<script src="chartDirective.js"></script>-->
	<script src="assets/js/service_url.js"></script>
	<script src="assets/js/main.js"></script>
	<script src="assets/js/generatechart.js"></script>
	<script src="assets/js/genericHighcharts.js"></script>
    <script src="assets/js/charts.js"></script>
    <script src="assets/js/constants.js"></script>
	<script src="assets/js/jquery.slimscroll.min.js"></script>
	<script src="assets/js/jquery.bxslider.js"></script>
	<script src="assets/js/pie-chart.js" type="text/javascript"></script>
	<script>
		$('#newsModal .modal-body').slimScroll({
			height: '500px',
			color: '#000',
			size : '4px',
			alwaysVisible : true,
			wheelStep : 10,
			distance : '2px',
		});
		
		/* $(function() {
			alert(8)
			//On load calling Overall sentiment chat
			$('input[name="sentimentalDatefilter"]').daterangepicker({
				"autoApply": true,
			    "minDate": moment().subtract(29, 'days'),
			    "maxDate": moment(),
			    "opens":"left",
			    locale: { 
			        format: 'DD-MM-YYYY'
			      }
			});
			
		    var txtsentimentalFrom = $('.sentimentalFrom').data('daterangepicker');
	        var txtsentimentalTo = $('.sentimentalTo').data('daterangepicker');
	        
	        txtsentimentalFrom.setStartDate(moment().subtract(1, "days"));
	        txtsentimentalFrom.setEndDate(moment().subtract(1, "days"));
	        
	        txtsentimentalTo.setStartDate(moment());
	        txtsentimentalTo.setEndDate(moment());
	        
	      //On load calling Periodic snapshot
	        $('input[name="periodicDatefilter"]').daterangepicker({
				"autoApply": true,
			    "minDate": moment().subtract(29, 'days'),
			    "maxDate": moment(),
			    "opens":"left",
			    locale: { 
			        format: 'DD-MM-YYYY'
			      }
			});
	        
	        var txtperiodicPrevious = $('.periodicPrevious').data('daterangepicker');
	        var txtperiodicCurrent = $('.periodicCurrent').data('daterangepicker');
	        
	        txtperiodicPrevious.setStartDate(moment().subtract(1, "days"));
	        txtperiodicPrevious.setEndDate(moment().subtract(1, "days"));
	        
	        txtperiodicCurrent.setStartDate(moment());
	        txtperiodicCurrent.setEndDate(moment());
			
		});
		
		//Checking if selected source is GES, tehn reinitilize the date range to 2 days back.
		//This is being calling from master-ctrl for Overallsentiment kpi
	     function fn_overall_sentiment_onChange_date_Set(sentimentalSource){
			alert(sentimentalSource)
			var txtsentimentalFrom = $('.sentimentalFrom').data('daterangepicker');
		    var txtsentimentalTo = $('.sentimentalTo').data('daterangepicker');
        	 if(sentimentalSource == "5"){
		        txtsentimentalFrom.setStartDate(moment().subtract(3, "days"));
		        txtsentimentalFrom.setEndDate(moment().subtract(3, "days"));
		        
		        txtsentimentalTo.setStartDate(moment().subtract(2, "days"));
		        txtsentimentalTo.setEndDate(moment().subtract(2, "days"));
        	} else {
		        txtsentimentalFrom.setStartDate(moment().subtract(1, "days"));
		        txtsentimentalFrom.setEndDate(moment().subtract(1, "days"));
		        
		        txtsentimentalTo.setStartDate(moment());
		        txtsentimentalTo.setEndDate(moment());
        	} 
		}
		
	   //Checking if selected source is GES, tehn reinitilize the date range to 2 days back.
	   //This is being calling from Analytics-ctrl for periodic snapshot
	     function fn_periodic_onChange_date_Set(periodicSource,clickIdentifier){
			alert(periodicSource)
			var txtperiodicPrevious = $('.periodicPrevious').data('daterangepicker');
	        var txtperiodicCurrent = $('.periodicCurrent').data('daterangepicker');
	        
        	 if(clickIdentifier == 'sourceClicked'){
        		 if(periodicSource == "5"){
        			 txtperiodicPrevious.setStartDate(moment().subtract(3, "days"));
        			 txtperiodicPrevious.setEndDate(moment().subtract(3, "days"));
     		        
        			 txtperiodicCurrent.setStartDate(moment().subtract(2, "days"));
        			 txtperiodicCurrent.setEndDate(moment().subtract(2, "days"));
             	} else {
             		txtperiodicPrevious.setStartDate(moment().subtract(1, "days"));
             		txtperiodicPrevious.setEndDate(moment().subtract(1, "days"));
     		        
     		        txtperiodicCurrent.setStartDate(moment());
     		        txtperiodicCurrent.setEndDate(moment());
             	}
	        }
		}
	   
	   //This is being calling from review-ctrl for periodic snapshot
	     function fn_periodic_onChange_date_Set_review_analysis(periodicSource,clickIdentifier){
			alert(periodicSource)
			var txtperiodicPrevious = $('.periodicPrevious').data('daterangepicker');
	        var txtperiodicCurrent = $('.periodicCurrent').data('daterangepicker');
	        
        	 if(clickIdentifier == 'sourceClicked'){
        		 if(periodicSource == "5"){
        			 txtperiodicPrevious.setStartDate(moment().subtract(3, "days"));
        			 txtperiodicPrevious.setEndDate(moment().subtract(3, "days"));
     		        
        			 txtperiodicCurrent.setStartDate(moment().subtract(2, "days"));
        			 txtperiodicCurrent.setEndDate(moment().subtract(2, "days"));
             	} else {
             		txtperiodicPrevious.setStartDate(moment().subtract(1, "days"));
             		txtperiodicPrevious.setEndDate(moment().subtract(1, "days"));
     		        
     		        txtperiodicCurrent.setStartDate(moment());
     		        txtperiodicCurrent.setEndDate(moment());
             	}
	        }
		} */
		
		window.onload = function() {
			setTimeout(function(){ 
				
				$('#kpiMentions').pieChart({
		            barColor: '#ff9b47',
		            trackColor: '#dfdfdf',
		            lineCap: 'butt',
		            lineWidth: 20,
		            size: 250,
		            onStep: function (from, to, percent) {
		                $(this.element).find('.pie-value').text(Math.round(percent) + '%');
		            }
		        });
				
				$('#campKpiPosts').pieChart({
		            barColor: '#00aeef',
		            trackColor: '#dfdfdf',
		            lineCap: 'butt',
		            lineWidth: 20,
		            size: 250,
		            onStep: function (from, to, percent) {
		                $(this.element).find('.pie-value').text(Math.round(percent) + '%');
		            }
		        });
			
			}, 3000);	
			  
		};
	</script>
	</html>