<!DOCTYPE html>
<html lang="en-US" class="no-js">
	<head>
		<title>Welcome | XpressoInsights</title>
		<%
			String loginUrl = request.getContextPath() + "/services/user/login";
			String invalid = (String) request.getParameter("wrong");
			String valid = " ";
			String reset = (String) request.getParameter("resetpassword");
			if (reset == null)
				reset = "";
			%>
		<!-- Main_meta_&_mobile_meta -->
		<meta charset="utf-8">
		<meta name="description" content="">
		<meta name="HandheldFriendly" content="true" />
		<meta name="apple-touch-fullscreen" content="yes" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta name="viewport"
			content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<link rel="shortcut icon" href="assets/img/favicon.png" type="image/png"/>
		<!-- favicon -->
		<!-- favicon -->
		<link rel="stylesheet" type="text/css"
			href="assets/css/font-awesome.css">
		<!-- font awesome css -->
		<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
		<!-- bootstrap css -->
		<link rel="stylesheet" type="text/css"
			href="assets/css/bootstrap-theme.css">
		<!-- bootstrap supporting css -->
		<link rel="stylesheet" type="text/css" href="assets/css/style.css">
		<!-- common css -->
		<link rel="stylesheet" type="text/css" href="assets/css/style-forms.css">
		<script src="https://cdn.linearicons.com/free/1.0.0/svgembedder.min.js"></script>
		<!-- common css -->
		<!--[if lt IE 9]>
		<script type="text/javascript" src="assets/js/html5shiv.js"></script>
		<script type="text/javascript" src="assets/js/respond.min.js"></script>
		<![endif]-->

</head>
<body>
<input type="hidden" name="authData" id="reset" value="<%=reset%>" />
<!--<section class="main-body-withoutmenu">
  <div class="col-md-12">
    <div class="logo-wrapper">
    	<img src="assets/img/logo1.png" alt="" class="login-logo" />
    </div>
  </div>
</section>-->
<section class="main-body-withoutmenu">
  <div class="col-md-5 col-sm-10 loginform-wrapper">
    <div class="gradiant-line"></div>
    <div class="block-cover">
		<div class="logo-wrapper">
			<img src="assets/img/logo_new.png" alt="" class="login-logo" />
		</div>
      <!--<div class="block-head">
        <h3>Login</h3>
      </div>-->
      <div class="block-body">
        <div class="row">
          <div class="col-md-12">
            <form action=<%=loginUrl%> method="POST">
              <div class="row margin-bottom-0">
                <% if (invalid != null) { %>
                	<div class="error-message"><% out.print(invalid); %></div>
                <% } %>
              </div>
              <div class="form-group row">
               <!-- <div class="col-md-2 col-md-offset-2 col-sm-12">
                  <label class="mand">User Id</label>
                </div>-->
                <div class="col-md-12 col-sm-12 icon-inner-addon right-addon">
					<i class="fa fa-user" style="position: absolute;"></i>
					<input type="text" name="email" class="form-control" id="" placeholder="Your user id">
                </div>
              </div>
              <div class="form-group row">
                <!--<div class="col-md-2 col-md-offset-2 col-sm-12">
                  <label class="mand">Password</label>
                </div>-->
                <div class="col-md-12 col-sm-12 icon-inner-addon right-addon">
					<i class="fa fa-lock" style="position: absolute;"></i>
                  <input type="password" name="password" class="form-control" id="" placeholder="Your password">
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-12 col-sm-12">
                  <button type="submit" class="col-md-12 col-sm-12 btn btn-secondary">Login<svg class="lnr lnr-download"><use xlink:href="#lnr-download"></use></svg></button>
                
                </div>
              </div>
            </form>
			<div class="form-group row">
				<div class="col-md-6 col-sm-6">
					
                </div>
				<div class="col-md-6 col-sm-6 text-right">
					<!-- <a href="forgotpassword.jsp" class="forget-password">Forgot Password?</a> -->
					<div></div>
                </div>
			</div>
			<!--  <div class="form-group row border-top">
				<div class="col-md-6 col-sm-6 padding-top-6">
					<span class="first-time-user">First time here?</span>
                </div>
				<div class="col-md-6 col-sm-6 text-right">
					<a href="registration.jsp" class="btn registration">Register</a>
					<div></div>
                </div>
			</div>  -->
			<div class="row padding-top">
				<div class="col-md-12 col-sm-12 padding-top-12 text-center">
					<span class="powered-by"><img src="assets/img/xpresso-logo1.png"/></span>
                </div>
			</div>
		</div>
		<script data-require="angular.js@*" data-semver="1.2.5" src="assets/js/angular.min.js"></script> 
		<script type="text/javascript" src="assets/js/jquery.min.js"></script> 
		<!-- Main_lib_script --> 
		<script type="text/javascript" src="assets/js/bootstrap.js"></script> 
		<!-- bootstrap js --> 
		<script>
			$(function() {
				$(window).load(function() {
					var reset1 = $('#reset').val().trim();
					if (reset1 !== "") {
						$('#resetModal').modal('show');
					} else {
						$('#resetModal').modal('hide');
					}
			
				});
			});
		</script>
	</body>
</html>