<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en-US" class="no-js">
	<!--<![endif]-->
	<head>
		<!-- title -->
		<title>Omni-Channel Customer Experience Management Suite | XpressoInsights</title>
		<!-- Main_meta_&_mobile_meta -->
		<meta charset="utf-8">
		<meta name="description" content="">
		<meta name="HandheldFriendly" content="true" />
		<meta name="apple-touch-fullscreen" content="yes" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<link rel="shortcut icon" href="<%=request.getContextPath()%>/assets/img/#.png"/>
		<link rel="shortcut icon" href="assets/img/favicon.png" type="image/png"/>
		<!-- favicon -->
		<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/assets/css/font-awesome.css">
		<!-- font awesome css -->
		<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/assets/css/bootstrap.css">
		<!-- bootstrap css -->
		<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/assets/css/bootstrap-theme.css">
		<!-- bootstrap supporting css -->
		<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/assets/css/style.css">
		<!-- common css -->
		<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/assets/css/style-forms.css">
		<!-- common css -->
		<!--[if lt IE 9]>
		<script type="text/javascript" src="assets/js/html5shiv.js"></script>
		<script type="text/javascript" src="assets/js/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<!--<section class="main-body-withoutmenu">
			<div class="col-md-12">
				<div class="logo-wrapper">
					<img src="<%=request.getContextPath()%>/assets/img/logo1.png" alt="RadianCX" class="login-logo"/>
					<h3 class="login-logotxt">Unified Customer Experience Management Suite</h3>
				</div>
			</div>
		</section>-->
		<section class="main-body-withoutmenu">
			<div class="col-md-7 loginform-wrapper activeprofile">
				<div class="gradiant-line"></div>
				<div class="block-cover">
					<div class="logo-wrapper">
						<img src="assets/img/logo_new.png" alt="" class="login-logo" />
					</div>
					<div class="block-body">
						<div class="error-page" style="margin-bottom: 0;">
							<div class="row">
								<div class="col-md-12">
									<div class="sadface"><img src="<%=request.getContextPath()%>/assets/img/sadface.png" alt="" /></div>
								</div>
							</div>
							<div class="row">
								<!--               <div class="col-md-10 col-md-offset-1">
									<div class="warning-ico"><img src="assets/img/warning_ico.png" alt="" /></div>
									<div class="error-text">
									    <h4>OOPS</h4>
									    <p>This server is experiencing technical problems. Please try again in a few moments. Thanks for your continued patience, and we're sorry for any inconvenience this may cause.</p>
									</div> -->
								<div class="col-md-12">
									<div class="error-con">
										<div class="warning-ico"><img src="<%=request.getContextPath()%>/assets/img/warning_ico.png" alt="" /></div>
										<div class="error-text">
											<h4>OOPS</h4>
											<p>This server is experiencing technical problems. Please try again in a few moments. Thanks for your continued patience, and we're sorry for any inconvenience this may cause.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<script data-require="angular.js@*" data-semver="1.2.5" src="<%=request.getContextPath()%>/assets/js/angular.js"></script> 
		<script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/jquery.min.js"></script><!-- Main_lib_script --> 
		<script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/bootstrap.js"></script><!-- bootstrap js -->
		<script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/validator.js"></script>
	</body>
</html>