<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%-- <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> --%>
<%-- <%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>  --%>  

<%@page import="java.io.*"%>
<%-- <%@ page import="java.sql.*" %> --%>
<%@page import="java.util.*"%>
<!DOCTYPE html>
<!--[if IE 7]> <html lang="en-US" class="no-js IE7 IE"> <![endif]-->
<!--[if IE 8]> <html lang="en-US" class="no-js IE8 IE"> <![endif]-->
<!--[if IE 9]> <html lang="en-US" class="no-js IE9 IE"> <![endif]-->
<!--[if gt IE 9]><!--><html lang="en-US" class="no-js"><!--<![endif]-->
	<head>
		<!-- title -->
		<title>Omni-Channel Customer Experience Management Suite | XpressoInsights</title>

		<!-- Main_meta_&_mobile_meta -->
		<meta charset="utf-8">
		<meta name="description" content="">
		<meta name="HandheldFriendly" content="true" />
		<meta name="apple-touch-fullscreen" content="yes" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!--  link rel="shortcut icon" href="assets/img/#.png"/--><!-- favicon -->
		<link rel="shortcut icon" href="assets/img/favicon.png" type="image/png"/><!-- favicon -->
		
		<link rel="stylesheet" type="text/css" href="assets/css/font-awesome.css"><!-- font awesome css -->
		<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css"><!-- bootstrap css -->
		<link rel="stylesheet" type="text/css" href="assets/css/bootstrap-theme.css"><!-- bootstrap supporting css -->
		<link rel="stylesheet" type="text/css" href="assets/css/bootstrap-select.css"><!-- bootstrap selectbox -->
        <link rel="stylesheet" type="text/css" href="assets/css/datetimepicker.css">
        <link rel="stylesheet" type="text/css" href="assets/css/datetimepicker.css.map">
        
		<link rel="stylesheet/less" type="text/css" href="assets/build/build_standalone.less" />
		<link rel="stylesheet" type="text/css" href="assets/css/style-setting.css">
		<link rel="stylesheet" type="text/css" href="assets/css/style-forms.css"><!-- common css -->
		<link rel="stylesheet" type="text/css" href="assets/css/jquery.bxslider.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.13/daterangepicker.min.css" />
		
		<link rel="stylesheet" type="text/css" href="assets/css/style.css"><!-- common css -->
		<link rel="stylesheet" type="text/css" href="assets/css/theme/purple.css"><!-- theme css -->
		
		<link rel="stylesheet" type="text/css" href="assets/css/univ-pagination.css"><!-- common css -->
		
    	<!--[if lt IE 9]>
			<script type="text/javascript" src="assets/js/html5shiv.js"></script>
			<script type="text/javascript" src="assets/js/respond.min.js"></script>
		<![endif]-->

	</head>
