<!-- Page to be shown while dashboard is getting generated -->

<div class="pre-active">
	<div class="pre-active-text2">Your Dashboard is getting created.</div>
	<div class="hourglass"><img src="assets/img/hourglass.png" alt=""></div>
    <div class="pre-active-text1">Please come back after 3 hours to view the dashboard.</div>
</div>