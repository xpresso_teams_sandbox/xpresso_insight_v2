
<div class="row" id="dashboard" >
	<div class="col-md-12">
		<div id="page1" style="background:#fff;">
			<div class="page-title">
				<h2 class="pull-left">Dashboard</h2>
				<!-- <h2 onclick="generatePdfForsectionAll('all')" class="pull-right hide-for-pdf" title="Generate Dashboard PDF" ><i class="fa fa-file-pdf-o "></i></h2> -->
				<div class="clearfix"></div>
			</div>
			<!-- filter -->
			<div class="block-cover">
				<div class="block-body">
					<div class="row mt-2 mb-2">
						<div class="col-md-12 mt-2 mb-3">
							<div class="row">
								<div class="col-md-12 inner-filter">
									<div class="inner-filter-col">
										<div class="inner-filter-lbl">Select Source</div>
										<select data-ng-model="selectedDashboardSource" data-ng-options="source[1] as source[0] for source in dashboardSources"
										    ng-change="fn_dashboard_source_onChange_dateSet()" class="selectpicker nbt-select-new pull-left" id="source">
											<option style="display: none;" value="">Source</option>
										</select>
									</div>
									<!-- Customer date picker  -->
									<div class="dropdown custom-datepicker">
										<div class="inner-filter-lbl">Start Duration</div>
	
										<a class="dropdown-toggle" role="button" data-toggle="dropdown" data-target="_self" href="">
											<div class="input-group">
												<input type="text" class="form-control" data-ng-model="dashboardFilteredStartDate" placeholder="From" id="dashboardFilteredStartDate"
												    readonly>
												<span class="input-group-addon">
													<i class="glyphicon glyphicon-calendar"></i>
												</span>
											</div>
										</a>
										<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
											<datetimepicker data-ng-model="dashboardFilteredStartDate" data-before-render="dateRangeValidation($view, $dates, $leftDate, $upDate, $rightDate)"
											    data-datetimepicker-config="{ dropdownSelector: '#dashboardFilteredStartDate', modelType: 'DD-MM-YYYY'+' 00:01', minView: 'day' }"
											/>
										</ul>
										<span class="validation-message" ng-show="dashboardFilteredStartDate=='' && dashboarFilteredEndDate!=''">*Required</span>
									</div>
									<!-- Customer date picker  -->
									<div class="dropdown custom-datepicker">
										<div class="inner-filter-lbl">End Duration</div>
										<a class="dropdown-toggle" role="button" data-toggle="dropdown" data-target="_self" href="">
											<div class="input-group">
												<input type="text" class="form-control" data-ng-model="dashboarFilteredEndDate" placeholder="To" id="dashboarFilteredEndDate"
												    readonly>
												<span class="input-group-addon">
													<i class="glyphicon glyphicon-calendar"></i>
												</span>
											</div>
										</a>
										<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
											<datetimepicker data-ng-model="dashboarFilteredEndDate" data-before-render="dateRangeValidation($view, $dates, $leftDate, $upDate, $rightDate)"
											    data-datetimepicker-config="{ dropdownSelector: '#dashboarFilteredEndDate', modelType: 'DD-MM-YYYY'+' 23:59', minView: 'day' }"
											/>
										</ul>
										<span class="validation-message" ng-show="dashboarFilteredEndDate=='' && dashboardFilteredStartDate!=''">*Required</span>
										<%-- <span class="validation-message" ng-show="dashboarFilteredEndDate < dashboardFilteredStartDate">*End date is more</span> --%>
									</div>
									<button class="btn primary-btn go-btn mt-4" aria-hidden="true"
										type="button"
										ng-click="fn_dashboard_kpi_filter()">
	
										<span aria-hidden="true" class="fa fa-arrow-right"></span> Go
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- filter -->
			<!-- 		Overall sentiment analysis -->
			
			<div class="block-cover" id="overallSentimentAnalysisChartDiv" style="height: auto">
				<div class="block-head">
					<div class="row">
						<div class="col-md-12">
							<h3 class="block-name pull-left">Overall sentiment analysis by media type
							</h3>
							<h3 onclick="generatePdfForsection('overallSentimentAnalysisChartDiv','Overall sentiment analysis by media type')" class="block-name pull-right  hide-for-pdf" title="Generate Overall Sentiment Analysis by Media Type PDF" ><i class="fa fa-file-pdf-o "></i></h3>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
				<div class="block-body">
					<div id="overallSentiment" style="min-width: 310px; height: 400px; margin: 0 auto"></div> 
					<div id="overallSentimentLine" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
				</div>
			</div>
	
			<!-- 		sentiment clouds -->
			</div>
			<div id="page2">
			<div class="row">
	
				<!-- <div class="col-md-12" id="Word_Cloud_on_Feedbac"> -->
				<div class="col-md-12" id="wordCloudChartDiv">
					<div class="block-cover">
						<div class="block-head">
							<div class="row">
								<div class="col-md-12">
									<h3 class="block-name left">Word Cloud on Feedback</h3>
									<!-- <span style="float:right;" class="Word_Cloud_on_Feedbac">Download</span> -->
									<h3 onclick="generatePdfForsection('wordCloudChartDiv','Word Cloud on Feedback')" class="block-name pull-right hide-for-pdf" title="Generate Word Cloud on Feedback PDF"><i class="fa fa-file-pdf-o "></i></h3>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
						<div class="block-body pt-3">
							<div class="row">
								<div class="col-md-4">
									<div class="block-cover">
										<div class="block-head">
											<div class="row">
												<div class="col-md-12">
													<h3 class="block-name">Overall Word Cloud</h3>
												</div>
											</div>
										</div>
										<div class="block-body">
										<!-- <div id="wordcloud" class="text-center margin-left-15 border"></div> -->
											<div id="overallWordCloud" style="height: 400px; margin: 0 auto"> 
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="block-cover">
										<div class="block-head">
											<div class="row">
												<div class="col-md-12">
													<h3 class="block-name">Positive Word Cloud</h3>
												</div>
											</div>
										</div>
										<div class="block-body">
											<div id="positiveWordCloud" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="block-cover">
										<div class="block-head">
											<div class="row">
												<div class="col-md-12">
													<h3 class="block-name">Negative Word Cloud</h3>
												</div>
											</div>
										</div>
										<div class="block-body">
											<div id="negativeWordCloud" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div style="height:40px;margin:35px;padding:35px; display:none;" class="extra-height" id="extra-height_before_pie">&nbsp;</div>
		</div>
		
		<!--  pie -->
		<div id="page3">
			<div style="height:70px;margin:15px;padding:15px; display:none;" class="extra-height" >&nbsp;</div>
			<div class="block-cover" id="contextWiseFeedbackPieChartDiv">
				<div class="block-head">
					<div class="row">
						<div class="col-md-12">
							<h3 class="block-name pull-left">Context Wise Feedback Distribution</h3>
							<h3 onclick="generatePdfForsection('contextWiseFeedbackPieChartDiv','Context Wise Feedback Distribution')" class="block-name pull-right hide-for-pdf" title="Generate Context Wise Feedback Distribution PDF"><i class="fa fa-file-pdf-o "></i></h3>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
				<div class="block-body">
					<div class="row mt-2">
						<div class="col-md-8 col-md-push-2">
							<div id="aspectRatio" style="min-width: 310px;height:600px; margin: 0 auto"></div>
						</div>
					</div>
				</div>
			</div>
			<div style="height:70px;margin:15px;padding:15px; display:none;" class="extra-height" >&nbsp;</div>
		</div>
		
		<!--  Stack and comments -->
		<div id="page4">
			<div class="block-cover" id="verbatismChartDiv">
				<div class="block-head">	
					<div class="row">
						<div class="col-md-12">
							<h3 class="block-name pull-left">Verbatism across all sources and by CHAMPS context</h3>
							<!-- <h3 onclick="generatePdfForsection_special_handel('verbatismChartDiv','Verbatism across all sources and by CHAMPS context')" class="block-name pull-right hide-for-pdf" title="Generate Verbatism across all sources and by CHAMPS context PDF"><i class="fa fa-file-pdf-o"  ></i></h3> -->
							<h3 onclick="generatePdfForsection('verbatismChartDiv','Verbatism across all sources and by CHAMPS context')" class="block-name pull-right hide-for-pdf" title="Generate Verbatism across all sources and by CHAMPS context PDF"><i class="fa fa-file-pdf-o"  ></i></h3>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
				<div class="block-body pt-3">
				<div  ng-repeat="topicDiv in $parent.dashboardTopicsForDiv" id="verba_{{topicDiv[0].substr(0,5)}}" class="item" ng-if="topicDiv[0] != 'All Contexts'"  ng-init="limit= 100" id="multiChart_{{topicDiv[0].substr(0,5)}}">
					<div class="block-cover">
						<div class="block-head">
							<div class="row">
								<div class="col-md-12">
									<h3 class="block-name" ng-bind="topicDiv[0]"></h3>
								</div>
							</div>
						</div>
						<div class="block-body" >
	
							<div class="row pt-3 pb-3">
								<div class="col-md-7">
									<div class="row">
										<div class="col-md-8">
											<div id="{{topicDiv[0].substr(0,5)}}" style="min-width: 310px; height: 500px; margin: 0 auto">
											</div>
										</div>
	
										<div class="col-md-4">
											<ul class="aspect-list" id="entityText_{{topicDiv[0].substr(0,5)}}">
												<!-- <li class="negativeText">Cold</li>
												<li class="negativeText">Bucket chicken</li>
												<li class="positiveText">Fried chicken</li>
												<li class="positiveText">Hot</li>
												<li class="positiveText">Tasty</li> -->
	
											</ul>
										</div>
									</div>
								</div>
								<div class="col-md-5 feeds-block  feeds-block_{{topicDiv[0].substr(0,5)}}"  >
									<div class="social-feeds pr-3 " id="feedbackText_{{topicDiv[0].substr(0,5)}}">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div style="height:60px;margin:70px;padding:60px; display:none;" class="extra-height-indivisual" id="extra-height_{{topicDiv[0].substr(0,5)}}">&nbsp;</div>
					
					<!-- <div ng-if="$index == 1" style="height:200px;margin:100px;padding:5px; display:none;" class="extra-height" >&nbsp;</div> -->
					<div ng-if="$index == 1"  style="height:140px;margin:85px;padding:15px; display:none;" class="extra-height" >&nbsp;</div>
					<div ng-if="$index == 2"  style="height:70px;margin:15px;padding:15px; display:none;" class="extra-height" >&nbsp;</div>
					<!-- <div ng-if="$index == 3" style="height:300px;margin:130px;padding:40px; display:none;" class="extra-height" >&nbsp;</div> -->
					<div ng-if="$index == 3" style="height:200px;margin:130px;padding:40px; display:none;" class="extra-height" >&nbsp;</div>
					<div ng-if="$index == 4"  style="height:70px;margin:15px;padding:15px; display:none;" class="extra-height" >&nbsp;</div>
					<div ng-if="$index == 5" style="height:250px;margin:100px;padding:70px; display:none;" class="extra-height" >&nbsp;</div>
					<div ng-if="$index == 6"  style="height:70px;margin:15px;padding:15px; display:none;" class="extra-height" >&nbsp;</div>
					<!-- <div ng-if="$index == 7" style="height:319px;margin:100px;padding:85px; display:none;" class="extra-height" >&nbsp;</div> -->
					<!-- <div ng-if="$index == 1 || $index == 2 || $index == 3 || $index == 4 || $index == 6" style="height:5px;margin:5px;padding:5px; display:none;" class="extra-height-indivisual" >&nbsp;</div>
					<div ng-if="$index == 5" style="height:30px;margin:30px;padding:40px; display:none;" class="extra-height-indivisual" >&nbsp;</div>
					<div ng-if="$index == 7" style="height:200px;margin:200px;padding:157px; display:none;" class="extra-height-indivisual" >&nbsp;</div> -->
				</div>
				</div>
			</div>
		</div>
		<div id="page5">
			<div style="height:200px;margin:100px;padding:85px; display:none;" class="extra-height" >&nbsp;</div> 
			<div class="row">
				<div class="col-md-12" id="contextWiseSentimentCombinedChartDiv">
					<div class="block-cover">
						<div class="block-head">
							<div class="row">
								<div class="col-md-12">
									<h3 class="block-name pull-name">Context Wise Sentiment & Entity Distribution
									</h3>
									<h3 onclick="generatePdfForsection('contextWiseSentimentCombinedChartDiv','Context Wise Sentiment & Entity Distribution')" class="block-name pull-right hide-for-pdf" title="Generate Context Wise Sentiment & Entity Distribution PDF"><i class="fa fa-file-pdf-o"></i></h3>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
						<div class="block-body pt-3">
							<div id="divContext" class="row">
								<div class="col-md-4 item" ng-repeat="topicDiv in $parent.dashboardTopicsForDiv" ng-if="topicDiv[0] != 'All Contexts'" >
									<div class="block-cover">
										<div class="block-head">
											<div class="row">
												<div class="col-md-12">
													<h3 class="block-name" ng-bind="topicDiv[0]"></h3>
												</div>
											</div>
										</div>
										<div class="block-body">
											<div id="quality_{{topicDiv[1]}}" style="min-width: 310px; height: 350px; margin: 0 auto" class="contextBar"></div>
											<div id="qualityPie_{{topicDiv[1]}}" style="min-width: 310px; height: 300px; margin: 0 auto" class="mt-10 contextPie"></div>
											<div class="clearfix"></div>
										</div>
									</div>
									<div ng-if="$index == 6" style="height:20px;margin:20px;padding:10px; display:none;" class="extra-height-indivisual" >&nbsp;</div>
									<!-- <div ng-if="$index == 6" style="height:20px;margin:20px;padding:10px; display:none;" class="extra-height" >&nbsp;</div> -->
									<div ng-if="$index == 6" style="height:100px;margin:20px;padding:10px; display:none;" class="extra-height" >&nbsp;</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- <div class="row" style="height:300px;background:#fff;">
			<div class="col-md-12">
				hello
			</div>		
		</div> -->
		<!-- 	end -->
	</div>

</div>
