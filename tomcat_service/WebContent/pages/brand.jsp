<div id="brand">
	<div class="row">
		<div class="col-md-12">
			<div class="page-title">
				<h2>{{activeMenu}}</h2>
			</div>
		</div>
		<div class="col-md-12">
			<div class="block-cover">
				<div class="block-body">
					<div class="inner-filter mt-3 mb-3">
						<button class="btn black-btn go-btn pull-right" aria-hidden="true" type="button" ng-click="fn_populate_summary_matrics();fn_populate_competitive_comparison();">
							<span aria-hidden="true" class="fa fa-arrow-right"></span> Go
						</button>
						<select data-ng-model="selectedSentimentMatricsSource" data-ng-options="source[1] as source[0] for source in brandPageSources" class="pull-right selectpicker  mr-5" id="sentimentMatricsSources">
							<!-- <option style="display: none;" value="">Source</option> -->
						</select>
						<label class="pull-right mr-3">By Source:</label>
						<select data-ng-model="selectedBrandDuration" data-ng-options="duration[0] as duration[1] for duration in snapshotDurations" class="pull-right selectpicker mr-5" id="brandDuration">
							<!-- <option style="display: none;" value="">Source</option> -->
						</select>
						<label class="pull-right mr-3">By:</label>
						<i class="fa fa-filter pull-right mr-3" aria-hidden="true"></i>
						<div class="clearfix"></div>
					</div>
					
					<!-- Summary Metrics Block starts -->
					<div class="block-cover" id="summaryMetricsDiv">
						<div class="block-head">
							<div class="row">
								<div class="col-md-6">
									<h3 class="block-name">Summary Metrics</h3>
								</div>
							</div>
						</div>
						<div class="block-body">
							<div class="grey-bg">
								<table class="brand-table">
									<tbody>
										<tr>
											<td>
												<i ng-class="(summarymatrics.mentions_curr>=summarymatrics.mentions_prev)?'fa fa-caret-up bg-green metrics-icon':'fa fa-caret-down bg-red metrics-icon'" aria-hidden="true"></i>
												<div class="metrics-content">
													<h3 class="metrics-title">Mentions <span class="metrics-count" ng-bind="summarymatrics.mentions_curr+summarymatrics.mentions_prev"></span></h3>
													<div class="metrics-desp">{{summarymatrics.mentions_diff}}% {{(summarymatrics.mentions_curr>=summarymatrics.mentions_prev)?'more':'less'}} mentions this {{selectedDurationName}}</div>
												</div>
											</td>
											<td>
												<div class="metrics-bar-data" >
													<div class="bar-lt bg-green" title="Total mention count for this {{selectedDurationName}} is  {{summarymatrics.mentions_curr}}" style="width:{{((summarymatrics.mentions_curr/(summarymatrics.mentions_curr+summarymatrics.mentions_prev))*100)}}%" title="Any Title Green"></div>
													<div class="bar-rt bg-orange" title="Total mention count for last {{selectedDurationName}} is  {{summarymatrics.mentions_prev}}" style="width:{{((summarymatrics.mentions_prev/(summarymatrics.mentions_curr+summarymatrics.mentions_prev))*100)}}%" title="Any Title Red"></div>
												</div>
												<div class="row graph-con">
													<div class="col-md-6">This {{selectedDurationName}}	<span class="ml-3" ng-bind="summarymatrics.mentions_curr"></span></div>
													<div class="col-md-6 text-right">Last {{selectedDurationName}}	<span class="ml-3" ng-bind="summarymatrics.mentions_prev"></span></div>
												</div>
											</td>
										</tr>
										<tr>
											<td>
												<!-- <i ng-class="(summarymatrics.mentions_curr>=summarymatrics.mentions_prev)?'fa fa-caret-up bg-green metrics-icon':'fa fa-caret-down bg-red metrics-icon'"  aria-hidden="true"></i> -->
												<i class="fa fa-caret-up bg-green metrics-icon"></i>
												<div class="metrics-content">
													<h3 class="metrics-title">Sentiments <span class="metrics-count" ng-bind="summarymatrics.Positive_curr+summarymatrics.Negative_curr+summarymatrics.Neutral_curr"></span></h3>
													<div class="metrics-desp">{{summarymatrics.Sentiment_diff}}% more sentiment this {{selectedDurationName}}</div>
												</div>
											</td>
											<td>
												<div class="metrics-bar-data">
													<div class="bar-lt bg-green" title="Positive sentiment count is {{summarymatrics.Positive_curr}}" style="width:{{((summarymatrics.Positive_curr/(summarymatrics.Positive_curr+summarymatrics.Negative_curr+summarymatrics.Neutral_curr))*100)}}%"></div>
													<div class="bar bg-red" title="Negative sentiment count is {{summarymatrics.Negative_curr}}" style="width:{{((summarymatrics.Negative_curr/(summarymatrics.Positive_curr+summarymatrics.Negative_curr+summarymatrics.Neutral_curr))*100)}}%"></div>
													<div class="bar-rt bg-orange" title="Neutral sentiment count is {{summarymatrics.Neutral_curr}}" style="width:{{((summarymatrics.Neutral_curr/(summarymatrics.Positive_curr+summarymatrics.Negative_curr+summarymatrics.Neutral_curr))*100)}}%"></div>
												</div>
												<div class="row graph-con">
													<div class="col-md-4">Positive <i class="fa fa-caret-down txt-red ml-4 mr-4" aria-hidden="true"></i> <span ng-bind="summarymatrics.Positive_curr"></span></div>
													<div class="col-md-4 text-center">Negative <i class="fa fa-caret-up txt-green ml-4 mr-4" aria-hidden="true"></i><span ng-bind="summarymatrics.Negative_curr"></span></div>
													<div class="col-md-4 text-right">Neutral <i class="fa fa-caret-up txt-green ml-4 mr-4" aria-hidden="true"></i><span ng-bind="summarymatrics.Neutral_curr"></span></div>
												</div>
											</td>
										</tr>
										<tr>
											<td>
												<i ng-class="(summarymatrics.users_curr>=summarymatrics.users_prev)?'fa fa-caret-up bg-green metrics-icon':'fa fa-caret-down bg-red metrics-icon'"  aria-hidden="true"></i>
												<div class="metrics-content">
													<h3 class="metrics-title">Users <span class="metrics-count" ng-bind="summarymatrics.users_curr+summarymatrics.users_prev"></span></h3>
													<div class="metrics-desp">{{summarymatrics.users_diff}}%  {{(summarymatrics.users_curr>=summarymatrics.users_prev)?'more':'less'}} users this {{selectedDurationName}}</div>
												</div>
											</td>
											<td>
												<div class="metrics-bar-data">
													<div class="bar-lt bg-green" title="User count this {{selectedDurationName}} is {{summarymatrics.users_curr}}" style="width:{{((summarymatrics.users_curr/(summarymatrics.users_curr+summarymatrics.users_prev))*100)}}%"></div>
													<div class="bar-rt bg-orange" title="User count last {{selectedDurationName}} is {{summarymatrics.users_prev}}" style="width:{{((summarymatrics.users_prev/(summarymatrics.users_curr+summarymatrics.users_prev))*100)}}%"></div>
												</div>
												<div class="row graph-con">
													<div class="col-md-6">This {{selectedDurationName}}	<span class="ml-3" ng-bind="summarymatrics.users_curr"></span></div>
													<div class="col-md-6 text-right">Last {{selectedDurationName}}	<span class="ml-3" ng-bind="summarymatrics.users_prev"></span></div>
												</div>
											</td>
										</tr>
										<tr>
											<td>
												<i ng-class="(summarymatrics.engagements_curr>=summarymatrics.engagements_prev)?'fa fa-caret-up bg-green metrics-icon':'fa fa-caret-down bg-red metrics-icon'" aria-hidden="true"></i>
												<div class="metrics-content">
													<h3 class="metrics-title">Engagements <span class="metrics-count" ng-bind="summarymatrics.engagements_curr+summarymatrics.engagements_prev"></span></h3>
													<div class="metrics-desp">{{summarymatrics.engagements_diff}}% {{(summarymatrics.engagements_curr>=summarymatrics.engagements_prev)?'more':'less'}} engagements this {{selectedDurationName}}</div>
												</div>
											</td>
											<td>
												<div class="metrics-bar-data">
													<div class="bar-lt bg-green" title="Total engagement count this {{selectedDurationName}} is {{summarymatrics.engagements_curr}}" style="width:{{((summarymatrics.engagements_curr/(summarymatrics.engagements_curr+summarymatrics.engagements_prev))*100)}}%"></div>
													<div class="bar-rt bg-orange" title="Total engagement count last {{selectedDurationName}} is {{summarymatrics.engagements_prev}}" style="width:{{((summarymatrics.engagements_prev/(summarymatrics.engagements_curr+summarymatrics.engagements_prev))*100)}}%"></div>
												</div>
												<div class="row graph-con">
													<div class="col-md-6">This {{selectedDurationName}}	<span class="ml-3" ng-bind="summarymatrics.engagements_curr"></span></div>
													<div class="col-md-6 text-right">Last {{selectedDurationName}}	<span class="ml-3" ng-bind="summarymatrics.engagements_prev"></span></div>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- Summary Metrics Block ends -->
					
					<!-- Competitive Comparison Block starts -->
					<div class="block-cover" id="competitiveComparison">
						<div class="block-head">
							<div class="row">
								<div class="col-md-6">
									<h3 class="block-name">Competitive Comparison</h3>
								</div>
								<div class="col-md-6">
									<div class="pull-right bar-filter">
										<!-- <select data-ng-model="selectedCompetitiveComparisonSource" data-ng-options="source[1] as source[0] for source in brandPageSources" class="selectpicker ml-1" id="competitiveComparisonSources" ng-change="fn_populate_competitive_comparison();">
											
										</select> -->
										<!-- <label class="pull-right ml-3">By Source:</label> -->
									</div>
								</div>
							</div>
						</div>
						<div class="block-body">
							<div class="grey-bg">
								<div class="table-head">
									<div>Brands</div>
									<div>Sentiments</div>
									<div>Mentions</div>
									<div>Engagements</div>
								</div>
								<table class="brand-table">
									<tbody>
										<tr data-ng-repeat="competitivecomparisondata in competitivecomparison">
											<td>
												<div class="source-logo cc-{{competitivecomparisondata[16]}}"></div>
												<div class="store-name" ng-bind="competitivecomparisondata[1]"><!-- KFC-india --></div>
											</td>
											<td>
												<div class="mr-6">
													<div class="metrics-bar-data">
														<div class="bar-lt bg-green" title="Positive sentiment count for {{competitivecomparisondata[1]}} is {{competitivecomparisondata[8]}}" style="width:{{((competitivecomparisondata[8]/(competitivecomparisondata[8]+competitivecomparisondata[9]+competitivecomparisondata[10]))*100)}}%"></div>
														<div class="bar bg-red" title="Negative sentiment count for {{competitivecomparisondata[1]}} is {{competitivecomparisondata[9]}}" style="width:{{((competitivecomparisondata[9]/(competitivecomparisondata[8]+competitivecomparisondata[9]+competitivecomparisondata[10]))*100)}}%"></div>
														<div class="bar-rt bg-orange" title="Neutral sentiment count for {{competitivecomparisondata[1]}} is {{competitivecomparisondata[10]}}" style="width:{{((competitivecomparisondata[10]/(competitivecomparisondata[8]+competitivecomparisondata[9]+competitivecomparisondata[10]))*100)}}%"></div>
													</div>
													<div class="row graph-con">
														<div class="col-md-12 txt-{{(competitivecomparisondata[13] >= 1)?'green':'red'}}"><i class="fa {{(competitivecomparisondata[13] >= 1)?'fa-caret-up':'fa-caret-down'}} mr-4" aria-hidden="true"></i>  {{competitivecomparisondata[13]}}% {{(competitivecomparisondata[13] >= 1)?'more':'less'}} than last {{selectedDurationName}}</div>
													</div>
												</div>
											</td>
											<td>
												<div class="mr-6">
													<div class="metrics-bar-data">
														<div class="bar-lt bg-green" title="Mention count for this {{selectedDurationName}} is {{competitivecomparisondata[4]}}" style="width:{{((competitivecomparisondata[4]/(competitivecomparisondata[4]+competitivecomparisondata[5]))*100)}}%"></div>
														<div class="bar-rt bg-orange" title="Mention count for last {{selectedDurationName}} is {{competitivecomparisondata[5]}}" style="width:{{((competitivecomparisondata[5]/(competitivecomparisondata[4]+competitivecomparisondata[5]))*100)}}%"></div>
													</div>
													<div class="row graph-con">
														<div class="col-md-12 txt-{{(competitivecomparisondata[11] >= 1)?'green':'red'}}"><i class="fa {{(competitivecomparisondata[11] >= 1)?'fa-caret-up':'fa-caret-down'}} mr-4" aria-hidden="true"></i>  {{competitivecomparisondata[11]}}% {{(competitivecomparisondata[4]>=competitivecomparisondata[5])?'more':'less'}} than last {{selectedDurationName}}</div>
													</div>
												</div>
											</td>
											<td>
												<div class="metrics-bar-data" >
													<div class="bar-lt bg-green" title="Engagement count for this {{selectedDurationName}} is {{competitivecomparisondata[6]}}" style="width:{{((competitivecomparisondata[6]/(competitivecomparisondata[6]+competitivecomparisondata[7]))*100)}}%"></div>
													<div class="bar-rt bg-orange" title="Engagement count for last {{selectedDurationName}} is {{competitivecomparisondata[7]}}" style="width:{{((competitivecomparisondata[7]/(competitivecomparisondata[6]+competitivecomparisondata[7]))*100)}}%"></div>
												</div>
												<div class="row graph-con">
													<!-- <div class="col-md-6">This {{selectedDurationName}}	<span class="ml-3" ng-bind="competitivecomparisondata[6]"></span></div>
													<div class="col-md-6 text-right">Last {{selectedDurationName}}	<span class="ml-3" ng-bind="competitivecomparisondata[7]"></span></div> -->
													<div class="col-md-12 txt-{{(competitivecomparisondata[12] >= 1)?'green':'red'}}"><i class="fa {{(competitivecomparisondata[12] >= 1)?'fa-caret-up':'fa-caret-down'}} mr-4" aria-hidden="true"></i>  {{competitivecomparisondata[12]}}% {{(competitivecomparisondata[12] >= 1)?'more':'less'}} than last {{selectedDurationName}}</div>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="graph-defn mb-3">Metric Definition : Mention - How much people discussing a topic in social media. Engegement - How many times a topic is engaged through like,share,comments etc. </div>	
						</div>
					</div>
					<!-- Competitive Comparison Block ends -->
				</div>
			</div>
			
			<!-- Brand Timeline Block starts -->
			<div class="block-cover" id="brandTimeline">
				<div class="block-head">
					<div class="row">
						<div class="col-md-6">
							<h3 class="block-name">Brand Timeline</h3>
						</div>
						<div class="col-md-6">
							<div class="pull-right bar-filter">
								<select data-ng-model="brandTimelineSource" data-ng-options="source[1] as source[0] for source in brandPageSources" class="selectpicker ml-1" id="brandTimelineSource" ng-change="fn_brand_timeline();">
									<!-- <option style="display: none;" value="">Source</option> -->
								</select>
								<label class="pull-right ml-3">By Source:</label>
							</div>
						</div>
					</div>
				</div>
				<div class="block-body">
					<div id="brandTimelineGraph" class="mt-10 mr-1"></div>
					<div class="graph-defn mb-3">Metric Definition : Mention - How much people discussing a topic in social media. Potential Impression - How many opportunities were there for user to view a topic. Net Sentiment - A score that expresses the ratio of positive to negative sentiment about a topic</div>
				</div>
			</div>
			<!-- Brand Timeline Block ends -->
			
			<div class="row" id="wordcloudDiv">
				<div class="col-md-12">
					<div class="block-cover">
						<div class="block-body">
							<div class="inner-filter mt-3 mb-3">
								<select data-ng-model="wordcloudSource" data-ng-options="source[1] as source[0] for source in brandPageSources" class="selectpicker pull-right" id="brandTimelineSource" ng-change="fn_generateWordcloudSentimentAttribute();fn_generateWordcloudTopHashTag();">
									<!-- <option style="display: none;" value="">Source</option> -->
								</select>
								<label class="pull-right mr-3">By Source:</label>
								<i class="fa fa-filter pull-right mr-3" aria-hidden="true"></i>
								<div class="clearfix"></div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<!-- Top Hashtags Block starts -->
									<div class="block-cover" id="topHashtags">
										<div class="block-head">
											<div class="row">
												<div class="col-md-6">
													<h3 class="block-name">Top Hashtags</h3>
												</div>
											</div>
										</div>
										<div class="block-body">
											<div ng-if="noDataCheckTopHashTag == ''"  class="wordcloudTopHasTag abz-world-cloud word-no-found"> No Data Found</div>
											<div ng-if="noDataCheckTopHashTag != ''" id="wordcloudTopHasTag" class="wordcloudTopHasTag abz-world-cloud"></div>
											<div class="graph-defn mb-3">Metric Definition : List of top hashtags based on conversations </div>
										</div>
									</div>
									<!-- Top Hashtags Block ends -->
								</div>
								<div class="col-md-6">
									<!-- Top Sentiment Attributes Block starts -->
									<div class="block-cover" id="topSentimentAttributes">
										<div class="block-head">
											<div class="row">
												<div class="col-md-6">
													<h3 class="block-name">Top Sentiment Attributes</h3>
												</div>
												<div class="col-md-6">
													<div class="pull-right bar-filter">
														<!-- <select data-ng-model=""  class="selectpicker ml-1" id="">
															<option value="{{city4[0]}}" ng-repeat="city4 in avgRatingCities">{{city4[1]}}</option>
														</select>
														<label class="pull-right ml-3">By Source:</label> -->
													</div>
												</div>
											</div>
										</div>
										<div class="block-body">
											<div ng-if="noDataCheckTopSentimentAttr == ''"  class="abz-world-cloud wordcloudtopsentimentattribute word-no-found"> No Data Found</div>
											<div ng-if="noDataCheckTopSentimentAttr != ''" id="wordcloudtopsentimentattribute" class="abz-world-cloud wordcloudtopsentimentattribute"></div>
											<div class="graph-defn mb-3">Metric Definition : List of top entities based on conversations</div>
										</div>
									</div>
									<!-- Top Sentiment Attributes Block ends -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Own Posts Block starts -->
			<div class="block-cover" id="ownPosts">
				<div class="block-head">
					<div class="row">
						<div class="col-md-6">
							<h3 class="block-name">KFC India Posts</h3>
						</div>
						<div class="col-md-6">
							<div class="pull-right bar-filter">
								<!-- <select data-ng-model=""  class="selectpicker ml-1" id="">
									<option value="{{city4[0]}}" ng-repeat="city4 in avgRatingCities">{{city4[1]}}</option>
								</select>
								<label class="pull-right ml-3">By Source:</label> -->
							</div>
						</div>
					</div>
				</div>
				<div class="block-body">
					<div class="row">
						<div class="col-md-6 mt-3 mb-3">
							<div class="user-social-post-wapper">
								<div class="user-post-list" ng-repeat="ownsocialpost in ownsocialposts">
									<div class="post-wapper">
										<div class="post-user-image">
											<img src="{{ownsocialpost[10]}}" ng-if="ownsocialpost[10] != null"  alt="{{ownsocialpost[3]}}"/>
											<img src="assets/img/no-img.jpg" ng-if="ownsocialpost[10] == null"  alt="{{ownsocialpost[3]}}"/>
										</div>
										<div class="post-desp-wapper">
											<div class="post-desp">
												<div class="user-name" ng-bind="ownsocialpost[3]"><!-- KFC --></div>
												<div class="post-time text-right">{{ownsocialpost[9]}} <i class="fa fa-facebook"></i></div>
											</div>
											<div class="post-content mb-3">
												<div ng-bind="ownsocialpost[4]"><!-- The highlight of the week! KFC Wednesday Special -12 for 300 offer! 12pc Boneless Chicken Strips & 4 dips only for Rs 300*! Walk-in/Call/Order: http://bit.ly/12for300 (Valid on Wednesday only) Tag your friends with whom you want to enjoy this delicious offer. *GST @ 18% extra --></div>
												<div class="post-pimg mt-3" ng-if="ownsocialpost[11]">
													<img src="{{ownsocialpost[11]}}" alt=""/>	
												</div>
											</div>
										</div>
									</div>
									<div class="review-wapper">
										<div>
											<div class="review-count" ng-bind="ownsocialpost[6]"></div>
											<div class="review-title">like</div>
										</div>
										<div>
											<div class="review-count" ng-bind="ownsocialpost[6]">62,871</div>
											<div class="review-title">Comment</div>
										</div>
										<div>
											<div class="review-count" ng-bind="ownsocialpost[7]"></div>
											<div class="review-title">Share</div>
										</div>
										<div>
											<div class="review-count" ng-bind="ownsocialpost[8]"></div>
											<div class="review-title">Engagements</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6 mt-3 mb-3">
							<div class="user-social-post-wapper">
								<div class="user-post-list" ng-repeat="ownsocialtweet in ownsocialtweets">
									<div class="post-wapper">
										<div class="post-user-image">
											<img src="{{ownsocialtweet[6]}}" ng-if="ownsocialtweet[6]!= null" alt="ownsocialtweet[0]"/>
											<img src="assets/img/no-img.jpg" ng-if="ownsocialtweet[6]== null" />
										</div>
										<div class="post-desp-wapper">
											<div class="post-desp">
												<div class="user-name" ng-bind="ownsocialtweet[0]"><!-- KFC --></div>
												<div class="post-time text-right">{{ownsocialtweet[5]}} <i class="fa fa-twitter"></i></div>
											</div>
											<div class="post-content mb-3">
												<div ng-bind="ownsocialtweet[1]"><!-- The highlight of the week! KFC Wednesday Special -12 for 300 offer! 12pc Boneless Chicken Strips & 4 dips only for Rs 300*! Walk-in/Call/Order: http://bit.ly/12for300 (Valid on Wednesday only) Tag your friends with whom you want to enjoy this delicious offer. *GST @ 18% extra --></div>
												<div class="post-pimg mt-3" ng-if="ownsocialtweet[7]">
												<img src="{{ownsocialtweet[7]}}" alt=""/>
												</div>	
											</div>
										</div>
									</div>
									<div class="review-wapper">
										<div>
											<div class="review-count" ng-bind="ownsocialtweet[2]"></div>
											<div class="review-title">Likes</div>
										</div>
										<div>
											<div class="review-count" ng-bind="ownsocialtweet[3]"></div>
											<div class="review-title">Retweet</div>
										</div>
										<div>
											<div class="review-count" ng-bind="ownsocialtweet[4]"></div>
											<div class="review-title">Engagement</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Own Posts Block ends -->
			<!-- User Block starts -->
			<div class="block-cover" id="userPosts">
				<div class="block-head">
					<div class="row">
						<div class="col-md-6">
							<h3 class="block-name">User Posts</h3>
						</div>
						<div class="col-md-6">
							<div class="pull-right bar-filter">
								<!-- <select data-ng-model=""  class="selectpicker ml-1" id="">
									<option value="{{city4[0]}}" ng-repeat="city4 in avgRatingCities">{{city4[1]}}</option>
								</select>
								<label class="pull-right ml-3">By Source:</label> -->
							</div>
						</div>
					</div>
				</div>
				<div class="block-body">
					<div class="row">
						<div class="col-md-6 mt-3 mb-3">
							<div class="user-social-post-wapper">
								<div class="user-post-list" ng-repeat="usersocialpost in usersocialposts">
									<div class="post-wapper">
										<div class="post-user-image">
											<img src="{{usersocialpost[7]}}" ng-if="usersocialpost[7]!= null" alt="{{usersocialpost[0]}}"/>
											<img src="assets/img/no-img.jpg" ng-if="usersocialpost[7]== null"  alt="{{usersocialpost[0]}}"/>
										</div>
										<div class="post-desp-wapper">
											<div class="post-desp">
												<div class="user-name" ng-if="usersocialpost[0] == null">****NO NAME****</div>
												<div class="user-name" ng-bind="usersocialpost[0]" ></div>
												<div class="post-time text-right">{{usersocialpost[6]}} <i class="fa fa-facebook"></i></div>
											</div>
											<div class="post-content mb-3">
												<div ng-bind="usersocialpost[1]"><!-- The highlight of the week! KFC Wednesday Special -12 for 300 offer! 12pc Boneless Chicken Strips & 4 dips only for Rs 300*! Walk-in/Call/Order: http://bit.ly/12for300 (Valid on Wednesday only) Tag your friends with whom you want to enjoy this delicious offer. *GST @ 18% extra --></div>
												<div class="post-pimg mt-3" ng-if="usersocialpost[8]">
													<img src="{{usersocialpost[8]}}" alt=""/>
												</div>	
											</div>
										</div>
									</div>
									<div class="review-wapper">
										<div>
											<div class="review-count" ng-bind="usersocialpost[2]"></div>
											<div class="review-title">likes</div>
										</div>
										<div>
											<div class="review-count" ng-bind="usersocialpost[3]"></div>
											<div class="review-title">Comment</div>
										</div>
										<div>
											<div class="review-count" ng-bind="usersocialpost[4]"></div>
											<div class="review-title">Share</div>
										</div>
										<div>
											<div class="review-count" ng-bind="usersocialpost[5]"></div>
											<div class="review-title">Engagement</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6 mt-3 mb-3">
							<div class="user-social-post-wapper">
								<div class="user-post-list" ng-repeat="usersocialtweet in usersocialtweets">
									<div class="post-wapper">
										<div class="post-user-image">
											<img src="{{usersocialtweet[6]}}" ng-if="usersocialtweet[6]!= null"  alt="{{usersocialtweet[0]}}"/>
											<img src="assets/img/no-img.jpg" ng-if="usersocialtweet[6]== null"  alt="{{usersocialtweet[0]}}"/>
										</div>
										<div class="post-desp-wapper">
											<div class="post-desp">
												<div class="user-name" ng-if="usersocialtweet[0] == null">****NO NAME****</div>
												<div class="user-name" ng-bind="usersocialtweet[0]"><!-- KFC --></div>
												<div class="post-time text-right">{{usersocialtweet[5]}} <i class="fa fa-twitter"></i></div>
											</div>
											<div class="post-content mb-3">
												<div ng-bind="usersocialtweet[1]"><!-- The highlight of the week! KFC Wednesday Special -12 for 300 offer! 12pc Boneless Chicken Strips & 4 dips only for Rs 300*! Walk-in/Call/Order: http://bit.ly/12for300 (Valid on Wednesday only) Tag your friends with whom you want to enjoy this delicious offer. *GST @ 18% extra --></div>
												<div class="post-pimg mt-3" ng-if="usersocialtweet[7]">
													<img src="{{usersocialtweet[7]}}" alt=""/>
												</div>	
											</div>
										</div>
									</div>
									<div class="review-wapper">
										<div>
											<div class="review-count" ng-bind="usersocialtweet[2]"></div>
											<div class="review-title">Like</div>
										</div>
										<div>
											<div class="review-count" ng-bind="usersocialtweet[3]"></div>
											<div class="review-title">Retweet</div>
										</div>
										<div>
											<div class="review-count" ng-bind="usersocialtweet[4]">62,871</div>
											<div class="review-title">Engagement</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- user Posts Block ends -->
		</div>
	</div>
</div>