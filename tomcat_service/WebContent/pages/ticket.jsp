<!-- ticket management page -->

<div class="row" id="ticket"> 
 <!--  <div class="col-md-12" id="navDiv4">
     <div class="block-cover" >
       <div class="block-head">
         <h3>Ticket Summary</h3>
       </div>
       <div class="block-body">
         <div class="row">
           <div class="col-md-3">
             <div id="ticketMang" style="height:300px;"></div>
             <h3 class="align-center">Ticket Status</h3>
           </div>
           <div class="col-md-3">
             <div id="ticketResTime" style="height:300px;"></div>
             <h3 class="align-center">Ticket Resolution Time</h3>
           </div>
           <div class="col-md-6">
             <div id="ticketRes" style="height:300px;"></div>
             <h3 class="align-center">Context Distribution of Tickets</h3>
           </div>
         </div>
       </div>
     </div>
 </div>-->
 
 <div class="col-md-12" id="navDiv6">
   <div class="block-cover">
     <div class="block-body padding-0 clearfix">
       <div class="block-inner-head col-md-12 col-sm-12 padding-15 padding-bottom-0 clearfix" style="border-top:0px;">
			<div class="col-md-10 col-sm-10 col-lg-10 first-panel">
				 <i class="fa fa-filter" aria-hidden="true"></i>
				 <select data-ng-model="selectedSource" data-ng-options="source[1] as source[0] for source in sources" class="selectpicker nbt-select-new" id="tkt-source">
					<option style="display: none;" value="">Source</option>
				 </select>
				 <select class="selectpicker nbt-select-new">
				   <option style="display: none;" value="">Status</option>
				   <option>Open</option>
				   <option>Closed</option>
				   <option>Reopen</option>
				 </select>
				 <select class="selectpicker nbt-select-new">
				   <option style="display: none;" value="">Priority</option>
				   <option>High</option>
				   <option>Medium</option>
				   <option>Low</option>
				 </select>
				 <select data-ng-model="selectedTopic" data-ng-options="topic for topic in topics" class="selectpicker nbt-select-new" id="tkt-aspect">
					<option style="display: none;" value="">Context</option>
				 </select>
				 <span class="input-group-btn nbt-select-btn">
					<button class="btn btn-default" type="button"><span class="fa fa-arrow-right" aria-hidden="true"></span> &nbsp;Go</button>
				 </span> 
			 </div>
			 <div class="col-md-2 col-sm-2 col-lg-2 second-panel">
				 <span class="input-group-btn nbt-select-btn pull-right"> 
					<a href="javascript:void(0);" class="btn btn-default"><span class="fa fa-share-square-o" aria-hidden="true"></span> &nbsp;Export All</a>
				</span>
			</div>
		</div>
		<div class="col-md-12 col-sm-12 no-padding clearfix" style="overflow-x: auto;">
			<table class="table table-hover nbt-table" id="tblTicket">
				<thead>
					<tr>
						<th>
							<label class="nbt-checkbox">
								<input type="checkbox">
								<span></span>
							</label>
						</th>
						<th>Source</th>
						<th>Status</th>
						<th>Priority</th>
						<th>Assigned To</th>
						<th>Comment</th>
						<th>Context</th>
						<th>Due Date</th>
						<th>Closed Date</th>
						<th>Closed By</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody id="tableTicketManagement">
				</tbody>
			</table>
		</div>
       <div class="pull-right nbt-peger"></div>
     </div>
   </div>
 </div>
</div>
