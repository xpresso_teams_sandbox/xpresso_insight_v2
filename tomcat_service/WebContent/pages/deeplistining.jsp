<div id="page-deelistining">
	<div class="row pt-4">
		<div class="col-md-12">
			<div class="block-cover">
				<div class="block-head">
					<div class="row">
						<div class="col-md-12">
							<h3 class="block-name">Deep Listening</h3>
						</div>
					</div>
				</div>
				<div class="block-body">
					<div class="inner-filter mt-3 mb-5 context-filter-wapper">
						<i class="fa fa-filter pull-left" aria-hidden="true"></i> <select
							data-ng-model="selectedDeepListSource"
							data-ng-options="source[1] as source[0] for source in $parent.deepListSources"
							class="selectpicker pull-left" id="source">
							<option style="display: none;" value="">All Sources</option>
						</select>
						<div class="validation context-filter pull-left">
							<div ng-click="showContexts=!showContexts">
								Contexts <i class="fa fa-caret-down pull-right"></i>
							</div>
							<div class="contexts-listing" ng-show="showContexts">
								<div ng-repeat="topic in $parent.deepListTopics" class="item">
									<input type="checkbox" ng-model="topic[2]"
										ng-change="sync(topic[2], topic,'explore')"
										ng-checked="topic[2]"> <span ng-bind="topic[0]"></span>
								</div>
							</div>
							<span class="validation-message" ng-show="!topic[0]"
								style="display: block; position: absolute;">*Required</span>
						</div>
						<select class="selectpicker pull-left" id="sentiment"
							ng-model="selectedDeepListSentiment">
							<!-- <option style="display: none;" value="">Sentiment</option>
							<option value="All">All Sentiment</option> -->
							<option value="100">All Sentiment</option>
							<option value="1">Positive</option>
							<option value="-1">Negative</option>
							<option value="0">Neutral</option>
						</select>
						<div class="dropdown pull-left">
							<a class="dropdown-toggle" id="analyticsFrom" role="button"
								data-toggle="dropdown" data-target="_self" href="">
								<div class="input-group">
									<input type="text" class="form-control"
										data-ng-model="selectedDeepListStartDate" placeholder="From"
										readonly> <span class="input-group-addon"><i
										class="glyphicon glyphicon-calendar"></i></span>
								</div>
							</a>
							<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
								<datetimepicker data-ng-model="selectedDeepListStartDate"
									data-before-render="dateRangeValidation($view, $dates, $leftDate, $upDate, $rightDate)"
									data-datetimepicker-config="{ dropdownSelector: '#analyticsFrom', modelType: 'DD-MM-YYYY'+' 00:01', minView: 'day'  }" />
							</ul>
						</div>
						<div class="dropdown pull-left">
							<a class="dropdown-toggle" id="analyticsTo" role="button"
								data-toggle="dropdown" data-target="_self" href="">
								<div class="input-group">
									<input type="text" class="form-control"
										data-ng-model="selectedDeepListEndDate" placeholder="To"
										readonly> <span class="input-group-addon"><i
										class="glyphicon glyphicon-calendar"></i></span>
								</div>
							</a>
							<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
								<datetimepicker data-ng-model="selectedDeepListEndDate"
									data-before-render="dateRangeValidation($view, $dates, $leftDate, $upDate, $rightDate)"
									data-datetimepicker-config="{ dropdownSelector: '#analyticsTo', modelType: 'DD-MM-YYYY'+' 23:59', minView: 'day' }" />
							</ul>
						</div>
						<button class="btn black-btn go-btn pull-left" aria-hidden="true"
							type="button" ng-click="fn_deep_list(1)">
							<span aria-hidden="true" class="fa fa-arrow-right"></span>&nbsp;Go
						</button>
						<div class="clearfix"></div>
					</div>

					<table class="table table-theme mt-4"
						ng-class="{'region-col': filteredSource==4 || filteredSource==2}"
						id="tblsocList">
						<thead>
							<tr>
								<!--  <th>Source</th> -->
								<th></th>
								<th>Post Type</th>
								<th ng-if="filteredSource==4 || filteredSource==2">Region/City</th>
								<!-- <th>Date-time</th> -->
								<th>Text</th>
								<th>Overall Sentiment</th>
								<th>Categorized Sentiment</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody id="tableSocialListening-social">
							<tr>
								<td class="td-align-center no-data"
									ng-if="deepListTotalSocialCount==0" colspan="8"
									ng-bind="'No Data Found'"></td>
							</tr>
							<tr
								dir-paginate="deepListSocList in deepListSocLists|itemsPerPage:deepListSocialEntries"
								pagination-id="tblsocList-paginatenew"
								current-page="deepListSocialPagination.deepListSocialCurrentPage"
								total-items="deepListTotalSocialCount" ng-init="limit= 100">
								<td><i
									class="source-icon fa fa-{{deepListSocList.sourceTypeCode}}"
									aria-hidden="true"></i></span></td>
								<td><span ng-bind="deepListSocList.postType"></span></td>
								<td ng-if="filteredSource==4 || filteredSource==2"><span
									ng-bind="deepListSocList.city"></span></td>
								<td><span ng-bind="deepListSocList.postDateTime"
									class="deep-list-date pull-left"></span> <a
									href="#ticketmanagement/{{deepListSocList.postId}}"
									title="Click for ticket detail"> <span
										class="label label-danger pull-right"
										ng-if="deepListSocList.hasTicket == '1'">Ticket
											Generated</span>
								</a>
									<div class="clearfix"></div> <span
									ng-bind="deepListSocList.postText | limitTo:limit"></span> <a
									class="read-more"><span
										ng-click="limit=more_less=='more'?deepListSocList.postText.length:100; more_less=more_less=='more'?'less':'more'"
										ng-bind="deepListSocList.postText.length>100?'read '+more_less:''"></span></a>
									<a
									ng-if="deepListSocList.postType != 'Reply' && deepListSocList.postType != 'CCIR' && deepListSocList.postType != 'GES' "
									href="{{ deepListSocList.postUrl }}" target="_blank"
									class="read-more"><i class="fa fa-external-link"></i></a> <a
									href="javaScript:void()" data-toggle="modal"
									data-target="#snippetModalForAnalytics"
									ng-click="fn_showPostDetails(deepListSocList.postId)"
									class="modal-link-btn">Click to view snippet level
										distribution</a></td>
								<td id="td_{{deepListSocList.postId}}"
									ng-class="deepListSocList.overallSentiment=='Positive'? 'text-green':
                    	 deepListSocList.overallSentiment=='Negative' ? 'text-red':'text-yellow'">
									<strong class="pull-left senti-txt"><span
										ng-bind="deepListSocList.overallSentiment"
										id="sentiment_{{deepListSocList.postId}}"> </span></strong>
									<div ng-if="authRole==1" class="dropdown senti-dropdown">
										<button class="btn btn-primary dropdown-toggle" type="button"
											data-toggle="dropdown">
											<span class="caret"></span>
										</button>
										<ul class="dropdown-menu">
											<li><a
												ng-click="updateOverallSentiment(1,deepListSocList.postId,deepListSocList.overallSentiment,deepListSocList.postText,deepListSocList.postDateTime)">Positive</a></li>
											<li><a
												ng-click="updateOverallSentiment(-1,deepListSocList.postId,deepListSocList.overallSentiment,deepListSocList.postText,deepListSocList.postDateTime)">Negative</a></li>
											<li><a
												ng-click="updateOverallSentiment(0,deepListSocList.postId,deepListSocList.overallSentiment,deepListSocList.postText,deepListSocList.postDateTime)">Neutral</a></li>
										</ul>
									</div>
									<div ng-if="authRole==1" class="tooltipforupdatedSentiment">
										<i class="fa fa-question-circle"
											id="td_{{deepListSocList.postId}}"></i> <span
											class="tooltiptextforupdatedSentiment">To change the
											displayed sentiment permanently click on the dropdown arrow</span>
									</div>
								</td>
								<td><span title="Cleanliness"
									class="badge background-{{deepListSocList.categorizedSentiment.C.toLowerCase()}}">C</span>
									<span title="Hospitality"
									class="badge background-{{deepListSocList.categorizedSentiment.H.toLowerCase()}}">H</span>
									<span title="Accuracy"
									class="badge background-{{deepListSocList.categorizedSentiment.A.toLowerCase()}}">A</span>
									<span title="Maintenance"
									class="badge background-{{deepListSocList.categorizedSentiment.M.toLowerCase()}}">M</span>
									<span title="Product Quality"
									class="badge background-{{deepListSocList.categorizedSentiment.P.toLowerCase()}}">P</span>
									<span title="Speed & Delivery"
									class="badge background-{{deepListSocList.categorizedSentiment.S.toLowerCase()}}">S</span>
									<span title="Prices and Offers"
									class="badge background-{{deepListSocList.categorizedSentiment.PR.toLowerCase()}}">PR</span>
									<span title="Others"
									class="badge background-{{deepListSocList.categorizedSentiment.O.toLowerCase()}}">O</span>
									<span title="Non-Relevant"
									class="badge background-{{deepListSocList.categorizedSentiment.N.toLowerCase()}}">N</span>
								</td>
								<td ng-if="authRole==1"><a
									ng-if="(deepListSocList.sourceTypeCode == 'fb' || deepListSocList.sourceTypeCode == 'tw' )"
									class="open-tkt-create-modal pointer" data-toggle="modal"
									data-id="{{ deepListSocList.postId +'#@#'+ deepListSocList.sourceTypeCode  }}"
									data-target="#tkt-create-modal"
									ng-click="setFeedbackURL(deepListSocList.postId,deepListSocList.sourceTypeId)"><i
										class="fa fa-ticket"></i></a>
									<a
									ng-if="(deepListSocList.sourceTypeCode != 'fb' && deepListSocList.sourceTypeCode != 'tw' )"
									class="open-tkt-create-modal disabled"><i
										class="fa fa-ticket" style="color: #ccc;"></i></a></td>
								<!-- <td ng-if="authRole==1 &&(deepListSocList.sourceTypeCode != 'fb' || deepListSocList.sourceTypeCode != 'tw' )"><a class="open-tkt-create-modal disabled" ><i class="fa fa-ticket" style="color: #ccc;"></i></a></td> -->
								<td ng-if="authRole==2"><a
									class="open-tkt-create-modal disabled"><i
										class="fa fa-ticket" style="color: #ccc;"></i></a></td>
							</tr>
						</tbody>
					</table>
					<dir-pagination-controls pagination-id="tblsocList-paginatenew"
						class="pull-right" max-size="5" direction-links="true"
						boundary-links="true"
						on-page-change="deepListSocialPageChanged(newPageNumber)"></dir-pagination-controls>
					<div class="clearfix"></div>

				</div>
			</div>
		</div>
	</div>
	
	<div class="row pt-4">
		<div class="col-md-12">
			<div class="block-cover">
				<div class="block-head">
					<div class="row">
						<div class="col-md-12">
							<h3 class="block-name">Media Analysis</h3>
						</div>
					</div>
				</div>
				<div class="block-body">
				<div class="inner-filter mt-3 mb-5 context-filter-wapper">
						<i class="fa fa-filter pull-left" aria-hidden="true"></i> 
						<select
							data-ng-model="selectedSummarySource"
							data-ng-options="source[1] as source[0] for source in summarySource"
							class="selectpicker pull-left" id="source" ng-change="changePostTypeSelection();">
							<option style="display: none;" value="">All Sources</option>
						</select>
						<select class="selectpicker pull-left" id="sentiment" data-ng-model="selectedSummaryPostType">
							<option value="0" ng-if="selectedSummarySource==1" >All</option>
							<option value="3" ng-if="selectedSummarySource==2">Tweets</option>
							<option value="1" ng-if="selectedSummarySource==1">Own Posts</option>
							<option value="11" ng-if="selectedSummarySource==1">Guest posts </option>
							<option value="9" ng-if="selectedSummarySource==1">Tagged</option>
							
						</select>
						<!-- <select  class="selectpicker pull-left" id="sortby" data-ng-model="selectedSummarySortby">
							<option ng-if="selectedSummarySource==1" value="1">Sort by Post</option>
							<option ng-if="selectedSummarySource==1" value="2">Sort by Comment</option>
							<option ng-if="selectedSummarySource==2" value="3">Sort by Tweet</option>
							<option ng-if="selectedSummarySource==2" value="4">Sort by Retweet</option>
						</select> -->
						<div class="dropdown pull-left">
							<a class="dropdown-toggle" id="summaryFrom" role="button"
								data-toggle="dropdown" data-target="_self" href="">
								<div class="input-group">
									<input type="text" class="form-control"
										data-ng-model="selectedSummaryStartDate" placeholder="From"
										readonly > <span class="input-group-addon"><i
										class="glyphicon glyphicon-calendar"></i></span>
								</div>
							</a>
							<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
								<datetimepicker data-ng-model="selectedSummaryStartDate"
									data-before-render="dateRangeValidation($view, $dates, $leftDate, $upDate, $rightDate)"
									data-datetimepicker-config="{ dropdownSelector: '#summaryFrom', modelType: 'DD-MM-YYYY'+' 00:01', minView: 'day'  }" />
							</ul>
						</div>
						<div class="dropdown pull-left">
							<a class="dropdown-toggle" id="summaryTo" role="button"
								data-toggle="dropdown" data-target="_self" href="">
								<div class="input-group">
									<input type="text" class="form-control"
										data-ng-model="selectedSummaryEndDate" placeholder="To"
										readonly > <span class="input-group-addon"><i
										class="glyphicon glyphicon-calendar"></i></span>
								</div>
							</a>
							<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
								<datetimepicker data-ng-model="selectedSummaryEndDate"
									data-before-render="dateRangeValidation($view, $dates, $leftDate, $upDate, $rightDate)"
									data-datetimepicker-config="{ dropdownSelector: '#summaryTo', modelType: 'DD-MM-YYYY'+' 23:59', minView: 'day' }" />
							</ul>
						</div>
						<button class="btn black-btn go-btn pull-left" aria-hidden="true"
							type="button" ng-click="fn_own_social_post_list();fn_generate_overall_sentiment_chart();fn_generate_data_counts();">
							<span aria-hidden="true" class="fa fa-arrow-right"></span>&nbsp;Go
						</button>
						<div class="clearfix"></div>
					</div>
				<div class="row pb-4">
				
				<div class="col-md-8">
					<!-- <div class="grey-bg">
						<div id="summaryChart" style="min-width: 310px; height: 200px; margin: 0 auto"></div> 
					</div> -->
					<div class="block-cover" id="topHashtags">
						<div class="block-head bg-black">
							<div class="row">
								<div class="col-md-6">
									<h3 class="block-name">Overall feedback analysis</h3>
								</div>
							</div>
						</div>
						<div class="block-body">
							<div id="summaryChart" style="min-width: 310px; height: 200px; margin: 0 auto 10px"></div> 
						</div>
					</div>
				</div>	
				<div class="col-md-4">
					<!-- <div class="grey-bg" style="height: 200px;">
						<ul class="tag-list">
							<li ng-repeat="dataWithCount in overallSentimentDataCount">{{dataWithCount[2]}} <span class="badge background-negative">{{dataWithCount[0]}}</span></li>
						</ul>
						<div class="clearfix"></div>
					</div> -->
					<div class="block-cover" id="topHashtags">
						<div class="block-head bg-black">
							<div class="row">
								<div class="col-md-6">
									<h3 class="block-name">Summary</h3>
								</div>
							</div>
						</div>
						<div class="block-body" style="height: 212px;">
							<ul class="tag-list">
								<li ng-repeat="dataWithCount in overallSentimentDataCount">{{dataWithCount[2]}} <span class="badge background-negative">{{dataWithCount[0]}}</span></li>
							</ul> 
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
				</div>
				
				<div class="block-cover">
						<div class="block-head bg-black">
							<div class="row">
								<div class="col-md-6">
									<h3 class="block-name">Media Details</h3>
								</div>
								<div class="pull-right mt-2 mr-2">
									<label class="radio-inline" style="color:#fff" ng-if="selectedSummarySource==1"><input type="radio" name="sortBy" ng-value="1" ng-model="selectedSummarySortby" ng-click="fn_change_sorting(1);"> Sort By Post</label>
									<label class="radio-inline" style="color:#fff" ng-if="selectedSummarySource==1"><input type="radio" name="sortBy" ng-value="2" ng-model="selectedSummarySortby" ng-click="fn_change_sorting(2);"> Sort By Comment</label>
									
									<label class="radio-inline" style="color:#fff" ng-if="selectedSummarySource==2"><input type="radio" name="sortBy" ng-value="3" ng-model="selectedSummarySortby" ng-click="fn_change_sorting(3);"> Sort By Tweet</label>
									<label class="radio-inline" style="color:#fff" ng-if="selectedSummarySource==2"><input type="radio" name="sortBy" ng-value="4" ng-model="selectedSummarySortby" ng-click="fn_change_sorting(4);"> Sort By Retweet</label>
								</div>
								<!-- <select  class="selectpicker pull-left" id="sortby" data-ng-model="selectedSummarySortby">
									<option ng-if="selectedSummarySource==1" value="1">Sort by Post</option>
									<option ng-if="selectedSummarySource==1" value="2">Sort by Comment</option>
									<option ng-if="selectedSummarySource==2" value="3">Sort by Tweet</option>
									<option ng-if="selectedSummarySource==2" value="4">Sort by Retweet</option>
								</select> -->
							</div>
						</div>			
	<div class="row pt-4 pb-4  block-body">

		<div class="col-md-3 br-1 pl-0">
			<h3 class="metrics-title">Post List</h3>
			<!-- <input type="text" class="form-control mt-1 mb-2"
				placeholder="Search" ng-model="searchPostList" ng-keypress="checkKeyPress($event)"> -->
				
				<div class="search-wrapper">
				
				<input type="text" class="form-control mt-1 mb-2 search-box"
				placeholder="Search" id="postSearch" >
				<!-- <button class="close-icon reset-search" onclick="document.getElementById('postSearch').value = ''"></button> -->
				
			</div>
			<div class="user-social-post-wapper">	
			<div class="alert alert-danger mt-20" ng-if="summarypostslists==''">
                <h4 class="mb-0">No Post found.</h4>
            </div>
			<div ng-class="selectedPostDivID==summarypostslist[0]? 'post_list postContent selected':
                    	  'post_list postContent'"
                    	  ng-repeat="summarypostslist in summarypostslists">
				<div class="post-user-image" style="width: 30px">
					<a href="#" target="_blank"><img src="{{summarypostslist[7]}}" ng-if="summarypostslist[7] != null"  alt="{{summarypostslist[4]}}"/></a>
					<a href="#" target="_blank"><img src="assets/img/no-img.jpg" ng-if="summarypostslist[7] == null"  alt="{{summarypostslist[4]}}"/></a>
				</div>
				<div class="post-desp-wapper" style="position:relative">
					<div class="post-content mb-3 text-ellipsis pointer">
						<a ng-bind="summarypostslist[3]" ng-click="fn_get_post_details(summarypostslist[0])"><!-- Live, love, eat! Some #WednesdaySpecials
							inspiration for you. ?? Ready for it? 12pc chicken  --></a>
					</div>
					<a href="{{summarypostslist[5]}}" class="post-link-icon" target="_blank" ><i class="fa fa-external-link"></i></a>
					<span class="label-tagged" ng-if="selectedSummarySource==1">{{summarypostslist[9]}}</span>
					<a title="Click for Invalid Post" ng-click="fn_invalidate_entry(summarypostslist[0],'post')" > 
						<span class="" id="btnInvalid_{{summarypostslist[0]}}">
									<i class="fa fa-trash text-danger pointer" style="position: absolute;right: -11px;top: 18px;"></i></span>
					</a>
					<!-- <a href="http://www.twitter.com/{{summarypostslist[0]}}" class="post-link-icon" target="_blank" ng-if="selectedSummarySource==2"><i class="fa fa-external-link"></i></a> --> 
				</div>
				<div class="clearfix"></div>
			</div>
			</div>
			
		</div>
		<div class="col-md-5 br-1 pr-0">
			<h3 class="metrics-title">Post Detail</h3>
			<div class="" ng-if="checksource == 2">
				<div class="alert alert-danger mt-20" ng-if="summarypostsDetails==''">
	                <h4 class="mb-0">No Details found.</h4>
	            </div>
				<div class="user-post-list ng-scope" ng-repeat="summarypostsDetail in summarypostsDetails">
					<div class="post-wapper">
						<div class="post-user-image">{{summarypostsDetail[6]}}
							<img src="{{summarypostsDetail[6]}}" ng-if="summarypostsDetail[6] != null"  alt="{{summarypostsDetail[6]}}"/>
							<img src="assets/img/no-img.jpg" ng-if="summarypostsDetail[6] == null"  alt="{{summarypostsDetail[6]}}"/>
						</div>
						<div class="post-desp-wapper">
							<div class="post-desp">
								<div class="user-name ng-binding" ng-bind="summarypostsDetail[8]"><!-- KFC --></div>
								<div class="post-time text-right">
									<!-- May 22 --> {{summarypostsDetail[9]}}<i class="fa fa-twitter"></i>
								</div>
							</div>
							<div class="post-content mb-3">
								<div ng-bind="summarypostsDetail[5]"><!-- Live, love, eat! Some #WednesdaySpecials inspiration
									for you. ?? Ready for it? 12pc chicken + 4 dips all for Rs.350
									(Inclusive of GST). Walk-in/Call/Order:
									http://bit.ly/KFC12for350 (Valid on Wednesday only) --></div>
								<!-- ngIf: ownsocialpost[11] -->
								<div class="post-pimg mt-3 ng-scope"> {{summarypostsDetail[11]}}
									<img src="{{summarypostsDetail[11]}}"   alt=""/>
									<!-- <img src="assets/img/no-img.jpg" ng-if="summarypostslist[7] == null"  alt="{{summarypostslist[4]}}"/> -->
								</div>
								<!-- end ngIf: ownsocialpost[11] -->
							</div>
						</div>
					</div>
					<div class="review-wapper">
						<div>
							<div class="review-count ng-binding" ng-bind="summarypostsDetail[1]">40</div>
							<div class="review-title">like</div>
						</div>
						<div>
							<div class="review-count ng-binding" ng-bind="summarypostsDetail[2]">40</div>
							<div class="review-title">Retweet</div>
						</div>
					</div>
				</div>
			</div>
			<div class="user-social-post-wapper" ng-if="checksource == 1">
				<div class="user-post-list ng-scope" ng-repeat="summarypostsDetail in summarypostsDetails">
					<div class="post-wapper">
						<div class="post-user-image">
							<img src="{{summarypostsDetail[12]}}" ng-if="summarypostsDetail[12] != null"  alt="{{summarypostsDetail[12]}}"/>
							<img src="assets/img/no-img.jpg" ng-if="summarypostsDetail[12] == null"  alt="{{summarypostsDetail[12]}}"/>
						</div>
						<div class="post-desp-wapper">
							<div class="post-desp">
								<div class="user-name ng-binding" ng-bind="summarypostsDetail[13]"><!-- KFC --></div>
								<div class="post-time text-right" style="float:right;">
									<!-- May 22 --> {{summarypostsDetail[14]}}<i class="fa fa-facebook"></i>
								</div>
							</div>
							<div class="post-content mb-3">
								<div ng-bind="summarypostsDetail[10]"><!-- Live, love, eat! Some #WednesdaySpecials inspiration
									for you. ?? Ready for it? 12pc chicken + 4 dips all for Rs.350
									(Inclusive of GST). Walk-in/Call/Order:
									http://bit.ly/KFC12for350 (Valid on Wednesday only) --></div>
								<!-- ngIf: ownsocialpost[11] -->
								<div class="post-pimg mt-3 ng-scope">
									<img src="{{summarypostsDetail[11]}}"   alt=""/>
									<!-- <img src="assets/img/no-img.jpg" ng-if="summarypostslist[7] == null"  alt="{{summarypostslist[4]}}"/> -->
								</div>
								<!-- end ngIf: ownsocialpost[11] -->
								<strong ng-class="summarypostsDetail[15]=='Negative'?'text-red pull-right': summarypostsDetail[15]=='Neutral' ? 'text-yellow  pull-right':'text-green pull-right'">{{summarypostsDetail[15]}}</strong>
							</div>
						</div>
					</div>
					<div class="review-wapper">
						<div>
							<div class="review-count ng-binding" ng-bind="summarypostsDetail[2]">40</div>
							<div class="review-title">like</div>
						</div>
						<div>
							<div class="review-count ng-binding" ng-bind="summarypostsDetail[1]">40</div>
							<div class="review-title">Comment</div>
						</div>
						<div>
							<div class="review-count ng-binding" ng-bind="summarypostsDetail[3]">31</div>
							<div class="review-title">Share</div>
						</div>
						<div>
							<div class="review-count ng-binding"ng-bind="summarypostsDetail[4]+summarypostsDetail[5]+summarypostsDetail[6]+summarypostsDetail[7]+summarypostsDetail[8]+summarypostsDetail[9]">3861</div>
							<div class="review-title">Engagements</div>
						</div>
						<div class="mr-0 pull-right">
							<i
								class="fa fa-ticket pl-1 pt-1 pointer fa-2x pointer" data-toggle="modal"
								data-id="summarypostsDetail[0] +'#@#'+ selectedSummarySource }}" 
								data-target="#tkt-create-modal"
								ng-click="setFeedbackURL(summarypostsDetail[0],selectedSummarySource)"></i>
						</div>
					</div>
				</div>
			</div>
	
		</div>
		<div class="col-md-4">
			<h3 class="metrics-title">Comments &amp; Replies</h3>
			<div class="search-wrapper">
			
				<input type="text" class="form-control mt-1 mb-2 search-box"
				placeholder="Search" id="commentSearch" >
				<!-- <button class="close-icon reset-search"  onclick="document.getElementById('commentSearch').value = ''"></button> -->
				
			</div>
			<div class="user-social-post-wapper">
			<!-- no data message -->
			<div class="alert alert-danger mt-20" ng-if="summarypostsComments==''">
                <h4 class="mb-0">No Comments or Replies found.</h4>
            </div>
			<ul class="comment-widget content">
				<li ng-repeat=" summarypostsComment in summarypostsComments" class="content">
				<div>
					<span class="username_label"><i class="fa fa-user"></i>{{summarypostsComment[2]}}</span>
					<span class="comment-date pull-right" ng-bind="summarypostsComment[4]"></span>
					{{summarypostsComment[1]}}
					<div class="clearfix"></div> 
					<a title="Click for Invalid" ng-click="fn_invalidate_entry(summarypostsComment[0],'comment')" > 
						<span class="" id="btnInvalid_{{summarypostsComment[0]}}">
							<i class="fa fa-trash text-danger pointer" style="font-size: 1rem;float: right;"></i>
						</span>
					</a>
				</div> 
					<i 
					class="fa fa-ticket pl-1 pt-1 pointer" data-toggle="modal"
					data-id="summarypostsComment[0] +'#@#'+ selectedSummarySource }}" 
					data-target="#tkt-create-modal"
					ng-click="setFeedbackURL(summarypostsComment[0],selectedSummarySource)"></i>
					<strong ng-class="summarypostsComment[3]=='Negative'?'text-red': summarypostsComment[3]=='Neutral' ? 'text-yellow':'text-green'">{{summarypostsComment[3]}}</strong>
					
					<ul id="reply_{{summarypostsComment[0]}}">
					</ul>
				</li>
				<!-- <li ng-if="summarypostsComment[0]>10 || commentTweetOffSet < summarypostsComment[0]"> -->
				<li ng-if="varTotalCount>10 && commentTweetOffSet < varTotalCount">
					<label class="load-more-btn" for="load-more" ng-click="loadMoreComments()">
					    <span class="unloaded">LOAD MORE </span>
					</label>
				</li>
				<li ng-if="commentTweetOffSet > varTotalCount">
					<label class="load-more-btn-no-more">
					    <span class="unloaded">NO MORE RECORDS</span>
					</label>
				</li>
					
			</ul>
			</div>
		</div>
	</div>
</div>
</div>
</div>
</div>

<div class="modal fade" id="sentimentDetailsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h2 class="modal-title" id="myModalLabel">Sentiment wise feedback details	</h2>
      </div>
      <div class="modal-body">
      		<div class="pull-right">
        	<div class="check-box_div_positive mb-3"> <span>Positive <strong>({{modalPositiveValue}})</strong></span> 
        	<input type="checkbox" id="chkPositive" value="Positive" class="chk-hidden"/>
    		<label for="chkPositive" class="check-box chkPositiveLabel"></label></div>
        	
        	<div class="check-box_div_neutral mb-3"> <span>Neutral <strong>({{modalNeutralValue}})</strong></span> 
        	<input type="checkbox" id="chkNeutral" value="Neutral" class="chk-hidden"/>
    		<label for="chkNeutral" class="check-box"></label></div>
    		
    		<div class="check-box_div_negative mb-3"> <span>Negative <strong>({{modalNegativeValue}})</strong></span> 
        	<input type="checkbox" id="chkNegative" value="Negative" class="chk-hidden"/>
    		<label for="chkNegative" class="check-box"></label></div>
    		</div>
    		<div class="clearfix"></div>
    		<div class="user-social-post-wapper">
            <table class="table table-theme">
			  <tr>
			    
			    <th>Feedback Text</th>
			    <th>Feedback Type</th>
			    <th>Sentiment</th>
			  </tr>
			  <tr ng-repeat="overallSentimentDetail in overallSentimentDetails " ng-init="mylimit= 40">
			   
			    <td><span class="deep-list-date" ng-bind="overallSentimentDetail[2]"></span> <!-- {{overallSentimentDetail[1]}} -->
			    	<span ng-bind="overallSentimentDetail[1] | limitTo:mylimit"></span>
			    	<span class="read-more"
						ng-click="mylimit=more_less=='more'?overallSentimentDetail[1].length:40; more_less=more_less=='more'?'less':'more'"
						ng-bind="overallSentimentDetail[1].length>40?'read '+more_less:''">
					</span>
			    	<a	ng-if="overallSentimentDetail[6] != 'Reply'"
						href="{{ overallSentimentDetail[4] }}" target="_blank"
						class="read-more">
					<i class="fa fa-external-link"></i></a>
			    </td>
			    <td> 
			    	<span class="label-tagged" ng-bind="overallSentimentDetail[6]" style="position: inherit;"></span> 
			   	</td>
			    <td ng-class="overallSentimentDetail[3]=='Positive'? 'text-green':
                    	 overallSentimentDetail[3]=='Negative' ? 'text-red':'text-yellow'"
                    ng-bind="overallSentimentDetail[3]"></td>
			  </tr>
			</table>
      		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="topNegativeAspectDetailsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h2 class="modal-title" id="myModalLabel">Negative aspect details	</h2>
      </div>
      <div class="modal-body">
    		<div class="user-social-post-wapper">
            <table class="table table-theme">
			  <tr>
			    
			    <th>Feedback Text</th>
			    <th>Feedback Type</th>
			    <th>Sentiment</th>
			  </tr>
			  <tr ng-repeat="overallSentimentDetail in topTopNegativeAspectDeatilsModal " ng-init="mylimit= 40">
			   
			    <td><span class="deep-list-date" ng-bind="overallSentimentDetail[2]"></span> <!-- {{overallSentimentDetail[1]}} -->
			    	<span ng-bind="overallSentimentDetail[1] | limitTo:mylimit"></span>
			    	<span class="read-more"
						ng-click="mylimit=more_less=='more'?overallSentimentDetail[1].length:40; more_less=more_less=='more'?'less':'more'"
						ng-bind="overallSentimentDetail[1].length>40?'read '+more_less:''">
					</span>
			    	<a	ng-if="overallSentimentDetail[5] != 'Reply'"
						href="{{ overallSentimentDetail[4] }}" target="_blank"
						class="read-more">
					<i class="fa fa-external-link"></i></a>
			    </td>
			    <td> 
			    	<span class="label-tagged" ng-bind="overallSentimentDetail[5]" style="position: inherit;"></span> 
			   	</td>
			    <td ng-class="overallSentimentDetail[3]=='Positive'? 'text-green':
                    	 overallSentimentDetail[3]=='Negative' ? 'text-red':'text-yellow'"
                    ng-bind="overallSentimentDetail[3]"></td>
			  </tr>
			</table>
      		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



</div>


<!-- new ticket response KPI -->
<div class="row pt-4" style="display:none;" >
	<div class="col-md-12">
		<div class="block-cover">
			<div class="block-head">
				<div class="row">
					<div class="col-md-12">
						<h3 class="block-name">Ticket Response Analysis</h3>
					</div>
				</div>
			</div>
			<div class="block-body">
				<table  class="table table-theme mt-4">
					<thead>
						<tr>
							<th style="vertical-align: top;">Ticket Number</th>
							<th style="vertical-align: top;">Created on</th>
							<th style="vertical-align: top;">Response on</th>
							<th style="vertical-align: top;">Turn around time <br> <em class="deep-list-date">Difference in first Response time <br>and generation time</em></th>
							<th style="vertical-align: top;">Closing Time</th>
							<th style="vertical-align: top;">Turn around time <br> <em class="deep-list-date">Difference in Generation time <br>and Closing time of ticket</em></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>12578</td>
							<td>02-01-2019 13:10</td>
							<td>02-01-2019 17:10</td>
							<td>4hrs</td>
							<td>03-01-2019 00:00</td>
							<td>1day</td>
						</tr>
						<tr>
							<td>12578</td>
							<td>02-01-2019 13:10</td>
							<td>02-01-2019 17:10</td>
							<td>4hrs</td>
							<td>03-01-2019 00:00</td>
							<td>1day</td>
						</tr>
						<tr>
							<td>12578</td>
							<td>02-01-2019 13:10</td>
							<td>02-01-2019 17:10</td>
							<td>4hrs</td>
							<td>03-01-2019 00:00</td>
							<td>1day</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>		
	</div>
</div>

<div class="row pt-4" >
	<div class="col-md-12">
		<div class="block-cover">
			<div class="block-head">
				<div class="row">
					<div class="col-md-12">
						<h3 class="block-name">Hourly basis sentiment analysis (Last 24 hr)</h3>
					</div>
				</div>
			</div>
			<div class="block-body">
			<div class="inner-filter mt-3 mb-5 context-filter-wapper">
						<i class="fa fa-filter pull-left" aria-hidden="true"></i> 
						<select
							data-ng-model="selectedSummaryHourlySource"
							data-ng-options="source[1] as source[0] for source in summaryHourlySource"
							class="selectpicker pull-left" id="source">
							<option style="display: none;" value="">All Sources</option>
						</select>
						
						
						<!-- <div class="dropdown pull-left">
							<a class="dropdown-toggle" id="summaryFrom" role="button"
								data-toggle="dropdown" data-target="_self" href="">
								<div class="input-group">
									<input type="text" class="form-control"
										data-ng-model="selectedSummaryStartDate" placeholder="From"
										readonly > <span class="input-group-addon"><i
										class="glyphicon glyphicon-calendar"></i></span>
								</div>
							</a>
							<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
								<datetimepicker data-ng-model="selectedSummaryStartDate"
									data-before-render="dateRangeValidation($view, $dates, $leftDate, $upDate, $rightDate)"
									data-datetimepicker-config="{ dropdownSelector: '#summaryFrom', modelType: 'DD-MM-YYYY'+' 00:01', minView: 'day'  }" />
							</ul>
						</div>
						<div class="dropdown pull-left">
							<a class="dropdown-toggle" id="summaryTo" role="button"
								data-toggle="dropdown" data-target="_self" href="">
								<div class="input-group">
									<input type="text" class="form-control"
										data-ng-model="selectedSummaryEndDate" placeholder="To"
										readonly > <span class="input-group-addon"><i
										class="glyphicon glyphicon-calendar"></i></span>
								</div>
							</a>
							<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
								<datetimepicker data-ng-model="selectedSummaryEndDate"
									data-before-render="dateRangeValidation($view, $dates, $leftDate, $upDate, $rightDate)"
									data-datetimepicker-config="{ dropdownSelector: '#summaryTo', modelType: 'DD-MM-YYYY'+' 23:59', minView: 'day' }" />
							</ul>
						</div> -->
						<button class="btn black-btn go-btn pull-left" aria-hidden="true"
							type="button" ng-click="fn_generate_hourly_sentiment_chart()">
							<span aria-hidden="true" class="fa fa-arrow-right"></span>&nbsp;Go
						</button>
						<div class="clearfix"></div>
					</div>
				<div id="hourlySentiment" style="min-width: 310px; height: 300px; margin: 0 auto 10px"></div>
			</div>
		</div>		
	</div>
</div>
<div class="row pt-4" >
	<div class="col-md-12">
		<div class="block-cover">
			<div class="block-head">
				<div class="row">
					<div class="col-md-12">
						<h3 class="block-name">Top negative aspect</h3>
					</div>
				</div>
			</div>
			<div class="block-body">
			<div class="inner-filter mt-3 mb-5 context-filter-wapper">
						<i class="fa fa-filter pull-left" aria-hidden="true"></i> 
						<select
							data-ng-model="selectedSummaryTopNegativeAspectSource"
							data-ng-options="source[1] as source[0] for source in summarySource"
							class="selectpicker pull-left" id="source">
							<option style="display: none;" value="">All Sources</option>
						</select>
						
						
						<div class="dropdown pull-left">
							<a class="dropdown-toggle" id="summaryTopNegativeAspectFrom" role="button"
								data-toggle="dropdown" data-target="_self" href="">
								<div class="input-group">
									<input type="text" class="form-control"
										data-ng-model="selectedSummaryTopNegativeAspectStartDate" placeholder="From"
										readonly > <span class="input-group-addon"><i
										class="glyphicon glyphicon-calendar"></i></span>
								</div>
							</a>
							<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
								<datetimepicker data-ng-model="selectedSummaryTopNegativeAspectStartDate"
									data-before-render="dateRangeValidation($view, $dates, $leftDate, $upDate, $rightDate)"
									data-datetimepicker-config="{ dropdownSelector: '#summaryTopNegativeAspectFrom', modelType: 'DD-MM-YYYY'+' 00:01', minView: 'day'  }" />
							</ul>
						</div>
						<div class="dropdown pull-left">
							<a class="dropdown-toggle" id="summaryTopNegativeAspectTo" role="button"
								data-toggle="dropdown" data-target="_self" href="">
								<div class="input-group">
									<input type="text" class="form-control"
										data-ng-model="selectedSummaryTopNegativeAspectEndDate" placeholder="To"
										readonly > <span class="input-group-addon"><i
										class="glyphicon glyphicon-calendar"></i></span>
								</div>
							</a>
							<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
								<datetimepicker data-ng-model="selectedSummaryTopNegativeAspectEndDate"
									data-before-render="dateRangeValidation($view, $dates, $leftDate, $upDate, $rightDate)"
									data-datetimepicker-config="{ dropdownSelector: '#summaryTopNegativeAspectTo', modelType: 'DD-MM-YYYY'+' 23:59', minView: 'day' }" />
							</ul>
						</div>
						<button class="btn black-btn go-btn pull-left" aria-hidden="true"
							type="button" ng-click="fn_generate_top_negative_aspect_chart();">
							<span aria-hidden="true" class="fa fa-arrow-right"></span>&nbsp;Go
						</button>
						<div class="clearfix"></div>
					</div>
				<div id="topNegativeAspect" style="min-width: 310px; height: 300px; margin: 0 auto 10px"></div>
			</div>
		</div>		
	</div>
</div>

<div class="row pt-4" style="display:none;">
	<div class="col-md-12">
<div class="block-cover">
	<div class="block-head ">
		<div class="row">
			<div class="col-md-6">
				<h3 class="block-name">Direct Messages (Twitter)</h3>
			</div>
			
		</div>
	</div>			
	<div class="row pt-4 pb-4  block-body">
		<div class="inner-filter mt-3 mb-5 context-filter-wapper">
			<i class="fa fa-filter pull-left" aria-hidden="true"></i> 
			<!-- <select
				data-ng-model="selectedDMSource"
				data-ng-options="source[1] as source[0] for source in summarySource"
				class="selectpicker pull-left" id="source">
				<option style="display: none;" value="">All Sources</option>
			</select> -->
			<select class="selectpicker pull-left">
				<option value="2">Twitter</option>
			</select>
			
			<div class="dropdown pull-left">
				<a class="dropdown-toggle" id="summaryDMFrom" role="button"
					data-toggle="dropdown" data-target="_self" href="">
					<div class="input-group">
						<input type="text" class="form-control"
							data-ng-model="selectedDMStartDate" placeholder="From"
							readonly > <span class="input-group-addon"><i
							class="glyphicon glyphicon-calendar"></i></span>
					</div>
				</a>
				<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
					<datetimepicker data-ng-model="selectedDMStartDate"
						data-before-render="dateRangeValidation($view, $dates, $leftDate, $upDate, $rightDate)"
						data-datetimepicker-config="{ dropdownSelector: '#summaryDMFrom', modelType: 'DD-MM-YYYY'+' 00:01', minView: 'day'  }" />
				</ul>
			</div>
			<div class="dropdown pull-left">
				<a class="dropdown-toggle" id="summaryDMTo" role="button"
					data-toggle="dropdown" data-target="_self" href="">
					<div class="input-group">
						<input type="text" class="form-control"
							data-ng-model="selectedDMEndDate" placeholder="To"
							readonly > <span class="input-group-addon"><i
							class="glyphicon glyphicon-calendar"></i></span>
					</div>
				</a>
				<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
					<datetimepicker data-ng-model="selectedDMEndDate"
						data-before-render="dateRangeValidation($view, $dates, $leftDate, $upDate, $rightDate)"
						data-datetimepicker-config="{ dropdownSelector: '#summaryDMTo', modelType: 'DD-MM-YYYY'+' 23:59', minView: 'day' }" />
				</ul>
			</div>
			<button class="btn black-btn go-btn pull-left" aria-hidden="true"
				type="button" ng-click="fn_user_list_direct_message();">
				<span aria-hidden="true" class="fa fa-arrow-right"></span>&nbsp;Go
			</button>
			<div class="clearfix"></div>
		</div>

		<div class="block-cover col-md-5 pl-0">
			<div class="block-head bg-black">
				<div class="row">
					<div class="col-md-12">
						<h3 class="block-name">Users</h3>
					</div>
				</div>
			</div>
			<div class="block-body">
			<div class="search-wrapper">
			<input type="text" class="form-control mt-1 mb-2 search-box" id="tweetUserSearch"
			placeholder="Search" >
			</div>
			<div class="user-social-post-wapper">	
			<div class="alert alert-danger mt-20" ng-if="dMUserListData==''">
                <h4 class="mb-0">No User found.</h4>
            </div>
            
            <ul class="tweet_user_list pl-0">
            	<!-- <div ng-class="selectedPostDivID==summarypostslist[0]? 'post_list postContent selected':
                    	  'post_list postContent'"
                    	  ng-repeat="summarypostslist in summarypostslists"> -->
            	<li ng-class="selectedDMDivID==dmUsers[0]? 'pointer tweetUsers dmUserList selected':
                    	  'pointer tweetUsers dmUserList'" ng-repeat="dmUsers in dMUserListData">
            	<div class="post-user-image" style="width: 40px">
					<a href="#" target="_blank"><img src="{{dmUsers[2]}}" ng-if="dmUsers[2] != null"  alt="{{dmUsers[0]	}}" class="img-circle"/></a>
					<a href="#" target="_blank"><img src="assets/img/no-img.jpg" ng-if="dmUsers[2] == null"  alt="{{dmUsers[0]}}" class="img-circle"/></a>
				</div>
				
				<div class="post-desp-wapper" ng-click="fn_get_DM_details(dmUsers[0])">
					<strong ng-bind="dmUsers[0]"></strong><a href="#" target="_blank" class="text-info ml-2">@{{dmUsers[0]}}</a>
					<!-- <p>You: to shubra</p> -->
				</div>
				<div class="clearfix"></div>
            	</li>
           	</ul>
			
			</div>
			</div>
		</div>
		<div class="block-cover col-md-7 pr-0">
			
			<div class="block-head bg-black">
				<div class="row">
					<div class="col-md-12">
						<h3 class="block-name">Direct message communication details</h3>
					</div>
				</div>
			</div>
			<div class="block-body pt-5 pb-5">
			<div  class="user-social-post-wapper">
			<div ng-class="chatData[2]==adminUserName? 'tweet-detail-self mb-3':
                    	  'tweet-detail-other mb-3'"
                  ng-repeat="chatData in dmDetailedChain"
                  >
				<div class="post-user-image" style="width: 40px">
					<a href="#" target="_blank"><img src="{{chatData[4]}}" ng-if="chatData[4] != null"  alt="{{chatData[2]}}" class="img-circle"/></a>
					<a href="#" target="_blank"><img src="assets/img/no-img.jpg" ng-if="chatData[4] == null"  alt="{{chatData[2]}}" class="img-circle"/></a>
				</div>
				
				<div ng-class="chatData[2]==adminUserName? 'pull-right  pr-3':
                    	  'pull-left'">
					<div class="tweet-message">
						{{chatData[1]}}
					</div>
					<div class="clearfix"></div>
					<div ng-class="chatData[2]==adminUserName? 'text-right':
                    	  'no-class'">
						{{chatData[0]}} <i class="fa fa-check"></i>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			</div>
			</div>
		</div>
		</div>
</div>
</div>
</div> 
</div> 
