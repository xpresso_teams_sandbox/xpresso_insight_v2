<%@include file="/WEB-INF/jspf/topFixedDailySnapshot.jspf" %>
<div class="panel-group analyze" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading" ng-click="accordian_expand_tw = accordian_expand_tw == 'angle-down' ? 'angle-up' : 'angle-down'">
      <div class="panel-title">
        <div class="kpi-head pointer" data-toggle="collapse" data-parent="#accordion" href="#collapse1" ng-click="check();">
          <div class="social-icon"><i class="fa fa-twitter"></i></div>
          <div class="action-arrow"><i class="fa fa-{{accordian_expand_tw}}"></i></div>
          <div class="kpi-bar">
            <div class="row">
              <div class="col-md-3 col-xs-3">
                <div><b>Followers:</b> <span ng-bind="socialKPITweeter.total_followers"></span></div>
              </div>
              <div class="col-md-3 col-xs-3">
                <div> <b>Tweets:</b> <span ng-bind="socialKPITweeter.post_id_count"></span> </div>
              </div>
              <div class="col-md-3 col-xs-3">
                <div> <b>Following:</b> <span ng-bind="socialKPITweeter.total_followings"></span> </div>
              </div>
              <div class="col-md-3 col-xs-3">
                <div> <b>Likes:</b> <span ng-bind="socialKPITweeter.page_like"></span> </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="collapse1" class="panel-collapse collapse in">
      <div class="panel-body">
        <div class="row">
          <div class="col-md-6">
            <div class="block-cover analyzeChartContainer" id="folloerGrowthRateContainer">
              <div class="block-head-black">
				  <div class="row">
						<div class="col-md-6">
							<i class="fa fa-line-chart" aria-hidden="true"></i>
							<h3 class="block-name">Follower Growth Rate</h3>
						</div>
						<div class="col-md-6">
							<div class="pull-right bar-filter">
								<select class="selectpicker nbt-select" id="selectFollowerGrowth" ng-change="fn_follower_growth_rate_interval_change();" ng-model="follower_growth_rate_interval">
								  <option value='1'>Daily Interval</option>
								  <option value='2'>Weekly Interval</option>
								  <!-- <option value='Neutral'>Neutral</option> -->
								</select>
							</div>
						</div>
					</div>
              </div>
              <div class="block-body">
				  <div class="row">
					<div class="col-md-12 mt-2 mb-3">
						<div id="twitter_analyze-chart1"></div>
						<div class="graph-defn">Daily and Weekly basis follower growth rate.</div>
					</div>
				</div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="block-cover" id="sentimentDistributionMonthlyGraphDiv">
				<div class="block-head-black">
				  <div class="row">
						<div class="col-md-12">
							<i class="fa fa-bar-chart" aria-hidden="true"></i>
							<h3 class="block-name">Per Tweet Interaction</h3>
						</div>
					</div>
              </div>
				<div class="block-body">
				  <div class="row">
					<div class="col-md-12 mt-2 mb-3">
						<div id="twitter_per_interaction" class="draw-chart"></div>
						<div class="graph-defn">Count of user interaction against a tweet.</div>
					</div>
				</div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="block-cover">
				<div class="block-head-black">
				  <div class="row">
						<div class="col-md-12">
							<h3 class="block-name">Top 5 most engaging tweets</h3>
						</div>
					</div>
              </div>
              <div class="block-body padding-0">
				<div class="row">
					<div class="col-md-12 mt-2 mb-3">
						<div class="facebook-listing" id="twitter_listing">
						  <div class="post" ng-repeat="tweets in most_engaging_tweets">
							<h3 ng-bind="tweets.author_name"></h3>
							<div class="post-txt" dd-text-collapse dd-text-collapse-max-length="130" dd-text-collapse-text="{{tweets.post_text}}"></div>
							<div class="expression">
								<div ng-bind="tweets.retweet_count" class="retweet"></div>
								<div ng-bind="tweets.love_count" class="love"></div>
							</div>
						  </div>
						  <div ng-if="!most_engaging_tweets" class="text-center">No Data Found.</div>
						</div>
						<div class="graph-defn ml-3 mr-3">Top 5 most engaging tweets.</div>
					</div>
				</div>
                
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="block-cover" id="">
				<div class="block-head-black">
				  <div class="row">
						<div class="col-md-12">
							<i class="fa fa-clock-o" aria-hidden="true"></i>
							<h3 class="block-name">Most Engaging Tweets Time</h3>
						</div>
					</div>
              </div>
              <div class="block-body" id="twitter_perday">
                <div class="row">
                  <div class="col-md-6">
                    <h3 class="text-center">Hourly</h3>
                    <div id="twitter_analyze-chart5" class="clock-chart">
                    	<div ng-if="!no_data_clock_tw" class="text-center"  style="padding:100px 0 80px;">No Data Found.</div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <h3 class="text-center">Daily</h3>
                    <div id="twitter_analyze-calender" class="analyze-calender"></div>
                  </div>
					<div class="col-md-12"><div class="graph-defn mb-3">Most Engaging Tweets Time within 24 hours.</div></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading" ng-click="customTrigger(); accordian_expand_fb = accordian_expand_fb == 'angle-down' ? 'angle-up' : 'angle-down'">
      <div class="panel-title">
        <div class="kpi-head pointer" data-toggle="collapse" data-parent="#accordion" href="#collapse2" ng-click="check();">
          <div class="social-icon"><i class="fa fa-facebook"></i></div>
          <div class="action-arrow"><i class="fa fa-{{accordian_expand_fb}}"></i></div>
          <div class="kpi-bar">
            <div class="row">
              <div class="col-md-3 col-xs-3">
                <div><b>Fans:</b> <span ng-bind="socialKPIFb.page_like"></span></div>
              </div>
              <div class="col-md-3 col-xs-3">
                <div><b>New likes:</b> <span ng-bind="socialKPIFb.new_like"></span></div>
              </div>
              <div class="col-md-3 col-xs-3">
                <div><b>Rate of Fan Growth:</b> <span ng-bind="socialKPIFb.rate_of_fan_growth"></span></div>
              </div>
              <div class="col-md-3 col-xs-3">
                <div><b>People Talking About:</b> <span ng-bind="socialKPIFb.talking_about_count"></span></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="collapse2" class="panel-collapse collapse">
      <div class="panel-body">
        <div class="row">
          <div class="col-md-6">
            <div class="block-cover analyzeChartContainer" id="sentimentDistributionMonthlyGraphDiv">
				<div class="block-head-black">
				  <div class="row">
						<div class="col-md-6">
							<i class="fa fa-line-chart" aria-hidden="true"></i>
							<h3 class="block-name">Fan Growth Rate</h3>
						</div>
						<div class="col-md-6">
							<div class="pull-right bar-filter">
								<select class="selectpicker nbt-select" id="selectFanGrowth" ng-change="fn_fan_growth_rate_interval_change()" ng-model="fan_growth_rate_interval">
								  <option value='1'>Daily Interval</option>
								  <option value='2'>Weekly Interval</option>
								  <!-- <option value='Neutral'>Neutral</option> -->
								</select>
							</div>
						</div>
					</div>
              </div>
              <div class="block-body" id="">
                <div class="row">
                  <div class="col-md-12 mt-2 mb-3">
                    <div id="fb_analyze-chart1"></div>
					  <div class="graph-defn">Daily and Weekly basis fan growth rate.</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
			  <div class="block-cover" id="sentimentDistributionMonthlyGraphDiv">
				<div class="block-head-black">
				  <div class="row">
						<div class="col-md-12">
							<i class="fa fa-bar-chart" aria-hidden="true"></i>
							<h3 class="block-name">Per Post Interaction</h3>
						</div>
					</div>
              </div>
				<div class="block-body">
				  <div class="row">
					<div class="col-md-12 mt-2 mb-3">
						<div id="post_per_interaction" class="draw-chart"></div>
						<div class="graph-defn">Count of user interaction against a post.</div>
					</div>
				</div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="block-cover">
				<div class="block-head-black">
				  <div class="row">
						<div class="col-md-12">
							<i class="fa fa-bar-chart" aria-hidden="true"></i>
							<h3 class="block-name">Top 5 most engaging posts</h3>
						</div>
					</div>
              </div>
              
              <div class="block-body padding-0">
				<div class="row">
					<div class="col-md-12 mt-2 mb-3">
						<div class="facebook-listing" id="facebook_listing">
						  <div class="post" ng-repeat="post in most_engaging_fb_posts">
							<h3 ng-bind="post.author_name"></h3>
							<div class="post-txt" dd-text-collapse dd-text-collapse-max-length="130" dd-text-collapse-text="{{post.post_text}}"></div>
							<div class="expression">
							  <div class="love" ng-bind="post.love_count">5</div>
							  <div class="laugh" ng-bind="post.haha_count">5</div>
							  <div class="wow" ng-bind="post.wow_count">6</div>
							  <div class="sad" ng-bind="post.sad_count">4</div>
							  <div class="angry" ng-bind="post.angry_count">7</div>
							  <div class="comment" ng-bind="post.comments_count">41</div>
							  <div class="seen" ng-bind="post.love_count">411</div>
							</div>
						  </div>
						  <div ng-if="!most_engaging_fb_posts" class="text-center"  style="padding:100px 0 80px;">No Data Found.</div>
						</div>
						<div class="graph-defn ml-3 mr-3">Top 5 most engaging posts.</div>
					</div>
				</div>
                
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="block-cover" id="">
				<div class="block-head-black">
				  <div class="row">
						<div class="col-md-12">
							<i class="fa fa-clock-o" aria-hidden="true"></i>
							<h3 class="block-name">Most Engaging Posts Time</h3>
						</div>
					</div>
              </div>
              <div class="block-body" id="">
                <div class="row">
                  <div class="col-md-6">
                    <h3 class="text-center">Hourly</h3>
                    <div id="fb_analyze-chart5" class="clock-chart">
                    	<div ng-if="!no_data_clock_fb" class="text-center"  style="padding:100px 0 80px;">No Data Found.</div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <h3 class="text-center">Daily</h3>
                    <div id="fb_analyze-calender" class="analyze-calender" ng-if="renderCalendar">
                      <!-- <datetimepicker data-ng-model="data.embeddedDate" data-selectable="false" data-renderOn="renderCalendar" data-before-render="selectableFalseFb($view, $dates, $leftDate, $upDate, $rightDate)" data-datetimepicker-config="{ startView:'day', minView:'day' }" template-url="templates1/datetimepicker.html"/> -->
                    </div>
                  </div>
					<div class="col-md-12"><div class="graph-defn mb-3">Most engaging post Time within 24 hours.</div></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="clearfix"></div>
<script type="text/ng-template" id="templates/datetimepicker.html">
	<div class="datetimepicker table-responsive"> 
		<table class="table table-condensed {{ data.currentView }}-view">
			<thead>			        
				<tr>           
					<th class="left" data-ng-click="changeView(data.currentView, data.leftDate, $event)" data-ng-show="data.leftDate.selectable"><i class="glyphicon glyphicon-arrow-left"><span class="sr-only">{{ screenReader.previous }}</span></i></th>				            
					<th class="switch" colspan="5" data-ng-show="data.previousViewDate.selectable" data-ng-click="changeView(data.previousView, data.previousViewDate, $event)">{{ data.previousViewDate.display }}</th>				            
					<th class="right" data-ng-click="changeView(data.currentView, data.rightDate, $event)" data-ng-show="data.rightDate.selectable"><i class="glyphicon glyphicon-arrow-right"><span class="sr-only">{{ screenReader.next }}</span></i></th>				        
				</tr>			        
				<tr>				            
					<th class="dow" data-ng-repeat="day in data.dayNames">{{ day }}</th>				        
				</tr>			        
			</thead>		        
			<tbody>			        
				<tr data-ng-if="data.currentView !== 'day'">				            
					<td colspan="7"><span class="{{ data.currentView }}" data-ng-repeat="dateObject in data.dates" data-ng-class="{active: dateObject.active, past: dateObject.past, future: dateObject.future, disabled: !dateObject.selectable}" data-ng-click="changeView(data.nextView, dateObject, $event)">{{ dateObject.display }}</span></td>				        
				</tr>			        
				<tr data-ng-if="data.currentView === 'day'" data-ng-repeat="week in data.weeks">
					<td data-ng-repeat="dateObject in week.dates" data-ng-click="changeView(data.nextView, dateObject, $event)" data-html="true" rel="tooltip" title="{{dateObject.result.iclass ? '<strong>Date:</strong> <span>'+ dateObject.result.date +'</span><br/><strong>Count:</strong> <span>'+ dateObject.result.count +'</span>' : ''}}" class="day {{ dateObject.result.iclass}}" data-ng-class="{active: dateObject.active, past: dateObject.past, future: dateObject.future, disabled: !dateObject.selectable, 'c-hidden': dateObject.hidden, 'future-date': dateObject.future_date}">{{ dateObject.display }}</td>				        
				</tr>		        
			</tbody>		    
		</table>	
	</div>
</script> 
