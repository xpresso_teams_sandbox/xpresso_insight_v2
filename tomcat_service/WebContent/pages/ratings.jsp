<div id="rating">
	<div class="row">
		<div class="col-md-12">
			<div class="page-title">
				<div class="row">
					<div class="col-md-6"><h2 ng-bind="activeMenu"></h2></div>
					<div class="col-md-6">
						<span class="crawling-date"><b>Data as of :</b> {{crawlingDateTime}}</span>
					</div>
				</div>
				
				
			</div>
		</div>
		<div class="col-md-12">
			<ul class="nav nav-tabs" role="tablist">
				<li ng-class="{true: 'active', false: ''}[ownerShipId == ownership[0]]" ng-repeat="ownership in ownershipMetaData">
					<a class="pointer" role="tab" data-toggle="tab" ng-click="changeOwnership(ownership[0])" ng-bind="ownership[1]"></a>
				</li>
			</ul>
			<div class="tab-content">
				<div id="equity" role="tabpanel" class="tab-pane in active">
					<div class="rating-tab-inner-wapper">
						<div class="block-cover" id="OOverallRatingAcrossStores">
							<div class="block-head">
								<div class="row">
									<div class="col-md-6">
										<h3 class="block-name">Overall rating across stores</h3>
									</div>
									<div class="col-md-6">
										<div class="pull-right bar-filter">
											<button class="btn black-btn go-btn pull-right ml-1" aria-hidden="true" type="button" ng-click="filterOverAllRatings()">
												<span aria-hidden="true" class="fa fa-arrow-right"></span> Go
											</button>
											
											<select data-ng-model="overAllRatingsSelectedCity" data-ng-options="city[0] as city[1] for city in overAllRatingsCities" class="selectpicker ml-1" id="overAllRatingsCitiesID">
											</select>
											<label class="pull-right ml-3">By Locations:</label>
										</div>
									</div>
								</div>
							</div>
							<div class="block-body">
								<div class="grey-bg pl-0 pr-0">
									<table class="rating-table">
										<tbody>
											<tr>
											  <td class="td-align-center no-data" ng-if="overallratingsstores==''" colspan="3" ng-bind="'No Data Found'"></td>
											</tr>
											<tr class="table-head">
												<td class="pl-2" style="width: 40%">Source</td>
												<td style="width: 15%">Avg Rating</td>
												<td style="width: 15%">No of Stores</td>
												<td style="width: 15%">Top Avg <br/><span style="font-size:9px;">(Average of top 10 rating stores)</span></td>
												<td style="width: 15%">Bottom Avg<br/><span style="font-size:9px;">(Average of bottom 10 rating stores)</span></td>
											</tr>
											<tr ng-repeat="overallratingsstore in overallratingsstores" ng-init='getTopAvgRating($index,overAllRatingsSelectedCity,overallratingsstore[4])'>
												<td class="pl-3">
													<div class="source-logo sl-{{overallratingsstore[0]}}"></div>
													<div class="store-name">{{overallratingsstore[1]}}</div>
												</td>
												<td class="pr-2">
													<div class="rating-count green">{{overallratingsstore[6] | number:1}}</div>
												</td>
												<td>
													<div class="">  {{overallratingsstore[7]}}</div>
												</td>
												<td>
													<div class="">  <span id="topAvg_{{$index}}"> </span> </div>
												</td>
												<td>
													<div class=""> <span id="botAvg_{{$index}}"> </span> </div>
												</td>
												<!-- <td>
													<div class="rating-star" ng-bind-html="toTrustedHTML(overallratingsstore[4])">
													</div>
												</td> -->
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="block-cover" id="StoresList">
							<div class="block-head">
								<div class="row">
									<div class="col-md-4">
										<h3 class="block-name">Store list</h3>
									</div>
									<div class="col-md-8">
										<div class="pull-right bar-filter">
											<div class="user-search pull-right">
												<input type="text" placeholder="Search Store" data-ng-model="searchText" value="">
												<div class="fa fa-search pointer" ng-click="fn_search_stores();"></div>
											</div>
											<button class="btn black-btn go-btn pull-right ml-1" aria-hidden="true" type="button" ng-click="filterSourceWiseStoreList()">
												<span aria-hidden="true" class="fa fa-arrow-right"></span> Go
											</button>
											<select data-ng-model="sourceWiseStoreListSelectedCity" data-ng-options="city3[0] as city3[1] for city3 in sourceWiseStoreListCities" class="selectpicker ml-1" id="sourceWiseStoreListCitiesID"
											ng-change="resetSearchTextBox()">
											</select>
											<label class="pull-right ml-3">By Locations:</label>
											<div class="pull-right pointer dld-store mr-5" title="Download CSV" ng-click="fn_download_csv();" >Download CSV store list<i class="fa fa-download pull-right mr-0 ml-2" aria-hidden="true"></i></div>
										</div>
									</div>
								</div>
							</div>
							<div class="block-body">
								<div class="grey-bg">
									<div class="table-head">
										<div ng-repeat="colName in sourcewisestorelistsColName track by $index">{{colName}}</div>
									</div>
									<table class="rating-table" id="tblStoreList">
										<tbody>
											<tr>
											  <td class="td-align-center no-data" ng-if="sourcewisestorelists==''" colspan="3" ng-bind="'No Data Found'"></td>
											</tr>
											<tr dir-paginate="sourceWiseStoreList in sourcewisestorelists|itemsPerPage:5" pagination-id="sourceWiseStoreListPaginate" current-page="currentPage">
												<td>
                    	 						{{sourceWiseStoreList[0]}} ({{sourceWiseStoreList[7]}})</td>
												<td ng-class="sourceWiseStoreList[16]>sourceWiseStoreList[20] ? 'text-green': sourceWiseStoreList[20]>sourceWiseStoreList[16] ? 'text-red':''">
												{{sourceWiseStoreList[16]}} (Last week : {{sourceWiseStoreList[20]}})</td>
												<td ng-class="sourceWiseStoreList[17]>sourceWiseStoreList[21] ? 'text-green': sourceWiseStoreList[21]>sourceWiseStoreList[17] ? 'text-red':''">
												{{sourceWiseStoreList[17]}} (Last week : {{sourceWiseStoreList[21]}})</td>
												<td ng-class="sourceWiseStoreList[19]>sourceWiseStoreList[22] ? 'text-green': sourceWiseStoreList[22]>sourceWiseStoreList[19] ? 'text-red':''">
												{{sourceWiseStoreList[19]}} (Last week : {{sourceWiseStoreList[22]}})</td>
												<td>&nbsp;</td>
												<!-- <td>{{sourceWiseStoreList[11]}}</td> --> 
											</tr>
										</tbody>
									</table>
								</div>
								<dir-pagination-controls pagination-id="sourceWiseStoreListPaginate" class="pull-right" max-size="5" direction-links="true" boundary-links="true" template-url="dirPagination.tpl.html"></dir-pagination-controls>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="block-cover" id="TopRatedStores">
							<div class="block-head">
								<div class="row">
									<div class="col-md-6">
										<h3 class="block-name">Top Performing Stores </h3>
									</div>
									<div class="col-md-6">
										<div class="pull-right bar-filter">
											<button class="btn black-btn go-btn pull-right ml-1" aria-hidden="true" type="button" ng-click="filterTopratedStores()">
												<span aria-hidden="true" class="fa fa-arrow-right"></span> Go
											</button>
											<select ng-model="topRatingsSelectedCity" ng-options="city2[0] as city2[1] for city2 in topRatingCities" class="selectpicker ml-1">
											</select>
											<label class="pull-right ml-3">By Locations:</label>
											<select data-ng-model="topRatingsSelectedSource" data-ng-options="source[0] as source[1] for source in topRatingsSources" class="selectpicker ml-1" ng-change="getTopRatingCities();">
											</select>
											<label class="pull-right">By Sources:</label>
										</div>
									</div>
								</div>
							</div>
							<div class="block-body">
								<div class="grey-bg">
									<table class="rating-table" id="topRatedStoreTable">
										<tbody id="topRatedStoreTableBody">
											<tr>
											  <td class="td-align-center no-data" ng-if="topratedstores==''" colspan="3" ng-bind="'No Data Found'"></td>
											</tr>
											<!-- <tr dir-paginate="topratedstore in topratedstores|itemsPerPage:10" pagination-id="topRatedStorePaginate" current-page="topRatedStoreCurrentPage"> -->
											<tr ng-repeat="topratedstore in topratedstores">
												<td>
													<div class="source-logo sl-{{topratedstore[6]}}"></div>
													<div class="store-name"><a href="{{topratedstore[7]}}" target="_blank">{{topratedstore[3]}}</a></div>
													<div class="store-address">{{topratedstore[2]}},{{topratedstore[4]}}</div>
												</td>
												<td>
													<div class="rating-count green">{{topratedstore[1] | number:1}}</div>
												</td>
												<td>
													<div class="rating-star" ng-bind-html="toTrustedHTML(topratedstore[8])">
													</div>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
								<!-- <dir-pagination-controls pagination-id="topRatedStorePaginate" class="pull-right" max-size="10" direction-links="true" boundary-links="true" template-url="dirPagination.tpl.html"></dir-pagination-controls>
								<div class="clearfix"></div> -->
								<div class="clearfix"></div> 
							</div>
						</div>
						<div class="block-cover" id="BottomRatedStores">
							<div class="block-head">
								<div class="row">
									<div class="col-md-6">
										<h3 class="block-name">Bottom Performing Stores</h3>
									</div>
									<div class="col-md-6">
										<div class="pull-right bar-filter">
											<button class="btn black-btn go-btn pull-right ml-1" aria-hidden="true" type="button" ng-click="filterBottomratedStores()">
												<span aria-hidden="true" class="fa fa-arrow-right"></span>&nbsp;Go
											</button>
											<select data-ng-model="bottomRatingsSelectedCity" data-ng-options="city1[0] as city1[1] for city1 in bottomRatingCities" class="selectpicker nbt-select-new " ng-disabled="bottomRatingsCities.length == 0">
											</select>
											<label class="pull-right ml-3">By Locations:</label>
											<select data-ng-model="bottomRatingsSelectedSource" data-ng-options="source[0] as source[1] for source in bottomRatingsSources" class="selectpicker ml-1"  ng-change="getBottomRatingCities();">
											</select>
											<label class="pull-right">By Sources:</label>
										</div>
									</div>
								</div>
							</div>
							<div class="block-body">
								<div class="grey-bg">
									<table class="rating-table" id="botRatedStoreTable">
										<tbody id="botRatedStoreTableBody">
											<tr>
											  <td class="td-align-center no-data" ng-if="bottomratedstores==''" colspan="3" ng-bind="'No Data Found'"></td>
											</tr>
											<tr ng-repeat="bottomratedstore in bottomratedstores">
												<td>
													<div class="source-logo sl-{{bottomratedstore[6]}}"></div>
													<div class="store-name"><a href="{{bottomratedstore[7]}}" target="_blank">{{bottomratedstore[3]}}</a></div>
													<div class="store-address">{{bottomratedstore[2]}} , {{bottomratedstore[4]}}</div>
												</td>
												<td>
													<div class="rating-count orange">{{bottomratedstore[1] | number:1}}</div>
												</td>
												<td>
													<div class="rating-star" ng-bind-html="toTrustedHTML(bottomratedstore[8])">
													</div>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						
						<div class="block-cover" id="CitySourceWiseStores">
							<div class="block-head">
								<div class="row">
									<div class="col-md-6">
										<h3 class="block-name">Rating across stores</h3>
									</div>
								</div>
							</div>
							<div class="block-body">
							<div class="mt-3 mb-3">
							<div class="row">
								<div class="col-md-11">
									<div class="row">
									<div class="col-md-3">
										<div class="dropdown  custom-datepicker">
										<div class="inner-filter-lbl">Start Date</div>
											<a class="dropdown-toggle" id="ratingCitySourceWiseStoreFrom" role="button" data-toggle="dropdown" data-target="_self" href="">
												<div class="input-group">
													<input type="text" class="form-control" data-ng-model="ratingCitySourceWiseStoreFrom" placeholder="From" readonly>
													<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
												</div>
											</a>
											<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
												<datetimepicker data-ng-model="ratingCitySourceWiseStoreFrom" data-before-render="dateRangeValidation($view, $dates, $leftDate, $upDate, $rightDate)" data-datetimepicker-config="{ dropdownSelector: '#ratingCitySourceWiseStoreFrom', modelType: 'DD-MM-YYYY'+' 00:01', minView: 'day' }" />
											</ul>
											<span class="validation-message" ng-show="ratingCitySourceWiseStoreFrom=='' && ratingCitySourceWiseStoreTo!=''">*Required</span>
										</div>
									</div>
									<div class="col-md-3">
										<div class="dropdown  custom-datepicker">
											<div class="inner-filter-lbl">End Date</div>
											<a class="dropdown-toggle" id="ratingCitySourceWiseStoreTo" role="button" data-toggle="dropdown" data-target="_self" href="">
												<div class="input-group">
													<input type="text" class="form-control" data-ng-model="ratingCitySourceWiseStoreTo" placeholder="To" readonly>
													<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
												</div>
											</a>
											<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
												<datetimepicker data-ng-model="ratingCitySourceWiseStoreTo" data-before-render="dateRangeValidation($view, $dates, $leftDate, $upDate, $rightDate)" data-datetimepicker-config="{ dropdownSelector: '#ratingCitySourceWiseStoreTo', modelType: 'DD-MM-YYYY'+' 23:59', minView: 'day' }" />
											</ul>
											<span class="validation-message" ng-show="ratingCitySourceWiseStoreFrom!='' && ratingCitySourceWiseStoreTo==''">*Required</span> 
										</div>
									</div>
									<div class="col-md-3">
										<div class="inner-filter-lbl">By Sources:</div>
										<select data-ng-model="chartRatingsSelectedSource" data-ng-options="source[0] as source[1] for source in chartRatingsSources" class="selectpicker"  ng-change="getChartCities();">
										</select>
									</div>
									<div class="col-md-3">
										<div class="inner-filter-lbl">By Locations</div>
										<select data-ng-model="chartRatingsSelectedCity" data-ng-options="city1[0] as city1[1] for city1 in chartRatingCities" class="selectpicker nbt-select-new " ng-disabled="chartRatingCities.length == 0">
										</select>
									</div>
									</div>
								</div>
								<div class="col-md-1">
								<button class="btn black-btn go-btn pull-right ml-1 mt-4" aria-hidden="true" type="button" ng-click="fn_CitySourceWiseHistoricRating()">
									<span aria-hidden="true" class="fa fa-arrow-right"></span>&nbsp;Go
								</button>
								</div>
							</div>
							<div class="clearfix"></div>
							</div>
								<div class="grey-bg">
									<div id="ratingChart">
									
									</div>
									<table class="rating-table">
										<tbody>
											<!-- <tr>
											  <td class="td-align-center no-data" ng-if="bottomratedstores==''" colspan="3" ng-bind="'No Data Found'"></td>
											</tr> -->
											<!-- <tr dir-paginate="ratingHistory in ratingHistoricValues track by $index|itemsPerPageChart:5" pagination-id="sourceWiseStoreListPaginateChart" current-page="currentPageChart" ng-init='getRatingChart($index,ratingHistory[3],ratingHistory[6],ratingHistory[9],ratingHistory[12],ratingHistory[4],ratingHistory[7],ratingHistory[10],ratingHistory[13])'> -->
											<tr dir-paginate="ratingHistory in ratingHistoricValues|itemsPerPage:5" pagination-id="sourceWiseStoreListPaginateChart" ng-init='getRatingChart($index,ratingHistory[3],ratingHistory[6],ratingHistory[9],ratingHistory[12],ratingHistory[4],ratingHistory[7],ratingHistory[10],ratingHistory[13])'>
												<td style="width:20%">
													<div class="source-logo sl-{{ratingHistory[2]}}"></div>
													<div class="store-name" style="font-size:12px;">{{ratingHistory[1]}}</div>
												</td>
												<!-- <td style="width:20%">
													<div class="rating-count orange pull-left mr-2">{{ratingHistory[3] | number:1}}</div> <div class="store-name">({{ratingHistory[2]}})</div>
													<div class="clearfix"></div>
													<div class="store-address">{{ratingHistory[4]}}</div>
												</td>
												<td style="width:20%">
													<div class="rating-count orange pull-left mr-2">{{ratingHistory[6] | number:1}}</div> <div class="store-name">({{ratingHistory[5]}})</div>
													<div class="clearfix"></div>
													<div class="store-address">{{ratingHistory[7]}}</div>
												</td>
												<td  style="width:20%">
													<div class="rating-count orange pull-left mr-2">{{ratingHistory[9] | number:1}}</div> <div class="store-name">({{ratingHistory[8]}})</div>
													<div class="clearfix"></div>
													<div class="store-address">{{ratingHistory[10]}}</div>
												</td>
												<td>
													<div style="height:100px;width:250px;" id="ratingChart_{{$index}}">{{$index}}</div>
												</td>
												</td> -->
												<td  style="width:80%">
													<div style="height:50px;" id="ratingChart_{{$index}}"></div>
												</td>

											</tr>
										</tbody>
									</table>
								</div>
								<dir-pagination-controls pagination-id="sourceWiseStoreListPaginateChart" class="pull-right" max-size="5" direction-links="true" boundary-links="true" template-url="dirPagination.tpl.html"></dir-pagination-controls>
								<div class="clearfix"></div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-12 mt-3" id="CompetitorComparison" style="display:none;">
			<div class="block-cover">
				<div class="block-head">
					<div class="row">
						<div class="col-md-6">
							<h3 class="block-name">Competitor Comparison</h3>
						</div>
						<div class="col-md-6">
							<div class="pull-right bar-filter">
								<button class="btn black-btn go-btn pull-right ml-1" aria-hidden="true" type="button" ng-click="filterCompetitorComparision()">
									<span aria-hidden="true" class="fa fa-arrow-right"></span> Go
								</button>
								<select data-ng-model="competitorComparisionSelectedCity" data-ng-options="city1[0] as city1[1] for city1 in competitorComparisionCities" class="selectpicker nbt-select-new " ng-disabled="competitorComparisionCities.length == 0">
								</select>
								<label class="pull-right ml-3">By Locations:</label>
								<select data-ng-model="competitorComparisionSelectedSource" data-ng-options="source[0] as source[1] for source in competitorComparisionSources" class="selectpicker nbt-select-new "  ng-change="getCompetitorComparisionCities();">
								</select>
								<label class="pull-right">By Sources:</label>
							</div>
						</div>
					</div>
				</div>
				<div class="block-body">
					<div class="grey-bg">
						<table class="rating-table">
							<tbody>
								<tr>
								  <td class="td-align-center no-data" ng-if="competitorcomparisionstores==''" colspan="3" ng-bind="'No Data Found'"></td>
								</tr>
								<tr dir-paginate="competitorcomparisionstore in competitorcomparisionstores|orderBy:sortType:sortReverse|itemsPerPage:10" pagination-id="storeListPaginate">
									<td>
										<div class="source-logo cc-{{competitorcomparisionstore[1]}}"></div>
										<div class="store-name">{{competitorcomparisionstore[0]}}</div>
									</td>
									<td>
										<div class="rating-count green">{{competitorcomparisionstore[4] | number:1}}</div>
									</td>
									<td>
										<div class="rating-star" ng-bind-html="toTrustedHTML(competitorcomparisionstore[3])">
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="ratingModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h2 class="modal-title" id="myModalLabel">Chart Details	</h2>
      </div>
      <div class="modal-body" id="ratingChartModal">
    		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

