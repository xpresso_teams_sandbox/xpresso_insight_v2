<div id="ticketManagement">
	<div class="row">
		<div class="col-md-12">
			<div class="page-title">
				<div class="row">
					<div class="col-md-6"><h2>Ticket Management</h2></div>
					<div class="col-md-6">
						<button type="button" ng-if="isRouted != '0' && labelVal < 1" onclick="goBack()" class="btn btn-default navbar-btn pull-right">
				        	<span class="glyphicon glyphicon-chevron-left">Back</span>
				        </button>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-12" id="ticketInfo">
			<div class="block-cover">
				<div class="block-head">
					<div class="row">
						<div class="col-md-12">
							<h3 class="block-name pull-left">Ticket History</h3>
							<!-- <div class="pull-right text-right mt-2">
								<h3 class="label bg-green ">12 Open Tickets</h3>
							</div>
							<div class="clearfix"></div> -->
						</div>
					</div>
				</div>
				<div class="block-body">
					<div class="row">
						<div class="col-md-12 inner-filter mt-3">
							<div class="inner-filter-col">
								<div class="inner-filter-lbl">Select Source</div>
								<!-- <select data-ng-model="$parent.sentimentalSource" data-ng-options="source[1] as source[0] for source in sentimentalSources" class="selectpicker" id="sentimentalsource" ng-change="getCities();fn_overall_sentiment_onChange_dateSet()">
								</select> -->
								<select  class="selectpicker" id="ticketSource" ng-model="ticketSource">
									<option value="2">Twitter</option>
									<option value="1">Facebook</option>
								</select>
							</div>
							<div class="inner-filter-col">
								<div class="inner-filter-lbl">Select Status</div>
								<!-- <select data-ng-model="$parent.sentimentalSource" data-ng-options="source[1] as source[0] for source in sentimentalSources" class="selectpicker" id="sentimentalsource" ng-change="getCities();fn_overall_sentiment_onChange_dateSet()">
								</select> -->
								<select  class="selectpicker" id="statusFilter" ng-model="statusFilter">
									<!-- <option value="0">All</option> -->
									<option value="All">All</option>
									<option value="NEW">New</option>
									<option value="WIP">WIP</option>
									<option value="Invalid">Invalid</option>
									<option value="Online Refund - Closed">Online Refund - Closed</option>
									<option value="Closed">Close</option>
								</select>
							</div>
							<div class="dropdown  custom-datepicker">
								<div class="inner-filter-lbl">Start Date</div>
								<a class="dropdown-toggle" id="ticketFrom" role="button" data-toggle="dropdown" data-target="_self" href="">
									<div class="input-group">
										<input type="text" class="form-control" data-ng-model="ticketFrom" placeholder="From" readonly>
										<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
									</div>
								</a>
								<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
									<datetimepicker data-ng-model="ticketFrom" data-before-render="dateRangeValidation($view, $dates, $leftDate, $upDate, $rightDate)" data-datetimepicker-config="{ dropdownSelector: '#ticketFrom', modelType: 'DD-MM-YYYY'+' 00:01', minView: 'day' }" />
								</ul>
								<span class="validation-message" ng-show="ticketFrom=='' && ticketTo!=''">*Required</span>
							</div>
							<div class="dropdown  custom-datepicker">
								<div class="inner-filter-lbl">End Date</div>
								<a class="dropdown-toggle" id="ticketTo" role="button" data-toggle="dropdown" data-target="_self" href="">
									<div class="input-group">
										<input type="text" class="form-control" data-ng-model="ticketTo" placeholder="To" readonly>
										<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
									</div>
								</a>
								<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
									<datetimepicker data-ng-model="ticketTo" data-before-render="dateRangeValidation($view, $dates, $leftDate, $upDate, $rightDate)" data-datetimepicker-config="{ dropdownSelector: '#ticketTo', modelType: 'DD-MM-YYYY'+' 23:59', minView: 'day' }" />
								</ul>
								<span class="validation-message" ng-show="ticketFrom!='' && ticketTo==''">*Required</span> 
							</div>
							<button class="btn primary-btn go-btn mt-4" aria-hidden="true" type="button" ng-click="getTicketList()">
								<span aria-hidden="true" class="fa fa-arrow-right"></span> Go
							</button>
							<div class="pull-right text-right mt-5">
								<h3 class="label bg-green " ng-if="labelVal != '0'" ng-bind="labelVal"><!-- 12 Open Tickets --></h3>
							</div>
						</div>
					</div>
					<!-- <div class="grey-bg"> -->
						<!-- <div class="table-head">
							<div>Ticket No.</div>
							<div>Ticket Date</div>
							<div>Customer Post</div>
							<div>Post Details</div>
							<div>Status</div>
							<div>Action</div>
						</div> -->
						<table class="table table-theme mt-3">
							<tr>
								<th style="width:15%" class="pointer"
									ng-click="sortType='-1'; sortReverse = !sortReverse">Ticket No.
									<span ng-show="!sortReverse"
									class="fa fa-caret-up"></span> <span
									ng-show="sortReverse"
									class="fa fa-caret-down"></span>
								</th>
								<th style="width:15%">Ticket Date</th>
								<th style="width:40%">Customer Post</th>
								<th style="width:10%">CCIR Status</th>
								<th style="width:10%" class="pointer"
									ng-click="sortType='-3'; sortReverse = !sortReverse">Status
									<span ng-show="!sortReverse"
									class="fa fa-caret-up"></span> <span
									ng-show="sortReverse"
									class="fa fa-caret-down"></span>
								</th>
								<th style="width:10%">Action</th>
							</tr>
							<tr>
							  <td class="td-align-center no-data" ng-if="tickets==''" colspan="6" ng-bind="'No Data Found'"></td>
							</tr>
							<tr
								dir-paginate="ticket in tickets|orderBy:sortType:sortReverse|itemsPerPage:5" current-page="ticketCurPage"
								pagination-id="ticket-paginate">
								<td ng-bind="ticket[1]"></td>
								<td ng-bind="ticket[4]"></td>
								<td>
									<span class="deep-list-date ng-binding" ng-bind="ticket[6]"></span>
									<p class="mb-1"><span  ng-bind="ticket[5]"></span>
									<a  href="{{ ticket[8] }}" target="_blank" class="read-more" title="Redirect to original post"><i class="fa fa-external-link"></i></a></p>
									<em class="deep-list-date">By : <a  href="https://twitter.com/{{ ticket[9] }}" target="_blank" title="Redirect to user wall" class="read-more">{{ ticket[9] }}</a></em>
								</td>
								<!-- <td ng-if="ticket[3] !='NEW'"><a href=""  class="ticket-action" data-toggle="modal" data-target="#ticketDetailsModal"><i class="fa fa-external-link" ng-click="showTicketDetails(ticket[1])"></i></a></td> -->
								<td><a href=""  class="ticket-action" data-toggle="modal" data-target="#ticketDetailsModal"><i class="fa fa-external-link" ng-click="showTicketDetails(ticket[1],ticket[3],ticket[6],ticket[5])"></i></a></td>
								<!-- <td ng-if="ticket[3] =='NEW'"><a href=""  class="ticket-action disable-comment" data-toggle="modal" data-target="#ticketDetailsModal"><i class="fa fa-external-link" ></i></a></td> -->
								<td><span class="label label-{{ticket[3]}}" ng-bind="ticket[3]"></span></td>
								<td><a href="" ng-class="ticket[3]=='Closed' || ticket[3]=='Online Refund - Closed'?'ticket-action': 'ticket-action disable-comment'" data-toggle="modal" data-target="#ticketInfoModal"><i class="fa fa-comments" ng-click="setCommentText(ticket[5],ticket[7],ticket[2]);"></i></a></td>
								<!-- <td><a href="" class="ticket-action" data-toggle="modal" data-target="#ticketInfoModal"><i class="fa fa-comments" ng-click="setCommentText(ticket[5],ticket[7],ticket[2]);"></i></a></td> -->
							</tr>
						</table>
						<dir-pagination-controls pagination-id="ticket-paginate"
								class="pull-right" max-size="5" direction-links="true"
							boundary-links="true" template-url="dirPagination.tpl.html">
						</dir-pagination-controls>
						<div class="clearfix"></div>
					<!-- </div> -->
				</div>
			</div>
		</div>
	</div>
</div>


<div id="ticketDetailsModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Detailed Feedback Communication</h4>
      </div>
      <div class="modal-body">
      <div class="row">
	      <div class="col-md-12">
	      	<!-- <div class="table-head">
				<div>Comment </div>
				<div>Status</div>
				<div>Date</div>
				<div>Handeled By</div>
			</div> -->
			<table class="table table-theme">
				<tbody>
					<tr>
						<th>Customer care comment</th>
						<th>Status</th>
						<th>Handeled By</th>
					</tr>
					<tr>
						<td>
							<span class="deep-list-date" ng-bind="userPostDate"></span>
							<p class="mb-0" ng-bind="userPostText"></p>
						</td>
						<td>
							<span ng-class="userPoststatus=='NA'?'label label-default': 'label label-NEW'"   ng-bind="userPoststatus"></span>
						</td>
						<td><p class="mb-0">NA</p></td>
					</tr>
					<tr
						dir-paginate="ticketDetail in ticketDetails|orderBy:sortType:sortReverse|itemsPerPage:5" current-page="ticketDetailsCurPage"
						pagination-id="ticketDetail-paginate">
						<td>
							<span class="deep-list-date" ng-bind="ticketDetail[3]"></span>
							<p class="mb-0" ng-bind="ticketDetail[0]"></p>
						</td>
						<td>
							<span class="label label-{{ticketDetail[1]}}" ng-bind="ticketDetail[1]"></span>
						</td>
						<td><p class="mb-0"  ng-bind="ticketDetail[2]"></p></td>
					</tr>
				</tbody>
			</table>
			<dir-pagination-controls pagination-id="ticketDetail-paginate"
				class="pull-right" max-size="5" direction-links="true"
			boundary-links="true" template-url="dirPagination.tpl.html">
		</div>
      </div>
      </div>
    </div>

  </div>
</div>

<div id="ticketInfoModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Submit Feedback for Customer</h4>
      </div>
      <div class="modal-body">
        <div class="row">
        	<div class="col-md-4">
        		<label>User</label>
        	</div>
        	<div class="col-md-8" ng-bind="userOriginalPost"></div>
        </div>
        <div class="row">
        	<div class="col-md-4">
        		<label>Source</label>
        	</div>
        	<div class="col-md-8" ng-bind="sourceTypeName"></div>
        </div>
        <div class="row">
        	<div class="col-md-4">
        		<label>Comment</label>
        	</div>
        	<div class="col-md-8">
				<div class="autocomplete-combo mt-0">
					<input type="text" id="feedbackUserInputTicket" list="msgList" name="singleMsg" placeholder="Select Message">
					<datalist name="dataListMsg" id="msgList">
						<option>Your ticket has been closed. Thanks for your feedback.</option>	
					</datalist>
					<i class="fa fa-angle-down"></i>
				</div>
			</div>
        </div>
      </div>
      <div class="modal-footer">
        <div class="row">
                <div class="col-md-12 col-sm-12">
                	<div class="row">
                		<div class="col-sm-6">
	                		<!-- Loader Animation -->
		                  	<div id="responseLoader" >
			                  	<div class="sendingText">Sending</div>
			                    <div id="floatBarsG">
									<div id="floatBarsG_1" class="floatBarsG"></div>
									<div id="floatBarsG_2" class="floatBarsG"></div>
									<div id="floatBarsG_3" class="floatBarsG"></div>
									<div id="floatBarsG_4" class="floatBarsG"></div>
									<div id="floatBarsG_5" class="floatBarsG"></div>
									<div id="floatBarsG_6" class="floatBarsG"></div>
									<div id="floatBarsG_7" class="floatBarsG"></div>
									<div id="floatBarsG_8" class="floatBarsG"></div>
								</div>
		                  	</div>
		                  <!-- Ends Loader animation -->
                		</div>
                		<div class="col-sm-6">
                			<button type="button" class="btn btn-default purple-btn" onClick="fn_sendFeedback('t5')">Submit</button>
					        <button type="button" class="btn btn-default close-btn" data-dismiss="modal">Close</button>
					        <input id="postIdTicket" type="text" value="" name="postId" ng-model="hidPostTypeID" style="height:1px;width:1px;opacity:-0.1"/>
                		</div>
                	</div>
                </div>
              </div>
      </div>
    </div>

  </div>
</div>