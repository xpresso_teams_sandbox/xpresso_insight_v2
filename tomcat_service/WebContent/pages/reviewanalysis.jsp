<%@include file="/WEB-INF/jspf/topFixedDailySnapshotReviewes.jspf" %>
<div id="reviewAnalysis">
	<div class="row">
		<div class="col-md-12">
			<div class="block-cover">
				<div class="block-body">
					<div class="inner-filter mt-3 mb-5 context-filter-wapper">
						<i class="fa fa-filter pull-left" aria-hidden="true"></i>
						<!-- <select data-ng-model="selectedSource" data-ng-options="source[0] as source[1] for source in review_analysis_sources" class="selectpicker" id="source"  ng-change="getCities()"> -->
						<select data-ng-model="selectedSource" data-ng-options="source[0] as source[1] for source in review_analysis_sources" class="selectpicker" id="source"  ng-change="resetAllSelectors()"> 
						</select>
						<select data-ng-model="selectedCity" data-ng-options="city[0] as city[1] for city in array_cities" class="selectpicker" id="city" ng-disabled="selectedSource==5" ng-change="getLocalities()">
							<option style="display: none;" value="">All Cities</option>
						</select>
						<select data-ng-model="selectedLocality" data-ng-options="locality as locality for locality in localities" class="selectpicker" id="locality" ng-disabled="selectedSource==5">
							<option style="display: none;" value="">All Locality</option>
						</select>
						<select class="selectpicker pull-left" id="sentiment" ng-model="selectedSentiment">
							<option value="100">All Sentiment</option>
							<option value="1">Positive</option>
							<option value="-1">Negative</option>
							<option value="0">Neutral</option> 
						</select>
						<select data-ng-model="selectedOwnership"  class="selectpicker pull-left" id="ownership">
							<option value="4">All </option>
							<option value="3">DIL</option>
							<option value="2">SFIPL</option>
							<option value="1">Equity</option> 
						</select>
						<div class="dropdown pull-left">
							<a class="dropdown-toggle" id="analyticsFrom" role="button"
								data-toggle="dropdown" data-target="_self" href="">
								<div class="input-group">
									<input type="text" class="form-control" data-ng-model="selectedStartDate" placeholder="From" readonly> 
									<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
								</div>
							</a>
							<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
								<datetimepicker data-ng-model="selectedStartDate" data-before-render="dateRangeValidationReviewAnalysis($view, $dates, $leftDate, $upDate, $rightDate)" data-datetimepicker-config="{ dropdownSelector: '#analyticsFrom', modelType: 'DD-MM-YYYY'+' 00:00', minView: 'day'  }" />
							</ul>
						</div>
					  	<div class="dropdown pull-left">
					  		<a class="dropdown-toggle" id="analyticsTo" role="button" data-toggle="dropdown" data-target="_self" href="">
								<div class="input-group">
								  <input type="text" class="form-control" data-ng-model="selectedEndDate" placeholder="To" readonly>
								  <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
								</div>
							</a>
							<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
							  <datetimepicker data-ng-model="selectedEndDate" data-before-render="dateRangeValidationReviewAnalysis($view, $dates, $leftDate, $upDate, $rightDate)" data-datetimepicker-config="{ dropdownSelector: '#analyticsTo', modelType: 'DD-MM-YYYY'+' 23:59', minView: 'day' }"/>
							</ul>
						</div>
						<button class="btn black-btn go-btn pull-left" aria-hidden="true" type="button" ng-click="fn_Review_Sentiment_Distribution(1,'from-others')">
							<span aria-hidden="true" class="fa fa-arrow-right"></span> Go 
						</button>
						<div class="clearfix"></div>
					</div>
					<div id="reviewAnalysisWapper">
						<div class="block-head">
							<div class="row">
								<div class="col-md-6">
									<h3 class="block-name">Review Analysis</h3>
								</div>
								<div class="col-md-6">
									<span class="block-name pull-right" ng-if="selectedSource==1"><b>Last Data pulling date:</b> {{LastPullingCurDate}}</span>
									<span class="block-name pull-right" ng-if="selectedSource==2||selectedSource==3||selectedSource==4"><b>Last Data pulling date:</b> {{mondayDate}}</span>
									<span class="block-name pull-right" ng-if="selectedSource==5"><b>Last Data pulling date:</b> {{LastPullingDateforAppStore}}</span>
									
								</div>
							</div>
						</div>
						<div class="block-body">
							<table class="table table-hover nbt-table mt-3">
								<thead>
				                  <tr> 
				                    <th>Review Text</th>
				                    <th>Overall Sentiment</th>
				                    <th>User Name</th>
				                    <!-- <th class="pointer"
											ng-click="sortType='-5'; sortReverse = !sortReverse">
											<span ng-show="sortType == '-5' && !sortReverse"
											class="fa fa-caret-up"></span> <span
											ng-show="sortType == '-5' && sortReverse"
											class="fa fa-caret-down"></span>User Rating</th> -->
									<th>Rating</th>
				                    <th>Business Aspects</th>
				                    <th>&nbsp;</th>
				                    <th>&nbsp;</th>
				                  </tr>
				                </thead>
				                <tbody id="revAnalysisList">
				                	<tr>
							  			<td class="td-align-center no-data" ng-if="review_analysis==''" colspan="6" ng-bind="'No Data Found'"></td>
									</tr>
									<!-- <tr dir-paginate="review in review_analysis|itemsPerPage:10" pagination-id="review_analysis_id" current-page="currentPage"> -->
									<tr dir-paginate="review in review_analysis|itemsPerPage:10" 
                  					pagination-id="review_analysis_id" current-page="reviewPagination.reviewCurrentPage" 
                  					total-items="totalReviewCount"  ng-init="limit= 100">
					                    <td ng-if="review[2]!=''">
					                    	<span class="review-date" ng-bind="review[0]"></span>
	                    					<span ng-bind="review[2]" class="break-word"></span>
					                    	<a ng-if="showHide==1" href="https://www.zomato.com/review/{{review[1]}}" target="_blank" class="read-more" title="Reply"><i class="fa fa-reply"></i></a>
					                    	<a ng-if="showHide==5" href=https://itunes.apple.com/us/review?id={{review[1]}}&type=Purple%20Software target="_blank" class="read-more" title="Reply"><i class="fa fa-external-link"></i></a>
					                    	<a href="javaScript:void()" data-toggle="modal" data-target="#snippetModalForReviewAnalysis" ng-click="fn_showPostDetailsForReviewAnalysisTab(review[1])" class="modal-link-btn">Click to view snippet level distribution</a>
					                    </td>
					                    <td ng-if="review[2]==''">
	                    					<span>N/A</span>
					                    </td>
					                    <td ng-class="review[3]=='Positive'? 'text-green': review[3]=='Negative' ?
					                     'text-red':'text-yellow'"><strong class="pull-left senti-txt" id=sentiment_{{review[1]}}>
					                     <span ng-bind="review[3]"></span></strong>
					                     <div ng-if="authRole==1" class="dropdown senti-dropdown">
										    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
										    <span class="caret"></span></button>
										    <ul class="dropdown-menu">
										      <li><a ng-click="updateReviewAnalysisOverallSentiment(1,review[1],review[3],review[2],review[0])">Positive</a></li>
										      <li><a ng-click="updateReviewAnalysisOverallSentiment(-1,review[1],review[3],review[2],review[0])">Negative</a></li>
										      <li><a ng-click="updateReviewAnalysisOverallSentiment(0,review[1],review[3],review[2],review[0])">Neutral</a></li>
										    </ul>
										</div>
										<i ng-if="authRole==1" class="fa fa-question-circle tooltipforupdatedSentiment" data-toggle="tooltip" id="tt_{{review[1]}}">
  											<span class="tooltiptextforupdatedSentiment">To change the displayed sentiment permanently click on the dropdown arrow</span>
										</i>
					                     </td>
					 					<td>
					 						<span ng-bind="review[4]" ></span> 
					 						 <a ng-if="showHide==5" href="{{review[7]}}" target="_blank" class="read-more" title="Reply"><i class="fa fa-external-link"></i></a> 
					 					</td>
					                   	<td><div class="rating-count green">{{review[5] | number:1}}</div></td>
					                   	<!-- <td ng-bind-html="toTrustedHTML(review[9])"> -->
					                   <!--  <td ><span ng-bind="review[6]" ></span></td> -->
					                   	<td ng-bind-html="toTrustedHTML(review[11])">
					                    </td>
					                    <td>
											<!-- <div class="rating-star" ng-bind-html="toTrustedHTML(review[7])"></div> -->
											<div class="rating-star" ng-bind-html="toTrustedHTML(review[9])"></div> 
										</td> 
									</tr>
				                </tbody>
							</table>
							<!-- <dir-pagination-controls pagination-id="review_analysis_id" class="pull-right" max-size="5" direction-links="true" boundary-links="true"></dir-pagination-controls> -->
							<dir-pagination-controls pagination-id="review_analysis_id" class="pull-right" max-size="5" direction-links="true" 
				              boundary-links="true"  on-page-change="reviewPageChanged(newPageNumber)"></dir-pagination-controls>
				            </div> 
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<!--snapshot modal  -->
    
  <div  class="modal fade" id="snapshotReviewAnalysisModal" role="dialog">
<div class="modal-dialog modal-md">
      <div class="modal-content">
      <div ng-if="postSnippets.length==0 && noSnippets == ''" class="loader"></div>
       <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
	      <h4 class="modal-title" style ="text-align:left;"><span ng-if="feedbackOrComplaint=='Complaint'">
          	List of Complaints
          </span>
          <span ng-if="feedbackOrComplaint=='Feedback'">
          List of Feedbacks
          </span></h4>
        </div>     
	    <div class="modal-body text-center ">
          	<div class="review-tooltip-tbl" style="overflow:scroll; height:300px">
          			<table class="table snippet-table">
          			<tr>
          				<th style ="text-align:left;"><span ng-if="feedbackOrComplaint=='Complaint'">
          				Complaints
          				</span>
          				<span ng-if="feedbackOrComplaint=='Feedback'">
          				Feedbacks
          				</span>
          				</th>
          			</tr>
          			<tr ng-if = "noSnippets != '' ">
          				<td style ="text-align:left">
          					{{noSnippets}}
          				</td>
          			</tr>
          			<tr ng-if = "noSnippets == '' " ng-repeat="snippet in postSnippets">
          				<td style ="text-align:left">
          					<span ng-bind="snippet[1]" class="deep-list-date"></span>
          					<span ng-bind="snippet[0]"></span>
          				</td>
          			</tr>
          		</table>
          	</div>
        </div>
      </div>
    </div>
  </div>
