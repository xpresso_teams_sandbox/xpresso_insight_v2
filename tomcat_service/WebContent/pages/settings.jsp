<div class="row" style="display:none;">
  <div class="col-md-12">
    <div id="setting" class="block-cover">
      <div class="block-head">
      <div class="row">
			<div class="col-md-12">
				<h3 class="block-name">Select compertitors</h3>
			</div>
		</div>
        
      </div>
      <div class="block-body clearfix">
      	<div class="row mt-3 mb-3">
             <div class="col-md-3" ng-repeat="company in companies" ng-hide="(LoggedInPrimaryCompanyID == company[0])">
		  	<div class="company-name">
		  		<input type="checkbox" checklist-model="selectedSecondaryCompany" ng-checked="isCheckedSelectedCompany(company[0])!=-1" ng-class="{'checked':isCheckedSelectedCompany(company[0])!=-1}" ng-click="updateSecondarySelection($event, company[0])" checklist-value="company[0]"> 
		  		<div>{{company[1]}}</div><span class="tick"></span>
		  	</div>
		  </div>
		</div>
      </div>
    </div>
    
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div id="setting" class="block-cover">
      <div class="block-head">
      	<div class="row">
			<div class="col-md-12">
				<h3 class="block-name">Manage your profile</h3>
			</div>
		</div>
      </div>
      <div class="block-body">
      <div class="row">
        <div class=" col-lg-6 col-md-6 col-sm-12 padding-lt-0 summary-content">
          <div class="activeprofile sm-admin">
            <div class="block-cover">
              <div class="block-head">
	              <div class="row">
	              	<div class="col-md-12">
	              		<i class="fa fa-user"></i>
						<h3 class="block-name">Profile details</h3>
					</div>
				</div>
              </div>
              <div class="block-body padding-0">
                <div>
                  <div>
                    <div class="admin-summary border-0">
                      <div class="summary-panel">
                        <h4 class="">Admin details</h4>
                        <table class="table margin-0">
                          <tbody>
                            <tr>
                              <td width="40%"><b>Name: </b> <span ng-bind="authName"></span></td>
                              <td><b>Email: </b> <span ng-bind="authEmail"></span></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div class="summary-panel">
                        <h4>Admin social platform details</h4>
                        <table class="table margin-0">
                          <thead>
                            <tr>
                              <th width="40%">Source Name</th>
                              <th>Page Name</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr ng-repeat="detail in userInfo.sourcesDetailsList">
                              <td><span ng-bind="detail.name"></span></td>
                              <td ng-if="detail.sourceTypeId == 1"><span ng-bind="'http://www.facebook.com/'+ detail.sourceURL"></span></td>
                              <td ng-if="detail.sourceTypeId == 2"><span ng-bind="'http://www.twitter.com/@'+ detail.sourceURL"></span></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <!--<div class="summary-panel">
						  <h4>Admin user details</h4>
						  <table class="table margin-0 col-lg-12" ng-if="userInfo.usersList">
							  <thead>
								  <tr>
									<th>User Name</th>
									<th>User Email</th>
								  </tr>
							  </thead>
							  <tbody>
								  <tr ng-repeat="user in userInfo.usersList">
									<td>{{ user.userName }}</td>
									<td>{{ user.userEmail }}</td>
								  </tr>
							  </tbody>
						  </table>
						</div>-->
                      <!-- <div class="summary-panel">
                        <h4>Competitors details</h4>
                        <table class="table margin-0 last-row-border-0">
                          <thead>
                            <tr>
                              <th width="35%">Competitor Name</th>
                              <th>Source</th>
                              <th>Url</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr ng-repeat="comp in userInfo.competitorsList">
                              <td rowspan="{{comp.competitorSources.length}}"><span ng-bind="comp.competitorName"></span></td>
                              <td rowspan="1"><span ng-bind="comp.competitorName"></span></td>
                              <td>{{comp.competitorSources[0].name}}</td>
                              <td ng-if="comp.competitorSources[0].sourceTypeId == 1"><span ng-bind="'http://www.facebook.com/'+ comp.competitorSources[0].sourceURL"></span></td>
                              <td ng-if="comp.competitorSources[0].sourceTypeId == 2"><span ng-bind="'http://www.twitter.com/@'+ comp.competitorSources[0].sourceURL"></span></td>
                            </tr>
                            <tr ng-repeat="detail in comp.competitorSources" ng-if="$index>0">
                              <td><span ng-bind="detail.name"></span></td>
                              <td ng-if="detail.sourceTypeId == 1"><span ng-bind="'http://www.facebook.com/'+ detail.sourceURL"></span></td>
                              <td ng-if="detail.sourceTypeId == 2"><span ng-bind="'http://www.twitter.com/@'+ detail.sourceURL"></span></td>
                            </tr>
                          </tbody>
                        </table>
                      </div> -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="setting-wrapper col-lg-6 col-md-6 col-sm-12 padding-rt-0">
          <div class="setting-wrapper-content">
            <div class="block-head">
            	<div class="row">
	              	<div class="col-md-12">
	              		<i class="fa fa-lock"></i>
						<h3 class="block-name"> Reset Password</h3>
					</div>
				</div>
            </div>
            <div class="block-body">
            <div class="mt-2 mb-3">
              <div class="error-message" ng-show="wrongpassword">
                <p>Your old password is wrong</p>
              </div>
              <%String resetPasswordUrl = request.getContextPath() + "/services/user/updatepassword"; %>
              <form id="resetPassword" name="resetPasswordForm" action=<%=resetPasswordUrl%> method="POST">
                <input type="hidden" name="adminDetails" id="adminDetails" value="" />
                <input type="hidden" name="adminEmail" id="adminEmail" value="" />
                <div class="row">
                  <div class="col-md-12">
                    <div class="row">
                      <div class="col-md-12">
                        <label>Old Password</label>
                      </div>
                      <div class="col-md-12">
                        <input type="password" name="oldPassword" ng-model="oldPassword" required/>
                        <span class="validation-message" ng-show="resetPasswordForm.oldPassword.$touched && resetPasswordForm.oldPassword.$invalid">Old password cannot be blank.</span> </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="row">
                      <div class="col-md-12">
                        <label>New Password</label>
                      </div>
                      <div class="col-md-12">
                        <input type="password" name="newPassword" ng-model="newPassword" ng-minlength="8" required/>
                        <span class="validation-message" ng-show="resetPasswordForm.newPassword.$touched && resetPasswordForm.newPassword.$invalid">New password should have minimum 8 charachters.</span> <span class="validation-message" ng-show="resetPasswordForm.newPassword.$valid && resetPasswordForm.oldPassword.$valid && oldPassword.toUpperCase()===newPassword.toUpperCase()">New password must not match with old password.</span> </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="row">
                      <div class="col-md-12">
                        <label>Confirm Password</label>
                      </div>
                      <div class="col-md-12">
                        <input type="password" name="confirmPassword" ng-model="confirmPassword" required/>
                        <span class="validation-message" ng-show="resetPasswordForm.confirmPassword.$touched && resetPasswordForm.confirmPassword.$invalid">Please confirm new password.</span> <span class="validation-message" ng-show="resetPasswordForm.confirmPassword.$touched && resetPasswordForm.confirmPassword.$valid && newPassword!=confirmPassword">Confirm password must match with New password.</span> </div>
                    </div>
                  </div>
                </div>
                <div class="btn-con clearfix">
                  <button class="btn btn-primary col-md-12 col-sm-12 " ng-disabled="oldPassword.toUpperCase()===newPassword.toUpperCase()||newPassword!=confirmPassword||!oldPassword||!newPassword||!confirmPassword||newPassword.length<8" type="submit">Reset Password <i class="fa fa-refresh"></i></button>
                </div>
              </form>
            </div>
         </div>
          </div>
        </div>
      </div>
      </div>
    </div>
    <!-- <div class="block-body profile-controller">
        <div class="setting-wrapper">
          <div class="row">
            <div class="col-sm-4" ng-repeat="topicProfile in userInfo.topicProfiles">
              <h3>{{topicProfile.profileType}}</h3>
              <div class="topice-listing" ng-repeat="detail in userInfo.sourcesDetailsList | filter:sourceFilter(topicProfile)">
              	<i class="fa fa-pencil"></i>
              	<a href="javascript:void()" title="{{detail.sourceURL}}" data-toggle="modal" data-target="#sprofile{{$parent.$index}}{{$index}}">{{detail.name}} Account</a>
                <div class="modal fade" id="sprofile{{$parent.$index}}{{$index}}" role="dialog">
                  <div class="modal-dialog modal-md">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title">Change your {{detail.name}} account</h4>
                      </div>
                      <div class="modal-body">
                        <div class="form-group row">
                          <div class="col-sm-4">
                            <label>{{detail.name}} Url</label>
                          </div>
                          <div class="col-sm-8">
                            <input type="text" ng-model="detail.sourceURL" class="form-control"/>
                          </div>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button class="btn btn-primary" type="button" ng-click="submit()">Submit</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <hr />
          <div class="row">
            <div class="col-sm-4">
              <h3>Users</h3>
              <div class="user-listing" ng-repeat="user in userInfo.usersList">
              	<i class="fa fa-user"></i>
                <a href="javascript:void()" title="{{user.userName}}" data-toggle="modal" data-target="#user{{$index}}">{{user.userName}}</a>
                <div class="modal fade" id="user{{$index}}" role="dialog">
                  <div class="modal-dialog modal-md">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title">Edit user account</h4>
                      </div>
                      <div class="modal-body">
                        <div class="form-group row">
                          <div class="col-sm-4">
                            <label>Name</label>
                          </div>
                          <div class="col-sm-8">
                            <input type="text" ng-model="user.userName" class="form-control"/>
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-sm-4">
                            <label>Email</label>
                          </div>
                          <div class="col-sm-8">
                            <input type="text" ng-model="user.userEmail" class="form-control"/>
                          </div>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button class="btn btn-primary" type="button" ng-click="submit()">Submit</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-4">
              <h3>Keywords</h3>
              <div class="keyword-listing" ng-repeat="key in userInfo.keywords">
              	<i class="fa fa-tag"></i>
                <a href="javascript:void()" data-toggle="modal" data-target="#key{{$index}}">{{key.keyword}}</a>
                <div class="modal fade" id="key{{$index}}" role="dialog">
                  <div class="modal-dialog modal-md">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title">Edit Keywords</h4>
                      </div>
                      <div class="modal-body">
                        <div class="form-group row">
                          <div class="col-sm-4">
                            <label>Keyword</label>
                          </div>
                          <div class="col-sm-8">
                            <input type="text" ng-model="key.keyword" />
                          </div>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button class="btn btn-primary" type="button" ng-click="submit()">Submit</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-4">
              <h3>Competitors</h3>
              <div class="competitors-listing" ng-repeat="competitor in userInfo.competitorsList">
              	<i class="fa fa-random"></i>
              	<a href="javascript:void()" data-toggle="modal" data-target="#competitor{{$index}}">{{competitor.competitorName}}</a>
                <div class="modal fade" id="competitor{{$index}}" role="dialog">
                  <div class="modal-dialog modal-md">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title">Change your competitor details</h4>
                      </div>
                      <div class="modal-body">
                        <div class="form-group row">
                          <div class="col-sm-4">
                            <label>Competitor Name</label>
                          </div>
                          <div class="col-sm-8">
                            <input type="text" ng-model="competitor.competitorName" class="form-control"/>
                          </div>
                        </div>
                        <div class="form-group row" ng-repeat="smdetail in competitor.competitorSources">
                          <div class="col-sm-4">
                            <label>{{smdetail.name}}</label>
                          </div>
                          <div class="col-sm-8">
                            <input type="text" ng-model="smdetail.sourceURL" class="form-control"/>
                          </div>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button class="btn btn-primary" type="button" ng-click="submit()">Submit</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> --> 
    
  </div>
</div>
