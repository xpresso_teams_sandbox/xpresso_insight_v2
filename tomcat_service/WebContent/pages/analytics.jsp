<%@include file="/WEB-INF/jspf/topFixedDailySnapshot.jspf" %>
<div id="analytics">
	<div class="row">
		<div class="col-md-12" id="navDiv3">
			<section id="senti-distrub">
				<div class="block-cover">
				
				
				
					<div class="block-head">
						<div class="row">
							<div class="col-md-12">
								<i class="fa fa-pie-chart" aria-hidden="true"></i>
								<h3 class="block-name">Overall Sentiment Distribution</h3>
							</div>
						</div>
					</div>
					<div class="block-body">
						<div class="row">
							<div class="col-md-12 mt-2 mb-3">
								<div class="row">
									<div class="col-md-12 inner-filter">
										<div class="inner-filter-col">
											<div class="inner-filter-lbl">Select Source</div>
											<!-- <select data-ng-model="$parent.sentimentalSource" data-ng-options="source[1] as source[0] for source in sentimentalSources" class="selectpicker" id="sentimentalsource" ng-change="getCities();getRegions()"> -->
											<select data-ng-model="$parent.sentimentalSource" data-ng-options="source[1] as source[0] for source in sentimentalSources" class="selectpicker" id="sentimentalsource" ng-change="getCities();fn_overall_sentiment_onChange_dateSet()">
												<!-- <option style="display: none;" value="">Source</option> -->
											</select>
										</div>
										<div class="inner-filter-col">
											<div class="inner-filter-lbl">Select City</div>
											<select data-ng-model="$parent.sentimentalCity" data-ng-options="city as city for city in cities track by city" class="selectpicker place" ng-disabled="$parent.sentimentalSource != 4 && $parent.sentimentalSource !=2">
												<option style="display: none;" value="">City</option>
											</select>
										</div>
										<!-- <select data-ng-model="sentimentalStore" data-ng-options="city as city for city in cities track by city" ng-disabled="true" class="selectpicker">
											<option style="display: none;" value="">Store</option>
										</select> -->
										<div class="inner-filter-col">
											<div class="inner-filter-lbl">Select Context</div>
											<div class="validation context-filter pointer">
									          	<div ng-click="showSentimentalContexts=!showSentimentalContexts">Contexts <i class="fa fa-caret-down pull-right"></i></div>
									          	<div class="contexts-listing" ng-show="showSentimentalContexts">
										          	<div ng-repeat="topic in $parent.topics" class="item">
										      			<input type="checkbox" ng-model="topic[2]" ng-change="$parent.sync(topic[2], topic,'overall_sentiment')" ng-checked="topic[2]"> <span ng-bind="topic[0]"></span>
										      		</div>
									      		</div>
									      		<span class="validation-message" ng-show="!$parent.selectedSentimentalTopics[0]" style="display:block; position: absolute;">*Required</span>
									        </div>
								        </div>

								        <!-- Customer date picker  -->
								        <div class="dropdown custom-datepicker">
								        	<div class="inner-filter-lbl">Start Duration</div>
									        <div class="input-group">
										        <input date-range-picker id="sentimentalFrom" name="sentimentalFrom" class="form-control date-picker" type="text"
		                           ng-model="sentimentalFrom"  min="'20-02-2018'" max="'20-04-2018'" options="opts" required/>
		                           				<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
			                        		</div>
                           				</div>
								        <div class="dropdown custom-datepicker">
								        <div class="inner-filter-lbl">End Duration</div>
									        <div class="input-group">
										        <input date-range-picker id="sentimentalTo" name="sentimentalTo" class="form-control date-picker" type="text"
		                            ng-model="sentimentalTo"   min="'20-02-2018'" max="'20-04-2018'" options="opts" required/>
		                            			<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
			                        		</div>
                           				</div>
                           				<button class="btn primary-btn go-btn mt-4" aria-hidden="true" type="button" ng-click="fn_overall_sentiment_distribution_filter()">
											<span aria-hidden="true" class="fa fa-arrow-right"></span> Go
										</button>

							        </div>
							        <!-- <div class="col-md-12 inner-filter mt-2">
								        <div class="dropdown">
											<a class="dropdown-toggle" id="sentimentalFrom" role="button" data-toggle="dropdown" data-target="_self" href="">
												<div class="input-group">
													<input type="text" class="form-control" data-ng-model="sentimentalFrom" placeholder="From" readonly>
													<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
												</div>
											</a>
											<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
												<datetimepicker data-ng-model="$parent.sentimentalFrom" data-before-render="dateRangeValidation($view, $dates, $leftDate, $upDate, $rightDate)" data-datetimepicker-config="{ dropdownSelector: '#sentimentalFrom', modelType: 'DD-MM-YYYY'+' 00:01', minView: 'day'  }" />
											</ul>
										</div>
										<div class="dropdown">
											<a class="dropdown-toggle" id="sentimentalTo" role="button"data-toggle="dropdown" data-target="_self" href="">
												<div class="input-group">
													<input type="text" class="form-control" data-ng-model="sentimentalTo" placeholder="To" readonly>
													<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
												</div>
											</a>
											<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
												<datetimepicker data-ng-model="$parent.sentimentalTo" data-before-render="dateRangeValidation($view, $dates, $leftDate, $upDate, $rightDate)" data-datetimepicker-config="{ dropdownSelector: '#sentimentalTo', modelType: 'DD-MM-YYYY'+' 23:59', minView: 'day' }" />
											</ul>
										</div>
										<button class="btn primary-btn go-btn" aria-hidden="true" type="button" ng-click="fn_overall_sentiment_distribution_filter()">
											<span aria-hidden="true" class="fa fa-arrow-right"></span> Go

										</button>
							        </div> -->
								</div>
								
								
								
								<!-- START -->
						<!-- 		<div class="nbt-tab">
								Nav tabs
								<ul class="nav nav-tabs">
									<li role="presentation" ng-click="overallSentimentChartTab ='OverallSentimentBarChart'"
										ng-class="{'active': overallSentimentChartTab == 'OverallSentimentBarChart'}"><a
										aria-controls="LineChart"  role="tab"
										data-toggle="tab"><i  aria-hidden="true"></i>BarChart</a>
									</li>
									<li role="presentation" ng-click="overallSentimentChartTab='OverallSentimentLineChart'"
										ng-class="{'active': overallSentimentChartTab == 'OverallSentimentLineChart'}"><a
										aria-controls="BarChart" role="tab" 
										data-toggle="tab"><i aria-hidden="true" ></i> Line chart</a>
									</li>
							</ul>
							Tab panes
							<div class="tab-content nbt-content">
								
							<div role="tabpanel" class="tab-pane"
									ng-class="{'active': overallSentimentChartTab == 'OverallSentimentBarChart'}" id="OverallSentimentBarChart" >
									 <div id="sentimentalSourceChart"></div>
									<div class="graph-defn">Metric Definition : Positive,Negative and Neutral sentiment distribution for a duration.</div>
								</div>
								
							<div role="tabpanel" class="tab-pane"
									ng-class="{'active': overallSentimentChartTab == 'OverallSentimentLineChart'}" id="OverallSentimentLineChart">
									
									<div id="sentimentalSourceChartLine"></div>
									<div class="graph-defn">Metric Definition : Positive,Negative and Neutral sentiment distribution for a duration.</div>
								</div>
							</div>
						</div> -->
					<!-- END -->					
								
								
								
								<div id="sentimentalSourceChart"></div>
								<div class="graph-defn">Metric Definition : Positive,Negative and Neutral sentiment distribution for a duration.</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
		<div class="col-md-12"id="topAdvocatesDiv">
			<section id="senti-distrub" class="top-advocates">
				<div class="block-cover">
					<div class="block-head">
						<div class="row">
							<div class="col-md-12">
								<i class="fa fa-group" aria-hidden="true"></i>
								<h3 class="block-name">Top Advocates</h3>
							</div>
						</div>
					</div>
					<div class="block-body">
						<div class="row">
							<div class="col-md-12 mt-2 mb-3">
								<div class="row">
									<div class="col-md-12 inner-filter">
										<select data-ng-model="$parent.advocateSource" data-ng-options="source[1] as source[0] for source in advocateSources" class="selectpicker nbt-select-new pull-left" id="source">
											<option style="display: none;" value="">Source</option>
										</select>
										<div class="dropdown">
											<a class="dropdown-toggle" id="advocateFrom" role="button" data-toggle="dropdown" data-target="_self" href="">
												<div class="input-group">
													<input type="text" class="form-control" data-ng-model="$parent.advocateFrom" placeholder="From" readonly>
													<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
												</div>
											</a>
											<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
												<datetimepicker data-ng-model="$parent.advocateFrom" data-before-render="dateRangeValidation($view, $dates, $leftDate, $upDate, $rightDate)" data-datetimepicker-config="{ dropdownSelector: '#advocateFrom', modelType: 'DD-MM-YYYY'+' 00:01', minView: 'day' }" />
											</ul>
											<span class="validation-message" ng-show="advocateFrom=='' && advocateTo!=''">*Required</span>
										</div>
										<div class="dropdown">
											<a class="dropdown-toggle" id="advocateTo" role="button" data-toggle="dropdown" data-target="_self" href="">
												<div class="input-group">
													<input type="text" class="form-control" data-ng-model="$parent.advocateTo" placeholder="To" readonly>
													<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
												</div>
											</a>
											<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
												<datetimepicker data-ng-model="$parent.advocateTo" data-before-render="dateRangeValidation($view, $dates, $leftDate, $upDate, $rightDate)" data-datetimepicker-config="{ dropdownSelector: '#advocateTo', modelType: 'DD-MM-YYYY'+' 23:59', minView: 'day' }" />
											</ul>
											<span class="validation-message" ng-show="advocateFrom!='' && advocateTo==''">*Required</span> 
										</div>
										<button class="btn primary-btn go-btn" aria-hidden="true" type="button" ng-click="fn_filter_top_advocates()" ng-disabled="advocateFrom!=advocateTo && (advocateTo==''||advocateFrom=='')">
											<span aria-hidden="true" class="fa fa-arrow-right"></span> Go
										</button>
									</div>
								</div>
								<div id="topAdvocatesBarChart"></div>
								<!-- <div>
								 <a href="javaScript:void()" data-toggle="modal" data-target="#TopAdvocatesModal" ng-click="fn_showPostsForTopAdvocates(2)"></a>
			                    </div> -->
								<div class="graph-defn">Metric Definition : List of users who support or recommend of a particular conversation.</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
		<div class="col-md-12" id="postTweetWiseDistributionDiv">
			<div class="block-cover">
				<div class="block-head">
					<div class="row">
						<div class="col-md-6">
							<i class="fa fa-pie-chart" aria-hidden="true"></i>
							<h3 class="block-name">Post/Tweet wise Engagement Score/Sentiment Distribution</h3>
						</div>
						<div class="col-md-6">
							<div class="pull-right bar-filter">
								<label>Sort by:</label>
								<button class="btn black-btn go-btn pull-right ml-1" aria-hidden="true" class="" type="button" ng-click="fn_filter_PostTweetWiseEngagementScore()" >
									<span aria-hidden="true" class="fa fa-arrow-right"></span>&nbsp;Go
								</button>
								<select data-ng-model="$parent.selectedSourceTF" class="selectpicker ml-1" id="" >
									<option value="1">Facebook</option>
									<option value="2">Twitter</option>
								</select>
								<select data-ng-model="$parent.selectedDurationTF" class="selectpicker ml-1" id="selectedDurationTF">
									<option value="1" selected="selected">Last Day</option>
									<option value="3">Last 3 Days</option>
									<option value="7">Last 7 Days</option>
									<option value="15">Last 15 Days</option>
									<option value="30">Last 30 Days</option>
								</select>
								<select data-ng-model="$parent.selectedSortByTF" class="selectpicker ml-1" id="selectedSortByTF">
									<option value="1" selected="selected">Engagement Score</option>
									<option value="2">Date</option>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="block-body">
					<div class="auto_scrollable">
						<table class="table table-hover nbt-table mt-3 mb-3" ng-class="" id="tblsocList1TF">
						  <thead>
							<tr>
							  <th>Date-time</th>
							  <th>Text</th>
							  <th>Engagement Score</th>
							  <th>Sentiment Distribution</th>
							</tr>
						  </thead>
						  <tbody id="tblPostTweetEngagementSentiment" ng-cloak>
							<tr>
							  <td class="td-align-center no-data" ng-if="entitiesTF==''" colspan="6" ng-bind="'No Data Found'"></td>
							</tr>

							<tr dir-paginate="entityTF in entitiesTF|orderBy:sortType|itemsPerPage:contextEntriesTF" pagination-id="entity-paginateTF"
							current-page="socialPaginationTF.socialCurrentPageTF" total-items="totalSocialCountTF" ng-init="limit= 50" >
							  <td><span ng-bind="entityTF.post_datetime"></span></td>
							  <td>
								<span ng-bind="entityTF.post_text | limitTo:limit"></span>
								<a class="read-more"><span ng-click="limit=more_less=='more'?entityTF.post_text.length:50; more_less=more_less=='more'?'less':'more'" ng-bind="entityTF.post_text.length>50?'read '+more_less:''"></span></a>
								<!-- <a href="{{ entityTF.post_text }}" target="_blank" class="read-more"><i class="fa fa-external-link"></i></a> -->
							   </td>
							  <td><span ng-bind="entityTF.engagement_score"></span></td>
							  <td class="padding-rt-15"><div class="sen-box"> <span class="btn nbt-green"  data-toggle="tooltip" title="{{ entityTF.posetive }}" ng-style="{width: '{{ entityTF.posetive }}%'}"></span> <span class="btn nbt-red"  data-toggle="tooltip" title="{{ entityTF.negative }}" ng-style="{width: '{{ entityTF.negative }}%'}"></span> <span class="btn nbt-yellow"  data-toggle="tooltip" title="{{ entityTF.neutral }}" ng-style="{width: '{{ entityTF.neutral }}%'}"></span> </div></td>
							</tr>
						  </tbody>
						</table>
					</div>
					<dir-pagination-controls pagination-id="entity-paginateTF" class="pull-right"
					   max-size="5"
					   direction-links="true"
					   boundary-links="true" on-page-change="postTweetWiseEngagementScorePageChange(newPageNumber)"> </dir-pagination-controls>
				</div>
			</div>
		</div>
		<div class="col-md-6" id="navDiv1">
			<div class="block-cover">
				<div class="block-head">
					<div class="row">
						<div class="col-md-6">
							<i class="fa fa-area-chart" aria-hidden="true"></i>
							<h3 class="block-name">SENTIMENT INDEX LIVE</h3>
						</div>
						<div class="col-md-6">
							<div class="pull-right bar-filter">
								<select class="selectpicker" id="setRange" ng-change="intervalChange()" ng-model="$parent.intervalValue">
									<!-- <option style="display: none;" value="">Interval</option> -->
									<option value="0">Hourly Interval</option>
									<option value="1">Daily Interval</option>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="block-body">
					<div class="row">
						<div class="col-md-12 mt-2 mb-3">
							<div id="chart1"></div>
							<div class="graph-defn">Metric Definition : Score that express the difference between positive sentiment and negative sentiment.</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6" id="navDiv2">
			<div class="block-cover">
				<div class="block-head">
					<div class="row">
						<div class="col-md-7">
							<i class="fa fa-line-chart" aria-hidden="true"></i>
							<h3 class="block-name">COMPETITIVE SENTIMENT INDEX</h3>
						</div>
						<div class="col-md-5">
							<div class="pull-right bar-filter">
								<select data-ng-options="topic[1] as topic[0] for topic in topics" data-ng-model="$parent.selectedContextMonthly" ng-change="fn_brand_comparission_monthly()" class="selectpicker" id="topics_Monthly_Competitive_HI">
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="block-body">
					<div class="row">
						<div class="col-md-12 mt-2 mb-3">
							<div id="chart2"></div>
							<div class="graph-defn">Metric Definition : Score that express the difference between positive sentiment and negative sentiment based on context selection.</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-12">
		<!--Contextwise Sentiment Distribution-->
			<div class="block-cover" id="sentimentDistributionMonthlyGraphDiv">
				<div class="block-head">
					<div class="row">
						<div class="col-md-6">
							<i class="fa fa-bar-chart" aria-hidden="true"></i>
							<h3 class="block-name">Monthly contextwise sentiment</h3>
						</div>
						<div class="col-md-6">
							<div class="pull-right bar-filter">
								<div class="pull-left">
									<label>Select Sentiment: </label>
									<select class="selectpicker" id="selectSentiment" ng-change="fn_contextwise_sentiment_distribution(); fn_contextwise_sentiment_distribution_percentile()" ng-model="sentiment">
										<option value='Positive'>Positive</option>
										<option value='Negative'>Negative</option>
										<option value='Neutral'>Neutral</option>
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="block-body" id="contextWiseSentimentDistributionChart">
					<div class="row">
						<div class="col-md-6 mt-2 mb-3">
						 <h3 class="graph-title">Sentiment distribution by count</h3>
						 <div id="contextWiseChart"></div>
						 <div class="graph-defn">Sentiment distribution by count.</div>
						</div>
						<div class="col-md-6 mt-2 mb-3">
						 <h3 class="graph-title">Sentiment distribution by %</h3>
						 <div id="contextWisePercentChart"></div>
						 <div class="graph-defn">Sentiment distribution by percentage.</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="block-cover">
				<div class="block-body">
					<div class="inner-filter mt-3 mb-5 context-filter-wapper">
						<i class="fa fa-filter pull-left" aria-hidden="true"></i>
						<select data-ng-model="$parent.selectedSource" data-ng-options="source[1] as source[0] for source in contextDetailsSources" class="selectpicker pull-left" id="source">
							<option style="display: none;" value="">All Sources</option>
						</select> 
						<div class="validation context-filter pull-left">
							<div ng-click="showContexts=!showContexts">Contexts <i class="fa fa-caret-down pull-right"></i></div>
							<div class="contexts-listing" ng-show="showContexts">
								<div ng-repeat="topic in $parent.topics" class="item">
									<input type="checkbox" ng-model="topic[2]" ng-change="sync(topic[2], topic,'explore')" ng-checked="topic[2]"> <span ng-bind="topic[0]"></span>
								</div>
							</div>
							<span class="validation-message" ng-show="!$parent.selectedTopics[0]" style="display:block; position: absolute;">*Required</span>
					   </div>
						<select class="selectpicker pull-left" id="sentiment" ng-model="$parent.selectedSentiment">
							<option style="display: none;" value="">Sentiment</option>
							<option value="All">All Sentiment</option>
							<option value="1">Positive</option>
							<option value="-1">Negative</option>
							<option value="0">Neutral</option>
						</select>
						<div class="dropdown pull-left">
							<a class="dropdown-toggle" id="analyticsFrom" role="button"
								data-toggle="dropdown" data-target="_self" href="">
								<div class="input-group">
									<input type="text" class="form-control" data-ng-model="$parent.analyticsFrom" placeholder="From" readonly> 
									<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
								</div>
							</a>
							<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
								<datetimepicker data-ng-model="$parent.analyticsFrom" data-before-render="dateRangeValidation($view, $dates, $leftDate, $upDate, $rightDate)" data-datetimepicker-config="{ dropdownSelector: '#analyticsFrom', modelType: 'DD-MM-YYYY'+' 00:01', minView: 'day'  }" />
							</ul>
						</div>
					  	<div class="dropdown pull-left"> <a class="dropdown-toggle" id="analyticsTo" role="button" data-toggle="dropdown" data-target="_self" href="">
							<div class="input-group">
							  <input type="text" class="form-control" data-ng-model="$parent.analyticsTo" placeholder="To" readonly>
							  <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span> </div>
							</a>
							<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
							  <datetimepicker data-ng-model="$parent.analyticsTo" data-before-render="dateRangeValidation($view, $dates, $leftDate, $upDate, $rightDate)" data-datetimepicker-config="{ dropdownSelector: '#analyticsTo', modelType: 'DD-MM-YYYY'+' 23:59', minView: 'day' }"/>
							</ul>
							</div>
						<button class="btn black-btn go-btn pull-left" aria-hidden="true" type="button" ng-click="fn_filter()">
							<span aria-hidden="true" class="fa fa-arrow-right"></span>&nbsp;Go
						</button>
						<div class="clearfix"></div>
					</div>
					<div id="contextDetailsDiv">
						<div class="block-head">
							<div class="row">
								<div class="col-md-12">
									<h3 class="block-name">Context Details</h3>
								</div>
							</div>
						</div>
						<div class="block-body">

						<div class="auto_scrollable">
							<table class="table table-hover nbt-table mt-3" id="tblsocList1">
								<thead>
									<tr>
										<th>Context</th>
										<th>Source</th>
										<th class="pointer"
											ng-click="sortType='-6'; sortReverse = !sortReverse">Feedback
											<span ng-show="sortType == '-6' && !sortReverse"
											class="fa fa-caret-up"></span> <span
											ng-show="sortType == '-6' && sortReverse"
											class="fa fa-caret-down"></span>
										</th>
										<th width="10%" class="text-right pointer"
											ng-click="sortType='-9'; sortReverse = !sortReverse">Complaint
											<span ng-show="sortType == '-9' && !sortReverse"
											class="fa fa-caret-up"></span> <span
											ng-show="sortType == '-9' && sortReverse"
											class="fa fa-caret-down"></span>
										</th>
										<th>Sentiment</th>
										<th class="td-align-center">View</th>
										<!-- <th class="text-center">Start Time</th>
					  <th class="text-center">End Time</th> -->
									</tr>
								</thead>
								<tbody id="tblSourceDateSentimentAnalysis" ng-cloak>
									<tr>
										<td class="td-align-center no-data" ng-if="entities==''"
											colspan="7" ng-bind="'No Data Found'"></td>
									</tr>
									<tr
										dir-paginate="entity in entities|orderBy:sortType:sortReverse|itemsPerPage:10" current-page="$parent.contextDetailsCurPage"
										pagination-id="entity-paginate">
										<td
											ng-class="entity[3]>entity[4] && entity[3]>entity[5]?'text-green': entity[4]>=entity[3] && entity[4]>=entity[5] ? 'text-red':'text-yellow'"><strong><span
												ng-bind="entity[2]"></span></strong></td>
										<td><i
											ng-class="entity[0]==1?'fa fa-facebook-official': entity[0]==2?'fa fa-twitter':'fa fa-rss'"
											aria-hidden="true"></i><span ng-bind="entity[1]"></span></td>
										<td><span ng-bind="entity[6]"></span></td>
										<!-- <td><span ng-bind="entity[9]"></span></td> -->
										<td><span ng-bind="entity[9]"></span></td>
										<td><div class="sen-box">
												<span class="btn bgcolor-positive" ng-if="entity[3] != 0"
													data-toggle="tooltip" title="{{ entity[3] }}"
													ng-style="{width: '{{ entity[3] }}%'}"></span><span
													class="btn bgcolor-neutral" ng-if="entity[5] != 0"
													data-toggle="tooltip" title="{{ entity[5] }}"
													ng-style="{width: '{{ 100 - (entity[3] + entity[4]) }}%'}"></span><span
													class="btn bgcolor-negative" ng-if="entity[4] != 0"
													data-toggle="tooltip" title="{{ entity[4] }}"
													ng-style="{width: '{{ entity[4] }}%'}"></span> 
											</div></td>
										<!-- <td class="text-center"><span ng-bind="entity[7] | date : 'dd-MM-yyyy HH:mm'"></span></td>
					  <td class="text-center"><span ng-bind="entity[8] | date : 'dd-MM-yyyy HH:mm'"></span></td> -->
										<!-- <td class="td-align-center"><a
											ng-click="fn_view_filter(entity[0], entity[2])"><i
												class="fa fa-eye fa-lg pointer" aria-hidden="true"></i></a></td> -->
										<td class="td-align-center"><a
											ng-click="fn_view_filter(entity[0], entity[8])"><i
												class="fa fa-eye fa-lg pointer" aria-hidden="true"></i></a></td>
									</tr>
								</tbody>
							</table>
							<dir-pagination-controls pagination-id="entity-paginate"
								class="pull-right" max-size="5" direction-links="true"
								boundary-links="true" template-url="dirPagination.tpl.html">
							</dir-pagination-controls>
							<div class="clearfix"></div>
						</div>
						<div class="clearfix"></div>
					</div>
					</div>
					
					<div class="block-body deep-list-wapper">
				<div class="nbt-tab">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs nbt-tab-ul" role="tablist">
						<li role="presentation" ng-click="activeTab='social'"
							ng-class="{'active': activeTab == 'social'}"><a
							aria-controls="Social" role="tab" data-toggle="tab"><i
								class="fa fa-table" aria-hidden="true"></i> Deep Listening</a></li>
						<li role="presentation" ng-click="activeTab='sentiment'"
							ng-class="{'active': activeTab == 'sentiment'}"><a
							aria-controls="Sentiment" id="sentimentTabHeader" role="tab"
							data-toggle="tab"><i class="fa fa-bolt" aria-hidden="true"></i>Trending
								topics</a></li>
						<!-- <li role="presentation" ng-click="activeTab='aspect';aspectDistributionPieChart()"
							ng-class="{'active': activeTab == 'aspect'}"><a
							aria-controls="Aspect" role="tab" data-toggle="tab"><i
								 aria-hidden="true" ></i> Aspect Distribution</a></li>
							<li>
							<button class="btn primary-btn" type="button" ng-click="saveAsExcel()">
							DownLoad 
							</button>	
							</li> -->
					</ul>
					<!-- Tab panes -->
					<div class="tab-content nbt-content">
						<div role="tabpanel" class="tab-pane"
							ng-class="{'active': activeTab == 'social'}" id="Social">

			<div ng-show="filteredCategory!='Enterprise'" role="tabpanel" class="tab-pane" ng-class="{'active': activeTab == 'social'}" id="Social">
              <table class="table table-hover nbt-table" ng-class="{'region-col': filteredSource==4 || filteredSource==2}" id="tblsocList">
                <thead>
                  <tr> 
                   <!--  <th>Source</th> -->
                   <th></th>
                    <th>Post Type</th>
                    <th ng-if="filteredSource==4 || filteredSource==2">Region/City</th>
                    <!-- <th>Date-time</th> -->
                    <th>Text</th>
                    <th>Overall Sentiment</th>
                    <th>Categorized Sentiment</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody id="tableSocialListening-social">
                  <tr>
                    <td class="td-align-center no-data" ng-if="deepListSocLists==''" colspan="8" ng-bind="'No Data Found'"></td>
                  </tr>
                  <tr dir-paginate="deepListSocList in deepListSocLists|itemsPerPage:socialEntries" 
                  pagination-id="tblsocList-paginate" current-page="socialPagination.socialCurrentPage" total-items="totalSocialCount"  ng-init="limit= 100">
                    <!-- <td><i class="source-icon" ng-class="deepListSocList.sourceTypeId==1?'fa fa-facebook-official': deepListSocList.sourceTypeId==2?'fa fa-twitter': deepListSocList.sourceTypeId==4?'zomato-icon' : ''" aria-hidden="true"></i><span class="source-name" ng-bind="deepListSocList.sourceTypeName"></span></td> -->
                    <!-- <td><i class="source-icon fa fa-{{deepListSocList.sourceTypeCode}}"  aria-hidden="true"></i><span class="source-name" ng-bind="deepListSocList.sourceTypeName"></span></td> -->
                    <td><i class="source-icon fa fa-{{deepListSocList.sourceTypeCode}}"  aria-hidden="true"></i></span></td>
                    <td><span ng-bind="deepListSocList.postType"></span></td>
                    <td ng-if="filteredSource==4 || filteredSource==2"><span ng-bind="deepListSocList.city"></span></td>
                    <td><span ng-bind="deepListSocList.postDateTime" class="deep-list-date pull-left"></span>
                    <a href="#ticketmanagement/{{deepListSocList.postId}}" title="Click for ticket detail">
              			<span class="label label-danger pull-right" ng-if="deepListSocList.hasTicket == '1'">Ticket Generated</span>    
                    </a>
                    <div class="clearfix"></div>
                    <!-- <td><span ng-bind="soclist[5] | limitTo:100"></span><a href="{{ soclist[6] }}" target="blank" class="read-more"> read more...</a></td> -->
                    	<span ng-bind="deepListSocList.postText | limitTo:limit"></span>
                    	<a class="read-more"><span ng-click="limit=more_less=='more'?deepListSocList.postText.length:100; more_less=more_less=='more'?'less':'more'" ng-bind="deepListSocList.postText.length>100?'read '+more_less:''"></span></a>
                    	<a ng-if="deepListSocList.postType != 'Reply' && deepListSocList.postType != 'CCIR' && deepListSocList.postType != 'GES' " href="{{ deepListSocList.postUrl }}" target="_blank" class="read-more"><i class="fa fa-external-link"></i></a>
                        <a href="javaScript:void()" data-toggle="modal" data-target="#snippetModalForAnalytics" ng-click="fn_showPostDetails(deepListSocList.postId)" class="modal-link-btn">Click to view snippet level distribution</a>
                    </td>
                    <td id="td_{{deepListSocList.postId}}" 
                    	ng-class="deepListSocList.overallSentiment=='Positive'? 'text-green':
                    	 deepListSocList.overallSentiment=='Negative' ? 'text-red':'text-yellow'">
                    	 <strong class="pull-left senti-txt"><span ng-bind="deepListSocList.overallSentiment" id="sentiment_{{deepListSocList.postId}}">
                    	 </span></strong>
                    	<div ng-if="authRole==1" class="dropdown senti-dropdown">
						    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
						    <span class="caret"></span></button>
						    <ul class="dropdown-menu">
						      <li><a ng-click="updateOverallSentiment(1,deepListSocList.postId,deepListSocList.overallSentiment,deepListSocList.postText,deepListSocList.postDateTime)">Positive</a></li>
						      <li><a ng-click="updateOverallSentiment(-1,deepListSocList.postId,deepListSocList.overallSentiment,deepListSocList.postText,deepListSocList.postDateTime)">Negative</a></li>
						      <li><a ng-click="updateOverallSentiment(0,deepListSocList.postId,deepListSocList.overallSentiment,deepListSocList.postText,deepListSocList.postDateTime)">Neutral</a></li>
						    </ul>
						</div>
						<div ng-if="authRole==1" class="tooltipforupdatedSentiment">
							<i class="fa fa-question-circle" id="td_{{deepListSocList.postId}}"></i>
							<span class="tooltiptextforupdatedSentiment">To change the displayed sentiment permanently click on the dropdown arrow</span>
						</div>
                    </td>
                    <td>
                    	<span title="Cleanliness" class="badge background-{{deepListSocList.categorizedSentiment.C.toLowerCase()}}">C</span>
                    	<span title="Hospitality" class="badge background-{{deepListSocList.categorizedSentiment.H.toLowerCase()}}">H</span>
                    	<span title="Accuracy" class="badge background-{{deepListSocList.categorizedSentiment.A.toLowerCase()}}">A</span>
                    	<span title="Maintenance" class="badge background-{{deepListSocList.categorizedSentiment.M.toLowerCase()}}">M</span>
                    	<span title="Product Quality" class="badge background-{{deepListSocList.categorizedSentiment.P.toLowerCase()}}">P</span>
                    	<span title="Speed & Delivery" class="badge background-{{deepListSocList.categorizedSentiment.S.toLowerCase()}}">S</span>
                    	<span title="Prices and Offers" class="badge background-{{deepListSocList.categorizedSentiment.PR.toLowerCase()}}">PR</span>
                    	<span title="Others" class="badge background-{{deepListSocList.categorizedSentiment.O.toLowerCase()}}">O</span>
                    	<span title="Non-Relevant" class="badge background-{{deepListSocList.categorizedSentiment.N.toLowerCase()}}">N</span>
                    </td>
                    <td ng-if="authRole==1">
                    	<a ng-if="(deepListSocList.sourceTypeCode == 'fb' || deepListSocList.sourceTypeCode == 'tw' )" class="open-tkt-create-modal pointer" data-toggle="modal" data-id="{{ deepListSocList.postId +'#@#'+ deepListSocList.sourceTypeCode  }}" data-target="#tkt-create-modal" ng-click="setFeedbackURL(deepListSocList.postId,deepListSocList.sourceTypeId)"><i class="fa fa-ticket"></i></a>
                    	<a ng-if="(deepListSocList.sourceTypeCode != 'fb' && deepListSocList.sourceTypeCode != 'tw' )" class="open-tkt-create-modal disabled" ><i class="fa fa-ticket" style="color: #ccc;"></i></a>
                    </td>
                    <!-- <td ng-if="authRole==1 &&(deepListSocList.sourceTypeCode != 'fb' || deepListSocList.sourceTypeCode != 'tw' )"><a class="open-tkt-create-modal disabled" ><i class="fa fa-ticket" style="color: #ccc;"></i></a></td> -->
                    <td ng-if="authRole==2"><a class="open-tkt-create-modal disabled" ><i class="fa fa-ticket" style="color: #ccc;"></i></a></td>
                  </tr>
                </tbody>
              </table>
              <dir-pagination-controls ng-if="filteredCategory!='Enterprise'"  pagination-id="tblsocList-paginate" class="pull-right" max-size="5" direction-links="true" 
              boundary-links="true"  on-page-change="socialPageChanged(newPageNumber)"></dir-pagination-controls>
              <div class="clearfix"></div>
            </div>
            <div ng-show="filteredCategory=='Enterprise'" role="tabpanel" class="tab-pane" ng-class="{'active': activeTab == 'social'}" id="Social">
            <div class="auto_scrollable">
              <table class="table table-hover nbt-table" id="tblsocList">
                <thead>
                  <tr> 
                    <!-- <th><label class="nbt-checkbox"><input type="checkbox"><span></span></label></th> -->
                    <th>Source</th>
                    <th>Date-Time</th>
                    <th>Text</th>
                    <th>Overall Sentiment</th>
                    <th>Categorized Sentiment</th>
                    <th>Name</th>
                    <th>Email ID</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody id="tableSocialListening-non-social">
                  <tr>
                    <td class="td-align-center no-data" ng-if="deepListNonSocLists==''" colspan="8" ng-bind="'No Data Found'"></td>
                  </tr>
                  <tr dir-paginate="deepListNonSocList in deepListNonSocLists|itemsPerPage:socialEntries" pagination-id="tblsocListFiltered-paginate" 
                   current-page="socialPagination.socialCurrentPage" total-items="totalSocialCount" ng-init="limit= 100">
                    <td><i class="source-icon fa fa-{{deepListNonSocList.sourceTypeCode}}"  aria-hidden="true"></i><span ng-bind="deepListNonSocList.sourceTypeName"></span></td>
                    <td><span ng-bind="deepListNonSocList.postDateTime"></span></td>
                    <td><span ng-bind="deepListNonSocList.postText | limitTo:limit"></span> <a class="read-more"><span ng-click="limit=more_less=='more'?deepListNonSocList.postText.length:100; more_less=more_less=='more'?'less':'more'" ng-bind="deepListNonSocList.postText.length>100?'read '+more_less:''"></span></a></td>
                    <td ng-class="deepListNonSocList.overallSentiment=='Positive'? 'text-green': deepListNonSocList.overallSentiment=='Negative' ? 'text-red':'text-yellow'"><strong><span ng-bind="deepListNonSocList.overallSentiment"></span></strong></td>
                    <td>
                    	<span title="Cleanliness" class="badge background-{{deepListNonSocList.categorizedSentiment.C.toLowerCase()}}">C</span>
                    	<span title="Hospitality" class="badge background-{{deepListNonSocList.categorizedSentiment.H.toLowerCase()}}">H</span>
                    	<span title="Accuracy" class="badge background-{{deepListNonSocList.categorizedSentiment.A.toLowerCase()}}">A</span>
                    	<span title="Maintenance" class="badge background-{{deepListNonSocList.categorizedSentiment.M.toLowerCase()}}">M</span>
                    	<span title="Product Quality" class="badge background-{{deepListNonSocList.categorizedSentiment.P.toLowerCase()}}">P</span>
                    	<span title="Speed & Delivery" class="badge background-{{deepListNonSocList.categorizedSentiment.S.toLowerCase()}}">S</span>
                    	<span title="Prices and Offers" class="badge background-{{deepListNonSocList.categorizedSentiment.PR.toLowerCase()}}">PR</span>
                    	<span title="Others" class="badge background-{{deepListNonSocList.categorizedSentiment.O.toLowerCase()}}">O</span>
                    	<span title="Non-Relevant" class="badge background-{{deepListNonSocList.categorizedSentiment.N.toLowerCase()}}">N</span>
                    </td>
                    <td><span ng-bind="deepListNonSocList.authorName"></span></td>
                    <td><span ng-bind="deepListNonSocList.emailId"></span></td>
                    <td><a class="open-email-modal" data-toggle="modal" data-id="{{deepListNonSocList.emailId}}" data-target="#emailEditor"><i class="fa fa-envelope"></i></a>
                      <div id="emailEditor" class="modal fade em-details"
												role="dialog">
                        <div class="modal-dialog modal-lg">
                          <form class="form-wapper">
                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">


                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h3 class="modal-title">Send Mail</h3>
                              </div>
                              <div class="modal-body">
                                <div class="row">
                                  <div class="col-md-1">
                                    <label>To</label>
                                  </div>
                                  <div class="col-md-11" id="receiverEmail"></div>
                                </div>
                                <div class="row">
                                  <div class="col-md-12">
                                    <label>Mail Content</label>
                                  </div>
                                  <div class="col-md-12">
                                  	<textarea></textarea>
                                  </div>
                                </div>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal" ng-click="$parent.callAlert('warning','Sending email is currently non-functionable. It is only for demo-purpose')">Send Mail</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div></td>
                  </tr>
                </tbody>
              </table>
              </div>
              <dir-pagination-controls ng-if="filteredCategory=='Enterprise'" pagination-id="tblsocListFiltered-paginate" class="pull-right" max-size="5" direction-links="true" 
              boundary-links="true" on-page-change="socialPageChanged(newPageNumber,socialPagination.socialCurrentPage)"></dir-pagination-controls>
            </div>
							<!-- <div class="auto_scrollable">
								<table class="table table-hover nbt-table" id="tblsocList">
									<thead>
										<tr>
											<th>Source</th>
											<th>Post Type</th>
											<th>Date-time</th>
											<th>Text</th>
											<th>Sentiment</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody id="tableSocialListening">
										<tr>
											<td class="td-align-center no-data" ng-if="soclists==''"
												colspan="6" ng-bind="'No Data Found'"></td>
										</tr>
										<tr dir-paginate="soclist in soclists|itemsPerPage:10"
											pagination-id="tblsocList-paginate">
											<td><i
												ng-class="soclist[2]==1?'fa fa-facebook-official': soclist[2]==2?'fa fa-twitter':'fa fa-rss'"
												aria-hidden="true"></i><span ng-bind="soclist[3]"></span></td>
											<td><span ng-bind="soclist[1]"></span></td>
											<td><span ng-bind="soclist[4]"></span></td>
											<td><span ng-bind="soclist[5] | limitTo:100"></span><a
												href="{{ soclist[6] }}" target="_blank" class="read-more">
													read more...</a></td>
											<td
												ng-class="soclist[8]=='Positive'? 'text-positive': soclist[8]=='Negative' ? 'text-negative':'text-neutral'"><strong><span
													ng-bind="soclist[8]"></span></strong></td>
											<td><a class="open-tkt-create-modal" data-toggle="modal"
												data-id="{{ soclist[7] }}" data-target="#tkt-create-modal"><i
													class="fa fa-ticket"></i></a></td>
										</tr>
									</tbody>
								</table>
							</div>
							<dir-pagination-controls pagination-id="tblsocList-paginate"
								class="pull-right" max-size="5" direction-links="true"
								boundary-links="true" template-url="dirPagination.tpl.html"></dir-pagination-controls> -->
						</div>
						<div role="tabpanel" class="tab-pane text-center"
							ng-class="{'active': activeTab == 'sentiment'}" id="Sentiment">
							<div class="senti-box">
								<div class="row">
									<div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 no-padding">
										<div class="word-cloud-title">
											<i class="fa fa-arrow-left pointer" aria-hidden="true"
												ng-hide="wordcloudContext=='All Contexts' || (selectedTopic!='' && selectedTopic!='All Contexts')"
												ng-click="filterContext=''; filterEntity=''; fn_word_cloud(null)"></i>
											<span ng-bind="wordcloudContext"></span>
										</div>
										<div id="wordcloud" class="text-center margin-left-15 border"></div>
									</div>
									<div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
										<!-- <div class="block-head-competitive-index">
											<div class="pull-right select-interval">
												<div class="pull-left">
													<select
														style="border: 0px solid #ccc !important;"
														data-ng-options="entity[0] as entity[0] for entity in wordcloudentity track by entity[0]"
														ng-model=selectedEntity
														ng-change=""
														class="selectpicker nbt-select">
													</select>
												</div>
											</div>
										</div>
										<div>-->
										<div
											class="auto_scrollable border margin-bottom-15 border-top-0"
											ng-style="(contextList || entityList) && {'min-height': '340px'} || {'min-height': 'auto'}"
											style="position: relative; top: -1px;">
											<table class="table table-hover nbt-table margin-0"
												id="tblsocList2">
												<thead>
													<tr>
														<th>Source</th>
														<th><span>Entity</span> <i
															class="fa fa-filter pointer selectEntity"
															aria-hidden="true"
															ng-click="contextList = false;entityList = !entityList"></i>
															<div class="dropdownList" ng-show="entityList">
																<ul>
																	<li ng-repeat="entity in entitiesForFilter track by $index"
																		class="pointer" ng-click="fn_filterByEntity(entity)"><span
																		ng-bind="entity"></span></li>
																</ul>
															</div></th>
														<th><span>Context</span> <i
															class="fa fa-filter pointer selectEntity"
															aria-hidden="true"
															ng-hide="selectedTopic!='' && selectedTopic!='All Contexts'"
															ng-click="contextList = !contextList; entityList = false"></i>
															<div class="dropdownList" ng-show="contextList">
																<ul>
																	<li ng-repeat="topic in topics" class="pointer"
																		ng-click="fn_word_cloud(topic[0])"><span
																		ng-bind="topic[0]"></span></li>
																</ul>
															</div></th>
														<!-- <th>Date-time</th> -->
														<th>Text</th>
														<th>Sentiment</th>
													</tr>
												</thead>
												<tbody id="tableWordCloud">
													<tr>
														<td class="text-center td-align-center no-data" ng-if="soclists==''"
															colspan="6" ng-bind="'No Data Found'"></td>
													</tr>
													<!-- <tr
														dir-paginate="data in wordCloudTableData|filter:{aspect:filterContext, entity:filterEntity}:exceptEmptyComparator|itemsPerPage:6"
														pagination-id="tblword-paginate" ng-init="limit= 30"> -->
													<tr
														dir-paginate="data in wordCloudTableData|itemsPerPage:6"
														pagination-id="tblword-paginate" current-page="$parent.wordCloudAspectListCurPage"  ng-init="limit= 30">
														<td><i
															ng-class="data.source_type_id==1?'fa fa-facebook-official': data.source_type_id==2?'fa fa-twitter':'fa fa-rss'"
															aria-hidden="true"></i>
															<!-- <span ng-bind="data.source_type_name"></span> --></td>
														<td><span ng-bind="data.entity | limitTo:limit"></span>
															<a class="read-more"> <span
																ng-click="limit=more_less=='more'?data.entity.length:30; more_less=more_less=='more'?'less':'more'"
																ng-bind="data.entity.length>30?more_less:''"></span>
														</a></td>
														<td><span ng-bind="data.aspect"></span></td>
														<td><span ng-bind="data.post_datetime" class="deep-list-date"></span>
														<span ng-bind="data.post_text | limitTo:40"></span><a
															href="{{ data.post_url }}" target="_blank"
															class="read-more"> read more...</a></td>
														<td
															ng-class="data.overall_sentiment=='Positive'? 'text-green': data.overall_sentiment=='Negative' ? 'text-red':'text-yellow'"><strong><span
																ng-bind="data.overall_sentiment"></span></strong></td>
													</tr>
												</tbody>
											</table>
										</div>
										<dir-pagination-controls pagination-id="tblword-paginate"
											class="pull-right" max-size="5" direction-links="true"
											boundary-links="true" template-url="dirPagination.tpl.html">
										</dir-pagination-controls>
										<!-- </div> -->
									</div>
								</div>
							</div>
						</div>
						
				<div role="tabpanel" class="tab-pane text-center"
							ng-class="{'active': activeTab == 'aspect'}" id="aspect"
							style="width: 100%; box-sizing: border-box;" >
							
							<div id="aspectDistributionPieChart"></div>
							<div class="graph-defn">Metric Definition : Positive,Negative and Neutral sentiment distribution for a duration.</div>
							<!-- <div class="senti-box">
								<div class="row">
									<div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 no-padding">
										<div class="word-cloud-title">
											<i class="fa fa-arrow-left pointer" aria-hidden="true"
												ng-hide="wordcloudContext=='Aspect Distribution' || (selectedTopic!='' && selectedTopic!='All Contexts')"
												ng-click="filterContext=''; filterEntity=''; fn_word_cloud(null)"></i>
											<span ng-bind="wordcloudContext"></span>
										</div>
										<div id="wordcloud" class="text-center margin-left-15 border"></div>
									</div>
									
								</div>
							</div> -->
						</div>
					</div>
				</div>
			</div>
				</div>
			</div>				
			
		</div>

	</div>											
</div>


<!--snapshot modal  -->
    
  <div  class="modal fade" id="snapshotModal" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
      <div ng-if="postSnippets.length==0 && noSnippets == ''" class="loader"></div>
       <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
	      <h4 class="modal-title" style ="text-align:left;"><span ng-if="feedbackOrComplaint=='Complaint'">
          	List of Complaints
          </span>
          <span ng-if="feedbackOrComplaint=='Feedback'">
          List of Feedbacks
          </span></h4>
        </div>     
	    <div class="modal-body text-center ">
          	<div class="review-tooltip-tbl" style="overflow:scroll; height:300px">
          		<table class="table snippet-table">
          			<tr>
          				<th style ="text-align:left;"><span ng-if="feedbackOrComplaint=='Complaint'">
          				Complaints
          				</span>
          				<span ng-if="feedbackOrComplaint=='Feedback'">
          				Feedbacks
          				</span>
          				</th>
          			</tr>
          			<tr ng-if = "noSnippets != '' ">
          				<td style ="text-align:left">
          					{{noSnippets}}
          				</td>
          			</tr>
          			<tr ng-if = "noSnippets == '' " ng-repeat="snippet in postSnippets">
          				<td style ="text-align:left">
          					<span ng-bind="snippet[1]" class="deep-list-date"></span>
          					<span ng-bind="snippet[0]"></span>
          				</td>
          			</tr>
          		</table>
          	</div>
        </div>
      </div>
    </div>
  </div>


