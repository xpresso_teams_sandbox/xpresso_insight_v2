<!-- add-info page -->

<div class="row" ng-hide="addinfosuccess">
  <div class="col-md-10 col-md-offset-1 addinfo-wrapper">
    <div class="block-cover">
      <div class="block-head">
        <h3>XpressoInsights Configuration</h3>
      </div>
      <form name="addUserInfo">
        <!--<pre style="position: fixed; top: 10px; left: 10px; height:500px; overflow-y:scroll">{{userInfo | json}}</pre>-->
        <div class="block-body">
          <div class="panel-group" id="addInfo">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#user" target="_self"><span class="users-ico"><i aria-hidden="true" class="fa fa-users"></i></span>Users</a> </h4>
              </div>
              <div id="user" class="panel-collapse collapse in">
                <div class="panel-body">
                  <div class="col-md-10">
                    <h3>Add Other Users</h3>
                    <div id="addUser">
                      <div class="form-group row">
                        <div class="col-sm-4">
                          <label class="form-control-label">Name</label>
                        </div>
                        <div class="col-sm-8">
                          <input type="text" class="form-control" name="userName" ng-model="userName">
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="col-sm-4">
                          <label class="form-control-label">Email</label>
                        </div>
                        <div class="col-sm-8">
                          <input type="email" class="form-control" name="userEmail" ng-model="userEmail">
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="col-sm-offset-4 col-sm-8">
                          <button id="addUserbtn" type="submit" class="btn btn-default add-com-btn" ng-click="addUser()">Add User</button>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-offset-4 col-sm-8" ng-show="userListing">
                          <div class="user-list">
                            <div class="user-btn" ng-repeat="ud in userInfo.usersList">
                              <div ng-bind="ud.userName"></div>
                              <span ng-click="deleteUser($index)"><i aria-hidden="true" class="fa fa-close"></i></span></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#keywords" target="_self"><span class="keywords-ico"><i class="fa fa-key" aria-hidden="true"></i></span>Keywords</a> </h4>
              </div>
              <div id="keywords" class="panel-collapse collapse">
                <div class="panel-body">
                  <div class="col-md-10">
                    <h3>Add Keywords</h3>
                    <div id="addKeywords">
                      <div class="form-group row">
                        <div class="col-sm-4">
                          <label class="form-control-label">Keywords</label>
                        </div>
                        <div class="col-sm-8">
                          <input type="text" class="form-control" name="keyword" ng-model="keyword">
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="col-sm-offset-4 col-sm-8">
                          <button id="addKeywordbtn" type="submit" class="btn btn-default add-com-btn" ng-click="addKeywords()">Add Keyword</button>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-offset-4 col-sm-8">
                        <div class="keyword-list">
                          <div class="keyword-btn" ng-repeat="keyword in userInfo.keywords">
                            <div>{{keyword.keyword}}</div>
                            <span ng-click="deleteKeyword($index)"><i aria-hidden="true" class="fa fa-close"></i></span> </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div> -->
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#socialPlatform" target="_self"><span class="social-platform-ico"><i aria-hidden="true" class="fa fa-twitter"></i><i aria-hidden="true" class="fa fa-facebook"></i></span>Social Platform</a> </h4>
              </div>
              <div id="socialPlatform" class="panel-collapse collapse">
                <div class="panel-body">
                  <div class="col-md-10">
                    <div id="addSocialProfiles">
                      <div class="row">
                        <div class="col-sm-4">
                          <label>Add Social Profiles</label>
                        </div>
                        <div class="col-sm-8">
                          <select class="select-social-profile" ng-change="addSocialProfile()" ng-model="selectedSocialProfile" ng-options="socialProfile as socialProfile.name for socialProfile in tempCompanysocialProfiles">
                            <option value="">Select Social Profiles</option>
                          </select>
                        </div>
                      </div>
                      <div class="add-social-listing" ng-show="socialListing">
                        <div class="form-group row" ng-repeat="sprofile in usersocialProfiles">
                          <div class="col-sm-4">
                            <label class="form-control-label">{{sprofile.name}} url</label>
                          </div>
                          <div class="col-sm-8">
                            <input type="url" class="form-control" placeholder="Enter Url" ng-model="sprofile.sourceURL">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#mainstreamMedia" target="_self"><span class="mediastream-media-ico"><i aria-hidden="true" class="fa fa-newspaper-o"></i></span>Mainstream Media</a> </h4>
              </div>
              <div id="mainstreamMedia" class="panel-collapse collapse">
                <div class="panel-body">
                  <div class="col-md-10">
                    <div id="addMainstreamMedia">
                      <div class="row">
                        <div class="col-sm-4">
                          <label>Add Mainstream Media</label>
                        </div>
                        <div class="col-sm-8">
                          <select class="select-mainstream-media" ng-change="addMainstreamMedia()" ng-model="selectedMainstreamMedia" ng-options="mainstreamMedias as mainstreamMedias.name for mainstreamMedias in tempCompanymainstreamMedias">
                            <option value="">Select Social Profiles</option>
                          </select>
                        </div>
                      </div>
                      <div class="add-mainstream-listing" ng-show="mainstreamListing">
                        <div class="form-group row" ng-repeat="msMedia in usermainstreamMedias">
                          <div class="col-sm-4">
                            <label class="form-control-label">{{msMedia.name}} url</label>
                          </div>
                          <div class="col-sm-8">
                            <input type="url" class="form-control" placeholder="Enter Url" ng-model="msMedia.sourceURL">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#otherRelevant" target="_self"><span class="other-relevant-ico"></span>Other Relevant Sources</a> </h4>
              </div>
              <div id="otherRelevant" class="panel-collapse collapse">
                <div class="panel-body">
                  <div class="col-md-10">
                    <div id="addotherSources">
                      <div class="row">
                        <div class="col-sm-4">
                          <label>Add Other Relevant Sources</label>
                        </div>
                        <div class="col-sm-8">
                          <select class="select-other-sources" ng-change="addOtherSources()" ng-model="selectedOtherSource" ng-options="otherSource as otherSource.name for otherSource in tempCompanyotherSources">
                            <option value="">Select Social Profiles</option>
                          </select>
                        </div>
                      </div>
                      <div class="add-othersources-listing" ng-show="othersourcesListing">
                        <div class="form-group row" ng-repeat="orSource in userotherSources">
                          <div class="col-sm-4">
                            <label class="form-control-label">{{orSource.name}} url</label>
                          </div>
                          <div class="col-sm-8">
                            <input type="url" class="form-control" placeholder="Enter Url" ng-model="orSource.sourceURL">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title"><span></span> <a data-toggle="collapse" data-parent="#accordion" href="#competitors" target="_self"><span class="competitors-ico"></span>Competitors</a> </h4>
              </div>
              <div id="competitors" class="panel-collapse collapse">
                <div class="panel-body">
                  <div class="col-md-10">
                    <div class="row">
                      <div id="addCompetitors">
                        <div class="form-group row">
                          <div class="col-sm-4">
                            <label class="form-control-label">Competitors</label>
                          </div>
                          <div class="col-sm-8">
                            <input type="text" class="form-control add-com-input" ng-model="competitorName">
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-sm-4">
                            <label>Add Social Profiles</label>
                          </div>
                          <div class="col-sm-8">
                            <select class="select-social-profile" ng-change="addCSocialProfile()" ng-model="selectedComSocialProfile" ng-options="socialProfile as socialProfile.name for socialProfile in tempComsocialProfiles">
                              <option value="">Select Social Profiles</option>
                            </select>
                          </div>
                        </div>
                        <div class="add-othersources-listing">
                          <div class="form-group row" ng-repeat="cspSource in ComsocialProfiles">
                            <div class="col-sm-4">
                              <label class="form-control-label">{{cspSource.name}} url</label>
                            </div>
                            <div class="col-sm-8">
                              <input type="url" class="form-control" placeholder="Enter Url" ng-model="cspSource.sourceURL">
                            </div>
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-sm-4">
                            <label>Add Mainstream Media</label>
                          </div>
                          <div class="col-sm-8">
                            <select class="select-mainstream-media" ng-change="addCMainstreamMedia()" ng-model="selectedComMainstreamMedia" ng-options="mainstreamMedias as mainstreamMedias.name for mainstreamMedias in tempCommainstreamMedias">
                              <option value="">Select Mainstream Media</option>
                            </select>
                          </div>
                        </div>
                        <div class="add-othersources-listing">
                          <div class="form-group row" ng-repeat="cmmSource in CommainstreamMedias">
                            <div class="col-sm-4">
                              <label class="form-control-label">{{cmmSource.name}} url</label>
                            </div>
                            <div class="col-sm-8">
                              <input type="url" class="form-control" placeholder="Enter Url" ng-model="cmmSource.sourceURL">
                            </div>
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-sm-4">
                            <label>Add Other Relevant Sources</label>
                          </div>
                          <div class="col-sm-8">
                            <select class="select-other-sources" ng-change="addCOtherSources()" ng-model="selectedComOtherSource" ng-options="otherSource as otherSource.name for otherSource in tempComotherSources">
                              <option value="">Select Other Relevant Sources</option>
                            </select>
                          </div>
                        </div>
                        <div class="add-othersources-listing">
                          <div class="form-group row" ng-repeat="corSource in ComotherSourceslist ">
                            <div class="col-sm-4">
                              <label class="form-control-label">{{corSource.name}} url</label>
                            </div>
                            <div class="col-sm-8">
                              <input type="url" class="form-control" placeholder="Enter Url" ng-model="corSource.sourceURL">
                            </div>
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-sm-offset-4 col-sm-8">
                            <button id="addCompetitorbtn" type="submit" class="btn btn-default add-com-btn" ng-click="addCompetitors()">Add Competitors</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-offset-4 col-sm-8" ng-show="comListing">
                        <div class="competitor-list">
                          <div class="competitor-btn" ng-repeat="competitor in userInfo.competitorsList track by $index">
                            <div ng-bind="competitor.competitorName"></div>
                            <span ng-click="deleteCompetitor($index)"><i aria-hidden="true" class="fa fa-close"></i></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="input-group-btn nbt-select-btn add-all-users">
          <button class="btn btn-default" ng-click="addAllUsers()" type="button">Save Details</button>
          </div> </div>
      </form>
    </div>
  </div>
</div>

<!-- add-info-summary page -->

<div class="col-md-12 addinfo-wrapper" ng-show="addinfosuccess">
	<div class="activeprofile sm-admin">
	  <div class="block-cover">
	    <div class="block-body">
	      <div class="row">
	        <div class="col-md-12">
	          <h4>Thanks for choosing XpressoInsights. We are always here to help
	            you.</h4>
	          <p>All your information is successfully added.</p>
	        </div>
	      </div>
	      <div>
	        <h3>Add Information</h3>
	        <form name="addUserInfo" class="ng-pristine ng-valid">
	          <div>
	          <div class="admin-summary">
	            <div class="summary-panel">
	              <h4>Admin details</h4>
	              <div class="row">
	                <div class="col-md-6">
	                  <div class="form-group row">
	                    <div class="col-sm-4"> <b>Name</b> </div>
	                    <div class="col-sm-8"><span ng-bind="authName"></span></div>
	                  </div>
	                </div>
	                <div class="col-md-6">
	                  <div class="form-group row">
	                    <div class="col-sm-4"> <b>Email</b> </div>
	                    <div class="col-sm-8"><span ng-bind="authEmail"></span></div>
	                  </div>
	                </div>
	              </div>
	            </div>
	            <div class="summary-panel">
	              <h4>Admin social platform details</h4>
	              <table class="table table-bordered">
					  <thead>
					      <tr>
					        <th>Source Name</th>
					        <th>Source Url</th>
					      </tr>
					  </thead>
					  <tbody ng-repeat="detail in userInfo.sourcesDetailsList">
						  <tr>
						    <td>{{ detail.name }}</td>
						    <td>{{ detail.sourceURL }}</td>
						  </tr>
					  </tbody>
				</table>
	            </div>
	            <div class="summary-panel">
	              <h4>Admin user details</h4>
	              <table class="table table-bordered">
					  <thead>
					      <tr>
					        <th>User Name</th>
					        <th>User Email</th>
					      </tr>
					  </thead>
					  <tbody ng-repeat="user in userInfo.usersList">
						  <tr>
						    <td>{{ user.userName }}</td>
						    <td>{{ user.userEmail }}</td>
						  </tr>
					  </tbody>
				  </table>
	              <div class="summary-panel">
	                <h4>Competitors details</h4>
	                <table class="table table-bordered">
					  <thead>
					      <tr>
					        <th>Competitor Name</th>
					        <th>Source</th>
					        <th>Url</th>
					      </tr>
					  </thead>
					  <tbody ng-repeat="comp in userInfo.competitorsList">
						  <tr>
						    <td rowspan="{{comp.competitorSources.length}}">{{comp.competitorName}}</td>
						    <td>{{comp.competitorSources[0].name}}</td>
						    <td>{{comp.competitorSources[0].sourceURL}}</td>
						  </tr>
						  <tr ng-repeat="detail in comp.competitorSources" ng-if="$index>0">
						    <td>{{ detail.name }}</td>
						    <td>{{ detail.sourceURL }}</td>
						  </tr>
					  </tbody>
					</table>
	              </div>
	            </div>
	          </div>
	        </form>
	      </div>
	    </div>
	  </div>
	</div>
</div>
