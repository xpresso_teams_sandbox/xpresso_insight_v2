
<!-- dashboard page -->


<div id="dashboard" class="row">


	<!--Brand Health Score-->
	<div class="col-md-6" id="navDiv1">
		<div class="block-cover-happiness-index">
			<div class="block-head-happiness-index">
				<i class="fa fa-area-chart" aria-hidden="true"></i>
				<h3 class="display-in-block">SENTIMENT INDEX LIVE</h3>
				<div class="pull-right select-interval">
					<div class="pull-left">
						<select style="border: 0px solid #ccc !important;"
							class="selectpicker nbt-select" id="setRange"
							ng-change="intervalChange()" ng-model="intervalValue">
							<option value='Daily'>Hourly Interval</option>
							<option value='Weekly'>Daily Interval</option>
						</select>
					</div>
				</div>
			</div>
			<div class="block-body" id="chart1"></div>
			
		</div>
	</div>
	<div id="navDiv2" class="col-md-6">
		<div class="block-cover-competitive-index">
			<div class="block-head-competitive-index">
				<i class="fa fa-line-chart" aria-hidden="true"></i>
				<h3 class="display-in-block">COMPETITIVE SENTIMENT INDEX</h3>
				<!-- 				<h3>MONTHLY HAPPINESS INDEX</h3> -->
				<div class="pull-right select-interval">
					<div class="pull-left">
						<select style="border: 0px solid #ccc !important;"
							data-ng-options="topic for topic in topics"
							ng-model=selectedContextMonthly
							ng-change="fn_brand_comparission_monthly()"
							class="selectpicker nbt-select"
							id="topics_Monthly_Competitive_HI">
						</select>
					</div>
				</div>
			</div>
			<div class="block-body" id="chart2"></div>
		</div>
	</div>
	
	<div class="col-md-12">
		<div class="block-cover">
			<div class="block-body" style="height: auto">
				<div class="block-inner-head col-md-12 filter"
					style="border-top: 0px;">
					<i class="fa fa-filter" aria-hidden="true"></i> <select
						data-ng-model="selectedSource"
						data-ng-options="source[1] as source[0] for source in sources"
						class="selectpicker nbt-select-new" id="source">
						<option style="display: none;" value="">Source</option>
					</select> <select data-ng-model="selectedTopic"
						data-ng-options="topic as topic for topic in topics track by topic"
						class="selectpicker nbt-select-new" id="topics">
						<option style="display: none;" value="">Context</option>
					</select> <select class="selectpicker nbt-select-new" id="sentiment"
						ng-model="selectedSentiment">
						<option style="display: none;" value="">Sentiment</option>
						<option value="All">All</option>
						<option>Positive</option>
						<option>Negative</option>
						<option>Neutral</option>
					</select>
					<div class="dropdown">
						<a class="dropdown-toggle" id="analyticsFrom" role="button"
							data-toggle="dropdown" data-target="_self" href="">
							<div class="input-group">
								<input type="text" class="form-control"
									data-ng-model="analyticsFrom" placeholder="From" readonly>
								<span class="input-group-addon"><i
									class="glyphicon glyphicon-calendar"></i></span>
							</div>
						</a>
						<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
							<datetimepicker data-ng-model="analyticsFrom"
								data-before-render="dateRangeValidation($view, $dates, $leftDate, $upDate, $rightDate)"
								data-datetimepicker-config="{ dropdownSelector: '#analyticsFrom', modelType: 'DD-MM-YYYY HH:mm', minuteStep: 1  }" />
						</ul>
					</div>
					<div class="dropdown">
						<a class="dropdown-toggle" id="analyticsTo" role="button"
							data-toggle="dropdown" data-target="_self" href="">
							<div class="input-group">
								<input type="text" class="form-control"
									data-ng-model="analyticsTo" placeholder="To" readonly>
								<span class="input-group-addon"><i
									class="glyphicon glyphicon-calendar"></i></span>
							</div>
						</a>
						<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
							<datetimepicker data-ng-model="analyticsTo"
								data-before-render="dateRangeValidation($view, $dates, $leftDate, $upDate, $rightDate)"
								data-datetimepicker-config="{ dropdownSelector: '#analyticsTo', modelType: 'DD-MM-YYYY HH:mm', minuteStep: 1 }" />
						</ul>
					</div>
					<span class="input-group-btn nbt-select-btn">
						<button class="btn btn-default" aria-hidden="true" class=""
							type="button" ng-click="fn_filter()">
							<span aria-hidden="true" class="fa fa-arrow-right"></span>&nbsp;Go
						</button>
					</span>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="col-md-12" id="viralityTable">
		<div class="block-cover">
			<!-- <div
				class="block-head block-inner-head col-md-12 padding-top-15 clearfix">
				<i class="fa fa-filter" aria-hidden="true"></i>
				<div class="dropContainer pull-left">

					<h4>Show</h4>
					<select class="selectpicker nbt-select" id="selPaginationRange"
						ng-model="entries">
						<option value="10">10 entries</option>
						<option value="20">20 entries</option>
						<option value="30">30 entries</option>
						<option value="40">40 entries</option>
						<option value="50">50 entries</option>
					</select>
				</div>
				<div class="dropContainer pull-left">
					<h4>From:</h4>
					<div class="dropdown">
						<a class="dropdown-toggle" id="dashboardFrom" role="button"
							data-toggle="dropdown" data-target="_self" href="">
							<div class="input-group">
								<input type="text" class="form-control"
									data-ng-model="$parent.dashboardFrom" readonly><span
									class="input-group-addon"><i
									class="glyphicon glyphicon-calendar"></i></span>
							</div>
						</a>
						<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
							<datetimepicker data-ng-model="$parent.dashboardFrom"
								data-before-render="dateRangeValidation($view, $dates, $leftDate, $upDate, $rightDate)"
								data-datetimepicker-config="{ dropdownSelector: '#dashboardFrom', modelType: 'DD-MM-YYYY HH:mm', minuteStep: 1  }" />
						</ul>
					</div>
				</div>
				<div class="dropContainer pull-left">
					<h4>To:</h4>
					<div class="dropdown">
						<a class="dropdown-toggle" id="dashboardTo" role="button"
							data-toggle="dropdown" data-target="_self" href="">
							<div class="input-group">
								<input type="text" class="form-control"
									data-ng-model="$parent.dashboardTo" readonly><span
									class="input-group-addon"><i
									class="glyphicon glyphicon-calendar"></i></span>
							</div>
						</a>
						<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
							<datetimepicker data-ng-model="$parent.dashboardTo"
								data-before-render="dateRangeValidation($view, $dates, $leftDate, $upDate, $rightDate)"
								data-datetimepicker-config="{ dropdownSelector: '#dashboardTo', modelType: 'DD-MM-YYYY HH:mm', minuteStep: 1 }" />
						</ul>
					</div>
				</div>
				<span class="input-group-btn nbt-select-btn">
					<button type="button" class="btn btn-default"
						ng-click="fn_filter()">
						<span aria-hidden="true" class="fa fa-arrow-right"></span>
						&nbsp;Go
					</button>
				</span>

			</div>
			 -->
			 <div class="block-head">
				<h3>Context Details</h3>
			</div>
			<div class="block-body padding-0">
				<div class="auto_scrollable">
					<table class="table table-hover nbt-table" id="tblVirality">
						<thead>
							<tr>
								<th>Context</th>
								<th>Source</th>
								<th class="pointer"
									ng-click="sortTypeEntity='-5'; sortReverseEntity = !sortReverseEntity">Feedback
									<span ng-show="sortTypeEntity == '-5' && !sortReverseEntity"
									class="fa fa-caret-up"></span> <span
									ng-show="sortTypeEntity == '-5' && sortReverseEntity"
									class="fa fa-caret-down"></span>
								</th>
								<th width="10%" class="text-right pointer"
									ng-click="sortTypeEntity='-10'; sortReverseEntity = !sortReverseEntity">
									Complaint <span
									ng-show="sortTypeEntity == '-10' && !sortReverseEntity"
									class="fa fa-caret-up"></span> <span
									ng-show="sortTypeEntity == '-10' && sortReverseEntity"
									class="fa fa-caret-down"></span>
								</th>
								<th>Sentiment</th>
								<th class="td-align-center">View</th>
							</tr>
						</thead>
						<tbody id="tableViralityComparission" class="ng-cloak">
							<tr>
								<td class="td-align-center no-data" ng-if="viralities==''"
									colspan="6" ng-bind="'No Data Found'"></td>
							</tr>
							<tr
								dir-paginate="virality in viralities|orderBy:sortTypeEntity:sortReverseEntity|itemsPerPage:entries"
								current-page="currentPage">
								<td
									ng-class="virality[6]>virality[7] && virality[6]>virality[8]?'text-positive': virality[7]>=virality[6] && virality[7]>=virality[8] ? 'text-negative':'text-neutral'"><strong><span
										ng-bind="virality[4]"></span></strong></td>
								<td><i
									ng-class="virality[2]==1?'fa fa-facebook-official': virality[2]==2?'fa fa-twitter':'fa fa-rss'"
									aria-hidden="true"></i><span ng-bind="virality[3]"></span></td>
								<td class="text-right"><span ng-bind="virality[5]"></span></td>
								<!--<td>
										<strong>
											<span ng-bind="fn_severityScoreRange($index)" ng-class="fn_severityScoreRange($index)=='HIGH'?'text-red':fn_severityScoreRange($index)=='MEDIUM'?'text-yellow':'text-green'">
											</span>
										</strong>
									</td>-->
								<td class="text-right"><span ng-bind="virality[10]"></span></td>
								<td>
									<div class="sen-box">
										<span class="btn sentimental-bar bgcolor-positive"
											data-toggle="tooltip" title="{{ virality[6] }}%"
											ng-style="{width: '{{ virality[6] }}%'}"></span> <span
											class="btn sentimental-bar bgcolor-neutral"
											data-toggle="tooltip" title="{{ virality[8] }}%"
											ng-style="{width: '{{ 100 - (virality[6] + virality[7]) }}%'}"></span>
										<span class="btn sentimental-bar bgcolor-negative"
											data-toggle="tooltip" title="{{ virality[7] }}%"
											ng-style="{width: '{{ virality[7] }}%'}"></span>
									</div>
								</td>
								<!-- <td>{{ virality[9] }}</td> -->
								<td class="td-align-center"><a
									ng-href="#analytics/{{virality[2]}}/{{virality[4]}}"
									ng-click="activeMenu='analytics'"><i
										class="fa fa-eye fa-lg" aria-hidden="true"></i></a></td>
							</tr>
						</tbody>
					</table>
				</div>
				<dir-pagination-controls class="pull-right" max-size="5"
					direction-links="true" boundary-links="true"
					template-url="dirPagination.tpl.html"> </dir-pagination-controls>
				<div class="clearfix"></div>
			</div>
		</div>

		<!--Contextwise Sentiment Distribution-->
		<!-- <div class="block-cover" id="sentimentDistributionMonthlyGraphDiv">
			<div class="block-head-competitive-index">
				<i class="fa fa-bar-chart" aria-hidden="true"></i>
				<h3 class="display-in-block">MONTHLY CONTEXTWISE SENTIMENT</h3>
				<div class="pull-right select-interval">
					<div class="pull-left">
						<label>Select Sentiment: </label>
						<select
							style="border: 0px solid #ccc !important;"
							class="selectpicker nbt-select" id="selectSentiment"
							ng-change="fn_contextwise_sentiment_distribution(); fn_contextwise_sentiment_distribution_percentile()"
							ng-model="sentiment">
							<option value='Negative'>Negative</option>
							<option value='Positive'>Positive</option>
							<option value='Neutral'>Neutral</option>
						</select>
					</div>
				</div>
			</div>
			<div class="block-body" id="contextWiseSentimentDistributionChart">
				<div class="row">
					<div class="col-md-6">
		             <div id="contextWiseChart"></div>
		             <h3 class="align-center">SENTIMENT DISTRIBUTION BY COUNT</h3>
		           	</div>
					<div class="col-md-6">
		             <div id="contextWisePercentChart"></div>
		             <h3 class="align-center">SENTIMENT DISTRIBUTION BY %</h3>
		           	</div>
				</div>
			</div>
		</div> -->
		<div class="clearfix"></div>
		<!--Contextwise Sentiment Distribution-->
		<!--Deep Listening & Trending topics-->
		<div id="analytics" class="row">
			<div class="col-md-12">
				<div class="block-cover">
					<div class="block-body">
						<div class="nbt-tab">
							<!-- Nav tabs -->
							<ul class="nav nav-tabs nbt-tab-ul" role="tablist">
								<li role="presentation" ng-click="activeTab='social'"
									ng-class="{'active': activeTab == 'social'}"><a
									aria-controls="Social" role="tab" data-toggle="tab"><i
										class="fa fa-table" aria-hidden="true"></i> Deep Listening</a></li>
								<li role="presentation" ng-click="activeTab='sentiment'"
									ng-class="{'active': activeTab == 'sentiment'}"><a
									aria-controls="Sentiment" id="sentimentTabHeader" role="tab"
									data-toggle="tab"><i class="fa fa-bolt" aria-hidden="true"></i>Trending
										topics</a></li>
							</ul>
							<!-- Tab panes -->
							<div class="tab-content nbt-content row">
								<div role="tabpanel" class="tab-pane"
									ng-class="{'active': activeTab == 'social'}" id="Social">

									<div class="auto_scrollable">
										<table class="table table-hover nbt-table" id="tblsocList">
											<thead>
												<tr>
													<!-- <th><label class="nbt-checkbox"><input type="checkbox"><span></span></label></th> -->
													<th>Source</th>
													<th>Post Type</th>
													<th>Date-time</th>
													<th>Text</th>
													<th>Sentiment</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody id="tableSocialListening">
												<tr>
													<td class="td-align-center no-data" ng-if="soclists==''"
														colspan="6" ng-bind="'No Data Found'"></td>
												</tr>
												<tr dir-paginate="soclist in soclists|itemsPerPage:10"
													pagination-id="tblsocList-paginate">
													<td><i
														ng-class="soclist[2]==1?'fa fa-facebook-official': soclist[2]==2?'fa fa-twitter':'fa fa-rss'"
														aria-hidden="true"></i><span ng-bind="soclist[3]"></span></td>
													<td><span ng-bind="soclist[1]"></span></td>
													<td><span ng-bind="soclist[4]"></span></td>
													<td><span ng-bind="soclist[5] | limitTo:100"></span><a
														href="{{ soclist[6] }}" target="_blank" class="read-more">
															read more...</a></td>
													<td
														ng-class="soclist[8]=='Positive'? 'text-green': soclist[8]=='Negative' ? 'text-red':'text-yellow'"><strong><span
															ng-bind="soclist[8]"></span></strong></td>
													<td><a class="open-tkt-create-modal"
														data-toggle="modal" data-id="{{ soclist[7] }}"
														data-target="#tkt-create-modal"><i
															class="fa fa-ticket"></i></a></td>
												</tr>
											</tbody>
										</table>
									</div>
									<dir-pagination-controls pagination-id="tblsocList-paginate"
										class="pull-right" max-size="5" direction-links="true"
										boundary-links="true" template-url="dirPagination.tpl.html"></dir-pagination-controls>
								</div>
								<div role="tabpanel" class="tab-pane text-center"
									ng-class="{'active': activeTab == 'sentiment'}" id="Sentiment"
									style="width: 100%; box-sizing: border-box;">
									<div class="senti-box">
										<div class="row">
											<div
												class="col-lg-5 col-md-12 col-sm-12 col-xs-12 no-padding">
												<div class="word-cloud-title">
													<i class="fa fa-arrow-left pointer" aria-hidden="true"
														ng-hide="wordcloudContext=='All Contexts' || (selectedTopic!='' && selectedTopic!='All Contexts')"
														ng-click="filterContext=''; filterEntity=''; fn_word_cloud(null)"></i>
													<span ng-bind="wordcloudContext"></span>
												</div>
												<div id="wordcloud"
													class="text-center margin-left-15 border"></div>
											</div>
											<div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
												<div
													class="auto_scrollable border margin-bottom-15 border-top-0"
													ng-style="(contextList || entityList) && {'min-height': '340px'} || {'min-height': 'auto'}"
													style="position: relative; top: -1px;">
													<table class="table table-hover nbt-table margin-0"
														id="tblsocList2">
														<thead>
															<tr>
																<th>Source</th>
																<th><span>Entity</span> <i
																	class="fa fa-filter pointer selectEntity"
																	aria-hidden="true"
																	ng-click="contextList = false;entityList = !entityList"></i>
																	<div class="dropdownList" ng-show="entityList">
																		<ul>
																			<li
																				ng-repeat="entity in entitiesForFilter track by $index"
																				class="pointer" ng-click="fn_filterByEntity(entity)"><span
																				ng-bind="entity"></span></li>
																		</ul>
																	</div></th>
																<th><span>Context</span> <i
																	class="fa fa-filter pointer selectEntity"
																	aria-hidden="true"
																	ng-hide="selectedTopic!='' && selectedTopic!='All Contexts'"
																	ng-click="contextList = !contextList; entityList = false"></i>
																	<div class="dropdownList" ng-show="contextList">
																		<ul>
																			<li ng-repeat="topic in topics" class="pointer"
																				ng-click="fn_word_cloud(topic)"><span
																				ng-bind="topic"></span></li>
																		</ul>
																	</div></th>
																<th>Date-time</th>
																<th>Text</th>
																<th>Sentiment</th>
															</tr>
														</thead>
														<tbody id="tableWordCloud">
															<tr>
																<td class="text-center td-align-center no-data"
																	ng-if="soclists==''" colspan="6"
																	ng-bind="'No Data Found'"></td>
															</tr>
															<tr
																dir-paginate="data in wordCloudTableData|filter:{aspect:filterContext, entity:filterEntity}:exceptEmptyComparator|itemsPerPage:6"
																pagination-id="tblword-paginate" ng-init="limit= 30">
																<td><i
																	ng-class="data.source_type_id==1?'fa fa-facebook-official': data.source_type_id==2?'fa fa-twitter':'fa fa-rss'"
																	aria-hidden="true"></i><span
																	ng-bind="data.source_type_name"></span></td>
																<td><span ng-bind="data.entity | limitTo:limit"></span>
																	<a class="read-more"> <span
																		ng-click="limit=more_less=='more'?data.entity.length:30; more_less=more_less=='more'?'less':'more'"
																		ng-bind="data.entity.length>30?more_less:''"></span>
																</a></td>
																<td><span ng-bind="data.aspect"></span></td>
																<td><span ng-bind="data.post_datetime"></span></td>
																<td><span ng-bind="data.post_text | limitTo:40"></span><a
																	href="{{ data.post_url }}" target="_blank"
																	class="read-more"> read more...</a></td>
																<td
																	ng-class="data.overall_sentiment=='Positive'? 'text-green': data.overall_sentiment=='Negative' ? 'text-red':'text-yellow'"><strong><span
																		ng-bind="data.overall_sentiment"></span></strong></td>
															</tr>
														</tbody>
													</table>
												</div>
												<dir-pagination-controls pagination-id="tblword-paginate"
													class="pull-right" max-size="5" direction-links="true"
													boundary-links="true" template-url="dirPagination.tpl.html">
												</dir-pagination-controls>
												<!-- </div> -->
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
