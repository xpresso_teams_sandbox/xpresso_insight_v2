<!-- add-info-summary page -->

<div class="col-md-12 addinfo-wrapper">
<div class="activeprofile sm-admin">
  <div class="block-cover">
    <div class="block-body">
      <div class="row">
        <div class="col-md-12">
          <h4>Thanks for choosing XpressoInsights. We are always here to help
            you.</h4>
          <p>All your information is successfully added.</p>
        </div>
      </div>
      <div>
        <h3>Add Information</h3>
        <form name="addUserInfo" class="ng-pristine ng-valid">
          <div>
          <div class="admin-summary">
            <div class="summary-panel">
              <h4>Admin details</h4>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group row">
                    <div class="col-sm-4"> <b>Name</b> </div>
                    <div class="col-sm-8">Admin Name</div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group row">
                    <div class="col-sm-4"> <b>Email</b> </div>
                    <div class="col-sm-8">admin@domailname.com</div>
                  </div>
                </div>
              </div>
            </div>
            <div class="summary-panel">
              <h4>Admin social platform details</h4>
              <ul class="sm-sp">
                <li>
                  <div class="sm-sp-col1"></div>
                  <div class="sm-sp-col2">
                    <ul>
                      <li><b>Source name</b></li>
                      <li><b>Source url</b></li>
                    </ul>
                  </div>
                </li>
                <li>
                  <div class="sm-sp-col1">Social platform</div>
                  <div class="sm-sp-col2">
                    <ul>
                      <li>Facebook</li>
                      <li>http://www.facebook.com</li>
                    </ul>
                    <ul>
                      <li>Twitter</li>
                      <li>http://www.twitter.com</li>
                    </ul>
                  </div>
                </li>
                <li>
                  <div class="sm-sp-col1">Mainstream media</div>
                  <div class="sm-sp-col2">
                    <ul>
                      <li>Economics times</li>
                      <li>http://www.et.com</li>
                    </ul>
                  </div>
                </li>
                <li>
                  <div class="sm-sp-col1">Other relavent sources</div>
                  <div class="sm-sp-col2">
                    <ul>
                      <li>Source 1</li>
                      <li>http://www.source1.com</li>
                    </ul>
                    <ul>
                      <li>Source 2</li>
                      <li>http://www.source2.com</li>
                    </ul>
                    <ul>
                      <li>Source 3</li>
                      <li>http://www.source3.com</li>
                    </ul>
                  </div>
                </li>
              </ul>
            </div>
            <div class="summary-panel">
              <h4>Admin user details</h4>
              <ul class="sm-cd">
                  <li>
                    <div class="sm-cd-col1"></div>
                    <div class="sm-cd-col2"><b>User 1</b></div>
                    <div class="sm-cd-col3"><b>User 2</b></div>
                    <div class="sm-cd-col4"><b>User 3</b></div>
                  </li>
                  <li>
                    <div class="sm-cd-col1">User Name</div>
                    <div class="sm-cd-col2">Username 1</div>
                    <div class="sm-cd-col3">Username 2</div>
                    <div class="sm-cd-col4">Username 3</div>
                  </li>
                  <li>
                    <div class="sm-cd-col1">User Email</div>
                    <div class="sm-cd-col2">user1@email.com</div>
                    <div class="sm-cd-col3">user2@email.com</div>
                    <div class="sm-cd-col4">user3@email.com</div>
                  </li>
                </ul>
              <div class="summary-panel">
                <h4>Competitors details</h4>
                <ul class="sm-cd">
                  <li>
                    <div class="sm-cd-col1"></div>
                    <div class="sm-cd-col2"><b>Competitor 1</b></div>
                    <div class="sm-cd-col3"><b>Competitor 2</b></div>
                    <div class="sm-cd-col4"><b>Competitor 3</b></div>
                  </li>
                  <li>
                    <div class="sm-cd-col1">Competitor Name</div>
                    <div class="sm-cd-col2">Competitorname 1</div>
                    <div class="sm-cd-col3">Competitorname 2</div>
                    <div class="sm-cd-col4">Competitorname 3</div>
                  </li>
                  <li>
                    <div class="sm-cd-col1">Competitor Email</div>
                    <div class="sm-cd-col2">competitor1@email.com</div>
                    <div class="sm-cd-col3">competitor2@email.com</div>
                    <div class="sm-cd-col4">competitor3@email.com</div>
                  </li>
                  <li>
                    <div class="sm-cd-col1">Facebook</div>
                    <div class="sm-cd-col2">http://www.facebook.com/Competitor1</div>
                    <div class="sm-cd-col3">http://www.facebook.com/Competitor2</div>
                    <div class="sm-cd-col4">http://www.facebook.com/Competitor3</div>
                  </li>
                  <li>
                    <div class="sm-cd-col1">Twitter</div>
                    <div class="sm-cd-col2">http://www.twitter.com/Competitor1</div>
                    <div class="sm-cd-col3">http://www.twitter.com/Competitor2</div>
                    <div class="sm-cd-col4">http://www.twitter.com/Competitor3</div>
                  </li>
                  <li>
                    <div class="sm-cd-col1">Economics times</div>
                    <div class="sm-cd-col2">http://www.et.com/Competitor1</div>
                    <div class="sm-cd-col3">http://www.et.com/Competitor2</div>
                    <div class="sm-cd-col4">http://www.et.com/Competitor3</div>
                  </li>
                  <li>
                    <div class="sm-cd-col1">Source 1</div>
                    <div class="sm-cd-col2">http://www.source1.com/Competitor1</div>
                    <div class="sm-cd-col3">http://www.source1.com/Competitor2</div>
                    <div class="sm-cd-col4">http://www.source1.com/Competitor3</div>
                  </li>
                  <li>
                    <div class="sm-cd-col1">Source 2</div>
                    <div class="sm-cd-col2">http://www.source2.com/Competitor1</div>
                    <div class="sm-cd-col3">http://www.source2.com/Competitor2</div>
                    <div class="sm-cd-col4">http://www.source2.com/Competitor3</div>
                  </li>
                  <li>
                    <div class="sm-cd-col1">Source 3</div>
                    <div class="sm-cd-col2">http://www.source3.com/Competitor1</div>
                    <div class="sm-cd-col3">http://www.source3.com/Competitor2</div>
                    <div class="sm-cd-col4">http://www.source3.com/Competitor3</div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
