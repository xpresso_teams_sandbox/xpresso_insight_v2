<div id="campaign">
	<div class="row">
		<div class="col-md-12">
			<div class="page-title">
				<div class="row">
					<div class="col-md-6">
						<h2>{{activeMenu}}</h2>
					</div>
					<div class="col-md-6">
						<div class="pull-right page-filter">
							<!-- Select box for selecting time interval Daily, weekly, monthly --> 
							<select data-ng-model=""  class="selectpicker ml-1" id="campaignTimeSelection">
								<option value="{{city4[0]}}" ng-repeat="city4 in avgRatingCities">{{city4[1]}}</option>
							</select>
							<label class="pull-right ml-3">By:</label>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="select-campaign mb-3">
				<label class="pull-left mr-3">Select Campaign:</label>
				<select data-ng-model="" class="selectpicker ml-1 pull-left" id="selectCampaign">
					<option value="{{city4[0]}}" ng-repeat="city4 in avgRatingCities">{{city4[1]}}</option>
				</select>
				<div class="clearfix"></div>
			</div>
			<!-- Campaign Information< Block starts -->
			<div class="block-cover" id="campaignInformation<">
				<div class="block-head">
					<div class="row">
						<div class="col-md-6">
							<h3 class="block-name">Campaign Information</h3>
						</div>
					</div>
				</div>
				<div class="block-body">
					<div class="grey-bg campaign-info-wapper">
						<div class="row">
							<div class="col-md-3"><div class="campaign-info-label">Label</div></div>
							<div class="col-md-9"><div class="campaign-info-det">details</div></div>
						</div>
						<div class="row">
							<div class="col-md-3"><div class="campaign-info-label">Label</div></div>
							<div class="col-md-9"><div class="campaign-info-det">details</div></div>
						</div>
					</div>
				</div>
			</div>
			<!-- Campaign Information< Block ends -->
			<!-- Summary Metrics Block starts -->
			<div class="block-cover" id="summaryMetrics">
				<div class="block-head">
					<div class="row">
						<div class="col-md-6">
							<h3 class="block-name">Summary Metrics</h3>
						</div>
					</div>
				</div>
				<div class="block-body">
					<div class="grey-bg">
						<table class="brand-table">
							<tbody>
								<tr>
									<td>
										<i class="fa fa-caret-up bg-green metrics-icon" aria-hidden="true"></i>
										<div class="metrics-content">
											<h3 class="metrics-title">Mentions <span class="metrics-count">8,475</span></h3>
											<div class="metrics-desp">20% more mentions this week</div>
										</div>
									</td>
									<td>
										<div class="metrics-bar-data">
											<div class="bar-lt bg-green" style="width:70%"></div>
											<div class="bar-rt bg-red" style="width:30%"></div>
										</div>
										<div class="row graph-con">
											<div class="col-md-6">This week	<span class="ml-3">8,475</span></div>
											<div class="col-md-6 text-right">Last week	<span class="ml-3">8,475</span></div>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<i class="fa fa-caret-down bg-red metrics-icon" aria-hidden="true"></i>
										<div class="metrics-content">
											<h3 class="metrics-title">Sentiments <span class="metrics-count">8,475</span></h3>
											<div class="metrics-desp">20% more mentions this week</div>
										</div>
									</td>
									<td>
										<div class="metrics-bar-data">
											<div class="bar-lt bg-green" style="width:45%"></div>
											<div class="bar-rt bg-red" style="width:55%"></div>
										</div>
										<div class="row graph-con">
											<div class="col-md-6">Positive <i class="fa fa-caret-down txt-red ml-4 mr-4" aria-hidden="true"></i> <span>8,475</span></div>
											<div class="col-md-6 text-right">Negative <i class="fa fa-caret-up txt-green ml-4 mr-4" aria-hidden="true"></i><span>8,475</span></div>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<i class="fa fa-caret-up bg-green metrics-icon" aria-hidden="true"></i>
										<div class="metrics-content">
											<h3 class="metrics-title">Users <span class="metrics-count">8,475</span></h3>
											<div class="metrics-desp">20% more mentions this week</div>
										</div>
									</td>
									<td>
										<div class="metrics-bar-data">
											<div class="bar-lt bg-orange" style="width:70%"></div>
										</div>
										<div class="row graph-con">
											<div class="col-md-6">This week	<span class="ml-3">8,475</span></div>
											<div class="col-md-6 text-right">Last week	<span class="ml-3">8,475</span></div>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<i class="fa fa-caret-up bg-green metrics-icon" aria-hidden="true"></i>
										<div class="metrics-content">
											<h3 class="metrics-title">Engagements <span class="metrics-count">8,475</span></h3>
											<div class="metrics-desp">20% more mentions this week</div>
										</div>
									</td>
									<td>
										<div class="metrics-bar-data">
											<div class="bar-lt bg-orange" style="width:50%"></div>
										</div>
										<div class="row graph-con">
											<div class="col-md-6">This week	<span class="ml-3">8,475</span></div>
											<div class="col-md-6 text-right">Last week	<span class="ml-3">8,475</span></div>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- Summary Metrics Block ends -->
			<!-- Campaign Trend Line Block starts -->
			<div class="block-cover" id="campaignTrendLine">
				<div class="block-head">
					<div class="row">
						<div class="col-md-6">
							<h3 class="block-name">Campaign Trend Line</h3>
						</div>
					</div>
				</div>
				<div class="block-body">
					<div id="campaignTrendLineGraph" class="mt-10 mr-1"></div>
					<div class="graph-defn mb-3">Graph defn comes here</div>
				</div>
			</div>
			<!-- Brand Timeline Block ends -->
			<!-- Competitive Comparison Block starts -->
			<div class="block-cover" id="KPICampaignOverview">
				<div class="block-head">
					<div class="row">
						<div class="col-md-6">
							<h3 class="block-name">KPI - Campaign Overview</h3>
						</div>
					</div>
				</div>
				<div class="block-body">
					<div class="row">
						<div class="col-md-4">
							<div id="kpiMentions" class="pie-title-center mt-3" data-percent="78">
								<!-- <span class="pie-value"></span> -->
								<div class="pie-txt">
									<span class="value">464</span>
									<span class="title">Mentions</span>
								</div>
							</div>
							<div class="grey-bg">
								<div class="row">
									<div class="col-md-6">By KFC</div>
									<div class="col-md-6 text-right">5</div>
								</div>
								<div class="row">
									<div class="col-md-6">By Users</div>
									<div class="col-md-6 text-right">5</div>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div id="campKpiPosts" class="pie-title-center mt-3" data-percent="43">
								<!-- <span class="pie-value"></span> -->
								<div class="pie-txt">
									<span class="value">54</span>
									<span class="title">Posts</span>
								</div>
							</div>
							<div class="grey-bg">
								<div class="row">
									<div class="col-md-6">By KFC</div>
									<div class="col-md-6 text-right">5</div>
								</div>
								<div class="row">
									<div class="col-md-6">By Users</div>
									<div class="col-md-6 text-right">5</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Campaign Trend Line Block ends -->
			<div class="row">
				<div class="col-md-6">
					<!-- Top Hashtags Block starts -->
					<div class="block-cover" id="topHashtags">
						<div class="block-head">
							<div class="row">
								<div class="col-md-6">
									<h3 class="block-name">Top Hashtags</h3>
								</div>
							</div>
						</div>
						<div class="block-body">
							<div id="wordcloud" class="mt-10 mr-1"></div>
							<div class="graph-defn mb-3">Graph defn comes here</div>
						</div>
					</div>
					<!-- Top Hashtags Block ends -->
				</div>
				<div class="col-md-6">
					<!-- Top Sentiment Attributes Block starts -->
					<div class="block-cover" id="topSentimentAttributes">
						<div class="block-head">
							<div class="row">
								<div class="col-md-6">
									<h3 class="block-name">Top Sentiment Attributes</h3>
								</div>
							</div>
						</div>
						<div class="block-body">
							<div id="wordcloud" class="mt-10 mr-1"></div>
							<div class="graph-defn mb-3">Graph defn comes here</div>
						</div>
					</div>
					<!-- Top Sentiment Attributes Block ends -->
				</div>
			</div>
			
			<!-- Top Campaign Conversation Block starts -->
			<div class="block-cover" id="mostSharedContent">
				<div class="block-head">
					<div class="row">
						<div class="col-md-6">
							<h3 class="block-name">Top Campaign Conversation</h3>
						</div>
					</div>
				</div>
				<div class="block-body">
					<div class="row">
						<div class="col-md-6">
							<div id="wordcloud" class="mt-10 mr-1"></div>
							<div class="graph-defn mb-3">Graph defn comes here</div>
						</div>
						<div class="col-md-6 mt-3 mb-3">
						<div class="user-social-post-wapper">
							<div class="user-post-list">
								<div class="post-wapper">
									<div class="post-user-image">
										<img src="https://scontent-bom1-1.xx.fbcdn.net/v/t1.0-1/p200x200/17884070_1522200331143856_6131706401786435668_n.png?oh=87f2f427dbdcba1887debaa9606d8dc1&oe=5A90F30E" alt="Post user image"/>
									</div>
									<div class="post-desp-wapper">
										<div class="post-desp">
											<div class="user-name">KFC</div>
											<div class="post-time text-right">Nov 6 <i class="fa fa-facebook"></i></div>
										</div>
										<div class="post-content">
											<div>The highlight of the week! KFC Wednesday Special -12 for 300 offer! 12pc Boneless Chicken Strips & 4 dips only for Rs 300*! Walk-in/Call/Order: http://bit.ly/12for300 (Valid on Wednesday only) Tag your friends with whom you want to enjoy this delicious offer. *GST @ 18% extra</div>
										</div>
									</div>
								</div>
								<div class="review-wapper">
									<div>
										<div class="review-count">62,871</div>
										<div class="review-title">Engagements</div>
									</div>
									<div>
										<div class="review-count">62,871</div>
										<div class="review-title">Engagements</div>
									</div>
									<div>
										<div class="review-count">62,871</div>
										<div class="review-title">Engagements</div>
									</div>
								</div>
							</div>
							<div class="user-post-list">
								<div class="post-wapper">
									<div class="post-user-image">
										<img src="https://scontent-bom1-1.xx.fbcdn.net/v/t1.0-1/p200x200/17884070_1522200331143856_6131706401786435668_n.png?oh=87f2f427dbdcba1887debaa9606d8dc1&oe=5A90F30E" alt="Post user image"/>
									</div>
									<div class="post-desp-wapper">
										<div class="post-desp">
											<div class="user-name">KFC</div>
											<div class="post-time text-right">Nov 6 <i class="fa fa-facebook"></i></div>
										</div>
										<div class="post-content">
											<div>The highlight of the week! KFC Wednesday Special -12 for 300 offer! 12pc Boneless Chicken Strips & 4 dips only for Rs 300*! Walk-in/Call/Order: http://bit.ly/12for300 (Valid on Wednesday only) Tag your friends with whom you want to enjoy this delicious offer. *GST @ 18% extra</div>
										</div>
									</div>
								</div>
								<div class="review-wapper">
									<div>
										<div class="review-count">62,871</div>
										<div class="review-title">Engagements</div>
									</div>
									<div>
										<div class="review-count">62,871</div>
										<div class="review-title">Engagements</div>
									</div>
									<div>
										<div class="review-count">62,871</div>
										<div class="review-title">Engagements</div>
									</div>
								</div>
							</div>
						</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Top Campaign Conversation Block ends -->
			
			<!-- Most Shared Content Block starts -->
			<div class="block-cover" id="mostSharedContent">
				<div class="block-head">
					<div class="row">
						<div class="col-md-6">
							<h3 class="block-name">Most Shared Content</h3>
						</div>
					</div>
				</div>
				<div class="block-body">
					<div class="row">
						<div class="col-md-6 mt-3 mb-3">
							<div class="user-social-post-wapper">
								<div class="user-post-list">
									<div class="post-wapper">
										<div class="post-user-image">
											<div class="post-user-image">
												<img src="https://scontent-bom1-1.xx.fbcdn.net/v/t1.0-1/p200x200/17884070_1522200331143856_6131706401786435668_n.png?oh=87f2f427dbdcba1887debaa9606d8dc1&oe=5A90F30E" alt="Post user image"/>
											</div>
										</div>
										<div class="post-desp-wapper">
											<div class="post-desp">
												<div class="user-name">KFC</div>
												<div class="post-time text-right">Nov 6 <i class="fa fa-facebook"></i></div>
											</div>
											<div class="post-content">
												<div>The highlight of the week! KFC Wednesday Special -12 for 300 offer! 12pc Boneless Chicken Strips & 4 dips only for Rs 300*! Walk-in/Call/Order: http://bit.ly/12for300 (Valid on Wednesday only) Tag your friends with whom you want to enjoy this delicious offer. *GST @ 18% extra</div>
													<img src="https://scontent-bom1-1.xx.fbcdn.net/v/t1.0-0/p480x480/24059208_1755080157855871_7318460442704936370_n.png?oh=84703fe599334dcb44444e30e7129119&oe=5A94D667" alt=""/>
											</div>
										</div>
									</div>
									<div class="review-wapper">
										<div>
											<div class="review-count">62,871</div>
											<div class="review-title">Engagements</div>
										</div>
										<div>
											<div class="review-count">62,871</div>
											<div class="review-title">Engagements</div>
										</div>
										<div>
											<div class="review-count">62,871</div>
											<div class="review-title">Engagements</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6 mt-3 mb-3">
							<div class="user-social-post-wapper">
								<div class="user-post-list">
									<div class="post-wapper">
										<div class="post-user-image">
											<div class="post-user-image">
												<img src="https://scontent-bom1-1.xx.fbcdn.net/v/t1.0-1/p200x200/17884070_1522200331143856_6131706401786435668_n.png?oh=87f2f427dbdcba1887debaa9606d8dc1&oe=5A90F30E" alt="Post user image"/>
											</div>
										</div>
										<div class="post-desp-wapper">
											<div class="post-desp">
												<div class="user-name">KFC</div>
												<div class="post-time text-right">Nov 6 <i class="fa fa-facebook"></i></div>
											</div>
											<div class="post-content">
												<div>The highlight of the week! KFC Wednesday Special -12 for 300 offer! 12pc Boneless Chicken Strips & 4 dips only for Rs 300*! Walk-in/Call/Order: http://bit.ly/12for300 (Valid on Wednesday only) Tag your friends with whom you want to enjoy this delicious offer. *GST @ 18% extra</div>
													<img src="https://scontent-bom1-1.xx.fbcdn.net/v/t1.0-0/p480x480/24059208_1755080157855871_7318460442704936370_n.png?oh=84703fe599334dcb44444e30e7129119&oe=5A94D667" alt=""/>
											</div>
										</div>
									</div>
									<div class="review-wapper">
										<div>
											<div class="review-count">62,871</div>
											<div class="review-title">Engagements</div>
										</div>
										<div>
											<div class="review-count">62,871</div>
											<div class="review-title">Engagements</div>
										</div>
										<div>
											<div class="review-count">62,871</div>
											<div class="review-title">Engagements</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Most Shared Content Block ends -->
		</div>
	</div>
</div></div>