<!DOCTYPE html>
<!--[if IE 7]> 
<html lang="en-US" class="no-js IE7 IE">
	<![endif]-->
	<!--[if IE 8]> 
	<html lang="en-US" class="no-js IE8 IE">
		<![endif]-->
		<!--[if IE 9]> 
		<html lang="en-US" class="no-js IE9 IE">
			<![endif]-->
			<!--[if gt IE 9]><!-->
			<html lang="en-US" class="no-js">
				<!--<![endif]-->
				<head>
					<!-- title -->
					<title>Password ChangeOmni-Channel Customer Experience Management Suite | XpressoInsights</title>
					<% String forgotpasswordUrl = request.getContextPath() + "/services/user/forgotpassword"; %>
					<% String invalid = (String) request.getParameter("wrongpassword");%>
					<%String valid = " "; %>
					<!-- Main_meta_&_mobile_meta -->
					<meta charset="utf-8">
					<meta name="description" content="">
					<meta name="HandheldFriendly" content="true" />
					<meta name="apple-touch-fullscreen" content="yes" />
					<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
					<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
					<link rel="shortcut icon" href="assets/img/favicon.png" type="image/png"/>
					<!-- favicon -->
					<!-- favicon -->
					<link rel="stylesheet" type="text/css" href="assets/css/font-awesome.css">
					<!-- font awesome css -->
					<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
					<!-- bootstrap css -->
					<link rel="stylesheet" type="text/css" href="assets/css/bootstrap-theme.css">
					<!-- bootstrap supporting css -->
					<link rel="stylesheet" type="text/css" href="assets/css/style.css">
					<!-- common css -->
					<link rel="stylesheet" type="text/css" href="assets/css/style-forms.css">
					<!-- common css -->
					<!--[if lt IE 9]>
					<script type="text/javascript" src="assets/js/html5shiv.js"></script>
					<script type="text/javascript" src="assets/js/respond.min.js"></script>
					<![endif]-->
				</head>
				<body data-ng-app="app">
					<!--<section class="main-body-withoutmenu" style="height:100px;">
						<div class="col-md-12">
							<div class="logo-wrapper">
								<img src="assets/img/logo1.png" alt="RadianCX" class="login-logo"/>
								<h3 class="login-logotxt">Unified Customer Experience Management Suite</h3>
							</div>
						</div>
					</section>-->
					<section class="main-body-withoutmenu" data-ng-controller="forgotPasswordController as forgotForm">
						<div class="col-md-5 col-sm-10 loginform-wrapper">
							<div class="gradiant-line"></div>
							<div class="block-cover">
								<div class="logo-wrapper">
									<img src="assets/img/logo_new.png" alt="" class="login-logo" />
								</div>
								<div class="block-body" style="padding-bottom: 0;">
									<div class="row">
										<div class="col-md-12">
											<form name="forgotForm.form" ng-submit="forgotForm.onSubmit()" novalidate method="post" action =<%=forgotpasswordUrl%> id="forgotForm">
												<div class="row margin-bottom-0">
													<% if(invalid != null){ %>
													<div class="error-message"><% out.print(invalid);%></div>
													<% } %>
												</div>
												<formly-form model="forgotForm.model" fields="forgotForm.fields" options="forgotForm.options" form="forgotForm.form">
													<div class="form-group border-top row">
														<div class="col-sm-12">
															<button type="submit" class="btn btn-secondary" ng-disabled="forgotForm.form.$invalid">Submit</button>
														</div>
													</div>
												</formly-form>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
					<script type="text/ng-template" id="custom.html">
						<div class="form-group row">
						<div class="col-sm-offset-2 col-sm-8">
						<label for="{{::id}}">{{options.templateOptions.label}}</label>
						<input id="{{::id}}" name="{{::name}}" class="form-control" ng-model="model[options.key]" />
						</div>
						</div>
					</script>
					<script type="text/ng-template" id="error-messages.html">
						<formly-transclude></formly-transclude>
						<div class="my-messages" ng-if="options.validation.errorExistsAndShouldBeVisible" ng-messages="options.formControl.$error">
							<div class="form-group row">
								<div class="col-sm-offset-2 col-sm-8">
									<div ng-messages-include="validation.html"></div>
									<div class="message" ng-message="{{::name}}" ng-repeat="(name, message) in ::options.validation.messages">
										{{message(options.formControl.$viewValue, options.formControl.$modelValue, this)}}
									</div>
								</div>
							</div>
						</div>
					</script>
					<script type="text/ng-template" id="validation.html">
						<div class="message" ng-message="required">This field is required</div>
						<div class="message" ng-message="email">Invalid email address</div>
					</script>
					<!-- Angular js files -->  
					<script src="assets/js/api-check.js"></script><!-- apiCheck is used by formly to validate its api --> 
					<script src="assets/js/angular.min.js"></script> 
					<script src="assets/js/angular-animate.js"></script> 
					<script src="assets/js/ui-bootstrap-tpls-2.0.0.js"></script> 
					<script src="assets/js/angular-messages.js"></script> 
					<script src="assets/js/formly.js"></script><!-- This is the current state of master for formly core. --> 
					<script src="assets/js/angular-formly-templates-bootstrap.js"></script><!-- This is the current state of master for the bootstrap templates --> 
					<script type="text/javascript" src="assets/js/jquery.min.js"></script><!-- Main_lib_script --> 
					<script type="text/javascript" src="assets/js/bootstrap.js"></script><!-- bootstrap js --> 
					<script type="text/javascript" src="assets/js/bootstrap-select.js"></script>
					<!-- Custom js files -->
					<script type="text/javascript" src="assets/js/validator.js"></script> 
					<script type="text/javascript" src="assets/js/forgotPassword.js"></script>
				</body>
			</html>