package com.abzooba.trackR24.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.abzooba.trackR24.models.AccountCompanyMapBean;
import com.abzooba.trackR24.models.AccountMasterBean;
import com.abzooba.trackR24.models.CompanyMasterBean;
import com.abzooba.trackR24.models.CompanySourceMapBean;
import com.abzooba.trackR24.models.Competitor;
import com.abzooba.trackR24.models.CompetitorBean;
import com.abzooba.trackR24.models.Result;
import com.abzooba.trackR24.models.SourceInfo;
import com.abzooba.trackR24.models.SourceMasterBean;
import com.abzooba.trackR24.models.TopicProfile;
import com.abzooba.trackR24.models.User;
import com.abzooba.trackR24.models.UserInfo;
import com.abzooba.trackR24.models.UserInfoBean;
import com.abzooba.trackR24.models.UserMasterBean;
import com.abzooba.trackR24.util.CommonHandler;
import com.abzooba.trackR24.util.PropertyHandler;
import static com.abzooba.trackR24.util.Constants.*;

/**
* Author Name: Kunal Kashyap
* Create Date: 18-08-2016
* Last Modified: Kunal Kashyap: 16-09-2016
* Class Insight:  This class contains all the methods for calling the sql queries and procedures using hibernate.
*/
public class HibernateUserDao implements IUserDao {
	
	private static final Logger LOGGER = Logger.getLogger(HibernateKPIDao.class);

	@Override
	public Result getStaticResults(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		return null;
	}

	@Override
	public List<String> loginUser(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		return Collections.emptyList();
	}

	@Override
	public String getUpdatePassword(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		return null;
	}

	@Override
	public int registerUser(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		return 0;
	}

	@Override
	public int addUserInfo(UserInfoBean userInfoBean, String adminEmail) {
		Session session = null;
		int adminAccountId = 0;
		int adminCompanyId = 0;
		int adminCountryId = 0;
		int adminTimezoneId = 0;
		int sourceId = 0;
		int companyId = 0;
		int domainId = 0;
		String defaultPassword = PropertyHandler.getPropertyValue("defaultPassword");
		String defaultDesignation = PropertyHandler.getPropertyValue("defaultDesignation");
		String compLogo = PropertyHandler.getPropertyValue("LOGO_IMAGE");
		Transaction tx = null;
		List<UserMasterBean> userMasterBeanList = userInfoBean.getUserMasterBeanList();
		List<SourceMasterBean> sourceMasterBeanList = userInfoBean.getSourceMasterBeanList();
		List<CompetitorBean> competitorBeanList = userInfoBean.getCompetitorBeanList();
		try {
			session = GenericHibernateDao.getSession();
			tx = session.beginTransaction();
			Criteria cr = session.createCriteria(UserMasterBean.class);
			cr.add(Restrictions.eq("user_id", adminEmail));
			List<UserMasterBean> users = cr.list();
			for (Iterator<UserMasterBean> iterator = users.iterator(); iterator.hasNext();) {
				UserMasterBean user = iterator.next();

				adminAccountId = user.getAccount_id();
				adminCompanyId = user.getCompany_id();
				adminCountryId = user.getCountry_id();
				adminTimezoneId = user.getTimezone_id();
			}
			for (UserMasterBean userMasterBean : userMasterBeanList) {
				UserMasterBean usermasterBean = new UserMasterBean();
				usermasterBean.setUser_id(userMasterBean.getUser_id());
				usermasterBean.setUser_name(userMasterBean.getUser_name());
				usermasterBean.setAccount_id(adminAccountId);
				usermasterBean.setUser_password(defaultPassword);
				usermasterBean.setCompany_id(adminCompanyId);
				usermasterBean.setDesignation(defaultDesignation);
				usermasterBean.setCountry_id(adminCountryId);
				usermasterBean.setTimezone_id(adminTimezoneId);
				usermasterBean.setRole_id(1);
				usermasterBean.setIsConfirmed(0);
				usermasterBean.setCreate_datetime(new Date());
				usermasterBean.setUpdate_datetime(new Date());
				usermasterBean.setUser_token(CommonHandler.randomAlphaNumeric(50));
				usermasterBean.setCompany_logo_image_url(compLogo);
				session.persist(usermasterBean);
			}
			clearSession(session, tx);

			Criteria cr4 = session.createCriteria(CompanyMasterBean.class);
			cr4.add(Restrictions.eq(COMPANY_ID, adminCompanyId));
			List<CompanyMasterBean> companyMasterList = cr4.list();
			for (Iterator<CompanyMasterBean> iterator = companyMasterList.iterator(); iterator.hasNext();) {
				CompanyMasterBean companyMaster =  iterator.next();

				domainId = companyMaster.getDomain_id();

			}

			for (SourceMasterBean sourceMasterBean : sourceMasterBeanList) {
				Transaction tx1 = session.beginTransaction();
				SourceMasterBean sourcemasterBean = new SourceMasterBean();
				sourcemasterBean.setSource_type_id(sourceMasterBean.getSource_type_id());
				sourcemasterBean.setDomain_id(domainId);
				sourcemasterBean.setPage_name(sourceMasterBean.getSource_url());
				sourcemasterBean.setSource_url(sourceMasterBean.getSource_url());
				sourcemasterBean.setLive_source(0);
				sourcemasterBean.setXpresso_enable(0);
				sourcemasterBean.setLive_status(0);
				sourcemasterBean.setLast_streaming(new Date());
				sourcemasterBean.setLast_streaming_comment(new Date());
				sourcemasterBean.setCreate_datetime(new Date());
				sourcemasterBean.setUpdate_datetime(new Date());
				session.persist(sourcemasterBean);
				clearSession(session, tx1);
			}

			for (SourceMasterBean sourceMasterBean : sourceMasterBeanList) {

				Criteria cr1 = session.createCriteria(SourceMasterBean.class);
				cr1.add(Restrictions.eq("source_type_id", sourceMasterBean.getSource_type_id()));
				cr1.add(Restrictions.eq("source_url", sourceMasterBean.getSource_url()));
				List<SourceMasterBean> sourceMasterList = cr1.list();
				for (Iterator<SourceMasterBean> iterator = sourceMasterList.iterator(); iterator.hasNext();) {
					Transaction tx2 = session.beginTransaction();
					SourceMasterBean sourceMaster = iterator.next();
					sourceId = sourceMaster.getSource_id();

					CompanySourceMapBean companySourceMapBean = new CompanySourceMapBean();
					companySourceMapBean.setSource_id(sourceId);
					companySourceMapBean.setCompany_id(adminCompanyId);
					companySourceMapBean.setCreate_datetime(new Date());
					companySourceMapBean.setUpdate_datetime(new Date());
					session.persist(companySourceMapBean);

					clearSession(session, tx2);
				}
			}

			Transaction tx3 = null;
			for (CompetitorBean competitorBean : competitorBeanList) {
				tx3 = session.beginTransaction();
				CompanyMasterBean companyMasterBean = new CompanyMasterBean();
				companyMasterBean.setCompany_name(competitorBean.getCompany_name());
				companyMasterBean.setDomain_id(domainId);
				companyMasterBean.setCreate_datetime(new Date());
				companyMasterBean.setUpdate_datetime(new Date());
				session.persist(companyMasterBean);
				clearSession(session, tx3);
			}

			Transaction tx4 = null;
			for (CompetitorBean competitorBean : competitorBeanList) {
				tx4 = session.beginTransaction();
				Criteria cr2 = session.createCriteria(CompanyMasterBean.class);
				cr2.add(Restrictions.eq("company_name", competitorBean.getCompany_name()));
				cr2.add(Restrictions.eq("domain_id", domainId));
				List<CompanyMasterBean> companyList = cr2.list();
				for (Iterator<CompanyMasterBean> iterator = companyList.iterator(); iterator.hasNext();) {
					CompanyMasterBean companyMaster = iterator.next();
					companyId = companyMaster.getCompany_id();
				}
				AccountCompanyMapBean accountCompanyMapBean = new AccountCompanyMapBean();
				accountCompanyMapBean.setAccount_id(adminAccountId);
				accountCompanyMapBean.setCompany_id(companyId);
				accountCompanyMapBean.setPrimary_company(0);
				accountCompanyMapBean.setCreate_datetime(new Date());
				accountCompanyMapBean.setUpdate_datetime(new Date());
				session.persist(accountCompanyMapBean);
				clearSession(session, tx4);

				List<SourceInfo> competitorSourcesList = competitorBean.getCompetitorSources();
				for (SourceInfo competitorSource : competitorSourcesList) {
					Transaction tx5 = session.beginTransaction();
					SourceMasterBean sourceMasterBean = new SourceMasterBean();
					sourceMasterBean.setSource_type_id(competitorSource.getSourceTypeId());
					sourceMasterBean.setDomain_id(domainId);
					sourceMasterBean.setPage_name(competitorSource.getSourceURL());
					sourceMasterBean.setSource_url(competitorSource.getSourceURL());
					sourceMasterBean.setLive_source(0);
					sourceMasterBean.setXpresso_enable(0);
					sourceMasterBean.setLive_status(0);
					sourceMasterBean.setLast_streaming(new Date());
					sourceMasterBean.setLast_streaming_comment(new Date());
					sourceMasterBean.setCreate_datetime(new Date());
					sourceMasterBean.setUpdate_datetime(new Date());
					session.persist(sourceMasterBean);

					clearSession(session, tx5);
				}

				for (SourceInfo competitorSource : competitorSourcesList) {

					Criteria cr3 = session.createCriteria(SourceMasterBean.class);
					cr3.add(Restrictions.eq("source_type_id", competitorSource.getSourceTypeId()));
					cr3.add(Restrictions.eq("source_url", competitorSource.getSourceURL()));
					List<SourceMasterBean> sourceMasterList = cr3.list();
					for (Iterator<SourceMasterBean> iterator = sourceMasterList.iterator(); iterator.hasNext();) {
						Transaction tx6 = session.beginTransaction();
						SourceMasterBean sourceMaster =  iterator.next();
						sourceId = sourceMaster.getSource_id();

						CompanySourceMapBean companySourceMapBean = new CompanySourceMapBean();
						companySourceMapBean.setSource_id(sourceId);
						companySourceMapBean.setCompany_id(companyId);
						companySourceMapBean.setCreate_datetime(new Date());
						companySourceMapBean.setUpdate_datetime(new Date());
						session.persist(companySourceMapBean);
						clearSession(session, tx6);
					}

				}

			}
			Transaction tx7 = session.beginTransaction();
			AccountMasterBean accountMasterBean = (AccountMasterBean) session.get(AccountMasterBean.class, adminAccountId);
			accountMasterBean.setIsConfigured(1);
			session.update(accountMasterBean);
			clearSession(session, tx7);

		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		} finally {
			session.flush();
			session.close();
		}

		return 1;

	}

	private void clearSession(Session session, Transaction tx) {
		session.flush();
		tx.commit();
		session.clear();
	}

	@SuppressWarnings("unchecked")
	@Override
	public UserInfo fetchUserInfo(String adminEmail) {
		Session session = null;
		Transaction tx = null;
		int companyId = 0;
		int accountId = 0;
		UserInfo userInfo = new UserInfo();

		try {
			session = GenericHibernateDao.getSession();
			tx = session.beginTransaction();
			Criteria cr = session.createCriteria(UserMasterBean.class);
			cr.add(Restrictions.eq("user_id", adminEmail));
			List<UserMasterBean> users = cr.list();
			for (Iterator<UserMasterBean> iterator = users.iterator(); iterator.hasNext();) {
				UserMasterBean user = iterator.next();
				companyId = user.getCompany_id();
				accountId = user.getAccount_id();
			}

			Criteria cr1 = session.createCriteria(CompanySourceMapBean.class);
			cr1.add(Restrictions.eq(COMPANY_ID, companyId));
			List<CompanySourceMapBean> companySourceMapBeanList = cr1.list();
			List<Integer> sourceIdList = new ArrayList<>();
			for (Iterator<CompanySourceMapBean> iterator = companySourceMapBeanList.iterator(); iterator.hasNext();) {
				CompanySourceMapBean companySourceMapBean = iterator.next();
				sourceIdList.add(companySourceMapBean.getSource_id());
			}
			tx.commit();

			List<String> sourceUrlList = new ArrayList<>();
			List<SourceInfo> sourcesDetailsList = new ArrayList<>();
			for (int i = 0; i < sourceIdList.size(); i++) {
				if(i >1){
					break;
				}
				Criteria cr2 = session.createCriteria(SourceMasterBean.class);
				cr2.add(Restrictions.eq("source_id", sourceIdList.get(i)));
				List<SourceMasterBean> sourceMasterBeanList = cr2.list();
				for (Iterator<SourceMasterBean> iterator = sourceMasterBeanList.iterator(); iterator.hasNext();) {
					SourceMasterBean sourceMasterBean = iterator.next();
					SourceInfo sourceInfo = new SourceInfo();
					sourceInfo.setSourceTypeId(sourceMasterBean.getSource_type_id());
					//Since in source_url data is not readable from user's view so mapping with page_name
					sourceInfo.setSourceURL(sourceMasterBean.getPage_name());
					sourceInfo.setSourceId(sourceMasterBean.getSource_id());
					if (sourceMasterBean.getSource_type_id() == 1)
						sourceInfo.setName("Facebook");
					else if (sourceMasterBean.getSource_type_id() == 2)
						sourceInfo.setName("Twitter");
					else if (sourceMasterBean.getSource_type_id() == 3)
						sourceInfo.setName("Economic Times");
					sourcesDetailsList.add(sourceInfo);
				}

			}
			userInfo.setSourcesDetailsList(sourcesDetailsList);

			List<User> userList = new ArrayList<>();
			Criteria cr2 = session.createCriteria(UserMasterBean.class);
			cr2.add(Restrictions.eq(COMPANY_ID, companyId));
			cr2.add(Restrictions.eq("role_id", 1));
			List<UserMasterBean> usersList = cr2.list();
			for (Iterator<UserMasterBean> iterator = usersList.iterator(); iterator.hasNext();) {
				UserMasterBean userMasterBean = iterator.next();

				User user = new User();
				user.setUserName(userMasterBean.getUser_name());
				user.setUserEmail(userMasterBean.getUser_id());
				userList.add(user);
			}
			userInfo.setUsersList(userList);

			Criteria cr3 = session.createCriteria(AccountCompanyMapBean.class);
			List<Integer> companyIdList = new ArrayList<>();
			cr3.add(Restrictions.eq("account_id", accountId));
			cr3.add(Restrictions.eq("primary_company", 0));
			List<AccountCompanyMapBean> accountCompanyMapList = cr3.list();
			for (Iterator<AccountCompanyMapBean> iterator = accountCompanyMapList.iterator(); iterator.hasNext();) {
				AccountCompanyMapBean accountCompanyMapBean =  iterator.next();
				companyIdList.add(accountCompanyMapBean.getCompany_id());
			}


			List<Competitor> competitorsList = new ArrayList<>();

			for (int i = 0; i < companyIdList.size(); i++) {
				Competitor competitor = new Competitor();

				Criteria cr4 = session.createCriteria(CompanyMasterBean.class);
				cr4.add(Restrictions.eq(COMPANY_ID, companyIdList.get(i)));
				List<CompanyMasterBean> companyMasterBeanList = cr4.list();
				for (Iterator<CompanyMasterBean> iterator = companyMasterBeanList.iterator(); iterator.hasNext();) {
					CompanyMasterBean companyMasterBean =  iterator.next();
					competitor.setCompetitorName(companyMasterBean.getCompany_name());
				}

				List<Integer> companySourceIds = new ArrayList<>();
				Criteria cr5 = session.createCriteria(CompanySourceMapBean.class);
				cr5.add(Restrictions.eq(COMPANY_ID, companyIdList.get(i)));
				List<CompanySourceMapBean> lCompanySourceMapBean = cr5.list();
				for (Iterator<CompanySourceMapBean> iterator = lCompanySourceMapBean.iterator(); iterator.hasNext();) {
					CompanySourceMapBean companySourceMapBean =  iterator.next();
					companySourceIds.add(companySourceMapBean.getSource_id());
				}

				List<SourceInfo> compSourceInfoList = new ArrayList<>();
				for (int j = 0; j < companySourceIds.size(); j++) {
					Criteria cr6 = session.createCriteria(SourceMasterBean.class);
					cr6.add(Restrictions.eq("source_id", companySourceIds.get(j)));
					List<SourceMasterBean> sourceMasterBeanList = cr6.list();
					for (Iterator<SourceMasterBean> iterator = sourceMasterBeanList.iterator(); iterator.hasNext();) {
						SourceMasterBean sourceMasterBean =  iterator.next();

						SourceInfo sourceInfo = new SourceInfo();
						sourceInfo.setSourceId(sourceMasterBean.getSource_id());
						sourceInfo.setSourceTypeId(sourceMasterBean.getSource_type_id());
						if (sourceMasterBean.getSource_type_id() == 1)
							sourceInfo.setName("Facebook");
						else if (sourceMasterBean.getSource_type_id() == 2)
							sourceInfo.setName("Twitter");
						else if (sourceMasterBean.getSource_type_id() == 3)
							sourceInfo.setName("Economic Times");

						sourceInfo.setSourceURL(sourceMasterBean.getSource_url());
						compSourceInfoList.add(sourceInfo);
					}
				}
				competitor.setCompetitorSources(compSourceInfoList);
				competitorsList.add(competitor);
			}

			userInfo.setCompetitorsList(competitorsList);

			List<TopicProfile> topicProfileList = new ArrayList<>();

			TopicProfile socialtopicProfile = new TopicProfile();
			socialtopicProfile.setProfileType("Social Profile");
			List<Integer> socialProfileDetailsList = new ArrayList<>();
			socialProfileDetailsList.add(1);
			socialProfileDetailsList.add(2);
			socialtopicProfile.setProfileDetails(socialProfileDetailsList);
			topicProfileList.add(socialtopicProfile);

			TopicProfile mainstreamtopicProfile = new TopicProfile();
			mainstreamtopicProfile.setProfileType("Mainstream Media");
			List<Integer> mainstreamProfileDetailsList = new ArrayList<>();
			mainstreamProfileDetailsList.add(3);
			mainstreamtopicProfile.setProfileDetails(mainstreamProfileDetailsList);
			topicProfileList.add(mainstreamtopicProfile);

			TopicProfile othertopicProfile = new TopicProfile();
			othertopicProfile.setProfileType("Other Relevant Source");
			List<Integer> otherProfileDetailsList = new ArrayList<>();
			otherProfileDetailsList.add(9);
			otherProfileDetailsList.add(10);
			otherProfileDetailsList.add(11);
			otherProfileDetailsList.add(12);
			othertopicProfile.setProfileDetails(otherProfileDetailsList);
			topicProfileList.add(othertopicProfile);

			userInfo.setTopicProfiles(topicProfileList);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}finally {
			session.flush();
			session.close();
		}

		return userInfo;
	}
}
