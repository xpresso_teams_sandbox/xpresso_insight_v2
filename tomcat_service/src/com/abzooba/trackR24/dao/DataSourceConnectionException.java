package com.abzooba.trackR24.dao;

@SuppressWarnings("serial")
public class DataSourceConnectionException extends DataSourceException {

	public DataSourceConnectionException(String message) {
		super(message);
	}
}
