package com.abzooba.trackR24.dao;

import java.util.Map;

import com.abzooba.trackR24.models.KPIParentBean;

/**
* Author Name: Kunal Kashyap
* Create Date: 18-10-2016
* Last Modified: Kunal Kashyap: 14-11-2016
* Interface Insight:  This interface contains all the method declaration of methods of HibernateKPIDao for KPI page.
*/
public interface IKPIDao {

	public KPIParentBean getFacebookKPI(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters);

	public KPIParentBean getTwitterKPI(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters);

	public KPIParentBean getFollowerFanGrowthRate(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters);

	public KPIParentBean getTopEngagingTweets(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters);
	
	public KPIParentBean getTopEngagingPosts(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters);
	
	public KPIParentBean getTopEngagingTweetsPostsDistribution(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters);
	
	public KPIParentBean gePerTweetPostInteraction(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters);
	
	public KPIParentBean getTopEngagingTweetsPostsDistributionMonthly(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters);
	
	public KPIParentBean getTopEngagingTweetsPostsDistributionDaily(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters);
}
