package com.abzooba.trackR24.dao;

import java.util.Map;

import org.apache.log4j.Logger;

import com.abzooba.trackR24.models.Result;

public class GenericJdbcDao {

	private static final Logger LOGGER = Logger.getLogger(GenericJdbcDao.class);
	protected DatabaseConnectionManager connManager = new DatabaseConnectionManager();
	Result result = null;

	public GenericJdbcDao() {
		super();
		connManager.connect();
	}

	public Result getProcedureResultSet(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		LOGGER.info(">>>>>>>GenericJdbcDao ::: Entering getProcedureResultSet>>>>>>>>" + dynamicParameters.get("inCompanyId"));
		try {
			result = connManager.getResults(staticParameters, dynamicParameters);
		} catch (Exception e) {
			LOGGER.error("Error occurred in GenericJdbcDao.getProcedureResultSet");
			LOGGER.error(e.getMessage());
			LOGGER.error(e.getStackTrace());
		} finally {
			connManager.disconnect();
		}
		LOGGER.info(">>>>>>>GenericJdbcDao ::: Exiting getProcedureResultSet>>>>>>>>");
		return result;
	}

	public Result getProcedureResultSetForNew(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		LOGGER.info(">>>>>>>GenericJdbcDao ::: Entering getProcedureResultSet>>>>>>>>");
		try {
			result = connManager.getResultsForNew(staticParameters, dynamicParameters);
		} catch (Exception e) {
			LOGGER.error("Error occurred in GenericJdbcDao.getProcedureResultSet");
			LOGGER.error(e.getMessage());
			LOGGER.error(e.getStackTrace());
		} finally {
			connManager.disconnect();
		}
		LOGGER.info(">>>>>>>GenericJdbcDao ::: Exiting getProcedureResultSet>>>>>>>>");
		return result;
	}
}
