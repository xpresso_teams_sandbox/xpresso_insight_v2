package com.abzooba.trackR24.dao;

import java.util.Map;

import com.abzooba.trackR24.models.Result;

public interface IBrandDao {

	public Result fetchSummaryMatrics(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException;
	public Result fetchCompetitiveComparison(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException;
	public Result fetchSocialPostTweet(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters,String callingMethodFrom) throws DataSourceException;
	public Result fetchWordCloudDataArray(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException;
}
