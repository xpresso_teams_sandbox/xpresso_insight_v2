package com.abzooba.trackR24.dao;

@SuppressWarnings("serial")
public class DataSourceException extends Exception {

	public DataSourceException(String message) {
		super(message);
	}

}
