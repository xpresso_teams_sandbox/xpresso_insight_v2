package com.abzooba.trackR24.dao;

import java.util.Map;

import com.abzooba.trackR24.models.Result;

public interface IDashboardDao {

	public Result fetchHappinessIndexLive(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException;

	public Result fetchCompetitiveHappinessIndex(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException;

	public Result fetchContextSentimentDetails(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException;

	public Result fetchContextwiseSentimentDistribution(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException;

	public Result fetchDailySnapshot(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException;
	
	public Result fetchDailySnapshotNew(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException;

}
