package com.abzooba.trackR24.dao;

import static com.abzooba.trackR24.util.Constants.ATTRIBUTE_SP_NAME;
import static com.abzooba.trackR24.util.Constants.ATTRIBUTE_SQL;
import static com.abzooba.trackR24.util.Constants.CALL;
import static com.abzooba.trackR24.util.Constants.ERROR_LOG;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_ACCOUNT_ID;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_ASPECT;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_ASPECT_NAME;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_CURR_TIME;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_FROM_DT;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_INTERVAL;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_SELECTED_DURATION;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_SENTIMENT_TYPE;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_SOURCE;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_SOURCE_ID;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_TOPIC;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_TO_DT;
import static com.abzooba.trackR24.util.Constants.SQL_LOG;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.abzooba.trackR24.models.DBResult;
import com.abzooba.trackR24.models.Result;
import com.abzooba.trackR24.util.PropertyHandler;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JdbcDashboardDao extends GenericJdbcDao implements IDashboardDao {

	private static final Logger LOGGER = Logger.getLogger(JdbcDashboardDao.class);
	PreparedStatement pst = null;
	ResultSet results;

	@Override
	public Result fetchHappinessIndexLive(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		return getDashboardKPIs(staticParameters, dynamicParameters);
	}
	
	public Result fetchOverallSentimentDistribution(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		return super.getProcedureResultSet(staticParameters, dynamicParameters);
	}

	public Result fetchFilteredOverallSentimentDistribution(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		return getFilteredOverallSentiment(staticParameters, dynamicParameters);
	}
	
	public Result fetchFilteredTopAdvocates(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceConnectionException {
		LOGGER.info(">>>>>>>JdbcDashboardDao ::: Entering fetchTopAdvocates>>>>>>>>");
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		callStmt = callStmt.append("{").append(CALL).append(spName).append("(?,?,STR_TO_DATE(?,'%d-%m-%Y %H:%i'),STR_TO_DATE(?,'%d-%m-%Y %H:%i'))}");
			try( CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
				
			stmt.setInt(1, (Integer) dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID));
			if (dynamicParameters.get(SP_IN_PARAM_SOURCE) == null) {
				stmt.setNull(2, Types.INTEGER);
			} else {
				stmt.setInt(2, (Integer) dynamicParameters.get(SP_IN_PARAM_SOURCE));
			}
			if (dynamicParameters.get(SP_IN_PARAM_FROM_DT) == null) {
				stmt.setNull(3, Types.VARCHAR);
			} else {
				stmt.setString(3, (String) dynamicParameters.get(SP_IN_PARAM_FROM_DT));
			}
			if (dynamicParameters.get(SP_IN_PARAM_TO_DT) == null) {
				stmt.setNull(4, Types.VARCHAR);
			} else {
				stmt.setString(4, (String) dynamicParameters.get(SP_IN_PARAM_TO_DT));
			}

			try(ResultSet resultSet = stmt.executeQuery();){
			LOGGER.debug(">>>>> >>>>>>>JdbcDashboardDao ::: CallableStatement >>> fetchTopAdvocates >>>>> " + stmt);
			ResultSetMetaData metaData = resultSet.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			for (i = 1; i <= numCols; i++) {
				result.addColumnName(metaData.getColumnLabel(i));
			}
			ArrayList<Object> row;
			while (resultSet.next()) {
				row = new ArrayList<>();
				for (i = 1; i <= numCols; i++) {
					row.add(resultSet.getObject(i));
				}
				result.addRow(row);
			}
			}
			LOGGER.info(">>>>>>>JdbcDashboardDao ::: fetchTopAdvocates>>>>>>>> DBResult result: " + result.toJSON());
			LOGGER.info(">>>>>>>JdbcDashboardDao ::: Exiting fetchTopAdvocates>>>>>>>>");
		} catch (SQLException se) {
			LOGGER.error(se.getMessage());
			LOGGER.error(se);
			LOGGER.error(se.getStackTrace());
		} finally {
			connManager.disconnect();
		}
		return result;
	}

	
	private Result getFilteredOverallSentiment(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceConnectionException {
		LOGGER.info(">>>>>>>JdbcDashboardDao ::: Entering getFilteredOverallSentimentResultSet>>>>>>>>");
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		
		callStmt = callStmt.append("{").append(CALL).append(spName).append("(?,?,?,?,STR_TO_DATE(?,'%d-%m-%Y %H:%i'),STR_TO_DATE(?,'%d-%m-%Y %H:%i'),STR_TO_DATE(?,'%d-%m-%Y %H:%i'),STR_TO_DATE(?,'%d-%m-%Y %H:%i'))}");
			try( CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){

			stmt.setInt(1, (Integer) dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID));
			if (dynamicParameters.get(SP_IN_PARAM_SOURCE) == null) {
				stmt.setNull(2, Types.INTEGER);
			} else {
				stmt.setInt(2, (Integer) dynamicParameters.get(SP_IN_PARAM_SOURCE));
			}
			
			if (dynamicParameters.get(SP_IN_PARAM_TOPIC) == null) {
				stmt.setNull(3, Types.VARCHAR);
			} else {
				stmt.setString(3, (String) dynamicParameters.get(SP_IN_PARAM_TOPIC));
			}
			if (dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_CITY")) == null) {
				stmt.setNull(4, Types.VARCHAR);
			} else {
				stmt.setString(4, (String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_CITY")));
			}
			if (dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_FROM_DT_CURRENT")) == null) {
				stmt.setNull(5, Types.VARCHAR);
			} else {
				stmt.setString(5, (String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_FROM_DT_CURRENT")));
			}
			if (dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_TO_DT_CURRENT")) == null) {
				stmt.setNull(6, Types.VARCHAR);
			} else {
				stmt.setString(6, (String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_TO_DT_CURRENT")));
			}
			if (dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_FROM_DT_PREVIOUS")) == null) {
				stmt.setNull(7, Types.VARCHAR);
			} else {
				stmt.setString(7, (String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_FROM_DT_PREVIOUS")));
			}
			if (dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_TO_DT_PREVIOUS")) == null) {
				stmt.setNull(8, Types.VARCHAR);
			} else {
				stmt.setString(8, (String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_TO_DT_PREVIOUS")));
			}

			LOGGER.info(">>>>> >>>>>>>JdbcDashboardDao ::: CallableStatement >>> getFilteredOverallSentimentResultSet >>>>> " + stmt);
			try(ResultSet resultSet = stmt.executeQuery();){
			ResultSetMetaData metaData = resultSet.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			for (i = 1; i <= numCols; i++) {
				result.addColumnName(metaData.getColumnLabel(i));
			}
			ArrayList<Object> row;
			while (resultSet.next()) {
				row = new ArrayList<>();
				for (i = 1; i <= numCols; i++) {
					row.add(resultSet.getObject(i));
				}
				result.addRow(row);
			}
			}
			LOGGER.info(">>>>>>>JdbcDashboardDao ::: getFilteredOverallSentimentResultSet>>>>>>>> DBResult result: " + result.toJSON());
			LOGGER.info(">>>>>>>JdbcDashboardDao ::: Exiting getFilteredOverallSentimentResultSet>>>>>>>>");
		} catch (SQLException se) {
			LOGGER.error(ERROR_LOG + spName  );
			LOGGER.error(se);
			LOGGER.error(se.getStackTrace());
		} finally {
			connManager.disconnect();
		}
		return result;
	}


	@Override
	public Result fetchCompetitiveHappinessIndex(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		return super.getProcedureResultSet(staticParameters, dynamicParameters);
	}
	public Result fetchTopAdvocates(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		return super.getProcedureResultSet(staticParameters, dynamicParameters);
	}
	@Override
	public Result fetchContextSentimentDetails(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		return getDashboardKPIs(staticParameters, dynamicParameters);
	}

	@Override
	public Result fetchContextwiseSentimentDistribution(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		return super.getProcedureResultSet(staticParameters, dynamicParameters);
	}

	@Override
	public Result fetchDailySnapshot(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		return super.getProcedureResultSet(staticParameters, dynamicParameters);
	}

	
	
	
	private Result getDashboardKPIs(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {

		LOGGER.info(">>>>>>>JdbcDashboardDao ::: Entering getDashboardKPIs>>>>>>>>");
		DBResult result = new DBResult();
		//CallableStatement stmt;
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		try {
			if (PropertyHandler.getPropertyValue("SP_BRAND_COMPARISON_24HR").equalsIgnoreCase(spName)) {
				LOGGER.info(">>>>> Inside IF Block >>>> IF : SP_BRAND_COMPARISON_24HR  >>>>>>");
				callStmt = callStmt.append("{").append(CALL).append(spName).append("(?,STR_TO_DATE(?,'%d-%m-%Y %H:%i'),?)}");
				
				try( CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
					stmt.setInt(1, (Integer) dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID));
					stmt.setString(2, (String) dynamicParameters.get(SP_IN_PARAM_CURR_TIME));
					stmt.setInt(3, (Integer) dynamicParameters.get(SP_IN_PARAM_SELECTED_DURATION));
					
					LOGGER.debug(">>>>> >>>>>>>JdbcDashboardDao ::: CallableStatement >>> getDashboardKPIs >>>>> " + stmt);
					try(ResultSet resultset = stmt.executeQuery();){
					ResultSetMetaData metaData = resultset.getMetaData();
					int i;
					int numCols = metaData.getColumnCount();
					for (i = 1; i <= numCols; i++) {
						result.addColumnName(metaData.getColumnLabel(i));
					}
					ArrayList<Object> row;
					int rowCounter = 0;
					while (resultset.next()) {
						row = new ArrayList<>();
						if (!PropertyHandler.getPropertyValue("SP_BRAND_COMPARISON_24HR").equalsIgnoreCase(spName) && rowCounter == 0) {
							result.setCount(Integer.parseInt(resultset.getObject(numCols).toString()));
						}
						rowCounter++;
						for (i = 1; i <= numCols; i++) {
							row.add(resultset.getObject(i));
						}
						result.addRow(row);
					}
					}
					LOGGER.info(">>>>>>>JdbcDashboardDao ::: getDashboardKPIs>>>>>>>> DBResult result: " + result.toJSON());
					LOGGER.info(">>>>>>>JdbcDashboardDao ::: Exiting getDashboardKPIs>>>>>>>>");
				}
				/*stmt = connManager.connection.prepareCall(callStmt.toString());
				stmt.setInt(1, (Integer) dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID));
				stmt.setString(2, (String) dynamicParameters.get(SP_IN_PARAM_CURR_TIME));
				stmt.setInt(3, (Integer) dynamicParameters.get(SP_IN_PARAM_SELECTED_DURATION));*/
			} else {
				LOGGER.info(">>>>> Inside ELSE Block >>>> IF Not :  SP_BRAND_COMPARISON_24HR  >>>>>>");
				callStmt = callStmt.append("{").append(CALL).append(spName).append("(?,?,?,STR_TO_DATE(?,'%d-%m-%Y %H:%i'), STR_TO_DATE(?,'%d-%m-%Y %H:%i'),?,?)}");
				//stmt = connManager.connection.prepareCall(callStmt.toString());
				try( CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
					if (dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_OFFSET")) == null) {
						stmt.setNull(1, Types.INTEGER);
					} else {
						stmt.setInt(1, (Integer) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_OFFSET")));
					}
					if (dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_LIMIT")) == null) {
						stmt.setNull(2, Types.INTEGER);
					} else {
						stmt.setInt(2, (Integer) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_LIMIT")));
					}
					stmt.setInt(3, (Integer) dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID));
					if (dynamicParameters.get(SP_IN_PARAM_FROM_DT) == null) {
						stmt.setNull(4, Types.VARCHAR);
					} else {
						stmt.setString(4, (String) dynamicParameters.get(SP_IN_PARAM_FROM_DT));
					}
					if (dynamicParameters.get(SP_IN_PARAM_TO_DT) == null) {
						stmt.setNull(5, Types.VARCHAR);
					} else {
						stmt.setString(5, (String) dynamicParameters.get(SP_IN_PARAM_TO_DT));
					}
					if (dynamicParameters.get(SP_IN_PARAM_INTERVAL) == null) {
						stmt.setNull(6, Types.INTEGER);
					} else {
						stmt.setInt(6, (Integer) dynamicParameters.get(SP_IN_PARAM_INTERVAL));
					}
					if (dynamicParameters.get(SP_IN_PARAM_SELECTED_DURATION) == null) {
						stmt.setNull(7, Types.INTEGER);
					} else {
						stmt.setInt(7, (Integer) dynamicParameters.get(SP_IN_PARAM_SELECTED_DURATION));
					}
					
					LOGGER.debug(">>>>> >>>>>>>JdbcDashboardDao ::: CallableStatement >>> getDashboardKPIs >>>>> " + stmt);
					try(ResultSet resultset = stmt.executeQuery();){
					ResultSetMetaData metaData = resultset.getMetaData();
					int i;
					int numCols = metaData.getColumnCount();
					for (i = 1; i <= numCols; i++) {
						result.addColumnName(metaData.getColumnLabel(i));
					}
					ArrayList<Object> row;
					int rowCounter = 0;
					while (resultset.next()) {
						row = new ArrayList<>();
						if (!PropertyHandler.getPropertyValue("SP_BRAND_COMPARISON_24HR").equalsIgnoreCase(spName) && rowCounter == 0) {
							result.setCount(Integer.parseInt(resultset.getObject(numCols).toString()));
						}
						rowCounter++;
						for (i = 1; i <= numCols; i++) {
							row.add(resultset.getObject(i));
						}
						result.addRow(row);
					}
					}
					LOGGER.info(">>>>>>>JdbcDashboardDao ::: getDashboardKPIs>>>>>>>> DBResult result: " + result.toJSON());
					LOGGER.info(">>>>>>>JdbcDashboardDao ::: Exiting getDashboardKPIs>>>>>>>>");
				}
			}

			
		} catch (SQLException se) {
			LOGGER.error(ERROR_LOG + spName);
			LOGGER.error(se);
			LOGGER.error(se.getStackTrace());
		} finally {
		
			connManager.disconnect();
		}
		return result;

	}

//=======================================================================================================================================
	@Override
	public Result fetchDailySnapshotNew(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		return super.getProcedureResultSetForNew(staticParameters, dynamicParameters);
	}

	public List<Map<String, Object>> getSnapshotsMetaInfo(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		String sql = (String) staticParameters.get(ATTRIBUTE_SQL);
		List<Map<String, Object>> snapshotMetaInfoList =  new ArrayList<>();
		Map<String, Object> eachSnapshotInfoMap = null;
		try {
			pst = connManager.connection.prepareStatement(staticParameters.get(ATTRIBUTE_SQL).toString());

			if (dynamicParameters != null) {
				pst.setInt(1, (Integer) dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID));
			}
			LOGGER.debug(SQL_LOG + sql);
			LOGGER.debug(SQL_LOG + pst);
			LOGGER.debug("Account ID :::::: " + (Integer) dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID));
			results = pst.executeQuery();
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			while (results.next()) {
				eachSnapshotInfoMap = new LinkedHashMap<>();
				for (i = 1; i <= numCols; i++) {
					eachSnapshotInfoMap.put(metaData.getColumnLabel(i), results.getObject(i));
				}
				snapshotMetaInfoList.add(eachSnapshotInfoMap);
			}
			return snapshotMetaInfoList;
		} catch (Exception exc) {
			LOGGER.error(ERROR_LOG + sql);
			LOGGER.error(exc.getMessage());
			LOGGER.error(exc.getStackTrace());
			throw new DataSourceException(exc.getMessage());
		} finally {
			try {
				results.close();
				pst.close();
			} catch (SQLException e) {
				LOGGER.error(e.getMessage());
			}

		}
	}
	
	public List<Map<String, Object>> getSnapshotsSnapshotSentimentsChartdata(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		String sql = (String) staticParameters.get(PropertyHandler.getPropertyValue("ATTRIBUTE_SQL"));
		List<Map<String, Object>> snapshotMetaInfoList =  new ArrayList<>();
		Map<String, Object> eachSnapshotInfoMap = null;
		try {
			pst = connManager.connection.prepareStatement(staticParameters.get(PropertyHandler.getPropertyValue("ATTRIBUTE_SQL")).toString());

			if (dynamicParameters != null) {
				pst.setInt(1, (Integer) dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID));
			}
			LOGGER.debug(SQL_LOG + sql);
			LOGGER.debug(SQL_LOG + pst);
			results = pst.executeQuery();
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			while (results.next()) {
				eachSnapshotInfoMap = new LinkedHashMap<>();
				for (i = 1; i <= numCols; i++) {
					eachSnapshotInfoMap.put(metaData.getColumnLabel(i), results.getObject(i));
				}
				snapshotMetaInfoList.add(eachSnapshotInfoMap);
			}
			return snapshotMetaInfoList;
		} catch (Exception exc) {
			LOGGER.error(ERROR_LOG + sql);
			LOGGER.error(exc.getMessage());
			LOGGER.error(exc.getStackTrace());
			throw new DataSourceException(exc.getMessage());
		} finally {
			try {
				results.close();
				pst.close();
			} catch (SQLException e) {
				LOGGER.error(e.getMessage());
			}

		}
	}

	public Result fetchFilteredOverallSentimentDistributionForDashboard(Map<String, Object> staticParameters,
			Map<String, Object> dynamicParameters) throws DataSourceConnectionException {

		return getFilteredOverallSentimentResultSetForDashboard(staticParameters, dynamicParameters);
	}
	

	public Result fetchAspectWiseDistributionForForDashboard(Map<String, Object> staticParameters,
			Map<String, Object> dynamicParameters) throws DataSourceConnectionException {

		return fetchAspectWiseDistributionForForDashboardData(staticParameters, dynamicParameters);
	}
	
	public Result fetchSourceAspectWiseSentimentDistribution(Map<String, Object> staticParameters,
			Map<String, Object> dynamicParameters) throws DataSourceConnectionException {

		return fetchSourceAspectWiseSentimentDistributionData(staticParameters, dynamicParameters);
	}
	public Result fetchSourceAspectWiseSentimentTopEntity(Map<String, Object> staticParameters,
			Map<String, Object> dynamicParameters) throws DataSourceConnectionException {

		return fetchSourceAspectWiseSentimentTopEntityData(staticParameters, dynamicParameters);
	}
	public Result fetchSourceAspectWiseSentimentTopFeedback(Map<String, Object> staticParameters,
			Map<String, Object> dynamicParameters) throws DataSourceConnectionException, IOException, ParseException {

		return fetchSourceAspectWiseSentimentTopFeedbackData(staticParameters, dynamicParameters);
	}

	private Result getFilteredOverallSentimentResultSetForDashboard(Map<String, Object> staticParameters,
			Map<String, Object> dynamicParameters) throws DataSourceConnectionException {


		LOGGER.info(">>>>>>>JdbcDashboardDao ::: Entering getFilteredOverallSentimentResultSet>>>>>>>>");
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		
		callStmt = callStmt.append("{").append(CALL).append(spName).append("(?,?,?,STR_TO_DATE(?,'%d-%m-%Y %H:%i'),STR_TO_DATE(?,'%d-%m-%Y %H:%i'))}");
			try( CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){

			stmt.setInt(1, (Integer) dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID));
			if (dynamicParameters.get(SP_IN_PARAM_SOURCE) == null) {
				stmt.setNull(2, Types.VARCHAR);
			} else {
				stmt.setString(2, (String) dynamicParameters.get(SP_IN_PARAM_SOURCE));
			}
			if (dynamicParameters.get(SP_IN_PARAM_TOPIC) == null) {
				stmt.setNull(3, Types.VARCHAR);
			} else {
				stmt.setString(3, (String) dynamicParameters.get(SP_IN_PARAM_TOPIC));
			}
			if (dynamicParameters.get(SP_IN_PARAM_TOPIC) == null) {
				stmt.setNull(4, Types.VARCHAR);
			} else {
				stmt.setString(4, (String) dynamicParameters.get(SP_IN_PARAM_FROM_DT));
			}
			if (dynamicParameters.get(SP_IN_PARAM_TO_DT) == null) {
				stmt.setNull(5, Types.VARCHAR);
			} else {
				stmt.setString(5, (String) dynamicParameters.get(SP_IN_PARAM_TO_DT));
			}
	

			LOGGER.info(">>>>> >>>>>>>JdbcDashboardDao ::: CallableStatement >>> getFilteredOverallSentimentResultSet >>>>> " + stmt);
			try(ResultSet resultSet = stmt.executeQuery();){
			ResultSetMetaData metaData = resultSet.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			for (i = 1; i <= numCols; i++) {
				result.addColumnName(metaData.getColumnLabel(i));
			}
			ArrayList<Object> row;
			while (resultSet.next()) {
				row = new ArrayList<>();
				for (i = 1; i <= numCols; i++) {
					row.add(resultSet.getObject(i));
				}
				result.addRow(row);
			}
			}
			LOGGER.info(">>>>>>>JdbcDashboardDao ::: getFilteredOverallSentimentResultSet>>>>>>>> DBResult result: " + result.toJSON());
			LOGGER.info(">>>>>>>JdbcDashboardDao ::: Exiting getFilteredOverallSentimentResultSet>>>>>>>>");
		} catch (SQLException se) {
			LOGGER.error(ERROR_LOG + spName  );
			LOGGER.error(se);
			LOGGER.error(se.getStackTrace());
		} finally {
			connManager.disconnect();
		}
			
		return result;
	}
	
	private Result fetchAspectWiseDistributionForForDashboardData(Map<String, Object> staticParameters,
			Map<String, Object> dynamicParameters) throws DataSourceConnectionException {

		LOGGER.info(">>>>>>>JdbcDashboardDao ::: Entering fetchAspectWiseDistributionForForDashboardData>>>>>>>>");
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		
		callStmt = callStmt.append("{").append(CALL).append(spName).append("(?,?,?,STR_TO_DATE(?,'%d-%m-%Y %H:%i'),STR_TO_DATE(?,'%d-%m-%Y %H:%i'))}");
			try( CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){

			stmt.setInt(1, (Integer) dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID));
			stmt.setInt(2, (Integer) dynamicParameters.get(SP_IN_PARAM_SOURCE_ID));
			stmt.setString(3, (String) dynamicParameters.get(SP_IN_PARAM_TOPIC));
			
			if (dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_FROM_DT")) == null) {
				stmt.setNull(4, Types.VARCHAR);
			} else {
				stmt.setString(4, (String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_FROM_DT")));
			}
			if (dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_TO_DT")) == null) {
				stmt.setNull(5, Types.VARCHAR);
			} else {
				stmt.setString(5, (String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_TO_DT")));
			}
			

			LOGGER.info(">>>>> >>>>>>>JdbcDashboardDao ::: CallableStatement >>> fetchAspectWiseDistributionForForDashboardData >>>>> " + stmt);
			try(ResultSet resultSet = stmt.executeQuery();){
			ResultSetMetaData metaData = resultSet.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			for (i = 1; i <= numCols; i++) {
				result.addColumnName(metaData.getColumnLabel(i));
			}
			ArrayList<Object> row;
			while (resultSet.next()) {
				row = new ArrayList<>();
				for (i = 1; i <= numCols; i++) {
					row.add(resultSet.getObject(i));
				}
				result.addRow(row);
			}
			}
			LOGGER.info(">>>>>>>JdbcDashboardDao ::: fetchAspectWiseDistributionForForDashboardData>>>>>>>> DBResult result: " + result.toJSON());
			LOGGER.info(">>>>>>>JdbcDashboardDao ::: Exiting fetchAspectWiseDistributionForForDashboardData>>>>>>>>");
		} catch (SQLException se) {
			LOGGER.error(ERROR_LOG + spName  );
			LOGGER.error(se);
			LOGGER.error(se.getStackTrace());
		} finally {
			connManager.disconnect();
		}
			
		return result;
	}
	
	private Result fetchSourceAspectWiseSentimentDistributionData(Map<String, Object> staticParameters,
			Map<String, Object> dynamicParameters) throws DataSourceConnectionException {


		LOGGER.info(">>>>>>>JdbcDashboardDao ::: Entering fetchSourceAspectWiseSentimentDistributionData>>>>>>>>");
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		
		callStmt = callStmt.append("{").append(CALL).append(spName).append("(?,?,?,STR_TO_DATE(?,'%d-%m-%Y %H:%i'),STR_TO_DATE(?,'%d-%m-%Y %H:%i'))}");
			try( CallableStatement stmt =  connManager.connection.prepareCall(callStmt.toString());){

			stmt.setInt(1, (Integer) dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID));
			stmt.setInt(2, (Integer) dynamicParameters.get(SP_IN_PARAM_SOURCE_ID));
			stmt.setString(3, (String) dynamicParameters.get(SP_IN_PARAM_ASPECT_NAME));
			
			if (dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_FROM_DT")) == null) {
				stmt.setNull(4, Types.VARCHAR);
			} else {
				stmt.setString(4, (String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_FROM_DT")));
			}
			if (dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_TO_DT")) == null) {
				stmt.setNull(5, Types.VARCHAR);
			} else {
				stmt.setString(5, (String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_TO_DT")));
			}
			

			LOGGER.info(">>>>> >>>>>>>JdbcDashboardDao ::: CallableStatement >>> fetchSourceAspectWiseSentimentDistributionData >>>>> " + stmt);
			try(ResultSet resultSet = stmt.executeQuery();){
			ResultSetMetaData metaData = resultSet.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			for (i = 1; i <= numCols; i++) {
				result.addColumnName(metaData.getColumnLabel(i));
			}
			ArrayList<Object> row;
			while (resultSet.next()) {
				row = new ArrayList<>();
				for (i = 1; i <= numCols; i++) {
					row.add(resultSet.getObject(i));
				}
				result.addRow(row);
			}
			}
			LOGGER.info(">>>>>>>JdbcDashboardDao ::: fetchAspectWiseDistributionForForDashboardData>>>>>>>> DBResult result: " + result.toJSON());
			LOGGER.info(">>>>>>>JdbcDashboardDao ::: Exiting fetchAspectWiseDistributionForForDashboardData>>>>>>>>");
		} catch (SQLException se) {
			LOGGER.error(ERROR_LOG + spName  );
			LOGGER.error(se);
			LOGGER.error(se.getStackTrace());
		} finally {
			connManager.disconnect();
		}
			
		return result;
	}
	
	
	private Result fetchSourceAspectWiseSentimentTopFeedbackData(Map<String, Object> staticParameters,
			Map<String, Object> dynamicParameters) throws DataSourceConnectionException, IOException, ParseException {


		LOGGER.info(">>>>>>>JdbcDashboardDao ::: Entering fetchSourceAspectWiseSentimentTopFeedbackData>>>>>>>>");
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		
		    callStmt = callStmt.append("{").append(CALL).append(spName).append("(?,?,?,STR_TO_DATE(?,'%d-%m-%Y %H:%i'),STR_TO_DATE(?,'%d-%m-%Y %H:%i'),?)}");
			try( CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){

			stmt.setInt(1, (Integer) dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID));
			stmt.setInt(2, (Integer) dynamicParameters.get(SP_IN_PARAM_SOURCE_ID));
			stmt.setString(3, (String) dynamicParameters.get(SP_IN_PARAM_TOPIC));
			
			if (dynamicParameters.get(SP_IN_PARAM_FROM_DT) == null) {
				stmt.setNull(4, Types.VARCHAR);
			} else {
				stmt.setString(4, (String) dynamicParameters.get(SP_IN_PARAM_FROM_DT));
			}
			if (dynamicParameters.get(SP_IN_PARAM_TO_DT) == null) {
				stmt.setNull(5, Types.VARCHAR);
			} else {
				stmt.setString(5, (String) dynamicParameters.get(SP_IN_PARAM_TO_DT));
			}
			if (dynamicParameters.get(SP_IN_PARAM_ASPECT) == null) {
				stmt.setNull(6, Types.VARCHAR);
			} else {
				stmt.setString(6, (String) dynamicParameters.get(SP_IN_PARAM_ASPECT));
			}
			
			LOGGER.info(">>>>> >>>>>>>JdbcDashboardDao ::: CallableStatement >>> fetchSourceAspectWiseSentimentTopFeedbackData >>>>> " + stmt);
			try(ResultSet resultSet = stmt.executeQuery();){
			ResultSetMetaData metaData = resultSet.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			for (i = 1; i <= numCols; i++) {
				result.addColumnName(metaData.getColumnLabel(i));
			}
			ArrayList<Object> row =  new ArrayList<>();
			ArrayList textArr =new ArrayList<>();
			HashMap sources = new HashMap();
			while (resultSet.next()) {
				for (i = 1; i <= numCols; i++) {
					if(sources.containsKey(resultSet.getObject(1)))
					{
						/*textArr.add(resultSet.getObject(2));
						textArr.add(resultSet.getObject(3));
						sources.put(resultSet.getObject(1), textArr);
						break;*/
						ArrayList postArr =new ArrayList<>();
						postArr.add(resultSet.getObject(2));
						postArr.add(resultSet.getObject(3));
						textArr.add(postArr);
						/*textArr.add(resultSet.getObject(2));
						textArr.add(resultSet.getObject(3));*/
						sources.put(resultSet.getObject(1), textArr);
						break;
					}
					else {
						textArr =new ArrayList<>();
						sources.put(resultSet.getObject(1), textArr);
					}
				}
			}
			ObjectMapper mapper = new ObjectMapper();
			String jsonResp = mapper.writeValueAsString(sources);
			JSONParser parser = new JSONParser(); 
			JSONObject json = (JSONObject) parser.parse(jsonResp);
			row.add(json);
			result.addRow(row);

			}
			LOGGER.info(">>>>>>>JdbcDashboardDao ::: fetchSourceAspectWiseSentimentTopFeedbackData>>>>>>>> DBResult result: " + result.toJSON());
			LOGGER.info(">>>>>>>JdbcDashboardDao ::: Exiting fetchSourceAspectWiseSentimentTopFeedbackData>>>>>>>>");
		} catch (SQLException se) {
			LOGGER.error(ERROR_LOG + spName  );
			LOGGER.error(se);
			LOGGER.error(se.getStackTrace());
		} finally {
			connManager.disconnect();
		}
			
		return result;
	}

	private Result fetchSourceAspectWiseSentimentTopEntityData(Map<String, Object> staticParameters,
			Map<String, Object> dynamicParameters) throws DataSourceConnectionException {


		LOGGER.info(">>>>>>>JdbcDashboardDao ::: Entering fetchSourceAspectWiseSentimentTopEntityData>>>>>>>>");
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		
		    callStmt = callStmt.append("{").append(CALL).append(spName).append("(?,?,?,STR_TO_DATE(?,'%d-%m-%Y %H:%i'),STR_TO_DATE(?,'%d-%m-%Y %H:%i'),?)}");
			try( CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){

			stmt.setInt(1, (Integer) dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID));
			stmt.setInt(2, (Integer) dynamicParameters.get(SP_IN_PARAM_SOURCE_ID));
			stmt.setString(3, (String) dynamicParameters.get(SP_IN_PARAM_TOPIC));
			
			if (dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_FROM_DT")) == null) {
				stmt.setNull(4, Types.VARCHAR);
			} else {
				stmt.setString(4, (String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_FROM_DT")));
			}
			if (dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_TO_DT")) == null) {
				stmt.setNull(5, Types.VARCHAR);
			} else {
				stmt.setString(5, (String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_TO_DT")));
			}
			if (dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_ASPECT")) == null) {
				stmt.setNull(6, Types.VARCHAR);
			} else {
				stmt.setString(6, (String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_ASPECT")));
			}
			
			LOGGER.info(">>>>> >>>>>>>JdbcDashboardDao ::: CallableStatement >>> fetchSourceAspectWiseSentimentTopEntityData >>>>> " + stmt);
			try(ResultSet resultSet = stmt.executeQuery();){
			ResultSetMetaData metaData = resultSet.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			for (i = 1; i <= numCols; i++) {
				result.addColumnName(metaData.getColumnLabel(i));
			}
			ArrayList<Object> row;
			while (resultSet.next()) {
				row = new ArrayList<>();
				for (i = 1; i <= numCols; i++) {
					row.add(resultSet.getObject(i));
				}
				result.addRow(row);
			}
			}
			LOGGER.info(">>>>>>>JdbcDashboardDao ::: fetchAspectWiseDistributionForForDashboardData>>>>>>>> DBResult result: " + result.toJSON());
			LOGGER.info(">>>>>>>JdbcDashboardDao ::: Exiting fetchAspectWiseDistributionForForDashboardData>>>>>>>>");
		} catch (SQLException se) {
			LOGGER.error(ERROR_LOG + spName  );
			LOGGER.error(se);
			LOGGER.error(se.getStackTrace());
		} finally {
			connManager.disconnect();
		}
			
		return result;
	}

	public Result fetchEntityAndSentimentForAspect(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		
		LOGGER.info(">>>>>>>JdbcDashboardDao ::: Entering fetchEntityAndSentimentForAspect>>>>>>>>");
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		DBResult result = null;
		callStmt = callStmt.append("{").append(CALL).append(spName).append("(?,?,?,STR_TO_DATE(?,'%d-%m-%Y %H:%i'),STR_TO_DATE(?,'%d-%m-%Y %H:%i'))}");
			try( CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){

			stmt.setInt(1, (Integer) dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID));
			if (dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_SOURCE")) == null) {
				stmt.setNull(2, Types.INTEGER);
			} else {
				stmt.setInt(2, (Integer) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_SOURCE")));
			}
			if (dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_ASPECT")) == null) {
				stmt.setNull(3, Types.INTEGER);
			} else {
				stmt.setInt(3, (Integer) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_ASPECT")));
			}
			if (dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_FROM_DT")) == null) {
				stmt.setNull(4, Types.VARCHAR);
			} else {
				stmt.setString(4, (String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_FROM_DT")));
			}
			if (dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_TO_DT")) == null) {
				stmt.setNull(5, Types.VARCHAR);
			} else {
				stmt.setString(5, (String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_TO_DT")));
			}
	

			LOGGER.info(">>>>> >>>>>>>JdbcDashboardDao ::: CallableStatement >>> fetchEntityAndSentimentForAspect >>>>> " + stmt);
			try(ResultSet resultSet = stmt.executeQuery();){
				
			result = JdbcCommonDao.formJsonResultFromQuery(resultSet);

			}
			LOGGER.info(">>>>>>>JdbcDashboardDao ::: fetchEntityAndSentimentForAspect>>>>>>>> DBResult result: " + result.toJSON());
			LOGGER.info(">>>>>>>JdbcDashboardDao ::: Exiting fetchEntityAndSentimentForAspect>>>>>>>>");
		} catch (SQLException se) {
			LOGGER.error(ERROR_LOG + spName  );
			LOGGER.error(se);
			LOGGER.error(se.getStackTrace());
		} finally {
			connManager.disconnect();
		}
			
		return result;
	}
	
	
	
	
	public Result fetchEntitiesForWorCloud(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {

		LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: Entering fetchEntitiesForWorCloud>>>>>>>>");
		DBResult result = new DBResult();
		CallableStatement stmt = null;
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		try {
			
				callStmt = callStmt.append("{").append(CALL).append(spName).append("(?,?,?,STR_TO_DATE(?,'%d-%m-%Y %H:%i'),STR_TO_DATE(?,'%d-%m-%Y %H:%i'))}");
				stmt = connManager.connection.prepareCall(callStmt.toString());
				stmt.setInt(1, (Integer) dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID));
				if (dynamicParameters.get(SP_IN_PARAM_SOURCE) == null) {
					stmt.setNull(2, Types.INTEGER);
				} else {
					stmt.setInt(2, (Integer) dynamicParameters.get(SP_IN_PARAM_SOURCE));
				}
				if (dynamicParameters.get(SP_IN_PARAM_SENTIMENT_TYPE) == null) {
					stmt.setNull(3, Types.VARCHAR);
				} else {
					stmt.setString(3, (String) dynamicParameters.get(SP_IN_PARAM_SENTIMENT_TYPE));
				}
				if (dynamicParameters.get(SP_IN_PARAM_FROM_DT) == null || "".equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_FROM_DT))) {
					stmt.setNull(4, Types.VARCHAR);
				} else {
					stmt.setString(4, (String) dynamicParameters.get(SP_IN_PARAM_FROM_DT));
				}
				if (dynamicParameters.get(SP_IN_PARAM_TO_DT) == null || "".equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_TO_DT))) {
					stmt.setNull(5, Types.VARCHAR);
				} else {
					stmt.setString(5, (String) dynamicParameters.get(SP_IN_PARAM_TO_DT));
				}


			try(ResultSet results = stmt.executeQuery();){
			LOGGER.debug(stmt);
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			for (i = 1; i <= numCols; i++) {
				result.addColumnName(metaData.getColumnLabel(i));
			}
			ArrayList<Object> row;
			while (results.next()) {
				row = new ArrayList<>();
				for (i = 1; i <= numCols; i++) {
					row.add(results.getObject(i));
				}
				result.addRow(row);
			}
			}
			LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: Exiting fetchAnalyticsKPIs>>>>>>>>");
		} catch (SQLException se) {
			LOGGER.error(ERROR_LOG + spName);
			LOGGER.error(se.getMessage());
			LOGGER.error(se.getStackTrace());
		} finally {
			try {
				stmt.close();
			} catch (SQLException e) {
				LOGGER.error(e.getMessage());
			}
			connManager.disconnect();
		}
		return result;

	}

	public Result fetchOverallSentimentDistributionDataDayWise(Map<String, Object> staticParameters,
			Map<String, Object> dynamicParameters) throws DataSourceConnectionException {
		
	
		LOGGER.info(">>>>>>>JdbcDashboardDao ::: Entering fetchOverallSentimentDistributionDataDayWise>>>>>>>>");
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		
		callStmt = callStmt.append("{").append(CALL).append(spName).append("(?,?,STR_TO_DATE(?,'%d-%m-%Y %H:%i'),STR_TO_DATE(?,'%d-%m-%Y %H:%i'))}");
			try( CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){

			stmt.setInt(1, (Integer) dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID));
			if (dynamicParameters.get(SP_IN_PARAM_SOURCE) == null) {
				stmt.setNull(2, Types.VARCHAR);
			} else {
				stmt.setString(2, (String) dynamicParameters.get(SP_IN_PARAM_SOURCE));
			}
			if (dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_FROM_DT")) == null) {
				stmt.setNull(3, Types.VARCHAR);
			} else {
				stmt.setString(3, (String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_FROM_DT")));
			}
			if (dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_TO_DT")) == null) {
				stmt.setNull(4, Types.VARCHAR);
			} else {
				stmt.setString(4, (String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_TO_DT")));
			}
			
			LOGGER.info(">>>>> >>>>>>>JdbcDashboardDao ::: CallableStatement >>> fetchOverallSentimentDistributionDataDayWise >>>>> " + stmt);
			try(ResultSet resultSet = stmt.executeQuery();){
			ResultSetMetaData metaData = resultSet.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			for (i = 1; i <= numCols; i++) {
				result.addColumnName(metaData.getColumnLabel(i));
			}
			ArrayList<Object> row;
			while (resultSet.next()) {
				row = new ArrayList<>();
				for (i = 1; i <= numCols; i++) {
					row.add(resultSet.getObject(i));
				}
				result.addRow(row);
			}
			}
			LOGGER.info(">>>>>>>JdbcDashboardDao ::: fetchOverallSentimentDistributionDataDayWise>>>>>>>> DBResult result: " + result.toJSON());
			LOGGER.info(">>>>>>>JdbcDashboardDao ::: Exiting fetchOverallSentimentDistributionDataDayWise>>>>>>>>");
		} catch (SQLException se) {
			LOGGER.error(ERROR_LOG + spName  );
			LOGGER.error(se);
			LOGGER.error(se.getStackTrace());
		} finally {
			connManager.disconnect();
		}
		return result;

	}
	
	
}
