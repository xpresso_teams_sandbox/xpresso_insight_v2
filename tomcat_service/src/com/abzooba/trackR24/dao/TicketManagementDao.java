package com.abzooba.trackR24.dao;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Map;

import org.apache.log4j.Logger;

import com.abzooba.trackR24.models.DBResult;
import com.abzooba.trackR24.models.Result;
import com.abzooba.trackR24.util.PropertyHandler;
import static com.abzooba.trackR24.util.Constants.*;

public class TicketManagementDao {
	public TicketManagementDao() throws DataSourceConnectionException {
		super();
		connManager.connect();
	}

	private static final Logger LOGGER = Logger.getLogger(TicketManagementDao.class);
	private DatabaseConnectionManager connManager = new DatabaseConnectionManager();
	ResultSet results = null;
	PreparedStatement pst = null;

	public void createNewTicket(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		LOGGER.info(">>>>>>>TicketManagementDao ::: Entering createNewTicket>>>>>>>>");

		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(PropertyHandler.getPropertyValue("ATTRIBUTE_SP_NAME"));
		if (PropertyHandler.getPropertyValue("SP_CREATE_UPDATE_TICKET").equalsIgnoreCase(spName)) {
			callStmt = callStmt.append("{call ").append(spName).append("(?,?,?,?,?,?,?,STR_TO_DATE(?,'%d-%m-%Y %H:%i'),?)}");

			try(CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
				setParamTicketId(dynamicParameters, stmt);
				setParamPostId(dynamicParameters, stmt);
				setParamAssignerId(dynamicParameters, stmt);
				setParamAssigneeId(dynamicParameters, stmt);
				setParamComment(dynamicParameters, stmt);
				setParamTicketPriority(dynamicParameters, stmt);
				setParamTicketStatus(dynamicParameters, stmt);
				setParamDueDate(dynamicParameters, stmt);
				setParamCloseDate(dynamicParameters, stmt);
				stmt.executeQuery();
				LOGGER.debug(stmt);

				LOGGER.info(">>>>>>>TicketManagementDao ::: Exiting createNewTicket>>>>>>>>");
			}
			catch (SQLException se) {
				LOGGER.error(ERROR_LOG + spName);
				getCatchMessagePrint(se);

			} finally {
				connManager.disconnect();
			}
		}
	}

	private void setParamCloseDate(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_CLOSED_DATE) == null || "".equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_CLOSED_DATE))) {
			stmt.setNull(9, Types.VARCHAR);
		} else {
			stmt.setString(9, (String) dynamicParameters.get(SP_IN_PARAM_CLOSED_DATE));
		}
	}

	private void setParamDueDate(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_DUE_DATE) == null || "".equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_DUE_DATE))) {
			stmt.setNull(8, Types.VARCHAR);
		} else {
			stmt.setString(8, (String) dynamicParameters.get(SP_IN_PARAM_DUE_DATE));
		}
	}

	private void setParamTicketStatus(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_TICKET_STATUS) == null || "".equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_TICKET_STATUS))) {
			stmt.setNull(7, Types.VARCHAR);
		} else {
			stmt.setString(7, (String) dynamicParameters.get(SP_IN_PARAM_TICKET_STATUS));
		}
	}

	private void setParamTicketPriority(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_TICKET_PRIORITY) == null || "".equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_TICKET_PRIORITY))) {
			stmt.setNull(6, Types.VARCHAR);
		} else {
			stmt.setString(6, (String) dynamicParameters.get(SP_IN_PARAM_TICKET_PRIORITY));
		}
	}

	private void setParamComment(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_COMMENTS) == null || "".equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_COMMENTS))) {
			stmt.setNull(5, Types.VARCHAR);
		} else {
			stmt.setString(5, (String) dynamicParameters.get(SP_IN_PARAM_COMMENTS));
		}
	}

	private void setParamAssigneeId(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_ASSIGNEE_ID) == null || "".equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_ASSIGNEE_ID))) {
			stmt.setNull(4, Types.VARCHAR);
		} else {
			stmt.setString(4, (String) dynamicParameters.get(SP_IN_PARAM_ASSIGNEE_ID));
		}
	}

	private void setParamAssignerId(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_ASSIGNER_ID) == null || "".equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_ASSIGNER_ID))) {
			stmt.setNull(3, Types.VARCHAR);
		} else {
			stmt.setString(3, (String) dynamicParameters.get(SP_IN_PARAM_ASSIGNER_ID));
		}
	}

	private void setParamPostId(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_POST_ID) == null || "".equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_POST_ID))) {
			stmt.setNull(2, Types.VARCHAR);
		} else {
			stmt.setString(2, (String) dynamicParameters.get(SP_IN_PARAM_POST_ID));
		}
	}

	private void setParamTicketId(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_TICKET_ID) == null) {
			stmt.setNull(1, Types.INTEGER);
		} else {
			stmt.setInt(1, (Integer) dynamicParameters.get(SP_IN_PARAM_TICKET_ID));
		}
	}

	public Result getTicketList(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		LOGGER.info(">>>>>>>TicketManagementDao ::: Entering getTicketDetails>>>>>>>>");
		DBResult result;
		String sql = (String) staticParameters.get(PropertyHandler.getPropertyValue("ATTRIBUTE_SQL"));
		
		try {
			pst = connManager.connection.prepareStatement(sql);
			String inStatus = (String) dynamicParameters.get(SP_IN_PARAM_TICKET_STATUS);
			if( inStatus.equalsIgnoreCase("All")){
				LOGGER.info("Inside IF block");
				pst.setString(1, (String) dynamicParameters.get(SP_IN_PARAM_SOURCE_ID));
				pst.setString(2, (String) dynamicParameters.get(SP_IN_PARAM_FROM_DT));
				pst.setString(3, (String) dynamicParameters.get(SP_IN_PARAM_TO_DT));
			} else {
				LOGGER.info("Inside ELSE block");
				pst.setString(1, (String) dynamicParameters.get(SP_IN_PARAM_SOURCE_ID));
				pst.setString(2, (String) dynamicParameters.get(SP_IN_PARAM_TICKET_STATUS));
				pst.setString(3, (String) dynamicParameters.get(SP_IN_PARAM_FROM_DT));
				pst.setString(4, (String) dynamicParameters.get(SP_IN_PARAM_TO_DT));
			}
			
			result = new DBResult();
			LOGGER.info("Statement :: " + pst.toString());
			results = pst.executeQuery();
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			i = addCols(result, metaData, numCols);
			ArrayList<Object> row;
			i = addRows(result, i, numCols);

		} catch (Exception exc) {
			LOGGER.error(ERROR_LOG + sql);
			getCatchMsg(exc);
			throw new DataSourceException(exc.getMessage());
		} finally {
			try {
				results.close();
				pst.close();
				connManager.disconnect();
			} catch (SQLException e) {
				LOGGER.error(e.getMessage());
			}
		}
		LOGGER.info(">>>>>>>TicketManagementDao ::: Exiting getTicketDetails>>>>>>>>");
		return result;
	}
	
	public Result getTicketListRoutParam(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		LOGGER.info(">>>>>>>TicketManagementDao ::: Entering getTicketDetails>>>>>>>>");
		DBResult result;
		String sql = (String) staticParameters.get(PropertyHandler.getPropertyValue("ATTRIBUTE_SQL"));
		
		try {
			pst = connManager.connection.prepareStatement(sql);
			pst.setString(1, (String) dynamicParameters.get(SP_IN_PARAM_POST_ID));
			result = new DBResult();
			LOGGER.info("Statement :: " + pst.toString());
			results = pst.executeQuery();
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			i = addCols(result, metaData, numCols);
			ArrayList<Object> row;
			i = addRows(result, i, numCols);

		} catch (Exception exc) {
			LOGGER.error(ERROR_LOG + sql);
			getCatchMsg(exc);
			throw new DataSourceException(exc.getMessage());
		} finally {
			try {
				results.close();
				pst.close();
				connManager.disconnect();
			} catch (SQLException e) {
				LOGGER.error(e.getMessage());
			}
		}
		LOGGER.info(">>>>>>>TicketManagementDao ::: Exiting getTicketDetails>>>>>>>>");
		return result;
	}

	private int addRows(DBResult result, int i, int numCols)
			throws SQLException {
		ArrayList<Object> row;
		while (results.next()) {
			row = new ArrayList<>();
			for (i = 1; i <= numCols; i++) {
				row.add(results.getObject(i));
			}
			result.addRow(row);
		}
		return i;
	}

	private void getCatchMsg(Exception exc) {
		LOGGER.error(exc.getMessage());
		LOGGER.error(exc.getStackTrace());
	}
	
	public Result getTicketDetails(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		LOGGER.info(">>>>>>>TicketManagementDao ::: Entering getTicketDetails>>>>>>>>");
		DBResult result;
		String sql = (String) staticParameters.get(PropertyHandler.getPropertyValue("ATTRIBUTE_SQL"));
		
		try {
			pst = connManager.connection.prepareStatement(sql);
			pst.setString(1, (String) dynamicParameters.get(SP_IN_PARAM_TICKET_ID));
			result = new DBResult();
			LOGGER.info("pst :: " + pst.toString());
			results = pst.executeQuery();
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			i = addCols(result, metaData, numCols);
			ArrayList<Object> row;
			i = addRows(result, i, numCols);

		} catch (Exception exc) {
			LOGGER.error(ERROR_LOG + sql);
			getCatchMsg(exc);
			throw new DataSourceException(exc.getMessage());
		} finally {
			try {
				results.close();
				pst.close();
				connManager.disconnect();
			} catch (SQLException e) {
				LOGGER.error(e.getMessage());
			}
		}
		LOGGER.info(">>>>>>>TicketManagementDao ::: Exiting getTicketDetails>>>>>>>>");
		return result;
	}

	private int addCols(DBResult result, ResultSetMetaData metaData, int numCols)
			throws SQLException {
		int i;
		for (i = 1; i <= numCols; i++) {
			result.addColumnName(metaData.getColumnLabel(i));
		}
		return i;
	}

	public void manageTicket(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {

		LOGGER.info(">>>>>>>TicketManagementDao ::: Entering manageTicket>>>>>>>>");
		connManager.connect();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(PropertyHandler.getPropertyValue("ATTRIBUTE_SP_NAME"));
		if (PropertyHandler.getPropertyValue("SP_CREATE_UPDATE_TICKET").equalsIgnoreCase(spName)) {
			callStmt = callStmt.append("{call ").append(spName).append("(?,?,?,?,?,?,?,STR_TO_DATE(?,'%d-%m-%Y %H:%i'),STR_TO_DATE(?,'%d-%m-%Y %H:%i'))}");
			try(CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
				setParamTicketId(dynamicParameters, stmt);
				setParamPostId(dynamicParameters, stmt);
				setParamAssignerId(dynamicParameters, stmt);
				setParamAssigneeId(dynamicParameters, stmt);
				setParamComment(dynamicParameters, stmt);
				setParamTicketPriority(dynamicParameters, stmt);
				setParamTicketStatus(dynamicParameters, stmt);
				setParamDueDate(dynamicParameters, stmt);
				setParamCloseDate(dynamicParameters, stmt);

				stmt.executeQuery();
				LOGGER.debug(stmt);
				LOGGER.info(">>>>>>>TicketManagementDao ::: Exiting manageTicket>>>>>>>>");
			}
			catch (SQLException se) {
				LOGGER.error(ERROR_LOG + spName);
				getCatchMessagePrint(se);

			} finally {
				connManager.disconnect();
			}

		}
	}

	private void getCatchMessagePrint(SQLException se) {
		LOGGER.error(se.getMessage());
		LOGGER.error(se.getStackTrace());
	}
	
	
	public  Map<String, Object> getTwitterUserToken() throws DataSourceException {
		Map<String, Object> updateStatus = null;
		LOGGER.info(">>>>>>>JdbcCommonDao ::: Entering overallSentimentUpdaterForReviewAnalysis>>>>>>>>");
		try {
			updateStatus = connManager.getTwitterUserToken();
			LOGGER.info(">>>>>>>JdbcCommonDao ::: overallSentimentUpdaterForReviewAnalysis>>>>>>>> overallSentimentUpdater result: " + updateStatus);
		} catch (Exception e) {
			LOGGER.error("Error occurred in JdbcCommonDao.overallSentimentUpdaterForReviewAnalysis");
			LOGGER.error(e);
			LOGGER.error(e.getStackTrace());
		} finally {
			connManager.disconnect();
		}
		LOGGER.info(">>>>>>>JdbcCommonDao ::: Exiting overallSentimentUpdaterForReviewAnalysis>>>>>>>>");
		return updateStatus;
	}
	
	
public Map<String, String> getPostDetails(String strPostID, String sourceType, String companyId) throws DataSourceConnectionException {
		
		
		Map<String, String> postDetails = null ;
		LOGGER.info(">>>>>>>JdbcCommonDao ::: Entering overallSentimentUpdaterForReviewAnalysis>>>>>>>>");
		try {
			postDetails = connManager.getPostDetails(strPostID,companyId);
			LOGGER.info(">>>>>>>JdbcCommonDao ::: overallSentimentUpdaterForReviewAnalysis>>>>>>>> overallSentimentUpdater result: " );
		} catch (Exception e) {
			LOGGER.error("Error occurred in JdbcCommonDao.overallSentimentUpdaterForReviewAnalysis");
			LOGGER.error(e);
			LOGGER.error(e.getStackTrace());
		} finally {
			connManager.disconnect();
		}
		LOGGER.info(">>>>>>>JdbcCommonDao ::: Exiting overallSentimentUpdaterForReviewAnalysis>>>>>>>>");
				
		
		return postDetails;

	}


}