package com.abzooba.trackR24.dao;

import java.util.Map;

import com.abzooba.trackR24.models.Result;

public interface IRatingsDao {

	public Result fetchTopBottomRatedStores(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException;
}
