package com.abzooba.trackR24.dao;

import static com.abzooba.trackR24.util.Constants.ATTRIBUTE_SP_NAME;
import static com.abzooba.trackR24.util.Constants.ERROR_LOG;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_ACCOUNT_ID;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_CITY_ID;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_OWNERSHIP_ID;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_SEARCH_TEXT;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.abzooba.trackR24.models.DBResult;


public class JdbcCommonDao extends GenericJdbcDao implements ICommonDao {

	private static final Logger LOGGER = Logger.getLogger(JdbcCommonDao.class);
	
	
	public int overallSentimentUpdater(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		int updateStatus = 0;
		LOGGER.info(">>>>>>>JdbcCommonDao ::: Entering overallSentimentUpdater>>>>>>>>");
		try {
			updateStatus = connManager.overallSentimentUpdater(staticParameters, dynamicParameters);
			LOGGER.info(">>>>>>>JdbcCommonDao ::: overallSentimentUpdater>>>>>>>> overallSentimentUpdater result: " + updateStatus);
		} catch (Exception e) {
			LOGGER.error("Error occurred in JdbcCommonDao.overallSentimentUpdater");
			LOGGER.error(e);
			LOGGER.error(e.getStackTrace());
		} finally {
			connManager.disconnect();
		}
		LOGGER.info(">>>>>>>JdbcCommonDao ::: Exiting overallSentimentUpdater>>>>>>>>");
		return updateStatus;
	}
	
	public int invalidDataUpdater(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		int updateStatus = 0;
		LOGGER.info(">>>>>>>JdbcCommonDao ::: Entering invalidDataUpdater>>>>>>>>");
		try {
			updateStatus = connManager.invalidDataUpdater(staticParameters, dynamicParameters);
			LOGGER.info(">>>>>>>JdbcCommonDao ::: invalidDataUpdater>>>>>>>> invalidDataUpdater result: " + updateStatus);
		} catch (Exception e) {
			LOGGER.error("Error occurred in JdbcCommonDao.invalidDataUpdater");
			LOGGER.error(e);
			LOGGER.error(e.getStackTrace());
		} finally {
			connManager.disconnect();
		}
		LOGGER.info(">>>>>>>JdbcCommonDao ::: Exiting invalidDataUpdater>>>>>>>>");
		return updateStatus;
	}

	/**
	 * Helper Method to form JSON From execute results
	 * @param results
	 * @return
	 * @throws SQLException
	 */
	public static DBResult formJsonResultFromQuery(ResultSet results) throws SQLException {
		
		DBResult result =new DBResult();
		
		ResultSetMetaData metaData = results.getMetaData();
		int i;
		int numCols = metaData.getColumnCount();
		for (i = 1; i <= numCols; i++) {
			result.addColumnName(metaData.getColumnLabel(i));
		}
		ArrayList<Object> row;
		while (results.next()) {
			row = new ArrayList<>();
			for (i = 1; i <= numCols; i++) {
				row.add(results.getObject(i));
			}
			result.addRow(row);
		}
		
		return result;
	}
	
//==============================Add By Jayanta====================================
	public Map<String, Object> fetchInvalidReport
			(Map<String, Object> staticParameters) throws DataSourceConnectionException {
		
			LOGGER.info(">>>>>>>jdbcCommonDao ::: Entering fetchInvalidReport>>>>>>>>"); 
			Map<String, Object> csvData = new HashMap<>();
			StringBuilder callStmt = new StringBuilder();
			String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);	
			callStmt = callStmt.append("{call ").append(spName).append("()}");

			try(CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
			LOGGER.info(stmt);		
			try(ResultSet results = stmt.executeQuery();){
			csvData = convertResultSetToCsv(results);		
			}
			LOGGER.info(">>>>>>>JdbccommonDao ::: Exiting fetchSourcewisestorelistsinExcelNew>>>>>>>>");
		} catch (SQLException se) {
			LOGGER.error(ERROR_LOG + spName);
			LOGGER.error(se.getMessage());
			LOGGER.error(se.getStackTrace());
		} finally {
			connManager.disconnect();
		}
		return csvData;

	}
	
	private Map<String, Object> convertResultSetToCsv(ResultSet results) throws  SQLException {
		LOGGER.info(">>>>>>>JdbcCommonDao ::: convertToCsvNew>>>>>>>>");
		List<String> body = new ArrayList<String>();
        ResultSetMetaData metaData = results.getMetaData() ;      
        int numberOfColumns = metaData.getColumnCount() ; 

        StringBuilder dataHeaders=new StringBuilder();
		for (int i = 1; i <= numberOfColumns; i++) {
			dataHeaders.append(metaData.getColumnLabel(i));
			if(i==1){
				dataHeaders.append(",");
				dataHeaders.append("Feedback Time");
			}
			if(i != numberOfColumns ){
				dataHeaders.append(",");
			}	
		}
		LOGGER.info("Header column:"+dataHeaders.toString());

		StringBuilder dataValues = new StringBuilder();
        while (results.next()) {
        	//StringBuilder dataValues = new StringBuilder();
        	dataValues.append(results.getString(1).substring(0, results.getString(1).indexOf(" ")));  
        	dataValues.append(",");
        	dataValues.append(results.getString(1).substring(results.getString(1).indexOf(" ")));   
        	dataValues.append(",");
        	dataValues.append(results.getString(2).replace(",", "~").replaceAll("\n", ""));  
        	dataValues.append(", ");
        	dataValues.append(results.getString(3));
        	dataValues.append(",");
        	dataValues.append(results.getString(4));
        	dataValues.append(",");
        	dataValues.append(results.getString(5));
        	dataValues.append(",");
        	dataValues.append(results.getString(6));
        	dataValues.append("\n");
        	//body.add(dataValues.toString());
        }
        LOGGER.info("rows...."+body.toString());
        LOGGER.info(">>>>>>>JdbcratingsDao ::: CSV Writing Ended>>>>>>>>");
        Map<String, Object> csvData = new HashMap<>();
        csvData.put("header", dataHeaders.toString());
        csvData.put("body", dataValues.toString());
        results.close();
		return csvData;
	}
//--------------------------------------------------------------------------------	
}
