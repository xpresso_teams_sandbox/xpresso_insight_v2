package com.abzooba.trackR24.dao;

import java.util.Map;

import com.abzooba.trackR24.models.Result;

/**
* Author Name: Kunal Kashyap
* Create Date: 18-08-2016
* Last Modified: Kunal Kashyap: 16-09-2016
* Class Insight:  This class contains all the dashboard related methods for calling the sql queries and procedures using hibernate.
*/
public class HibernateDashboardDao implements IDashboardDao {

	@Override
	public Result fetchHappinessIndexLive(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		return null;
	}

	@Override
	public Result fetchDailySnapshotNew(Map<String, Object> staticParameters,
			Map<String, Object> dynamicParameters) throws DataSourceException {
		return null;
	}

	@Override
	public Result fetchCompetitiveHappinessIndex(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		return null;
	}

	@Override
	public Result fetchContextSentimentDetails(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		return null;
	}

	@Override
	public Result fetchContextwiseSentimentDistribution(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		return null;
	}

	@Override
	public Result fetchDailySnapshot(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		return null;
	}

}
