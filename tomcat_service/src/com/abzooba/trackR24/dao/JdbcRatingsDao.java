package com.abzooba.trackR24.dao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.abzooba.trackR24.models.DBResult;
import com.abzooba.trackR24.models.Result;

import static com.abzooba.trackR24.util.Constants.*;

public class JdbcRatingsDao extends GenericJdbcDao implements IRatingsDao {

	private static final Logger LOGGER = Logger.getLogger(JdbcRatingsDao.class);

	public Result fetchTopBottomRatedStores(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {

		LOGGER.info(">>>>>>>jdbcRatingsDao ::: Entering fetchTopBottomRatedStores>>>>>>>>");
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		
		callStmt = callStmt.append("{").append(CALL).append(spName).append(CONST_FOUR_QUESTION_MARK);

		try(CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
			setInParamSourceId(dynamicParameters, stmt);
			setInParamCityIdPositionTwo(dynamicParameters, stmt);
			setInParamAccountIdPositionThree(dynamicParameters, stmt);
			setInParamAccountIdPositionFour(dynamicParameters, stmt);

			try(ResultSet results = stmt.executeQuery();){
			LOGGER.debug(stmt);
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			i = setColumnNames(result, metaData, numCols);
			ArrayList<Object> row;
			while (results.next()) {
				row = new ArrayList<>();
				for (i = 1; i <= numCols; i++) {
					row.add(results.getObject(i));
				}
				result.addRow(row);
			}
			}
			LOGGER.info(">>>>>>>JdbcratingsDao ::: Exiting fetchTopBottomRatedStores>>>>>>>>");
		} catch (SQLException se) {
			generatePrintLogMessage(spName, se);
		} finally {
			connManager.disconnect();
		}
		return result;

	}

	private void setInParamAccountIdPositionThree(
			Map<String, Object> dynamicParameters, CallableStatement stmt)
			throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID) == null) {
			stmt.setNull(3, Types.INTEGER);
		} else {
			stmt.setInt(3, (Integer) dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID));
		}
	}

	public Result fetchCitywiseSourcewiseStoresRating(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {

		LOGGER.info(">>>>>>>jdbcRatingsDao ::: Entering fetchTopBottomRatedStores>>>>>>>>");
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		
		callStmt = callStmt.append("{").append(CALL).append(spName).append("(?,?,?,?,STR_TO_DATE(?,'%d-%m-%Y %H:%i'),STR_TO_DATE(?,'%d-%m-%Y %H:%i'))}");

		try(CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
			setInParamSourceId(dynamicParameters, stmt);
			setInParamCityIdPositionTwo(dynamicParameters, stmt);
			setInParamAccountIdPositionThree(dynamicParameters, stmt);
			setInParamAccountIdPositionFour(dynamicParameters, stmt);
			setInParamFromDate(dynamicParameters, stmt);
			setInParamToDate(dynamicParameters, stmt);
			
			try(ResultSet results = stmt.executeQuery();){
			LOGGER.debug(stmt);
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			i = setColumnNames(result, metaData, numCols);
			ArrayList<Object> row;
			while (results.next()) {
				row = new ArrayList<>();
				for (i = 1; i <= numCols; i++) {
					row.add(results.getObject(i));
				}
				result.addRow(row);
			}
			}
			LOGGER.info(">>>>>>>JdbcratingsDao ::: Exiting fetchTopBottomRatedStores>>>>>>>>");
		} catch (SQLException se) {
			generatePrintLogMessage(spName, se);
		} finally {
			connManager.disconnect();
		}
		return result;

	}

	private void setInParamCityIdPositionTwo(
			Map<String, Object> dynamicParameters, CallableStatement stmt)
			throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_CITY_ID) == null) {
			stmt.setNull(2, Types.INTEGER);
		} else {
			stmt.setInt(2, (Integer) dynamicParameters.get(SP_IN_PARAM_CITY_ID));
		}
	}

	private void setInParamToDate(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_TO_DT) == null || StringUtils.EMPTY.equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_TO_DT))) {
			stmt.setNull(6, Types.VARCHAR);
		} else {
			stmt.setString(6, (String) dynamicParameters.get(SP_IN_PARAM_TO_DT));
		}
	}

	private void setInParamFromDate(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_FROM_DT) == null || StringUtils.EMPTY.equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_FROM_DT))) {
			stmt.setNull(5, Types.VARCHAR);
		} else {
			stmt.setString(5, (String) dynamicParameters.get(SP_IN_PARAM_FROM_DT));
		}
	}

	private void setInParamAccountIdPositionFour(
			Map<String, Object> dynamicParameters, CallableStatement stmt)
			throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_OWNERSHIP_ID) == null) {
			stmt.setNull(4, Types.INTEGER);
		} else {
			stmt.setInt(4, (Integer) dynamicParameters.get(SP_IN_PARAM_OWNERSHIP_ID));
		}
	}

	private int setColumnNames(DBResult result, ResultSetMetaData metaData,
			int numCols) throws SQLException {
		int i;
		for (i = 1; i <= numCols; i++) {
			result.addColumnName(metaData.getColumnLabel(i));
			LOGGER.info(COLUMN_NAME_LOG + metaData.getColumnLabel(i));
		}
		return i;
	}

	public Result fetchCompetitorComparisionStores(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {

		LOGGER.info(">>>>>>>jdbcRatingsDao ::: Entering fetchCompetitorComparisionStores>>>>>>>>");
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		callStmt = callStmt.append("{").append(CALL).append(spName).append("(?,?,?)}");

			try(CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
				
			setInParamSourceId(dynamicParameters, stmt);
			setInParamCityIdPositionTwo(dynamicParameters, stmt);
			setInParamAccountIdPositionThree(dynamicParameters, stmt);
			

			try(ResultSet results = stmt.executeQuery();){
				LOGGER.debug(stmt);
				ResultSetMetaData metaData = results.getMetaData();
				int i;
				int numCols = metaData.getColumnCount();
				i = setColumnNames(result, metaData, numCols);
				ArrayList<Object> row;
				while (results.next()) {
					row = new ArrayList<>();
					for (i = 1; i <= numCols; i++) {
						row.add(results.getObject(i));
					}
					result.addRow(row);
				}
			}
			LOGGER.info(">>>>>>>JdbcratingsDao ::: Exiting fetchCompetitorComparisionStores>>>>>>>>");
		} catch (SQLException se) {
			generatePrintLogMessage(spName, se);
		} finally {
			connManager.disconnect();
		}
		return result;

	}

	private void generatePrintLogMessage(String spName, SQLException se) {
		LOGGER.error(ERROR_LOG + spName);
		LOGGER.error(se.getMessage());
		LOGGER.error(se.getStackTrace());
	}

	private void setInParamSourceId(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_SOURCE_ID) == null) {
			stmt.setNull(1, Types.INTEGER);
		} else {
			stmt.setInt(1, (Integer) dynamicParameters.get(SP_IN_PARAM_SOURCE_ID));
		}
	}
	
	public Result fetchAvgRatedStores(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {

		LOGGER.info(">>>>>>>jdbcRatingsDao ::: Entering fetchAvgRatedStores>>>>>>>>");
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		callStmt = callStmt.append("{").append(CALL).append(spName).append("(?)}");
		try(CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
			
			setInParamCityId(dynamicParameters, stmt);

			try(ResultSet results = stmt.executeQuery();){
			LOGGER.debug(stmt);
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			i = setColumnNames(result, metaData, numCols);
			ArrayList<Object> row;
			while (results.next()) {
				row = new ArrayList<>();
				for (i = 1; i <= numCols; i++) {
					row.add(results.getObject(i));
				}
				result.addRow(row);
			}
			}
			LOGGER.info(">>>>>>>JdbcratingsDao ::: Exiting fetchTopRatedStores>>>>>>>>");
		} catch (SQLException se) {
			generatePrintLogMessage(spName, se);
		} finally {
			connManager.disconnect();
		}
		return result;

	}

	public Result fetchOverallRatingStores(	Map<String, Object> staticParameters,Map<String, Object> dynamicParameters) throws DataSourceConnectionException {
		LOGGER.info(">>>>>>>jdbcRatingsDao ::: Entering fetchTopRatedStores>>>>>>>>");
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		
			callStmt = callStmt.append(CONST_CALL_STRING_FOR_SP).append(spName).append("(?,?,?)}");

			try(CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
			
			setInParamCityId(dynamicParameters, stmt);
			setInParamAccountId(dynamicParameters, stmt);
			setInParamOwnershipId(dynamicParameters, stmt);

			try(ResultSet results = stmt.executeQuery();){
			LOGGER.debug(stmt);
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			i = setColumnNames(result, metaData, numCols);
			ArrayList<Object> row;
			while (results.next()) {
				row = new ArrayList<>();
				for (i = 1; i <= numCols; i++) {
					row.add(results.getObject(i));
					LOGGER.info("Adding Row : " + results.getObject(i));
				}
				result.addRow(row);
			}
			}
			LOGGER.info(">>>>>>>JdbcratingsDao ::: Exiting fetchTopRatedStores>>>>>>>>");
		} catch (SQLException se) {
			generatePrintLogMessage(spName, se);
		} finally {
			connManager.disconnect();
		}
		return result;
	}

	public Result fetchSourcewisestorelists(Map<String, Object> staticParameters,Map<String, Object> dynamicParameters) throws DataSourceConnectionException {
		LOGGER.info(">>>>>>>jdbcRatingsDao ::: Entering fetchSourcewisestorelists>>>>>>>>");
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		callStmt = callStmt.append(CONST_CALL_STRING_FOR_SP).append(spName).append(CONST_FOUR_QUESTION_MARK);
			try(CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
			
			setInParamCityId(dynamicParameters, stmt);
			setInParamAccountId(dynamicParameters, stmt);
			setInParamOwnershipId(dynamicParameters, stmt);
			setInParamSearchText(dynamicParameters, stmt);

			try(ResultSet results = stmt.executeQuery();){
			LOGGER.debug(stmt);
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			i = setColumnNames(result, metaData, numCols);
			ArrayList<Object> row;
			while (results.next()) {
				row = new ArrayList<>();
				for (i = 1; i <= numCols; i++) {
					row.add(results.getObject(i));
					LOGGER.info("Adding Row : " + results.getObject(i));
				}
				result.addRow(row);
			}
			}
			LOGGER.info(">>>>>>>JdbcratingsDao ::: Exiting fetchSourcewisestorelists>>>>>>>> Printing : " + result.toString());
		} catch (SQLException se) {
			generatePrintLogMessage(spName, se);
		} finally {
			connManager.disconnect();
		}
		return result;



	}

	
	public Result fetchOwnershipList(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		LOGGER.info(">>>>>>>JdbcCommonDao ::: Entering fetchOwnershipList>>>>>>>>");
		try {
			result = connManager.getCompanyListResultSet(staticParameters, dynamicParameters);
			LOGGER.info(">>>>>>>JdbcCommonDao ::: fetchOwnershipList>>>>>>>>  result: " + result.toJSON());
		} catch (Exception e) {
			LOGGER.error("Error occurred in JdbcCommonDao.fetchOwnershipList");
			LOGGER.error(e);
			LOGGER.error(e.getStackTrace());
		} finally {
			connManager.disconnect();
		}
		LOGGER.info(">>>>>>>JdbcCommonDao ::: Exiting fetchOwnershipList>>>>>>>>");
		return result;
	}

	public Result fetchSourcewisestorelistsinExcel(Map<String, Object> staticParameters,Map<String, Object> dynamicParameters, String filePath) throws DataSourceConnectionException, FileNotFoundException 
	{
		LOGGER.info(">>>>>>>jdbcRatingsDao ::: Entering fetchSourcewisestorelistsinExcel>>>>>>>>");
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
	
			callStmt = callStmt.append(CONST_CALL_STRING_FOR_SP).append(spName).append(CONST_FOUR_QUESTION_MARK);
			try(CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
			
			setInParamCityId(dynamicParameters, stmt);
			setInParamAccountId(dynamicParameters, stmt);
			setInParamOwnershipId(dynamicParameters, stmt);
			setInParamSearchTextPositionFour(dynamicParameters, stmt);

			try(ResultSet results = stmt.executeQuery();){
			convertToCsv(results,filePath);
			}
			LOGGER.info(">>>>>>>JdbcratingsDao ::: Exiting fetchSourcewisestorelistsinExcel>>>>>>>>");
		} catch (SQLException se) {
			generatePrintLogMessage(spName, se);
		} finally {
			connManager.disconnect();
		}
		return result;

	}

	private void setInParamSearchTextPositionFour(
			Map<String, Object> dynamicParameters, CallableStatement stmt)
			throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_SEARCH_TEXT) == null) {
			stmt.setNull(4, Types.NULL);
		} else {
			stmt.setString(4,  (String) dynamicParameters.get(SP_IN_PARAM_SEARCH_TEXT));
		}
	}

	private void setInParamCityId(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_CITY_ID) == null) {
			stmt.setNull(1, Types.INTEGER);
		} else {
			stmt.setInt(1, (Integer) dynamicParameters.get(SP_IN_PARAM_CITY_ID));
		}
	}

	private void convertToCsv(ResultSet results, String filePath) throws SQLException, FileNotFoundException {
		LOGGER.info(">>>>>>>JdbcratingsDao ::: CSV Writing started>>>>>>>>");
		try(PrintWriter csvWriter = new PrintWriter(new File(filePath));){
       

        ResultSetMetaData metaData = results.getMetaData() ; 
        
        int numberOfColumns = metaData.getColumnCount() ; 
        
        
        StringBuilder dataHeaders=new StringBuilder();
		for (int i = 1; i <= numberOfColumns; i++) {
			dataHeaders.append("\"");
			dataHeaders.append(metaData.getColumnLabel(i));
			dataHeaders.append("\"");
			dataHeaders.append(",");
		
		}
		LOGGER.info("Header column:"+dataHeaders.toString());
		
        csvWriter.println(dataHeaders.toString()) ;
        
        StringBuilder dataValues = new StringBuilder();
        while (results.next()) {
        	dataValues.append(results.getString("store_name"));
        	dataValues.append(",");
        	dataValues.append(Double.toString(results.getDouble("Zomato")));
        	dataValues.append(",");
        	dataValues.append(Double.toString(results.getDouble("Food Panda")));
        	dataValues.append(",");
        	dataValues.append(Double.toString(results.getDouble("MagicPin")));
        	dataValues.append("\n");

        }
        LOGGER.info("rows...."+dataValues.toString());
        csvWriter.println(dataValues.toString()) ;
        results.close();
		}
  
        LOGGER.info(">>>>>>>JdbcratingsDao ::: CSV Writing Ended>>>>>>>>");
	}

	public Map<String, String> fetchSourcewisestorelistsinExcelNew(
			Map<String, Object> staticParameters,
			Map<String, Object> dynamicParameters) throws DataSourceConnectionException {
		
		LOGGER.info(">>>>>>>jdbcRatingsDao ::: Entering fetchSourcewisestorelistsinExcelNew>>>>>>>>"); 
		Map<String, String> csvData = new HashMap<>();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		
			//AKN10102018
			callStmt = callStmt.append(CONST_CALL_STRING_FOR_SP).append(spName).append(CONST_FOUR_QUESTION_MARK);
			//eocAKN10102018

			try(CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
			
			setInParamCityId(dynamicParameters, stmt);
			setInParamAccountId(dynamicParameters, stmt);
			setInParamOwnershipId(dynamicParameters, stmt);
			setInParamSearchText(dynamicParameters, stmt);

			LOGGER.info(stmt);
			
			try(ResultSet results = stmt.executeQuery();){
	
			csvData = convertToCsvNew(results);
			
			}
			LOGGER.info(">>>>>>>JdbcratingsDao ::: Exiting fetchSourcewisestorelistsinExcelNew>>>>>>>>");
		} catch (SQLException se) {
			generatePrintLogMessage(spName, se);
		} finally {
			connManager.disconnect();
		}
		return csvData;

	}

	private void setInParamSearchText(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_SEARCH_TEXT) == null) {
			stmt.setNull(4, Types.VARCHAR);
		} else {
			stmt.setString(4,  (String) dynamicParameters.get(SP_IN_PARAM_SEARCH_TEXT));
		}
	}

	private void setInParamOwnershipId(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_OWNERSHIP_ID) == null) {
			stmt.setNull(3, Types.INTEGER);
		} else {
			stmt.setInt(3, (Integer) dynamicParameters.get(SP_IN_PARAM_OWNERSHIP_ID));
		}
	}

	private void setInParamAccountId(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID) == null) {
			stmt.setNull(2, Types.INTEGER);
		} else {
			stmt.setInt(2, (Integer) dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID));
		}
	}

	private Map<String, String> convertToCsvNew(ResultSet results) throws  SQLException {
		LOGGER.info(">>>>>>>JdbcratingsDao ::: convertToCsvNew>>>>>>>>");

        ResultSetMetaData metaData = results.getMetaData() ; 
        
        int numberOfColumns = metaData.getColumnCount() ; 
        
        
        StringBuilder dataHeaders=new StringBuilder();
		for (int i = 1; i <= numberOfColumns; i++) {
			dataHeaders.append(metaData.getColumnLabel(i));
			if(i != numberOfColumns ){
				dataHeaders.append(",");
			}
		
		}
		LOGGER.info("Header column:"+dataHeaders.toString());
		
        
        StringBuilder dataValues = new StringBuilder();
        while (results.next()) {
        	dataValues.append(results.getString("store_name"));
        	dataValues.append(",");
        	dataValues.append(Double.toString(results.getDouble("Zomato")));
        	dataValues.append(",");
        	dataValues.append(Double.toString(results.getDouble("Food Panda")));
        	//dataValues.append(",");
        	//dataValues.append(Double.toString(results.getDouble("MagicPin")));
        	dataValues.append(",");
        	dataValues.append(Double.toString(results.getDouble("Swiggy")));
        	dataValues.append(",");
        	dataValues.append(results.getString("Entity"));
        	dataValues.append(",");
        	dataValues.append(results.getString("last_Zomato"));
        	dataValues.append(",");
        	dataValues.append(results.getString("last_Food_Panda"));
        	dataValues.append(",");
        	dataValues.append(results.getString("last_Swiggy"));
        	dataValues.append("\n");

        }
        LOGGER.info("rows...."+dataValues.toString());
        LOGGER.info(">>>>>>>JdbcratingsDao ::: CSV Writing Ended>>>>>>>>");
        Map<String, String> csvData = new HashMap<>();
        csvData.put("header", dataHeaders.toString());
        csvData.put("body", dataValues.toString());
        results.close();
		return csvData;

	}

}
