package com.abzooba.trackR24.dao;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Map;

import org.apache.log4j.Logger;

import com.abzooba.trackR24.models.DBResult;
import com.abzooba.trackR24.models.Result;
import com.abzooba.trackR24.util.PropertyHandler;
import static com.abzooba.trackR24.util.Constants.*;

public class JdbcReviewAnalysisDao extends GenericJdbcDao implements IReviewAnalysisDao {

	private static final Logger LOGGER = Logger.getLogger(JdbcReviewAnalysisDao.class);

	public Result fetchReviewAnalysisData(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceConnectionException {
		LOGGER.info(">>>>>>>JdbcReviewAnalyticsDao ::: Entering fetchReviewAnalysisData>>>>>>>>");
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(PropertyHandler.getPropertyValue("ATTRIBUTE_SP_NAME"));
		callStmt = callStmt.append("{call ").append(spName).append("(?,?,?,?,?,STR_TO_DATE(?,'%d-%m-%Y %H:%i'),STR_TO_DATE(?,'%d-%m-%Y %H:%i'),?,?,?)}");
		
		try(CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
			if (dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID) == null) {
				stmt.setNull(1, Types.INTEGER);
			} else {
				stmt.setInt(1, (Integer) dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID));
			}
			if (dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_SOURCE")) == null) {
				stmt.setNull(2, Types.INTEGER);
			} else {
				stmt.setInt(2, (Integer) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_SOURCE")));
			}
			// --
			if (dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_CITY")) == null) {
				stmt.setNull(3, Types.INTEGER);
			} else {
				stmt.setInt(3, (Integer) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_CITY")));
			}
			if (dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_LOCALITY")) == null) {
				stmt.setNull(4, Types.VARCHAR);
			} else {
				stmt.setString(4, (String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_LOCALITY")));
			}
			if (dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_SENTIMENT_TYPE")) == null) {
				stmt.setNull(5, Types.INTEGER);
			} else {
				stmt.setInt(5, (int) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_SENTIMENT_TYPE")));
			}
			if (dynamicParameters.get(SP_IN_PARAM_FROM_DT) == null || "".equalsIgnoreCase((String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_FROM_DT")))) {
				stmt.setNull(6, Types.VARCHAR);
			} else {
				stmt.setString(6, (String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_FROM_DT")));
			}
			if (dynamicParameters.get(SP_IN_PARAM_TO_DT) == null || "".equalsIgnoreCase((String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_TO_DT")))) {
				stmt.setNull(7, Types.VARCHAR);
			} else {
				stmt.setString(7, (String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_TO_DT")));
			}
			// Adding parameter for pagination
			if (dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_OFFSET")) == null) {
				stmt.setNull(8, Types.INTEGER);
			} else {
				stmt.setInt(8, (Integer) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_OFFSET")));
			}
			if (dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_LIMIT")) == null) {
				stmt.setNull(9, Types.INTEGER);
			} else {
				stmt.setInt(9, (Integer) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_LIMIT")));
			}
			if (dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_OWNERSHIP_ID")) == null) {
				stmt.setNull(10, Types.INTEGER);
			} else {
				stmt.setInt(10, (int) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_OWNERSHIP_ID")));
			}
			
			LOGGER.debug(">>>>>>>JdbcAnalyticsDao ::: fetchExploreKPI>>>>>>>> CallableStatement: " + stmt);
			try (ResultSet results = stmt.executeQuery();) {
				LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: fetchExploreKPI>>>>>>>> " + stmt + " >>>>>>>> execution complete...");
				ResultSetMetaData metaData = results.getMetaData();
				int i;
				int numCols = metaData.getColumnCount();
				LOGGER.info("total column:"+ numCols);
				i = addColumns(result, metaData, numCols);
				ArrayList<Object> row;
				while (results.next()) {
					row = new ArrayList<>();
					//This is being commented as we are handling the count part in different way in the frontend itself. fetching the count from first row .
					result.setCount(0);
					for (i = 1; i <= numCols; i++) {
						row.add(results.getObject(i));
					}
					result.addRow(row);
				}
				LOGGER.info(">>>>>>>JdbcReviewAnalyticsDao ::: fetchExploreKPI>>>>>>>> DBResult result: " + result.toJSON());
			} 
		} catch (SQLException se) {
			LOGGER.error("Error occurred in " + spName);
			LOGGER.error(se);
			LOGGER.error(se.getStackTrace());
		} finally {
			connManager.disconnect();
		}
		return result;
	}

	private int addColumns(DBResult result, ResultSetMetaData metaData,
			int numCols) throws SQLException {
		int i;
		for (i = 1; i <= numCols; i++) {
			result.addColumnName(metaData.getColumnLabel(i));
		}
		return i;
	}
	
	public Result fetchReviewAnalysisSources(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceConnectionException {
		LOGGER.info(">>>>>>>JdbcReviewAnalysisDao ::: Entering fetchReviewAnalysisSources>>>>>>>>");
		LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: Entering fetchReviewAnalysisSources>>>>>>>>");
		try {
			result = connManager.getSourceTopicResultSet(staticParameters, dynamicParameters);
		} catch (Exception e) {
			LOGGER.error("Error occurred in JdbcAnalyticsDao.fetchSourcesOrContexts");
			printCatchLogMessage(e);
		} finally {
			connManager.disconnect();
		}
		LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: Exiting fetchSourcesOrContexts>>>>>>>>");
		return result;
	}

	private void printCatchLogMessage(Exception e) {
		LOGGER.error(e.getMessage());
		LOGGER.error(e.getStackTrace());
	}
	
	//Getting kpi list
	public Result fetchKpiList(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		LOGGER.info(">>>>>>>fetchKpiList ::: Entering fetchKpiList>>>>>>>>");
		try {
			result = connManager.getKPIListResultSet(staticParameters, dynamicParameters);
			LOGGER.info(">>>>>>>JdbcCommonDao ::: fetchHeadlines>>>>>>>> getHeadlinesResultSet result: " + result.toJSON());
		} catch (Exception e) {
			LOGGER.error("Error occurred in JdbcCommonDao.fetchHeadlines");
			printlogCatchMessages(e);
		} finally {
			connManager.disconnect();
		}
		LOGGER.info(">>>>>>>JdbcCommonDao ::: Exiting fetchHeadlines>>>>>>>>");
		return result;
	}

	private void printlogCatchMessages(Exception e) {
		LOGGER.error(e);
		LOGGER.error(e.getStackTrace());
	}

	//Getting city list against selected source
	public Result fetchCityListAginstSource(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceConnectionException {
		LOGGER.info(">>>>>>>fetchCityListAginstSource ::: Entering fetchCityListAginstSource>>>>>>>>");
		try {
			result = connManager.getCityResultSet(staticParameters, dynamicParameters);
		} catch (Exception e) {
			LOGGER.error("Error occurred in fetchCityListAginstSource");
			printCatchLogMessage(e);
		} finally {
			connManager.disconnect();
		}
		LOGGER.info(">>>>>>>fetchCityListAginstSource ::: Exiting fetchCityListAginstSource>>>>>>>>");
		return result;
	}
	
	
	//Getting localities list against selected source
	public Result fetchLocalityListAginstSource(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceConnectionException {
		LOGGER.info(">>>>>>>JdbcReviewAnalysisDao ::: Entering fetchLocalityListAginstSource>>>>>>>>");
		try {
			result = connManager.getCityResultSet(staticParameters, dynamicParameters);
		} catch (Exception e) {
			LOGGER.error("Error occurred in fetchCityListAginstSource");
			printCatchLogMessage(e);
		} finally {
			connManager.disconnect();
		}
		LOGGER.info(">>>>>>>JdbcReviewAnalysisDao ::: Exiting fetchLocalityListAginstSource>>>>>>>>");
		return result;
	}
	
		
	//Getting localities list against selected source and city
	public Result fetchLocalityListAginstSourceCity(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceConnectionException {
		LOGGER.info(">>>>>>>JdbcReviewAnalysisDao ::: Entering fetchLocalityListAginstSourceCity>>>>>>>>");
		try {
			result = connManager.getLocalityResultSet(staticParameters, dynamicParameters);
		} catch (Exception e) {
			LOGGER.error("Error occurred in fetchLocalityListAginstSourceCity");
			printCatchLogMessage(e);
		} finally {
			connManager.disconnect();
		}
		LOGGER.info(">>>>>>>JdbcReviewAnalysisDao ::: Exiting fetchLocalityListAginstSourceCity>>>>>>>>");
		return result;
	}

	public Result fetchReviewSnapshotData(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceConnectionException {
		LOGGER.info(">>>>>>>JdbcDashboardDao ::: Entering fetchReviewSnapshotData>>>>>>>>");
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(PropertyHandler.getPropertyValue("ATTRIBUTE_SP_NAME"));
		callStmt = callStmt.append("{call ").append(spName).append("(?,?,STR_TO_DATE(?,'%d-%m-%Y %H:%i'),STR_TO_DATE(?,'%d-%m-%Y %H:%i'),STR_TO_DATE(?,'%d-%m-%Y %H:%i'),STR_TO_DATE(?,'%d-%m-%Y %H:%i'))}");
		try( CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
			if (dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_SOURCE_ID")) == null) {
				stmt.setNull(1, Types.INTEGER);
			} else {
				stmt.setInt(1, (Integer) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_SOURCE_ID")));
			}
			if (dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_ACCOUNT_ID")) == null) {
				stmt.setNull(2, Types.INTEGER);
			} else {
				stmt.setInt(2, (Integer) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_ACCOUNT_ID")));
			}
			if (dynamicParameters.get(SP_IN_PARAM_PERIODIC_START_FROM) == null || "".equalsIgnoreCase((String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_PERIODIC_START_FROM")))) {
				stmt.setNull(3, Types.VARCHAR);
			} else {
				stmt.setString(3, (String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_PERIODIC_START_FROM")));
			}
			if (dynamicParameters.get(SP_IN_PARAM_PERIODIC_START_TO) == null || "".equalsIgnoreCase((String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_PERIODIC_START_TO")))) {
				stmt.setNull(4, Types.VARCHAR);
			} else {
				stmt.setString(4, (String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_PERIODIC_START_TO")));
			}
			if (dynamicParameters.get(SP_IN_PARAM_PERIODIC_END_FROM) == null || "".equalsIgnoreCase((String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_PERIODIC_END_FROM")))) {
				stmt.setNull(5, Types.VARCHAR);
			} else {
				stmt.setString(5, (String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_PERIODIC_END_FROM")));
			}
			if (dynamicParameters.get(SP_IN_PARAM_PERIODIC_END_TO) == null || "".equalsIgnoreCase((String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_PERIODIC_END_TO")))) {
				stmt.setNull(6, Types.VARCHAR);
			} else {
				stmt.setString(6, (String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_PERIODIC_END_TO")));
			}
			
			LOGGER.debug(">>>>> >>>>>>>JdbcDashboardDao ::: CallableStatement >>> fetchReviewSnapshotData >>>>> " + stmt);
			try(ResultSet results = stmt.executeQuery();){
			
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			i = addColumns(result, metaData, numCols);
			ArrayList<Object> row;
			int rowCounter = 0;
			while (results.next()) {
				row = new ArrayList<>();
				rowCounter++;
				for (i = 1; i <= numCols; i++) {
					row.add(results.getObject(i));
				}
				result.addRow(row);
			}
			}

			LOGGER.info(">>>>>>>JdbcDashboardDao ::: fetchReviewSnapshotData>>>>>>>> DBResult result: " + result.toJSON());
		} catch (SQLException se) {
			LOGGER.error("Error occurred in " + spName);
			LOGGER.error(se);
			LOGGER.error(se.getStackTrace());
		} finally {
			connManager.disconnect();
		}
		return result;
	}

	public Result fetchOwnerships(Map<String, Object> staticParameters) throws DataSourceConnectionException {
		
		DBResult ownershipResult = new DBResult();
		try(CallableStatement stmt = connManager.connection.prepareCall((String) staticParameters.get(ATTRIBUTE_SQL))){
			LOGGER.debug(">>>>>>>JdbcReciewAnalyticsDao ::: fetchOwnerships>>>>>>>> CallableStatement: " + stmt);
			try(ResultSet results = stmt.executeQuery();){
			LOGGER.info(">>>>>>>JdbcReciewAnalyticsDao ::: fetchOwnerships>>>>>>>> " + stmt + " >>>>>>>> execution complete...");
			
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			i = addColumns(ownershipResult, metaData, numCols);
			ArrayList<Object> row;
			i = addRows(ownershipResult, results, i, numCols);
			}
			LOGGER.info(">>>>>>>JdbcReciewAnalyticsDao ::: fetchOwnerships>>>>>>>> DBResult result: " + ownershipResult.toJSON());
			LOGGER.info(">>>>>>>JdbcReciewAnalyticsDao ::: Exiting fetchOwnerships>>>>>>>>");
		} catch (SQLException se) {
			LOGGER.error(ERROR_LOG );
			LOGGER.error(se);
			LOGGER.error(se.getStackTrace());
		} finally {
			connManager.disconnect();
		}
		return ownershipResult;
	}

	private int addRows(DBResult ownershipResult, ResultSet results, int i,
			int numCols) throws SQLException {
		ArrayList<Object> row;
		while (results.next()) {
			row = new ArrayList<>();
			for (i = 1; i <= numCols; i++) {
				row.add(results.getObject(i));
			}
			ownershipResult.addRow(row);
		}
		return i;
	}

	
}
