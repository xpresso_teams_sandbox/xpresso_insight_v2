package com.abzooba.trackR24.dao;

import org.apache.log4j.Logger;

public class FactoryDao {
	
	private FactoryDao() {}
	
	private static final Logger LOGGER = Logger.getLogger(FactoryDao.class);
	public static Object getInstance(String className){
		Object dao = null;
		try {
			Class<?> daoClass = Class.forName(className);
			dao = daoClass.newInstance();
		} catch (ClassNotFoundException|InstantiationException|IllegalAccessException e) {
			LOGGER.error(e.getMessage());
		} 
		return dao;
	}
}