package com.abzooba.trackR24.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.abzooba.trackR24.models.Result;
import com.abzooba.trackR24.models.UserInfo;
import com.abzooba.trackR24.models.UserInfoBean;

/**
* Author Name: Kunal Kashyap
* Create Date: 19-08-2016
* Last Modified: Kunal Kashyap: 23-09-2016
* Interface Insight:  This interface contains all the method declaration of methods of HibernateUserDao and JdbcUserDao.
*/
public interface IUserDao {

	public int addUserInfo(UserInfoBean userInfoBean, String adminEmail);

	public UserInfo fetchUserInfo(String adminEmail);

	public Result getStaticResults(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException;

	public List<String> loginUser(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException;

	public String getUpdatePassword(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException;

	public int registerUser(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException;

}