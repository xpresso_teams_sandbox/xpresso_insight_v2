package com.abzooba.trackR24.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

/**
* Author Name: Kunal Kashyap
* Create Date: 18-08-2016
* Last Modified: Kunal Kashyap: 16-09-2016
* Class Insight:  This class is responsible for establishing database connection using hibernate.
*/
public class GenericHibernateDao {
	private static  SessionFactory sessionFactory;
	private ServiceRegistry serviceRegistry = null;
	private static GenericHibernateDao singleInstance = new GenericHibernateDao();

	private GenericHibernateDao() {
		Configuration configuration = new Configuration().configure();
		serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
	            configuration.getProperties()).build();
		sessionFactory = configuration.buildSessionFactory(serviceRegistry);
	}

	public static Session getSession() {
		if(singleInstance == null){
			singleInstance = new GenericHibernateDao();
		}
		return sessionFactory.openSession();
	}

}
