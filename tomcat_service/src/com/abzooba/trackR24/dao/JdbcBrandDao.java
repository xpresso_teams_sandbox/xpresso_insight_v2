package com.abzooba.trackR24.dao;


import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.abzooba.trackR24.models.DBResult;
import com.abzooba.trackR24.models.Result;
import com.abzooba.trackR24.util.PropertyHandler;

import static com.abzooba.trackR24.util.Constants.*;

public class JdbcBrandDao extends GenericJdbcDao implements IBrandDao {

	private static final Logger LOGGER = Logger.getLogger(JdbcBrandDao.class);

	public Result fetchSummaryMatrics(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {

		LOGGER.info(CONST_RATINGDAO_ENTER_LOGGER_MESSAGE +"fetchSummaryMatrics");
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		callStmt = callStmt.append("{").append(CALL).append(spName).append(CONST_THREE_QUESTION_MARK);

			try(CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
			setInParamDurationPositionOne(dynamicParameters, stmt);
			setInParamSourceIdPositionTwo(dynamicParameters, stmt);
			setParamAccountIdPositionThree(dynamicParameters, stmt);
			
			try(ResultSet results = stmt.executeQuery();){
			LOGGER.debug(stmt);
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			i = setColumnNames(result, metaData, numCols);
			ArrayList<Object> row;
			i = addRowsLoop(result, results, i, numCols);
			}
			LOGGER.info(CONST_RATINGDAO_EXIT_LOGGER_MESSAGE+" fetchSummaryMatrics");
		} catch (SQLException se) {
			generatePrintLogMessages(spName, se);
		} finally {
			connManager.disconnect();
		}
		return result;
	}

	private int addRowsLoop(DBResult result, ResultSet results, int i,
			int numCols) throws SQLException {
		ArrayList<Object> row;
		while (results.next()) {
			row = new ArrayList<>();
			for (i = 1; i <= numCols; i++) {
				row.add(results.getObject(i));
			}
			result.addRow(row);
		}
		return i;
	}

	private void setInParamSourceIdPositionTwo(
			Map<String, Object> dynamicParameters, CallableStatement stmt)
			throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_SOURCE_ID) == null) {
			stmt.setNull(2, Types.INTEGER);
		} else {
			stmt.setInt(2, (Integer) dynamicParameters.get(SP_IN_PARAM_SOURCE_ID));
		}
	}

	private void setInParamDurationPositionOne(
			Map<String, Object> dynamicParameters, CallableStatement stmt)
			throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_DURATION) == null) {
			stmt.setNull(1, Types.INTEGER);
		} else {
			stmt.setInt(1, (Integer) dynamicParameters.get(SP_IN_PARAM_DURATION));
		}
	}

	public Result fetchBrandTimeline(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {

		LOGGER.info(CONST_RATINGDAO_ENTER_LOGGER_MESSAGE +"fetchBrandTimeline");
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		callStmt = callStmt.append("{").append(CALL).append(spName).append("(?,?)}");

		try(CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
			
			if (dynamicParameters.get(SP_IN_PARAM_SOURCE_ID) == null) {
				stmt.setNull(1, Types.INTEGER);
			} else {
				stmt.setInt(1, (Integer) dynamicParameters.get(SP_IN_PARAM_SOURCE_ID));
			}
			setParamAccountIdPositionTwo(dynamicParameters, stmt);
			
			try(ResultSet results = stmt.executeQuery();){
			LOGGER.debug(stmt);
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			i = setColumnNames(result, metaData, numCols);
			ArrayList<Object> row;
			i = addRowsLoop(result, results, i, numCols);
			}
			LOGGER.info(CONST_RATINGDAO_EXIT_LOGGER_MESSAGE+" fetchSummaryMatrics");
		} catch (SQLException se) {
			generatePrintLogMessages(spName, se);
		} finally {
			connManager.disconnect();
		}
		return result;
	}

	
	public Result fetchCompetitiveComparison(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {

		LOGGER.info(CONST_RATINGDAO_ENTER_LOGGER_MESSAGE + "fetchCompetitiveComparison");
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		
		callStmt = callStmt.append("{").append(CALL).append(spName).append(CONST_THREE_QUESTION_MARK);

		try(CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
			setInParamDurationPositionOne(dynamicParameters, stmt);
			setInParamSourceIdPositionTwo(dynamicParameters, stmt);
			setParamAccountIdPositionThree(dynamicParameters, stmt);
			
			try(ResultSet results = stmt.executeQuery();){
			LOGGER.debug(stmt);
			ResultSetMetaData metaData = results.getMetaData();
			int competitorRes;
			int numCols = metaData.getColumnCount();
			competitorRes = setColumnNames(result, metaData, numCols);
			ArrayList<Object> row;
			competitorRes = addRowsLoop(result, results, competitorRes, numCols);
			}
			LOGGER.info(CONST_RATINGDAO_EXIT_LOGGER_MESSAGE+" fetchCompetitiveComparison");
			}catch (SQLException se) {
			generatePrintLogMessages(spName, se);
		} finally {
			connManager.disconnect();
		}
		return result;
	}

	private void setParamAccountIdPositionThree(
			Map<String, Object> dynamicParameters, CallableStatement stmt)
			throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID) == null) {
			stmt.setNull(3, Types.INTEGER);
		} else {
			stmt.setInt(3, (Integer) dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID));
		}
	}
	
	public Result fetchSocialPostTweet(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters, String callingMethodFrom) throws DataSourceException {
		LOGGER.info(CONST_RATINGDAO_ENTER_LOGGER_MESSAGE +callingMethodFrom);
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		
		callStmt = callStmt.append(CONST_CALL_STRING_FOR_SP).append(spName).append("(?,?)}");

		try(CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
			setInParamDurationPositionOne(dynamicParameters, stmt);
			setParamAccountIdPositionTwo(dynamicParameters, stmt);
			
			try(ResultSet results = stmt.executeQuery();){
			LOGGER.debug(stmt);
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			i = setColumnNames(result, metaData, numCols);
			ArrayList<Object> row;
			i = addRowsLoop(result, results, i, numCols);
			}
			LOGGER.info(CONST_RATINGDAO_EXIT_LOGGER_MESSAGE+callingMethodFrom);
		} catch (SQLException se) {
			generatePrintLogMessages(spName, se);
		} finally {
			connManager.disconnect();
		}
		return result;
	}

	private void setParamAccountIdPositionTwo(
			Map<String, Object> dynamicParameters, CallableStatement stmt)
			throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID) == null) {
			stmt.setNull(2, Types.INTEGER);
		} else {
			stmt.setInt(2, (Integer) dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID));
		}
	}
	
	public Result fetchSummarySentimentChart(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters, String callingMethodFrom) throws DataSourceException {
		LOGGER.info(CONST_RATINGDAO_ENTER_LOGGER_MESSAGE +callingMethodFrom);
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		
		callStmt = callStmt.append(CONST_CALL_STRING_FOR_SP).append(spName).append("(?,?,STR_TO_DATE(?,'%d-%m-%Y %H:%i'),STR_TO_DATE(?,'%d-%m-%Y %H:%i'))}");
		
		try(CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
			setInParamAccountIdPositionOne(dynamicParameters, stmt);
			setInParamPostTypePositionTwo(dynamicParameters, stmt);
			setInParamFromDatePositionThree(dynamicParameters, stmt);
			setInParamToDatePositionFour(dynamicParameters, stmt);
			
			try(ResultSet results = stmt.executeQuery();){
			LOGGER.debug(stmt);
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			i = setColumnNames(result, metaData, numCols);
			ArrayList<Object> row;
			i = addRowsLoop(result, results, i, numCols);
			}
			LOGGER.info(CONST_RATINGDAO_EXIT_LOGGER_MESSAGE+callingMethodFrom);
		} catch (SQLException se) {
			generatePrintLogMessages(spName, se);
		} finally {
			connManager.disconnect();
		}
		return result;
	}

	private void setInParamToDatePositionFour(
			Map<String, Object> dynamicParameters, CallableStatement stmt)
			throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_TO_DT) == null || "".equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_TO_DT))) {
			stmt.setNull(4, Types.VARCHAR);
		} else {
			stmt.setString(4, (String) dynamicParameters.get(SP_IN_PARAM_TO_DT));
		}
	}

	private void setInParamFromDatePositionThree(
			Map<String, Object> dynamicParameters, CallableStatement stmt)
			throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_FROM_DT) == null || StringUtils.EMPTY.equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_FROM_DT))) {
			stmt.setNull(3, Types.VARCHAR);
		} else {
			stmt.setString(3, (String) dynamicParameters.get(SP_IN_PARAM_FROM_DT));
		}
	}

	private void setInParamPostTypePositionTwo(
			Map<String, Object> dynamicParameters, CallableStatement stmt)
			throws SQLException {
		setInParamPostTypePositionOne(dynamicParameters, stmt);
	}

	private void setInParamAccountIdPositionOne(
			Map<String, Object> dynamicParameters, CallableStatement stmt)
			throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID) == null) {
			stmt.setNull(1, Types.INTEGER);
		} else {
			stmt.setInt(1, (Integer) dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID));
		}
	}

	private int setColumnNames(DBResult result, ResultSetMetaData metaData,
			int numCols) throws SQLException {
		int i;
		for (i = 1; i <= numCols; i++) {
			result.addColumnName(metaData.getColumnLabel(i));
		}
		return i;
	}
	
	public Result fetchSummaryTopNegativeSentimentChart(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters, String callingMethodFrom) throws DataSourceException {
		LOGGER.info(CONST_RATINGDAO_ENTER_LOGGER_MESSAGE +callingMethodFrom);
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		
		callStmt = callStmt.append(CONST_CALL_STRING_FOR_SP).append(spName).append("(?,STR_TO_DATE(?,'%d-%m-%Y %H:%i'),STR_TO_DATE(?,'%d-%m-%Y %H:%i'))}");
		
		try(CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
			setInParamSource(dynamicParameters, stmt);
			setInParamFromDate(dynamicParameters, stmt);
			setInParamToDate(dynamicParameters, stmt);
			
			try(ResultSet results = stmt.executeQuery();){
			LOGGER.debug(stmt);
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			i = setColumnNames(result, metaData, numCols);
			ArrayList<Object> row;
			i = addRowsLoop(result, results, i, numCols);
			}
			LOGGER.info(CONST_RATINGDAO_EXIT_LOGGER_MESSAGE+callingMethodFrom);
		} catch (SQLException se) {
			generatePrintLogMessages(spName, se);
		} finally {
			connManager.disconnect();
		}
		return result;
	}

	private void generatePrintLogMessages(String spName, SQLException se) {
		LOGGER.error(ERROR_LOG + spName);
		LOGGER.error(se.getMessage());
		LOGGER.error(se.getStackTrace());
	}

	private void setInParamToDate(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_TO_DT) == null || StringUtils.EMPTY.equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_TO_DT))) {
			stmt.setNull(3, Types.VARCHAR);
		} else {
			stmt.setString(3, (String) dynamicParameters.get(SP_IN_PARAM_TO_DT));
		}
	}

	private void setInParamFromDate(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_FROM_DT) == null || StringUtils.EMPTY.equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_FROM_DT))) {
			stmt.setNull(2, Types.VARCHAR);
		} else {
			stmt.setString(2, (String) dynamicParameters.get(SP_IN_PARAM_FROM_DT));
		}
	}

	private void setInParamSource(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_SOURCE) == null || StringUtils.EMPTY.equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_SOURCE))) {
			stmt.setNull(1, Types.VARCHAR);
		} else {
			stmt.setString(1, (String) dynamicParameters.get(SP_IN_PARAM_SOURCE));
		}
	}
	
	public Result fetchTopNegativeAspectDeatilsModal(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters, String callingMethodFrom) throws DataSourceException {
		LOGGER.info(CONST_RATINGDAO_ENTER_LOGGER_MESSAGE +callingMethodFrom);
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		
		callStmt = callStmt.append(CONST_CALL_STRING_FOR_SP).append(spName).append("(?,STR_TO_DATE(?,'%d-%m-%Y %H:%i'),?)}");
		
		try(CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
			setInParamAspect(dynamicParameters, stmt);
			setInParamDueDate(dynamicParameters, stmt);
			setInParamSourceIdPositionThree(dynamicParameters, stmt);
			
			
			try(ResultSet results = stmt.executeQuery();){
			LOGGER.debug(stmt);
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			i = setColumnNames(result, metaData, numCols);
			ArrayList<Object> row;
			i = addRowsLoop(result, results, i, numCols);
			}
			LOGGER.info(CONST_RATINGDAO_EXIT_LOGGER_MESSAGE+callingMethodFrom);
		} catch (SQLException se) {
			generatePrintLogMessages(spName, se);
		} finally {
			connManager.disconnect();
		}
		return result;
	}

	private void setInParamSourceIdPositionThree(
			Map<String, Object> dynamicParameters, CallableStatement stmt)
			throws SQLException {
		if (dynamicParameters.get(SOURCE_ID) == null) {
			stmt.setNull(3, Types.INTEGER);
		} else {
			stmt.setInt(3, (int) dynamicParameters.get(SOURCE_ID));
		}
	}

	private void setInParamDueDate(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_DUE_DATE) == null || StringUtils.EMPTY.equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_DUE_DATE))) {
			stmt.setNull(2, Types.VARCHAR);
		} else {
			stmt.setString(2, (String) dynamicParameters.get(SP_IN_PARAM_DUE_DATE));
		}
	}

	private void setInParamAspect(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_ASPECT) == null || StringUtils.EMPTY.equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_ASPECT))) {
			stmt.setNull(1, Types.VARCHAR);
		} else {
			stmt.setString(1, (String) dynamicParameters.get(SP_IN_PARAM_ASPECT));
		}
	}
	
	public Result fetchSummarySentimentChartHourly(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters, String callingMethodFrom) throws DataSourceException {
		LOGGER.info(CONST_RATINGDAO_ENTER_LOGGER_MESSAGE +callingMethodFrom);
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		
		callStmt = callStmt.append(CONST_CALL_STRING_FOR_SP).append(spName).append("(?)}");
		
		try(CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
			
			setInParamSourcePositionOne(dynamicParameters, stmt);
			
			try(ResultSet results = stmt.executeQuery();){
			LOGGER.debug(stmt);
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			i = setColumnNames(result, metaData, numCols);
			ArrayList<Object> row;
			i = addRowsLoop(result, results, i, numCols);
			}
			LOGGER.info(CONST_RATINGDAO_EXIT_LOGGER_MESSAGE+callingMethodFrom);
		} catch (SQLException se) {
			generatePrintLogMessages(spName, se);
		} finally {
			connManager.disconnect();
		}
		return result;
	}

	private void setInParamSourcePositionOne(
			Map<String, Object> dynamicParameters, CallableStatement stmt)
			throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_SOURCE) == null || StringUtils.EMPTY.equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_SOURCE))) {
			stmt.setNull(1, Types.INTEGER);
		} else {
			stmt.setString(1, (String) dynamicParameters.get(SP_IN_PARAM_SOURCE));
		}
	}
	
	public Result fetchSummaryPostTypeCount(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters, String callingMethodFrom) throws DataSourceException {
		LOGGER.info(CONST_RATINGDAO_ENTER_LOGGER_MESSAGE +callingMethodFrom);
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		
		callStmt = callStmt.append(CONST_CALL_STRING_FOR_SP).append(spName).append("(?,?,?,?,STR_TO_DATE(?,'%d-%m-%Y %H:%i'),STR_TO_DATE(?,'%d-%m-%Y %H:%i'))}");
		
		try(CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
			setInParamAccountIdPositionOne(dynamicParameters, stmt);
			setInParamSourceIdPositionOne(dynamicParameters, stmt);
			setInParamSortBy(dynamicParameters, stmt);
			setInParamPostType(dynamicParameters, stmt);
			setInParamFromDatePositionFive(dynamicParameters, stmt);
			setInParamToDatePositionSix(dynamicParameters, stmt);
			
			try(ResultSet results = stmt.executeQuery();){
			LOGGER.debug(stmt);
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			i = setColumnNames(result, metaData, numCols);
			ArrayList<Object> row;
			i = addRowsLoop(result, results, i, numCols);
			}
			LOGGER.info(CONST_RATINGDAO_EXIT_LOGGER_MESSAGE+callingMethodFrom);
		} catch (SQLException se) {
			generatePrintLogMessages(spName, se);
		} finally {
			connManager.disconnect();
		}
		return result;
	}

	private void setInParamToDatePositionSix(
			Map<String, Object> dynamicParameters, CallableStatement stmt)
			throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_TO_DT) == null || StringUtils.EMPTY.equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_TO_DT))) {
			stmt.setNull(6, Types.VARCHAR);
		} else {
			stmt.setString(6, (String) dynamicParameters.get(SP_IN_PARAM_TO_DT));
		}
	}

	private void setInParamFromDatePositionFive(
			Map<String, Object> dynamicParameters, CallableStatement stmt)
			throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_FROM_DT) == null || StringUtils.EMPTY.equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_FROM_DT))) {
			stmt.setNull(5, Types.VARCHAR);
		} else {
			stmt.setString(5, (String) dynamicParameters.get(SP_IN_PARAM_FROM_DT));
		}
	}

	private void setInParamPostType(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_POST_TYPE_ID) == null || StringUtils.EMPTY.equalsIgnoreCase((String) dynamicParameters.get(SP_IN_POST_TYPE_ID))) {
			stmt.setNull(4, Types.VARCHAR);
		} else {
			stmt.setString(4, (String) dynamicParameters.get(SP_IN_POST_TYPE_ID));
		}
	}

	private void setInParamSortBy(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_SORT) == null || StringUtils.EMPTY.equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_SORT))) {
			stmt.setNull(3, Types.VARCHAR);
		} else {
			stmt.setString(3, (String) dynamicParameters.get(SP_IN_PARAM_SORT));
		}
	}

	private void setInParamSourceIdPositionOne(
			Map<String, Object> dynamicParameters, CallableStatement stmt)
			throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_SOURCE_ID) == null || StringUtils.EMPTY.equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_SOURCE_ID))) {
			stmt.setNull(2, Types.VARCHAR);
		} else {
			stmt.setString(2, (String) dynamicParameters.get(SP_IN_PARAM_SOURCE_ID));
		}
	}
	
	public Result fetchSummarySentimentDetails(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters, String callingMethodFrom) throws DataSourceException {
		LOGGER.info(CONST_RATINGDAO_ENTER_LOGGER_MESSAGE +callingMethodFrom);
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		
		callStmt = callStmt.append(CONST_CALL_STRING_FOR_SP).append(spName).append("(?,?,?,STR_TO_DATE(?,'%d-%m-%Y %H:%i'),STR_TO_DATE(?,'%d-%m-%Y %H:%i'),?)}");
		
		try(CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
			setInParamAccountIdPositionOne(dynamicParameters, stmt);
			setInParamPostTypePositionOne(dynamicParameters, stmt);
			setInParamSentiment(dynamicParameters, stmt);
			setInParamFromDatePositionFour(dynamicParameters, stmt);
			setInParamToDatePositionFive(dynamicParameters, stmt);
			setInParamSourcePositionSix(dynamicParameters, stmt);
			
			//
			
			try(ResultSet results = stmt.executeQuery();){
			LOGGER.debug(stmt);
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			i = setColumnNames(result, metaData, numCols);
			ArrayList<Object> row;
			i = addRowsLoop(result, results, i, numCols);
			}
			LOGGER.info(CONST_RATINGDAO_EXIT_LOGGER_MESSAGE+callingMethodFrom);
		} catch (SQLException se) {
			generatePrintLogMessages(spName, se);
		} finally {
			connManager.disconnect();
		}
		return result;
	}

	private void setInParamSourcePositionSix(
			Map<String, Object> dynamicParameters, CallableStatement stmt)
			throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_SOURCE) == null || StringUtils.EMPTY.equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_SOURCE))) {
			stmt.setNull(6, Types.VARCHAR);
		} else {
			stmt.setString(6, (String) dynamicParameters.get(SP_IN_PARAM_SOURCE));
		}
	}

	private void setInParamToDatePositionFive(
			Map<String, Object> dynamicParameters, CallableStatement stmt)
			throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_TO_DT) == null || StringUtils.EMPTY.equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_TO_DT))) {
			stmt.setNull(5, Types.VARCHAR);
		} else {
			stmt.setString(5, (String) dynamicParameters.get(SP_IN_PARAM_TO_DT));
		}
	}

	private void setInParamFromDatePositionFour(
			Map<String, Object> dynamicParameters, CallableStatement stmt)
			throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_FROM_DT) == null || StringUtils.EMPTY.equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_FROM_DT))) {
			stmt.setNull(4, Types.VARCHAR);
		} else {
			stmt.setString(4, (String) dynamicParameters.get(SP_IN_PARAM_FROM_DT));
		}
	}

	private void setInParamSentiment(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_SENTIMENT_TYPE) == null || StringUtils.EMPTY.equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_SENTIMENT_TYPE))) {
			stmt.setNull(3, Types.VARCHAR);
		} else {
			stmt.setString(3, (String) dynamicParameters.get(SP_IN_PARAM_SENTIMENT_TYPE));
		}
	}

	private void setInParamPostTypePositionOne(
			Map<String, Object> dynamicParameters, CallableStatement stmt)
			throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_POST_TYPE) == null || StringUtils.EMPTY.equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_POST_TYPE))) {
			stmt.setNull(2, Types.VARCHAR);
		} else {
			stmt.setString(2, (String) dynamicParameters.get(SP_IN_PARAM_POST_TYPE));
		}
	}
	
	public Result fetchSummaryPostTweetDetails(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters, String callingMethodFrom) throws DataSourceException {
		LOGGER.info(CONST_RATINGDAO_ENTER_LOGGER_MESSAGE +callingMethodFrom+dynamicParameters.get(SP_IN_PARAM_POST_ID));
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		
		callStmt = callStmt.append(CONST_CALL_STRING_FOR_SP).append(spName).append(CONST_THREE_QUESTION_MARK);

		try(CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
			setInParamAccountIdPositionOne(dynamicParameters, stmt);
			setInParamPostId(dynamicParameters, stmt);
			setInParamSourcePositionThree(dynamicParameters, stmt);
			
			try(ResultSet results = stmt.executeQuery();){
			LOGGER.debug(stmt);
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			i = addColumnNames(result, metaData, numCols);
			ArrayList<Object> row;
			while (results.next()) {
				row = new ArrayList<>();
				i = addRows(results, numCols, row);
				result.addRow(row);
			}
			}
			LOGGER.info(CONST_RATINGDAO_EXIT_LOGGER_MESSAGE+callingMethodFrom);
		} catch (SQLException se) {
			generatePrintLogMessages(spName, se);
		} finally {
			connManager.disconnect();
		}
		return result;
	}

	private int addRows(ResultSet results, int numCols, ArrayList<Object> row)
			throws SQLException {
		int i;
		for (i = 1; i < numCols; i++) {
			row.add(results.getObject(i));
		}
		return i;
	}

	private void setInParamSourcePositionThree(
			Map<String, Object> dynamicParameters, CallableStatement stmt)
			throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_SOURCE) == null || StringUtils.EMPTY.equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_SOURCE))) {
			stmt.setNull(3, Types.VARCHAR);
		} else {
			stmt.setString(3, (String) dynamicParameters.get(SP_IN_PARAM_SOURCE));
		}
	}

	private void setInParamPostId(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_POST_ID) == null || StringUtils.EMPTY.equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_POST_ID))) {
			stmt.setNull(2, Types.VARCHAR);
		} else {
			stmt.setString(2, (String) dynamicParameters.get(SP_IN_PARAM_POST_ID));
		}
	}
	
	public Result fetchSummaryPostTweetDetailsLimit(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters, String callingMethodFrom) throws DataSourceException {
		LOGGER.info(CONST_RATINGDAO_ENTER_LOGGER_MESSAGE +callingMethodFrom+dynamicParameters.get(SP_IN_PARAM_POST_ID));
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		
		callStmt = callStmt.append(CONST_CALL_STRING_FOR_SP).append(spName).append("(?,?,?,?,?)}");

		try(CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
			setInParamAccountIdPositionOne(dynamicParameters, stmt);
			setInParamPostId(dynamicParameters, stmt);
			setInParamSourcePositionThree(dynamicParameters, stmt);
			setInParamOffset(dynamicParameters, stmt);
			setInParamLimit(dynamicParameters, stmt);
			
			try(ResultSet results = stmt.executeQuery();){
			LOGGER.debug(stmt);
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			i = addColumnNames(result, metaData, numCols);
			ArrayList<Object> row;
			while (results.next()) {
				row = new ArrayList<>();
				i = addRows(results, numCols, row);
				result.addRow(row);
			}
			}
			LOGGER.info(CONST_RATINGDAO_EXIT_LOGGER_MESSAGE+callingMethodFrom);
		} catch (SQLException se) {
			generatePrintLogMessages(spName, se);
		} finally {
			connManager.disconnect();
		}
		return result;
	}

	private void setInParamLimit(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_LIMIT) == null) {
			stmt.setNull(5, Types.INTEGER);
		} else {
			stmt.setInt(5, (Integer) dynamicParameters.get(SP_IN_PARAM_LIMIT));
		}
	}

	private void setInParamOffset(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_OFFSET) == null) {
			stmt.setNull(4, Types.INTEGER);
		} else {
			stmt.setInt(4, (Integer) dynamicParameters.get(SP_IN_PARAM_OFFSET));
		}
	}
	
	public Result fetchDMDetailedChain(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters, String callingMethodFrom) throws DataSourceException {
		LOGGER.info(CONST_RATINGDAO_ENTER_LOGGER_MESSAGE +callingMethodFrom);
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		
		callStmt = callStmt.append(CONST_CALL_STRING_FOR_SP).append(spName).append("(?,?,?,STR_TO_DATE(?,'%d-%m-%Y %H:%i'),STR_TO_DATE(?,'%d-%m-%Y %H:%i'))}");

		try(CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
			setInParamAccountIdPositionOne(dynamicParameters, stmt);
			setInParamSourcePositionTwo(dynamicParameters, stmt);
			setInParamUserName(dynamicParameters, stmt);
			setInParamFromDatePositionFour(dynamicParameters, stmt);
			setInParamToDatePositionFive(dynamicParameters, stmt);
			
			try(ResultSet results = stmt.executeQuery();){
			LOGGER.debug(stmt);
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			i = setColumnNames(result, metaData, numCols);
			ArrayList<Object> row;
			i = addRowsLoop(result, results, i, numCols);
			}
			LOGGER.info(CONST_RATINGDAO_EXIT_LOGGER_MESSAGE+callingMethodFrom);
		} catch (SQLException se) {
			generatePrintLogMessages(spName, se);
		} finally {
			connManager.disconnect();
		}
		return result;
	}

	private void setInParamUserName(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_USER_NAME) == null || "".equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_USER_NAME))) {
			stmt.setNull(3, Types.VARCHAR);
		} else {
			stmt.setString(3, (String) dynamicParameters.get(SP_IN_PARAM_USER_NAME));
		}
	}

	private void setInParamSourcePositionTwo(
			Map<String, Object> dynamicParameters, CallableStatement stmt)
			throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_SOURCE) == null || "".equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_SOURCE))) {
			stmt.setNull(2, Types.VARCHAR);
		} else {
			stmt.setString(2, (String) dynamicParameters.get(SP_IN_PARAM_SOURCE));
		}
	}

	
	public Result fetchSummaryPostTweet(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters, String callingMethodFrom) throws DataSourceException {
		LOGGER.info(CONST_RATINGDAO_ENTER_LOGGER_MESSAGE +callingMethodFrom);
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		
		callStmt = callStmt.append(CONST_CALL_STRING_FOR_SP).append(spName).append("(?,STR_TO_DATE(?,'%d-%m-%Y %H:%i'),STR_TO_DATE(?,'%d-%m-%Y %H:%i'),?,?)}");

		try(CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
			setInParamAccountIdPositionOne(dynamicParameters, stmt);
			setInParamFromDate(dynamicParameters, stmt);
			setInParamToDate(dynamicParameters, stmt);
			setInParamSearchStringPositionFour(dynamicParameters, stmt);
			setInParamSortBYPositionFive(dynamicParameters, stmt);
			
			try(ResultSet results = stmt.executeQuery();){
			LOGGER.debug(stmt);
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			i = addColumnNames(result, metaData, numCols);
			ArrayList<Object> row;
			while (results.next()) {
				row = new ArrayList<>();
				i = addRows(results, numCols, row);
				result.addRow(row);
			}
			}
			LOGGER.info(CONST_RATINGDAO_EXIT_LOGGER_MESSAGE+callingMethodFrom);
		} catch (SQLException se) {
			generatePrintLogMessages(spName, se);
		} finally {
			connManager.disconnect();
		}
		return result;
	}

	private int addColumnNames(DBResult result, ResultSetMetaData metaData,
			int numCols) throws SQLException {
		int i;
		for (i = 1; i < numCols; i++) {
			result.addColumnName(metaData.getColumnLabel(i));
		}
		return i;
	}

	private void setInParamSortBYPositionFive(
			Map<String, Object> dynamicParameters, CallableStatement stmt)
			throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_SORT) == null || StringUtils.EMPTY.equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_SORT))) {
			stmt.setNull(5, Types.VARCHAR);
		} else {
			stmt.setString(5, (String) dynamicParameters.get(SP_IN_PARAM_SORT));
		}
	}

	private void setInParamSearchStringPositionFour(
			Map<String, Object> dynamicParameters, CallableStatement stmt)
			throws SQLException {
		if (dynamicParameters.get(SP_IN_SEARCH_STRING) == null || StringUtils.EMPTY.equalsIgnoreCase((String) dynamicParameters.get(SP_IN_SEARCH_STRING))) {
			stmt.setNull(4, Types.VARCHAR);
		} else {
			stmt.setString(4, (String) dynamicParameters.get(SP_IN_SEARCH_STRING));
		}
	}

	public Result fetchDirectMessageUserList(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters, String callingMethodFrom) throws DataSourceException {
		LOGGER.info(CONST_RATINGDAO_ENTER_LOGGER_MESSAGE +callingMethodFrom);
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		
		callStmt = callStmt.append(CONST_CALL_STRING_FOR_SP).append(spName).append("(?,?,STR_TO_DATE(?,'%d-%m-%Y %H:%i'),STR_TO_DATE(?,'%d-%m-%Y %H:%i'))}");

		try(CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
			setInParamAccountIdPositionOne(dynamicParameters, stmt);
			setInParamSourcePositionTwo(dynamicParameters, stmt);
			setInParamFromDatePositionThree(dynamicParameters, stmt);
			setInParamToDatePositionFour(dynamicParameters, stmt);
			
			try(ResultSet results = stmt.executeQuery();){
			LOGGER.debug(stmt);
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			i = setColumnNames(result, metaData, numCols);
			ArrayList<Object> row;
			i = addRowsLoop(result, results, i, numCols);
			}
			LOGGER.info(CONST_RATINGDAO_EXIT_LOGGER_MESSAGE+callingMethodFrom);
		} catch (SQLException se) {
			generatePrintLogMessages(spName, se);
		} finally {
			connManager.disconnect();
		}
		return result;
	}

	public Result fetchWordCloudDataArray(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: Entering fetchWordCloudDataArray>>>>>>>>");
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		
			callStmt = callStmt.append(CONST_CALL_STRING_FOR_SP).append(spName).append("(?,?,?)}");
			try(CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
			setInParamSelectedDuration(dynamicParameters, stmt);
			setParamAccountIdPositionThree(dynamicParameters, stmt);
			if (dynamicParameters.get(SP_IN_PARAM_SOURCE) == null) {
				stmt.setNull(2, Types.INTEGER);
			} else {
				stmt.setInt(2, (Integer) dynamicParameters.get(SP_IN_PARAM_SOURCE));
			}
			try(ResultSet results = stmt.executeQuery();){
			LOGGER.debug(stmt);
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			i = setColumnNames(result, metaData, numCols);
			ArrayList<Object> row;
			i = addRowsLoop(result, results, i, numCols);
			}
			LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: Exiting fetchAnalyticsKPIs>>>>>>>>");
		} catch (SQLException se) {
			generatePrintLogMessages(spName, se);
		} finally {
			connManager.disconnect();
		}
		return result;

	}

	private void setInParamSelectedDuration(
			Map<String, Object> dynamicParameters, CallableStatement stmt)
			throws SQLException {
		if (dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_SELECTED_DURATION")) == null) {
			stmt.setNull(1, Types.INTEGER);
		} else {
			stmt.setInt(1, (Integer) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_SELECTED_DURATION")));
		}
	}

	

	
}
