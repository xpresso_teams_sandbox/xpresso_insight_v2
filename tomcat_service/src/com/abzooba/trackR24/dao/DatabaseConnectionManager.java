package com.abzooba.trackR24.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.abzooba.trackR24.models.DBResult;
import com.abzooba.trackR24.models.Result;
import com.abzooba.trackR24.util.PropertyHandler;
import static com.abzooba.trackR24.util.Constants.*;

public class DatabaseConnectionManager {

	private static final Logger LOGGER = Logger.getLogger(DatabaseConnectionManager.class);

	protected Connection connection;
	protected DataSource dataSource;
	CallableStatement st;
	ResultSet results;
	PreparedStatement pst;

	public DatabaseConnectionManager() {
		try {
			Context initContext = new InitialContext();
			Context envContext = (Context) initContext.lookup("java:/comp/env");
			dataSource = (DataSource) envContext.lookup("jdbc/db_name");

		} catch (Exception ioe) {
			LOGGER.error("Error occurred while loading properties file");
			LOGGER.error(ioe.getMessage());
			LOGGER.error(ioe.getStackTrace());
		}
	}

	public void connect() {
		try {
			connection = dataSource.getConnection();
		} catch (Exception exc) {
			LOGGER.error(exc.getMessage());
		}
	}

	public void disconnect() throws DataSourceConnectionException {
		try {
			connection.close();
		} catch (Exception exc) {
			throw new DataSourceConnectionException(exc.getMessage());
		}
	}

	public Result getResults(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		LOGGER.info(" Printing spName :" + spName  + dynamicParameters.get("inCompanyId"));
		try {
			st = prepareCallableStatement(spName, dynamicParameters);
			DBResult result = new DBResult();
			LOGGER.debug(st);
			results = st.executeQuery();
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			for (i = 1; i <= numCols; i++) {
				if (!SP_BRAND_COMPARISON_MONTHLY.equalsIgnoreCase(spName)) {
					result.addColumnName(metaData.getColumnLabel(i));
				} else {
					checkNumCols(result, metaData, i, numCols);
				}
			}
			ArrayList<Object> row;
			while (results.next()) {
				row = new ArrayList<>();
				for (i = 1; i <= numCols; i++) {
					if (!SP_BRAND_COMPARISON_MONTHLY.equalsIgnoreCase(spName)) {
						row.add(results.getObject(i));
					} else {
						checknumCols(i, numCols, row);
					}
				}
				result.addRow(row);
			}
			return result;
		} catch (Exception exc) {
			LOGGER.error(ERROR_LOG + spName);
			LOGGER.error(exc.getMessage());
			LOGGER.error(exc.getStackTrace());
			throw new DataSourceException(exc.getMessage());
		} finally {
			try {
				results.close();
				st.close();
			} catch (SQLException e) {
				LOGGER.error(e.getMessage());
			}

		}
	}

	private void checknumCols(int i, int numCols, ArrayList<Object> row)
			throws SQLException {
		if (i != numCols) {
			row.add(results.getObject(i));
		}
	}

	private void checkNumCols(DBResult result, ResultSetMetaData metaData,
			int i, int numCols) throws SQLException {
		if (i != numCols) {
			result.addColumnName(metaData.getColumnLabel(i));
		}
	}

	protected CallableStatement prepareCallableStatement(String spName, Map<String, Object> params) throws SQLException {
		LOGGER.info("Param inCompanyId: " + params.get("inCompanyId"));
		DatabaseMetaData dbMetaData = null;
		HashMap<String, Integer> paramInfo = new HashMap<>();
		if(connection == null || connection.isClosed()) {
			 connect();
		}
		dbMetaData = connection.getMetaData();
		getParamInfo(spName, dbMetaData, paramInfo);
			
		StringBuilder call = new StringBuilder();
		call.append("{").append(CALL).append(spName).append("(");
		for (int i = 0; i < paramInfo.size(); i++)
			call.append("?,");
		checkParamInfoSize(paramInfo, call);
			
		
		LOGGER.info("Print Call : " + call);
		st = connection.prepareCall(call.toString());
		for(String paramName : paramInfo.keySet()) {
			int paramType = paramInfo.get(paramName);
			Object paramVal = params.get(paramName);
			checkSwitchCase(paramName, paramType, paramVal);
		}
		return st;

	}

	private void getParamInfo(String spName, DatabaseMetaData dbMetaData,
			HashMap<String, Integer> paramInfo) throws SQLException {
		if (dbMetaData != null) {
			ResultSet rs = dbMetaData.getProcedureColumns(null, null, spName.toUpperCase(), "%");
			while (rs.next())
				paramInfo.put(rs.getString(4), rs.getInt(6));
			rs.close();
		}
	}

	private void checkSwitchCase(String paramName, int paramType,
			Object paramVal) throws SQLException {
		if (paramVal != null) {
			switch (paramType) {
				case Types.DATE:
					st.setDate(paramName, (java.sql.Date) paramVal);
					break;
				case Types.FLOAT:
					st.setFloat(paramName, (Float) paramVal);
					break;
				case Types.BIGINT:
					st.setLong(paramName, (Long) paramVal);
					break;
				case Types.INTEGER:
					st.setInt(paramName, (Integer) paramVal);
					break;
				case Types.SMALLINT:
					st.setShort(paramName, (Short) paramVal);
					break;
				case Types.VARCHAR:
					st.setString(paramName, (String) paramVal);
					break;
				case Types.NVARCHAR:
					st.setString(paramName, (String) paramVal);
					break;
				default:
					st.setString(paramName, (String) paramVal);
					break;

			}
		} else {
			switch (paramType) {
				case Types.DATE:
					st.setDate(paramName, new java.sql.Date(0));
					break;
				case Types.FLOAT:
					st.setFloat(paramName, 0f);
					break;
				case Types.BIGINT:
					st.setLong(paramName, 0l);
					break;
				case Types.INTEGER:
					st.setNull(paramName, Types.INTEGER);
					break;
				case Types.SMALLINT:
					st.setShort(paramName, (short) 0);
					break;
				case Types.VARCHAR:
					st.setNull(paramName, Types.VARCHAR);
					break;
				case Types.NVARCHAR:
				default:
					st.setString(paramName, null);
					break;

			}
		}
	}

	private void checkParamInfoSize(HashMap<String, Integer> paramInfo,
			StringBuilder call) {
		if (paramInfo.size() > 0)
			call.replace(call.length()-1, call.length(), ")}");
	}


	public Result getSourceTopicResultSet(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		LOGGER.info(">>>>>>>DatabaseConnectionManager ::: Entering getSourceTopicResultSet>>>>>>>>");
		try {
			pst = connection.prepareStatement((staticParameters.get(ATTRIBUTE_SQL)).toString());
			if (dynamicParameters != null) {
				pst.setInt(1, (Integer) dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID));
			}
			
			DBResult result = new DBResult();
			LOGGER.debug(">>>>>>>DatabaseConnectionManager ::: getSourceTopicResultSet>>>>>>>> prepareStatement: " + pst);
			results = pst.executeQuery();
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			for (i = 1; i <= numCols; i++) {
				result.addColumnName(metaData.getColumnLabel(i));
			}
			ArrayList<Object> row;
			while (results.next()) {
				row = new ArrayList<>();
				for (i = 1; i <= numCols; i++) {
					row.add(results.getObject(i));
				}
				result.addRow(row);
			}
			LOGGER.info(">>>>>>>DatabaseConnectionManager ::: getSourceTopicResultSet>>>>>>>> DBResult result: " + result.toJSON());
			return result;
		} catch (Exception exc) {
			LOGGER.error(ERROR_LOG + (staticParameters.get(ATTRIBUTE_SQL)).toString());
			LOGGER.error(exc);
			LOGGER.error(exc.getStackTrace());
			throw new DataSourceException(exc.getMessage());
		} finally {
			try {
				LOGGER.info(">>>>>>>DatabaseConnectionManager ::: getSourceTopicResultSet ::: Closing resultSet and Statement");
				results.close();
				pst.close();
			} catch (SQLException e) {
				LOGGER.error(e);
			}
			LOGGER.info(">>>>>>>DatabaseConnectionManager ::: Exiting getSourceTopicResultSet>>>>>>>>");
		}
	}

	public Result getWordCloudData(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		String sql =  staticParameters.get(ATTRIBUTE_SQL).toString();
		try {
			pst = connection.prepareStatement(staticParameters.get(ATTRIBUTE_SQL).toString());
			if (dynamicParameters != null) {
				pst.setInt(1, (Integer) dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID));
			}
			DBResult result = new DBResult();
			results = pst.executeQuery();
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			for (i = 1; i <= numCols; i++) {
				result.addColumnName(metaData.getColumnLabel(i));
			}
			ArrayList<Object> row;
			while (results.next()) {
				row = new ArrayList<>();
				for (i = 1; i <= numCols; i++) {
					row.add(results.getObject(i));
				}
				result.addRow(row);
			}
			return result;
		} catch (Exception exc) {
			LOGGER.error(ERROR_LOG + sql);
			LOGGER.error(exc.getMessage());
			LOGGER.error(exc.getStackTrace());
			throw new DataSourceException(exc.getMessage());
		} finally {
			try {
				results.close();
				pst.close();
			} catch (SQLException e) {
				LOGGER.error(e.getMessage());
			}

		}
	}

	public Integer insertUserData(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		CallableStatement stmt = null;
		int success = 0;
		StringBuilder callStmt = new StringBuilder();
		try {

			callStmt = callStmt.append("{call ").append(spName).append("(?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			stmt = connection.prepareCall(callStmt.toString());

			stmt.setString(1, (String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_USER_ID")));
			stmt.setString(2, (String) dynamicParameters.get(SP_IN_PARAM_USER_NAME));
			stmt.setString(3, (String) dynamicParameters.get(SP_IN_PARAM_PASSWORD));
			stmt.setString(4, (String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_COMPANY_NAME")));
			if (dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_DESIGNATION")) == null) {
				stmt.setNull(5, Types.VARCHAR);
			} else {
				stmt.setString(5, (String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_DESIGNATION")));
			}
			if (dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_COUNTRY_ID")) == null) {
				stmt.setNull(6, Types.INTEGER);
			} else {
				stmt.setInt(6, (Integer) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_COUNTRY_ID")));
			}
			if (dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_DOMAIN_ID")) == null) {
				stmt.setNull(7, Types.INTEGER);
			} else {
				stmt.setInt(7, (Integer) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_DOMAIN_ID")));
			}
			if (dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_TIMEZONE_ID")) == null) {
				stmt.setNull(8, Types.INTEGER);
			} else {
				stmt.setInt(8, (Integer) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_TIMEZONE_ID")));
			}

			stmt.setInt(9, (Integer) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_ROLE_ID")));
			stmt.setInt(10, (Integer) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_CONFIRMED_INDEX")));
			stmt.setString(11, (String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_USER_TOKEN")));

			if (dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_ACCOUNT_NAME")) == null) {
				stmt.setNull(12, Types.VARCHAR);
			} else {
				stmt.setString(12, (String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_ACCOUNT_NAME")));
			}
			if (dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_EXPIRY_INTERVAL")) == null) {
				stmt.setNull(13, Types.VARCHAR);
			} else {
				stmt.setString(13, (String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_EXPIRY_INTERVAL")));
			}
			try(ResultSet result = stmt.executeQuery();){
			if (result.next())
				success = result.getInt(1);
			LOGGER.debug(stmt);
			}
			return success;
		} catch (Exception exc) {
			LOGGER.error(ERROR_LOG + spName);
			LOGGER.error(exc.getMessage());
			LOGGER.error(exc.getStackTrace());
			throw new DataSourceException(exc.getMessage());
		} finally {
			try {
				stmt.close();
			} catch (SQLException e) {
				LOGGER.error(e.getMessage());
			}

		}

	}

	public List<String> getUserPrimaryDetails(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		try {
			ArrayList<String> loginDetails = new ArrayList<>();
			st = prepareCallableStatement(spName, dynamicParameters);
			if (spName.equalsIgnoreCase(PropertyHandler.getPropertyValue("SP_CHECK_LOGIN"))) {
				st.setString(1, (String) dynamicParameters.get(SP_IN_PARAM_USER_NAME));
				st.setString(2, (String) dynamicParameters.get(SP_IN_PARAM_PASSWORD));
			} else if (spName.equalsIgnoreCase(PropertyHandler.getPropertyValue("SP_CHECK_MAIL_CONF"))) {
				st.setString(1, (String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_KEY")));
			}

			results = st.executeQuery();
			ResultSetMetaData metaData = results.getMetaData();
			int numCols = metaData.getColumnCount();
			while (results.next()) {
				for (int i = 1; i <= numCols; i++) {
					loginDetails.add(results.getString(i));
				}
			}
			return loginDetails;
		} catch (Exception exc) {
		
			LOGGER.error(ERROR_LOG + spName);
			LOGGER.error(exc.getMessage());
			LOGGER.error(exc.getStackTrace());
			throw new DataSourceException(exc.getMessage());
		} finally {
			try {
				results.close();
				st.close();
			} catch (SQLException e) {
				LOGGER.error(e.getMessage());
			}
		}

	}

	public String getUserpassword(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		String password = null;
		String name = null;
		try {
			pst = connection.prepareStatement(staticParameters.get(ATTRIBUTE_SQL).toString());
			if (dynamicParameters != null) {
				pst.setString(1, (String) dynamicParameters.get(SP_IN_PARAM_USER_NAME));
			}
			LOGGER.debug(staticParameters.get(ATTRIBUTE_SQL).toString());
			try(ResultSet rs = pst.executeQuery();){
			if (rs.next()) {
				name = rs.getString(1);
				password = rs.getString(2);
			}
			}
			String data = "";
			if(name!=null)
				data=name.concat("$").concat(password);
			
			return data;
		} catch (Exception exc) {
			LOGGER.error(ERROR_LOG );
			LOGGER.error(exc.getMessage());
			LOGGER.error(exc.getStackTrace());
			throw new DataSourceException(exc.getMessage());
		} finally {
			try {
				pst.close();
			} catch (SQLException e) {
				LOGGER.error(e.getMessage());
			}

		}

	}

	public Integer updateUserpassword(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		int success = 0;
		StringBuilder callStmt = new StringBuilder();

			callStmt = callStmt.append("{call ").append(spName).append("(?,?,?)}");
			try(CallableStatement stmt = connection.prepareCall(callStmt.toString());){

			stmt.setString(1, (String) dynamicParameters.get(SP_IN_PARAM_USER_NAME));
			stmt.setString(2, (String) dynamicParameters.get(SP_IN_PARAM_OLD_PASSWORD));
			stmt.setString(3, (String) dynamicParameters.get(SP_IN_PARAM_PASSWORD));

			success = stmt.executeUpdate();
			LOGGER.debug(stmt);
			return success;
		} catch (Exception exc) {
			LOGGER.error(ERROR_LOG + spName);
			LOGGER.error(exc.getMessage());
			LOGGER.error(exc.getStackTrace());
			throw new DataSourceException(exc.getMessage());
		}
	}
	
	public Result getResultsForNew(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		try {
			st = prepareCallableStatement(spName, dynamicParameters);
			DBResult result = new DBResult();
			LOGGER.debug(st);
			results = st.executeQuery();
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			for (i = 1; i <= numCols; i++) {
				if (!SP_BRAND_COMPARISON_MONTHLY.equalsIgnoreCase(spName)) {
					result.addColumnName(metaData.getColumnLabel(i));
				} else {
					checkNumCols(result, metaData, i, numCols);
				}
			}
			ArrayList<Object> row;
			while (results.next()) {
				row = new ArrayList<>();
				for (i = 1; i <= numCols; i++) {
					if (!SP_BRAND_COMPARISON_MONTHLY.equalsIgnoreCase(spName)) {
						row.add(results.getObject(i));
					} else {
						checknumCols(i, numCols, row);
					}
				}
				result.addRow(row);
			}
			return result;
		} catch (Exception exc) {
			LOGGER.error(ERROR_LOG + spName);
			LOGGER.error(exc.getMessage());
			LOGGER.error(exc.getStackTrace());
			throw new DataSourceException(exc.getMessage());
		} finally {
			try {
				results.close();
				st.close();
			} catch (SQLException e) {
				LOGGER.error(e.getMessage());
			}

		}
	}

	public Result getHeadlinesResultSet(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		LOGGER.info(">>>>>>>DatabaseConnectionManager ::: Entering getHeadlinesResultSet>>>>>>>>");
		
		try {
			pst = connection.prepareStatement(staticParameters.get(ATTRIBUTE_SQL).toString());
			if (dynamicParameters != null) {
				pst.setInt(1, (Integer) dynamicParameters.get(SP_IN_PARAM_COMPANY_ID));
			}
			DBResult result = new DBResult();
			LOGGER.debug(">>>>>>>DatabaseConnectionManager ::: getHeadlinesResultSet>>>>>>>> prepareStatement: " + pst);
			results = pst.executeQuery();
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			for (i = 1; i <= numCols; i++) {
				result.addColumnName(metaData.getColumnLabel(i));
			}
			ArrayList<Object> row;
			while (results.next()) {
				row = new ArrayList<>();
				for (i = 1; i <= numCols; i++) {
					row.add(results.getObject(i));
				}
				result.addRow(row);
			}
			LOGGER.info(">>>>>>>DatabaseConnectionManager ::: getHeadlinesResultSet>>>>>>>> DBResult result: " + result.toJSON());
			return result;
		} catch (Exception exc) {
			LOGGER.error(ERROR_LOG + staticParameters.get(ATTRIBUTE_SQL).toString());
			LOGGER.error(exc);
			LOGGER.error(exc.getStackTrace());
			throw new DataSourceException(exc.getMessage());
		} finally {
			try {
				LOGGER.info(">>>>>>>DatabaseConnectionManager ::: getHeadlinesResultSet ::: Closing resultSet and Statement");
				results.close();
				pst.close();
			} catch (SQLException e) {
				LOGGER.error(e);
			}
			LOGGER.info(">>>>>>>DatabaseConnectionManager ::: Exiting getHeadlinesResultSet>>>>>>>>");
		}
	}

	public Result getDurationsResultSet(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		LOGGER.info(">>>>>>>DatabaseConnectionManager ::: Entering getDurationsResultSet>>>>>>>>");
		
		try {
			pst = connection.prepareStatement(staticParameters.get(ATTRIBUTE_SQL).toString());
			if (dynamicParameters != null) {
				pst.setInt(1, (Integer) dynamicParameters.get(SP_IN_PARAM_COMPANY_ID));
			}
			DBResult result = new DBResult();
			LOGGER.debug(">>>>>>>DatabaseConnectionManager ::: getDurationsResultSet>>>>>>>> prepareStatement: " + pst);
			results = pst.executeQuery();
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			for (i = 1; i <= numCols; i++) {
				result.addColumnName(metaData.getColumnLabel(i));
			}
			ArrayList<Object> row;
			while (results.next()) {
				row = new ArrayList<>();
				for (i = 1; i <= numCols; i++) {
					row.add(results.getObject(i));
				}
				result.addRow(row);
			}
			LOGGER.info(">>>>>>>DatabaseConnectionManager ::: getDurationsResultSet>>>>>>>> DBResult result: " + result.toJSON());
			return result;
		} catch (Exception exc) {
			LOGGER.error(ERROR_LOG + staticParameters.get(ATTRIBUTE_SQL).toString());
			LOGGER.error(exc);
			LOGGER.error(exc.getStackTrace());
			throw new DataSourceException(exc.getMessage());
		} finally {
			try {
				LOGGER.info(">>>>>>>DatabaseConnectionManager ::: getDurationsResultSet ::: Closing resultSet and Statement");
				results.close();
				pst.close();
			} catch (SQLException e) {
				LOGGER.error(e);
			}
			LOGGER.info(">>>>>>>DatabaseConnectionManager ::: Exiting getDurationsResultSet>>>>>>>>");
		}
	}

	public Result getCompanyListResultSet(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		LOGGER.info(">>>>>>>DatabaseConnectionManager ::: Entering getCompanyListResultSet>>>>>>>>");
		
		try {
			pst = connection.prepareStatement(staticParameters.get(ATTRIBUTE_SQL).toString());
			if (dynamicParameters != null) {
				pst.setInt(1, (Integer) dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID));
			}
			DBResult result = new DBResult();
			LOGGER.debug(">>>>>>>DatabaseConnectionManager ::: getCompanyListResultSet>>>>>>>> prepareStatement: " + pst);
			results = pst.executeQuery();
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			for (i = 1; i <= numCols; i++) {
				result.addColumnName(metaData.getColumnLabel(i));
			}
			ArrayList<Object> row;
			while (results.next()) {
				row = new ArrayList<>();
				for (i = 1; i <= numCols; i++) {
					row.add(results.getObject(i));
				}
				result.addRow(row);
			}
			LOGGER.info(">>>>>>>DatabaseConnectionManager ::: getCompanyListResultSet>>>>>>>> DBResult result: " + result.toJSON());
			return result;
		} catch (Exception exc) {
			LOGGER.error(ERROR_LOG + staticParameters.get(ATTRIBUTE_SQL).toString());
			LOGGER.error(exc);
			LOGGER.error(exc.getStackTrace());
			throw new DataSourceException(exc.getMessage());
		} finally {
			try {
				LOGGER.info(">>>>>>>DatabaseConnectionManager ::: getCompanyListResultSet ::: Closing resultSet and Statement");
				results.close();
				pst.close();
			} catch (SQLException e) {
				LOGGER.error(e);
			}
			LOGGER.info(">>>>>>>DatabaseConnectionManager ::: Exiting getCompanyListResultSet>>>>>>>>");
		}
	}
	
	public Result getPageTokenResultSet(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		LOGGER.info(">>>>>>>DatabaseConnectionManager ::: Entering getPageTokenResultSet>>>>>>>>");
		
		try {
			pst = connection.prepareStatement(staticParameters.get(ATTRIBUTE_SQL).toString());
			if (dynamicParameters != null) {
				pst.setInt(1, (Integer) dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID));
			}
			DBResult result = new DBResult();
			LOGGER.debug(">>>>>>>DatabaseConnectionManager ::: getPageTokenResultSet>>>>>>>> prepareStatement: " + pst);
			results = pst.executeQuery();
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			for (i = 1; i <= numCols; i++) {
				result.addColumnName(metaData.getColumnLabel(i));
			}
			ArrayList<Object> row;
			while (results.next()) {
				row = new ArrayList<>();
				for (i = 1; i <= numCols; i++) {
					row.add(results.getObject(i));
				}
				result.addRow(row);
			}
			LOGGER.info(">>>>>>>DatabaseConnectionManager ::: getPageTokenResultSet>>>>>>>> DBResult result: " + result.toJSON());
			return result;
		} catch (Exception exc) {
			LOGGER.error(ERROR_LOG + staticParameters.get(ATTRIBUTE_SQL).toString());
			LOGGER.error(exc);
			LOGGER.error(exc.getStackTrace());
			throw new DataSourceException(exc.getMessage());
		} finally {
			try {
				LOGGER.info(">>>>>>>DatabaseConnectionManager ::: getPageTokenResultSet ::: Closing resultSet and Statement");
				results.close();
				pst.close();
			} catch (SQLException e) {
				LOGGER.error(e);
			}
			LOGGER.info(">>>>>>>DatabaseConnectionManager ::: Exiting getPageTokenResultSet>>>>>>>>");
		}
	}
	
	
	public Result getSelectedRatingsCityListResultSet(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		LOGGER.info(">>>>>>>DatabaseConnectionManager ::: Entering getSelectedRatingsCityListResultSet>>>>>>>>");
		
		String sql = (String) staticParameters.get(ATTRIBUTE_SQL);
		try {
			pst = connection.prepareStatement(staticParameters.get(ATTRIBUTE_SQL).toString());
			if (dynamicParameters != null) {
				pst.setString(1, (String) dynamicParameters.get("inOwnerShipId"));
				pst.setString(2, (String) dynamicParameters.get("source_id"));
				pst.setString(3, (String) dynamicParameters.get("inAccountId"));
			}
			DBResult result = new DBResult();
			LOGGER.debug(">>>>>>>DatabaseConnectionManager ::: getSelectedRatingsCityListResultSet>>>>>>>> prepareStatement: " + pst);
			results = pst.executeQuery();
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			for (i = 1; i <= numCols; i++) {
				result.addColumnName(metaData.getColumnLabel(i));
			}
			ArrayList<Object> row;
			while (results.next()) {
				row = new ArrayList<>();
				for (i = 1; i <= numCols; i++) {
					row.add(results.getObject(i));
				}
				result.addRow(row);
			}
			LOGGER.info(">>>>>>>DatabaseConnectionManager ::: getSelectedRatingsCityListResultSet>>>>>>>> DBResult result: " + result.toJSON());
			return result;
		} catch (Exception exc) {
			LOGGER.error(ERROR_LOG + sql);
			LOGGER.error(exc);
			LOGGER.error(exc.getStackTrace());
			throw new DataSourceException(exc.getMessage());
		} finally {
			try {
				LOGGER.info(">>>>>>>DatabaseConnectionManager ::: getSelectedRatingsCityListResultSet ::: Closing resultSet and Statement");
				results.close();
				pst.close();
			} catch (SQLException e) {
				LOGGER.error(e);
			}
			LOGGER.info(">>>>>>>DatabaseConnectionManager ::: Exiting getSelectedRatingsCityListResultSet>>>>>>>>");
		}
	}

	public Result getKPIListResultSet(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		LOGGER.info(">>>>>>>DatabaseConnectionManager ::: Entering getHeadlinesResultSet>>>>>>>>");
		
		try {
			pst = connection.prepareStatement(staticParameters.get(ATTRIBUTE_SQL).toString());
			if (dynamicParameters != null) {
				pst.setInt(1, (Integer) dynamicParameters.get("inTabId"));
			}
			DBResult result = new DBResult();
			LOGGER.debug(">>>>>>>DatabaseConnectionManager ::: getHeadlinesResultSet>>>>>>>> prepareStatement: " + pst);
			results = pst.executeQuery();
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			for (i = 1; i <= numCols; i++) {
				result.addColumnName(metaData.getColumnLabel(i));
			}
			ArrayList<Object> row;
			while (results.next()) {
				row = new ArrayList<>();
				for (i = 1; i <= numCols; i++) {
					row.add(results.getObject(i));
				}
				result.addRow(row);
			}
			LOGGER.info(">>>>>>>DatabaseConnectionManager ::: getHeadlinesResultSet>>>>>>>> DBResult result: " + result.toJSON());
			return result;
		} catch (Exception exc) {
			LOGGER.error(ERROR_LOG + staticParameters.get(ATTRIBUTE_SQL).toString());
			LOGGER.error(exc);
			LOGGER.error(exc.getStackTrace());
			throw new DataSourceException(exc.getMessage());
		} finally {
			try {
				LOGGER.info(">>>>>>>DatabaseConnectionManager ::: getHeadlinesResultSet ::: Closing resultSet and Statement");
				results.close();
				pst.close();
			} catch (SQLException e) {
				LOGGER.error(e);
			}
			LOGGER.info(">>>>>>>DatabaseConnectionManager ::: Exiting getHeadlinesResultSet>>>>>>>>");
		}
	}
	
	//get city result against source for Review Analysis 
	public Result getCityResultSet(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		LOGGER.info(">>>>>>>DatabaseConnectionManager ::: Entering getCityResultSet>>>>>>>>");
		String sql = (String) staticParameters.get(ATTRIBUTE_SQL);
		try {
			pst = connection.prepareStatement(staticParameters.get(ATTRIBUTE_SQL).toString());
			if (dynamicParameters != null) {
				LOGGER.debug("================= ITS NOTTTT NULL ================");
				pst.setInt(1, (Integer) dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID));
				pst.setInt(2, (Integer) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_SOURCE_ID")));
			}else {
				LOGGER.info("================= ITS NULL ================");
			}
			
			
			
			DBResult result = new DBResult();
			LOGGER.debug(">>>>>>>DatabaseConnectionManager ::: getCityResultSet>>>>>>>> prepareStatement: " + pst);
			results = pst.executeQuery();
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			for (i = 1; i <= numCols; i++) {
				result.addColumnName(metaData.getColumnLabel(i));
			}
			ArrayList<Object> row;
			while (results.next()) {
				row = new ArrayList<>();
				for (i = 1; i <= numCols; i++) {
					row.add(results.getObject(i));
				}
				result.addRow(row);
			}
			LOGGER.info(">>>>>>>DatabaseConnectionManager ::: getCityResultSet>>>>>>>> DBResult result: " + result.toJSON());
			return result;
		} catch (Exception exc) {
			LOGGER.error(ERROR_LOG + sql);
			LOGGER.error(exc);
			LOGGER.error(exc.getStackTrace());
			throw new DataSourceException(exc.getMessage());
		} finally {
			try {
				LOGGER.info(">>>>>>>DatabaseConnectionManager ::: getCityResultSet ::: Closing resultSet and Statement");
				results.close();
				pst.close();
			} catch (SQLException e) {
				LOGGER.error(e);
			}
			LOGGER.info(">>>>>>>DatabaseConnectionManager ::: Exiting getCityResultSet>>>>>>>>");
		}
	}


	
	//get locality result against source and city for Review Analysis 
	public Result getLocalityResultSet(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		LOGGER.info(">>>>>>>DatabaseConnectionManager ::: Entering getLocalityResultSet>>>>>>>>");
		String sql = (String) staticParameters.get(ATTRIBUTE_SQL);
		try {
			pst = connection.prepareStatement(staticParameters.get(ATTRIBUTE_SQL).toString());
			if (dynamicParameters != null) {
				LOGGER.debug("================= ITS NOTTTT NULL ================");
				pst.setInt(1, (Integer) dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID));
				pst.setInt(2, (Integer) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_SOURCE_ID")));
				pst.setInt(3, (Integer) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_CITY_ID")));
			}else {
				LOGGER.info("================= ITS NULL ================");
			}
				
			DBResult result = new DBResult();
			LOGGER.debug(">>>>>>>DatabaseConnectionManager ::: getLocalityResultSet>>>>>>>> prepareStatement: " + pst);
			results = pst.executeQuery();
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			for (i = 1; i <= numCols; i++) {
				result.addColumnName(metaData.getColumnLabel(i));
			}
			ArrayList<Object> row;
			while (results.next()) {
				row = new ArrayList<>();
				for (i = 1; i <= numCols; i++) {
					row.add(results.getObject(i));
				}
				result.addRow(row);
			}
			LOGGER.info(">>>>>>>DatabaseConnectionManager ::: getLocalityResultSet>>>>>>>> DBResult result: " + result.toJSON());
			return result;
		} catch (Exception exc) {
			LOGGER.error(ERROR_LOG + sql);
			LOGGER.error(exc);
			LOGGER.error(exc.getStackTrace());
			throw new DataSourceException(exc.getMessage());
		} finally {
			try {
				LOGGER.info(">>>>>>>DatabaseConnectionManager ::: getLocalityResultSet ::: Closing resultSet and Statement");
				results.close();
				pst.close();
			} catch (SQLException e) {
				LOGGER.error(e);
			}
			LOGGER.info(">>>>>>>DatabaseConnectionManager ::: Exiting getLocalityResultSet>>>>>>>>");
		}
	}

	public int overallSentimentUpdater(Map<String, Object> staticParameters,
			Map<String, Object> dynamicParameters) throws SQLException, ParseException {
		
		LOGGER.info(">>>>>>>JdbcCommonDao ::: Entering overallSentimentUpdater>>>>>>>>");
		String postID = (String) dynamicParameters.get(SP_IN_PARAM_POST_ID);
		String postText = (String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_POST_TEXT"));
		String originalSentiment = (String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_ORIGINAL_SENTIMENT"));
		String changedSentiment = (String) dynamicParameters.get(SP_IN_PARAM_SENTIMENT_TYPE);
		String tabName = (String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_TAB_NAME"));
		String postDateTime = (String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_POST_DATETIME"));
		String accountId = (String) dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID);
		
		
		String strUpdateSQL = "UPDATE tblwrongsentimentforxpressotrained SET changed_overall_sentiment = ? WHERE post_id = ?";
		
		//Checking if already post id is exist in table then it will update the same roe 
		//else it will insert the new row.
		String isExistSQL = "SELECT COUNT(post_id) as postCnt FROM tblwrongsentimentforxpressotrained WHERE post_id = ?";
		int postCnt = 0;
		try(PreparedStatement selectPrepareStatement = connection.prepareStatement(isExistSQL);){
			selectPrepareStatement.setString(1, postID);
			
			try(ResultSet selectResult = selectPrepareStatement.executeQuery();){

			while (selectResult.next()) {
				postCnt = selectResult.getInt("postCnt");
			}
			LOGGER.info("Getting Post count in tblwrongsentimentforxpressotrained table is  : " + postCnt);
			}
		}catch(SQLException e) {
			LOGGER.error(e.getMessage());
		}
		
		int insertUpdateStatus = 0;
		LOGGER.info("Got Post count : " + postCnt);
		if(postCnt == 0){
			LOGGER.info("I am inside Insert statement execution");
			//If there is no post id same as current post id then it will insert a new record
			try (PreparedStatement preparedInsertStatement = connection.prepareStatement(staticParameters.get(ATTRIBUTE_SQL).toString())){
				preparedInsertStatement.setString(1, postID);
				preparedInsertStatement.setString(2, postText);
				preparedInsertStatement.setString(3, originalSentiment);
				preparedInsertStatement.setString(4, changedSentiment);
				preparedInsertStatement.setString(5, tabName);
				
				String pdt="";
				
				if(postDateTime.contains("-")) {
				pdt = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new SimpleDateFormat(
						"dd-MM-yyyy HH:mm").parse(postDateTime));
				}else if(postDateTime.contains("/"))
				{
				pdt = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new SimpleDateFormat(
						"dd/MM/yyyy HH:mm").parse(postDateTime+" 00:01"));
				}
				preparedInsertStatement.setString(6, pdt);
				preparedInsertStatement.setInt(7, Integer.parseInt(accountId));
				LOGGER.info("Insert new changed sentiment in  tblwrongsentimentforxpressotrained: " + preparedInsertStatement.toString());
				insertUpdateStatus = preparedInsertStatement.executeUpdate();
			} catch (SQLException e) {
				LOGGER.error(e.getMessage());
			}
		} else {
			LOGGER.info("I am inside Update statement execution");
			//If there is already post id same as current post id then it will update the same record
			try (PreparedStatement preparedUpdateStatement = connection.prepareStatement(strUpdateSQL)){
				preparedUpdateStatement.setString(1, changedSentiment);
				preparedUpdateStatement.setString(2, postID);
				LOGGER.info("Update changed sentiment in  tblwrongsentimentforxpressotrained: " + preparedUpdateStatement.toString());
				insertUpdateStatus = preparedUpdateStatement.executeUpdate();
			} catch (SQLException e) {
				LOGGER.error(e.getMessage());
			}
		}
		
		return insertUpdateStatus;
	}

	public int invalidDataUpdater(Map<String, Object> staticParameters,
			Map<String, Object> dynamicParameters) throws SQLException {
		
		LOGGER.info(">>>>>>>JdbcCommonDao ::: Entering invalidDataUpdater>>>>>>>>");
		String postID = (String) dynamicParameters.get(SP_IN_PARAM_POST_ID);
		String strUpdateSQL = staticParameters.get(ATTRIBUTE_SQL).toString();
		int insertUpdateStatus = 0;
		try (PreparedStatement preparedUpdateStatement = connection.prepareStatement(strUpdateSQL)){
			preparedUpdateStatement.setString(1, postID);
			LOGGER.info("Update Invalid data in  social_data_6 : " + preparedUpdateStatement.toString());
		    insertUpdateStatus = preparedUpdateStatement.executeUpdate();
		}
		return insertUpdateStatus;
	}

	public Result fetchPostSnippets(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		LOGGER.info(">>>>>>>DatabaseConnectionManager ::: Entering fetchPostSnippets>>>>>>>>");
		
		String sql = (String) staticParameters.get(ATTRIBUTE_SQL);
		try {
			pst = connection.prepareStatement(staticParameters.get(ATTRIBUTE_SQL).toString());
			if (dynamicParameters != null) {
				pst.setInt(1,  (int) dynamicParameters.get(SP_IN_PARAM_COMPANY_ID));
				pst.setString(2,  (String) dynamicParameters.get(SP_IN_PARAM_POST_ID));
			}
			DBResult result = new DBResult();
			LOGGER.debug(">>>>>>>DatabaseConnectionManager ::: fetchPostSnippets>>>>>>>> prepareStatement: " + pst);
			results = pst.executeQuery();
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			for (i = 1; i <= numCols; i++) {
				result.addColumnName(metaData.getColumnLabel(i));
			}
			ArrayList<Object> row;
			while (results.next()) {
				row = new ArrayList<>();
				for (i = 1; i <= numCols; i++) {
					row.add(results.getObject(i));
				}
				result.addRow(row);
			}
			LOGGER.info(">>>>>>>DatabaseConnectionManager ::: fetchPostSnippets>>>>>>>> DBResult result: " + result.toJSON());
			return result;
		} catch (Exception exc) {
			LOGGER.error(ERROR_LOG + sql);
			LOGGER.error(exc);
			LOGGER.error(exc.getStackTrace());
			throw new DataSourceException(exc.getMessage());
		} finally {
			try {
				LOGGER.info(">>>>>>>DatabaseConnectionManager ::: fetchPostSnippets ::: Closing resultSet and Statement");
				results.close();
				pst.close();
			} catch (SQLException e) {
				LOGGER.error(e);
			}
			LOGGER.info(">>>>>>>DatabaseConnectionManager ::: Exiting fetchPostSnippets>>>>>>>>");
		}
	}
	
	public Result fetchIFTicketAvailableData(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		LOGGER.info(">>>>>>>DatabaseConnectionManager ::: Entering fetchIFTicketAvailableData>>>>>>>>");
		
		String sql = (String) staticParameters.get(ATTRIBUTE_SQL);
		try {
			pst = connection.prepareStatement(staticParameters.get(ATTRIBUTE_SQL).toString());
			if (dynamicParameters != null) {
				pst.setString(1,  (String) dynamicParameters.get(SP_IN_PARAM_POST_ID));
			}
			DBResult result = new DBResult();
			LOGGER.debug(">>>>>>>DatabaseConnectionManager ::: fetchIFTicketAvailableData>>>>>>>> prepareStatement: " + pst);
			results = pst.executeQuery();
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			for (i = 1; i <= numCols; i++) {
				result.addColumnName(metaData.getColumnLabel(i));
			}
			ArrayList<Object> row;
			while (results.next()) {
				row = new ArrayList<>();
				for (i = 1; i <= numCols; i++) {
					row.add(results.getObject(i));
				}
				result.addRow(row);
			}
			LOGGER.info(">>>>>>>DatabaseConnectionManager ::: fetchIFTicketAvailableData>>>>>>>> DBResult result: " + result.toJSON());
			return result;
		} catch (Exception exc) {
			LOGGER.error(ERROR_LOG + sql);
			LOGGER.error(exc);
			LOGGER.error(exc.getStackTrace());
			throw new DataSourceException(exc.getMessage());
		} finally {
			try {
				LOGGER.info(">>>>>>>DatabaseConnectionManager ::: fetchIFTicketAvailableData ::: Closing resultSet and Statement");
				results.close();
				pst.close();
			} catch (SQLException e) {
				LOGGER.error(e);
			}
			LOGGER.info(">>>>>>>DatabaseConnectionManager ::: Exiting fetchIFTicketAvailableData>>>>>>>>");
		}
	}

	public Result fetchPostSnippetsForReviewAnalysis(Map<String, Object> staticParameters,
			Map<String, Object> dynamicParameters) throws DataSourceException {
		LOGGER.info(">>>>>>>DatabaseConnectionManager ::: Entering fetchPostSnippets>>>>>>>>");

		String sql = (String) staticParameters.get(ATTRIBUTE_SQL);
		try {
			pst = connection.prepareStatement(staticParameters.get(ATTRIBUTE_SQL).toString());
			if (dynamicParameters != null) {
				pst.setString(1,  (String) dynamicParameters.get(SP_IN_PARAM_POST_ID));
			}
			DBResult result = new DBResult();
			LOGGER.debug(">>>>>>>DatabaseConnectionManager ::: fetchPostSnippets>>>>>>>> prepareStatement: " + pst);
			results = pst.executeQuery();
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			for (i = 1; i <= numCols; i++) {
				result.addColumnName(metaData.getColumnLabel(i));
			}
			ArrayList<Object> row;
			while (results.next()) {
				row = new ArrayList<>();
				for (i = 1; i <= numCols; i++) {
					row.add(results.getObject(i));
				}
				result.addRow(row);
			}
			LOGGER.info(">>>>>>>DatabaseConnectionManager ::: fetchPostSnippets>>>>>>>> DBResult result: " + result.toJSON());
			return result;
		} catch (Exception exc) {
			LOGGER.error(ERROR_LOG + sql);
			LOGGER.error(exc);
			LOGGER.error(exc.getStackTrace());
			throw new DataSourceException(exc.getMessage());
		} finally {
			try {
				LOGGER.info(">>>>>>>DatabaseConnectionManager ::: fetchPostSnippets ::: Closing resultSet and Statement");
				results.close();
				pst.close();
			} catch (SQLException e) {
				LOGGER.error(e);
			}
			LOGGER.info(">>>>>>>DatabaseConnectionManager ::: Exiting fetchPostSnippets>>>>>>>>");
		}
	}

	public int overallSentimentUpdaterForReviewAnalysis(Map<String, Object> staticParameters,
			Map<String, Object> dynamicParameters) {

		LOGGER.info(">>>>>>>JdbcCommonDao ::: Entering overallSentimentUpdater for review analysis>>>>>>>>");
		String overallSentiment = (String) dynamicParameters.get(SP_IN_PARAM_SENTIMENT_TYPE);
		String reviewID = (String) dynamicParameters.get(SP_IN_PARAM_POST_ID);
		String overallSentimentNumber = (String) dynamicParameters.get(SP_IN_PARAM_SENTIMENT_NUMBER);
		int updateStatus = 0;
		try(PreparedStatement preparedStatement = connection.prepareStatement(staticParameters.get(ATTRIBUTE_SQL).toString());){
		
			preparedStatement.setString(1, overallSentiment);
			preparedStatement.setString(2, overallSentimentNumber);
			preparedStatement.setString(3, reviewID);
			LOGGER.info("Sentiment Updater : " + preparedStatement.toString());
			updateStatus = preparedStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(e.getMessage());
		}
		return updateStatus;
	
	}
	
	public Map<String, Object> getTwitterUserToken(
			) throws  SQLException {
		
		Map<String, Object> twitterTokenMap = new HashMap();

		try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER_TOKEN_TWITTER);
			ResultSet resultSet = preparedStatement.executeQuery()){
			
			if (resultSet != null) {
				while (resultSet.next()){
					twitterTokenMap.put("app_key", resultSet.getString("app_key"));
					twitterTokenMap.put("app_secret", resultSet.getString("app_secret"));
					twitterTokenMap.put("token", resultSet.getString("token"));
					twitterTokenMap.put("token_secret", resultSet.getString("token_secret"));
				}
			}
		} catch (SQLException e) {
			LOGGER.error("Error in getting twitter user token");
		} 
		
		return twitterTokenMap;

	}
	
	public Map<String, String> getPostDetails(String strPostID, String companyId) throws SQLException {

		Map<String, String> postDetails = new HashMap<>();
		ResultSet resultSet = null;
		StringBuilder sb = new StringBuilder();
		sb.append(SELECT_POST_DETAILS1);
		sb.append(SELECT_POST_DETAILS2);
		sb.append(companyId);
		sb.append(SELECT_POST_DETAILS3);
		
		try(PreparedStatement preparedStatement = connection.prepareStatement(sb.toString());){
			preparedStatement.setString(1, strPostID);
			LOGGER.info("****"+preparedStatement);
			resultSet = preparedStatement.executeQuery();
				while (resultSet.next()) {
					postDetails.put("authorName", resultSet.getString("author_name"));
					postDetails.put("postText", resultSet.getString("post_text"));
				
			resultSet.close();
			connection.close();
			}} catch (SQLException e) {
				LOGGER.error(e.getMessage());
			} finally {
				if (resultSet != null)
					resultSet.close();
				
				if (connection != null)
					connection.close();
			}
		return postDetails;

}

}
