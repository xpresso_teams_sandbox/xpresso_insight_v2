package com.abzooba.trackR24.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

import com.abzooba.trackR24.models.KPIFacebookBean;
import com.abzooba.trackR24.models.KPIFollowerFanGrowthRateBean;
import com.abzooba.trackR24.models.KPIMostEngagingTweetsPosts;
import com.abzooba.trackR24.models.KPIParentBean;
import com.abzooba.trackR24.models.KPIPerTweetPostInteractionBean;
import com.abzooba.trackR24.models.KPITopEngagingDailyBean;
import com.abzooba.trackR24.models.KPITopEngagingMonthlyBean;
import com.abzooba.trackR24.models.KPITopEngagingPostsBean;
import com.abzooba.trackR24.models.KPITopEngagingTweetsBean;
import com.abzooba.trackR24.models.KPITwitterBean;
import com.abzooba.trackR24.util.PropertyHandler;
import static com.abzooba.trackR24.util.Constants.*;

/**
* Author Name: Kunal Kashyap
* Create Date: 18-10-2016
* Last Modified: Kunal Kashyap: 18-10-2016
* Class Insight: This class contains all the methods of dao layer for KPI page.
*/
public class HibernateKPIDao implements IKPIDao {

	private static final Logger LOGGER = Logger.getLogger(HibernateKPIDao.class);

	/**
	* Author Name: Kunal Kashyap
	* Create Date: 18-10-2016
	* Last Modified: Kunal Kashyap: 18-10-2016
	* Input: Map,Map
	* Output: KPIParentBean object
	* Method Insight:  This method calls the procedure for facebook ribbon in KPI page.
	*/
	@SuppressWarnings("rawtypes")
	@Override
	public KPIParentBean getFacebookKPI(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) {
		LOGGER.info(">>>>>>> ::: Entering getFacebookKPI>>>>>>>>");
		Session session = null;
		Query query = null;
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);

		KPIParentBean kpiParentBean = new KPIParentBean();
		KPIFacebookBean kpiFacebookBean = new KPIFacebookBean();
		try {
			session = GenericHibernateDao.getSession();
			query = session.createSQLQuery(CALL.concat(spName).concat("(:1)"));
			setParamAccountId(dynamicParameters, query);
			List beanList = query.list();
			LOGGER.debug("beanList>>>>>> " + beanList.toString());
			Iterator it = beanList.iterator();
			while (it.hasNext()) {
				Object[] rows = (Object[]) it.next();
				kpiFacebookBean.setTalking_about_count((Integer) rows[0]);
				kpiFacebookBean.setPage_like((Integer) rows[1]);
				kpiFacebookBean.setNew_like((Integer) rows[2]);
				kpiFacebookBean.setRate_of_fan_growth((float) rows[3]);
				kpiParentBean.setKpiFacebookBean(kpiFacebookBean);
			}
		} catch (Exception e) {
			LOGGER.error("Error occurred getFacebookKPI");
			printCatchLog(e);
		} finally {
			if (session != null) {
				session.flush();
				session.close();
			}
		}
		LOGGER.info(">>>>>>> ::: Existing getFacebookKPI>>>>>>>>");
		return kpiParentBean;
	}

	/**
	* Author Name: Kunal Kashyap
	* Create Date: 18-10-2016
	* Last Modified: Kunal Kashyap: 18-10-2016
	* Input: Map,Map
	* Output: KPIParentBean object
	* Method Insight:  This method calls the procedure for twitter ribbon in KPI page.
	*/
	@SuppressWarnings("rawtypes")
	@Override
	public KPIParentBean getTwitterKPI(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) {
		LOGGER.info(">>>>>>> ::: Entering getTwitterKPI>>>>>>>>");

		Session session = null;
		Query query = null;
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);

		KPIParentBean kpiParentBean = new KPIParentBean();
		KPITwitterBean kpiTwitterBean = new KPITwitterBean();
		try {
			session = GenericHibernateDao.getSession();
			query = session.createSQLQuery(CALL.concat(spName).concat("(:1)"));
			setParamAccountId(dynamicParameters, query);

			List beanList = query.list();
			LOGGER.debug("beanList>>>>>> " + beanList.toString());
			Iterator it = beanList.iterator();
			while (it.hasNext()) {
				Object[] rows = (Object[]) it.next();
				kpiTwitterBean.setTotal_followers((Integer) rows[0]);
				kpiTwitterBean.setTotal_followings((Integer) rows[1]);
				kpiTwitterBean.setPage_like((Integer) rows[2]);
				kpiTwitterBean.setPost_id_count((Integer) rows[3]);
				kpiParentBean.setKpiTwitterBean(kpiTwitterBean);

			}

		} catch (Exception e) {
			LOGGER.error("Error occurred getTwitterKPI");
			printCatchLog(e);
		} finally {
			if (session != null) {
				session.flush();
				session.close();
			}
		}
		LOGGER.info(">>>>>>> ::: Existing getTwitterKPI>>>>>>>>");
		return kpiParentBean;
	}

	/**
	* Author Name: Kunal Kashyap
	* Create Date: 21-10-2016
	* Last Modified: Kunal Kashyap: 21-10-2016
	* Input: Map,Map
	* Output: KPIParentBean object
	* Method Insight:  This method calls the procedure for follower and fan growth rate in KPI page.
	*/
	@SuppressWarnings("rawtypes")
	@Override
	public KPIParentBean getFollowerFanGrowthRate(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) {
		LOGGER.info(">>>>>>> ::: Entering getFollowerFanGrowthRate>>>>>>>>");
		Session session = null;
		Query query = null;
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);

		KPIParentBean kpiParentBean = new KPIParentBean();
		List<KPIFollowerFanGrowthRateBean> kpiFollowerFanGrowthRateBeanList = new ArrayList<KPIFollowerFanGrowthRateBean>();

		try {
			session = GenericHibernateDao.getSession();
			query = session.createSQLQuery(CALL+ spName + "(:1,:2)");
			setParamAccountId(dynamicParameters, query);
			setParamDuration(dynamicParameters, query);

			List beanList = query.list();
			LOGGER.debug("beanList>>>>>> " + beanList.toString());
			Iterator it = beanList.iterator();
			while (it.hasNext()) {
				KPIFollowerFanGrowthRateBean kpiFollowerFanGrowthRateBean = new KPIFollowerFanGrowthRateBean();
				Object[] rows = (Object[]) it.next();
				DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String postDate = df.format((Date) rows[0]);

				kpiFollowerFanGrowthRateBean.setKpi_date(postDate);
				kpiFollowerFanGrowthRateBean.setGrowth_rate((float) rows[1]);
				kpiFollowerFanGrowthRateBeanList.add(kpiFollowerFanGrowthRateBean);
			}
			kpiParentBean.setKpiFollowerFanGrowthRateBeanList(kpiFollowerFanGrowthRateBeanList);

		} catch (Exception e) {
			LOGGER.error("Error occurred getFollowerFanGrowthRate");
			printCatchLog(e);
		} finally {
			if (session != null) {
				session.flush();
				session.close();
			}
		}
		LOGGER.info(">>>>>>> ::: Existing getFollowerFanGrowthRate>>>>>>>>");
		return kpiParentBean;
	}

	/**
	* Author Name: Kunal Kashyap
	* Create Date: 21-10-2016
	* Last Modified: Kunal Kashyap: 26-10-2016
	* Input: Map,Map
	* Output: KPIParentBean object
	* Method Insight:  This method calls the procedure for top engaging tweets in KPI page.
	*/
	@SuppressWarnings("rawtypes")
	@Override
	public KPIParentBean getTopEngagingTweets(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) {
		LOGGER.info(">>>>>>> ::: Entering getTopEngagingTweets>>>>>>>>");
		Session session = null;
		Query query = null;
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);

		KPIParentBean kpiParentBean = new KPIParentBean();
		List<KPITopEngagingTweetsBean> kpiTopEngagingTweetsBeanList = new ArrayList<KPITopEngagingTweetsBean>();

		try {
			session = GenericHibernateDao.getSession();
			query = session.createSQLQuery(CALL+ spName + "(:1)");
			setParamAccountId(dynamicParameters, query);
			List beanList = query.list();
			LOGGER.debug("beanList>>>>>> " + beanList.toString());
			Iterator it = beanList.iterator();
			while (it.hasNext()) {
				KPITopEngagingTweetsBean kpiTopEngagingTweetsBean = new KPITopEngagingTweetsBean();
				Object[] rows = (Object[]) it.next();
				kpiTopEngagingTweetsBean.setPost_id((String) rows[0]);
				kpiTopEngagingTweetsBean.setAuthor_name((String) rows[1]);
				kpiTopEngagingTweetsBean.setPost_text((String) rows[2]);
				kpiTopEngagingTweetsBean.setCount(Integer.valueOf(rows[3].toString()));
				kpiTopEngagingTweetsBean.setRetweet_count((int) rows[5]);
				kpiTopEngagingTweetsBean.setLove_count((int) rows[4]);
				kpiTopEngagingTweetsBeanList.add(kpiTopEngagingTweetsBean);
			}
			kpiParentBean.setKpiTopEngagingTweetsBeanList(kpiTopEngagingTweetsBeanList);
		} catch (Exception e) {
			LOGGER.error("Error occurred getTopEngagingTweets");
			printCatchLog(e);
		} finally {
			if (session != null) {
				session.flush();
				session.close();
			}
		}
		LOGGER.info(">>>>>>> ::: Existing getTopEngagingTweets>>>>>>>>");
		return kpiParentBean;
	}

	/**
	* Author Name: Kunal Kashyap
	* Create Date: 21-10-2016
	* Last Modified: Kunal Kashyap: 26-10-2016
	* Input: Map,Map
	* Output: KPIParentBean object
	* Method Insight:  This method calls the procedure for top engaging posts in KPI page.
	*/
	@SuppressWarnings("rawtypes")
	@Override
	public KPIParentBean getTopEngagingPosts(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) {
		LOGGER.info(">>>>>>> ::: Entering getTopEngagingPosts>>>>>>>>");
		Session session = null;
		Query query = null;
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);

		KPIParentBean kpiParentBean = new KPIParentBean();
		List<KPITopEngagingPostsBean> kpiTopEngagingPostsBeanList = new ArrayList<KPITopEngagingPostsBean>();

		try {
			session = GenericHibernateDao.getSession();
			query = session.createSQLQuery(CALL.concat(spName).concat("(:1)"));
			setParamAccountId(dynamicParameters, query);

			List beanList = query.list();
			LOGGER.debug("beanList>>>>>> " + beanList.toString());
			Iterator it = beanList.iterator();
			while (it.hasNext()) {
				KPITopEngagingPostsBean kpiTopEngagingPostsBean = new KPITopEngagingPostsBean();
				Object[] rows = (Object[]) it.next();
				kpiTopEngagingPostsBean.setPost_id((String) rows[0]);
				
				kpiTopEngagingPostsBean.setAuthor_name((String) rows[1]);
				kpiTopEngagingPostsBean.setPost_text((String) rows[2]);
				kpiTopEngagingPostsBean.setLikes_count((int) rows[3]);
				kpiTopEngagingPostsBean.setComments_count((int) rows[4]);
				kpiTopEngagingPostsBean.setReply_count((int) rows[5]);
				kpiTopEngagingPostsBean.setLove_count((int) rows[6]);
				kpiTopEngagingPostsBean.setHaha_count((int) rows[7]);
				kpiTopEngagingPostsBean.setWow_count((int) rows[8]);
				kpiTopEngagingPostsBean.setSad_count((int) rows[9]);
				kpiTopEngagingPostsBean.setAngry_count((int) rows[10]);
				kpiTopEngagingPostsBean.setThankful_count((int) rows[11]);
				kpiTopEngagingPostsBeanList.add(kpiTopEngagingPostsBean);
			}
			kpiParentBean.setKpiTopEngagingPostsBeanList(kpiTopEngagingPostsBeanList);
		} catch (Exception e) {
			LOGGER.error("Error occurred getTopEngagingPosts");
			printCatchLog(e);
		} finally {
			if (session != null) {
				session.flush();
				session.close();
			}
		}
		LOGGER.info(">>>>>>> ::: Existing getTopEngagingPosts>>>>>>>>");
		return kpiParentBean;
	}

	/**
	* Author Name: Kunal Kashyap
	* Create Date: 28-10-2016
	* Last Modified: Kunal Kashyap: 04-11-2016
	* Input: Map,Map
	* Output: KPIParentBean object
	* Method Insight:  This method calls the procedure for top engaging tweets and posts distribution in KPI page.
	*/
	@SuppressWarnings("rawtypes")
	@Override
	public KPIParentBean getTopEngagingTweetsPostsDistribution(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) {
		LOGGER.info(">>>>>>> ::: Entering getTopEngagingTweetsPostsDistribution>>>>>>>>");
		Session session = null;
		Query query = null;
		int prevCount = 0;
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);

		KPIParentBean kpiParentBean = new KPIParentBean();
		List<KPIMostEngagingTweetsPosts> kpiMostEngagingTweetsBeanList = new ArrayList<>();

		try {
			session = GenericHibernateDao.getSession();
			query = session.createSQLQuery(CALL.concat(spName).concat("(:1,:2,:3)"));
			setParamAccountId(dynamicParameters, query);
			setParamDuration(dynamicParameters, query);
			setParamSelectedDays(dynamicParameters, query);

			List beanList = query.list();
			LOGGER.debug("beanList>>>>>> " + beanList.toString());
			Iterator it = beanList.iterator();

			if (((String) dynamicParameters.get(SP_IN_PARAM_SELECTED_DURATION)).equalsIgnoreCase("daily")) {
				while (it.hasNext()) {
					KPIMostEngagingTweetsPosts kpiMostEngagingTweetsPosts = new KPIMostEngagingTweetsPosts();
					Object[] rows = (Object[]) it.next();
					DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
					String postDate = df.format((Date) rows[0]);
					kpiMostEngagingTweetsPosts.setDate(postDate);
					kpiMostEngagingTweetsPosts.setHour((int) rows[1]);
					setMostEngagingPostTweetCount(prevCount,
							kpiMostEngagingTweetsPosts, rows);
					prevCount = (int) rows[2];
					kpiMostEngagingTweetsBeanList.add(kpiMostEngagingTweetsPosts);
				}
			}

			else {
				while (it.hasNext()) {
					KPIMostEngagingTweetsPosts kpiMostEngagingTweetsPosts = new KPIMostEngagingTweetsPosts();
					Object[] rows = (Object[]) it.next();
					DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
					String postDate = df.format((Date) rows[0]);
					kpiMostEngagingTweetsPosts.setDate(postDate);
					kpiMostEngagingTweetsPosts.setHour((int) rows[1]);
					if (rows[2] == null) {
						kpiMostEngagingTweetsPosts.setCount(0);
					} else {
						kpiMostEngagingTweetsPosts.setCount((int) rows[2]);
					}
					kpiMostEngagingTweetsBeanList.add(kpiMostEngagingTweetsPosts);
				}
			}
			kpiParentBean.setKpiMostEngagingTweetsPostsList(kpiMostEngagingTweetsBeanList);
		} catch (Exception e) {
			LOGGER.error("Error occurred getTopEngagingTweetsPostsDistribution");
			printCatchLog(e);
		} finally {
			if (session != null) {
				session.flush();
				session.close();
			}
		}
		LOGGER.info(">>>>>>> ::: Existing getMostEngagingTweetsPosts>>>>>>>>");
		return kpiParentBean;
	}

	private void setMostEngagingPostTweetCount(int prevCount,
			KPIMostEngagingTweetsPosts kpiMostEngagingTweetsPosts, Object[] rows) {
		if (rows[2] == null) {
			kpiMostEngagingTweetsPosts.setCount(0);
		} else {
			kpiMostEngagingTweetsPosts.setCount((int) rows[2] - prevCount);
		}
	}

	private void setParamSelectedDays(Map<String, Object> dynamicParameters,
			Query query) {
		if (dynamicParameters.get(PropertyHandler.getPropertyValue(SP_IN_PARAM_SELECTED_DAYS)) == null) {
			query.setParameter("3", null);
		} else {
			query.setParameter("3", (Integer) dynamicParameters.get(PropertyHandler.getPropertyValue(SP_IN_PARAM_SELECTED_DAYS)));
		}
	}

	private void setParamDuration(Map<String, Object> dynamicParameters,
			Query query) {
		if (dynamicParameters.get(SP_IN_PARAM_SELECTED_DURATION) == null) {
			query.setParameter("2", null);
		} else {
			query.setParameter("2", (Integer) dynamicParameters.get(SP_IN_PARAM_SELECTED_DURATION));
		}
	}

	private void setParamAccountId(Map<String, Object> dynamicParameters,
			Query query) {
		if (dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID) == null) {
			query.setParameter("1", null);
		} else {
			query.setParameter("1", (Integer) dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID));
		}
	}

	/**
	* Author Name: Kunal Kashyap
	* Create Date: 04-11-2016
	* Last Modified: Kunal Kashyap: 11-11-2016
	* Input: Map,Map
	* Output: KPIParentBean object
	* Method Insight:  This method calls the procedure for per tweet and post interaction in KPI page.
	*/
	@SuppressWarnings("rawtypes")
	@Override
	public KPIParentBean gePerTweetPostInteraction(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) {
		LOGGER.info(">>>>>>> ::: Eentering gePerTweetPostInteraction>>>>>>>>");
		Session session = null;
		Query query = null;
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);

		KPIParentBean kpiParentBean = new KPIParentBean();
		List<KPIPerTweetPostInteractionBean> kpiPerTweetPostInteractionBeanList = new ArrayList<KPIPerTweetPostInteractionBean>();

		try {
			session = GenericHibernateDao.getSession();
			query = session.createSQLQuery(CALL.concat(spName).concat("(:1)"));
			setParamAccountId(dynamicParameters, query);

			List beanList = query.list();
			LOGGER.debug("beanList>>>>>> " + beanList.toString());
			Iterator it = beanList.iterator();
			while (it.hasNext()) {
				KPIPerTweetPostInteractionBean kpiPerTweetPostInteractionBean = new KPIPerTweetPostInteractionBean();
				Object[] rows = (Object[]) it.next();
				kpiPerTweetPostInteractionBean.setCount_date((String) rows[0]);
				kpiPerTweetPostInteractionBean.setAverage_count((double) rows[1]);
				kpiPerTweetPostInteractionBeanList.add(kpiPerTweetPostInteractionBean);
			}
			kpiParentBean.setKpiPerTweetPostInteractionBeanList(kpiPerTweetPostInteractionBeanList);
		} catch (Exception e) {
			LOGGER.error("Error occurred gePerTweetPostInteraction");
			printCatchLog(e);
		} finally {
			if (session != null) {
				session.flush();
				session.close();
			}
		}
		return kpiParentBean;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public KPIParentBean getTopEngagingTweetsPostsDistributionMonthly(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) {
		LOGGER.info(">>>>>>> ::: Eentering getTopEngagingTweetsPostsDistributionMonthly>>>>>>>>");
		Session session = null;
		Query query = null;
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		KPIParentBean kpiParentBean = new KPIParentBean();
		List<KPIMostEngagingTweetsPosts> kpiMostEngagingTweetsPostsList = new ArrayList<>();
		List<KPITopEngagingMonthlyBean> kpiTopEngagingMonthlyBeanList = new ArrayList<>();

		try {
			session = GenericHibernateDao.getSession();
			query = session.createSQLQuery(CALL.concat(spName).concat("(:1)"));
			setParamAccountId(dynamicParameters, query);
			KPIMostEngagingTweetsPosts kpiMostEngagingTweetsPostsBean = null;
			List beanList = query.list();
			LOGGER.debug("beanList>>>>>> " + beanList.toString());
			Iterator it = beanList.iterator();
			while (it.hasNext()) {
				Object[] rows = (Object[]) it.next();
				kpiMostEngagingTweetsPostsBean = new KPIMostEngagingTweetsPosts();
				kpiMostEngagingTweetsPostsBean.setDate((String) rows[0]);
				kpiMostEngagingTweetsPostsBean.setHour(Integer.parseInt(rows[1].toString()));
				kpiMostEngagingTweetsPostsBean.setCount(Integer.parseInt(rows[2].toString()));
				kpiMostEngagingTweetsPostsList.add(kpiMostEngagingTweetsPostsBean);
			}
			kpiMostEngagingTweetsPostsBean = null;
			Map<Integer, Integer> hmp = new HashMap<>();
			int x = 0;
			int y = 0;

			for (int i = 0; i < kpiMostEngagingTweetsPostsList.size() - 1; i++) {

				x = kpiMostEngagingTweetsPostsList.get(i).getCount() - kpiMostEngagingTweetsPostsList.get(i + 1).getCount();
				if (hmp.containsKey(kpiMostEngagingTweetsPostsList.get(i).getHour())) {
					y = (int) hmp.get(kpiMostEngagingTweetsPostsList.get(i).getHour());
					hmp.put(kpiMostEngagingTweetsPostsList.get(i).getHour(), y + x);
				} else {
					hmp.put(kpiMostEngagingTweetsPostsList.get(i).getHour(), x);
				}
			}
			KPITopEngagingMonthlyBean kpiTopEngagingMonthlyBean = null;
			for (Map.Entry<Integer, Integer> entry : hmp.entrySet()) {
				kpiTopEngagingMonthlyBean = new KPITopEngagingMonthlyBean();
				kpiTopEngagingMonthlyBean.setWeekCount(entry.getKey());
				kpiTopEngagingMonthlyBean.setAvgCount(entry.getValue() / 4);
				kpiTopEngagingMonthlyBeanList.add(kpiTopEngagingMonthlyBean);
			}
			kpiParentBean.setKpiTopEngagingMonthlyBeanList(kpiTopEngagingMonthlyBeanList);
		} catch (Exception e) {
			LOGGER.error("Error occurred getTopEngagingTweetsPostsDistributionMonthly");
			printCatchLog(e);
		} finally {
			session.flush();
			session.close();
		}
		LOGGER.info(">>>>>>> ::: Existing getTopEngagingTweetsPostsDistributionMonthly>>>>>>>>");
		return kpiParentBean;
	}

	@Override
	public KPIParentBean getTopEngagingTweetsPostsDistributionDaily(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) {
		LOGGER.info(">>>>>>> ::: Eentering getTopEngagingTweetsPostsDistributionDaily>>>>>>>>");
		Session session = null;
		Query query = null;
		int prevCount = 0;
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		KPIParentBean kpiParentBean = new KPIParentBean();
		List<KPITopEngagingDailyBean> kpiTopEngagingDailyBeanList = new ArrayList<>();
		KPITopEngagingDailyBean kpiTopEngagingDailyBean = null;
		try {
			session = GenericHibernateDao.getSession();
			query = session.createSQLQuery(CALL.concat(spName).concat("(:1,:2)"));
			setParamAccountId(dynamicParameters, query);
			if (dynamicParameters.get(PropertyHandler.getPropertyValue(SP_IN_PARAM_SELECTED_DAYS)) == null) {
				query.setParameter("2", null);
			} else {
				query.setParameter("2", (Integer) dynamicParameters.get(PropertyHandler.getPropertyValue(SP_IN_PARAM_SELECTED_DAYS)));
			}

			List<?> beanList = query.list();
			LOGGER.debug("beanList>>>>>> " + beanList.toString());
			Iterator<?> it = beanList.iterator();
			int noOfDays = (Integer) dynamicParameters.get(PropertyHandler.getPropertyValue(SP_IN_PARAM_SELECTED_DAYS));
			while (it.hasNext()) {
				Object[] rows = (Object[]) it.next();
				if (((int) rows[0]) != 0) {
					kpiTopEngagingDailyBean = new KPITopEngagingDailyBean();
					kpiTopEngagingDailyBean.setUpdateHour((int) rows[0]);
					kpiTopEngagingDailyBean.setAvgCount((Integer.valueOf(rows[1].toString()) - prevCount) / noOfDays);
					prevCount = Integer.valueOf(rows[1].toString());
					kpiTopEngagingDailyBeanList.add(kpiTopEngagingDailyBean);
				} else {
					prevCount = Integer.valueOf(rows[1].toString());
				}
			}

			kpiParentBean.setKpiTopEngagingDailyBeanList(kpiTopEngagingDailyBeanList);
		} catch (Exception e) {
			LOGGER.error("Error occurred getTopEngagingTweetsPostsDistributionDaily");
			printCatchLog(e);
		} finally {
			if(session!=null) {
			session.flush();
			session.close();
			}
		}
		LOGGER.info(">>>>>>> ::: Existing getTopEngagingTweetsPostsDistributionDaily>>>>>>>>");
		return kpiParentBean;
	}

	private void printCatchLog(Exception e) {
		LOGGER.error(e.getMessage());
		LOGGER.error(e.getStackTrace());
	}
}
