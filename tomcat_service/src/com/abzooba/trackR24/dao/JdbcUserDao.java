package com.abzooba.trackR24.dao;

import java.util.ArrayList;
import java.util.Map;

import org.apache.log4j.Logger;

import com.abzooba.trackR24.models.Result;
import com.abzooba.trackR24.models.UserInfo;
import com.abzooba.trackR24.models.UserInfoBean;
import com.abzooba.trackR24.util.PropertyHandler;

/**
* Author Name: Kunal Kashyap
* Create Date: 18-08-2016
* Last Modified: Kunal Kashyap: 16-09-2016
* Class Insight:  This class contains all the user related methods for calling the sql queries and procedures using jdbc.
*/
public class JdbcUserDao extends GenericJdbcDao implements IUserDao {

	private static final Logger LOGGER = Logger.getLogger(JdbcUserDao.class);

	@Override
	public int addUserInfo(UserInfoBean userInfo, String adminEmail) {
		return 0;
	}

	@Override
	public UserInfo fetchUserInfo(String adminEmail) {
		return null;
	}

	@Override
	public Result getStaticResults(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		Result result = null;
		LOGGER.info(">>>>>>>JdbcUserDao ::: Entering getStaticResults>>>>>>>>");
		try {
			result = connManager.getSourceTopicResultSet(staticParameters, dynamicParameters);
		} catch (Exception e) {
			LOGGER.error("Error occurred in JdbcUserDao.getStaticResults");
			getErrorMessagePrint(e);

		} finally {
			connManager.disconnect();
		}
		LOGGER.info(">>>>>>>JdbcUserDao ::: Exiting getStaticResults>>>>>>>>");
		return result;
	}

	private void getErrorMessagePrint(Exception e) {
		LOGGER.error(e.getMessage());
		LOGGER.error(e.getStackTrace());
	}

	@Override
	public ArrayList loginUser(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		LOGGER.info(">>>>>>>JdbcUserDao ::: Entering loginRegisterUser>>>>>>>>");
		ArrayList<String> loginDetails = null;
		try {
			loginDetails = (ArrayList<String>) connManager.getUserPrimaryDetails(staticParameters, dynamicParameters);
		} catch (Exception e) {
			LOGGER.error("Error occurred in JdbcUserDao.loginRegisterUser");
			getErrorMessagePrint(e);
		} finally {
			connManager.disconnect();
		}
		LOGGER.info(">>>>>>>JdbcUserDao ::: Exiting loginRegisterUser>>>>>>>>");
		return loginDetails;
	}

	@Override
	public String getUpdatePassword(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		LOGGER.info(">>>>>>>JdbcUserDao ::: Entering getPassword>>>>>>>>");
		String resultString = null;
		try {
			resultString = connManager.getUserpassword(staticParameters, dynamicParameters);
		} catch (Exception e) {
			LOGGER.error("Error occurred in JdbcUserDao.getPassword");
			getErrorMessagePrint(e);
		} finally {
			connManager.disconnect();
		}
		LOGGER.info(">>>>>>>JdbcUserDao ::: Exiting getPassword>>>>>>>>");
		return resultString;
	}

	@Override
	public int registerUser(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		LOGGER.info(">>>>>>>JdbcUserDao ::: Entering loginRegisterUser>>>>>>>>");
		Integer resultInt = null;
		String spName = (String) staticParameters.get(PropertyHandler.getPropertyValue("ATTRIBUTE_SP_NAME"));
		try {
			if (spName.equalsIgnoreCase(PropertyHandler.getPropertyValue("SP_INSERT_DATA"))) {
				resultInt = connManager.insertUserData(staticParameters, dynamicParameters);
			} else if (spName.equalsIgnoreCase(PropertyHandler.getPropertyValue("SP_RESET_PASSWORD"))) {
				resultInt = connManager.updateUserpassword(staticParameters, dynamicParameters);
			}

		} catch (Exception e) {
			LOGGER.error("Error occurred in JdbcUserDao.loginRegisterUser");
			getErrorMessagePrint(e);
		} finally {
			connManager.disconnect();
		}
		LOGGER.info(">>>>>>>JdbcUserDao ::: Exiting loginRegisterUser>>>>>>>>");
		return resultInt;
	}

}
