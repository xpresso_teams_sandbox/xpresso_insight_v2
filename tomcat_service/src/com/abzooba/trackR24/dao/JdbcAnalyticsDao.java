package com.abzooba.trackR24.dao;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Map;
import static com.abzooba.trackR24.util.Constants.*;
import org.apache.log4j.Logger;

import com.abzooba.trackR24.models.DBResult;
import com.abzooba.trackR24.models.Result;
import com.abzooba.trackR24.util.PropertyHandler;

public class JdbcAnalyticsDao extends GenericJdbcDao implements IAnalyticsDao {

	private static final Logger LOGGER = Logger.getLogger(JdbcAnalyticsDao.class);
	
	
	public Result fetchPostSnippets(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: Entering fetchPostSnippets>>>>>>>>");
		try {
			result = connManager.fetchPostSnippets(staticParameters, dynamicParameters);
			LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: fetchPostSnippets>>>>>>>>  result: " + result.toJSON());
		} catch (Exception e) {
			LOGGER.error("Error occurred in JdbcAnalyticsDao.fetchPostSnippets");
			printLOgMessage(e);
		} finally {
			connManager.disconnect();
		}
		LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: Exiting fetchPostSnippets>>>>>>>>");
		return result;
	}

	private void printLOgMessage(Exception e) {
		LOGGER.error(e.getMessage());
		LOGGER.error(e.getStackTrace());
	}
	
	public Result fetchIFTicketAvailable(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: Entering fetchIFTicketAvailable>>>>>>>>");
		try {
			result = connManager.fetchIFTicketAvailableData(staticParameters, dynamicParameters);
			LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: fetchIFTicketAvailable>>>>>>>>  result: " + result.toJSON());
		} catch (Exception e) {
			LOGGER.error("Error occurred in JdbcAnalyticsDao.fetchIFTicketAvailable");
			printLOgMessage(e);
		} finally {
			connManager.disconnect();
		}
		LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: Exiting fetchIFTicketAvailable>>>>>>>>");
		return result;
	}

	public Result fetchPostSnippetsForReviewAnalysis(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: Entering fetchPostSnippetsForReviewAnalysis>>>>>>>>");
		try {
			result = connManager.fetchPostSnippetsForReviewAnalysis(staticParameters, dynamicParameters);
			LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: fetchPostSnippetsForReviewAnalysis>>>>>>>>  result: " + result.toJSON());
		} catch (Exception e) {
			LOGGER.error("Error occurred in JdbcAnalyticsDao.fetchPostSnippets");
			catcgErrorMessage(e);
		} finally {
			connManager.disconnect();
		}
		LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: Exiting fetchPostSnippets>>>>>>>>");
		return result;
	}
	
	public Result fetchSourcesOrContexts(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: Entering fetchSourcesOrContexts>>>>>>>>");
		try {
			result = connManager.getSourceTopicResultSet(staticParameters, dynamicParameters);
		} catch (Exception e) {
			LOGGER.error("Error occurred in JdbcAnalyticsDao.fetchSourcesOrContexts");
			printLOgMessage(e);
		} finally {
			connManager.disconnect();
		}
		LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: Exiting fetchSourcesOrContexts>>>>>>>>");
		return result;

	}
	
	
	public Result fetchExploreKPI(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceConnectionException {
		LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: Entering fetchExploreKPI>>>>>>>>");
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		callStmt = callStmt.append("{").append(CALL).append(spName).append("(?,?,?,?,?,STR_TO_DATE(?,'%d-%m-%Y %H:%i'),STR_TO_DATE(?,'%d-%m-%Y %H:%i'),?,?,?,?)}");
		try(CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
			// Adding parameter for pagination
			setInParamOffset(dynamicParameters, stmt);
			setParamLimit(dynamicParameters, stmt);
			setParamAccountId(dynamicParameters, stmt);
			setParamSource(dynamicParameters, stmt);
			setParamTopic(dynamicParameters, stmt);
			setParamFromDate(dynamicParameters, stmt);
			setParamToDate(dynamicParameters, stmt);
			setParamSentimentType(dynamicParameters, stmt);
			setParamCity(dynamicParameters, stmt);
			setParamSourceCategory(dynamicParameters, stmt);
			setParamSearchKeyword(dynamicParameters, stmt);

			LOGGER.debug(">>>>>>>JdbcAnalyticsDao ::: fetchExploreKPI>>>>>>>> CallableStatement: " + stmt);
			try(ResultSet results = stmt.executeQuery();){
			LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: fetchExploreKPI>>>>>>>> " + stmt + " >>>>>>>> execution complete...");
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			i = addColNames(result, metaData, numCols);
			ArrayList<Object> row;
			while (results.next()) {
				row = new ArrayList<>();
				result.setCount(Integer.parseInt(results.getObject(numCols).toString()));
				for (i = 1; i <= numCols; i++) {
					row.add(results.getObject(i));
				}
				result.addRow(row);
			}
			}
			LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: fetchExploreKPI>>>>>>>> DBResult result: " + result.toJSON());
			LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: Exiting fetchExploreKPI>>>>>>>>");
		} catch (SQLException se) {
			printErrorMessage(spName, se);
		} finally {
			connManager.disconnect();
		}
		return result;
	}

	private void setParamSearchKeyword(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_SEARCH_KEYWORD")) == null) {
			stmt.setNull(11, Types.VARCHAR);
		} else {
			stmt.setString(11, (String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_SEARCH_KEYWORD")));
		}
	}

	private void setParamSourceCategory(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_SOURCE_CATEGORY")) == null) {
			stmt.setNull(10, Types.VARCHAR);
		} else {
			stmt.setString(10, (String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_SOURCE_CATEGORY")));
		}
	}

	private void setParamCity(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_CITY) == null || "".equalsIgnoreCase((String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_CITY")))) {
			stmt.setNull(9, Types.VARCHAR);
		} else {
			stmt.setString(9, (String) dynamicParameters.get(PropertyHandler.getPropertyValue("SP_IN_PARAM_CITY")));
		}
	}

	private void setParamSentimentType(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_SENTIMENT_TYPE) == null) {
			stmt.setNull(8, Types.INTEGER);
		} else {
			stmt.setInt(8, (Integer) dynamicParameters.get(SP_IN_PARAM_SENTIMENT_TYPE));
		}
	}

	private void setParamToDate(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_TO_DT) == null || "".equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_TO_DT))) {
			stmt.setNull(7, Types.VARCHAR);
		} else {
			stmt.setString(7, (String) dynamicParameters.get(SP_IN_PARAM_TO_DT));
		}
	}

	private void setParamFromDate(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_FROM_DT) == null || "".equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_FROM_DT))) {
			stmt.setNull(6, Types.VARCHAR);
		} else {
			stmt.setString(6, (String) dynamicParameters.get(SP_IN_PARAM_FROM_DT));
		}
	}

	private void setParamTopic(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_TOPIC) == null) {
			stmt.setNull(5, Types.VARCHAR);
		} else {
			stmt.setString(5, (String) dynamicParameters.get(SP_IN_PARAM_TOPIC));
		}
	}

	private void setParamSource(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_SOURCE) == null) {
			stmt.setNull(4, Types.INTEGER);
		} else {
			stmt.setInt(4, (Integer) dynamicParameters.get(SP_IN_PARAM_SOURCE));
		}
	}

	private void setParamAccountId(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID) == null) {
			stmt.setNull(3, Types.INTEGER);
		} else {
			stmt.setInt(3, (Integer) dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID));
		}
	}

	private void setParamLimit(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_LIMIT) == null) {
			stmt.setNull(2, Types.INTEGER);
		} else {
			stmt.setInt(2, (Integer) dynamicParameters.get(SP_IN_PARAM_LIMIT));
		}
	}

	private void setInParamOffset(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_OFFSET) == null) {
			stmt.setNull(1, Types.INTEGER);
		} else {
			stmt.setInt(1, (Integer) dynamicParameters.get(SP_IN_PARAM_OFFSET));
		}
	}


	public Result fetchWordCloudData(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: Entering fetchWordCloudData>>>>>>>>");
		try {
			result = connManager.getWordCloudData(staticParameters, dynamicParameters);
		} catch (Exception e) {
			LOGGER.error("Error occurred in JdbcAnalyticsDao.fetchSourcesOrContexts");
			printLOgMessage(e);
		} finally {
			connManager.disconnect();
		}
		LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: Exiting fetchWordCloudData>>>>>>>>");
		return result;

	}

	public Result fetchAnalyticsKPIs(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {

		LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: Entering fetchAnalyticsKPIs>>>>>>>>");
		DBResult result = new DBResult();
		CallableStatement stmt = null;
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		try {
			if (PropertyHandler.getPropertyValue("SP_ENTITY_FILTER_DATA").equalsIgnoreCase(spName)) {
				callStmt = callStmt.append("{").append(CALL).append(spName).append("(?,?,?,STR_TO_DATE(?,'%d-%m-%Y %H:%i'),STR_TO_DATE(?,'%d-%m-%Y %H:%i'),?,?)}");
				stmt = connManager.connection.prepareCall(callStmt.toString());
				setParamAccountIdPosOne(dynamicParameters, stmt);
				setParamSourcePosTwo(dynamicParameters, stmt);
				setParamTopicPos3(dynamicParameters, stmt);
				setParamFromDatePos4(dynamicParameters, stmt);
				setParamToDatePosFive(dynamicParameters, stmt);
				setParamSentimentTypeposSix(dynamicParameters, stmt);
				setParamDurationPosSeven(dynamicParameters, stmt);
			} else if (PropertyHandler.getPropertyValue("SP_WORD_CLOUD_ASPECT").equalsIgnoreCase(spName)) {
				callStmt = callStmt.append("{").append(CALL).append(spName).append("(?,?,STR_TO_DATE(?,'%d-%m-%Y %H:%i'),STR_TO_DATE(?,'%d-%m-%Y %H:%i'),?)}");
				stmt = connManager.connection.prepareCall(callStmt.toString());
				stmt.setInt(1, (Integer) dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID));
				setParamSourcePosTwo(dynamicParameters, stmt);
				setParamFromDatePosThree(dynamicParameters, stmt);
				setParamToDatePosFour(dynamicParameters, stmt);
				setParamSentimentPosFive(dynamicParameters, stmt);
			} else {
				callStmt = callStmt.append("{").append(CALL).append(spName).append("(?,?,?,STR_TO_DATE(?,'%d-%m-%Y %H:%i'),STR_TO_DATE(?,'%d-%m-%Y %H:%i'),?)}");
				stmt = connManager.connection.prepareCall(callStmt.toString());
				stmt.setInt(1, (Integer) dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID));
				setParamSourcePosTwo(dynamicParameters, stmt);
				setParamTopicPosThree(dynamicParameters, stmt);
				setParamFromDatePos4(dynamicParameters, stmt);
				setParamToDatePosFive(dynamicParameters, stmt);
				setParamSentimentTypeposSix(dynamicParameters, stmt);
			}

			try(ResultSet results = stmt.executeQuery();){
			LOGGER.debug(stmt);
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			i = addColNames(result, metaData, numCols);
			ArrayList<Object> row;
			while (results.next()) {
				row = new ArrayList<>();
				for (i = 1; i <= numCols; i++) {
					row.add(results.getObject(i));
				}
				result.addRow(row);
			}
			}
			LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: Exiting fetchAnalyticsKPIs>>>>>>>>");
		} catch (SQLException se) {
			LOGGER.error(ERROR_LOG + spName);
			getPrintErrorLog(se);
		} finally {
			try {
				stmt.close();
			} catch (SQLException e) {
				LOGGER.error(e.getMessage());
			}
			connManager.disconnect();
		}
		return result;

	}

	private void getPrintErrorLog(SQLException se) {
		LOGGER.error(se.getMessage());
		LOGGER.error(se.getStackTrace());
	}

	private void setParamTopicPosThree(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_TOPIC) == null) {
			stmt.setNull(3, Types.VARCHAR);
		} else {
			stmt.setString(3, (String) dynamicParameters.get(SP_IN_PARAM_TOPIC));
		}
	}

	private void setParamSentimentPosFive(
			Map<String, Object> dynamicParameters, CallableStatement stmt)
			throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_SENTIMENT_TYPE) == null) {
			stmt.setNull(5, Types.VARCHAR);
		} else {
			stmt.setString(5, (String) dynamicParameters.get(SP_IN_PARAM_SENTIMENT_TYPE));
		}
	}

	private void setParamToDatePosFour(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_TO_DT) == null || "".equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_TO_DT))) {
			stmt.setNull(4, Types.VARCHAR);
		} else {
			stmt.setString(4, (String) dynamicParameters.get(SP_IN_PARAM_TO_DT));
		}
	}

	private void setParamFromDatePosThree(
			Map<String, Object> dynamicParameters, CallableStatement stmt)
			throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_FROM_DT) == null || "".equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_FROM_DT))) {
			stmt.setNull(3, Types.VARCHAR);
		} else {
			stmt.setString(3, (String) dynamicParameters.get(SP_IN_PARAM_FROM_DT));
		}
	}

	private void setParamDurationPosSeven(
			Map<String, Object> dynamicParameters, CallableStatement stmt)
			throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_SELECTED_DURATION) == null) {
			stmt.setNull(7, Types.VARCHAR);
		} else {
			stmt.setString(7, (String) dynamicParameters.get(SP_IN_PARAM_SELECTED_DURATION));
		}
	}

	private void setParamSentimentTypeposSix(
			Map<String, Object> dynamicParameters, CallableStatement stmt)
			throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_SENTIMENT_TYPE) == null) {
			stmt.setNull(6, Types.VARCHAR);
		} else {
			stmt.setString(6, (String) dynamicParameters.get(SP_IN_PARAM_SENTIMENT_TYPE));
		}
	}

	private void setParamToDatePosFive(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_TO_DT) == null || "".equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_TO_DT))) {
			stmt.setNull(5, Types.VARCHAR);
		} else {
			stmt.setString(5, (String) dynamicParameters.get(SP_IN_PARAM_TO_DT));
		}
	}

	private void setParamFromDatePos4(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_FROM_DT) == null || "".equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_FROM_DT))) {
			stmt.setNull(4, Types.VARCHAR);
		} else {
			stmt.setString(4, (String) dynamicParameters.get(SP_IN_PARAM_FROM_DT));
		}
	}

	private void setParamTopicPos3(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_TOPIC) == null || "".equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_TOPIC))) {
			stmt.setNull(3, Types.VARCHAR);
		} else {
			stmt.setString(3, (String) dynamicParameters.get(SP_IN_PARAM_TOPIC));
		}
	}

	private void setParamSourcePosTwo(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_SOURCE) == null) {
			stmt.setNull(2, Types.INTEGER);
		} else {
			stmt.setInt(2, (Integer) dynamicParameters.get(SP_IN_PARAM_SOURCE));
		}
	}

	private void setParamAccountIdPosOne(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID) == null) {
			stmt.setNull(1, Types.INTEGER);
		} else {
			stmt.setInt(1, (Integer) dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID));
		}
	}

	public Result fetchFilteredAnalyticsKPIs(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {

		LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: Entering fetchFilteredAnalyticsKPIs>>>>>>>>");
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
			addParamagainstSPName(callStmt, spName);
		try(CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
			setParamAccountIdPosOne(dynamicParameters, stmt);
			setParamSourcePosTwo(dynamicParameters, stmt);
			setParamTopicPos3(dynamicParameters, stmt);
			setParamFromDatePos4(dynamicParameters, stmt);
			setParamToDatePosFive(dynamicParameters, stmt);
			setParamSentimentTypeposSix(dynamicParameters, stmt);
			
			if(spName.equalsIgnoreCase("sp_wordcloud_aspect_entity_click")){
				stmt.setString(7, (String) dynamicParameters.get(IS_ASPECT_ENTITY));
			}
			
			try(ResultSet results = stmt.executeQuery();){
			LOGGER.debug(stmt);
			ResultSetMetaData metaData = results.getMetaData();
			int numCols = metaData.getColumnCount();
			for (int i = 1; i <= numCols; i++) {
				result.addColumnName(metaData.getColumnLabel(i));
			}
			ArrayList<Object> row;
			while (results.next()) {
				row = new ArrayList<>();
				for (int i = 1; i <= numCols; i++) {
					row.add(results.getObject(i));
				}
				result.addRow(row);
			}
			}
			LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: Exiting fetchFilteredAnalyticsKPIs>>>>>>>>");
		} catch (SQLException se) {
			LOGGER.error(ERROR_LOG + spName);
			getPrintErrorLog(se);
		} finally {
			connManager.disconnect();
		}
		return result;

	}

	private void addParamagainstSPName(StringBuilder callStmt, String spName) {
		if(spName.equalsIgnoreCase("sp_wordcloud_aspect_entity_click")){
			callStmt = callStmt.append("{").append(CALL).append(spName).append("(?,?,?,STR_TO_DATE(?,'%d-%m-%Y %H:%i'),STR_TO_DATE(?,'%d-%m-%Y %H:%i'),?,?)}");
		} else {
			callStmt = callStmt.append("{").append(CALL).append(spName).append("(?,?,?,STR_TO_DATE(?,'%d-%m-%Y %H:%i'),STR_TO_DATE(?,'%d-%m-%Y %H:%i'),?)}");
		}
	}

	public Result fetchSnapshotData(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {

		LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: Entering fetchSnapshotData>>>>>>>>");
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		callStmt = callStmt.append("{").append(CALL).append(spName).append("(?,?)}");

			try(CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
			setParamAccountIdPosOne(dynamicParameters, stmt);
			if (dynamicParameters.get(SP_IN_PARAM_SELECTED_DURATION) == null) {
				stmt.setNull(2, Types.INTEGER);
			} else {
				stmt.setInt(2, (Integer) dynamicParameters.get(SP_IN_PARAM_SELECTED_DURATION));
			}

			try(ResultSet results = stmt.executeQuery();){
			LOGGER.debug(stmt);
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			i = addColNames(result, metaData, numCols);
			ArrayList<Object> row;
			i = addRows(result, results, i, numCols);
			}
			LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: Exiting fetchFilteredAnalyticsKPIs>>>>>>>>");
		} catch (SQLException se) {
			LOGGER.error(ERROR_LOG + spName);
			getPrintErrorLog(se);
		} finally {
			connManager.disconnect();
		}
		return result;

	}

	private int addRows(DBResult result, ResultSet results, int i, int numCols)
			throws SQLException {
		ArrayList<Object> row;
		while (results.next()) {
			row = new ArrayList<>();
			for (i = 1; i <= numCols; i++) {
				row.add(results.getObject(i));
				LOGGER.info("Adding Row : " + results.getObject(i));
			}
			result.addRow(row);
		}
		return i;
	}

	private int addColNames(DBResult result, ResultSetMetaData metaData,
			int numCols) throws SQLException {
		int i;
		i = addColumnNames(result, metaData, numCols);
		return i;
	}

		public Result getPostTweetWiseEngagementScore(
			Map<String, Object> staticParameters,
			Map<String, Object> dynamicParameters) throws DataSourceConnectionException {
		LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: Entering getPostTweetWiseEngagementScore>>>>>>>>");
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
			callStmt = callStmt.append("{").append(CALL).append(spName).append("(?,?,?,?,?,?)}");
			try(CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
			setInParamOffset(dynamicParameters, stmt);
			setParamLimit(dynamicParameters, stmt);
			setParamAccountId(dynamicParameters, stmt);
			setParamSource(dynamicParameters, stmt);
			if (dynamicParameters.get(SP_IN_PARAM_SELECTED_DURATION) == null) {
				stmt.setNull(5, Types.INTEGER);
			} else {
				stmt.setInt(5, (Integer) dynamicParameters.get(SP_IN_PARAM_SELECTED_DURATION));
			}
			if (dynamicParameters.get(SP_IN_PARAM_SORTBY) == null) {
				stmt.setNull(6, Types.INTEGER);
			} else {
				stmt.setInt(6, (Integer) dynamicParameters.get(SP_IN_PARAM_SORTBY));
			}

			try(ResultSet results = stmt.executeQuery();){
			LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: getPostTweetWiseEngagementScore>>>>>>>> " + stmt + " >>>>>>>> execution complete...");
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			LOGGER.info("Print NumCols value : " + numCols);
			i = addColumnNames(result, metaData, numCols);
			ArrayList<Object> row;
			i = addRowsFromResultSet(result, results, i, numCols);
			}
			LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: getPostTweetWiseEngagementScore>>>>>>>> DBResult result: " + result.toJSON());
			LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: Exiting getPostTweetWiseEngagementScore>>>>>>>>");
		} catch (SQLException se) {
			printErrorMessage(spName, se);
		} finally {
			connManager.disconnect();
		}
		return result;
	}

		private void printErrorMessage(String spName, SQLException se) {
			LOGGER.error(ERROR_LOG + spName);
			LOGGER.error(se);
			LOGGER.error(se.getStackTrace());
		}

		private int addRowsFromResultSet(DBResult result, ResultSet results,
				int i, int numCols) throws SQLException {
			ArrayList<Object> row;
			while (results.next()) {
				row = new ArrayList<>();
				LOGGER.info("Result set count : " + results.getObject(numCols).toString());
				result.setCount(Integer.parseInt(results.getObject(numCols).toString()));
				
				for (i = 1; i <= numCols; i++) {
					row.add(results.getObject(i));
				}
				result.addRow(row);
			}
			return i;
		}

		private int addColumnNames(DBResult result, ResultSetMetaData metaData,
				int numCols) throws SQLException {
			int i;
			for (i = 1; i <= numCols; i++) {
				result.addColumnName(metaData.getColumnLabel(i));
			}
			return i;
		}


	public Result fetchHeadlines(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: Entering fetchHeadlines>>>>>>>>");
		try {
			result = connManager.getHeadlinesResultSet(staticParameters, dynamicParameters);
			LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: fetchHeadlines>>>>>>>> getHeadlinesResultSet result: " + result.toJSON());
		} catch (Exception e) {
			LOGGER.error("Error occurred in JdbcAnalyticsDao.fetchHeadlines");
			catcgErrorMessage(e);
		} finally {
			connManager.disconnect();
		}
		LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: Exiting fetchHeadlines>>>>>>>>");
		return result;
	}

	private void catcgErrorMessage(Exception e) {
		LOGGER.error(e);
		LOGGER.error(e.getStackTrace());
	}
	public Result fetchDurations(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: Entering fetchDurations>>>>>>>>");
		try {
			result = connManager.getDurationsResultSet(staticParameters, dynamicParameters);
			LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: fetchHeadlines>>>>>>>> fetchDurations result: " + result.toJSON());
		} catch (Exception e) {
			LOGGER.error("Error occurred in JdbcAnalyticsDao.fetchDurations");
			catcgErrorMessage(e);
		} finally {
			connManager.disconnect();
		}
		LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: Exiting fetchDurations>>>>>>>>");
		return result;
	}
	public Result fetchCompanyList(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		LOGGER.info(">>>>>>>JdbcAnalytics ::: Entering fetchPageToken>>>>>>>>");
		try {
			result = connManager.getCompanyListResultSet(staticParameters, dynamicParameters);
			LOGGER.info(">>>>>>>JdbcAnalytics ::: fetchCompanyList>>>>>>>> result: " + result.toJSON());
		} catch (Exception e) {
			LOGGER.error("Error occurred in JdbcAnalytics.fetchCompanyList");
			catcgErrorMessage(e);
		} finally {
			connManager.disconnect();
		}
		LOGGER.info(">>>>>>>JdbcAnalytics ::: Exit fetchPageToken>>>>>>>>");
		return result;
	}
	
	public Result fetchPageToken(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		LOGGER.info(">>>>>>>JdbcAnalytics ::: Entering fetchPageToken>>>>>>>>");
		try {
			result = connManager.getPageTokenResultSet(staticParameters, dynamicParameters);
			LOGGER.info(">>>>>>>JdbcAnalytics ::: fetchCompanyList>>>>>>>> result: " + result.toJSON());
		} catch (Exception e) {
			LOGGER.error("Error occurred in JdbcAnalytics.fetchPageToken");
			catcgErrorMessage(e);
		} finally {
			connManager.disconnect();
		}
		LOGGER.info(">>>>>>>JdbcAnalytics ::: Exit fetchPageToken>>>>>>>>");
		return result;
	}
		
	public Result fetchRatingsSelectedCityList(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: Entering fetchRatingsSelectedCityList>>>>>>>>");
		try {
			result = connManager.getSelectedRatingsCityListResultSet(staticParameters, dynamicParameters);
			LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: fetchRatingsSelectedCityList>>>>>>>>  result: " + result.toJSON());
		} catch (Exception e) {
			LOGGER.error("Error occurred in JdbcAnalyticsDao.fetchRatingsSelectedCityList");
			catcgErrorMessage(e);
		} finally {
			connManager.disconnect();
		}
		LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: Exiting fetchRatingsSelectedCityList>>>>>>>>");
		return result;
	}
	
	public Result fetchKpiList(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: Entering fetchKpiList>>>>>>>>");
		try {
			result = connManager.getKPIListResultSet(staticParameters, dynamicParameters);
			LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: fetchKpiList>>>>>>>>  result: " + result.toJSON());
		} catch (Exception e) {
			LOGGER.error("Error occurred in JdbcAnalyticsDao.fetchKpiList");
			catcgErrorMessage(e);
		} finally {
			connManager.disconnect();
		}
		LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: Exiting fetchKpiList>>>>>>>>");
		return result;
	}


	public Result fetchPeriodicSnapshotData(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceConnectionException {
		LOGGER.info(">>>>>>>JdbcDashboardDao ::: Entering fetchPeriodicSnapshotData>>>>>>>>");
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
			callStmt = callStmt.append("{").append(CALL).append(spName).append("(?,?,STR_TO_DATE(?,'%d-%m-%Y %H:%i'),STR_TO_DATE(?,'%d-%m-%Y %H:%i'),STR_TO_DATE(?,'%d-%m-%Y %H:%i'),STR_TO_DATE(?,'%d-%m-%Y %H:%i'))}");
			try(CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
			setPeriodicSnapshotParams(dynamicParameters, stmt);
			
			LOGGER.debug(">>>>> >>>>>>>JdbcDashboardDao ::: CallableStatement >>> fetchPeriodicSnapshotData >>>>> " + stmt);
			try(ResultSet results = stmt.executeQuery();){
			
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			i = addColNames(result, metaData, numCols);
			ArrayList row;
			int rowCounter = 0;
			while (results.next()) {
				row = new ArrayList<>();
				rowCounter++;
				for (i = 1; i <= numCols; i++) {
					row.add(results.getObject(i));
				}
				result.addRow(row);
			}
			}
			LOGGER.info(">>>>>>>JdbcDashboardDao ::: fetchPeriodicSnapshotData>>>>>>>> DBResult result: " + result.toJSON());
		} catch (SQLException se) {
			printErrorMessage(spName, se);
		} finally {
			connManager.disconnect();
		}
		return result;
	}

	private void setPeriodicSnapshotParams(
			Map<String, Object> dynamicParameters, CallableStatement stmt)
			throws SQLException {
		setSourceIdPosOne(dynamicParameters, stmt);
		if (dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID) == null) {
			stmt.setNull(2, Types.INTEGER);
		} else {
			stmt.setInt(2, (Integer) dynamicParameters.get(SP_IN_PARAM_ACCOUNT_ID));
		}
		if (dynamicParameters.get(SP_IN_PARAM_PERIODIC_START_FROM) == null || "".equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_PERIODIC_START_FROM))) {
			stmt.setNull(3, Types.VARCHAR);
		} else {
			stmt.setString(3, (String) dynamicParameters.get(SP_IN_PARAM_PERIODIC_START_FROM));
		}
		if (dynamicParameters.get(SP_IN_PARAM_PERIODIC_START_TO) == null || "".equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_PERIODIC_START_TO))) {
			stmt.setNull(4, Types.VARCHAR);
		} else {
			stmt.setString(4, (String) dynamicParameters.get(SP_IN_PARAM_PERIODIC_START_TO));
		}
		if (dynamicParameters.get(SP_IN_PARAM_PERIODIC_END_FROM) == null || "".equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_PERIODIC_END_FROM))) {
			stmt.setNull(5, Types.VARCHAR);
		} else {
			stmt.setString(5, (String) dynamicParameters.get(SP_IN_PARAM_PERIODIC_END_FROM));
		}
		if (dynamicParameters.get(SP_IN_PARAM_PERIODIC_END_TO) == null || "".equalsIgnoreCase((String) dynamicParameters.get(SP_IN_PARAM_PERIODIC_END_TO))) {
			stmt.setNull(6, Types.VARCHAR);
		} else {
			stmt.setString(6, (String) dynamicParameters.get(SP_IN_PARAM_PERIODIC_END_TO));
		}
	}


	public int overallSentimentUpdater(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		int updateStatus = 0;
		LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: Entering overallSentimentUpdater>>>>>>>>");
		try {
			updateStatus = connManager.overallSentimentUpdater(staticParameters, dynamicParameters);
			LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: overallSentimentUpdater>>>>>>>> overallSentimentUpdater result: " + updateStatus);
		} catch (Exception e) {
			LOGGER.error("Error occurred in JdbcAnalyticsDao.overallSentimentUpdater");
			catcgErrorMessage(e);
		} finally {
			connManager.disconnect();
		}
		LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: Exiting overallSentimentUpdater>>>>>>>>");
		return updateStatus;
	}

	public int overallSentimentUpdaterForReviewAnalysis(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceException {
		int updateStatus = 0;
		LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: Entering overallSentimentUpdaterForReviewAnalysis>>>>>>>>");
		try {
			updateStatus = connManager.overallSentimentUpdaterForReviewAnalysis(staticParameters, dynamicParameters);
			LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: overallSentimentUpdaterForReviewAnalysis>>>>>>>> overallSentimentUpdater result: " + updateStatus);
		} catch (Exception e) {
			LOGGER.error("Error occurred in JdbcAnalyticsDao.overallSentimentUpdaterForReviewAnalysis");
			catcgErrorMessage(e);
		} finally {
			connManager.disconnect();
		}
		LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: Exiting overallSentimentUpdaterForReviewAnalysis>>>>>>>>");
		return updateStatus;
	}

	public Result fetchComplaintData(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceConnectionException {
		LOGGER.info(">>>>>>>JdbcDashboardDao ::: Entering fetchComplaintData>>>>>>>>");
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		callStmt = callStmt.append("{").append(CALL).append(spName).append("(?,?,STR_TO_DATE(?,'%d-%m-%Y %H:%i'),STR_TO_DATE(?,'%d-%m-%Y %H:%i'))}");
			try(CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
			setSourceIdPosOne(dynamicParameters, stmt);
			setCompanyIdPosTwo(dynamicParameters, stmt);
			setParamFromDatePosThree(dynamicParameters, stmt);
			setParamToDatePosFour(dynamicParameters, stmt);
			
			LOGGER.debug(">>>>> >>>>>>>JdbcDashboardDao ::: CallableStatement >>> fetchComplaintData >>>>> " + stmt);
			try(ResultSet results = stmt.executeQuery();){
			
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			i = addColNames(result, metaData, numCols);
			ArrayList<Object> row;
			while (results.next()) {
				row = new ArrayList<>();
				for (i = 1; i <= numCols; i++) {
					row.add(results.getObject(i));
				}
				result.addRow(row);
			}
			}

			LOGGER.info(">>>>>>>JdbcDashboardDao ::: fetchComplaintData>>>>>>>> DBResult result: " + result.toJSON());
			LOGGER.info(">>>>>>>JdbcDashboardDao ::: Exiting fetchComplaintData>>>>>>>>");
		} catch (SQLException se) {
			printErrorMessage(spName, se);
		} finally {
			connManager.disconnect();
		}
		return result;
	}

	private void setCompanyIdPosTwo(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_PRIMARY_COMPANY_ID) == null) {
			stmt.setNull(2, Types.INTEGER);
		} else {
			stmt.setInt(2, (Integer) dynamicParameters.get(SP_IN_PARAM_PRIMARY_COMPANY_ID));
		}
	}

	private void setSourceIdPosOne(Map<String, Object> dynamicParameters,
			CallableStatement stmt) throws SQLException {
		if (dynamicParameters.get(SP_IN_PARAM_SOURCE_ID) == null) {
			stmt.setNull(1, Types.INTEGER);
		} else {
			stmt.setInt(1, (Integer) dynamicParameters.get(SP_IN_PARAM_SOURCE_ID));
		}
	}
	
	public Result fetchFeedbackData(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceConnectionException {
		LOGGER.info(">>>>>>>JdbcDashboardDao ::: Entering fetchFeedbackData>>>>>>>>");
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		callStmt = callStmt.append("{").append(CALL).append(spName).append("(?,?,STR_TO_DATE(?,'%d-%m-%Y %H:%i'),STR_TO_DATE(?,'%d-%m-%Y %H:%i'))}");
			try(CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
			setSourceIdPosOne(dynamicParameters, stmt);
			setCompanyIdPosTwo(dynamicParameters, stmt);
			setParamFromDatePosThree(dynamicParameters, stmt);
			setParamToDatePosFour(dynamicParameters, stmt);
			
			LOGGER.debug(">>>>> >>>>>>>JdbcDashboardDao ::: CallableStatement >>> fetchFeedbackData >>>>> " + stmt);
			try(ResultSet results = stmt.executeQuery();){
			
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			i = addColNames(result, metaData, numCols);
			ArrayList<Object> row;
			while (results.next()) {
				row = new ArrayList<>();
				for (i = 1; i <= numCols; i++) {
					row.add(results.getObject(i));
				}
				result.addRow(row);
			}
			}
			LOGGER.info(">>>>>>>JdbcDashboardDao ::: fetchFeedbackData>>>>>>>> DBResult result: " + result.toJSON());
			LOGGER.info(">>>>>>>JdbcDashboardDao ::: Exiting fetchFeedbackData>>>>>>>>");
		} catch (SQLException se) {
			printErrorMessage(spName, se);
		} finally {
			connManager.disconnect();
		}
		return result;
	}

	public Result fetchTopAdvocatesData(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceConnectionException {
		LOGGER.info(">>>>>>>JdbcDashboardDao ::: Entering fetchTopAdvocatesData>>>>>>>>");
		DBResult result = new DBResult();
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
			callStmt = callStmt.append("{").append(CALL).append(spName).append("(?,?,?,STR_TO_DATE(?,'%d-%m-%Y %H:%i'),STR_TO_DATE(?,'%d-%m-%Y %H:%i'))}");
			try(CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
			setSourceIdPosOne(dynamicParameters, stmt);
			setCompanyIdPosTwo(dynamicParameters, stmt);
			if (dynamicParameters.get(SP_IN_AUTHOR_NAME) == null) {
				stmt.setNull(3, Types.VARCHAR);
			} else {
				stmt.setString(3,(String) dynamicParameters.get(SP_IN_AUTHOR_NAME));
			}
			setParamFromDatePos4(dynamicParameters, stmt);
			setParamToDatePosFive(dynamicParameters, stmt);
			
			LOGGER.debug(">>>>> >>>>>>>JdbcDashboardDao ::: CallableStatement >>> fetchTopAdvocatesData >>>>> " + stmt);
			try(ResultSet results = stmt.executeQuery();){
			
			ResultSetMetaData metaData = results.getMetaData();
			int i;
			int numCols = metaData.getColumnCount();
			i = addColNames(result, metaData, numCols);
			ArrayList<Object> row;
			int rowCounter = 0;
			while (results.next()) {
				row = new ArrayList<>();
				rowCounter++;
				for (i = 1; i <= numCols; i++) {
					row.add(results.getObject(i));
				}
				result.addRow(row);
			}
			}
			LOGGER.info(">>>>>>>JdbcDashboardDao ::: fetchTopAdvocatesData>>>>>>>> DBResult result: " + result.toJSON());
			LOGGER.info(">>>>>>>JdbcDashboardDao ::: Exiting fetchTopAdvocatesData>>>>>>>>");
		} catch (SQLException se) {
			printErrorMessage(spName, se);
		} finally {
			connManager.disconnect();
		}
		return result;
	}
	
	
	public ResultSet fetchDataForDeepList(Map<String, Object> staticParameters, Map<String, Object> dynamicParameters) throws DataSourceConnectionException {
		LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: Entering fetchExploreKPI>>>>>>>>");
		
		//ResultSet results = null;
		ResultSet allResults = null;
		StringBuilder callStmt = new StringBuilder();
		String spName = (String) staticParameters.get(ATTRIBUTE_SP_NAME);
		callStmt = callStmt.append("{").append(CALL).append(spName).append("(?,?,?,?,?,STR_TO_DATE(?,'%d-%m-%Y %H:%i'),STR_TO_DATE(?,'%d-%m-%Y %H:%i'),?,?,?,?)}");
		try(CallableStatement stmt = connManager.connection.prepareCall(callStmt.toString());){
			setInParamOffset(dynamicParameters, stmt);
			setParamLimit(dynamicParameters, stmt);
			setParamAccountId(dynamicParameters, stmt);
			setParamSource(dynamicParameters, stmt);
			setParamTopic(dynamicParameters, stmt);
			setParamFromDate(dynamicParameters, stmt);
			setParamToDate(dynamicParameters, stmt);
			setParamSentimentType(dynamicParameters, stmt);
			setParamCity(dynamicParameters, stmt);
			setParamSourceCategory(dynamicParameters, stmt);
			setParamSearchKeyword(dynamicParameters, stmt);

			LOGGER.debug(">>>>>>>JdbcAnalyticsDao ::: fetchExploreKPI>>>>>>>> CallableStatement: " + stmt);
			//results = stmt.executeQuery();
			
			try(ResultSet results = stmt.executeQuery();){
				LOGGER.info(">>>>>>>JdbcAnalyticsDao ::: fetchExploreKPI>>>>>>>> " + stmt + " >>>>>>>> execution complete...");
			}
			
			}
		 catch (SQLException se) {
			printErrorMessage(spName, se);
		} finally {
			connManager.disconnect();
		}
		return allResults;
	}



}
