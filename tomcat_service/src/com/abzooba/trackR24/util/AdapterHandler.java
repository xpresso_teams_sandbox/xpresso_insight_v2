package com.abzooba.trackR24.util;

import java.util.ArrayList;
import java.util.List;

import com.abzooba.trackR24.models.Competitor;
import com.abzooba.trackR24.models.CompetitorBean;
import com.abzooba.trackR24.models.SourceInfo;
import com.abzooba.trackR24.models.SourceMasterBean;
import com.abzooba.trackR24.models.User;
import com.abzooba.trackR24.models.UserInfo;
import com.abzooba.trackR24.models.UserInfoBean;
import com.abzooba.trackR24.models.UserMasterBean;

/**
 * Author Name: Kunal Kashyap
 * Create Date: 03-08-2016
 * Class Insight: This class is responsible for converting the UserInfo bean to UserInfoBean bean
 * 				  which is used for adding and fetching user details in the settings page.
 */
public class AdapterHandler {

	public UserInfoBean getBean(UserInfo userInfo) {

		UserInfoBean userInfoBean = new UserInfoBean();
		List<UserMasterBean> userMasterBeanList = new ArrayList<>();
		List<SourceMasterBean> sourceMasterBeanList = new ArrayList<>();
		List<CompetitorBean> competitorBeanList = new ArrayList<>();

		List<User> usersList = userInfo.getUsersList();
		List<SourceInfo> sourcesDetailsList = userInfo.getSourcesDetailsList();
		List<Competitor> competitorsList = userInfo.getCompetitorsList();

		for (User user : usersList) {
			UserMasterBean userMasterBean = new UserMasterBean();
			userMasterBean.setUser_name(user.getUserName());
			userMasterBean.setUser_id(user.getUserEmail());
			userMasterBeanList.add(userMasterBean);

		}
		userInfoBean.setUserMasterBeanList(userMasterBeanList);

		for (SourceInfo sourceInfo : sourcesDetailsList) {
			SourceMasterBean sourceMasterBean = new SourceMasterBean();
			if (sourceInfo.getSourceURL() != null) {
				sourceMasterBean.setSource_type_id(sourceInfo.getSourceTypeId());
				sourceMasterBean.setSource_url(sourceInfo.getSourceURL());
				sourceMasterBeanList.add(sourceMasterBean);
			}
		}

		userInfoBean.setSourceMasterBeanList(sourceMasterBeanList);

		for (Competitor competitor : competitorsList) {
			CompetitorBean competitorBean = new CompetitorBean();
			competitorBean.setCompany_name(competitor.getCompetitorName());

			List<SourceInfo> competitorSourcesDetailsList = competitor.getCompetitorSources();

			List<SourceInfo> competitorBeanSourcesList = new ArrayList<>();
			for (SourceInfo sourceInfo : competitorSourcesDetailsList) {
				if (sourceInfo.getSourceURL() != null) {
					SourceInfo compSourceInfo = new SourceInfo();
					compSourceInfo.setSourceTypeId(sourceInfo.getSourceTypeId());
					compSourceInfo.setSourceURL(sourceInfo.getSourceURL());
					competitorBeanSourcesList.add(compSourceInfo);
				}
			}
			competitorBean.setCompetitorSources(competitorBeanSourcesList);

			competitorBeanList.add(competitorBean);
		}
		userInfoBean.setCompetitorBeanList(competitorBeanList);

		return userInfoBean;

	}
}
