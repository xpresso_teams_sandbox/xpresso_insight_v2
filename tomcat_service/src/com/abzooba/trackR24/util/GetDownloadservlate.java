package com.abzooba.trackR24.util;


import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.abzooba.trackR24.services.RatingsService;


@WebServlet("/GetDownloadservlate")
public class GetDownloadservlate extends HttpServlet {
	
	private static final Logger LOGGER = Logger.getLogger(GetDownloadservlate.class);
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		RatingsService ratingservice = new RatingsService();
		Map<String, String> csvData = new HashMap<>();
		
		String inCityId = request.getParameter("inCityId");
		String inAccountId = request.getParameter("inAccountId");
		String inOwnerShipId = request.getParameter("inOwnerShipId");
		String inSearchText = request.getParameter("inSearchText");
		
		Integer intCityId = null;
		try {
		intCityId = inCityId.equalsIgnoreCase("-1") ? null : Integer.valueOf(inCityId);
		}catch(Exception e) {
			LOGGER.error("Eror in felching city id");
		}
		String strSearchText = inSearchText.equalsIgnoreCase("-1") ? null : "%"+inSearchText+"%";
		
		try {
			 csvData = ratingservice.getExcelFormat(intCityId, inAccountId, inOwnerShipId,strSearchText);
		} catch (Exception e1) {
			LOGGER.error("Error in dowloading ");
		}
		
		LOGGER.info("Printing All csv Final Data : " + csvData.get("header"));
		
		response.addHeader("Content-Type", "application/force-download");
	    response.addHeader("Content-Disposition", "attachment; filename=\"Store_List.csv\"");
	    try
	    {
	        OutputStream outputStream = response.getOutputStream();
	        String outputResult = csvData.get("header") +"\n"+  csvData.get("body") ;
	        
	        outputStream.write(outputResult.getBytes());
	        outputStream.flush();
	        outputStream.close();
	    }
	    catch(Exception e)
	    {
	        LOGGER.error(e.toString());
	    }
		

	}

}