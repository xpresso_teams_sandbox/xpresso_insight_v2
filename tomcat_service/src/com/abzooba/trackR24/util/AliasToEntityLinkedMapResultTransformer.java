package com.abzooba.trackR24.util;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import org.hibernate.transform.BasicTransformerAdapter;

/**
* Author Name: Kunal Kashyap
* Create Date: 07-10-2016
* Last Modified: Kunal Kashyap: 07-10-2016
* Class Insight: This class is responsible for converting the hashmap to linked hashmap while calling 
* 				 the procedures using hibernate.
*/
public class AliasToEntityLinkedMapResultTransformer extends BasicTransformerAdapter implements Serializable {
	 
	private static final long serialVersionUID = 1L;
	public static final AliasToEntityLinkedMapResultTransformer INSTANCE = new AliasToEntityLinkedMapResultTransformer();

	private AliasToEntityLinkedMapResultTransformer() {

	}
	
	@Override
	public Object transformTuple(Object[] tuple, String[] aliases) {
		Map<String, Object> result = new LinkedHashMap<>(tuple.length);
		for (int i = 0; i < tuple.length; i++) {
			String alias = aliases[i];
			if (alias != null) {
				result.put(alias, tuple[i]);
			}
		}

		return result;
	}

	public Object readResolve() {
		return INSTANCE;
	}

	public boolean equals(Object other) {
		return other != null && AliasToEntityLinkedMapResultTransformer.class.isInstance(other);
	}

	public int hashCode() {
		return getClass().getName().hashCode();
	}
}
