package com.abzooba.trackR24.util;

import java.util.PropertyResourceBundle;

import org.apache.log4j.Logger;

/**
 * Author Name: Kunal Kashyap
 * Create Date: 04-08-2016
 * Class Insight: This class is responsible for fetching data from properties file
 */
public class PropertyHandler {
	
	private static final Logger LOGGER = Logger.getLogger(PropertyHandler.class);
	private static PropertyResourceBundle bundle;
	
	private PropertyHandler() {}
	
	static {
		try {
			bundle = (PropertyResourceBundle) PropertyResourceBundle.getBundle("config");
			LOGGER.info(">>>>>>>Properties file successfully loaded>>>>>>>>");
		} catch (Exception e) {
			LOGGER.error("Error occurred while loading properties file");
		}
	}

	public static String getPropertyValue(String key) {
		return bundle.getString(key);
	}

}
