package com.abzooba.trackR24.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.abzooba.trackR24.services.CommonService;
import com.opencsv.CSVWriter;

/**
 * Servlet implementation class DownloadInvalidReport
 */
@WebServlet(description = "Download all report which are invalid", urlPatterns = { "/DownloadInvalidReport" })
public class DownloadInvalidReport extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger(DownloadInvalidReport.class);
	
    public DownloadInvalidReport() {
        super();
        // TODO Auto-generated constructor stub
    }

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 CommonService commonService = new CommonService();
		// File file = new File("D:/Invalid_Report_List.csv"); 
		 Map<String, Object> csvData = new HashMap<>();	
		/*try{
			 FileWriter outputfile = new FileWriter(file);
			 CSVWriter writer = new CSVWriter(outputfile); 
			 
			 csvData = commonService.getInvaildReport();
			 
			 String[] header = csvData.get("header").toString().split(","); 
		     writer.writeNext(header); 
		     
		     List<String> body = (List<String>) csvData.get("body"); 
		     for(String data : body){
		    	 String[] dataArray = data.split(","); 
		    	 writer.writeNext(dataArray); 
		     }
		     writer.close(); 
 
		}catch(Exception e){
			LOGGER.error("Error in dowloading ");
		}*/
		
				
		try {
			 csvData = commonService.getInvaildReport();
		} catch (Exception e1) {
			LOGGER.error("Error in dowloading ");
		}
		LOGGER.info("Printing All csv Final Data : " + csvData.get("header"));
 
		response.setContentType("text/csv"); 
		response.addHeader("Content-Type", "application/force-download");
	    response.addHeader("Content-Disposition", "attachment; filename=\"Invalid_Feedback_List.csv\"");	    
	    try {
	        OutputStream outputStream = response.getOutputStream();
	        String outputResult = csvData.get("header") +"\n"+  csvData.get("body") ;
	        
	        outputStream.write(outputResult.getBytes());
	        outputStream.flush();
	        outputStream.close();
	    }
	    catch(Exception e)
	    {
	        LOGGER.error(e.toString());
	    }
	}


	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
