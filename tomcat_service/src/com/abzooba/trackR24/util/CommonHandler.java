package com.abzooba.trackR24.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;

public class CommonHandler {
	
	private CommonHandler() {}
	
	private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	private static final Logger LOGGER = Logger.getLogger(PropertyHandler.class);

	/**
	 * Method Insight: This method generates a unique key of user defined length. 
	 * @param count
	 * @return
	 */
	public static String randomAlphaNumeric(int count) {
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
			builder.append(ALPHA_NUMERIC_STRING.charAt(character));
		}
		return builder.toString();
	}

	/**
	 * Method Insight: This static method returns a string after appending all the required details of logged in user.
	 * @param loginDetails
	 * @return String
	 */
	public static String concatArrayList(ArrayList loginDetails) {
		StringBuilder  concatResult= new StringBuilder();
		for (int i = 0; i < loginDetails.size(); i++) {
			if (i == 1 && loginDetails.get(i) == null) {
				concatResult.append(PropertyHandler.getPropertyValue("LOGO_IMAGE") );
				concatResult.append(PropertyHandler.getPropertyValue("VARIABLE_COLUMN_DELIMITER"));
			} else if (i == (loginDetails.size() - 1)) {
				concatResult.append(loginDetails.get(i));
			} else {
				concatResult.append(loginDetails.get(i)); 
				concatResult.append(PropertyHandler.getPropertyValue("VARIABLE_COLUMN_DELIMITER"));
			}
		}
		return concatResult.toString();
	}

	/**
	 * Method Insight: This method is responsible for sending mail in the application.
	 * @param email,data,type
	 * @throws AddressException,MessagingException,SQLException
	 */
	public static void sendEmail(String email, String data, String type) throws MessagingException {

		final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
		Properties props = System.getProperties();
		props.setProperty("mail.smtp.host", "smtp.gmail.com");
		props.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
		props.setProperty("mail.smtp.socketFactory.fallback", "false");
		props.setProperty("mail.smtp.port", "465");
		props.setProperty("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.auth", "true");
		props.put("mail.debug", "true");
		props.put("mail.store.protocol", "pop3");
		props.put("mail.transport.protocol", "smtp");
		final String username = PropertyHandler.getPropertyValue("MAIL_USERNAME");
		final String password = PropertyHandler.getPropertyValue("MAIL_PASSWORD");
		String content = null;
		try {
			Session session = Session.getDefaultInstance(props, new Authenticator() {
				@Override
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			});

			Message msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress("XpressoInsights Support<" + PropertyHandler.getPropertyValue("MAIL_USERNAME") + ">"));
			msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email, false));

			if (type.equalsIgnoreCase("confirmation")) {
				msg.setSubject(PropertyHandler.getPropertyValue("MAIL_SUBJECT_REGISTRATION"));

				String[] splitValues = data.split("\\$");
				String myurl = splitValues[1];
				String key = splitValues[0];
				String name = splitValues[2];
				String[] names = name.split(" ");
				String firstName = names[0];
				String userPassword = splitValues[3];
				

				StringBuilder message = new StringBuilder();

				message.append(PropertyHandler.getPropertyValue("MAIL_BODY_REGISTRATION_HEADER") + firstName + PropertyHandler.getPropertyValue("MAIL_BODY_REGISTRATION_BODY_PART_1")+ email + PropertyHandler.getPropertyValue("MAIL_BODY_REGISTRATION_BODY_PART_2")+ userPassword + PropertyHandler.getPropertyValue("MAIL_BODY_REGISTRATION_BODY_PART_3") + myurl + PropertyHandler.getPropertyValue("MAIL_BODY_REGISTRATION_BODY_PART_4")+ key + PropertyHandler.getPropertyValue("MAIL_BODY_REGISTRATION_BODY_PART_5"));
				content = message.toString();

			} else if (type.equalsIgnoreCase("forgot")) {
				msg.setSubject(PropertyHandler.getPropertyValue("MAIL_SUBJECT_PASSWORD"));
				String[] splitValues = data.split("\\$");
				String name = splitValues[0];
				String[] names = name.split(" ");
				String firstName = names[0];
				String userPassword = splitValues[1];
				 
				
				StringBuilder message = new StringBuilder();
				
				message.append(PropertyHandler.getPropertyValue("MAIL_BODY_REGISTRATION_HEADER") + firstName + PropertyHandler.getPropertyValue("FORGOT_PASSWORD_BODY_PART_1")+ email + PropertyHandler.getPropertyValue("FORGOT_PASSWORD_BODY_PART_2")+ userPassword + PropertyHandler.getPropertyValue("FORGOT_PASSWORD_BODY_PART_3"));
				
				content = message.toString();
			}
			msg.setContent(content, "text/html");
			msg.setSentDate(new Date());
			Transport.send(msg);
			LOGGER.info(">>>>>>>Properties file successfully loaded>>>>>>>>");
		} catch (MessagingException e) {
			LOGGER.error(e.getMessage());
		}
	}
}
