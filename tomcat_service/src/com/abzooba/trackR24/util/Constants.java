package com.abzooba.trackR24.util;

public final class Constants {

	private Constants() {
	    throw new IllegalStateException("Utility class");
	  }
	public static final String ATTRIBUTE_SQL = PropertyHandler.getPropertyValue("ATTRIBUTE_SQL");
	public static final String JDBC_ANALYTICS_DAO = PropertyHandler.getPropertyValue("JDBC_ANALYTICS_DAO");
	public static final String JDBC_DASHBOARD_DAO = PropertyHandler.getPropertyValue("JDBC_DASHBOARD_DAO");
	public static final String JDBC_BRAND_DAO = PropertyHandler.getPropertyValue("JDBC_BRAND_DAO");
	public static final String JDBC_RATINGS_DAO =PropertyHandler.getPropertyValue("JDBC_RATINGS_DAO");
	public static final String JDBC_USER_DAO = PropertyHandler.getPropertyValue("JDBC_USER_DAO");
	public static final String ATTRIBUTE_SP_NAME = PropertyHandler.getPropertyValue("ATTRIBUTE_SP_NAME");
	
	public static final String CONST_RATINGDAO_EXIT_LOGGER_MESSAGE = PropertyHandler.getPropertyValue("CONST_RATINGDAO_EXIT_LOGGER_MESSAGE");
	public static final String CONST_RATINGDAO_ENTER_LOGGER_MESSAGE = PropertyHandler.getPropertyValue("CONST_RATINGDAO_EXIT_LOGGER_MESSAGE");
	public static final String CONST_CALL_STRING_FOR_SP = PropertyHandler.getPropertyValue("CONST_CALL_STRING_FOR_SP");
	public static final String CONST_FOUR_QUESTION_MARK = PropertyHandler.getPropertyValue("CONST_FOUR_QUESTION_MARK");
	public static final String CONST_THREE_QUESTION_MARK = PropertyHandler.getPropertyValue("CONST_THREE_QUESTION_MARK");
	
	
	
	
	public static final String SP_KPI_TOP_ENGAGING_POSTS = PropertyHandler.getPropertyValue("SP_KPI_TOP_ENGAGING_POSTS");
	public static final String SP_IN_PARAM_SELECTED_DURATION_KPISERVICE = PropertyHandler.getPropertyValue("SP_IN_PARAM_SELECTED_DURATION_KPISERVICE");
	public static final String SP_KPI_FAN_GROWTH_RATE = PropertyHandler.getPropertyValue("SP_KPI_FAN_GROWTH_RATE");
	public static final String SP_KPI_FOLLOWER_GROWTH_RATE = PropertyHandler.getPropertyValue("SP_KPI_FOLLOWER_GROWTH_RATE");
	public static final String SP_KPI_RIBBON_FACEBOOK = PropertyHandler.getPropertyValue("SP_KPI_RIBBON_FACEBOOK");
	public static final String SP_IN_PARAM_SELECTED_DAYS = PropertyHandler.getPropertyValue("SP_IN_PARAM_SELECTED_DAYS");
	public static final String SP_IN_PARAM_POST_TYPE = PropertyHandler.getPropertyValue("SP_IN_PARAM_POST_TYPE");
	public static final String SP_IN_PARAM_ACCOUNT_ID = PropertyHandler.getPropertyValue("SP_IN_PARAM_ACCOUNT_ID");
	public static final String SP_IN_PARAM_PRIMARY_COMPANY_ID = PropertyHandler.getPropertyValue("SP_IN_PARAM_PRIMARY_COMPANY_ID");
	public static final String SP_IN_PARAM_SOURCE = PropertyHandler.getPropertyValue("SP_IN_PARAM_SOURCE");
	public static final String SP_IN_PARAM_TOPIC = PropertyHandler.getPropertyValue("SP_IN_PARAM_TOPIC");
	public static final String SP_IN_PARAM_FROM_DT = PropertyHandler.getPropertyValue("SP_IN_PARAM_FROM_DT");
	public static final String SP_IN_PARAM_TO_DT = PropertyHandler.getPropertyValue("SP_IN_PARAM_TO_DT");
	public static final String SP_IN_PARAM_SENTIMENT_TYPE = PropertyHandler.getPropertyValue("SP_IN_PARAM_SENTIMENT_TYPE");
	public static final String SP_IN_PARAM_SOURCE_ID = PropertyHandler.getPropertyValue("SP_IN_PARAM_SOURCE_ID");
	public static final String SP_IN_PARAM_INTERVAL = PropertyHandler.getPropertyValue("SP_IN_PARAM_INTERVAL");
	public static final String SP_IN_PARAM_CITY_ID =PropertyHandler.getPropertyValue("SP_IN_PARAM_CITY_ID");
	public static final String SP_IN_PARAM_ASPECT = PropertyHandler.getPropertyValue("SP_IN_PARAM_ASPECT");
	
	public static final String SP_IN_PARAM_PERIODIC_START_FROM = PropertyHandler.getPropertyValue("SP_IN_PARAM_PERIODIC_START_FROM");
	public static final String SP_IN_PARAM_PERIODIC_START_TO = PropertyHandler.getPropertyValue("SP_IN_PARAM_PERIODIC_START_TO");
	public static final String SP_IN_PARAM_PERIODIC_END_FROM = PropertyHandler.getPropertyValue("SP_IN_PARAM_PERIODIC_END_FROM");
	public static final String SP_IN_PARAM_PERIODIC_END_TO = PropertyHandler.getPropertyValue("SP_IN_PARAM_PERIODIC_END_TO");
	public static final String SP_IN_PARAM_FROM_DT_CURRENT = PropertyHandler.getPropertyValue("SP_IN_PARAM_FROM_DT_CURRENT");
	public static final String SP_IN_PARAM_TO_DT_CURRENT = PropertyHandler.getPropertyValue("SP_IN_PARAM_TO_DT_CURRENT");
	public static final String SP_IN_PARAM_FROM_DT_PREVIOUS = PropertyHandler.getPropertyValue("SP_IN_PARAM_FROM_DT_PREVIOUS");
	public static final String SP_IN_PARAM_TO_DT_PREVIOUS = PropertyHandler.getPropertyValue("SP_IN_PARAM_TO_DT_PREVIOUS");
	public static final String SP_IN_PARAM_DURATION = PropertyHandler.getPropertyValue("SP_IN_PARAM_DURATION");
	public static final String SP_IN_PARAM_OWNERSHIP_ID = PropertyHandler.getPropertyValue("SP_IN_PARAM_OWNERSHIP_ID");
	public static final String START_TIME = " 00:01";
	public static final String END_TIME = " 23:59";
	
	public static final String SP_IN_PARAM_SELECTED_DURATION = PropertyHandler.getPropertyValue("SP_IN_PARAM_SELECTED_DURATION");
	public static final String SP_IN_PARAM_CITY = PropertyHandler.getPropertyValue("SP_IN_PARAM_CITY");
	public static final String SP_IN_PARAM_OFFSET = PropertyHandler.getPropertyValue("SP_IN_PARAM_OFFSET");
	public static final String SP_IN_PARAM_LIMIT = PropertyHandler.getPropertyValue("SP_IN_PARAM_LIMIT");
	public static final String SP_IN_PARAM_USER_NAME = PropertyHandler.getPropertyValue("SP_IN_PARAM_USER_NAME");
	public static final String SP_IN_PARAM_PASSWORD = PropertyHandler.getPropertyValue("SP_IN_PARAM_PASSWORD");
	public static final String SP_IN_AUTHOR_NAME = PropertyHandler.getPropertyValue("SP_IN_AUTHOR_NAME");
	public static final String SP_IN_PARAM_SORTBY = PropertyHandler.getPropertyValue("SP_IN_PARAM_SORTBY");
	public static final String SP_IN_PARAM_SORT = PropertyHandler.getPropertyValue("SP_IN_PARAM_SORTBY");
	public static final String SP_IN_POST_TYPE_ID = PropertyHandler.getPropertyValue("SP_IN_POST_TYPE_ID");
	public static final String SP_IN_SEARCH_STRING = PropertyHandler.getPropertyValue("SP_IN_SEARCH_STRING");
	
	
	
	
		
	public static final String BLANK_STRING = PropertyHandler.getPropertyValue("BLANK_STRING");
	public static final String SP_WORD_CLOUD_SOCIAL_POST_LIST = PropertyHandler.getPropertyValue("SP_WORD_CLOUD_SOCIAL_POST_LIST");
	public static final String SP_SOCIAL_POST_FILTER_LIST = PropertyHandler.getPropertyValue("SP_SOCIAL_POST_FILTER_LIST");
	public static final String SP_SOURCE_WISE_STORES = PropertyHandler.getPropertyValue("SP_SOURCE_WISE_STORES");
	public static final String SP_SOURCE_WISE_STORES_CSV = PropertyHandler.getPropertyValue("SP_SOURCE_WISE_STORES_CSV");
	public static final String SP_BRAND_COMPARISON_MONTHLY = PropertyHandler.getPropertyValue("SP_BRAND_COMPARISON_MONTHLY");
	public static final String SP_IN_PARAM_SEARCH_TEXT = PropertyHandler.getPropertyValue("SP_IN_PARAM_SEARCH_TEXT");
	public static final String SP_IN_PARAM_ASSIGNEE_ID =  PropertyHandler.getPropertyValue("SP_IN_PARAM_ASSIGNEE_ID");
	public static final String SP_IN_PARAM_CURR_TIME = PropertyHandler.getPropertyValue("SP_IN_PARAM_CURR_TIME");
	public static final String SP_IN_PARAM_USER_ID =PropertyHandler.getPropertyValue("SP_IN_PARAM_USER_ID");
	public static final String SP_IN_PARAM_COMPANY_NAME =PropertyHandler.getPropertyValue("SP_IN_PARAM_COMPANY_NAME");
	public static final String SP_IN_PARAM_COMPANY_ID =PropertyHandler.getPropertyValue("SP_IN_PARAM_COMPANY_ID");
	public static final String SP_IN_PARAM_POST_ID = PropertyHandler.getPropertyValue("SP_IN_PARAM_POST_ID");
	public static final String SP_IN_PARAM_SENTIMENT_NUMBER = PropertyHandler.getPropertyValue("SP_IN_PARAM_SENTIMENT_NUMBER");
	public static final String SP_IN_PARAM_OLD_PASSWORD = PropertyHandler.getPropertyValue("SP_IN_PARAM_OLD_PASSWORD");
	public static final String SP_IN_PARAM_TICKET_ID = PropertyHandler.getPropertyValue("SP_IN_PARAM_TICKET_ID");
	public static final String SP_IN_PARAM_ASSIGNER_ID = PropertyHandler.getPropertyValue("SP_IN_PARAM_ASSIGNER_ID");
	public static final String SP_IN_PARAM_COMMENTS =  PropertyHandler.getPropertyValue("SP_IN_PARAM_COMMENTS");
	public static final String SP_IN_PARAM_TICKET_STATUS =  PropertyHandler.getPropertyValue("SP_IN_PARAM_TICKET_STATUS");
	public static final String SP_IN_PARAM_TICKET_PRIORITY =  PropertyHandler.getPropertyValue("SP_IN_PARAM_TICKET_PRIORITY");
	public static final String SP_IN_PARAM_DUE_DATE =  PropertyHandler.getPropertyValue("SP_IN_PARAM_DUE_DATE");
	public static final String SP_IN_PARAM_CLOSED_DATE =  PropertyHandler.getPropertyValue("SP_IN_PARAM_CLOSED_DATE");
	public static final String SP_IN_PARAM_ASPECT_NAME =  PropertyHandler.getPropertyValue("SP_IN_PARAM_ASPECT_NAME");
	
	public static final String SP_INVALID_REPORT_LIST = PropertyHandler.getPropertyValue("SP_INVALID_REPORT_LIST");
	
	
	
	public static final String ASPECTS_FOR_WORD_CLOUD = PropertyHandler.getPropertyValue("ASPECTS_FOR_WORD_CLOUD");
	public static final String ENTITIES_FOR_WORD_CLOUD = PropertyHandler.getPropertyValue("ENTITIES_FOR_WORD_CLOUD");
	
	
	public static final String WORD_CLOUD = PropertyHandler.getPropertyValue("WORD_CLOUD");
	public static final String SOCIAL_LISTENING = PropertyHandler.getPropertyValue("SOCIAL_LISTENING");
	public static final String PRICES_AND_OFFERS_CONTEXT = PropertyHandler.getPropertyValue("PRICES_AND_OFFERS_CONTEXT");
	public static final String PRICES_AND_OFFERS_ABR = PropertyHandler.getPropertyValue("PRICES_AND_OFFERS_ABR");
	public static final String HIBERNATE_KPI_DAO = PropertyHandler.getPropertyValue("HIBERNATE_KPI_DAO");
	public static final String JDBC_REVIEW_ANALYSIS_DAO = PropertyHandler.getPropertyValue("JDBC_REVIEW_ANALYSIS_DAO");
	
	public static final String JDBC_COMMON_DAO =PropertyHandler.getPropertyValue("JDBC_COMMON_DAO");
	
	
	public static final String RESULTS = "results";
	public static final String GET_ROWS = "getRows";
	public static final String OUTPUT = "output";
	public static final String GET_COUNT = "getCount";
	public static final String COUNT = "count";
	public static final String COLUMN_NAMES = "column_names";
	public static final String ROWS = "rows";
	public static final String SOURCE_ID = "source_id";
	
	public static final String SP_NAME = ">>>>>>>SP Name :::: ";
	public static final String BEAN_NAME = "kpiParentBean>>>>>>";
	public static final String TWEET = "tweet";
	public static final String POST = "post";
	public static final String COMPANY_ID = "company_id";
	public static final String CALL = "CALL "; 
	public static final String ERROR_LOG = "Error occurred in ";
	public static final String SQL_LOG ="SQL :::::: ";
	public static final String COLUMN_NAME_LOG = "===== Column names :====";
	public static final String IS_ASPECT_ENTITY = "inIsAspectEntity";
	public static final String MESSAGE ="message";
	
	
	public static final String SELECT_USER_TOKEN_TWITTER = "SELECT app_key,app_secret,token,token_secret FROM token_master_automated_reply WHERE source_type_id= 2";
		
	public static final String SELECT_POST_DETAILS1 = "select author_name,post_text FROM ";
	public static final String SELECT_POST_DETAILS2 = "social_data_";
	public static final String SELECT_POST_DETAILS3 = " where post_id = ? ";
}
