package com.abzooba.trackR24.models;

import java.util.Map;

import org.json.simple.JSONObject;

/**
 * 
 * represents an object with an ID and properties (name, value pairs)
 * 
 * @author Krishna Kumar
 *
 */
public interface IdentifiableSerializableObject {

	/**
	 * 
	 * gets object ID
	 * @return object ID
	 */
	public long getID();
	
	/**
	 * 
	 * sets object ID
	 * @param ID object ID
	 */
	public void setID(long ID);
	
	/**
	 * 
	 * gets object classname
	 * @return object classname
	 */
	public String getClassname ();

	/**
	 * 
	 * sets object classname
	 * @param ID object classname
	 */
	public void setClassname (String classname);
	
	/**
	 * 
	 * gets properties 
	 * @return properties map of name, value pairs 
	 */
	public Map <String, Object> getProperties ();
	
	/**
	 * 
	 * sets properties
	 * @param properties map of name, value pairs
	 */
	public void setProperties (Map <String, Object> properties);
	
	/**
	 * 
	 * adds a property
	 * @param name property name
	 * @param value property value
	 */
	public void addProperty (String name, Object value);
	
	/**
	 * 
	 * gets value of a property
	 * @param name property name
	 * @return property value
	 */
	public Object getProperty (String name);

	/**
	 * 
	 * loads object from JSON 
	 * @param dsObj JSON object
	 */
	public void fromJSON (JSONObject dsObj) throws ObjectInstantiationException;
	
	/**
	 * 
	 * saves object to JSON
	 * @return JSON object
	 */
	public JSONObject toJSON();
	

}
