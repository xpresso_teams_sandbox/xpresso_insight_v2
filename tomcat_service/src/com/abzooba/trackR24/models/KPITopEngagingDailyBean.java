package com.abzooba.trackR24.models;

public class KPITopEngagingDailyBean {
	
	private Integer updateHour;
	private Integer avgCount;
	public Integer getUpdateHour() {
		return updateHour;
	}
	public void setUpdateHour(Integer updateHour) {
		this.updateHour = updateHour;
	}
	public Integer getAvgCount() {
		return avgCount;
	}
	public void setAvgCount(Integer avgCount) {
		this.avgCount = avgCount;
	}
	
	

}
