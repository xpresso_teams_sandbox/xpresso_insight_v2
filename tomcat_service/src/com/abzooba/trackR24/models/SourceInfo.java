package com.abzooba.trackR24.models;

import javax.persistence.Transient;

/**
* Author Name: Kunal Kashyap
* Create Date: 20-09-2016
* Last Modified: Kunal Kashyap: 20-09-2016
* Class Insight: This is the bean class used for adding and fetching details of user in add info
* 				 and settings page respectively.
*/
public class SourceInfo {

	private int sourceTypeId;
	private String sourceURL;
	private String name;
	@Transient
	private int sourceId;

	public int getSourceId() {
		return sourceId;
	}

	public void setSourceId(int sourceId) {
		this.sourceId = sourceId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSourceTypeId() {
		return sourceTypeId;
	}

	public void setSourceTypeId(int sourceTypeId) {
		this.sourceTypeId = sourceTypeId;
	}

	public String getSourceURL() {
		return sourceURL;
	}

	public void setSourceURL(String sourceURL) {
		this.sourceURL = sourceURL;
	}

}
