package com.abzooba.trackR24.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
* Author Name: Kunal Kashyap
* Create Date: 05-09-2016
* Last Modified: Kunal Kashyap: 05-09-2016
* Class Insight: This is the bean class for mapping with source_master table of database.
*/
@Entity
@Table(name = "source_master")
public class SourceMasterBean {

	@Id
	private int source_id;
	private int source_type_id;
	private int domain_id;
	private String page_name;
	private String source_category;
	private String source_url;
	private int live_source;
	private int xpresso_enable;
	private int live_status;
	private Date last_streaming;
	private Date last_streaming_comment;
	private Date create_datetime;
	private Date update_datetime;

	public int getSource_id() {
		return source_id;
	}

	public void setSource_id(int source_id) {
		this.source_id = source_id;
	}

	public int getSource_type_id() {
		return source_type_id;
	}

	public void setSource_type_id(int source_type_id) {
		this.source_type_id = source_type_id;
	}

	public int getDomain_id() {
		return domain_id;
	}

	public void setDomain_id(int domain_id) {
		this.domain_id = domain_id;
	}

	public String getPage_name() {
		return page_name;
	}

	public void setPage_name(String page_name) {
		this.page_name = page_name;
	}

	public String getSource_category() {
		return source_category;
	}

	public void setSource_category(String source_category) {
		this.source_category = source_category;
	}

	public String getSource_url() {
		return source_url;
	}

	public void setSource_url(String source_url) {
		this.source_url = source_url;
	}

	public int getLive_source() {
		return live_source;
	}

	public void setLive_source(int live_source) {
		this.live_source = live_source;
	}

	public int getXpresso_enable() {
		return xpresso_enable;
	}

	public void setXpresso_enable(int xpresso_enable) {
		this.xpresso_enable = xpresso_enable;
	}

	public int getLive_status() {
		return live_status;
	}

	public void setLive_status(int live_status) {
		this.live_status = live_status;
	}

	public Date getLast_streaming() {
		return last_streaming;
	}

	public void setLast_streaming(Date last_streaming) {
		this.last_streaming = last_streaming;
	}

	public Date getLast_streaming_comment() {
		return last_streaming_comment;
	}

	public void setLast_streaming_comment(Date last_streaming_comment) {
		this.last_streaming_comment = last_streaming_comment;
	}

	public Date getCreate_datetime() {
		return create_datetime;
	}

	public void setCreate_datetime(Date create_datetime) {
		this.create_datetime = create_datetime;
	}

	public Date getUpdate_datetime() {
		return update_datetime;
	}

	public void setUpdate_datetime(Date update_datetime) {
		this.update_datetime = update_datetime;
	}

}
