package com.abzooba.trackR24.models;

import java.util.List;

/**
* Author Name: Kunal Kashyap
* Create Date: 18-10-2016
* Last Modified: Kunal Kashyap: 15-11-2016
* Class Insight: This is the parent bean class for dao layer for KPI page.
*/
public class KPIServiceBean {

	private KPIFacebookBean kpiFacebookBean;
	private KPITwitterBean kpiTwitterBean;
	private List<KPIFollowerFanGrowthRateBean> kpiFollowerFanGrowthRateBeanList;
	private List<KPITopEngagingTweetsBean> kpiTopEngagingTweetsBeanList;
	private List<KPITopEngagingPostsBean> kpiTopEngagingPostsBeanList;
	private List<KPIMostEngagingTweetsPosts> kpiMostEngagingTweetsPostsList;
	private List<KPIPerTweetPostInteractionBean> kpiPerTweetPostInteractionBeanList;
	private List<KPITopEngagingMonthlyBean> kpiTopEngagingMonthlyBeanList;
	private List<KPITopEngagingDailyBean> kpiTopEngagingDailyBeanList;
	
	public List<KPITopEngagingDailyBean> getKpiTopEngagingDailyBeanList() {
		return kpiTopEngagingDailyBeanList;
	}
	public void setKpiTopEngagingDailyBeanList(
			List<KPITopEngagingDailyBean> kpiTopEngagingDailyBeanList) {
		this.kpiTopEngagingDailyBeanList = kpiTopEngagingDailyBeanList;
	}
	public List<KPITopEngagingMonthlyBean> getKpiTopEngagingMonthlyBeanList() {
		return kpiTopEngagingMonthlyBeanList;
	}
	public void setKpiTopEngagingMonthlyBeanList(
			List<KPITopEngagingMonthlyBean> kpiTopEngagingMonthlyBeanList) {
		this.kpiTopEngagingMonthlyBeanList = kpiTopEngagingMonthlyBeanList;
	}
	public KPIFacebookBean getKpiFacebookBean() {
		return kpiFacebookBean;
	}
	public void setKpiFacebookBean(KPIFacebookBean kpiFacebookBean) {
		this.kpiFacebookBean = kpiFacebookBean;
	}
	public KPITwitterBean getKpiTwitterBean() {
		return kpiTwitterBean;
	}
	public void setKpiTwitterBean(KPITwitterBean kpiTwitterBean) {
		this.kpiTwitterBean = kpiTwitterBean;
	}
	public List<KPIFollowerFanGrowthRateBean> getKpiFollowerFanGrowthRateBeanList() {
		return kpiFollowerFanGrowthRateBeanList;
	}
	public void setKpiFollowerFanGrowthRateBeanList(
			List<KPIFollowerFanGrowthRateBean> kpiFollowerFanGrowthRateBeanList) {
		this.kpiFollowerFanGrowthRateBeanList = kpiFollowerFanGrowthRateBeanList;
	}
	public List<KPITopEngagingTweetsBean> getKpiTopEngagingTweetsBeanList() {
		return kpiTopEngagingTweetsBeanList;
	}
	public void setKpiTopEngagingTweetsBeanList(
			List<KPITopEngagingTweetsBean> kpiTopEngagingTweetsBeanList) {
		this.kpiTopEngagingTweetsBeanList = kpiTopEngagingTweetsBeanList;
	}
	public List<KPITopEngagingPostsBean> getKpiTopEngagingPostsBeanList() {
		return kpiTopEngagingPostsBeanList;
	}
	public void setKpiTopEngagingPostsBeanList(
			List<KPITopEngagingPostsBean> kpiTopEngagingPostsBeanList) {
		this.kpiTopEngagingPostsBeanList = kpiTopEngagingPostsBeanList;
	}
	public List<KPIMostEngagingTweetsPosts> getKpiMostEngagingTweetsPostsList() {
		return kpiMostEngagingTweetsPostsList;
	}
	public void setKpiMostEngagingTweetsPostsList(
			List<KPIMostEngagingTweetsPosts> kpiMostEngagingTweetsPostsList) {
		this.kpiMostEngagingTweetsPostsList = kpiMostEngagingTweetsPostsList;
	}
	public List<KPIPerTweetPostInteractionBean> getKpiPerTweetPostInteractionBeanList() {
		return kpiPerTweetPostInteractionBeanList;
	}
	public void setKpiPerTweetPostInteractionBeanList(
			List<KPIPerTweetPostInteractionBean> kpiPerTweetPostInteractionBeanList) {
		this.kpiPerTweetPostInteractionBeanList = kpiPerTweetPostInteractionBeanList;
	}
	 
	
	 
	 
}
