package com.abzooba.trackR24.models;

public interface Result extends IdentifiableSerializableObject {

	/**
	 * 
	 * gets name
	 * @return name
	 */
	public String getName();

	/**
	 * 
	 * sets name
	 * @param name name
	 */
	public void setName(String name);

}
