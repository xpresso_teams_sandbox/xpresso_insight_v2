package com.abzooba.trackR24.models;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
* Author Name: Kunal Kashyap
* Create Date: 05-09-2016
* Last Modified: Kunal Kashyap: 05-09-2016
* Class Insight: This is the bean class used for fetching user details in settings page.
*/
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserInfo {

	private List<User> usersList;
	private List<SourceInfo> sourcesDetailsList;
	private List<Competitor> competitorsList;
	private List<TopicProfile> topicProfiles;

	public List<TopicProfile> getTopicProfiles() {
		return topicProfiles;
	}

	public void setTopicProfiles(List<TopicProfile> topicProfiles) {
		this.topicProfiles = topicProfiles;
	}

	public List<User> getUsersList() {
		return usersList;
	}

	public void setUsersList(List<User> usersList) {
		this.usersList = usersList;
	}

	public List<SourceInfo> getSourcesDetailsList() {
		return sourcesDetailsList;
	}

	public void setSourcesDetailsList(List<SourceInfo> sourcesDetailsList) {
		this.sourcesDetailsList = sourcesDetailsList;
	}

	public List<Competitor> getCompetitorsList() {
		return competitorsList;
	}

	public void setCompetitorsList(List<Competitor> competitorsList) {
		this.competitorsList = competitorsList;
	}

}
