package com.abzooba.trackR24.models;

import java.util.List;

/**
* Author Name: Kunal Kashyap
* Create Date: 05-09-2016
* Last Modified: Kunal Kashyap: 05-09-2016
* Class Insight: This is the parent bean class used for fetching user details in settings page.
*/
public class UserInfoBean {

	private List<UserMasterBean> userMasterBeanList;
	private List<SourceMasterBean> sourceMasterBeanList;
	private List<CompanySourceMapBean> companySourceMapBeanList;
	private List<CompanyMasterBean> companyMasterBeanList;
	private List<AccountCompanyMapBean> accountCompanyMapBeanList;
	private List<CompetitorBean> competitorBeanList;

	public List<CompetitorBean> getCompetitorBeanList() {
		return competitorBeanList;
	}

	public void setCompetitorBeanList(List<CompetitorBean> competitorBeanList) {
		this.competitorBeanList = competitorBeanList;
	}

	public List<UserMasterBean> getUserMasterBeanList() {
		return userMasterBeanList;
	}

	public void setUserMasterBeanList(List<UserMasterBean> userMasterBeanList) {
		this.userMasterBeanList = userMasterBeanList;
	}

	public List<SourceMasterBean> getSourceMasterBeanList() {
		return sourceMasterBeanList;
	}

	public void setSourceMasterBeanList(List<SourceMasterBean> sourceMasterBeanList) {
		this.sourceMasterBeanList = sourceMasterBeanList;
	}

	public List<CompanySourceMapBean> getCompanySourceMapBeanList() {
		return companySourceMapBeanList;
	}

	public void setCompanySourceMapBeanList(List<CompanySourceMapBean> companySourceMapBeanList) {
		this.companySourceMapBeanList = companySourceMapBeanList;
	}

	public List<CompanyMasterBean> getCompanyMasterBeanList() {
		return companyMasterBeanList;
	}

	public void setCompanyMasterBeanList(List<CompanyMasterBean> companyMasterBeanList) {
		this.companyMasterBeanList = companyMasterBeanList;
	}

	public List<AccountCompanyMapBean> getAccountCompanyMapBeanList() {
		return accountCompanyMapBeanList;
	}

	public void setAccountCompanyMapBeanList(List<AccountCompanyMapBean> accountCompanyMapBeanList) {
		this.accountCompanyMapBeanList = accountCompanyMapBeanList;
	}

}
