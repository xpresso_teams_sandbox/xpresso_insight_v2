 package com.abzooba.trackR24.models;

 /**
 * Author Name: Kunal Kashyap
 * Create Date: 21-10-2016
 * Last Modified: Kunal Kashyap: 21-10-2016
 * Class Insight: This is the bean class for follower and fan growth rate of KPI page.
 */
public class KPIFollowerFanGrowthRateBean {

 
	private String kpi_date;
	private float growth_rate;
	public String getKpi_date() {
		return kpi_date;
	}
	public void setKpi_date(String kpi_date) {
		this.kpi_date = kpi_date;
	}
	public float getGrowth_rate() {
		return growth_rate;
	}
	public void setGrowth_rate(float growth_rate) {
		this.growth_rate = growth_rate;
	}

	 

}
