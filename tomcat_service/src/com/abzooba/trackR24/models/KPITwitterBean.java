package com.abzooba.trackR24.models;

/**
* Author Name: Kunal Kashyap
* Create Date: 18-10-2016
* Last Modified: Kunal Kashyap: 18-10-2016
* Class Insight: This is the bean class for KPI twitter ribbon.
*/
public class KPITwitterBean {

	private int total_followers;
	private int total_followings;
	private int page_like;
	private int post_id_count;

	public int getTotal_followers() {
		return total_followers;
	}

	public void setTotal_followers(int total_followers) {
		this.total_followers = total_followers;
	}

	public int getTotal_followings() {
		return total_followings;
	}

	public void setTotal_followings(int total_followings) {
		this.total_followings = total_followings;
	}

	public int getPage_like() {
		return page_like;
	}

	public void setPage_like(int page_like) {
		this.page_like = page_like;
	}

	public int getPost_id_count() {
		return post_id_count;
	}

	public void setPost_id_count(int post_id_count) {
		this.post_id_count = post_id_count;
	}

}
