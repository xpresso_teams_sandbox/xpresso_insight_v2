package com.abzooba.trackR24.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
* Author Name: Kunal Kashyap
* Create Date: 05-09-2016
* Last Modified: Kunal Kashyap: 05-09-2016
* Class Insight: This is the bean class for mapping with user_master table of database.
*/
@Entity
@Table(name = "user_master")
public class UserMasterBean {

	@Id
	private String user_id;
	private String user_name;
	private int account_id;
	private String user_password;
	private int company_id;
	private String designation;
	private int country_id;
	private int timezone_id;
	private int role_id;
	private int isConfirmed;
	private Date create_datetime;
	private Date update_datetime;
	private String user_token;
	private String company_logo_image_url;

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public int getAccount_id() {
		return account_id;
	}

	public void setAccount_id(int account_id) {
		this.account_id = account_id;
	}

	public String getUser_password() {
		return user_password;
	}

	public void setUser_password(String user_password) {
		this.user_password = user_password;
	}

	public int getCompany_id() {
		return company_id;
	}

	public void setCompany_id(int company_id) {
		this.company_id = company_id;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public int getCountry_id() {
		return country_id;
	}

	public void setCountry_id(int country_id) {
		this.country_id = country_id;
	}

	public int getTimezone_id() {
		return timezone_id;
	}

	public void setTimezone_id(int timezone_id) {
		this.timezone_id = timezone_id;
	}

	public int getRole_id() {
		return role_id;
	}

	public void setRole_id(int role_id) {
		this.role_id = role_id;
	}

	public int getIsConfirmed() {
		return isConfirmed;
	}

	public void setIsConfirmed(int isConfirmed) {
		this.isConfirmed = isConfirmed;
	}

	public Date getCreate_datetime() {
		return create_datetime;
	}

	public void setCreate_datetime(Date create_datetime) {
		this.create_datetime = create_datetime;
	}

	public Date getUpdate_datetime() {
		return update_datetime;
	}

	public void setUpdate_datetime(Date update_datetime) {
		this.update_datetime = update_datetime;
	}

	public String getUser_token() {
		return user_token;
	}

	public void setUser_token(String user_token) {
		this.user_token = user_token;
	}

	public String getCompany_logo_image_url() {
		return company_logo_image_url;
	}

	public void setCompany_logo_image_url(String company_logo_image_url) {
		this.company_logo_image_url = company_logo_image_url;
	}

}
