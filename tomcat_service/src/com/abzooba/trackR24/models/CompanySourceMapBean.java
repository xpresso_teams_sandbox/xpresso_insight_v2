package com.abzooba.trackR24.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
* Author Name: Kunal Kashyap
* Create Date: 05-09-2016
* Last Modified: Kunal Kashyap: 05-09-2016
* Class Insight: This is the bean class for mapping with company_source_map table of database.
*/
@Entity
@Table(name = "company_source_map")
public class CompanySourceMapBean implements Serializable {

	@Id
	private int company_id;
	@Id
	private int source_id;
	private Date create_datetime;
	private Date update_datetime;

	public int getCompany_id() {
		return company_id;
	}

	public void setCompany_id(int company_id) {
		this.company_id = company_id;
	}

	public int getSource_id() {
		return source_id;
	}

	public void setSource_id(int source_id) {
		this.source_id = source_id;
	}

	public Date getCreate_datetime() {
		return create_datetime;
	}

	public void setCreate_datetime(Date create_datetime) {
		this.create_datetime = create_datetime;
	}

	public Date getUpdate_datetime() {
		return update_datetime;
	}

	public void setUpdate_datetime(Date update_datetime) {
		this.update_datetime = update_datetime;
	}

	@Override
	public String toString() {
		return "CompanySourceMapBean [company_id=" + company_id + ", source_id=" + source_id + ", create_datetime=" + create_datetime + ", update_datetime=" + update_datetime + "]";
	}

}
