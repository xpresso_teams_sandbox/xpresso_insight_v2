package com.abzooba.trackR24.models;

import java.util.List;

/**
* Author Name: Kunal Kashyap
* Create Date: 15-09-2016
* Last Modified: Kunal Kashyap: 15-09-2016
* Class Insight: This is the bean class used for fetching the details of user in settings page .
*/
public class Competitor {
	
	private String competitorName;
	private List<SourceInfo> competitorSources;
	
	public String getCompetitorName() {
		return competitorName;
	}
	public void setCompetitorName(String competitorName) {
		this.competitorName = competitorName;
	}
	public List<SourceInfo> getCompetitorSources() {
		return competitorSources;
	}
	public void setCompetitorSources(List<SourceInfo> competitorSources) {
		this.competitorSources = competitorSources;
	}
	
	
}
