package com.abzooba.trackR24.models;


public class KPITopEngagingMonthlyBean {
	
	private Integer weekCount;
	private Integer avgCount;
	public Integer getWeekCount() {
		return weekCount;
	}
	public void setWeekCount(Integer weekCount) {
		this.weekCount = weekCount;
	}
	public Integer getAvgCount() {
		return avgCount;
	}
	public void setAvgCount(Integer avgCount) {
		this.avgCount = avgCount;
	}

}
