package com.abzooba.trackR24.models;

/**
 * 
 * Exception thrown when unable to instantiate object
 * @author Krishna Kumar
 *
 */
@SuppressWarnings("serial")
public class ObjectInstantiationException extends Exception {

	/**
	 * 
	 * constructor
	 * @param message exception message
	 */
	public ObjectInstantiationException(String message) {
		super(message);
	}
}
