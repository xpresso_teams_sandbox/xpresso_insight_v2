/**
 * This model object binds in UI with the Table in the Entities Table in Analytics/Explore page.
 */

package com.abzooba.trackR24.models;

public class PostWiseEngagementSentimentObject {

	private String  post_id;
	private String  post_text;
	private String  post_datetime;
	private double engagement_score;
	private double posetive;
	private double negative;
	private double neutral;
	private String  post_url;
	
	public double getEngagement_score() {
		return engagement_score;
	}
	public void setEngagement_score(double engagement_score) {
		this.engagement_score = engagement_score;
	}
	public String getPost_datetime() {
		return post_datetime;
	}
	public void setPost_datetime(String post_datetime) {
		this.post_datetime = post_datetime;
	}
	public String getPost_text() {
		return post_text;
	}
	public void setPost_text(String post_text) {
		this.post_text = post_text;
	}
	public String getPost_id() {
		return post_id;
	}
	public void setPost_id(String post_id) {
		this.post_id = post_id;
	}
	public double getPosetive() {
		return posetive;
	}
	public void setPosetive(double posetive) {
		this.posetive = posetive;
	}
	public double getNegative() {
		return negative;
	}
	public void setNegative(double negative) {
		this.negative = negative;
	}
	public double getNeutral() {
		return neutral;
	}
	public void setNeutral(double neutral) {
		this.neutral = neutral;
	}
	
	public String getPost_url() {
		return post_url;
	}
	public void setPost_url(String post_url) {
		this.post_url = post_url;
	}
	

	
}
