package com.abzooba.trackR24.models;

import java.util.List;

/**
* Author Name: Kunal Kashyap
* Create Date: 05-09-2016
* Last Modified: Kunal Kashyap: 05-09-2016
* Class Insight: This is the bean class used for fetching user details in settings page.
*/
public class TopicProfile {

	private String profileType;
	private List<Integer> profileDetails;

	public String getProfileType() {
		return profileType;
	}

	public void setProfileType(String profileType) {
		this.profileType = profileType;
	}

	public List<Integer> getProfileDetails() {
		return profileDetails;
	}

	public void setProfileDetails(List<Integer> profileDetails) {
		this.profileDetails = profileDetails;
	}

	@Override
	public String toString() {
		return "TopicProfile [profileType=" + profileType + ", profileDetails=" + profileDetails + "]";
	}

}
