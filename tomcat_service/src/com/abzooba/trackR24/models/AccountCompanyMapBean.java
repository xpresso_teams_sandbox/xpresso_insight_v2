package com.abzooba.trackR24.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
* Author Name: Kunal Kashyap
* Create Date: 05-09-2016
* Last Modified: Kunal Kashyap: 05-09-2016
* Class Insight: This is the bean class for mapping with account_company_map table of database.
*/
@Entity
@Table(name = "account_company_map")
public class AccountCompanyMapBean implements Serializable {

	@Id
	private int account_id;
	@Id
	private int company_id;
	private int primary_company;
	private Date create_datetime;
	private Date update_datetime;

	public int getAccount_id() {
		return account_id;
	}

	public void setAccount_id(int account_id) {
		this.account_id = account_id;
	}

	public int getCompany_id() {
		return company_id;
	}

	public void setCompany_id(int company_id) {
		this.company_id = company_id;
	}

	public int getPrimary_company() {
		return primary_company;
	}

	public void setPrimary_company(int primary_company) {
		this.primary_company = primary_company;
	}

	public Date getCreate_datetime() {
		return create_datetime;
	}

	public void setCreate_datetime(Date create_datetime) {
		this.create_datetime = create_datetime;
	}

	public Date getUpdate_datetime() {
		return update_datetime;
	}

	public void setUpdate_datetime(Date update_datetime) {
		this.update_datetime = update_datetime;
	}

}
