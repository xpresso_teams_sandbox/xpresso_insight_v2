package com.abzooba.trackR24.models;

/**
* Author Name: Kunal Kashyap
* Create Date: 18-10-2016
* Last Modified: Kunal Kashyap: 15-11-2016
* Class Insight: This is the bean class for top engaging posts for KPI page.
*/
public class KPITopEngagingPostsBean {
	
	private String post_id;
	private String author_name;
	private String post_text;
	private int likes_count;
	private int comments_count;
	private int reply_count;
	private int love_count;
	private int haha_count;
	private int wow_count;
	private int sad_count;
	private int angry_count;
	private int thankful_count;
	
	public String getPost_id() {
		return post_id;
	}
	public void setPost_id(String post_id) {
		this.post_id = post_id;
	}
	public String getAuthor_name() {
		return author_name;
	}
	public void setAuthor_name(String author_name) {
		this.author_name = author_name;
	}
	public String getPost_text() {
		return post_text;
	}
	public void setPost_text(String post_text) {
		this.post_text = post_text;
	}
	public int getLikes_count() {
		return likes_count;
	}
	public void setLikes_count(int likes_count) {
		this.likes_count = likes_count;
	}
	public int getComments_count() {
		return comments_count;
	}
	public void setComments_count(int comments_count) {
		this.comments_count = comments_count;
	}
	public int getReply_count() {
		return reply_count;
	}
	public void setReply_count(int reply_count) {
		this.reply_count = reply_count;
	}
	public int getLove_count() {
		return love_count;
	}
	public void setLove_count(int love_count) {
		this.love_count = love_count;
	}
	public int getHaha_count() {
		return haha_count;
	}
	public void setHaha_count(int haha_count) {
		this.haha_count = haha_count;
	}
	public int getWow_count() {
		return wow_count;
	}
	public void setWow_count(int wow_count) {
		this.wow_count = wow_count;
	}
	public int getSad_count() {
		return sad_count;
	}
	public void setSad_count(int sad_count) {
		this.sad_count = sad_count;
	}
	public int getAngry_count() {
		return angry_count;
	}
	public void setAngry_count(int angry_count) {
		this.angry_count = angry_count;
	}
	public int getThankful_count() {
		return thankful_count;
	}
	public void setThankful_count(int thankful_count) {
		this.thankful_count = thankful_count;
	}
	
	
	
	
	
	

}
