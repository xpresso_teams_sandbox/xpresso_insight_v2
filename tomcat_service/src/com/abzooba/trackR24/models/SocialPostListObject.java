package com.abzooba.trackR24.models;

import java.util.HashMap;
import java.util.Map;

public class SocialPostListObject {

	private Integer postTypeId;
	private String postType;
	private Integer sourceTypeId;
	private String sourceTypeName;
	private String sourceTypeCode;
	private String postDateTime;
	private String postText;
	private String postUrl;
	private String postId;
	private String overallSentiment;
	private String city;
	private String totalCount;
	private String hasTicket;
	private Map<String, String> categorizedSentiment = new HashMap<>();

	public Integer getPostTypeId() {
		return postTypeId;
	}

	public void setPostTypeId(Integer postTypeId) {
		this.postTypeId = postTypeId;
	}
	
	public String getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(String totalCount) {
		this.totalCount = totalCount;
	}


	public String getPostType() {
		return postType;
	}

	public void setPostType(String postType) {
		this.postType = postType;
	}

	public Integer getSourceTypeId() {
		return sourceTypeId;
	}

	public void setSourceTypeId(Integer sourceTypeId) {
		this.sourceTypeId = sourceTypeId;
	}

	public String getSourceTypeName() {
		return sourceTypeName;
	}

	public String getSourceTypeCode() {
		return sourceTypeCode;
	}

	public void setSourceTypeCode(String sourceTypeCode) {
		this.sourceTypeCode = sourceTypeCode;
	}

	public void setSourceTypeName(String sourceTypeName) {
		this.sourceTypeName = sourceTypeName;
	}

	public String getPostDateTime() {
		return postDateTime;
	}

	public void setPostDateTime(String postDateTime) {
		this.postDateTime = postDateTime;
	}

	public String getPostText() {
		return postText;
	}

	public void setPostText(String postText) {
		this.postText = postText;
	}

	public String getPostUrl() {
		return postUrl;
	}

	public void setPostUrl(String postUrl) {
		this.postUrl = postUrl;
	}

	public String getPostId() {
		return postId;
	}

	public void setPostId(String postId) {
		this.postId = postId;
	}

	public String getOverallSentiment() {
		return overallSentiment;
	}

	public void setOverallSentiment(String overallSentiment) {
		this.overallSentiment = overallSentiment;
	}

	public Map<String, String> getCategorizedSentiment() {
		return categorizedSentiment;
	}

	public void setCategorizedSentiment(Map<String, String> categorizedSentiment) {
		this.categorizedSentiment = categorizedSentiment;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getHasTicket() {
		return hasTicket;
	}

	public void setHasTicket(String hasTicket) {
		this.hasTicket = hasTicket;
	}

}
