package com.abzooba.trackR24.models;

import java.util.List;

/**
* Author Name: Kunal Kashyap
* Create Date: 15-09-2016
* Last Modified: Kunal Kashyap: 15-09-2016
* Class Insight: This is the bean class used for adding details of user in add info page.
*/
public class CompetitorBean {
	private String company_name;
	private int sourceTypeId;
	private String sourceURL;
	private List<SourceInfo> competitorSources;

	public List<SourceInfo> getCompetitorSources() {
		return competitorSources;
	}

	public void setCompetitorSources(List<SourceInfo> competitorSources) {
		this.competitorSources = competitorSources;
	}

	public String getCompany_name() {
		return company_name;
	}

	public void setCompany_name(String company_name) {
		this.company_name = company_name;
	}

	public int getSourceTypeId() {
		return sourceTypeId;
	}

	public void setSourceTypeId(int sourceTypeId) {
		this.sourceTypeId = sourceTypeId;
	}

	public String getSourceURL() {
		return sourceURL;
	}

	public void setSourceURL(String sourceURL) {
		this.sourceURL = sourceURL;
	}

}
