package com.abzooba.trackR24.models;

/**
* Author Name: Kunal Kashyap
* Create Date: 05-09-2016
* Last Modified: Kunal Kashyap: 05-09-2016
* Class Insight: This is the bean class used for fetching user details in settings page.
*/
public class User {

	private String userName;
	private String userEmail;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

}
