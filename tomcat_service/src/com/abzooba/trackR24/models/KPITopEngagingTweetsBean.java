package com.abzooba.trackR24.models;

/**
* Author Name: Kunal Kashyap
* Create Date: 18-10-2016
* Last Modified: Kunal Kashyap: 15-11-2016
* Class Insight: This is the bean class for top engaging tweets for KPI page.
*/
public class KPITopEngagingTweetsBean {

	private String post_id;
	private String author_name;
	private String post_text;
	private Integer count;
	private int retweet_count;
	private int love_count;

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public String getPost_id() {
		return post_id;
	}

	public void setPost_id(String post_id) {
		this.post_id = post_id;
	}

	public String getAuthor_name() {
		return author_name;
	}

	public void setAuthor_name(String author_name) {
		this.author_name = author_name;
	}

	public String getPost_text() {
		return post_text;
	}

	public void setPost_text(String post_text) {
		this.post_text = post_text;
	}

	public int getRetweet_count() {
		return retweet_count;
	}

	public void setRetweet_count(int retweet_count) {
		this.retweet_count = retweet_count;
	}

	public int getLove_count() {
		return love_count;
	}

	public void setLove_count(int love_count) {
		this.love_count = love_count;
	}

}
