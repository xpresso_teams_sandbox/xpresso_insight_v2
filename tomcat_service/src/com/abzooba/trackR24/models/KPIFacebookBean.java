package com.abzooba.trackR24.models;

/**
* Author Name: Kunal Kashyap
* Create Date: 18-10-2016
* Last Modified: Kunal Kashyap: 18-10-2016
* Class Insight: This is the bean class for KPI facebook ribbon.
*/
public class KPIFacebookBean {

	private int talking_about_count;
	private int page_like;
	private int new_like;
	private float rate_of_fan_growth;

	public int getTalking_about_count() {
		return talking_about_count;
	}

	public void setTalking_about_count(int talking_about_count) {
		this.talking_about_count = talking_about_count;
	}

	public int getPage_like() {
		return page_like;
	}

	public void setPage_like(int page_like) {
		this.page_like = page_like;
	}

	public int getNew_like() {
		return new_like;
	}

	public void setNew_like(int new_like) {
		this.new_like = new_like;
	}

	public float getRate_of_fan_growth() {
		return rate_of_fan_growth;
	}

	public void setRate_of_fan_growth(float rate_of_fan_growth) {
		this.rate_of_fan_growth = rate_of_fan_growth;
	}

}
