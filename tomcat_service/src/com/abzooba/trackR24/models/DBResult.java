package com.abzooba.trackR24.models;

import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.abzooba.trackR24.util.PropertyHandler;

/**
 * 
 * represents results of a database query
 * @author Krishna Kumar
 *
 */
public class DBResult extends AbstractResult {

	// column names
	ArrayList<String> columnNames = new ArrayList<>();

	// rows
	ArrayList<ArrayList<Object>> rows = new ArrayList<ArrayList<Object>>();
	
	protected int count = 0;

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	/**
	 * 
	 * adds column name to result
	 * @param columnName column name to be added
	 */
	
	public void addColumnName(String columnName) {
		columnNames.add(columnName);
	}

	public ArrayList getColumnNames() {
		return columnNames;
	}

	/**
	 * 
	 * adds a row to the result
	 * @param row row to be added
	 */
	public void addRow(ArrayList<Object> row) {
		rows.add(row);
	}

	public ArrayList<ArrayList<Object>> getRows() {
		return rows;
	}

	@Override
	/**
	 * 
	 * loads object from JSON 
	 * @param obj JSON object
	 */
	public void fromJSON(JSONObject obj) throws ObjectInstantiationException {
		super.fromJSON(obj);

		// get column names
		JSONArray columnNamesObj = (JSONArray) obj.get(PropertyHandler.getPropertyValue("ATTRIBUTE_COLUMNNAMES"));
		int i, numColumns = columnNamesObj.size();
		for (i = 0; i < numColumns; i++)
			columnNames.add((String) columnNamesObj.get(i));

		// get rows
		JSONArray rowsObj = (JSONArray) obj.get(PropertyHandler.getPropertyValue("ATTRIBUTE_ROWS"));
		int numRows = rowsObj.size();
		for (i = 0; i < numRows; i++) {
			ArrayList<Object> row = new ArrayList<Object>();
			JSONObject rowObj = (JSONObject) rowsObj.get(i);
			JSONArray colsObj = (JSONArray) rowObj.get(PropertyHandler.getPropertyValue("ATTRIBUTE_ROW"));
			int numCols = colsObj.size();
			for (int j = 0; j < numCols; j++)
				row.add(colsObj.get(i));
			addRow(row);
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	/**
	 * 
	 * saves object to JSON
	 * @return JSON object
	 */
	public JSONObject toJSON() {
		JSONObject obj = super.toJSON();

		// set column names
		JSONArray columnNamesObj = new JSONArray();
		for (String columnName : columnNames)
			columnNamesObj.add(columnName);
		obj.put(PropertyHandler.getPropertyValue("ATTRIBUTE_COLUMNNAMES"), columnNamesObj);

		// set rows
		JSONArray rowsObj = new JSONArray();
		for (ArrayList<Object> row : rows) {
			JSONArray colsObj = new JSONArray();
			for (Object col : row)
				colsObj.add(col);
			rowsObj.add(colsObj);
		}
		obj.put(PropertyHandler.getPropertyValue("ATTRIBUTE_ROWS"), rowsObj);
		return obj;

	}

}
