package com.abzooba.trackR24.models;

import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;

import com.abzooba.trackR24.util.PropertyHandler;

/**
 * 
 * represents an abstract identifiable and serializable object
 * @author Krishna Kumar
 *
 */
public class AbstractIdentifiableSerializableObject implements IdentifiableSerializableObject {

	// Object ID
	protected long id = 0l;

	// Object classname
	protected String classname;

	// properties
	protected Map<String, Object> properties = new HashMap<>();

	/**
	 * 
	 * gets object ID
	 * @return object ID
	 */
	public long getID() {
		return id;
	}

	/**
	 * 
	 * sets object id
	 * @param ID object ID
	 */
	public void setID(long id) {
		this.id = id;
	}

	/**
	 * 
	 * gets object classname
	 * @return object classname
	 */
	public String getClassname() {
		return classname;
	}

	/**
	 * 
	 * sets object classname
	 * @param ID object classname
	 */
	public void setClassname(String classname) {
		this.classname = classname;
	}

	/**
	 * 
	 * gets properties 
	 * @return properties map of name, value pairs 
	 */
	public Map<String, Object> getProperties() {
		return properties;
	}

	/**
	 * 
	 * sets properties
	 * @param properties map of name, value pairs
	 */
	public void setProperties(Map<String, Object> properties) {
		this.properties = properties;
	}

	/**
	 * 
	 * adds a property
	 * @param name property name
	 * @param value property value
	 */
	public void addProperty(String name, Object value) {
		properties.put(name, value);
	}

	/**
	 * 
	 * gets value of a property
	 * @param name property name
	 * @return property value
	 */
	public Object getProperty(String name) {
		return properties.get(name);
	}

	@Override
	/**
	 * 
	 * loads object from JSON 
	 * @param obj JSON object
	 */
	public void fromJSON(JSONObject obj) throws ObjectInstantiationException {
		Long attributeId;
		JSONObject propertiesObj;
		if ((attributeId = ((Long) obj.get(PropertyHandler.getPropertyValue("ATTRIBUTE_ID")))) != null)
			setID(attributeId);

		if ((classname = ((String) obj.get(PropertyHandler.getPropertyValue("ATTRIBUTE_CLASSNAME")))) != null)
			setClassname(classname);

		if ((propertiesObj = (JSONObject) obj.get(PropertyHandler.getPropertyValue("ATTRIBUTE_PROPERTIES"))) != null) {

		
			for (Object key : propertiesObj.keySet())
				addProperty((String) key, propertiesObj.get(key));
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	/**
	 * 
	 * saves object to JSON
	 * @return JSON object
	 */
	public JSONObject toJSON() {
		JSONObject obj = new JSONObject();

		if (id != 0)
			obj.put(PropertyHandler.getPropertyValue("ATTRIBUTE_ID"), id);

		if (classname != null)
			obj.put(PropertyHandler.getPropertyValue("ATTRIBUTE_CLASSNAME"), classname);

		if (properties.size() > 0) {
			JSONObject propertiesObj = new JSONObject();
			for (String key : properties.keySet()) {
				propertiesObj.put(key, properties.get(key));
			}
			obj.put(PropertyHandler.getPropertyValue("ATTRIBUTE_PROPERTIES"), propertiesObj);
		}

		return obj;

	}

}
