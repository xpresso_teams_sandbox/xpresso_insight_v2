/**
 * This model object binds in UI with the Table in the Deep Listening Table both for Social & Non-Social Category.
 */
package com.abzooba.trackR24.models;

import java.util.List;

public class DeepListeningObject {
	
	private int accountId;
	private int sourceId;
	private String searchKey;
	private String sourceCategory;
	private String city;
	private String sourceTopic;
	private String startDate;
	private String endDate;
	private String virality;
	private int sentiment;
	private String tabName;
	private Integer perPageEntries;
	private Integer newPage;
	private int interval;

	
	private List<SocialPostListObject> socialPostListObj;
	private List<NonSocialPostListObject> nonSocialPostListObj;
	
	
	public List<SocialPostListObject> getSocialPostListObj() {
		return socialPostListObj;
	}
	public void setSocialPostListObj(List<SocialPostListObject> socialPostListObj) {
		this.socialPostListObj = socialPostListObj;
	}
	public List<NonSocialPostListObject> getNonSocialPostListObj() {
		return nonSocialPostListObj;
	}
	public void setNonSocialPostListObj(
			List<NonSocialPostListObject> nonSocialPostListObj) {
		this.nonSocialPostListObj = nonSocialPostListObj;
	}
	public int getAccountId() {
		return accountId;
	}
	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}
	public int getSourceId() {
		return sourceId;
	}
	public void setSourceId(int sourceId) {
		this.sourceId = sourceId;
	}
	public String getSearchKey() {
		return searchKey;
	}
	public void setSearchKey(String searchKey) {
		this.searchKey = searchKey;
	}
	public String getSourceCategory() {
		return sourceCategory;
	}
	public void setSourceCategory(String sourceCategory) {
		this.sourceCategory = sourceCategory;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getSourceTopic() {
		return sourceTopic;
	}
	public void setSourceTopic(String sourceTopic) {
		this.sourceTopic = sourceTopic;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getVirality() {
		return virality;
	}
	public void setVirality(String virality) {
		this.virality = virality;
	}
	public int getSentiment() {
		return sentiment;
	}
	public void setSentiment(int sentiment) {
		this.sentiment = sentiment;
	}
	public String getTabName() {
		return tabName;
	}
	public void setTabName(String tabName) {
		this.tabName = tabName;
	}
	public Integer getPerPageEntries() {
		return perPageEntries;
	}
	public void setPerPageEntries(Integer perPageEntries) {
		this.perPageEntries = perPageEntries;
	}
	public Integer getNewPage() {
		return newPage;
	}
	public void setNewPage(Integer newPage) {
		this.newPage = newPage;
	}
	public int getInterval() {
		return interval;
	}
	public void setInterval(int interval) {
		this.interval = interval;
	}

	
	
}
