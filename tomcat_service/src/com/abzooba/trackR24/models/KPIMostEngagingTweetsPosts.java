package com.abzooba.trackR24.models;

/**
* Author Name: Kunal Kashyap
* Create Date: 25-10-2016
* Last Modified: Kunal Kashyap: 26-10-2016
* Class Insight: This is the bean class for most engaging tweets and posts for KPI page.
*/
public class KPIMostEngagingTweetsPosts {
	
	private String date;
	private Integer hour;
	private Integer count;
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Integer getHour() {
		return hour;
	}
	public void setHour(Integer hour) {
		this.hour = hour;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	 
	 
	
	
	

}
