package com.abzooba.trackR24.models;

import org.json.simple.JSONObject;

import com.abzooba.trackR24.util.PropertyHandler;

/**
 * 
 * represents an abstract data service result
 * @author Krishna Kumar
 *
 */
public class AbstractResult extends AbstractIdentifiableSerializableObject implements Result {

	// name 
	protected String name;

	/**
	 * 
	 * gets name
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * sets name
	 * @param name name
	 */
	public void setName(String name) {
		this.name = name;
	}

	@Override
	/**
	 * 
	 * loads object from JSON 
	 * @param obj JSON object
	 */
	public void fromJSON(JSONObject obj) throws ObjectInstantiationException {
		super.fromJSON(obj);
		setName((String) obj.get(PropertyHandler.getPropertyValue("ATTRIBUTE_NAME")));
	}

	@SuppressWarnings("unchecked")
	@Override
	/**
	 * 
	 * saves object to JSON
	 * @return JSON object
	 */
	public JSONObject toJSON() {
		JSONObject obj = super.toJSON();
		obj.put(PropertyHandler.getPropertyValue("ATTRIBUTE_NAME"), name);
		return obj;

	}

}
