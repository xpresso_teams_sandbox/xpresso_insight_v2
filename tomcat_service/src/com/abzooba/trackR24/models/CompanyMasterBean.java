package com.abzooba.trackR24.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
* Author Name: Kunal Kashyap
* Create Date: 05-09-2016
* Last Modified: Kunal Kashyap: 05-09-2016
* Class Insight: This is the bean class for mapping with company_master table of database.
*/
@Entity
@Table(name = "company_master")
public class CompanyMasterBean {

	@Id
	private int company_id;
	private String company_name;
	private int domain_id;
	private Date create_datetime;
	private Date update_datetime;

	public int getCompany_id() {
		return company_id;
	}

	public void setCompany_id(int company_id) {
		this.company_id = company_id;
	}

	public String getCompany_name() {
		return company_name;
	}

	public void setCompany_name(String company_name) {
		this.company_name = company_name;
	}

	public int getDomain_id() {
		return domain_id;
	}

	public void setDomain_id(int domain_id) {
		this.domain_id = domain_id;
	}

	public Date getCreate_datetime() {
		return create_datetime;
	}

	public void setCreate_datetime(Date create_datetime) {
		this.create_datetime = create_datetime;
	}

	public Date getUpdate_datetime() {
		return update_datetime;
	}

	public void setUpdate_datetime(Date update_datetime) {
		this.update_datetime = update_datetime;
	}

}
