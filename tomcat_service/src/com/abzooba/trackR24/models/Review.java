package com.abzooba.trackR24.models;

public class Review {

	private int accountId;
	private int sourceId;
	private int cityId;
	private String locality;
	private int sentiment;
	private String startDt;
	private String endDt;
	private Integer perPageEntries;
	private Integer newPage;
	private int ownershipId;
	
	public int getAccountId() {
		return accountId;
	}
	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}
	public int getSourceId() {
		return sourceId;
	}
	public void setSourceId(int sourceId) {
		this.sourceId = sourceId;
	}
	public int getCityId() {
		return cityId;
	}
	public void setCityId(int cityId) {
		this.cityId = cityId;
	}
	public String getLocality() {
		return locality;
	}
	public void setLocality(String locality) {
		this.locality = locality;
	}
	public int getSentiment() {
		return sentiment;
	}
	public void setSentiment(int sentiment) {
		this.sentiment = sentiment;
	}
	public String getStartDt() {
		return startDt;
	}
	public void setStartDt(String startDt) {
		this.startDt = startDt;
	}
	public String getEndDt() {
		return endDt;
	}
	public void setEndDt(String endDt) {
		this.endDt = endDt;
	}
	public Integer getPerPageEntries() {
		return perPageEntries;
	}
	public void setPerPageEntries(Integer perPageEntries) {
		this.perPageEntries = perPageEntries;
	}
	public Integer getNewPage() {
		return newPage;
	}
	public void setNewPage(Integer newPage) {
		this.newPage = newPage;
	}
	public int getOwnershipId() {
		return ownershipId;
	}
	public void setOwnershipId(int ownershipId) {
		this.ownershipId = ownershipId;
	}


}
