package com.abzooba.trackR24.models;

/**
* Author Name: Kunal Kashyap
* Create Date: 04-11-2016
* Last Modified: Kunal Kashyap: 04-11-2016
* Class Insight: This is the bean class for per tweet and post interaction for KPI page.
*/
public class KPIPerTweetPostInteractionBean {

	private String count_date;
	private double average_count;
	public String getCount_date() {
		return count_date;
	}
	public void setCount_date(String count_date) {
		this.count_date = count_date;
	}
	public double getAverage_count() {
		return average_count;
	}
	public void setAverage_count(double average_count) {
		this.average_count = average_count;
	}
	 
	
	 
	 
	 
}
