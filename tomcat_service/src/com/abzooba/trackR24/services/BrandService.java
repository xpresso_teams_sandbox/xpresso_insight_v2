package com.abzooba.trackR24.services;


import static com.abzooba.trackR24.util.Constants.ATTRIBUTE_SP_NAME;
import static com.abzooba.trackR24.util.Constants.JDBC_BRAND_DAO;
import static com.abzooba.trackR24.util.Constants.RESULTS;
import static com.abzooba.trackR24.util.Constants.SOURCE_ID;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_ACCOUNT_ID;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_ASPECT;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_DUE_DATE;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_DURATION;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_FROM_DT;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_LIMIT;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_OFFSET;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_POST_ID;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_SENTIMENT_TYPE;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_SORT;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_SOURCE;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_SOURCE_ID;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_TO_DT;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_USER_NAME;
import static com.abzooba.trackR24.util.Constants.SP_IN_POST_TYPE_ID;
import static com.abzooba.trackR24.util.Constants.SP_IN_SEARCH_STRING;
import static com.abzooba.trackR24.util.Constants.SP_NAME;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

import com.abzooba.trackR24.dao.DataSourceException;
import com.abzooba.trackR24.dao.FactoryDao;
import com.abzooba.trackR24.dao.JdbcBrandDao;
import com.abzooba.trackR24.models.Result;
import com.abzooba.trackR24.util.PropertyHandler;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Path("/brand")
@SuppressWarnings("unchecked")
public class BrandService {

	private static final Logger LOGGER = Logger.getLogger(BrandService.class);

	private Map<String, Object> staticParameters;
	private Map<String, Object> dynamicParameters;

	
	/**
	 * 
	 * @Author Name: Ashis Nayak
	 * @Create Date: 12-18-2017
	 * @Last Modified: 
	 * @Input:
	 * @Output: 
	 * @Method:
	 * @throws DataSourceException, IOException, InvocationTargetException, IllegalArgumentException, IllegalAccessException, SecurityException, NoSuchMethodException, JsonMappingException, JsonGenerationException 
	 */
	
	@POST
	@Path("/getSummaryMatrics")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSummaryMatrics(@FormParam("inSourceId") int inSourceId, @FormParam("inDuration") int inDuration,@FormParam("inAccountId") int inAccountId) throws DataSourceException {

		LOGGER.info(">>>>>>>BrandService ::: Entering getSummaryMatrics>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		String spName = null;
		staticParameters = new HashMap<>();
		spName = PropertyHandler.getPropertyValue("SP_SUMMARY_MATRICS");
		LOGGER.debug(SP_NAME + spName);
		staticParameters.put(ATTRIBUTE_SP_NAME, spName);
		dynamicParameters = new HashMap<>();
		
		dynamicParameters.put(SP_IN_PARAM_SOURCE_ID, inSourceId);
		dynamicParameters.put(SP_IN_PARAM_DURATION, inDuration);
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, inAccountId);
		
		JdbcBrandDao jdbcBrand = (JdbcBrandDao) FactoryDao.getInstance(JDBC_BRAND_DAO);
		Result result = jdbcBrand.fetchSummaryMatrics(staticParameters, dynamicParameters);
		LOGGER.info("Result set getSummaryMatrics JSON : " + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>BrandService ::: Exiting getSummaryMatrics>>>>>>>>");
		return responseBuilder.build();

	}
	
	
	/**
	 * 
	 * @Author Name: Ashis Nayak
	 * @Create Date: 12-19-2017
	 * @Last Modified: 
	 * @Input:
	 * @Output: 
	 * @Method:
	 * @throws DataSourceException, IOException, InvocationTargetException, IllegalArgumentException, IllegalAccessException, SecurityException, NoSuchMethodException, JsonMappingException, JsonGenerationException 
	 */
	
	@POST
	@Path("/getCompetitiveComparison")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCompetitiveComparison(@FormParam("inDuration") int inDuration,@FormParam("inSourceId") int inSourceId, @FormParam("inAccountId") int inAccountId) throws DataSourceException {
		LOGGER.info(">>>>>>>BrandService ::: Entering getCompetitiveComparison>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		String spName = null;
		staticParameters = new HashMap<>();
		spName = PropertyHandler.getPropertyValue("SP_COMPETITIVE_COMPARISION");
		LOGGER.debug(SP_NAME + spName);
		staticParameters.put(ATTRIBUTE_SP_NAME, spName);
		dynamicParameters = new HashMap<>();
		
		dynamicParameters.put(SP_IN_PARAM_DURATION, inDuration);
		dynamicParameters.put(SP_IN_PARAM_SOURCE_ID, inSourceId);
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, inAccountId);
		
		JdbcBrandDao jdbcBrand = (JdbcBrandDao) FactoryDao.getInstance(JDBC_BRAND_DAO);
		Result result = jdbcBrand.fetchCompetitiveComparison(staticParameters, dynamicParameters);
		LOGGER.info("Result set getCompetitiveComparison JSON : " + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>BrandService ::: Exiting getCompetitiveComparison>>>>>>>>");
		return responseBuilder.build();
	}
	
	/**
	 * 
	 * @Author Name: Ashis Nayak
	 * @Create Date: 12-20-2017
	 * @Last Modified: 
	 * @Input:
	 * @Output: 
	 * @Method:
	 * @throws DataSourceException, IOException, InvocationTargetException, IllegalArgumentException, IllegalAccessException, SecurityException, NoSuchMethodException, JsonMappingException, JsonGenerationException 
	 */
	
	@POST
	@Path("/getSummaryOverallDataCount")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSummaryOverallDataCount(@FormParam("inDuration") int inDuration, @FormParam("inAccountId") int inAccountId, 
			@FormParam("posttype") String inType, @FormParam("sourceType") String sourceType,@FormParam("sortby") String sortby  ,
			@FormParam("start_dt") String startDt, @FormParam("end_dt") String endDt) throws DataSourceException{
		LOGGER.info(">>>>>>>BrandsService ::: Entering getSummaryOverallDataCount>>>>>>>>" + inType);
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		String spName = null;
		staticParameters = new HashMap<>();
		
		spName = PropertyHandler.getPropertyValue("SP_SUMMARY_POST_TYPE_COUNT");
		
		LOGGER.debug(SP_NAME + spName);
		staticParameters.put(ATTRIBUTE_SP_NAME, spName);
		dynamicParameters = new HashMap<>();
		
		dynamicParameters.put(SP_IN_PARAM_SOURCE_ID, sourceType);
		dynamicParameters.put(SP_IN_PARAM_SORT, sortby);
		dynamicParameters.put(SP_IN_POST_TYPE_ID, inType);
		dynamicParameters.put(SP_IN_PARAM_FROM_DT, "".trim().equalsIgnoreCase(startDt) ? null : startDt);
		dynamicParameters.put(SP_IN_PARAM_TO_DT, "".trim().equalsIgnoreCase(endDt) ? null : endDt);
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, inAccountId);
		
		JdbcBrandDao jdbcBrand = (JdbcBrandDao) FactoryDao.getInstance(JDBC_BRAND_DAO);
		Result result = jdbcBrand.fetchSummaryPostTypeCount(staticParameters, dynamicParameters,"getSummaryOverallDataCount");
		LOGGER.info("Result set getSummaryOverallDataCount JSON : " + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>BrandsService ::: Exiting getSummaryOverallDataCount>>>>>>>>");
		return responseBuilder.build();
	}
	
	
	@POST
	@Path("/getSummarysentimentChart")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSummarysentimentChart(@FormParam("inDuration") int inDuration, @FormParam("inAccountId") int inAccountId, @FormParam("postType") String inType, @FormParam("sourceType") String sourceType,@FormParam("start_dt") String startDt, @FormParam("end_dt") String endDt) throws DataSourceException{
		LOGGER.info(">>>>>>>BrandsService ::: Entering getSummarysentimentChart>>>>>>>>" + inType);
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		String spName = null;
		staticParameters = new HashMap<>();
		
		spName = PropertyHandler.getPropertyValue("SP_SUMMARY_SENTIMENT_CHART");
		
		LOGGER.debug(SP_NAME + spName);
		staticParameters.put(ATTRIBUTE_SP_NAME, spName);
		dynamicParameters = new HashMap<>();
		
		dynamicParameters.put(SP_IN_PARAM_SOURCE, sourceType);
		dynamicParameters.put("inPostType", inType);
		dynamicParameters.put(SP_IN_PARAM_FROM_DT, "".trim().equalsIgnoreCase(startDt) ? null : startDt);
		dynamicParameters.put(SP_IN_PARAM_TO_DT, "".trim().equalsIgnoreCase(endDt) ? null : endDt);
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, inAccountId);
		
		JdbcBrandDao jdbcBrand = (JdbcBrandDao) FactoryDao.getInstance(JDBC_BRAND_DAO);
		Result result = jdbcBrand.fetchSummarySentimentChart(staticParameters, dynamicParameters,"getSummarysentimentChart");
		LOGGER.info("Result set getSummarysentimentChart JSON : " + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>BrandsService ::: Exiting getSummarysentimentChart>>>>>>>>");
		return responseBuilder.build();
	}
	
	
	@POST
	@Path("/getSummarysentimentHourlyChart")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSummarysentimentHourlyChart( @FormParam("inAccountId") int inAccountId, @FormParam("sourceType") String sourceType) throws DataSourceException{
		LOGGER.info(">>>>>>>BrandsService ::: Entering getSummarysentimentHourlyChart>>>>>>>>" + inAccountId);
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		String spName = null;
		staticParameters = new HashMap<>();
		
		spName = PropertyHandler.getPropertyValue("SP_SUMMARY_SENTIMENT_CHART_HOURLY");
		
		LOGGER.debug(SP_NAME + spName);
		staticParameters.put(ATTRIBUTE_SP_NAME, spName);
		dynamicParameters = new HashMap<>();
		
		dynamicParameters.put(SP_IN_PARAM_SOURCE, sourceType);
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, inAccountId);
		
		JdbcBrandDao jdbcBrand = (JdbcBrandDao) FactoryDao.getInstance(JDBC_BRAND_DAO);
		Result result = jdbcBrand.fetchSummarySentimentChartHourly(staticParameters, dynamicParameters,"getSummarysentimentHourlyChart");
		LOGGER.info("Result set getSummarysentimentHourlyChart JSON : " + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>BrandsService ::: Exiting getSummarysentimentHourlyChart>>>>>>>>");
		return responseBuilder.build();
	}
	
	@POST
	@Path("/getSummaryTopNegativeAspectChart")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSummaryTopNegativeAspectChart(@FormParam("inAccountId") int inAccountId, 
			@FormParam("sourceType") String sourceType,
			@FormParam("start_dt") String startDt, @FormParam("end_dt") String endDt) throws DataSourceException{
		LOGGER.info(">>>>>>>BrandsService ::: Entering getSummaryTopNegativeAspectChart>>>>>>>>" + sourceType);
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		//String spName = null;
		staticParameters = new HashMap<>();
		
		//spName = PropertyHandler.getPropertyValue("SP_SUMMARY_SENTIMENT_CHART");
		
		LOGGER.debug(SP_NAME + "sp_platform_and_date_wise_most_negative_aspect");
		staticParameters.put(ATTRIBUTE_SP_NAME, "sp_platform_and_date_wise_most_negative_aspect");
		dynamicParameters = new HashMap<>();
		
		dynamicParameters.put(SP_IN_PARAM_SOURCE, sourceType);
		dynamicParameters.put(SP_IN_PARAM_FROM_DT, "".trim().equalsIgnoreCase(startDt) ? null : startDt);
		dynamicParameters.put(SP_IN_PARAM_TO_DT, "".trim().equalsIgnoreCase(endDt) ? null : endDt);
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, inAccountId);
		
		JdbcBrandDao jdbcBrand = (JdbcBrandDao) FactoryDao.getInstance(JDBC_BRAND_DAO);
		Result result = jdbcBrand.fetchSummaryTopNegativeSentimentChart(staticParameters, dynamicParameters,"getSummaryTopNegativeAspectChart");
		LOGGER.info("Result set getSummaryTopNegativeAspectChart JSON : " + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>BrandsService ::: Exiting getSummaryTopNegativeAspectChart>>>>>>>>");
		return responseBuilder.build();
	}
	
	@POST
	@Path("/getTopNegativeAspectDeatilsModal")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTopNegativeAspectDeatilsModal( 
			@FormParam("selected_dt") String selected_dt,
			@FormParam("sourceType") int selected_source,
			@FormParam("selected_aspect") String selected_aspect) throws DataSourceException{
		LOGGER.info(">>>>>>>BrandsService ::: Entering getTopNegativeAspectDeatilsModal>>>>>>>>" + selected_aspect);
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		String spName = null;
		staticParameters = new HashMap<>();
		
		spName = PropertyHandler.getPropertyValue("SP_TOP_NEGATIVE_ASPECT_DETAILS");
		
		LOGGER.debug(SP_NAME + spName);
		
		staticParameters.put(ATTRIBUTE_SP_NAME, spName);
		dynamicParameters = new HashMap<>();
		
		dynamicParameters.put(SP_IN_PARAM_DUE_DATE, selected_dt+" 00:01");
		dynamicParameters.put(SP_IN_PARAM_ASPECT, selected_aspect);
		dynamicParameters.put(SOURCE_ID, selected_source);
		
		
		JdbcBrandDao jdbcBrand = (JdbcBrandDao) FactoryDao.getInstance(JDBC_BRAND_DAO);
		Result result = jdbcBrand.fetchTopNegativeAspectDeatilsModal(staticParameters, dynamicParameters,"getTopNegativeAspectDeatilsModal");
		LOGGER.info("Result set getTopNegativeAspectDeatilsModal JSON : " + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>BrandsService ::: Exiting getTopNegativeAspectDeatilsModal>>>>>>>>");
		return responseBuilder.build();
	}
	
	
	@POST
	@Path("/getSummarysentimentDetails")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSummarysentimentDetails(@FormParam("inDuration") int inDuration, @FormParam("inAccountId") int inAccountId, @FormParam("postType") String inType, @FormParam("selectedSentiment") String selectedSentiment, @FormParam("sourceType") String sourceType,@FormParam("start_dt") String startDt, @FormParam("end_dt") String endDt) throws DataSourceException{
		LOGGER.info(">>>>>>>BrandsService ::: Entering getSummarysentimentChart>>>>>>>>" + inType);
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		String spName = null;
		staticParameters = new HashMap<>();
		
		spName = PropertyHandler.getPropertyValue("SP_SUMMARY_SENTIMENT_MODAL_DETAILS");
		
		LOGGER.debug(SP_NAME + spName);
		staticParameters.put(ATTRIBUTE_SP_NAME, spName);
		dynamicParameters = new HashMap<>();
		
		dynamicParameters.put(SP_IN_PARAM_SOURCE, sourceType);
		dynamicParameters.put("inPostType", inType);
		dynamicParameters.put(SP_IN_PARAM_SENTIMENT_TYPE, selectedSentiment);
		dynamicParameters.put(SP_IN_PARAM_FROM_DT, "".trim().equalsIgnoreCase(startDt) ? null : startDt);
		dynamicParameters.put(SP_IN_PARAM_TO_DT, "".trim().equalsIgnoreCase(endDt) ? null : endDt);
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, inAccountId);
		
		JdbcBrandDao jdbcBrand = (JdbcBrandDao) FactoryDao.getInstance(JDBC_BRAND_DAO);
		Result result = jdbcBrand.fetchSummarySentimentDetails(staticParameters, dynamicParameters,"getSummarysentimentDModalDetails");
		LOGGER.info("Result set getSummarysentimentChart JSON : " + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>BrandsService ::: Exiting getSummarysentimentChart>>>>>>>>");
		return responseBuilder.build();
	}
	
	@POST
	@Path("/getOwnSocialPostTweet")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getOwnSocialPostTweet(@FormParam("inDuration") int inDuration, @FormParam("inAccountId") int inAccountId, @FormParam("posttype") String inType) throws DataSourceException{
		LOGGER.info(">>>>>>>BrandService ::: Entering getOwnSocialPostTweet>>>>>>>>" + inType);
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		String spName = null;
		staticParameters = new HashMap<>();
		
		if(inType.equalsIgnoreCase("post")){
			spName = PropertyHandler.getPropertyValue("SP_OWN_SOCIAL_POST");
		} else if(inType.equalsIgnoreCase("tweet")) {
			spName = PropertyHandler.getPropertyValue("SP_OWN_SOCIAL_TWEET");
		}
		
		LOGGER.debug(SP_NAME + spName);
		staticParameters.put(ATTRIBUTE_SP_NAME, spName);
		dynamicParameters = new HashMap<>();
		
		dynamicParameters.put(SP_IN_PARAM_DURATION, inDuration);
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, inAccountId);
		
		JdbcBrandDao jdbcBrand = (JdbcBrandDao) FactoryDao.getInstance(JDBC_BRAND_DAO);
		Result result = jdbcBrand.fetchSocialPostTweet(staticParameters, dynamicParameters,"getOwnSocialPostTweet");
		LOGGER.info("Result set getOwnSocialPostTweet JSON : " + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>BrandService ::: Exiting getOwnSocialPostTweet>>>>>>>>");
		return responseBuilder.build();
	}
	
	/**
	 * 
	 * @Author Name: Ashis Nayak
	 * @Create Date: 12-20-2017
	 * @Last Modified: 
	 * @Input:
	 * @Output: 
	 * @Method:
	 * @throws DataSourceException, IOException, InvocationTargetException, IllegalArgumentException, IllegalAccessException, SecurityException, NoSuchMethodException, JsonMappingException, JsonGenerationException 
	 */
	
	@POST
	@Path("/getSummaryOwnSocialPostTweet")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSummaryOwnSocialPostTweet(@FormParam("inAccountId") int inAccountId,@FormParam("sourceType") String sourceType, @FormParam("postType") String postType,@FormParam("start_dt") String startDt, @FormParam("end_dt") String endDt, @FormParam("strSearchString") String strSearchString,@FormParam("sortby") String sortby) throws DataSourceException{
		LOGGER.info(">>>>>>>BrandService ::: Entering getOwnSocialPostTweet>>>>>>>>" + strSearchString);
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		String spName = null;
		staticParameters = new HashMap<>();
		
		if(sourceType.equalsIgnoreCase("1")){
			
			//Facebook
			if(postType.equalsIgnoreCase("0")) // All post
				spName = PropertyHandler.getPropertyValue("SP_SUMMARY_ALL_SOCIAL_POST");
			
			if(postType.equalsIgnoreCase("1")) // Own Posts
				spName = PropertyHandler.getPropertyValue("SP_SUMMARY_OWN_SOCIAL_POST");
			
			if(postType.equalsIgnoreCase("11")) // Guest posts
				spName = PropertyHandler.getPropertyValue("SP_SUMMARY_GUEST_SOCIAL_POST");
			
			if(postType.equalsIgnoreCase("9")) // Tagged
				spName = PropertyHandler.getPropertyValue("SP_SUMMARY_TAGGED_SOCIAL_POST");
			
			
			//sp_summary_analysis_all_post
			
		} else if(sourceType.equalsIgnoreCase("2")) {
			//Twitter
			if(postType.equalsIgnoreCase("3"))
			spName = PropertyHandler.getPropertyValue("SP_SUMMARY_TAGGED_SOCIAL_TWEET");
		}
		
		LOGGER.debug(SP_NAME + spName);
		staticParameters.put(ATTRIBUTE_SP_NAME, spName);
		dynamicParameters = new HashMap<>();
		
		dynamicParameters.put(SP_IN_PARAM_FROM_DT, "".trim().equalsIgnoreCase(startDt) ? null : startDt);
		dynamicParameters.put(SP_IN_PARAM_TO_DT, "".trim().equalsIgnoreCase(endDt) ? null : endDt);
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, inAccountId);
		dynamicParameters.put(SP_IN_SEARCH_STRING, strSearchString);
		dynamicParameters.put(SP_IN_PARAM_SORT, sortby);
		
		JdbcBrandDao jdbcBrand = (JdbcBrandDao) FactoryDao.getInstance(JDBC_BRAND_DAO);
		Result result = jdbcBrand.fetchSummaryPostTweet(staticParameters, dynamicParameters,"getOwnSocialPostTweet");
		LOGGER.info("Result set getOwnSocialPostTweet JSON : " + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>BrandService ::: Exiting getOwnSocialPostTweet>>>>>>>>");
		return responseBuilder.build();
	}
	
	/**
	 * 
	 * @Author Name: Ashis Nayak
	 * @Create Date: 29-01-2018
	 * @Last Modified: 
	 * @Input:
	 * @Output: 
	 * @Method:
	 * @throws DataSourceException, IOException, InvocationTargetException, IllegalArgumentException, IllegalAccessException, SecurityException, NoSuchMethodException, JsonMappingException, JsonGenerationException 
	 */
	
	@POST
	@Path("/getDMUserList")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDMUserList(@FormParam("inAccountId") int inAccountId,@FormParam("sourceType") String sourceType,@FormParam("start_dt") String startDt, @FormParam("end_dt") String endDt) throws DataSourceException{
		LOGGER.info(">>>>>>>BrandService ::: Entering getDMUserList>>>>>>>>" + sourceType);
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		String spName = null;
		staticParameters = new HashMap<>();
		
		/*if(sourceType.equalsIgnoreCase("1")){
			//Facebook
		} else if(sourceType.equalsIgnoreCase("2")) {
			//Twitter
			spName = PropertyHandler.getPropertyValue("SP_SUMMARY_TAGGED_SOCIAL_TWEET");
		}*/
		spName = PropertyHandler.getPropertyValue("SP_DM_USER_LIST");
		
		LOGGER.debug(SP_NAME + spName);
		staticParameters.put(ATTRIBUTE_SP_NAME, spName);
		dynamicParameters = new HashMap<>();
		
		dynamicParameters.put(SP_IN_PARAM_FROM_DT, "".trim().equalsIgnoreCase(startDt) ? null : startDt);
		dynamicParameters.put(SP_IN_PARAM_TO_DT, "".trim().equalsIgnoreCase(endDt) ? null : endDt);
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, inAccountId);
		dynamicParameters.put(SP_IN_PARAM_SOURCE, sourceType);
		
		JdbcBrandDao jdbcBrand = (JdbcBrandDao) FactoryDao.getInstance(JDBC_BRAND_DAO);
		Result result = jdbcBrand.fetchDirectMessageUserList(staticParameters, dynamicParameters,"getDMUserList");
		LOGGER.info("Result set getDMUserList JSON : " + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>BrandService ::: Exiting getDMUserList>>>>>>>>");
		return responseBuilder.build();
	}
	
	/**
	 * 
	 * @Author Name: Ashis Nayak
	 * @Create Date: 27-11-2018
	 * @Last Modified: 
	 * @Input: 
	 * @Output: 
	 * @Method:
	 * @throws DataSourceException, IOException, InvocationTargetException, IllegalArgumentException, IllegalAccessException, SecurityException, NoSuchMethodException, JsonMappingException, JsonGenerationException 
	 */
	
	@POST
	@Path("/getSummaryComments")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSummaryComments(@FormParam("inAccountId") int inAccountId,@FormParam("sourceType") String sourceType, @FormParam("postId") String postId,@FormParam("offset") int offset, @FormParam("limit") int limit) throws DataSourceException{
		LOGGER.info(">>>>>>>BrandService ::: Entering getOwnSocialPostTweet>>>>>>>> offset : " + offset);
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		String spName = null;
		staticParameters = new HashMap<>();
		
		spName = PropertyHandler.getPropertyValue("SP_SUMMARY_OWN_SOCIAL_POST_TWEET_COMMENTS");
		
		LOGGER.debug(SP_NAME + spName);
		staticParameters.put(ATTRIBUTE_SP_NAME, spName);
		dynamicParameters = new HashMap<>();
		
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, inAccountId);
		dynamicParameters.put(SP_IN_PARAM_POST_ID, postId);
		dynamicParameters.put(SP_IN_PARAM_SOURCE, sourceType);
		dynamicParameters.put(SP_IN_PARAM_OFFSET, offset);
		dynamicParameters.put(SP_IN_PARAM_LIMIT, limit);
		
		JdbcBrandDao jdbcBrand = (JdbcBrandDao) FactoryDao.getInstance(JDBC_BRAND_DAO);
		Result result = jdbcBrand.fetchSummaryPostTweetDetailsLimit(staticParameters, dynamicParameters,"getOwnSocialPostTweetdetails");
		LOGGER.info("Result set getOwnSocialPostTweet JSON : " + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>BrandService ::: Exiting getOwnSocialPostTweet>>>>>>>>");
		return responseBuilder.build();
	}
	
	@POST
	@Path("/getSummaryreplies")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSummaryreplies(@FormParam("inAccountId") int inAccountId,@FormParam("sourceType") String sourceType, @FormParam("postId") String postId) throws DataSourceException{
		LOGGER.info(">>>>>>>BrandService ::: Entering getSummaryreplies>>>>>>>> ");
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		String spName = null;
		staticParameters = new HashMap<>();
		
		spName = PropertyHandler.getPropertyValue("SP_SUMMARY_OWN_SOCIAL_POST_TWEET_REPLIES");
		
		LOGGER.debug(SP_NAME + spName);
		staticParameters.put(ATTRIBUTE_SP_NAME, spName);
		dynamicParameters = new HashMap<>();
		
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, inAccountId);
		dynamicParameters.put(SP_IN_PARAM_POST_ID, postId);
		dynamicParameters.put(SP_IN_PARAM_SOURCE, sourceType);
		
		JdbcBrandDao jdbcBrand = (JdbcBrandDao) FactoryDao.getInstance(JDBC_BRAND_DAO);
		Result result = jdbcBrand.fetchSummaryPostTweetDetails(staticParameters, dynamicParameters,"getSummaryreplies");
		LOGGER.info("Result set getOwnSocialPostTweet JSON : " + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>BrandService ::: Exiting getOwnSocialPostTweet>>>>>>>>");
		return responseBuilder.build();
	}

	@POST
	@Path("/getSummaryDetailsPostTweet")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSummaryDetailsPostTweet(@FormParam("inAccountId") int inAccountId,@FormParam("sourceType") String sourceType, @FormParam("postId") String postId) throws DataSourceException{
		LOGGER.info(">>>>>>>BrandService ::: Entering getOwnSocialPostTweet>>>>>>>>" + postId);
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		String spName = null;
		staticParameters = new HashMap<>();
		
		spName = PropertyHandler.getPropertyValue("SP_SUMMARY_OWN_SOCIAL_POST_TWEET_DETAILS");
		
		LOGGER.debug(SP_NAME + spName);
		staticParameters.put(ATTRIBUTE_SP_NAME, spName);
		dynamicParameters = new HashMap<>();
		
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, inAccountId);
		dynamicParameters.put(SP_IN_PARAM_POST_ID, postId);
		dynamicParameters.put(SP_IN_PARAM_SOURCE, sourceType);
		
		JdbcBrandDao jdbcBrand = (JdbcBrandDao) FactoryDao.getInstance(JDBC_BRAND_DAO);
		Result result = jdbcBrand.fetchSummaryPostTweetDetails(staticParameters, dynamicParameters,"getOwnSocialPostTweetdetails");
		LOGGER.info("Result set getOwnSocialPostTweet JSON : " + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>BrandService ::: Exiting getOwnSocialPostTweet>>>>>>>>");
		return responseBuilder.build();
	}

	@POST
	@Path("/getDMDetailedChain")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDMDetailedChain(@FormParam("inAccountId") int inAccountId,@FormParam("sourceType") String sourceType, @FormParam("userName") String userName,@FormParam("start_dt") String startDt, @FormParam("end_dt") String endDt) throws DataSourceException{
		LOGGER.info(">>>>>>>BrandService ::: Entering getDMDetailedChain>>>>>>>>" + userName);
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		String spName = null;
		staticParameters = new HashMap<>();
		
		spName = PropertyHandler.getPropertyValue("SP_DM_DETAILED_CHAIN");
		
		LOGGER.debug(SP_NAME + spName);
		staticParameters.put(ATTRIBUTE_SP_NAME, spName);
		dynamicParameters = new HashMap<>();
		
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, inAccountId);
		dynamicParameters.put(SP_IN_PARAM_USER_NAME, userName);
		dynamicParameters.put(SP_IN_PARAM_SOURCE, sourceType);
		dynamicParameters.put(SP_IN_PARAM_FROM_DT, "".trim().equalsIgnoreCase(startDt) ? null : startDt);
		dynamicParameters.put(SP_IN_PARAM_TO_DT, "".trim().equalsIgnoreCase(endDt) ? null : endDt);
		
		JdbcBrandDao jdbcBrand = (JdbcBrandDao) FactoryDao.getInstance(JDBC_BRAND_DAO);
		Result result = jdbcBrand.fetchDMDetailedChain(staticParameters, dynamicParameters,"getDMDetailedChain");
		LOGGER.info("Result set getDMDetailedChain JSON : " + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>BrandService ::: Exiting getDMDetailedChain>>>>>>>>");
		return responseBuilder.build();
	}
	
	/**
	 * 
	 * @Author Name: Ashis Nayak
	 * @Create Date: 12-20-2017
	 * @Last Modified: 
	 * @Input:
	 * @Output: 
	 * @Method:
	 * @throws DataSourceException, IOException, InvocationTargetException, IllegalArgumentException, IllegalAccessException, SecurityException, NoSuchMethodException, JsonMappingException, JsonGenerationException 
	 */
	
	@POST
	@Path("/getUserSocialPostTweet")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUserSocialPostTweet(@FormParam("inDuration") int inDuration, @FormParam("inAccountId") int inAccountId, @FormParam("posttype") String inType) throws DataSourceException {
		LOGGER.info(">>>>>>>BrandService ::: Entering getUserSocialPostTweet>>>>>>>>" + inType);
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		String spName = null;
		staticParameters = new HashMap<>();
		
		if(inType.equalsIgnoreCase("post")){
			spName = PropertyHandler.getPropertyValue("SP_USER_SOCIAL_POST");
		} else if(inType.equalsIgnoreCase("tweet")) {
			spName = PropertyHandler.getPropertyValue("SP_USER_SOCIAL_TWEET");
		}
		
		LOGGER.debug(SP_NAME + spName);
		staticParameters.put(ATTRIBUTE_SP_NAME, spName);
		dynamicParameters = new HashMap<>();
		
		dynamicParameters.put(SP_IN_PARAM_DURATION, inDuration);
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, inAccountId);
		
		JdbcBrandDao jdbcBrand = (JdbcBrandDao) FactoryDao.getInstance(JDBC_BRAND_DAO);
		/*
		 * Using the same method from JDBCBrandDAO which is already used for OwnSocialPost as both are similar
		 */
		Result result = jdbcBrand.fetchSocialPostTweet(staticParameters, dynamicParameters,"getUserSocialPostTweet");
		LOGGER.info("Result set getUserSocialPostTweet JSON : " + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>BrandService ::: Exiting getUserSocialPostTweet>>>>>>>>");
		return responseBuilder.build();
	}
	
	/**
	 * 
	 * @Author Name: Ashis Nayak
	 * @Create Date: 12-20-2017
	 * @Last Modified: 
	 * @Input:
	 * @Output: 
	 * @Method:
	 * @throws DataSourceException, IOException, InvocationTargetException, IllegalArgumentException, IllegalAccessException, SecurityException, NoSuchMethodException, JsonMappingException, JsonGenerationException 
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonGenerationException 
	 */
	
	@POST
	@Path("/getBrandsWordcloud")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getWordCloudData(@FormParam("inDuration") int inDuration, @FormParam("inAccountId") int inAccountId, @FormParam("inSourceId") int inSourceId, @FormParam("inChartName") String inChartName) throws DataSourceException, IOException{
		LOGGER.info(">>>>>>>AnalyticsService ::: Entering getBrandsWordcloud>>>>>>>>");
		ResponseBuilder responseBuilder;
		List<Result> resultListObj = new ArrayList<>();
		ObjectMapper mapper = new ObjectMapper();
		Result aspectResultObj1 = fetchWordCloudData(inDuration, inAccountId, inSourceId,inChartName);
		resultListObj.add(aspectResultObj1);
		String wordCloudJson = mapper.writeValueAsString(resultListObj);
		responseBuilder = Response.ok(wordCloudJson);
		LOGGER.info(">>>>>>>AnalyticsService ::: Exiting getBrandsWordcloud>>>>>>>>");
		return responseBuilder.build();
	}
	
	/**
	 * 
	 * @Author Name: Ashis Nayak
	 * @Create Date: 12-22-2017
	 * @Last Modified: 
	 * @Input:
	 * @Output: 
	 * @Method:
	 * @throws DataSourceException, IOException, InvocationTargetException, IllegalArgumentException, IllegalAccessException, SecurityException, NoSuchMethodException, JsonMappingException, JsonGenerationException 
	 */
	@POST
	@Path("/getBrandTimeline")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getBrandTimeline(@FormParam("inSourceId") int inSourceId, @FormParam("inAccountId") int inAccountId) throws DataSourceException{
		LOGGER.info(">>>>>>>BrandService ::: Entering getBrandTimeline>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		String spName = null;
		staticParameters = new HashMap<>();
		spName = PropertyHandler.getPropertyValue("SP_BRAND_TIME_LINE");
		LOGGER.debug(SP_NAME + spName);
		staticParameters.put(ATTRIBUTE_SP_NAME, spName);
		dynamicParameters = new HashMap<>();
		
		dynamicParameters.put(SP_IN_PARAM_SOURCE_ID, inSourceId);
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, inAccountId);
		
		JdbcBrandDao jdbcBrand = (JdbcBrandDao) FactoryDao.getInstance(JDBC_BRAND_DAO);
		Result result = jdbcBrand.fetchBrandTimeline(staticParameters, dynamicParameters);
		LOGGER.info("Result set getBrandTimeline JSON : " + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>BrandService ::: Exiting getBrandTimeline>>>>>>>>");
		return responseBuilder.build();
	}
	

	private Result fetchWordCloudData(int inDuration, int inAccountId,
			int inSourceId, String inChartName) throws DataSourceException {
		LOGGER.info(">>>>>>>AnalyticsService ::: Entering getBrandsWordcloud>>>>>>>>");
		staticParameters = new HashMap<>();
		if (inChartName.equalsIgnoreCase("WordcloudSentimentAttribute")) {
			staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_WORD_CLOUD_SENTIMENT_ATTRIBUTE"));
		} else {
			staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_WORD_CLOUD_HAS_TAG"));
		}
		dynamicParameters = new HashMap<>();
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_SELECTED_DURATION"), inDuration);
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, inAccountId);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_SOURCE"), inSourceId);
		
		JdbcBrandDao jdbcBrand = (JdbcBrandDao) FactoryDao.getInstance(JDBC_BRAND_DAO);
		Result result = jdbcBrand.fetchWordCloudDataArray(staticParameters, dynamicParameters);
		LOGGER.info(">>>>>>>AnalyticsService ::: Exiting getBrandsWordcloud>>>>>>>>");
		return result;
	}

}
