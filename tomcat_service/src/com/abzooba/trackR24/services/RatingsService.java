package com.abzooba.trackR24.services;

import static com.abzooba.trackR24.util.Constants.ATTRIBUTE_SP_NAME;
import static com.abzooba.trackR24.util.Constants.ATTRIBUTE_SQL;
import static com.abzooba.trackR24.util.Constants.BLANK_STRING;
import static com.abzooba.trackR24.util.Constants.JDBC_ANALYTICS_DAO;
import static com.abzooba.trackR24.util.Constants.JDBC_RATINGS_DAO;
import static com.abzooba.trackR24.util.Constants.RESULTS;
import static com.abzooba.trackR24.util.Constants.SOURCE_ID;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_ACCOUNT_ID;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_CITY_ID;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_FROM_DT;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_OWNERSHIP_ID;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_SEARCH_TEXT;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_SOURCE_ID;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_TO_DT;
import static com.abzooba.trackR24.util.Constants.SP_NAME;
import static com.abzooba.trackR24.util.Constants.SP_SOURCE_WISE_STORES;
import static com.abzooba.trackR24.util.Constants.SP_SOURCE_WISE_STORES_CSV;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

import com.abzooba.trackR24.dao.DataSourceConnectionException;
import com.abzooba.trackR24.dao.DataSourceException;
import com.abzooba.trackR24.dao.FactoryDao;
import com.abzooba.trackR24.dao.JdbcAnalyticsDao;
import com.abzooba.trackR24.dao.JdbcRatingsDao;
import com.abzooba.trackR24.models.Result;
import com.abzooba.trackR24.util.PropertyHandler;
import com.fasterxml.jackson.databind.ObjectMapper;

@Path("/ratings")
@SuppressWarnings("unchecked")
public class RatingsService {

	private static final Logger LOGGER = Logger.getLogger(RatingsService.class);

	private Map<String, Object> staticParameters;
	private Map<String, Object> dynamicParameters;

	
	/********************************************************************
	* Get available city list on source selection
	* www.abzooba.com
	* -------------------
	* Created By: Ashis Nayak
	* Last Updated By: Ashis Nayak
	* Last Updated On: 20-11-2017 (GMT + 5.30)
	* Revision No: 
	*
	* The software and related user documentation are protected under
	* copyright laws and remain the sole property of Abzooba.
	*
	* Technical support is available via the Abzooba website at
	* http://www.abzooba.com
	**************************************************************************/
	
	@GET
	@Path("/getCitiesOnSourceSelection")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSources(@QueryParam("accountId") String accountId, @QueryParam("sourceId") String selectedSourceId, @QueryParam("inOwnerShipId") String ownerShipId) throws DataSourceException{
		LOGGER.info(">>>>>>>RatingsService ::: Entering getCitiesOnSourceSelectionUrl>>>>>>>>" + selectedSourceId);
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		dynamicParameters = new HashMap<>();
		
		if(ownerShipId.equalsIgnoreCase("0")){
			// Competitior comparsoin 
			if(selectedSourceId.equalsIgnoreCase("-1")){
				staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_GET_CITY_COMPETITORS_ONLOAD"));
				dynamicParameters.put(SP_IN_PARAM_OWNERSHIP_ID, "1");
				dynamicParameters.put(SOURCE_ID, "1");
				dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
			} else {
				staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_GET_CITY_COMPETITORS_ONSOURCE_CHANGE"));
				dynamicParameters.put(SP_IN_PARAM_OWNERSHIP_ID, "1");
				dynamicParameters.put(SOURCE_ID, selectedSourceId);
				dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
			}
			
		} else if(ownerShipId.equalsIgnoreCase("4")){
			// All tab
			if(selectedSourceId.equalsIgnoreCase("-1")){
				staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_GET_CITY_COMPETITORS_ONLOAD_ALL_TAB" ));
				dynamicParameters.put(SP_IN_PARAM_OWNERSHIP_ID, "1");
				dynamicParameters.put(SOURCE_ID, "1");
				dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
			} else {
				staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_GET_CITY_COMPETITORS_ONSOURCE_CHANGE_ALL_TAB"));
				dynamicParameters.put(SP_IN_PARAM_OWNERSHIP_ID, "1");
				dynamicParameters.put(SOURCE_ID, selectedSourceId);
				dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
			}
		} else {
			if(selectedSourceId.equalsIgnoreCase("-1")){
				staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_GET_CITY_RATINGS"));
				dynamicParameters.put(SP_IN_PARAM_OWNERSHIP_ID, ownerShipId);
				dynamicParameters.put(SOURCE_ID, "1");
				dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
				
			} else {
				staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_GET_CITY_ON_SOURCE_SELECTION_RATINGS"));
				dynamicParameters.put(SP_IN_PARAM_OWNERSHIP_ID, ownerShipId);
				dynamicParameters.put(SOURCE_ID, CommonService.checkForNull(selectedSourceId));
				dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
			}
		}
			
		
		JdbcAnalyticsDao jdbcCD = (JdbcAnalyticsDao) FactoryDao.getInstance(JDBC_ANALYTICS_DAO);
		Result result = jdbcCD.fetchRatingsSelectedCityList(staticParameters, dynamicParameters);
		
		LOGGER.info(">>>>>>>RatingsService ::: getCitiesOnSourceSelection ::: result from getCitiesOnSourceSelection>>>>>>>>" + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>RatingsService ::: Exiting getCitiesOnSourceSelection>>>>>>>>");
		return responseBuilder.build();
	}
	
	/********************************************************************
	* Get Top rated stores depending on there current rating available in DB
	* www.abzooba.com
	* -------------------
	* Created By: Ashis Nayak
	* Last Updated By: Ashis Nayak
	* Last Updated On: 03-03-2019
	* Revision No: 
	*
	* The software and related user documentation are protected under
	* copyright laws and remain the sole property of Abzooba.
	*
	* Technical support is available via the Abzooba website at
	* http://www.abzooba.com
	**************************************************************************/
	@POST
	@Path("/getTopratedStore")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTopratedStore(@FormParam("inSourceId") int sourceId, @FormParam("inCityId") int cityId, @FormParam("inAccountId") int accountId, @FormParam("inOwnerShipId") int ownerShipId) throws DataSourceException {

		LOGGER.info(">>>>>>>RatingsService ::: Entering getTopratedStore>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		String spName = null;
		staticParameters = new HashMap<>();
		spName = PropertyHandler.getPropertyValue("SP_TOP_RATED_STORES");
		LOGGER.debug(SP_NAME + spName);
		staticParameters.put(ATTRIBUTE_SP_NAME, spName);
		dynamicParameters = new HashMap<>();
		Integer inSourceId = sourceId == -1 ? null : Integer.valueOf(sourceId);
		Integer inCityId = cityId == -1 ? null : Integer.valueOf(cityId);
		
		addParams(accountId, inSourceId, inCityId);
		dynamicParameters.put(SP_IN_PARAM_OWNERSHIP_ID, ownerShipId);
		
		JdbcRatingsDao jdbcRD = (JdbcRatingsDao) FactoryDao.getInstance(JDBC_RATINGS_DAO);
		Result result = jdbcRD.fetchTopBottomRatedStores(staticParameters, dynamicParameters);
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>RatingsService ::: Exiting getTopratedStore>>>>>>>>"+result.toJSON());
		return responseBuilder.build();

	}
	
	/********************************************************************
	* Get average rating for top 10 stores
	* www.abzooba.com
	* -------------------
	* Created By: Ashis Nayak
	* Last Updated By: Ashis Nayak
	* Last Updated On: 13-03-2019 (GMT + 5.30)
	* Revision No: 
	*
	* The software and related user documentation are protected under
	* copyright laws and remain the sole property of Abzooba.
	*
	* Technical support is available via the Abzooba website at
	* http://www.abzooba.com
	**************************************************************************/
	@POST
	@Path("/getTopratedStoreAvg")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTopratedStoreAvg(@FormParam("inSourceId") int sourceId, @FormParam("inCityId") int cityId, @FormParam("inAccountId") int accountId, @FormParam("inOwnerShipId") int ownerShipId) throws DataSourceException {

		LOGGER.info(">>>>>>>RatingsService ::: Entering getTopratedStoreAvg>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		String spName = null;
		staticParameters = new HashMap<>();
		spName = PropertyHandler.getPropertyValue("SP_TOP_RATED_STORES_AVG");
		LOGGER.debug(SP_NAME + spName);
		staticParameters.put(ATTRIBUTE_SP_NAME, spName);
		dynamicParameters = new HashMap<>();
		Integer inSourceId = sourceId == -1 ? null : Integer.valueOf(sourceId);
		Integer inCityId = cityId == -1 ? null : Integer.valueOf(cityId);
		
		addParams(accountId, inSourceId, inCityId);
		dynamicParameters.put(SP_IN_PARAM_OWNERSHIP_ID, ownerShipId);
		
		JdbcRatingsDao jdbcRD = (JdbcRatingsDao) FactoryDao.getInstance(JDBC_RATINGS_DAO);
		Result result = jdbcRD.fetchTopBottomRatedStores(staticParameters, dynamicParameters);
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>RatingsService ::: Exiting getTopratedStoreAvg>>>>>>>>"+result.toJSON());
		return responseBuilder.build();

	}
	
	/********************************************************************
	* Get average rating for bottom 10 stores
	* www.abzooba.com
	* -------------------
	* Created By: Ashis Nayak
	* Last Updated By: Ashis Nayak
	* Last Updated On: 13-03-2019 (GMT + 5.30)
	* Revision No: 
	*
	* The software and related user documentation are protected under
	* copyright laws and remain the sole property of Abzooba.
	*
	* Technical support is available via the Abzooba website at
	* http://www.abzooba.com
	**************************************************************************/
	@POST
	@Path("/getBottomratedStoreAvg")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getBottomratedStoreAvg(@FormParam("inSourceId") int sourceId, @FormParam("inCityId") int cityId, @FormParam("inAccountId") int accountId, @FormParam("inOwnerShipId") int ownerShipId) throws DataSourceException {

		LOGGER.info(">>>>>>>RatingsService ::: Entering getTopratedStoreAvg>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		String spName = null;
		staticParameters = new HashMap<>();
		spName = PropertyHandler.getPropertyValue("SP_BOT_RATED_STORES_AVG");
		LOGGER.debug(SP_NAME + spName);
		staticParameters.put(ATTRIBUTE_SP_NAME, spName);
		dynamicParameters = new HashMap<>();
		Integer inSourceId = sourceId == -1 ? null : Integer.valueOf(sourceId);
		Integer inCityId = cityId == -1 ? null : Integer.valueOf(cityId);
		
		addParams(accountId, inSourceId, inCityId);
		dynamicParameters.put(SP_IN_PARAM_OWNERSHIP_ID, ownerShipId);
		
		JdbcRatingsDao jdbcRD = (JdbcRatingsDao) FactoryDao.getInstance(JDBC_RATINGS_DAO);
		Result result = jdbcRD.fetchTopBottomRatedStores(staticParameters, dynamicParameters);
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>RatingsService ::: Exiting getTopratedStoreAvg>>>>>>>>"+result.toJSON());
		return responseBuilder.build();

	}
	private void addParams(int accountId, Integer inSourceId, Integer inCityId) {
		dynamicParameters.put(SP_IN_PARAM_SOURCE_ID, inSourceId);
		dynamicParameters.put(SP_IN_PARAM_CITY_ID, inCityId);
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
	}

	
	/********************************************************************
	* Script to get rating across various competitor available for primary company
	* www.abzooba.com
	* -------------------
	* Created By: Ashis Nayak
	* Last Updated By: Ashis Nayak
	* Last Updated On: 01-02-2018 (GMT + 5.30)
	* Revision No: 
	*
	* The software and related user documentation are protected under
	* copyright laws and remain the sole property of Abzooba.
	*
	* Technical support is available via the Abzooba website at
	* http://www.abzooba.com
	**************************************************************************/
	@POST
	@Path("/getCompetitorComparisionStore")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCompetitorComparisionStore(@FormParam("inSourceId") int sourceId, @FormParam("inCityId") int cityId, @FormParam("inAccountId") int accountId) throws DataSourceException {

		LOGGER.info(">>>>>>>RatingsService ::: Entering getCompetitorComparisionStore>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		String spName = null;
		staticParameters = new HashMap<>();
		spName = PropertyHandler.getPropertyValue("SP_COMPETITOR_COMPARISION_STORES");
		LOGGER.debug(SP_NAME + spName);
		staticParameters.put(ATTRIBUTE_SP_NAME, spName);
		dynamicParameters = new HashMap<>();
		Integer inSourceId = sourceId == -1 ? null : Integer.valueOf(sourceId);
		Integer inCityId = cityId == -1 ? null : Integer.valueOf(cityId);
		
		addParams(accountId, inSourceId, inCityId);
		
		JdbcRatingsDao jdbcRD = (JdbcRatingsDao) FactoryDao.getInstance(JDBC_RATINGS_DAO);
		Result result = jdbcRD.fetchCompetitorComparisionStores(staticParameters, dynamicParameters);
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>RatingsService ::: Exiting getCompetitorComparisionStore>>>>>>>>"+ result.toJSON());
		return responseBuilder.build();

	}
	
	
	/********************************************************************
	* Script to get overall rating across stores
	* www.abzooba.com
	* -------------------
	* Created By: Ashis Nayak
	* Last Updated By: Ashis Nayak
	* Last Updated On: 12-11-2018 (GMT + 5.30)
	* Revision No: 
	*
	* The software and related user documentation are protected under
	* copyright laws and remain the sole property of Abzooba.
	*
	* Technical support is available via the Abzooba website at
	* http://www.abzooba.com
	**************************************************************************/
	@POST
	@Path("/getOverAllRatings")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getOverAllRatings(@FormParam("inCityId") int cityId, @FormParam("inAccountId") int accountId, @FormParam("inOwnerShipId") int ownerShipId) throws DataSourceConnectionException {

		LOGGER.info(">>>>>>>RatingsService ::: Entering getOverAllRatings>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		String spName = null;
		staticParameters = new HashMap<>();
		spName = PropertyHandler.getPropertyValue("SP_OVERALL_RATED_STORES");
		LOGGER.debug(SP_NAME + spName);
		staticParameters.put(ATTRIBUTE_SP_NAME, spName);
		dynamicParameters = new HashMap<>();
		Integer inCityId = cityId == -1 ? null : Integer.valueOf(cityId);
		
		dynamicParameters.put(SP_IN_PARAM_CITY_ID, inCityId);
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
		dynamicParameters.put(SP_IN_PARAM_OWNERSHIP_ID, ownerShipId);
		
		JdbcRatingsDao jdbcRD = (JdbcRatingsDao) FactoryDao.getInstance(JDBC_RATINGS_DAO);
		Result result = jdbcRD.fetchOverallRatingStores(staticParameters, dynamicParameters);
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>RatingsService ::: Exiting getOverAllRatings>>>>>>>>" + result.toJSON());
		return responseBuilder.build();

	}
	
	/********************************************************************
	* Script to get store list against selected source
	* www.abzooba.com
	* -------------------
	* Created By: Ashis Nayak
	* Last Updated By: Ashis Nayak
	* Last Updated On: 12-11-2018 (GMT + 5.30)
	* Revision No: 
	*
	* The software and related user documentation are protected under
	* copyright laws and remain the sole property of Abzooba.
	*
	* Technical support is available via the Abzooba website at
	* http://www.abzooba.com
	**************************************************************************/
	@POST
	@Path("/getSourcewisestorelists")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSourceWiseStoreLists(@FormParam("inCityId") int cityId, @FormParam("inAccountId") int accountId, @FormParam("inOwnerShipId") int ownerShipId , @FormParam("inSearchText") String strSearchText) throws DataSourceConnectionException {

		LOGGER.info(">>>>>>>RatingsService ::: Entering getSourceWiseStoreLists>>>>>>>>" + strSearchText);
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		String spName = null;
		staticParameters = new HashMap<>();
		spName = SP_SOURCE_WISE_STORES;
		LOGGER.debug(SP_NAME + spName);
		staticParameters.put(ATTRIBUTE_SP_NAME, spName);
		dynamicParameters = new HashMap<>();
		Integer inCityId = cityId == -1 ? null : Integer.valueOf(cityId);
		String inSearchText = strSearchText.equalsIgnoreCase("-1") ? null : strSearchText;
		dynamicParameters.put(SP_IN_PARAM_CITY_ID, inCityId);
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
		dynamicParameters.put(SP_IN_PARAM_OWNERSHIP_ID, ownerShipId);
		dynamicParameters.put(SP_IN_PARAM_SEARCH_TEXT, inSearchText);
		
		JdbcRatingsDao jdbcRD = (JdbcRatingsDao) FactoryDao.getInstance(JDBC_RATINGS_DAO);
		Result result = jdbcRD.fetchSourcewisestorelists(staticParameters, dynamicParameters);
		
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>RatingsService ::: Exiting getSourceWiseStoreLists>>>>>>>>");
		return responseBuilder.build();

	}	
	
	/********************************************************************
	* Script to get Ownership data from metadata table
	* www.abzooba.com
	* -------------------
	* Created By: Ashis Nayak
	* Last Updated By: Ashis Nayak
	* Last Updated On: 12-11-2018 (GMT + 5.30)
	* Revision No: 
	*
	* The software and related user documentation are protected under
	* copyright laws and remain the sole property of Abzooba.
	*
	* Technical support is available via the Abzooba website at
	* http://www.abzooba.com
	**************************************************************************/
	@POST
	@Path("/getWonershipMetaData")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTabMetaData(@FormParam("inAccountId") int accountId) throws DataSourceException {

		LOGGER.info(">>>>>>>RatingsService ::: Entering getWonershipMetaData>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		
		staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_GET_OWNERSHIP"));

		JdbcRatingsDao jdbcCD = (JdbcRatingsDao) FactoryDao.getInstance(JDBC_RATINGS_DAO);
		Result result = jdbcCD.fetchOwnershipList(staticParameters, dynamicParameters);
		
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>AnalyticsService ::: Exiting getAnalyticsDetails>>>>>>>>");
		return responseBuilder.build();

	}

	
	/********************************************************************
	* Script to get last crawling time for rating crawler from DB
	* www.abzooba.com
	* -------------------
	* Created By: Ashis Nayak
	* Last Updated By: Ashis Nayak
	* Last Updated On: 13-11-2018 (GMT + 5.30)
	* Revision No: 
	*
	* The software and related user documentation are protected under
	* copyright laws and remain the sole property of Abzooba.
	*
	* Technical support is available via the Abzooba website at
	* http://www.abzooba.com
	**************************************************************************/
	@GET
	@Path("/getCrawlingTime")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCrawlingTime(@QueryParam("inAccountId") int accountId) throws DataSourceException {

		LOGGER.info(">>>>>>>RatingsService ::: Entering getCrawlingTime>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		
		staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_CRAWLING_LAST_TIME"));

		JdbcRatingsDao jdbcCD = (JdbcRatingsDao) FactoryDao.getInstance(JDBC_RATINGS_DAO);
		Result result = jdbcCD.fetchOwnershipList(staticParameters, dynamicParameters);
		
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>AnalyticsService ::: Exiting getCrawlingTime>>>>>>>>");
		return responseBuilder.build();

	}
	
	/********************************************************************
	* Script to get store list those are performing low
	* www.abzooba.com
	* -------------------
	* Created By: Ashis Nayak
	* Last Updated By: Ashis Nayak
	* Last Updated On: 13-11-2018 (GMT + 5.30)
	* Revision No: 
	*
	* The software and related user documentation are protected under
	* copyright laws and remain the sole property of Abzooba.
	*
	* Technical support is available via the Abzooba website at
	* http://www.abzooba.com
	**************************************************************************/
	@POST
	@Path("/getBottomratedStore")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getBottomratedStore(@FormParam("inSourceId") int sourceId, @FormParam("inCityId") int cityId, @FormParam("inAccountId") int accountId, @FormParam("inOwnerShipId") int ownerShipId) throws DataSourceException{

		LOGGER.info(">>>>>>>RatingsService ::: Entering getBottomratedStore>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		String spName = null;
		staticParameters = new HashMap<>();
		spName = PropertyHandler.getPropertyValue("SP_BOTTOM_RATED_STORES");
		LOGGER.debug(SP_NAME + spName);
		staticParameters.put(ATTRIBUTE_SP_NAME, spName);
		dynamicParameters = new HashMap<>();
		Integer inSourceId = sourceId == -1 ? null : Integer.valueOf(sourceId);
		Integer inCityId = cityId == -1 ? null : Integer.valueOf(cityId);
		
		addParams(accountId, inSourceId, inCityId);
		dynamicParameters.put(SP_IN_PARAM_OWNERSHIP_ID, ownerShipId);
		
		JdbcRatingsDao jdbcRD = (JdbcRatingsDao) FactoryDao.getInstance(JDBC_RATINGS_DAO);
		Result result = jdbcRD.fetchTopBottomRatedStores(staticParameters, dynamicParameters);
		LOGGER.info("Result set JSON : " + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>RatingsService ::: Exiting getBottomratedStore>>>>>>>>");
		return responseBuilder.build();

	}

	/********************************************************************
	* Script to get city and source wise store rating historical data
	* www.abzooba.com
	* -------------------
	* Created By: Ashis Nayak
	* Last Updated By: Ashis Nayak
	* Last Updated On: 10-02-2019 (GMT + 5.30)
	* Revision No: 
	*
	* The software and related user documentation are protected under
	* copyright laws and remain the sole property of Abzooba.
	*
	* Technical support is available via the Abzooba website at
	* http://www.abzooba.com
	**************************************************************************/
	@POST
	@Path("/getCityWiseSourceWiseStoreHistory")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCityWiseSourceWiseStoreHistory(@FormParam("inSourceId") int sourceId, @FormParam("inCityId") int cityId, 
			@FormParam("inAccountId") int accountId, @FormParam("inOwnerShipId") int ownerShipId,
			@FormParam("start_date") String startdate,@FormParam("end_date") String enddate) throws DataSourceException{

		LOGGER.info(">>>>>>>RatingsService ::: Entering getCityWiseSourceWiseStoreHistory>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		String spName = null;
		staticParameters = new HashMap<>();
		spName = PropertyHandler.getPropertyValue("SP_SOURCE_CITY_WISE_STORE_RATING_CHART");
		LOGGER.debug(SP_NAME + spName);
		staticParameters.put(ATTRIBUTE_SP_NAME, spName);
		dynamicParameters = new HashMap<>();
		Integer inSourceId = sourceId == -1 ? null : Integer.valueOf(sourceId);
		Integer inCityId = cityId == -1 ? null : Integer.valueOf(cityId);
		
		addParams(accountId, inSourceId, inCityId);
		dynamicParameters.put(SP_IN_PARAM_OWNERSHIP_ID, ownerShipId);
		dynamicParameters.put(SP_IN_PARAM_FROM_DT, startdate);
		dynamicParameters.put(SP_IN_PARAM_TO_DT, enddate);
		
		JdbcRatingsDao jdbcRD = (JdbcRatingsDao) FactoryDao.getInstance(JDBC_RATINGS_DAO);
		Result result = jdbcRD.fetchCitywiseSourcewiseStoresRating(staticParameters, dynamicParameters);
		LOGGER.info("Result set JSON : " + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>RatingsService ::: Exiting getCityWiseSourceWiseStoreHistory>>>>>>>>");
		return responseBuilder.build();

	}

	/********************************************************************
	* Script to generate store list in a csv format
	* www.abzooba.com
	* -------------------
	* Created By: Ashis Nayak
	* Last Updated By: Ashis Nayak
	* Last Updated On: 12-11-2018 (GMT + 5.30)
	* Revision No: 
	*
	* The software and related user documentation are protected under
	* copyright laws and remain the sole property of Abzooba.
	*
	* Technical support is available via the Abzooba website at
	* http://www.abzooba.com
	**************************************************************************/
	@POST
	@Path("/getStoreListInExcelFormat")
	public String getStoreListInExcelFormat(@FormParam("inCityId") int cityId, @FormParam("inAccountId") int accountId, @FormParam("inOwnerShipId") int ownerShipId,@FormParam("inSearchText") String inSearchText) throws DataSourceConnectionException, FileNotFoundException {
		LOGGER.info(">>>>>>>RatingsService ::: Entering getStoreListInExcelFormat>>>>>>>>");
		String filePath = PropertyHandler.getPropertyValue("SOURCE_WISE_RATING_FILE_PATH");
		String spName = null;
		staticParameters = new HashMap<>();
		spName = SP_SOURCE_WISE_STORES;
		LOGGER.debug(SP_NAME + spName);
		staticParameters.put(ATTRIBUTE_SP_NAME, spName);
		dynamicParameters = new HashMap<>();
		Integer inCityId = cityId == -1 ? null : Integer.valueOf(cityId);
		String strSearchText = inSearchText.equalsIgnoreCase("-1") ? null : inSearchText;
		dynamicParameters.put(SP_IN_PARAM_CITY_ID, inCityId);
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
		dynamicParameters.put(SP_IN_PARAM_OWNERSHIP_ID, ownerShipId);
		dynamicParameters.put(SP_IN_PARAM_SEARCH_TEXT, strSearchText);
		
		JdbcRatingsDao jdbcRD = (JdbcRatingsDao) FactoryDao.getInstance(JDBC_RATINGS_DAO);
		jdbcRD.fetchSourcewisestorelistsinExcel(staticParameters, dynamicParameters , filePath);
		
		LOGGER.info(">>>>>>>RatingsService ::: Leaving getStoreListInExcelFormat>>>>>>>>");
		return filePath;
	}
	
	public ResponseBuilder prepareHeadlineData(Result result) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, IOException {

		LOGGER.info(">>>>>>>CommonService ::: Entering prepareHeadlineData>>>>>>>>");
		LOGGER.info(">>>>>>>CommonService ::: prepareHeadlineData ::: parameters>>>>>>>>" + result.toJSON());
		ResponseBuilder responseBuilder;
		ObjectMapper mapper = new ObjectMapper();

		Method fieldGetter = result.getClass().getMethod("getRows");
		List headlineListData = (List) fieldGetter.invoke(result);
		List<HeadlineObject> headlineObjList = new ArrayList<>();
		for (Object rowObj : headlineListData) {
			HeadlineObject headlineObj = new HeadlineObject();
			headlineObj.setHeadline(((List) rowObj).get(0).toString());
			headlineObj.setPublishDatetime(((List) rowObj).get(1).toString());
			headlineObj.setNewsUrl(((List) rowObj).get(2).toString());
			headlineObjList.add(headlineObj);
		}
		String headlineObjListResultJSON = mapper.writeValueAsString(headlineObjList);
		LOGGER.debug(">>>>>>>CommonService ::: prepareHeadlineData ::: headlineObjListResultJSON>>>>>>>>" + headlineObjListResultJSON);
		responseBuilder = Response.ok(headlineObjListResultJSON);
		return responseBuilder;
	}
	
		
	/********************************************************************
	* This method just initializes the Map with the abbreviated contexts as key with blank string as corresponding value.
	* www.abzooba.com
	* -------------------
	* Created By: Ashis Nayak
	* Last Updated By: Ashis Nayak
	* Last Updated On: 01-02-2018 (GMT + 5.30)
	* Revision No: 
	*
	* The software and related user documentation are protected under
	* copyright laws and remain the sole property of Abzooba.
	*
	* Technical support is available via the Abzooba website at
	* http://www.abzooba.com
	**************************************************************************/
	public Map<String, String> initContextWiseSentimentMap(Map<String, String> contextWiseSentiment) {
		contextWiseSentiment.put(PropertyHandler.getPropertyValue("CLEANLINESS"),BLANK_STRING);
		contextWiseSentiment.put(PropertyHandler.getPropertyValue("HOSPITALITY"),BLANK_STRING);
		contextWiseSentiment.put(PropertyHandler.getPropertyValue("ACCURACY"),BLANK_STRING);
		contextWiseSentiment.put(PropertyHandler.getPropertyValue("MAINTENANCE"),BLANK_STRING);
		contextWiseSentiment.put(PropertyHandler.getPropertyValue("PRODUCT_QUALITY"),BLANK_STRING);
		contextWiseSentiment.put(PropertyHandler.getPropertyValue("SPEED"),BLANK_STRING);
		contextWiseSentiment.put(PropertyHandler.getPropertyValue("PRICES_AND_OFFERS_ABR"),BLANK_STRING);
		contextWiseSentiment.put(PropertyHandler.getPropertyValue("OTHERS"),BLANK_STRING);
		contextWiseSentiment.put(PropertyHandler.getPropertyValue("NON_RELEVANT"),BLANK_STRING);
		return contextWiseSentiment;
	}
	
	
	public Map<String, String> getExcelFormat(Integer intCityId,String inAccountId, String inOwnerShipId, String strSearchText) throws DataSourceConnectionException {

		LOGGER.info(">>>>>>>RatingsService ::: Entering getStoreListInExcelFormat>>>>>>>>");
		String spName = null;
		staticParameters = new HashMap<>();
		spName = SP_SOURCE_WISE_STORES_CSV;
		LOGGER.info(SP_NAME + spName);
		staticParameters.put(ATTRIBUTE_SP_NAME, spName);
		dynamicParameters = new HashMap<>();
		dynamicParameters.put(SP_IN_PARAM_CITY_ID, intCityId);
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, Integer.parseInt(inAccountId));
		dynamicParameters.put(SP_IN_PARAM_OWNERSHIP_ID, Integer.parseInt(inOwnerShipId));
		dynamicParameters.put(SP_IN_PARAM_SEARCH_TEXT, strSearchText);
		
		JdbcRatingsDao jdbcRD = (JdbcRatingsDao) FactoryDao.getInstance(JDBC_RATINGS_DAO);
		Map<String, String> csvData = jdbcRD.fetchSourcewisestorelistsinExcelNew(staticParameters, dynamicParameters);
		
		LOGGER.info(">>>>>>>RatingsService ::: Leaving getStoreListInExcelFormat>>>>>>>>");
		return csvData;
		
		

	}

	/********************************************************************
	* This service method is responsible for retrieving all the sources related to rating page.
	* www.abzooba.com
	* -------------------
	* Created By: Ashis Nayak
	* Last Updated By: Ashis Nayak
	* Last Updated On: 01-02-2018 (GMT + 5.30)
	* Revision No: 
	*
	* The software and related user documentation are protected under
	* copyright laws and remain the sole property of Abzooba.
	*
	* Technical support is available via the Abzooba website at
	* http://www.abzooba.com
	**************************************************************************/
	@GET
	@Path("/getSourcesForRating")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSourcesForRating(@QueryParam("accountId") int accountId, @QueryParam("type") String type) throws DataSourceException {
		
		LOGGER.info(">>>>>>>CommonService ::: Entering getSourcesForRating>>>>>>>>" + type);
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		if ("sources".equalsIgnoreCase(type)) {
			staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_GET_SOURCES"));
			dynamicParameters = new HashMap<>();
			dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
		} else if ("ratingCities".equalsIgnoreCase(type)) {
			staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_GET_CITIES_RATING"));
		} else if ("topRatingSources".equalsIgnoreCase(type)) {
			staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_GET_SOURCES_RATING"));
		} 
		
		JdbcAnalyticsDao jdbcCD = (JdbcAnalyticsDao) FactoryDao.getInstance(JDBC_ANALYTICS_DAO);
		Result result = jdbcCD.fetchSourcesOrContexts(staticParameters, dynamicParameters);
		LOGGER.info(">>>>>>>CommonService ::: getSources ::: result from fetchSourcesOrContexts>>>>>>>>" + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>CommonService ::: Exiting getSources>>>>>>>>");
		return responseBuilder.build();
	}
	


}
