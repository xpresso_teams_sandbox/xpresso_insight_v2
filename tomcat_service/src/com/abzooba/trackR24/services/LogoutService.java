package com.abzooba.trackR24.services;

import java.net.URI;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.apache.log4j.Logger;

/**
 * Author Name: Kunal Kashyap
 * Create Date: 21-07-2016
 * Class Insight: This class is responsible for logout funtionality in the application. 
 */
@Path("/logoutservice")
public class LogoutService {
	private static final Logger LOGGER = Logger.getLogger(LogoutService.class);
	URI uri;

	@GET
	@Path("/logout")
	public Response checkLogout(@Context HttpServletRequest request){
		LOGGER.info(">>>>>>>LogoutService ::: Entering checkLogout>>>>>>>>");
		HttpSession session = request.getSession();
		session.removeAttribute("User");
		session.invalidate();
		uri = UriBuilder.fromPath("../login.jsp").build();
		LOGGER.info(">>>>>>>LogoutService ::: Exiting checkLogout>>>>>>>>>");
		return Response.seeOther(uri).build();
	}

}
