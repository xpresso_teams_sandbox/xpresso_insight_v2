package com.abzooba.trackR24.services;

import static com.abzooba.trackR24.util.Constants.ATTRIBUTE_SP_NAME;
import static com.abzooba.trackR24.util.Constants.BEAN_NAME;
import static com.abzooba.trackR24.util.Constants.HIBERNATE_KPI_DAO;
import static com.abzooba.trackR24.util.Constants.POST;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_ACCOUNT_ID;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_SELECTED_DURATION_KPISERVICE;
import static com.abzooba.trackR24.util.Constants.SP_KPI_FAN_GROWTH_RATE;
import static com.abzooba.trackR24.util.Constants.SP_KPI_FOLLOWER_GROWTH_RATE;
import static com.abzooba.trackR24.util.Constants.SP_KPI_RIBBON_FACEBOOK;
import static com.abzooba.trackR24.util.Constants.SP_KPI_TOP_ENGAGING_POSTS;
import static com.abzooba.trackR24.util.Constants.TWEET;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;

import com.abzooba.trackR24.dao.FactoryDao;
import com.abzooba.trackR24.dao.HibernateKPIDao;
import com.abzooba.trackR24.models.KPIParentBean;
import com.abzooba.trackR24.models.KPIServiceBean;
import com.abzooba.trackR24.util.PropertyHandler;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Author Name: Kunal Kashyap
 * Create Date: 18-10-2016
 * Class Insight: This class contains all the service methods for calling the dao layer of all the KPI functionalities of KPI page. 
 */
@Path("/kpi")
public class KPIService {

	private static final Logger LOGGER = Logger.getLogger(KPIService.class);

	private Map<String, Object> staticParameters;
	private Map<String, Object> dynamicParameters;

	/**
	 * Method Insight: This method calls the dao layer for facebook ribbon in KPI page.
	 * @param accountId
	 * @return Response
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonGenerationException 
	 * @throws Exception
	 */
	@POST
	@Path("/getFacebookKPI")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getFacebookKPI(@FormParam("accountId") int accountId) throws IllegalAccessException, InvocationTargetException, IOException  {
		LOGGER.info(">>>>>>>KPIService ::: Entering getFacebookKPI>>>>>>>>");
		ResponseBuilder responseBuilder = null;
		staticParameters = new HashMap<>();
		staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue(SP_KPI_RIBBON_FACEBOOK));
		addAccountIdInHashMap(accountId);
		HibernateKPIDao hibernateKD = (HibernateKPIDao) FactoryDao.getInstance(HIBERNATE_KPI_DAO);
		KPIParentBean kpiParentBean = hibernateKD.getFacebookKPI(staticParameters, dynamicParameters);
		LOGGER.debug(BEAN_NAME+kpiParentBean.toString());

		responseBuilder = getResponseBuilderOutput(kpiParentBean);
		LOGGER.info(">>>>>>>KPIService ::: Exiting getFacebookKPI>>>>>>>>");
		return responseBuilder.build();
	}

	private void addAccountIdInHashMap(int accountId) {
		dynamicParameters = new HashMap<>();
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
	}

	private ResponseBuilder getResponseBuilderOutput(KPIParentBean kpiParentBean)
			throws IllegalAccessException, InvocationTargetException,
			IOException, JsonGenerationException, JsonMappingException {
		ResponseBuilder responseBuilder;
		/*Mapping from one bean to another*/
		KPIServiceBean kpiServiceBean = new KPIServiceBean();
		BeanUtils.copyProperties(kpiServiceBean, kpiParentBean);

		/*Converting object to JSON String*/
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = mapper.writeValueAsString(kpiServiceBean);

		responseBuilder = Response.ok(jsonInString);
		return responseBuilder;
	}

	/**
	 * Method Insight: This method calls the dao layer for twitter ribbon in KPI page.
	 * @param accountId
	 * @return Response
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonGenerationException 
	 * @throws Exception
	 */
	@POST
	@Path("/getTwitterKPI")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTwitterKPI(@FormParam("accountId") int accountId) throws IllegalAccessException, InvocationTargetException, IOException {
		LOGGER.info(">>>>>>>KPIService ::: Entering getTwitterKPI>>>>>>>>");
		ResponseBuilder responseBuilder = null;
		staticParameters = new HashMap<>();
		staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_KPI_RIBBON_TWITTER"));
		addAccountIdInHashMap(accountId);
		HibernateKPIDao hibernateKD = (HibernateKPIDao) FactoryDao.getInstance(HIBERNATE_KPI_DAO);
		KPIParentBean kpiParentBean = hibernateKD.getTwitterKPI(staticParameters, dynamicParameters);
		LOGGER.debug(BEAN_NAME+kpiParentBean.toString());

		responseBuilder = getResponseBuilderOutput(kpiParentBean);
		LOGGER.info(">>>>>>>KPIService ::: Exiting getTwitterKPI>>>>>>>>");
		return responseBuilder.build();
	}

	/**
	 * Method Insight: This method calls the dao layer for follower and fan growth rate in KPI page.
	 * @param accountId,selectedInterval,type
	 * @return Response
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonGenerationException 
	 * @throws Exception
	 */
	@POST
	@Path("/getFollowerFanGrowthRate")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getFollowerFanGrowthRate(@FormParam("accountId") int accountId, @FormParam("selectedInterval") Integer selectedInterval, @FormParam("type") String type) throws IllegalAccessException, InvocationTargetException, IOException {
		LOGGER.info(">>>>>>>KPIService ::: Entering getFollowerFanGrowthRate>>>>>>>>");
		ResponseBuilder responseBuilder = null;
		staticParameters = new HashMap<>();
		if (type.equalsIgnoreCase(TWEET)) {
			staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue(SP_KPI_FOLLOWER_GROWTH_RATE));
		} else if (type.equalsIgnoreCase(POST)) {
			staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue(SP_KPI_FAN_GROWTH_RATE));
		}
		addAccountIdInHashMap(accountId);
		dynamicParameters.put(PropertyHandler.getPropertyValue(SP_IN_PARAM_SELECTED_DURATION_KPISERVICE), selectedInterval);
		HibernateKPIDao hibernateKD = (HibernateKPIDao) FactoryDao.getInstance(HIBERNATE_KPI_DAO);
		KPIParentBean kpiParentBean = hibernateKD.getFollowerFanGrowthRate(staticParameters, dynamicParameters);
		LOGGER.debug(BEAN_NAME+kpiParentBean.toString());

		responseBuilder = getResponseBuilderOutput(kpiParentBean);
		LOGGER.info(">>>>>>>KPIService ::: Exiting getFollowerFanGrowthRate>>>>>>>>");
		return responseBuilder.build();
	}

	/**
	 * Method Insight: This method calls the dao layer for top engaging tweets in KPI page.
	 * @param accountId,days
	 * @return Response
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonGenerationException 
	 * @throws Exception
	 */
	@POST
	@Path("/getTopEngagingTweets")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTopEngagingTweets(@FormParam("accountId") int accountId) throws IllegalAccessException, InvocationTargetException, IOException {
		LOGGER.info(">>>>>>>KPIService ::: Entering getTopEngagingTweets>>>>>>>>");
		ResponseBuilder responseBuilder = null;
		staticParameters = new HashMap<>();
		staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_KPI_TOP_ENGAGING_TWEETS"));
		addAccountIdInHashMap(accountId);
		HibernateKPIDao hibernateKD = (HibernateKPIDao) FactoryDao.getInstance(HIBERNATE_KPI_DAO);
		KPIParentBean kpiParentBean = hibernateKD.getTopEngagingTweets(staticParameters, dynamicParameters);
		LOGGER.debug(BEAN_NAME+kpiParentBean.toString());

		responseBuilder = getResponseBuilderOutput(kpiParentBean);
		LOGGER.info(">>>>>>>KPIService ::: Exiting getTopEngagingTweets>>>>>>>>");
		return responseBuilder.build();
	}

	/**
	 * Method Insight: This method calls the dao layer for top engaging posts in KPI page.
	 * @param accountId,days
	 * @return Response
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws  
	 * @throws Exception
	 */
	@POST
	@Path("/getTopEngagingPosts")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTopEngagingPosts(@FormParam("accountId") int accountId) throws IllegalAccessException, InvocationTargetException, IOException{
		LOGGER.info(">>>>>>>KPIService ::: Entering getTopEngagingPosts>>>>>>>>");
		ResponseBuilder responseBuilder = null;
		staticParameters = new HashMap<>();
		staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue(SP_KPI_TOP_ENGAGING_POSTS));
		addAccountIdInHashMap(accountId);
		HibernateKPIDao hibernateKD = (HibernateKPIDao) FactoryDao.getInstance(HIBERNATE_KPI_DAO);
		KPIParentBean kpiParentBean = hibernateKD.getTopEngagingPosts(staticParameters, dynamicParameters);
		LOGGER.debug(BEAN_NAME+kpiParentBean.toString());

		responseBuilder = getResponseBuilderOutput(kpiParentBean);
		LOGGER.info(">>>>>>>KPIService ::: Exiting getTopEngagingPosts>>>>>>>>");
		return responseBuilder.build();
	}

	/**
	 * Method Insight: This method calls the dao layer for per tweet and post interaction in KPI page.
	 * @param accountId,type
	 * @return Response
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * @throws IOException 
	 * @throws  
	 * @throws  
	 * @throws Exception
	 */
	@POST
	@Path("/getPerTweetPostInteraction")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPerTweetPostInteraction(@FormParam("accountId") int accountId, @FormParam("type") String type) throws IllegalAccessException, InvocationTargetException, IOException  {
		LOGGER.info(">>>>>>>KPIService ::: Entering getPerTweetPostInteraction>>>>>>>>");
		ResponseBuilder responseBuilder = null;
		staticParameters = new HashMap<>();
		if (type.equalsIgnoreCase(TWEET)) {
			staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_KPI_PER_TWEET_INTERACTION"));
		} else if (type.equalsIgnoreCase(POST)) {
			staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_KPI_PER_POST_INTERACTION"));
		}
		addAccountIdInHashMap(accountId);
		HibernateKPIDao hibernateKD = (HibernateKPIDao) FactoryDao.getInstance(HIBERNATE_KPI_DAO);
		KPIParentBean kpiParentBean = hibernateKD.gePerTweetPostInteraction(staticParameters, dynamicParameters);
		LOGGER.debug(BEAN_NAME+kpiParentBean.toString());

		responseBuilder = getResponseBuilderOutput(kpiParentBean);
		LOGGER.info(">>>>>>>KPIService ::: Exiting getPerTweetPostInteraction>>>>>>>>");
		return responseBuilder.build();

	}

	@POST
	@Path("/getTopEngagingTweetsPostsDistributionMonthly")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTopEngagingTweetsPostsMonthly(@FormParam("accountId") int accountId, @FormParam("type") String type) throws IllegalAccessException, InvocationTargetException, IOException {
		LOGGER.info(">>>>>>>KPIService ::: Entering getTopEngagingTweetsPostsDistributionMonthly>>>>>>>>");
		ResponseBuilder responseBuilder = null;
		staticParameters = new HashMap<>();
		switch (type) {
			case TWEET:
				staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_KPI_TOP_ENGAGING_TWEETS_DISTRIBUTION_MONTHLY"));
				break;
			case POST:
				staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_KPI_TOP_ENGAGING_POSTS_DISTRIBUTION_MONTHLY"));
				break;
			default:
				LOGGER.info("NO TYPE");
		}
		addAccountIdInHashMap(accountId);
		HibernateKPIDao hibernateKD = (HibernateKPIDao) FactoryDao.getInstance(HIBERNATE_KPI_DAO);
		KPIParentBean kpiParentBean = hibernateKD.getTopEngagingTweetsPostsDistributionMonthly(staticParameters, dynamicParameters);
		LOGGER.debug(BEAN_NAME+kpiParentBean.toString());

		responseBuilder = getResponseBuilderOutput(kpiParentBean);
		LOGGER.info(">>>>>>>KPIService ::: Exiting getTopEngagingTweetsPostsDistributionMonthly>>>>>>>>");
		return responseBuilder.build();

	}

	@POST
	@Path("/getTopEngagingTweetsPostsDistributionDaily")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTopEngagingTweetsPostsDistributionDaily(@FormParam("accountId") int accountId, @FormParam("selectedDays") int selectedDays, @FormParam("type") String type) throws IllegalAccessException, InvocationTargetException, IOException {
		LOGGER.info(">>>>>>>KPIService ::: Entering getTopEngagingTweetsPostsDistributionDaily>>>>>>>>");
		ResponseBuilder responseBuilder = null;
		staticParameters = new HashMap<>();
		switch (type) {
			case TWEET:
				staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_KPI_TOP_ENGAGING_TWEETS_DISTRIBUTION_DAILY"));
				break;
			case POST:
				staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_KPI_TOP_ENGAGING_POSTS_DISTRIBUTION_DAILY"));
				break;
			default:
				LOGGER.info("NO TYPE");
		}
		addAccountIdInHashMap(accountId);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_SELECTED_DAYS"), selectedDays);
		HibernateKPIDao hibernateKD = (HibernateKPIDao) FactoryDao.getInstance(HIBERNATE_KPI_DAO);
		KPIParentBean kpiParentBean = hibernateKD.getTopEngagingTweetsPostsDistributionDaily(staticParameters, dynamicParameters);
		LOGGER.debug(BEAN_NAME+kpiParentBean.toString());

		responseBuilder = getResponseBuilderOutput(kpiParentBean);
		LOGGER.info(">>>>>>>KPIService ::: Exiting getTopEngagingTweetsPostsDistributionDaily>>>>>>>>");
		return responseBuilder.build();

	}
}
