package com.abzooba.trackR24.services;

import static com.abzooba.trackR24.util.Constants.ASPECTS_FOR_WORD_CLOUD;
import static com.abzooba.trackR24.util.Constants.ATTRIBUTE_SP_NAME;
import static com.abzooba.trackR24.util.Constants.ATTRIBUTE_SQL;
import static com.abzooba.trackR24.util.Constants.BLANK_STRING;
import static com.abzooba.trackR24.util.Constants.COUNT;
import static com.abzooba.trackR24.util.Constants.END_TIME;
import static com.abzooba.trackR24.util.Constants.ENTITIES_FOR_WORD_CLOUD;
import static com.abzooba.trackR24.util.Constants.GET_COUNT;
import static com.abzooba.trackR24.util.Constants.GET_ROWS;
import static com.abzooba.trackR24.util.Constants.JDBC_ANALYTICS_DAO;
import static com.abzooba.trackR24.util.Constants.JDBC_DASHBOARD_DAO;
import static com.abzooba.trackR24.util.Constants.OUTPUT;
import static com.abzooba.trackR24.util.Constants.PRICES_AND_OFFERS_ABR;
import static com.abzooba.trackR24.util.Constants.PRICES_AND_OFFERS_CONTEXT;
import static com.abzooba.trackR24.util.Constants.RESULTS;
import static com.abzooba.trackR24.util.Constants.SOCIAL_LISTENING;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_ACCOUNT_ID;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_CITY;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_FROM_DT;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_FROM_DT_CURRENT;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_FROM_DT_PREVIOUS;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_LIMIT;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_OFFSET;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_PERIODIC_END_FROM;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_PERIODIC_END_TO;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_PERIODIC_START_FROM;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_PERIODIC_START_TO;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_PRIMARY_COMPANY_ID;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_SELECTED_DURATION;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_SENTIMENT_TYPE;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_SOURCE;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_SOURCE_ID;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_TOPIC;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_TO_DT;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_TO_DT_CURRENT;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_TO_DT_PREVIOUS;
import static com.abzooba.trackR24.util.Constants.SP_SOCIAL_POST_FILTER_LIST;
import static com.abzooba.trackR24.util.Constants.SP_WORD_CLOUD_SOCIAL_POST_LIST;
import static com.abzooba.trackR24.util.Constants.START_TIME;
import static com.abzooba.trackR24.util.Constants.WORD_CLOUD;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

import com.abzooba.trackR24.dao.DataSourceConnectionException;
import com.abzooba.trackR24.dao.DataSourceException;
import com.abzooba.trackR24.dao.FactoryDao;
import com.abzooba.trackR24.dao.JdbcAnalyticsDao;
import com.abzooba.trackR24.dao.JdbcDashboardDao;
import com.abzooba.trackR24.models.DeepListeningObject;
import com.abzooba.trackR24.models.NonSocialPostListObject;
import com.abzooba.trackR24.models.PostWiseEngagementSentimentObject;
import com.abzooba.trackR24.models.Result;
import com.abzooba.trackR24.models.SocialPostListObject;
import com.abzooba.trackR24.util.PropertyHandler;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Path("/analytics")
@SuppressWarnings("unchecked")
public class AnalyticsService {

	private static final Logger LOGGER = Logger.getLogger(AnalyticsService.class);

	private Map<String, Object> staticParameters;
	private Map<String, Object> dynamicParameters;

	
	/**
	 * @Author Name: Ashis Nayak
	 * @Create Date: 11-01-2017
	 * @Last Modified: 
	 * @Input:
	 * @Output: Response
	 * @Method Insight: this method calls the SP for top five news headlines from Google News
	 * @throws DataSourceException, IOException, InvocationTargetException, IllegalArgumentException, IllegalAccessException, SecurityException, NoSuchMethodException, JsonMappingException, JsonGenerationException 
	 */
	@GET
	@Path("/getHeadlines")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getHeadlines() throws DataSourceException, NoSuchMethodException, IllegalAccessException,InvocationTargetException, IOException {

		LOGGER.info(">>>>>>>CommonService ::: Entering getHeadlines>>>>>>>>");
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_GET_HEADLINES"));

		JdbcAnalyticsDao jdbcCD = (JdbcAnalyticsDao) FactoryDao.getInstance(JDBC_ANALYTICS_DAO);
		Result result = jdbcCD.fetchHeadlines(staticParameters, dynamicParameters);
		LOGGER.info(">>>>>>>CommonService ::: getHeadlines ::: result from fetchHeadlines>>>>>>>>" + result.toJSON());

		responseBuilder = prepareHeadlineData(result);
		LOGGER.info(">>>>>>>CommonService ::: Exiting getHeadlines>>>>>>>>");
		return responseBuilder.build();

	}
	
	/**
	 * 
	 * @Author Name: Ashis Nayak
	 * @Create Date: 11-10-2017
	 * @Last Modified: 
	 * @Input:
	 * @Output: Response
	 * @Method Insight: this method calls the SP for getting durations
	 * @throws DataSourceException, IOException, InvocationTargetException, IllegalArgumentException, IllegalAccessException, SecurityException, NoSuchMethodException, JsonMappingException, JsonGenerationException 
	 */
	@GET
	@Path("/getDurations")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDurations() throws DataSourceException{

		LOGGER.info(">>>>>>>AnalyticsService ::: Entering getDurations>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_GET_DURATIONS"));

		JdbcAnalyticsDao jdbcCD = (JdbcAnalyticsDao) FactoryDao.getInstance(JDBC_ANALYTICS_DAO);
		Result result = jdbcCD.fetchDurations(staticParameters, dynamicParameters);
		LOGGER.info(">>>>>>>CommonService ::: getDuration ::: result from getDuration>>>>>>>>" + result.toJSON());
		
		LOGGER.info("Result set JSON : " + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		return responseBuilder.build();
	}	
	
	@POST
	@Path("/getCompanies")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCompanies(@FormParam("inAccountId") int accountId) throws DataSourceException  {

		LOGGER.info(">>>>>>>AnalyticsService ::: Entering getCompanies>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		dynamicParameters = new HashMap<>();
		
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
		staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_GET_COMPANY"));

		JdbcAnalyticsDao jdbcCD = (JdbcAnalyticsDao) FactoryDao.getInstance(JDBC_ANALYTICS_DAO);
		Result result = jdbcCD.fetchCompanyList(staticParameters, dynamicParameters);
		
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>AnalyticsService ::: Exiting getCompanies>>>>>>>>");
		return responseBuilder.build();

	}

	
	public ResponseBuilder prepareHeadlineData(Result result) throws NoSuchMethodException,IllegalAccessException, InvocationTargetException, IOException {

		LOGGER.info(">>>>>>>CommonService ::: Entering prepareHeadlineData>>>>>>>>");
		LOGGER.info(">>>>>>>CommonService ::: prepareHeadlineData ::: parameters>>>>>>>>" + result.toJSON());
		ResponseBuilder responseBuilder;
		ObjectMapper mapper = new ObjectMapper();

		Method fieldGetter = result.getClass().getMethod(GET_ROWS);
		List headlineListData = (List) fieldGetter.invoke(result);
		List<HeadlineObject> headlineObjList = new ArrayList<>();
		for (Object rowObj : headlineListData) {
			HeadlineObject headlineObj = new HeadlineObject();
			headlineObj.setHeadline(((List) rowObj).get(0).toString());
			headlineObj.setPublishDatetime(((List) rowObj).get(1).toString());
			headlineObj.setNewsUrl(((List) rowObj).get(2).toString());
			headlineObjList.add(headlineObj);
		}
		String headlineObjListResultJSON = mapper.writeValueAsString(headlineObjList);
		LOGGER.debug(">>>>>>>CommonService ::: prepareHeadlineData ::: headlineObjListResultJSON>>>>>>>>" + headlineObjListResultJSON);
		responseBuilder = Response.ok(headlineObjListResultJSON);
		LOGGER.info(">>>>>>>CommonService ::: Exiting prepareHeadlineData>>>>>>>>");
		return responseBuilder;

	}
	
	public ResponseBuilder prepareDurationData(Result result) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, IOException {

		LOGGER.info(">>>>>>>CommonService ::: Entering prepareDurationData>>>>>>>>");
		LOGGER.info(">>>>>>>CommonService ::: prepareDurationData ::: parameters>>>>>>>>" + result.toJSON());
		ResponseBuilder responseBuilder;
		ObjectMapper mapper = new ObjectMapper();

		Method fieldGetter = result.getClass().getMethod(GET_ROWS);
		List headlineListData = (List) fieldGetter.invoke(result);
		List<HeadlineObject> headlineObjList = new ArrayList<>();
		for (Object rowObj : headlineListData) {
			HeadlineObject headlineObj = new HeadlineObject();
			headlineObj.setHeadline(((List) rowObj).get(0).toString());
			headlineObj.setPublishDatetime(((List) rowObj).get(1).toString());
			headlineObjList.add(headlineObj);
		}
		String headlineObjListResultJSON = mapper.writeValueAsString(headlineObjList);
		LOGGER.debug(">>>>>>>CommonService ::: prepareDurationData ::: durationObjListResultJSON>>>>>>>>" + headlineObjListResultJSON);
		responseBuilder = Response.ok(headlineObjListResultJSON);
		LOGGER.info(">>>>>>>CommonService ::: Exiting prepareHeadlineData>>>>>>>>");
		return responseBuilder;

	}


	/**
	 * This service method is responsible for filtering the deep listening and virality table according to the given parameters 
	 * @param accountId,source,topic,startDt,endDt,virality,sentiment,detailsTabName
	 * @return Response
	 * @throws DataSourceException 
	 * @throws Exception
	 */
	@POST
	@Path("/getFilterAnalyticsDetails")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getFilterAnalyticsDetails(@FormParam("inAccountId") int accountId, @FormParam("inSource") int source, @FormParam("inTopic") String topic, @FormParam("start_dt") String startDt, @FormParam("end_dt") String endDt, @FormParam("sentiment") String sentiment, @FormParam("details_tab_name") String detailsTabName) throws DataSourceException{

		LOGGER.info(">>>>>>>AnalyticsService ::: Entering getFilterAnalyticsDetails>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		String spName = null;
		staticParameters = new HashMap<>();
		LOGGER.debug("detailsTabName = " + detailsTabName);
		if (WORD_CLOUD.trim().equalsIgnoreCase(detailsTabName)) {
			spName = SP_WORD_CLOUD_SOCIAL_POST_LIST;
		} else if (SOCIAL_LISTENING.equalsIgnoreCase(detailsTabName)) {
			spName = SP_SOCIAL_POST_FILTER_LIST;
		} 

		staticParameters.put(ATTRIBUTE_SP_NAME, spName);
		dynamicParameters = new HashMap<>();
		Integer acctId = accountId == -1 ? null : Integer.valueOf(accountId);
		Integer srcId = source == -1 ? null : Integer.valueOf(source);
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, acctId);
		dynamicParameters.put(SP_IN_PARAM_SOURCE, srcId);
		dynamicParameters.put(SP_IN_PARAM_TOPIC, ((topic == null) || (topic != null && topic.trim() == "") ? null : topic));
		dynamicParameters.put(SP_IN_PARAM_FROM_DT, ((startDt == null) || (startDt != null && startDt.trim() == "") ? null : startDt));
		dynamicParameters.put(SP_IN_PARAM_TO_DT, ((endDt == null) || (endDt != null && endDt.trim() == "") ? null : endDt));
		dynamicParameters.put(SP_IN_PARAM_SENTIMENT_TYPE, (sentiment == null) || ("".equalsIgnoreCase(sentiment.trim())) ? null : sentiment);
		JdbcAnalyticsDao jdbcAD = (JdbcAnalyticsDao) FactoryDao.getInstance(JDBC_ANALYTICS_DAO);
		Result result = jdbcAD.fetchFilteredAnalyticsKPIs(staticParameters, dynamicParameters);
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>AnalyticsService ::: Exiting getFilterAnalyticsDetails>>>>>>>>");
		return responseBuilder.build();

	}

	/**
	 * This service is invoked for default loading of Periodic Snapshot and when user changes the source too.
	 * @param selectedSourceId
	 * @return
	 * @throws DataSourceConnectionException 
	 * @throws Exception
	 */
	@POST
	@Path("/getPeriodicSnapshotData")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPeriodicSnapshotData(@FormParam("selectedSourceId") int selectedSourceId,@FormParam("inCurrentStartEndDt") 	String currentStartEndDt ,@FormParam("inPreviousStartEndDt") String previousStartEndDt ,@FormParam("accountId") int accountId) throws DataSourceConnectionException {
		LOGGER.info(">>>>>>>AnalyticsService ::: Entering getPeriodicSnapshotData>>>>>>>>"+ currentStartEndDt + selectedSourceId);
		JSONObject resultObj = new JSONObject();
	
		String[] inCurrentStartEndDtValue = currentStartEndDt.split(" - ");
		String[] inPreviousStartEndDtValue = previousStartEndDt.split(" - ");
		
		LOGGER.info("START DATE CURRENT PRINTING : " + inCurrentStartEndDtValue[0]);
		LOGGER.info("END DATE CURRENT PRINTING : " + inCurrentStartEndDtValue[1]);
		LOGGER.info("START DATE PREVIOUS PRINTING : " + inPreviousStartEndDtValue[0]);
		LOGGER.info("END DATE PREVIOUS PRINTING : " + inPreviousStartEndDtValue[1]);
		
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		dynamicParameters = new HashMap<>();
		staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_PERIODIC_SNAPSHOT"));
		dynamicParameters.put(SP_IN_PARAM_SOURCE_ID, selectedSourceId);
		dynamicParameters.put(SP_IN_PARAM_PERIODIC_START_FROM, inCurrentStartEndDtValue[0]+ START_TIME );
		dynamicParameters.put(SP_IN_PARAM_PERIODIC_START_TO, inCurrentStartEndDtValue[1]+END_TIME );
		dynamicParameters.put(SP_IN_PARAM_PERIODIC_END_FROM, inPreviousStartEndDtValue[0]+START_TIME);
		dynamicParameters.put(SP_IN_PARAM_PERIODIC_END_TO, inPreviousStartEndDtValue[1]+ END_TIME);
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
		
		JdbcAnalyticsDao jdbcAD = (JdbcAnalyticsDao) FactoryDao.getInstance(JDBC_ANALYTICS_DAO);
		Result result = jdbcAD.fetchPeriodicSnapshotData(staticParameters, dynamicParameters);
		LOGGER.info(">>>>>>>AnalyticService ::: getPeriodicSnapshotData ::: result >>>>>>>>" + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>AnalyticService ::: Exiting getFilterTopAdvocates>>>>>>>>");
		return responseBuilder.build();
		
	}

	
	@POST
	@Path("/getFilterAnalyticsDetailsClick")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getFilterAnalyticsDetailsClick(@FormParam("inAccountId") int accountId, @FormParam("inSource") int source, @FormParam("inTopic") String topic, @FormParam("start_dt") String startDt, @FormParam("end_dt") String endDt, @FormParam("sentiment") String sentiment, @FormParam("details_tab_name") String detailsTabName) throws DataSourceException{

		LOGGER.info(">>>>>>>AnalyticsService ::: Entering getFilterAnalyticsDetailsClick>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		String spName = null;
		staticParameters = new HashMap<>();
		dynamicParameters = new HashMap<>();
		LOGGER.debug("detailsTabName = " + detailsTabName);
		if (WORD_CLOUD.trim().equalsIgnoreCase(detailsTabName)) {
			spName = PropertyHandler.getPropertyValue("SP_WORD_CLOUD_ASPECT_CLICK");
			dynamicParameters.put("inIsAspectEntity", "aspect");
		} else if (SOCIAL_LISTENING.equalsIgnoreCase(detailsTabName)) {
			spName = PropertyHandler.getPropertyValue("SP_SOCIAL_POST_FILTER_LIST");
		}else if(detailsTabName.equalsIgnoreCase("Entity_click")) {
			spName = PropertyHandler.getPropertyValue("SP_WORD_CLOUD_ENTITY_CLICK");
			dynamicParameters.put("inIsAspectEntity", "entity");
		}
		staticParameters.put(ATTRIBUTE_SP_NAME, spName);
		Integer acctId = accountId == -1 ? null : Integer.valueOf(accountId);
		Integer srcId = source == -1 ? null : Integer.valueOf(source);
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, acctId);
		dynamicParameters.put(SP_IN_PARAM_SOURCE, srcId);
		dynamicParameters.put(SP_IN_PARAM_TOPIC, ((topic == null) || (topic != null && topic.trim() == "") ? null : topic));
		dynamicParameters.put(SP_IN_PARAM_FROM_DT, ((startDt == null) || (startDt != null && startDt.trim() == "") ? null : startDt));
		dynamicParameters.put(SP_IN_PARAM_TO_DT, ((endDt == null) || (endDt != null && endDt.trim() == "") ? null : endDt));
		dynamicParameters.put(SP_IN_PARAM_SENTIMENT_TYPE, (sentiment == null) || ("".equalsIgnoreCase(sentiment.trim())) ? null : sentiment);
		
		JdbcAnalyticsDao jdbcAD = (JdbcAnalyticsDao) FactoryDao.getInstance(JDBC_ANALYTICS_DAO);
		Result result = jdbcAD.fetchFilteredAnalyticsKPIs(staticParameters, dynamicParameters);
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>AnalyticsService ::: Exiting getFilterAnalyticsDetailsClick>>>>>>>>");
		return responseBuilder.build();

	}

	
	@POST
	@Path("/getSnapshotData")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSnapshotData(@FormParam("inAccountId") int accountId, @FormParam("inDuration") int duration) throws DataSourceException{

		LOGGER.info(">>>>>>>AnalyticsService ::: Entering getSnapshotData>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		String spName = null;
		staticParameters = new HashMap<>();
		spName = PropertyHandler.getPropertyValue("SP_GET_FEEDBACK_SNAPSHOT_DATA");
		LOGGER.debug(">>>>>>>SP Name :::: " + spName);
		staticParameters.put(ATTRIBUTE_SP_NAME, spName);
		dynamicParameters = new HashMap<>();
		Integer acctId = accountId == -1 ? null : Integer.valueOf(accountId);
		Integer snapDuration = duration == -1 ? null : Integer.valueOf(duration);
		//AS21112028
		dynamicParameters.put("inAccountId", 4);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_SELECTED_DURATION"), snapDuration);
		//eoc AS21112028
		JdbcAnalyticsDao jdbcAD = (JdbcAnalyticsDao) FactoryDao.getInstance(JDBC_ANALYTICS_DAO);
		Result result = jdbcAD.fetchSnapshotData(staticParameters, dynamicParameters);
		LOGGER.info("Result set JSON : " + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>AnalyticsService ::: Exiting getSnapshotData>>>>>>>>");
		return responseBuilder.build();

	}

	@POST
	@Path("/getTabMetaData")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTabMetaData(@FormParam("inAccountId") int accountId) throws DataSourceException{

		LOGGER.info(">>>>>>>AnalyticsService ::: Entering getTabMetaData>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_GET_TABS"));

		JdbcAnalyticsDao jdbcCD = (JdbcAnalyticsDao) FactoryDao.getInstance(JDBC_ANALYTICS_DAO);
		Result result = jdbcCD.fetchCompanyList(staticParameters, dynamicParameters);
		
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>AnalyticsService ::: Exiting getTabMetaData>>>>>>>>");
		return responseBuilder.build();

	}
	
	@GET
	@Path("/getPageAccessToken")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPageAccessToken(@QueryParam("accountId") int accountId) throws DataSourceException{

		LOGGER.info(">>>>>>>AnalyticsService ::: Entering getPageAccessToken>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		dynamicParameters = new HashMap<>();
		
		staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_GET_PAGE_TOKEN"));
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);

		JdbcAnalyticsDao jdbcCD = (JdbcAnalyticsDao) FactoryDao.getInstance(JDBC_ANALYTICS_DAO);
		Result result = jdbcCD.fetchPageToken(staticParameters, dynamicParameters);
		
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>AnalyticsService ::: Exiting getPageAccessToken>>>>>>>>");
		return responseBuilder.build();

	}
	
	@POST
	@Path("/getKpiMetaData")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getKpiMetaData(@FormParam("inTabId") int inTabId) throws DataSourceException{

		LOGGER.info(">>>>>>>AnalyticsService ::: Entering getKpiMetaData>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		dynamicParameters = new HashMap<>();
		
		dynamicParameters.put("inTabId", inTabId);
		staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_GET_KPIS"));

		JdbcAnalyticsDao jdbcCD = (JdbcAnalyticsDao) FactoryDao.getInstance(JDBC_ANALYTICS_DAO);
		Result result = jdbcCD.fetchKpiList(staticParameters, dynamicParameters);
		
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>AnalyticsService ::: Exiting getKpiMetaData>>>>>>>>");
		return responseBuilder.build();

	}
	
	/**
	 * This service method is responsible for filtering the entity table according to the given parameters
	 * @param accountId,source,topic,startDt,endDt,virality,sentiment
	 * @return Response
	 * @throws DataSourceException 
	 * @throws SQLException 
	 * @throws Exception
	 */
	@POST
	@Path("/getFilterEntityDetails")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getFilterEntityDetails(@FormParam("inAccountId") int accountId, @FormParam("inSource") int source, @FormParam("inTopic") String topic, @FormParam("start_dt") String startDt, @FormParam("end_dt") String endDt, @FormParam("virality") String virality, @FormParam("sentiment") String sentiment) throws DataSourceException {

		LOGGER.info(">>>>>>>AnalyticsService ::: Entering getEntityDetails>>>>>>>>");

		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_ENTITY_FILTER_DATA"));
		dynamicParameters = new HashMap<>();
		Integer acctId = accountId == -1 ? null : Integer.valueOf(accountId);
		Integer srcId = source == -1 ? null : Integer.valueOf(source);
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, acctId);
		dynamicParameters.put(SP_IN_PARAM_SOURCE, srcId);
		dynamicParameters.put(SP_IN_PARAM_TOPIC, ((topic == null) || (topic.trim() == "") ? null : topic));
		dynamicParameters.put(SP_IN_PARAM_FROM_DT, ((startDt == null) || (startDt.trim() == "") ? null : startDt));
		dynamicParameters.put(SP_IN_PARAM_TO_DT, ((endDt == null) || (endDt.trim() == "") ? null : endDt));
		dynamicParameters.put(SP_IN_PARAM_SENTIMENT_TYPE, ((sentiment == null) || ("".equalsIgnoreCase(sentiment.trim())) ? null : sentiment));
		dynamicParameters.put(SP_IN_PARAM_SELECTED_DURATION, null);
		JdbcAnalyticsDao jdbcAD = (JdbcAnalyticsDao) FactoryDao.getInstance(JDBC_ANALYTICS_DAO);
		Result result = jdbcAD.fetchAnalyticsKPIs(staticParameters, dynamicParameters);
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>AnalyticsService ::: Exiting getEntityDetails>>>>>>>>");
		return responseBuilder.build();

	}
	
	
	/**
	 * 
	 * @throws DataSourceException 
	 * @Author Name: Ashis Nayak
	 * @Create Date: 10-31-2017
	 * @Last Modified: Ashis Nayak: 10-31-2017
	 * @Input: accountId, type, filterValue
	 * @Output: Response
	 * @Method Insight: this method calls the SPs for Overall Sentimental Distribution when filtered with the dropdown options
	 * @throws Exception
	 */
	@POST
	@Path("/getFilterOverallSentimentDistribution")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getFilterOverallSentimentDistribution(@FormParam("accountId") int accountId, @FormParam("sourceId") int source, @FormParam("topic") String topic, @FormParam("city") String city, @FormParam("startDate") String startDate, @FormParam("endDate") String endDate) throws DataSourceException {

		LOGGER.info(">>>>>>>DashboardService ::: Entering getFilterOverallSentimentDistribution>>>>>>>>");
		LOGGER.info(">>>>>>>DashboardService ::: getFilterOverallSentimentDistribution parameters>>>>>>>> accountId: " + accountId + " source: " + source + " topic: " + topic + " city: " + city  + " startDate: " + startDate + " endDate: " + endDate);
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		dynamicParameters = new HashMap<>();

		staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_OVERALL_SENTIMENT_DONUT_FILTER_DATA"));

		Integer acctId = accountId == -1 ? null : Integer.valueOf(accountId);
		Integer srcId = source == -1 ? null : Integer.valueOf(source);
		String aspectId = null;
		if(!topic.equalsIgnoreCase("ALL")){
			aspectId = topic;
		}
		
		String[] paramStartEndDateCurrent = endDate.split(" - ");
		String[] paramStartEndDatePrevious = startDate.split(" - ");
		
		LOGGER.info("Print splited start end date : " + paramStartEndDateCurrent[0] + " - "+ paramStartEndDateCurrent[1]);
		
		LOGGER.info(">>>>>>>DashboardService ::: getFilterOverallSentimentDistribution ::: calculated dynamicParameters>>>>>>>> acctId: " + acctId + " srcId: " + srcId);

		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, acctId);
		dynamicParameters.put(SP_IN_PARAM_SOURCE, srcId);
		dynamicParameters.put(SP_IN_PARAM_TOPIC, aspectId);
		dynamicParameters.put(SP_IN_PARAM_CITY, "".equalsIgnoreCase(city.trim()) ? null : city);
		dynamicParameters.put(SP_IN_PARAM_FROM_DT_CURRENT, paramStartEndDateCurrent[0]+START_TIME);
		dynamicParameters.put(SP_IN_PARAM_TO_DT_CURRENT, paramStartEndDateCurrent[1]+END_TIME);
		dynamicParameters.put(SP_IN_PARAM_FROM_DT_PREVIOUS, paramStartEndDatePrevious[0]+START_TIME);
		dynamicParameters.put(SP_IN_PARAM_TO_DT_PREVIOUS,  paramStartEndDatePrevious[1]+END_TIME);

		JdbcDashboardDao jdbcDD = (JdbcDashboardDao) FactoryDao.getInstance(JDBC_DASHBOARD_DAO);
		Result result = jdbcDD.fetchFilteredOverallSentimentDistribution(staticParameters, dynamicParameters);
		LOGGER.info(">>>>>>>DashboardService ::: getFilterOverallSentimentDistribution ::: result from fetchFilteredOverallSentimentDistribution>>>>>>>>" + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>DashboardService ::: Exiting getFilterOverallSentimentDistribution>>>>>>>>"+resultObj.toJSONString());
		return responseBuilder.build();

	}

	/**
	 * 
	 * @throws DataSourceException 
	 * @Author Name: Ashis Nayak
	 * @Create Date: 10-31-2017
	 * @Last Modified: Ashis Nayak: 10-31-2017
	 * @Input: accountId, type, filterValue
	 * @Output: Response
	 * @Method Insight: this method calls the SPs for Overall Sentimental Distribution when filtered with the dropdown options
	 * @throws Exception
	 */
	@POST
	@Path("/getBrandComparison24Hour")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getBrandComparison24Hour(@FormParam("account_Id") int accountId, @FormParam("current_Time") String currentTime, @FormParam("selectedInterval") int selectedInterval) throws DataSourceException {

		LOGGER.info(">>>>>>>DashboardService ::: Entering getBrandComparison24Hour>>>>>>>>");
		LOGGER.info(">>>>>>>DashboardService ::: getTopAdvocates ::: In Parameters >>>>>>>> accountId :" + accountId + "current_Time : " + currentTime + "selectedInterval : " + selectedInterval);
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_BRAND_COMPARISON_24HR"));
		dynamicParameters = new HashMap<>();
		if ("".equalsIgnoreCase(currentTime)) {
			currentTime = null;
		}
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_CURR_TIME"), currentTime);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_SELECTED_DURATION"), selectedInterval);

		JdbcDashboardDao jdbcDD = (JdbcDashboardDao) FactoryDao.getInstance(PropertyHandler.getPropertyValue("JDBC_DASHBOARD_DAO"));
		Result result = jdbcDD.fetchHappinessIndexLive(staticParameters, dynamicParameters);
		LOGGER.info(">>>>>>>DashboardService ::: getBrandComparison24Hour ::: result >>>>>>>>" + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>DashboardService ::: Exiting getBrandComparison24Hour>>>>>>>>");
		return responseBuilder.build();

	}

	
	@POST
	@Path("/getPostTweetWiseEngagementScore")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPostTweetWiseEngagementScore(@FormParam("inAccountId") int accountId,@FormParam("inDurationTF") int inDurationTF,@FormParam("inSourceTF") int inSourceTF, @FormParam("perPageEntries") int perPageEntries, @FormParam("newPage") int pageIndex, @FormParam("sortBy") int inSortBy) throws DataSourceException, IllegalAccessException, InvocationTargetException, NoSuchMethodException, IOException {

		LOGGER.info(">>>>>>>AnalyticsService ::: Entering getPostTweetWiseEngagementScore>>>>>>>>");
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_TWITTER_FB_ENGAGEMENT_SENTIMENT"));
		dynamicParameters = new HashMap<>();
		Integer acctId = accountId == -1 ? null : Integer.valueOf(accountId);
		Integer sourceId = inSourceTF ;
		Integer selectedDuration = inDurationTF;
		int dataOffset = 0;
		dataOffset = (pageIndex - 1) * perPageEntries;
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, acctId);
		dynamicParameters.put(SP_IN_PARAM_SOURCE, sourceId);
		dynamicParameters.put(SP_IN_PARAM_SELECTED_DURATION,selectedDuration);
		dynamicParameters.put(SP_IN_PARAM_OFFSET, dataOffset);
		dynamicParameters.put(SP_IN_PARAM_LIMIT, perPageEntries);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_SORTBY"), inSortBy);

		JdbcAnalyticsDao jdbcAD = (JdbcAnalyticsDao) FactoryDao.getInstance(JDBC_ANALYTICS_DAO);
		Result result = jdbcAD.getPostTweetWiseEngagementScore(staticParameters, dynamicParameters);
		responseBuilder = preparePostTweetWiseEngagementScoreData(result);
		LOGGER.info(">>>>>>>AnalyticsService ::: getPostTweetWiseEngagementScore :: responseBuilder>>>>>>>>" + responseBuilder);
		LOGGER.info(">>>>>>>AnalyticsService ::: Exiting getContextDetails>>>>>>>>");
		return responseBuilder.build();

	}
	
	private ResponseBuilder preparePostTweetWiseEngagementScoreData(Result result) throws IOException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		LOGGER.info(">>>>>>>AnalyticsService ::: Entering preparePostTweetWiseEngagementScoreData>>>>>>>>");
		ResponseBuilder responseBuilder;
		ObjectMapper mapper = new ObjectMapper();
		Method fieldGetter = result.getClass().getMethod(GET_ROWS);
		Method fg = result.getClass().getMethod(GET_COUNT);
		int countValue = (int) fg.invoke(result);
		HashMap<String, Object> responseMap = new HashMap<>();
		responseMap.put(COUNT, countValue);
		LOGGER.info("Value of Count for post wise score : " + countValue);
		List entityListData = (List) fieldGetter.invoke(result);
		List<PostWiseEngagementSentimentObject> entityObjList = new ArrayList<>();
		for (Object rowObj : entityListData) {

			PostWiseEngagementSentimentObject entityObj = new PostWiseEngagementSentimentObject();
			entityObj.setPost_id(((List) rowObj).get(0).toString());
			entityObj.setPost_text(((List) rowObj).get(1).toString());
			entityObj.setPost_datetime(((List) rowObj).get(2).toString());
			entityObj.setEngagement_score(Double.parseDouble(((List) rowObj).get(3).toString()));
			entityObj.setPosetive(Double.parseDouble(((List) rowObj).get(4).toString()));
			entityObj.setNegative(Double.parseDouble(((List) rowObj).get(5).toString()));
			entityObj.setNeutral(Double.parseDouble(((List) rowObj).get(6).toString()));
			entityObj.setPost_url(((List) rowObj).get(7).toString());
			
			LOGGER.info(entityObj.getPost_text() +"---||||||||||--"+ entityObj.getPost_datetime());
			
			
			
			entityObjList.add(entityObj);
		}
		responseMap.put(OUTPUT, entityObjList);
		String viralityResultJson = mapper.writeValueAsString(responseMap);
		LOGGER.debug(">>>>>>>>>>>>" + viralityResultJson);
		responseBuilder = Response.ok(viralityResultJson);
		LOGGER.info(">>>>>>>AnalyticsService ::: preparePostTweetWiseEngagementScoreData :: >>>>>>>>" + responseBuilder.toString());
		LOGGER.info(">>>>>>>AnalyticsService ::: Exiting preparePostTweetWiseEngagementScoreData>>>>>>>>");
		return responseBuilder;
	}

	//"originalSentiment": originalSentiment,
	//"postText

	@POST
	@Path("/overallSentimentUpdater")
	public int overallSentimentUpdater(@FormParam("company_Id") int companyID,@FormParam("sentimentID") String sentimentID, @FormParam("postID") String postID,@FormParam("originalSentiment") String originalSentiment, @FormParam("postText") String postText,@FormParam("tabName") String tabName) throws DataSourceException {

		LOGGER.info(">>>>>>>AnalyticsService ::: Entering overallSentimentUpdater>>>>>>>>");
		staticParameters = new HashMap<>();
		dynamicParameters = new HashMap<>();
		
		String overAllSentiment = "Neutral";
		if(sentimentID.equalsIgnoreCase("1")){
			overAllSentiment = "Positive";
		}else if(sentimentID.equalsIgnoreCase("-1")){
			overAllSentiment = "Negative";
		}
		
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_POST_ID"), postID);
		dynamicParameters.put(SP_IN_PARAM_SENTIMENT_TYPE, overAllSentiment);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_COMPANY"),companyID);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_SENTIMENT_NUMBER"),sentimentID);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_ORIGINAL_SENTIMENT"),originalSentiment);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_POST_TEXT"),postText);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_TAB_NAME"),tabName);
		staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_INSERT_CHANGED_OVERALL_SENTIMENT"));

		JdbcAnalyticsDao jdbcCD = (JdbcAnalyticsDao) FactoryDao.getInstance(JDBC_ANALYTICS_DAO);
		int updateStatus = jdbcCD.overallSentimentUpdater(staticParameters, dynamicParameters);
		
		LOGGER.info(">>>>>>>AnalyticsService ::: Exiting overallSentimentUpdater>>>>>>>>"+ updateStatus);
		return updateStatus;

	}

	@POST
	@Path("/getAnalyticsDetails")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAnalyticsDetails(@FormParam("inAccountId") int accountId, @FormParam("inSource") int source, @FormParam("inTopic") String topic, @FormParam("start_dt") String startDt, @FormParam("end_dt") String endDt, @FormParam("sentiment") String sentiment, @FormParam("details_tab_name") String detailsTabName) throws DataSourceException {

		LOGGER.info(">>>>>>>AnalyticsService ::: Entering getAnalyticsDetails>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		String spName = null;
		staticParameters = new HashMap<>();
		if (PropertyHandler.getPropertyValue("SENTIMENT_ANALYSIS").equalsIgnoreCase(detailsTabName)) {
			spName = PropertyHandler.getPropertyValue("SP_SOURCE_DATE_SENTIMENT_ANALYSIS");
		} else if (SOCIAL_LISTENING.equalsIgnoreCase(detailsTabName)) {
			spName = SP_SOCIAL_POST_FILTER_LIST;
		} else if (WORD_CLOUD.trim().equalsIgnoreCase(detailsTabName)) {
			spName = SP_WORD_CLOUD_SOCIAL_POST_LIST;
		}
		staticParameters.put(ATTRIBUTE_SP_NAME, spName);
		dynamicParameters = new HashMap<>();
		Integer sourceId = null;
		if (source != -1) {
			sourceId = Integer.valueOf(source);
		}
		if (topic != null && "".equalsIgnoreCase(topic.trim())) {
			topic = null;
		}
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
		dynamicParameters.put(SP_IN_PARAM_SOURCE, sourceId);
		dynamicParameters.put(SP_IN_PARAM_TOPIC, topic);
		dynamicParameters.put(SP_IN_PARAM_FROM_DT, startDt);
		dynamicParameters.put(SP_IN_PARAM_TO_DT, endDt);
		dynamicParameters.put(SP_IN_PARAM_SENTIMENT_TYPE, null);

		JdbcAnalyticsDao jdbcAD = (JdbcAnalyticsDao) FactoryDao.getInstance(JDBC_ANALYTICS_DAO);
		Result result = jdbcAD.fetchAnalyticsKPIs(staticParameters, dynamicParameters);
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>AnalyticsService ::: Exiting getAnalyticsDetails>>>>>>>>");
		return responseBuilder.build();

	}

	@POST
	@Path("/getEntityDetails")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getEntityDetails(@FormParam("inAccountId") int accountId, @FormParam("inSource") int source, @FormParam("inTopic") String topic, @FormParam("start_dt") String startDt, @FormParam("end_dt") String endDt, @FormParam("time_interval") int timeInterval, @FormParam("selectedInterval") String selectedInterval) throws DataSourceException {

		LOGGER.info(">>>>>>>AnalyticsService ::: Entering getEntityDetails>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_ENTITY_FILTER_DATA"));
		dynamicParameters = new HashMap<>();
		Integer acctId = accountId == -1 ? null : Integer.valueOf(accountId);
		Integer sourceId = source == -1 ? null : Integer.valueOf(source);
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, acctId);
		dynamicParameters.put(SP_IN_PARAM_SOURCE, sourceId);
		dynamicParameters.put(SP_IN_PARAM_TOPIC, ((topic == null) || (topic != null && topic.trim() == "") ? null : topic));
		dynamicParameters.put(SP_IN_PARAM_FROM_DT, ((startDt == null) || (startDt != null && startDt.trim() == "") ? null : startDt));
		dynamicParameters.put(SP_IN_PARAM_TO_DT, ((endDt == null) || (endDt != null && endDt.trim() == "") ? null : endDt));
		dynamicParameters.put(SP_IN_PARAM_SENTIMENT_TYPE, null);
		dynamicParameters.put(SP_IN_PARAM_SELECTED_DURATION, ((selectedInterval == null) || (selectedInterval.trim() == "") ? null : selectedInterval));

		JdbcAnalyticsDao jdbcAD = (JdbcAnalyticsDao) FactoryDao.getInstance(JDBC_ANALYTICS_DAO);
		Result result = jdbcAD.fetchAnalyticsKPIs(staticParameters, dynamicParameters);
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>AnalyticsService ::: Exiting getEntityDetails>>>>>>>>");
		return responseBuilder.build();

	}

	/**
	 * This service method is responsible for retrieving all the sources and topics.
	 * @param companyId
	 * @param type
	 * @return Response
	 * @throws DataSourceException 
	 * @throws Exception
	 */
	@GET
	@Path("/getSourcesTopics")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSources(@QueryParam("accountId") int accountId, @QueryParam("type") String type) throws DataSourceException{
		
		LOGGER.info(">>>>>>>CommonService ::: Entering getSources>>>>>>>>" + type);
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		if ("sources".equalsIgnoreCase(type)) {
			staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_GET_SOURCES"));
			dynamicParameters = new HashMap<>();
			dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
		} else if ("topics".equalsIgnoreCase(type)) {
			staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_GET_TOPICS"));
			dynamicParameters = new HashMap<>();
			dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
		} else if ("feedbackcategory".equalsIgnoreCase(type)) {
			staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_GET_FEEDBACK_TOPICS"));
			dynamicParameters = new HashMap<>();
			dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
		} else if ("feedbackquestions".equalsIgnoreCase(type)) {
			staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_GET_FEEDBACK_QUESTIONS"));
			dynamicParameters = new HashMap<>();
			dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
		} else if ("feedbacktext".equalsIgnoreCase(type)) {
			staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_GET_FEEDBACK_TEXT"));
			dynamicParameters = new HashMap<>();
			dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
		} else if ("citiesZomato".equalsIgnoreCase(type)) {
			staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_GET_CITIES_ZOMATO"));
		} else if ("citiesTwitter".equalsIgnoreCase(type)) {
			staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_GET_CITIES_TWITTER"));
		} else if ("regionsZomato".equalsIgnoreCase(type)) {
			staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_GET_REGIONS_ZOMATO"));
		} else if ("domainDisplayName".equalsIgnoreCase(type)) {
			staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_GET_DOMAIN_NAME"));
			dynamicParameters = new HashMap<>();
			dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
		} else if ("ratingCities".equalsIgnoreCase(type)) {
			staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_GET_CITIES_RATING"));
		} else if ("topRatingSources".equalsIgnoreCase(type)) {
			staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_GET_SOURCES_RATING"));
		} else if ("reviewsources".equalsIgnoreCase(type)) {
			staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_GET_SOURCES_REVIEW"));
		}
			
		JdbcAnalyticsDao jdbcCD = (JdbcAnalyticsDao) FactoryDao.getInstance(JDBC_ANALYTICS_DAO);
		Result result = jdbcCD.fetchSourcesOrContexts(staticParameters, dynamicParameters);
		LOGGER.info(">>>>>>>CommonService ::: getSources ::: result from fetchSourcesOrContexts>>>>>>>>" + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>CommonService ::: Exiting getSources>>>>>>>>");
		return responseBuilder.build();
	}
	
	/**
	 * 
	 * @Author Name: Urmi Saha
	 * @Create Date: 27-12-2016
	 * @Last Modified: Urmi Saha: 27-12-2016
	 * @Input: accountId, sourceId, startDate, endDate
	 * @Output: Response
	 * @Method Insight: this method calls the SP for Top Advocates when filtered with the dropdown options
	 * @throws DataSourceException 
	 */
	@POST
	@Path("/getFilterTopAdvocates")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getFilterTopAdvocates(@FormParam("accountId") int accountId, @FormParam("sourceId") int source, @FormParam("startDate") String startDate, @FormParam("endDate") String endDate) throws DataSourceException {

		LOGGER.info(">>>>>>>DashboardService ::: Entering getFilterTopAdvocates>>>>>>>>");
		LOGGER.info(">>>>>>>DashboardService ::: getFilterTopAdvocates parameters>>>>>>>> accountId: " + accountId + " source: " + source + " startDate: " + startDate + " endDate: " + endDate);
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_FILTER_TOP_ADVOCATES"));
		dynamicParameters = new HashMap<>();
		Integer srcId = source == -1 ? null : Integer.valueOf(source);
		if ("".equalsIgnoreCase(startDate)) {
			startDate = null;
		}
		if ("".equalsIgnoreCase(endDate)) {
			endDate = null;
		}
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
		dynamicParameters.put(SP_IN_PARAM_SOURCE, srcId);
		dynamicParameters.put(SP_IN_PARAM_FROM_DT, startDate);
		dynamicParameters.put(SP_IN_PARAM_TO_DT, endDate);

		JdbcDashboardDao jdbcDD = (JdbcDashboardDao) FactoryDao.getInstance(PropertyHandler.getPropertyValue("JDBC_DASHBOARD_DAO"));
		Result result = jdbcDD.fetchFilteredTopAdvocates(staticParameters, dynamicParameters);
		LOGGER.info(">>>>>>>DashboardService ::: getFilterTopAdvocates ::: result >>>>>>>>" + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>DashboardService ::: Exiting getFilterTopAdvocates>>>>>>>>");
		return responseBuilder.build();

	}


	/**
	 * This service is invoked for default loading of Word Cloud containing Aspects & top entities.
	 * @param accountId
	 * @return
	 * @throws DataSourceException 
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonGenerationException 
	 * @throws Exception
	 */
	@POST
	@Path("/getWordCloudData")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getWordCloudData(@FormParam("accountId") int accountId, @FormParam("selectedContext") String selectedContext, @FormParam("sourceId") int sourceId, @FormParam("startDate") String startDate, @FormParam("endDate") String endDate, @FormParam("sentiment") String sentiment) throws DataSourceException,IOException{
		LOGGER.info(">>>>>>>AnalyticsService ::: Entering getWordCloudData>>>>>>>>");
		ResponseBuilder responseBuilder;
		List<Result> resultListObj = new ArrayList<>();
		ObjectMapper mapper = new ObjectMapper();
		Result aspectResultObj1 = fetchWordCloudData(accountId, ASPECTS_FOR_WORD_CLOUD, null, sourceId, startDate, endDate, sentiment);
		Result aspectResultObj2 = fetchWordCloudData(accountId, ENTITIES_FOR_WORD_CLOUD, null, sourceId, startDate, endDate, sentiment);
		resultListObj.add(aspectResultObj1);
		resultListObj.add(aspectResultObj2);
		String wordCloudJson = mapper.writeValueAsString(resultListObj);
		responseBuilder = Response.ok(wordCloudJson);
		LOGGER.info(">>>>>>>AnalyticsService ::: Exiting getWordCloudData>>>>>>>>");
		return responseBuilder.build();
	}

	/**
	 * This service is invoked for retrieving related top entities for selected Aspects. 
	 * @param accountId
	 * @param selectedContext
	 * @return
	 * @throws DataSourceException 
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonGenerationException 
	 * @throws Exception
	 */
	@POST
	@Path("/getEntitiesForSelectedContext")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getEntitiesForSelectedContext(@FormParam("accountId") int accountId, @FormParam("selectedContext") String selectedContext, @FormParam("sourceId") int sourceId, @FormParam("startDate") String startDate, @FormParam("endDate") String endDate, @FormParam("sentiment") String sentiment) throws DataSourceException, IOException{
		LOGGER.info(">>>>>>>AnalyticsService ::: Entering getEntitiesForSelectedContext>>>>>>>>");
		ResponseBuilder responseBuilder;
		List<Result> resultListObj = new ArrayList<>();
		ObjectMapper mapper = new ObjectMapper();
		Result entitiesResultObj = fetchWordCloudData(accountId, ENTITIES_FOR_WORD_CLOUD, selectedContext, sourceId, startDate, endDate, sentiment);
		resultListObj.add(entitiesResultObj);
		String entitiesWordCloudJson = mapper.writeValueAsString(resultListObj);
		responseBuilder = Response.ok(entitiesWordCloudJson);
		LOGGER.info(">>>>>>>AnalyticsService ::: Exiting getEntitiesForSelectedContext>>>>>>>>");
		return responseBuilder.build();
	}
	@POST
	@Path("/getEntitiesForSelectedContextClick")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getEntitiesForSelectedContextClick(@FormParam("accountId") int accountId, @FormParam("selectedContext") String selectedContext, @FormParam("sourceId") int sourceId, @FormParam("startDate") String startDate, @FormParam("endDate") String endDate, @FormParam("sentiment") String sentiment) throws IOException, DataSourceException{
		LOGGER.info(">>>>>>>AnalyticsService ::: Entering getEntitiesForSelectedContext>>>>>>>>");
		ResponseBuilder responseBuilder;
		List<Result> resultListObj = new ArrayList<>();
		ObjectMapper mapper = new ObjectMapper();
		Result entitiesResultObj = fetchWordCloudDataClick(accountId, ENTITIES_FOR_WORD_CLOUD, selectedContext, sourceId, startDate, endDate, sentiment);
		resultListObj.add(entitiesResultObj);
		String entitiesWordCloudJson = mapper.writeValueAsString(resultListObj);
		responseBuilder = Response.ok(entitiesWordCloudJson);
		LOGGER.info(">>>>>>>AnalyticsService ::: Exiting getEntitiesForSelectedContext>>>>>>>>");
		return responseBuilder.build();
	}
	/**
	 * This service is invoked when user filters the Explore Page.
	 * @param accountId
	 * @return
	 * @throws DataSourceException 
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonGenerationException 
	 * @throws Exception
	 */
	@POST
	@Path("/getFilteredWordCloudData")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getFilteredWordCloudData(@FormParam("accountId") int accountId, @FormParam("selectedContext") String selectedContext, @FormParam("sourceId") int sourceId, @FormParam("startDate") String startDate, @FormParam("endDate") String endDate, @FormParam("sentiment") String sentiment) throws DataSourceException, IOException{
		LOGGER.info(">>>>>>>AnalyticsService ::: Entering getWordCloudData>>>>>>>>");
		ResponseBuilder responseBuilder;
		List<Result> resultListObj = new ArrayList<>();
		ObjectMapper mapper = new ObjectMapper();
		Result filteredResultObj1 = null;
		Result filteredResultObj2 = fetchWordCloudData(accountId, ENTITIES_FOR_WORD_CLOUD, selectedContext, sourceId, startDate, endDate, sentiment);
		if (BLANK_STRING.equalsIgnoreCase(selectedContext) || selectedContext == null) {
			filteredResultObj1 = fetchWordCloudData(accountId, ASPECTS_FOR_WORD_CLOUD, selectedContext, sourceId, startDate, endDate, sentiment);
		}
		if (filteredResultObj1 != null) {
			resultListObj.add(filteredResultObj1);
		}
		resultListObj.add(filteredResultObj2);
		String wordCloudJson = mapper.writeValueAsString(resultListObj);
		responseBuilder = Response.ok(wordCloudJson);
		LOGGER.info(">>>>>>>AnalyticsService ::: Exiting getWordCloudData>>>>>>>>");
		return responseBuilder.build();
	}

	public Result fetchWordCloudData(int accountId, String aspectOrEntityFlag, String selectedContext, int sourceId, String startDt, String endDt, String sentiment) throws DataSourceException {
		LOGGER.info(">>>>>>>AnalyticsService ::: Entering fetchWordCloudData>>>>>>>>");
		staticParameters = new HashMap<>();
		if (aspectOrEntityFlag.equalsIgnoreCase(PropertyHandler.getPropertyValue("ASPECTS_FOR_WORD_CLOUD"))) {
			staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_WORD_CLOUD_ASPECT"));
		} else {
			staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_WORD_CLOUD_ENTITY"));
		}
		dynamicParameters = new HashMap<>();
		Integer sourceID = sourceId == -1 ? null : Integer.valueOf(sourceId);
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
		dynamicParameters.put(SP_IN_PARAM_SOURCE, sourceID);
		dynamicParameters.put(SP_IN_PARAM_FROM_DT, ((startDt == null) || (startDt.trim() == "") ? null : startDt));
		dynamicParameters.put(SP_IN_PARAM_TO_DT, ((endDt == null) || (endDt.trim() == "") ? null : endDt));
		dynamicParameters.put(SP_IN_PARAM_SENTIMENT_TYPE, ((sentiment == null) || ("".equalsIgnoreCase(sentiment.trim())) ? null : sentiment));
		if (aspectOrEntityFlag.equalsIgnoreCase(ENTITIES_FOR_WORD_CLOUD)) {
			dynamicParameters.put(SP_IN_PARAM_TOPIC, ((selectedContext == null) || ("".equalsIgnoreCase(selectedContext.trim())) ? null : selectedContext));
		}
		JdbcAnalyticsDao jdbcAD = (JdbcAnalyticsDao) FactoryDao.getInstance(JDBC_ANALYTICS_DAO);
		Result result = jdbcAD.fetchAnalyticsKPIs(staticParameters, dynamicParameters);
		LOGGER.info(">>>>>>>AnalyticsService ::: Exiting fetchWordCloudData>>>>>>>>");
		return result;
	}
	
	public Result fetchWordCloudDataClick(int accountId, String aspectOrEntityFlag, String selectedContext, int sourceId, String startDt, String endDt, String sentiment) throws DataSourceException{
		LOGGER.info(">>>>>>>AnalyticsService ::: Entering fetchWordCloudData>>>>>>>>");
		staticParameters = new HashMap<>();
		if (aspectOrEntityFlag.equalsIgnoreCase(PropertyHandler.getPropertyValue("ASPECTS_FOR_WORD_CLOUD"))) {
			staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_WORD_CLOUD_ASPECT"));
		} else {
			staticParameters.put(ATTRIBUTE_SP_NAME, "sp_wordcloud_entity_click");
		}
		dynamicParameters = new HashMap<>();
		Integer sourceID = sourceId == -1 ? null : Integer.valueOf(sourceId);
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
		dynamicParameters.put(SP_IN_PARAM_SOURCE, sourceID);
		dynamicParameters.put(SP_IN_PARAM_FROM_DT, ((startDt == null) || (startDt.trim() == "") ? null : startDt));
		dynamicParameters.put(SP_IN_PARAM_TO_DT, ((endDt == null) || (endDt.trim() == "") ? null : endDt));
		dynamicParameters.put(SP_IN_PARAM_SENTIMENT_TYPE, ((sentiment == null) || ("".equalsIgnoreCase(sentiment.trim())) ? null : sentiment));
		if (aspectOrEntityFlag.equalsIgnoreCase(ENTITIES_FOR_WORD_CLOUD)) {
			dynamicParameters.put(SP_IN_PARAM_TOPIC, ((selectedContext == null) || ("".equalsIgnoreCase(selectedContext.trim())) ? null : selectedContext));
		}
		JdbcAnalyticsDao jdbcAD = (JdbcAnalyticsDao) FactoryDao.getInstance(JDBC_ANALYTICS_DAO);
		Result result = jdbcAD.fetchAnalyticsKPIs(staticParameters, dynamicParameters);
		LOGGER.info(">>>>>>>AnalyticsService ::: Exiting fetchWordCloudData>>>>>>>>");
		return result;
	}

	@POST
	@Path("/getAnalyticsDataOnLoad")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAnalyticsDataOnLoad(@FormParam("accountId") int accountId, @FormParam("sectionName") String sectionName) throws DataSourceException {
		LOGGER.info(">>>>>>>AnalyticsService ::: Entering getAnalyticsDataOnLoad>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		String spName = null;
		switch (sectionName) {
			case "context_details":
				spName = PropertyHandler.getPropertyValue("SP_SOURCE_ENTITY_DETAILS_ONLOAD");
				break;
			case "social_listening":
				spName = PropertyHandler.getPropertyValue("SP_SOCIAL_POST_LIST_ONLOAD");
				break;
			case "word_cloud":
				spName = PropertyHandler.getPropertyValue("SP_WORD_CLOUD_SOCIAL_POST_LIST_ONLOAD");
				break;
			default:
				throw new IllegalArgumentException("Invalid sectionName: " + sectionName);
		}
		staticParameters.put(ATTRIBUTE_SP_NAME, spName);
		dynamicParameters = new HashMap<>();
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
		JdbcAnalyticsDao jdbcAD = (JdbcAnalyticsDao) FactoryDao.getInstance(JDBC_ANALYTICS_DAO);
		Result result = jdbcAD.getProcedureResultSet(staticParameters, dynamicParameters);
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>AnalyticsService ::: Exiting getAnalyticsDataOnLoad>>>>>>>>");
		return responseBuilder.build();
	}

	/**
	 * This service is called in case of MCI for populating word cloud entities & aspects during onLoad of Explore Page.
	 * @param accountId
	 * @return
	 * @throws DataSourceException 
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonGenerationException 
	 * @throws Exception
	 */
	@POST
	@Path("/getWordCloudDataOnLoad")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getWordCloudDataOnLoad(@FormParam("accountId") int accountId) throws DataSourceException, IOException {
		LOGGER.info(">>>>>>>AnalyticsService ::: Entering getWordCloudDataOnLoad>>>>>>>>");
		ResponseBuilder responseBuilder;
		List<Result> resultListObj = new ArrayList<>();
		ObjectMapper mapper = new ObjectMapper();
		Result filteredResultObj1 = fetchAspectsOnLoadForWordCloud(accountId);
		Result filteredResultObj2 = fetchEntitiesOnLoadForWordCloud(accountId);
		resultListObj.add(filteredResultObj1);
		resultListObj.add(filteredResultObj2);
		String wordCloudJson = mapper.writeValueAsString(resultListObj);
		responseBuilder = Response.ok(wordCloudJson);
		LOGGER.info(">>>>>>>AnalyticsService ::: Exiting getWordCloudDataOnLoad>>>>>>>>");
		return responseBuilder.build();
	}
	@POST
	@Path("/getDeepListeningfilter")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getFilterAnalyticsDetailsFilter(@FormParam("inAccountId") int accountId, @FormParam("inSearchKeyword") String searchKeyword, @FormParam("inSource") int source, @FormParam("inSourceCategory") String inSourceCategory, @FormParam("inTopic") String topics, @FormParam("inEntity") String entity, @FormParam("start_dt") String startDt, @FormParam("end_dt") String endDt, @FormParam("sentiment") int sentiment, @FormParam("city") String city, @FormParam("details_tab_name") String detailsTabName, @FormParam("interval") int interval, @FormParam("perPageEntries") Integer perPageEntries, @FormParam("newPage") Integer pageIndex) throws DataSourceException, NoSuchMethodException,  InvocationTargetException, IOException, IllegalAccessException {
		LOGGER.info(">>>>>>>AnalyticsService ::: Entering getDeepListening>>>>>>>> : inSourceCategory = " + inSourceCategory);
		int dataOffset = 0;
		dataOffset = (pageIndex - 1) * perPageEntries;

		ResponseBuilder responseBuilder;
		String spName = null;
		staticParameters = new HashMap<>();
		dynamicParameters = new HashMap<>();
		Integer acctId = accountId == -1 ? null : Integer.valueOf(accountId);
		Integer srcId = source == -1 ? null : Integer.valueOf(source);
		Integer sentimentId = sentiment == 100 ? null : Integer.valueOf(sentiment); // sentiment = 100 for sentiment filter value All
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, acctId);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_SEARCH_KEYWORD"), "".trim().equalsIgnoreCase(searchKeyword) ? null : searchKeyword);
		dynamicParameters.put(SP_IN_PARAM_SOURCE, srcId);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_SOURCE_CATEGORY"), inSourceCategory);
		dynamicParameters.put(SP_IN_PARAM_TOPIC, "".equalsIgnoreCase(topics) ? null : topics);
		dynamicParameters.put(SP_IN_PARAM_FROM_DT, "".trim().equalsIgnoreCase(startDt) ? null : startDt);
		dynamicParameters.put(SP_IN_PARAM_TO_DT, "".trim().equalsIgnoreCase(endDt) ? null : endDt);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_OFFSET"), dataOffset);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_LIMIT"), perPageEntries);
		dynamicParameters.put(SP_IN_PARAM_SENTIMENT_TYPE, sentimentId);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_CITY"), ("".equalsIgnoreCase(city.trim())) ? null : city);

		spName = PropertyHandler.getPropertyValue("SP_DEEP_LISTENING");
		LOGGER.debug(">>>>>>>SP Name :::: +++ " + spName);
		staticParameters.put(ATTRIBUTE_SP_NAME, spName);

		JdbcAnalyticsDao jdbcAD = (JdbcAnalyticsDao) FactoryDao.getInstance(JDBC_ANALYTICS_DAO);
		LOGGER.info("JdbcAnalyticsDao Object obtained...");
		Result result = jdbcAD.fetchExploreKPI(staticParameters, dynamicParameters);
		if (PropertyHandler.getPropertyValue("SOURCE_CATEGORY_ENTERPRISE").equalsIgnoreCase(inSourceCategory)) {
			LOGGER.info("Calling prepareNonSocialPostListData");
			responseBuilder = prepareNonSocialPostListData(result);
		} else {
			boolean noCityColumnFlag; // this variable determines whether city column data is required or not. City column is shown for only Zomato and Twitter till date.
			if (source == 4 || source == 2) {
				noCityColumnFlag = false;
			} else {
				noCityColumnFlag = true;
			}
			LOGGER.info("Calling prepareSocialPostListData");
			responseBuilder = prepareSocialPostListData(result, noCityColumnFlag);
		}
		LOGGER.debug(">>>>>>>AnalyticsService ::: >>>>>>>>>>>> getDeepListening :: responseBuilder >>>" + responseBuilder.toString());
		LOGGER.info(">>>>>>>AnalyticsService ::: Exiting getDeepListening>>>>>>>>");
		return responseBuilder.build();

	}
	
	/**
	 * Method added for deeplistening tab
	 * @param jsonString
	 * @return
	 * @throws DataSourceException
	 * @throws NoSuchMethodException
	 * @throws InvocationTargetException
	 * @throws IOException
	 * @throws IllegalAccessException
	 */
	
	@POST
	@Path("/getDataForDeepListTab")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDeepListeningDataForDeepistTab( String jsonString) throws DataSourceException, NoSuchMethodException,  InvocationTargetException, IOException, IllegalAccessException {
		LOGGER.info(">>>>>>>AnalyticsService ::: Entering getDataForDeepListTab>>>>>>>>"+jsonString);
		
		ObjectMapper objectMapper = new ObjectMapper();
		DeepListeningObject deepListObj = objectMapper.readValue(jsonString, DeepListeningObject.class);
		
		int dataOffset = 0;
		dataOffset = (deepListObj.getNewPage() - 1) * deepListObj.getPerPageEntries();

		int accountId = deepListObj.getAccountId();
		int source = deepListObj.getSourceId();
		int sentiment = deepListObj.getSentiment();
		
		ResponseBuilder responseBuilder;
		String spName = null;
		staticParameters = new HashMap<>();
		dynamicParameters = new HashMap<>();
		Integer acctId = accountId == -1 ? null : Integer.valueOf(accountId);
		Integer srcId = source == -1 ? null : Integer.valueOf(source);
		Integer sentimentId = sentiment == 100 ? null : Integer.valueOf(sentiment); // sentiment = 100 for sentiment filter value All
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, acctId);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_SEARCH_KEYWORD"), "".trim().equalsIgnoreCase(deepListObj.getSearchKey()) ? null : deepListObj.getSearchKey());
		dynamicParameters.put(SP_IN_PARAM_SOURCE, srcId);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_SOURCE_CATEGORY"), deepListObj.getSourceCategory());
		dynamicParameters.put(SP_IN_PARAM_TOPIC, "".equalsIgnoreCase(deepListObj.getSourceTopic()) ? null : deepListObj.getSourceTopic());
		dynamicParameters.put(SP_IN_PARAM_FROM_DT, "".trim().equalsIgnoreCase( deepListObj.getStartDate()) ? null : deepListObj.getStartDate());
		dynamicParameters.put(SP_IN_PARAM_TO_DT, "".trim().equalsIgnoreCase(deepListObj.getEndDate()) ? null : deepListObj.getEndDate());
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_OFFSET"), dataOffset);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_LIMIT"), deepListObj.getPerPageEntries());
		dynamicParameters.put(SP_IN_PARAM_SENTIMENT_TYPE, sentimentId);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_CITY"), ("".equalsIgnoreCase((deepListObj.getCity()).trim())) ? null : deepListObj.getCity());

		spName = PropertyHandler.getPropertyValue("SP_DEEP_LISTENING");
		LOGGER.debug(">>>>>>>SP Name :::: +++ " + spName);
		staticParameters.put(ATTRIBUTE_SP_NAME, spName);

		JdbcAnalyticsDao jdbcAD = (JdbcAnalyticsDao) FactoryDao.getInstance(JDBC_ANALYTICS_DAO);
		LOGGER.info("JdbcAnalyticsDao Object obtained...");
		Result result = jdbcAD.fetchExploreKPI(staticParameters, dynamicParameters);
		if (PropertyHandler.getPropertyValue("SOURCE_CATEGORY_ENTERPRISE").equalsIgnoreCase(deepListObj.getSourceCategory())) {
			LOGGER.info("Calling prepareNonSocialPostListData");
			responseBuilder = prepareNonSocialPostListData(result);
		} else {
			boolean noCityColumnFlag; // this variable determines whether city column data is required or not. City column is shown for only Zomato and Twitter till date.
			if (source == 4 || source == 2) {
				noCityColumnFlag = false;
			} else {
				noCityColumnFlag = true;
			}
			LOGGER.info("Calling prepareSocialPostListData");
			responseBuilder = prepareSocialPostListData(result, noCityColumnFlag);
		}
		LOGGER.debug(">>>>>>>AnalyticsService ::: >>>>>>>>>>>> getDataForDeepListTab :: responseBuilder >>>" + responseBuilder.toString());
		LOGGER.info(">>>>>>>AnalyticsService ::: Exiting getDataForDeepListTab>>>>>>>>");
		return responseBuilder.build();

	}

	
	@POST
	@Path("/getDeepListening")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getFilterAnalyticsDetails(@FormParam("inAccountId") int accountId, @FormParam("inSearchKeyword") String searchKeyword, @FormParam("inSource") int source, @FormParam("inSourceCategory") String inSourceCategory, @FormParam("inTopic") String topics, @FormParam("inEntity") String entity, @FormParam("start_dt") String startDt, @FormParam("end_dt") String endDt, @FormParam("sentiment") int sentiment, @FormParam("city") String city, @FormParam("details_tab_name") String detailsTabName, @FormParam("interval") int interval, @FormParam("perPageEntries") Integer perPageEntries, @FormParam("newPage") Integer pageIndex) throws DataSourceException, NoSuchMethodException, InvocationTargetException, IOException, IllegalAccessException {
		LOGGER.info(">>>>>>>AnalyticsService ::: Entering getDeepListening>>>>>>>>" + pageIndex);
		int dataOffset = 0;
		dataOffset = (pageIndex - 1) * perPageEntries;

		ResponseBuilder responseBuilder;
		String spName = null;
		staticParameters = new HashMap<>();
		dynamicParameters = new HashMap<>();
		Integer acctId = accountId == -1 ? null : Integer.valueOf(accountId);
		Integer srcId = source == -1 ? null : Integer.valueOf(source);
		Integer sentimentId = sentiment == 100 ? null : Integer.valueOf(sentiment); // sentiment = 100 for sentiment filter value All
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, acctId);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_SEARCH_KEYWORD"), "".trim().equalsIgnoreCase(searchKeyword) ? null : searchKeyword);
		dynamicParameters.put(SP_IN_PARAM_SOURCE, srcId);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_SOURCE_CATEGORY"), inSourceCategory);
		dynamicParameters.put(SP_IN_PARAM_TOPIC, "".equalsIgnoreCase(topics) ? null : topics);
		dynamicParameters.put(SP_IN_PARAM_FROM_DT, "".trim().equalsIgnoreCase(startDt) ? null : startDt);
		dynamicParameters.put(SP_IN_PARAM_TO_DT, "".trim().equalsIgnoreCase(endDt) ? null : endDt);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_OFFSET"), dataOffset);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_LIMIT"), perPageEntries);
		dynamicParameters.put(SP_IN_PARAM_SENTIMENT_TYPE, sentimentId);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_CITY"), ("".equalsIgnoreCase(city.trim())) ? null : city);

		spName = PropertyHandler.getPropertyValue("SP_DEEP_LISTENING");
		LOGGER.debug(">>>>>>>SP Name :::: " + spName);
		staticParameters.put(ATTRIBUTE_SP_NAME, spName);

		JdbcAnalyticsDao jdbcAD = (JdbcAnalyticsDao) FactoryDao.getInstance(JDBC_ANALYTICS_DAO);
		LOGGER.info("JdbcAnalyticsDao Object obtained...");
		Result result = jdbcAD.fetchExploreKPI(staticParameters, dynamicParameters);
		if (PropertyHandler.getPropertyValue("SOURCE_CATEGORY_ENTERPRISE").equalsIgnoreCase(inSourceCategory)) {
			LOGGER.info("Loading time prepareNonSocialPostListData");
			responseBuilder = prepareNonSocialPostListData(result);
		} else {
			boolean noCityColumnFlag; // this variable determines whether city column data is required or not. City column is shown for only Zomato and Twitter till date.
			if (source == 4 || source == 2) {
				noCityColumnFlag = false;
			} else {
				noCityColumnFlag = true;
			}
			LOGGER.info("Loading time prepareSocialPostListData");
			responseBuilder = prepareSocialPostListData(result, noCityColumnFlag);
		}
		LOGGER.debug(">>>>>>>AnalyticsService ::: >>>>>>>>>>>> getDeepListening :: responseBuilder >>>" + responseBuilder.toString());
		LOGGER.info(">>>>>>>AnalyticsService ::: Exiting getDeepListening>>>>>>>>");
		return responseBuilder.build();

	}

	/**
	 * Data for Social Post List is formed in such a way that aspect wise categorized sentiment per post is included.
	 * 
	 * @param result
	 * @return
	 * @throws SecurityException 
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonGenerationException 
	 * @throws Exception
	 */
	public ResponseBuilder prepareSocialPostListData(Result result, boolean noCityColumnFlag) throws NoSuchMethodException, IllegalAccessException,  InvocationTargetException, IOException {
		LOGGER.info("Inside new code");
		LOGGER.info(">>>>>>>AnalyticsService ::: Entering prepareSocialPostListData>>>>>>>>");
		ResponseBuilder responseBuilder;
		ObjectMapper mapper = new ObjectMapper();

		Method fieldGetter = result.getClass().getMethod(GET_ROWS);
		List socialPostListData = (List) fieldGetter.invoke(result);
		Method fg = result.getClass().getMethod(GET_COUNT);
		int countValue = (int) fg.invoke(result);
		LOGGER.info("countValue : " + countValue);
		LinkedHashMap<String, Object> socialPostListMap = new LinkedHashMap<>();
		HashMap<String, Object> responseMap = new HashMap<>();
		responseMap.put(COUNT, countValue);
		DeepListeningObject deepListeningObj = new DeepListeningObject();
		List<SocialPostListObject> socialPostObjList = new ArrayList<>();
		for (Object rowObj : socialPostListData) {
			String contextName = ((List) rowObj).get(10).toString();
			if (!"Overall".equalsIgnoreCase(contextName)) {
				String postId = ((List) rowObj).get(7).toString();
				LOGGER.info("POST_ID : ----->>"+postId);
				SocialPostListObject socialPostListObj;
				Map<String, String> contextWiseSentiment;

				String contextWiseSentimentKey;
				LOGGER.info("------>" + socialPostListMap.get(postId));
				
				//When the map being created doesn't contain the post then code of if is executed.
				if (!socialPostListMap.containsKey(postId)) {
					LOGGER.info("Inside IF block");
					socialPostListObj = new SocialPostListObject();
					socialPostListObj.setPostTypeId(Integer.parseInt(((List) rowObj).get(0).toString()));
					socialPostListObj.setPostType(((List) rowObj).get(1).toString());
					socialPostListObj.setSourceTypeId(Integer.parseInt(((List) rowObj).get(2).toString()));
					socialPostListObj.setSourceTypeName(((List) rowObj).get(3).toString());
					socialPostListObj.setSourceTypeCode(((List) rowObj).get(4).toString());
					socialPostListObj.setPostDateTime(((List) rowObj).get(5) == null ? "" : ((List) rowObj).get(5).toString());
					socialPostListObj.setPostText(((List) rowObj).get(6) == null ? "" : ((List) rowObj).get(6).toString());
					socialPostListObj.setPostUrl(((List) rowObj).get(7).toString());
					socialPostListObj.setPostId(((List) rowObj).get(8).toString());
					socialPostListObj.setOverallSentiment(((List) rowObj).get(9).toString());
					socialPostListObj.setTotalCount(((List) rowObj).get(12).toString());
					
					socialPostListObj.setHasTicket(((List) rowObj).get(13).toString());
					
					//City column appears only in case when filtered from Explore Page.
					if (!noCityColumnFlag) {
						socialPostListObj.setCity(((List) rowObj).get(12).toString());
					}
					contextWiseSentiment = socialPostListObj.getCategorizedSentiment();
					contextWiseSentiment = initContextWiseSentimentMap(contextWiseSentiment);
					contextName = ((List) rowObj).get(10).toString();
					if (PRICES_AND_OFFERS_CONTEXT.equalsIgnoreCase(contextName)) {
						contextWiseSentiment.put(PRICES_AND_OFFERS_ABR, ((List) rowObj).get(11).toString());
					} else {
						contextWiseSentiment.put(contextName.substring(0, 1), ((List) rowObj).get(11).toString());
					}
					socialPostListMap.put(postId, socialPostListObj);

				} else {
					LOGGER.info("Inside Else block");
					//When the map has an entry for this post_id execution enters the else block.
					socialPostListObj = (SocialPostListObject) socialPostListMap.get(postId);
					contextWiseSentiment = socialPostListObj.getCategorizedSentiment();
					contextName = ((List) rowObj).get(10).toString();
					if (PRICES_AND_OFFERS_CONTEXT.equalsIgnoreCase(contextName)) {
						contextWiseSentimentKey = PRICES_AND_OFFERS_ABR;
					} else {
						contextWiseSentimentKey = contextName.substring(0, 1);
					}
					contextWiseSentiment.put(contextWiseSentimentKey, ((List) rowObj).get(11).toString());
				}
			}
		}

		for (Object key : socialPostListMap.keySet()) {
			SocialPostListObject socialPostListObject = (SocialPostListObject) socialPostListMap.get(key);
			socialPostObjList.add(socialPostListObject);
		}

		deepListeningObj.setSocialPostListObj(socialPostObjList);
		responseMap.put(OUTPUT, deepListeningObj);

		String socialPostResultJson = mapper.writeValueAsString(responseMap);
		LOGGER.debug(">>>>>>>AnalyticsService ::: >>>>>>>>>>>> socialPostResultJson >>>" + socialPostResultJson);
		responseBuilder = Response.ok(socialPostResultJson);
		LOGGER.info(">>>>>>>AnalyticsService ::: Exiting prepareSocialPostListData>>>>>>>>");
		return responseBuilder;
	}

	public Result fetchAspectsOnLoadForWordCloud(int accountId) throws DataSourceException{
		LOGGER.info(">>>>>>>AnalyticsService ::: Entering fetchAspectsOnLoadForWordCloud>>>>>>>>");
		staticParameters = new HashMap<>();
		staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_WORD_CLOUD_ASPECT_ONLOAD"));
		dynamicParameters = new HashMap<>();
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
		JdbcAnalyticsDao jdbcAD = (JdbcAnalyticsDao) FactoryDao.getInstance(JDBC_ANALYTICS_DAO);
		Result result = jdbcAD.getProcedureResultSet(staticParameters, dynamicParameters);
		LOGGER.info(">>>>>>>AnalyticsService ::: Exiting fetchAspectsOnLoadForWordCloud>>>>>>>>");
		return result;
	}

	public Result fetchEntitiesOnLoadForWordCloud(int accountId) throws DataSourceException {
		LOGGER.info(">>>>>>>AnalyticsService ::: Entering fetchEntitiesOnLoadForWordCloud>>>>>>>>");
		staticParameters = new HashMap<>();
		staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_WORD_CLOUD_ENTITY_ONLOAD"));
		dynamicParameters = new HashMap<>();
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
		JdbcAnalyticsDao jdbcAD = (JdbcAnalyticsDao) FactoryDao.getInstance(JDBC_ANALYTICS_DAO);
		Result result = jdbcAD.getProcedureResultSet(staticParameters, dynamicParameters);
		LOGGER.info(">>>>>>>AnalyticsService ::: Exiting fetchEntitiesOnLoadForWordCloud>>>>>>>>");
		return result;
	}
	
	/**
	 * Data for Non-Social Post List is formed in such a way that aspect wise categorized sentiment per post is included.
	 * 
	 * @param result
	 * @return
	 * @throws SecurityException, NoSuchMethodException, InvocationTargetException, IllegalArgumentException, IllegalAccessException, IOException, JsonMappingException, JsonGenerationException, Exception
	 */
	public ResponseBuilder prepareNonSocialPostListData(Result result) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, IOException {

		LOGGER.info(">>>>>>>AnalyticsService ::: Entering prepareNonSocialPostListData>>>>>>>>");
		ResponseBuilder responseBuilder;
		ObjectMapper mapper = new ObjectMapper();

		Method fieldGetter = result.getClass().getMethod(GET_ROWS);
		List nonSocialPostListData = (List) fieldGetter.invoke(result);
		Method fg = result.getClass().getMethod(GET_COUNT);
		int countValue = (int) fg.invoke(result);
		LinkedHashMap<String, Object> nonSocialPostListMap = new LinkedHashMap<>();
		HashMap<String, Object> responseMap = new HashMap<>();
		responseMap.put(COUNT, countValue);
		DeepListeningObject deepListeningObj = new DeepListeningObject();
		List<NonSocialPostListObject> nonSocialPostObjList = new ArrayList<>();
		for (Object rowObj : nonSocialPostListData) {
			String postId = ((List) rowObj).get(8).toString();
			NonSocialPostListObject nonSocialPostListObj;
			Map<String, String> contextWiseSentiment;
			//When the map being created doesn't contain the post then code of if is executed.
			if (!nonSocialPostListMap.containsKey(postId)) {
				nonSocialPostListObj = new NonSocialPostListObject();
				nonSocialPostListObj.setPostTypeId(Integer.parseInt(((List) rowObj).get(0).toString()));
				nonSocialPostListObj.setPostType(((List) rowObj).get(1).toString());
				nonSocialPostListObj.setSourceTypeId(Integer.parseInt(((List) rowObj).get(2).toString()));
				nonSocialPostListObj.setSourceTypeName(((List) rowObj).get(3).toString());
				nonSocialPostListObj.setSourceTypeCode(((List) rowObj).get(4).toString());
				nonSocialPostListObj.setPostDateTime(((List) rowObj).get(5) == null ? "" : ((List) rowObj).get(5).toString());
				nonSocialPostListObj.setPostText(((List) rowObj).get(6).toString());
				nonSocialPostListObj.setPostUrl(((List) rowObj).get(7) == null ? "" : ((List) rowObj).get(7).toString());
				nonSocialPostListObj.setPostId(((List) rowObj).get(8).toString());
				nonSocialPostListObj.setOverallSentiment(((List) rowObj).get(9).toString());
				nonSocialPostListObj.setEmailId(((List) rowObj).get(12).toString());
				nonSocialPostListObj.setAuthorName(((List) rowObj).get(13) == null ? "" : ((List) rowObj).get(13).toString());
				contextWiseSentiment = nonSocialPostListObj.getCategorizedSentiment();
				contextWiseSentiment = initContextWiseSentimentMap(contextWiseSentiment);
				String contextName = ((List) rowObj).get(10).toString();
				if (PRICES_AND_OFFERS_CONTEXT.equalsIgnoreCase(contextName)) {
					contextWiseSentiment.put(PRICES_AND_OFFERS_ABR, ((List) rowObj).get(11).toString());
				} else {
					contextWiseSentiment.put(contextName.substring(0, 1), ((List) rowObj).get(11).toString());
				}
				nonSocialPostListMap.put(postId, nonSocialPostListObj);
			} else {
				//When the map has an entry for this post_id execution enters the else block.
				nonSocialPostListObj = (NonSocialPostListObject) nonSocialPostListMap.get(postId);
				contextWiseSentiment = nonSocialPostListObj.getCategorizedSentiment();
				String contextName = ((List) rowObj).get(10).toString();
				if (!PropertyHandler.getPropertyValue("OTHERS").equalsIgnoreCase(contextName)) {
					if (PRICES_AND_OFFERS_CONTEXT.equalsIgnoreCase(contextName)) {
						contextWiseSentiment.put(PRICES_AND_OFFERS_ABR, ((List) rowObj).get(11).toString());
					} else {
						contextWiseSentiment.put(contextName.substring(0, 1), ((List) rowObj).get(11).toString());
					}
				}
			}
		}

		for (Map.Entry<String, Object> entry : nonSocialPostListMap.entrySet()) {
			NonSocialPostListObject nonSocialPostListObject = (NonSocialPostListObject) entry.getValue();
			nonSocialPostObjList.add(nonSocialPostListObject);
		}

		deepListeningObj.setNonSocialPostListObj(nonSocialPostObjList);
		responseMap.put(OUTPUT, deepListeningObj);

		String nonSocialPostResultJson = mapper.writeValueAsString(responseMap);
		LOGGER.debug(">>>>>>>AnalyticsService ::: >>>>>>>>>>>> nonSocialPostResultJson >>>" + nonSocialPostResultJson);
		responseBuilder = Response.ok(nonSocialPostResultJson);
		LOGGER.info(">>>>>>>AnalyticsService ::: Exiting prepareNonSocialPostListData>>>>>>>>");
		return responseBuilder;

	}
	
	
	@POST
	@Path("/getPostDetailsSnipperwiseData")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPostDetailsSnipperwiseData(@FormParam("postId") String postId,@FormParam("companyId") int companyId) throws DataSourceException {

		LOGGER.info(">>>>>>>AnalyticsService ::: Entering getPostDetailsSnipperwiseData>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		dynamicParameters = new HashMap<>();
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_POST_ID"), postId);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_COMPANY_ID"), companyId);
		staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_SNIPPET_LEVEL_DETAILS"));

		JdbcAnalyticsDao jdbcCD = (JdbcAnalyticsDao) FactoryDao.getInstance(JDBC_ANALYTICS_DAO);
		Result result = jdbcCD.fetchPostSnippets(staticParameters, dynamicParameters);
		
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>AnalyticsService ::: Exiting getPostDetailsSnipperwiseData>>>>>>>>");
		return responseBuilder.build();
	}
	
	@POST
	@Path("/checkIfTicketAvailable")
	@Produces(MediaType.APPLICATION_JSON)
	public Response checkIfTicketAvailable(@FormParam("postID") String postId) throws DataSourceException {

		LOGGER.info(">>>>>>>AnalyticsService ::: Entering checkIfTicketAvailable>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		dynamicParameters = new HashMap<>();
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_POST_ID"), postId);
		staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_CHECK_POSTID_ID_ALREADY_HAS_TICKET"));

		JdbcAnalyticsDao jdbcCD = (JdbcAnalyticsDao) FactoryDao.getInstance(JDBC_ANALYTICS_DAO);
		Result result = jdbcCD.fetchIFTicketAvailable(staticParameters, dynamicParameters);
		
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>AnalyticsService ::: Exiting getPostDetailsSnipperwiseData>>>>>>>>");
		return responseBuilder.build();
	}
	
	
	@POST
	@Path("/getFeedbackdata")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getFeedbackdata(@FormParam("selectedSourceId") int selectedSourceId,@FormParam("inStartDt") String startDate ,@FormParam("inEndDt") String endDate ,@FormParam("companyId") int companyId) throws DataSourceConnectionException {
		
		LOGGER.info(">>>>>>>AnalyticsService ::: Entering getFeedbackdata>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		dynamicParameters = new HashMap<>();
		staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_FEEDBACK_SNAPSHOT"));
		dynamicParameters.put(SP_IN_PARAM_SOURCE_ID, selectedSourceId);
		dynamicParameters.put(SP_IN_PARAM_FROM_DT, startDate+START_TIME );
		dynamicParameters.put(SP_IN_PARAM_TO_DT, endDate+END_TIME );
		dynamicParameters.put(SP_IN_PARAM_PRIMARY_COMPANY_ID, companyId);
		
		JdbcAnalyticsDao jdbcAD = (JdbcAnalyticsDao) FactoryDao.getInstance(JDBC_ANALYTICS_DAO);
		Result result = jdbcAD.fetchFeedbackData(staticParameters, dynamicParameters);
		LOGGER.info(">>>>>>>AnalyticService ::: getFeedbackdata ::: result >>>>>>>>" + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>AnalyticService ::: Exiting getFeedbackdata>>>>>>>>");
		return responseBuilder.build();
		
	}


	
	@POST
	@Path("/getComplaintData")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getComplaintData(@FormParam("selectedSourceId") int selectedSourceId,@FormParam("inStartDt") String startDate ,@FormParam("inEndDt") String endDate ,@FormParam("companyId") int companyId) throws DataSourceConnectionException {
		
		LOGGER.info(">>>>>>>AnalyticsService ::: Entering getComplaintData>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		dynamicParameters = new HashMap<>();
		staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_COMPLAINT_SNAPSHOT"));
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_SOURCE_ID"), selectedSourceId);
		dynamicParameters.put(SP_IN_PARAM_FROM_DT, startDate+START_TIME );
		dynamicParameters.put(SP_IN_PARAM_TO_DT, endDate+END_TIME );
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_PRIMARY_COMPANY_ID"), companyId);
		
		JdbcAnalyticsDao jdbcAD = (JdbcAnalyticsDao) FactoryDao.getInstance(JDBC_ANALYTICS_DAO);
		Result result = jdbcAD.fetchComplaintData(staticParameters, dynamicParameters);
		LOGGER.info(">>>>>>>AnalyticService ::: getComplaintData ::: result >>>>>>>>" + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>AnalyticService ::: Exiting getComplaintData>>>>>>>>");
		return responseBuilder.build();
		
	}
	
	
	@POST
	@Path("/getTopAdvocatesData")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTopAdvocatesData(@FormParam("selectedSourceId") int selectedSourceId,
			@FormParam("inStartDt") String startDate ,@FormParam("inEndDt") String endDate ,
			@FormParam("inAuthorName") String authorName ,
			@FormParam("companyId") int companyId) throws DataSourceConnectionException{
		
		LOGGER.info(">>>>>>>AnalyticsService ::: Entering fetchTopAdvocatesData>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		dynamicParameters = new HashMap<>();
		staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_TOP_ADVOCATES_POST"));
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_SOURCE_ID"), selectedSourceId);
		dynamicParameters.put(SP_IN_PARAM_FROM_DT, startDate);
		dynamicParameters.put(SP_IN_PARAM_TO_DT, endDate);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_PRIMARY_COMPANY_ID"), companyId);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_AUTHOR_NAME"), authorName);
		
		JdbcAnalyticsDao jdbcAD = (JdbcAnalyticsDao) FactoryDao.getInstance(JDBC_ANALYTICS_DAO);
		Result result = jdbcAD.fetchTopAdvocatesData(staticParameters, dynamicParameters);
		LOGGER.info(">>>>>>>AnalyticService ::: fetchTopAdvocatesData ::: result >>>>>>>>" + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>AnalyticService ::: Exiting fetchTopAdvocatesData>>>>>>>>");
		return responseBuilder.build();	
	}
	
	/**
	 * This method just initializes the Map with the abbreviated contexts as key with blank string as corresponding value.
	 * 
	 * @param contextWiseSentiment
	 * @return
	 */
	public Map<String, String> initContextWiseSentimentMap(Map<String, String> contextWiseSentiment) {
		contextWiseSentiment.put(PropertyHandler.getPropertyValue("CLEANLINESS"), BLANK_STRING);
		contextWiseSentiment.put(PropertyHandler.getPropertyValue("HOSPITALITY"), BLANK_STRING);
		contextWiseSentiment.put(PropertyHandler.getPropertyValue("ACCURACY"), BLANK_STRING);
		contextWiseSentiment.put(PropertyHandler.getPropertyValue("MAINTENANCE"), BLANK_STRING);
		contextWiseSentiment.put(PropertyHandler.getPropertyValue("PRODUCT_QUALITY"), BLANK_STRING);
		contextWiseSentiment.put(PropertyHandler.getPropertyValue("SPEED"), BLANK_STRING);
		contextWiseSentiment.put(PRICES_AND_OFFERS_ABR, BLANK_STRING);
		contextWiseSentiment.put(PropertyHandler.getPropertyValue("OTHERS"), BLANK_STRING);
		contextWiseSentiment.put(PropertyHandler.getPropertyValue("NON_RELEVANT"), BLANK_STRING);
		return contextWiseSentiment;
	}

	@POST
	@Path("/saveDeepListDataForExcel")
	@Produces(MediaType.APPLICATION_JSON)
	public void saveDeepListDataForExcel(@FormParam("inAccountId") int accountId, @FormParam("inSearchKeyword") String searchKeyword, 
			@FormParam("inSource") int source, @FormParam("inSourceCategory") String inSourceCategory, 
			@FormParam("inTopic") String topics, @FormParam("inEntity") String entity, 
			@FormParam("start_dt") String startDt, @FormParam("end_dt") String endDt, 
			@FormParam("sentiment") int sentiment, @FormParam("city") String city, 
			@FormParam("details_tab_name") String detailsTabName, 
			@FormParam("interval") int interval, 
			@FormParam("perPageEntries") Integer perPageEntries, 
			@FormParam("newPage") Integer pageIndex) throws DataSourceException {
		
		LOGGER.info(">>>>>>>AnalyticsService ::: Entering getDeepListening>>>>>>>> : inSourceCategory = " + inSourceCategory);
		int dataOffset = 0;
		dataOffset = (pageIndex - 1) * perPageEntries;

		ResponseBuilder responseBuilder;
		String spName = null;
		staticParameters = new HashMap<>();
		dynamicParameters = new HashMap<>();
		Integer acctId = accountId == -1 ? null : Integer.valueOf(accountId);
		Integer srcId = source == -1 ? null : Integer.valueOf(source);
		Integer sentimentId = sentiment == 100 ? null : Integer.valueOf(sentiment); // sentiment = 100 for sentiment filter value All
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, acctId);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_SEARCH_KEYWORD"), "".trim().equalsIgnoreCase(searchKeyword) ? null : searchKeyword);
		dynamicParameters.put(SP_IN_PARAM_SOURCE, srcId);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_SOURCE_CATEGORY"), inSourceCategory);
		dynamicParameters.put(SP_IN_PARAM_TOPIC, "".equalsIgnoreCase(topics) ? null : topics);
		dynamicParameters.put(SP_IN_PARAM_FROM_DT, "".trim().equalsIgnoreCase(startDt) ? null : startDt);
		dynamicParameters.put(SP_IN_PARAM_TO_DT, "".trim().equalsIgnoreCase(endDt) ? null : endDt);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_OFFSET"), dataOffset);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_LIMIT"), perPageEntries);
		dynamicParameters.put(SP_IN_PARAM_SENTIMENT_TYPE, sentimentId);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_CITY"), ("".equalsIgnoreCase(city.trim())) ? null : city);

		spName = PropertyHandler.getPropertyValue("SP_DEEP_LISTENING");
		LOGGER.debug(">>>>>>>SP Name :::: +++ " + spName);
		staticParameters.put(ATTRIBUTE_SP_NAME, spName);

		JdbcAnalyticsDao jdbcAD = (JdbcAnalyticsDao) FactoryDao.getInstance(JDBC_ANALYTICS_DAO);
		LOGGER.info("JdbcAnalyticsDao Object obtained...");
		ResultSet result = jdbcAD.fetchDataForDeepList(staticParameters, dynamicParameters);
	
		CommonService cms= new CommonService();

		//cms.saveDataToExcel(result);

	}
}
