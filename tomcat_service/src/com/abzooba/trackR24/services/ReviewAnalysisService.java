package com.abzooba.trackR24.services;

import static com.abzooba.trackR24.util.Constants.ATTRIBUTE_SP_NAME;
import static com.abzooba.trackR24.util.Constants.ATTRIBUTE_SQL;
import static com.abzooba.trackR24.util.Constants.END_TIME;
import static com.abzooba.trackR24.util.Constants.JDBC_ANALYTICS_DAO;
import static com.abzooba.trackR24.util.Constants.JDBC_REVIEW_ANALYSIS_DAO;
import static com.abzooba.trackR24.util.Constants.RESULTS;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_ACCOUNT_ID;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_CITY_ID;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_FROM_DT;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_PERIODIC_END_FROM;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_PERIODIC_END_TO;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_PERIODIC_START_FROM;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_PERIODIC_START_TO;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_SOURCE_ID;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_TO_DT;
import static com.abzooba.trackR24.util.Constants.START_TIME;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

import com.abzooba.trackR24.dao.DataSourceConnectionException;
import com.abzooba.trackR24.dao.DataSourceException;
import com.abzooba.trackR24.dao.FactoryDao;
import com.abzooba.trackR24.dao.JdbcAnalyticsDao;
import com.abzooba.trackR24.dao.JdbcReviewAnalysisDao;
import com.abzooba.trackR24.models.Result;
import com.abzooba.trackR24.models.Review;
import com.abzooba.trackR24.util.PropertyHandler;
import com.fasterxml.jackson.databind.ObjectMapper;

@Path("/reviewanalysis")
@SuppressWarnings("unchecked")
public class ReviewAnalysisService { 

	private static final Logger LOGGER = Logger.getLogger(AnalyticsService.class);

	private Map<String, Object> staticParameters;
	private Map<String, Object> dynamicParameters;

	
	/**
	 * Generating Review Analysis Data
	 * @throws DataSourceConnectionException 
	 * @throws IOException 
	 * @throws  
	 * @throws  
	 */

	@POST
	@Path("/getReviewAnalysisData")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getReviewAnalyticsData(String jsonString) throws DataSourceConnectionException, IOException {
		
		ObjectMapper objectMapper = new ObjectMapper();

		Review review = objectMapper.readValue(jsonString, Review.class); 
		
		//LOGGER.info(">>>>>>>ReviewAnalysisService ::: Entering getReviewAnalyticsData>>>>>>>> : inAccountId = " + accountId + "--" + pageIndex + "--" + perPageEntries);
		int dataOffset = 0;
		//dataOffset = (pageIndex - 1) * perPageEntries;
		dataOffset = (review.getNewPage() - 1) * review.getPerPageEntries();
		LOGGER.info("dataOffset :" + dataOffset);
		
		ResponseBuilder responseBuilder;
		String spName = null;
		staticParameters = new HashMap<>();
		dynamicParameters = new HashMap<>();
		int sentiment = review.getSentiment();
		
		//accountId = accountId == -1 ? null : Integer.valueOf(accountId);
		//source = source == -1 ? null : Integer.valueOf(source);
		//sentiment = sentiment == 100 ? null : Integer.valueOf(sentiment); // sentiment = 100 for sentiment filter value All
		
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, review.getAccountId());
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_SOURCE"), review.getSourceId());
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_CITY"), review.getCityId());
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_LOCALITY"),review.getLocality());
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_SENTIMENT_TYPE"), null);
		dynamicParameters.put(SP_IN_PARAM_FROM_DT, "".trim().equalsIgnoreCase(review.getStartDt()) ? null : review.getStartDt());
		dynamicParameters.put(SP_IN_PARAM_TO_DT, "".trim().equalsIgnoreCase(review.getEndDt()) ? null : review.getEndDt());
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_OFFSET"), dataOffset);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_LIMIT"), review.getPerPageEntries());
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_OWNERSHIP_ID"), review.getOwnershipId());
		spName = PropertyHandler.getPropertyValue("SP_REVIEW_ANALYSIS");
		LOGGER.debug(">>>>>>>SP Name :::: " + spName);
		staticParameters.put(ATTRIBUTE_SP_NAME, spName);

		JdbcReviewAnalysisDao jdbcAD = (JdbcReviewAnalysisDao) FactoryDao.getInstance(JDBC_REVIEW_ANALYSIS_DAO);
		LOGGER.info("JdbcReviewAnalyticsDao Object obtained...");
		Result result = jdbcAD.fetchReviewAnalysisData(staticParameters, dynamicParameters);

		JSONObject resultObj = new JSONObject();
		LOGGER.info("Review Analysis result:"+result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());

		LOGGER.debug(">>>>>>>AnalyticsService ::: >>>>>>>>>>>> getReviewAnalyticsData :: responseBuilder >>>" + responseBuilder.toString());
		LOGGER.info(">>>>>>>AnalyticsService ::: Exiting getReviewAnalyticsData>>>>>>>>");
		return responseBuilder.build();

	}
	
	@POST
	@Path("/getKpiMetaData")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getKpiMetaData(@FormParam("inTabId") int inTabId) throws DataSourceException {

		LOGGER.info(">>>>>>>ReviewAnalysisService ::: Entering getKpiMetaData>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		dynamicParameters = new HashMap<>();
		
		dynamicParameters.put("inTabId", inTabId);
		staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_GET_KPIS"));

		JdbcReviewAnalysisDao jdbcCD = (JdbcReviewAnalysisDao) FactoryDao.getInstance(JDBC_REVIEW_ANALYSIS_DAO);
		Result result = jdbcCD.fetchKpiList(staticParameters, dynamicParameters);
		
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>ReviewAnalysisService ::: Exiting getKpiMetaData>>>>>>>>");
		return responseBuilder.build();

	}
	
	//Getting Review Analysis Source from tblReviewAnalysisSources table
	//@POST
	//@Path("/getReviewAnalysisSources")
	//@Produces(MediaType.APPLICATION_JSON) 
	
	@GET
	@Path("/getReviewAnalysisSources")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getReviewAnalysisSources(@QueryParam("accountId") int accountId) throws DataSourceConnectionException {

		LOGGER.info(">>>>>>>getReviewAnalysisSources ::: Entering getReviewAnalysisSources>>>>>>>>" + accountId);
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_GET_REVIEW_ANALYSIS_SOURCES"));
		dynamicParameters = new HashMap<>();
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
		
		JdbcReviewAnalysisDao jdbcCD = (JdbcReviewAnalysisDao) FactoryDao.getInstance(JDBC_REVIEW_ANALYSIS_DAO);
		Result result = jdbcCD.fetchReviewAnalysisSources(staticParameters, dynamicParameters);
		LOGGER.info(">>>>>>>CommonService ::: getSources ::: result from fetchSourcesOrContexts>>>>>>>>" + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>CommonService ::: Exiting getSources>>>>>>>>");
		return responseBuilder.build();
	}
	
	/**
	 * This service is invoked for default loading of Periodic Snapshot and when user changes the source too.
	 * @param selectedSourceId
	 * @return
	 * @throws DataSourceConnectionException 
	 * @throws Exception
	 */
	@POST
	@Path("/getReviewSnapshotData")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getReviewSnapshotData(@FormParam("selectedSourceId") int selectedSourceId,@FormParam("inCurrentStartEndDt") String currentStartEndDt ,@FormParam("inPreviousStartEndDt") String previousStartEndDt ,@FormParam("accountId") int accountId) throws DataSourceConnectionException  {
		LOGGER.info(">>>>>>>AnalyticsService ::: Entering getReviewSnapshotData>>>>>>>>"+ currentStartEndDt + selectedSourceId);
		JSONObject resultObj = new JSONObject();
		
		String[] inCurrentStartEndDtValue = currentStartEndDt.split(" - ");
		String[] inPreviousStartEndDtValue = previousStartEndDt.split(" - ");
		
		LOGGER.info("START CURRENT DATE PRINTING : " + inCurrentStartEndDtValue[0]);
		LOGGER.info("CURRENT END DATE PRINTING : " + inCurrentStartEndDtValue[1]);
		LOGGER.info("START PREVIOUS DATE PRINTING : " + inPreviousStartEndDtValue[0]);
		LOGGER.info("PREVIOUS END DATE PRINTING : " + inPreviousStartEndDtValue[1]);
		
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		dynamicParameters = new HashMap<>();
		staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_PERIODIC_SNAPSHOT_REVIEW_ANALYSIS"));
		dynamicParameters.put(SP_IN_PARAM_SOURCE_ID, selectedSourceId);
		dynamicParameters.put(SP_IN_PARAM_PERIODIC_START_FROM, inCurrentStartEndDtValue[0]+START_TIME );
		dynamicParameters.put(SP_IN_PARAM_PERIODIC_START_TO, inCurrentStartEndDtValue[1]+END_TIME );
		dynamicParameters.put(SP_IN_PARAM_PERIODIC_END_FROM, inPreviousStartEndDtValue[0]+START_TIME );
		dynamicParameters.put(SP_IN_PARAM_PERIODIC_END_TO, inPreviousStartEndDtValue[1]+END_TIME );
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
		
		JdbcReviewAnalysisDao jdbcRD = (JdbcReviewAnalysisDao) FactoryDao.getInstance(JDBC_REVIEW_ANALYSIS_DAO);
		Result result = jdbcRD.fetchReviewSnapshotData(staticParameters, dynamicParameters);
		LOGGER.info(">>>>>>>AnalyticService ::: getReviewSnapshotData ::: result >>>>>>>>" + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>AnalyticService ::: Exiting getReviewSnapshotData>>>>>>>>");
		return responseBuilder.build();
		
	}

	
	//getting cities against selected source
	@GET
	@Path("/getCitiesAgainstSource")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCitiesAgainstSource(@QueryParam("accountId") int inAccountId,@QueryParam("selectedsource") int selectedSource) throws DataSourceConnectionException  {
		LOGGER.info(">>>>>>>getCitiesAgainstSource ::: Entering getCitiesAgainstSource>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		dynamicParameters = new HashMap<>();
		
		staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_GET_CITIES_AGAINST_SOURCES"));
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, inAccountId);
		dynamicParameters.put(SP_IN_PARAM_SOURCE_ID, selectedSource);

		JdbcReviewAnalysisDao jdbcCD = (JdbcReviewAnalysisDao) FactoryDao.getInstance(JDBC_REVIEW_ANALYSIS_DAO);
		Result result = jdbcCD.fetchCityListAginstSource(staticParameters, dynamicParameters);
		
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>getCitiesAgainstSource ::: Exiting getCitiesAgainstSource>>>>>>>>");
		return responseBuilder.build();

	}
	
	
	//getting locality against selected source - This is during loading time
	@GET
	@Path("/getLocalitiesAgainstSource")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getLocalitiesAgainstSource(@QueryParam("accountId") int inAccountId,@QueryParam("selectedsource") int selectedSource) throws DataSourceConnectionException {
		LOGGER.info(">>>>>>>ReviewAnalysisService ::: Entering getLocalitiesAgainstSource>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		dynamicParameters = new HashMap<>();
		
		staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_GET_LOCALITIES_AGAINST_SOURCES"));
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, inAccountId);
		dynamicParameters.put(SP_IN_PARAM_SOURCE_ID, selectedSource);

		JdbcReviewAnalysisDao jdbcCD = (JdbcReviewAnalysisDao) FactoryDao.getInstance(JDBC_REVIEW_ANALYSIS_DAO);
		Result result = jdbcCD.fetchLocalityListAginstSource(staticParameters, dynamicParameters);
		
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>ReviewAnalysisService ::: Exiting getLocalitiesAgainstSource>>>>>>>>");
		return responseBuilder.build();

	}
	
	
	//getting locality against selected source and city - This is in filter selection
	@GET
	@Path("/getLocalitiesAgainstSourceCity")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getLocalitiesAgainstSourceCity(@QueryParam("accountId") int inAccountId,@QueryParam("selectedsource") int selectedSource,@QueryParam("selectedcity") int selectedCity) throws DataSourceConnectionException {
		LOGGER.info(">>>>>>>ReviewAnalysisService ::: Entering getLocalitiesAgainstSourceCity>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		dynamicParameters = new HashMap<>();
		
		dynamicParameters.put("account_id", inAccountId);
		dynamicParameters.put("selectedsource", selectedSource);
		dynamicParameters.put("selectedcity", selectedCity);
		staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_GET_LOCALITIES_AGAINST_SOURCES_CITY"));
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, inAccountId);
		dynamicParameters.put(SP_IN_PARAM_SOURCE_ID, selectedSource);
		dynamicParameters.put(SP_IN_PARAM_CITY_ID, selectedCity);
		
		JdbcReviewAnalysisDao jdbcCD = (JdbcReviewAnalysisDao) FactoryDao.getInstance(JDBC_REVIEW_ANALYSIS_DAO);
		Result result = jdbcCD.fetchLocalityListAginstSourceCity(staticParameters, dynamicParameters);
		
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>ReviewAnalysisService ::: Exiting getLocalitiesAgainstSourceCity>>>>>>>>");
		return responseBuilder.build();

	}
	
	
	@POST
	@Path("/getPostDetailsSnippetwiseDataForReviewAnalysis")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPostDetailsSnippetwiseDataForReviewAnalysis(@FormParam("reviewId") String reviewId) throws DataSourceException {
		LOGGER.info(">>>>>>>ReviewAnalysisService ::: Entering getPostDetailsSnippetwiseData>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		dynamicParameters = new HashMap<>();
		
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_POST_ID"), reviewId);
		staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_SNIPPET_LEVEL_DETAILS_FOR_REVIEW_ANALYSIS"));

		JdbcAnalyticsDao jdbcCD = (JdbcAnalyticsDao) FactoryDao.getInstance(JDBC_ANALYTICS_DAO);
		Result result = jdbcCD.fetchPostSnippetsForReviewAnalysis(staticParameters, dynamicParameters);
		
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>AnalyticsService ::: Exiting getPostDetailsSnipperwiseData>>>>>>>>");
		return responseBuilder.build();
	}
	
	@POST
	@Path("/overallSentimentUpdaterForReviewAnalysis")
	public int overallSentimentUpdaterForReviewAnalysis(@FormParam("company_Id") int companyID,@FormParam("sentimentID") String sentimentID, @FormParam("reviewID") String reviewID) throws DataSourceException {

		LOGGER.info(">>>>>>>AnalyticsService ::: Entering overallSentimentUpdaterForReviewAnalysis>>>>>>>>");
		staticParameters = new HashMap<>();
		dynamicParameters = new HashMap<>();
		
		String overAllSentiment = "Neutral";
		if(sentimentID.equalsIgnoreCase("1")){
			overAllSentiment = "Positive";
		}else if(sentimentID.equalsIgnoreCase("-1")){
			overAllSentiment = "Negative";
		}
		
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_POST_ID"), reviewID);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_SENTIMENT_TYPE"), overAllSentiment);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_SENTIMENT_NUMBER"),sentimentID);
		staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_UPDATE_OVERALL_SENTIMENT_FOR_REVIEW_ANALYSIS"));

		JdbcAnalyticsDao jdbcCD = (JdbcAnalyticsDao) FactoryDao.getInstance(JDBC_ANALYTICS_DAO);
		int updateStatus = jdbcCD.overallSentimentUpdaterForReviewAnalysis(staticParameters, dynamicParameters);
		
		LOGGER.info(">>>>>>>AnalyticsService ::: Exiting overallSentimentUpdater>>>>>>>>"+ updateStatus);
		return updateStatus;

	}
	
	
	
	@POST
	@Path("/getFeedbackdataForReviewAnalysis")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getFeedbackdata(@FormParam("selectedSourceId") int selectedSourceId,@FormParam("inStartDt") String startDate ,
			@FormParam("inEndDt") String endDate ,@FormParam("companyId") int companyId,@FormParam("inAccountId") int accountId) throws DataSourceConnectionException{
		
		LOGGER.info(">>>>>>>ReviewAnalysisService ::: Entering getFeedbackdata>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		dynamicParameters = new HashMap<>();
		staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_FEEDBACK_SNAPSHOT_FOR_REVIEW_ANALYSIS"));
		dynamicParameters.put(SP_IN_PARAM_SOURCE_ID, selectedSourceId);

		dynamicParameters.put(SP_IN_PARAM_FROM_DT, startDate+" 00:00" );
		dynamicParameters.put(SP_IN_PARAM_TO_DT, endDate+END_TIME );
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_PRIMARY_COMPANY_ID"), companyId);
		
		JdbcAnalyticsDao jdbcAD = (JdbcAnalyticsDao) FactoryDao.getInstance(JDBC_ANALYTICS_DAO);
		Result result = jdbcAD.fetchFeedbackData(staticParameters, dynamicParameters);
		LOGGER.info(">>>>>>>ReviewAnalysisService ::: getFeedbackdata ::: result >>>>>>>>" + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>ReviewAnalysisService ::: Exiting getFeedbackdata>>>>>>>>");
		return responseBuilder.build();
		
	}


	
	@POST
	@Path("/getComplaintDataForReviewAnalysis")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getComplaintData(@FormParam("selectedSourceId") int selectedSourceId,@FormParam("inStartDt") String startDate ,@FormParam("inEndDt") String endDate 
			,@FormParam("companyId") int companyId,@FormParam("inAccountId") int accountId) throws DataSourceConnectionException {
		
		LOGGER.info(">>>>>>>ReviewAnalysisService ::: Entering getComplaintData>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		dynamicParameters = new HashMap<>();
		staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_COMPLAINT_SNAPSHOT_FOR_REVIEW_ANALYSIS"));
		dynamicParameters.put(SP_IN_PARAM_SOURCE_ID, selectedSourceId);
		dynamicParameters.put(SP_IN_PARAM_FROM_DT, startDate+" 00:00" );
		dynamicParameters.put(SP_IN_PARAM_TO_DT, endDate+END_TIME );
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_PRIMARY_COMPANY_ID"), companyId);
		
		JdbcAnalyticsDao jdbcAD = (JdbcAnalyticsDao) FactoryDao.getInstance(JDBC_ANALYTICS_DAO);
		Result result = jdbcAD.fetchComplaintData(staticParameters, dynamicParameters);
		LOGGER.info(">>>>>>>ReviewAnalysisService ::: getComplaintData ::: result >>>>>>>>" + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>ReviewAnalysisService ::: Exiting getComplaintData>>>>>>>>");
		return responseBuilder.build();
		
	}
	
	
	
	@Path("/getOwnerships")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getOwnerships() throws DataSourceException {
		
		LOGGER.info(">>>>>>>CommonService ::: Entering getOwnerships>>>>>>>>" );
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
	
		staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_GET_OWNERSHIP_FOR_REVIEW_ANALYSIS"));
		
		JdbcReviewAnalysisDao jdbcCD = (JdbcReviewAnalysisDao) FactoryDao.getInstance(JDBC_REVIEW_ANALYSIS_DAO);
		Result result = jdbcCD.fetchOwnerships(staticParameters);
		LOGGER.info(">>>>>>>CommonService ::: getSources ::: result from fetchSourcesOrContexts>>>>>>>>" + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>CommonService ::: Exiting getSources>>>>>>>>");
		return responseBuilder.build();
	}
}
