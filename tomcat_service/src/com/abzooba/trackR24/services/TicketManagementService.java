package com.abzooba.trackR24.services;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import SocialResponder.SocialReplyingSystem;

import com.abzooba.trackR24.dao.DataSourceException;
import com.abzooba.trackR24.dao.TicketManagementDao;
import com.abzooba.trackR24.models.Result;
import com.abzooba.trackR24.util.PropertyHandler;
//import SocialResponder.SocialReplyingSystem;

@SuppressWarnings("unchecked")
@Path("/ticket")
public class TicketManagementService {
	private static final Logger LOGGER = Logger.getLogger(TicketManagementService.class);
	private TicketManagementDao ticketManagementDao;
	private Map<String, Object> staticParameters;
	private Map<String, Object> dynamicParameters;

	@POST
	@Path("/createTicket")
	public void create(@FormParam("assignerEmail") String assignerEmail, @FormParam("assigneeEmail") String assigneeEmail, @FormParam("priority") String priority, @FormParam("dueDate") String dueDate, @FormParam("comment") String comment, @FormParam("post_id") String posId) throws DataSourceException {

		LOGGER.info(">>>>>>>TicketManagementService ::: Creating Ticket>>>>>>>> with assignerEmail:" + assignerEmail + " assigneeEmail: " + assigneeEmail + " priority: " + priority + " dueDate " + dueDate + " comment " + comment + " posId " + posId);

		ticketManagementDao = new TicketManagementDao();
		staticParameters = new HashMap<>();
		staticParameters.put(PropertyHandler.getPropertyValue("ATTRIBUTE_SP_NAME"), PropertyHandler.getPropertyValue("SP_CREATE_UPDATE_TICKET"));
		dynamicParameters = new HashMap<>();
		Integer ticketId = null;
		String closedDate = null;
		String ticketStatus = "NEW";

		if ("".equalsIgnoreCase(assignerEmail.trim())) {
			assignerEmail = null;
		}
		if ("".equalsIgnoreCase(assigneeEmail.trim())) {
			assigneeEmail = null;
		}
		if ("".equalsIgnoreCase(comment.trim())) {
			comment = null;
		}
		if ("".equalsIgnoreCase(dueDate.trim())) {
			dueDate = null;
		}
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_TICKET_ID"), ticketId);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_POST_ID"), posId);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_ASSIGNEE_ID"), assigneeEmail);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_COMMENTS"), comment);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_TICKET_PRIORITY"), priority);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_TICKET_STATUS"), ticketStatus);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_DUE_DATE"), dueDate);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_CLOSED_DATE"), closedDate);

		ticketManagementDao.createNewTicket(staticParameters, dynamicParameters);
		LOGGER.info(">>>>>>>TicketManagementService ::: Exiting createTicket>>>>>>>>");
	}

	@POST
	@Path("/getTicketListRoutParam")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTicketListRoutParam(@FormParam("inAccountId") String inAccountId,@FormParam("inSource") String inSource,@FormParam("inStatus") String inStatus,@FormParam("inPostId") String inPostId,@FormParam("start_dt") String start_dt,@FormParam("end_dt") String end_dt) throws DataSourceException {

		LOGGER.info(">>>>>>>getTicketListRoutParam ::: Entering getTicketList with inAccountId : " + inAccountId + ">>>>>>>> ");
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder = null;
		ticketManagementDao = new TicketManagementDao();
		staticParameters = new HashMap<>();
		staticParameters.put(PropertyHandler.getPropertyValue("ATTRIBUTE_SQL"), PropertyHandler.getPropertyValue("SQL_FOR_GET_TICKET_DETAILS_LIST_ROUT_PARAM"));
		dynamicParameters = new HashMap<>();
		/*dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_ACCOUNT_ID"), inAccountId);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_SOURCE_ID"), inSource);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_TICKET_STATUS"), inStatus);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_POST_ID"), inPostId);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_FROM_DT"), start_dt);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_TO_DT"), end_dt);*/
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_POST_ID"), inPostId);
		Result result = ticketManagementDao.getTicketListRoutParam(staticParameters, dynamicParameters);
		resultObj.put("results", result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>getTicketListRoutParam ::: Exiting getTicketList>>>>>>>>");
		return responseBuilder.build();

	}

	
	@POST
	@Path("/getTicketList")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTicketList(@FormParam("inAccountId") String inAccountId,@FormParam("inSource") String inSource,@FormParam("inStatus") String inStatus,@FormParam("start_dt") String start_dt,@FormParam("end_dt") String end_dt) throws DataSourceException {

		LOGGER.info(">>>>>>>TicketManagementService ::: Entering getTicketList with inAccountId : " + inAccountId + ">>>>>>>> ");
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder = null;
		ticketManagementDao = new TicketManagementDao();
		staticParameters = new HashMap<>();
		LOGGER.info("inStatus : " + inStatus);
		if(inStatus.equalsIgnoreCase("All")){
			staticParameters.put(PropertyHandler.getPropertyValue("ATTRIBUTE_SQL"), PropertyHandler.getPropertyValue("SQL_FOR_GET_TICKET_DETAILS_LIST_ALL"));
		} else {
			staticParameters.put(PropertyHandler.getPropertyValue("ATTRIBUTE_SQL"), PropertyHandler.getPropertyValue("SQL_FOR_GET_TICKET_DETAILS_LIST"));
		}
		LOGGER.info("SQL : " + (String) staticParameters.get(PropertyHandler.getPropertyValue("ATTRIBUTE_SQL")));
		
		
		dynamicParameters = new HashMap<>();
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_ACCOUNT_ID"), inAccountId);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_SOURCE_ID"), inSource);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_TICKET_STATUS"), inStatus);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_FROM_DT"), start_dt);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_TO_DT"), end_dt);
		Result result = ticketManagementDao.getTicketList(staticParameters, dynamicParameters);
		resultObj.put("results", result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>TicketManagementService ::: Exiting getTicketList>>>>>>>>");
		return responseBuilder.build();

	}
	
	@POST
	@Path("/getTicketDetails")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTicketDetails(@FormParam("ticketID") String ticketID) throws DataSourceException {

		LOGGER.info(">>>>>>>TicketManagementService ::: Entering getTicketDetails with inAccountId : " + ticketID + ">>>>>>>> ");
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder = null;
		ticketManagementDao = new TicketManagementDao();
		staticParameters = new HashMap<>();
		staticParameters.put(PropertyHandler.getPropertyValue("ATTRIBUTE_SQL"), PropertyHandler.getPropertyValue("SQL_FOR_GET_TICKET_INDIVISUAL_DETAILS"));
		dynamicParameters = new HashMap<>();
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_TICKET_ID"), ticketID);
		Result result = ticketManagementDao.getTicketDetails(staticParameters, dynamicParameters);
		resultObj.put("results", result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>TicketManagementService ::: Exiting getTicketDetails>>>>>>>>");
		return responseBuilder.build();

	}

	@POST
	@Path("/manageTicket")
	public void manage(@FormParam("assignerEmail") String assignerEmail, @FormParam("assigneeEmail") String assigneeEmail, @FormParam("priority") String priority, @FormParam("status") String status, @FormParam("dueDate") String dueDate, @FormParam("closedDate") String closedDate, @FormParam("comment") String comment, @FormParam("ticket_id") int ticketId) throws DataSourceException {

		ticketManagementDao = new TicketManagementDao();
		staticParameters = new HashMap<>();
		staticParameters.put(PropertyHandler.getPropertyValue("ATTRIBUTE_SP_NAME"), PropertyHandler.getPropertyValue("SP_CREATE_UPDATE_TICKET"));
		dynamicParameters = new HashMap<>();

		String postId = null;

		if ("".equalsIgnoreCase(comment.trim())) {
			comment = null;
		}
		if ("".equalsIgnoreCase(dueDate.trim())) {
			dueDate = null;
		}
		if ("".equalsIgnoreCase(closedDate.trim())) {
			closedDate = null;
		}

		LOGGER.info(">>>>>>>TicketManagementService ::: Managing Ticket>>>>>>>> with assignerEmail" + assignerEmail + " assigneeEmail " + assigneeEmail + " priority " + priority + " status " + status + " due_date " + dueDate + " closedDate " + closedDate + " comment " + comment + " ticketId " + ticketId);

		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_ASSIGNER_ID"), assignerEmail);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_ASSIGNEE_ID"), assigneeEmail);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_TICKET_PRIORITY"), priority);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_TICKET_STATUS"), status);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_DUE_DATE"), dueDate);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_CLOSED_DATE"), closedDate);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_COMMENTS"), comment);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_TICKET_ID"), ticketId);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_POST_ID"), postId);

		ticketManagementDao.manageTicket(staticParameters, dynamicParameters);
		LOGGER.info(">>>>>>>TicketManagementService ::: Exiting manageTicket>>>>>>>>");
	}
	
	@SuppressWarnings("static-access")
	@POST
	@Path("/sendUserResponse")
	//@Produces(MediaType.APPLICATION_JSON)
	public String sendUserResponse(@FormParam("postID") String postID,@FormParam("sourceType") String sourceType,@FormParam("feedbackUrl") String feedbackUrl,@FormParam("msgToPost") String msgToPost,@FormParam("companyId") String companyId) {
		
		LOGGER.info("Calling SocialResponder form TicketManagement class");
		SocialReplyingSystem socialresponder;
		//socialresponder.socialResponse(sourceType, postID, msgToPost, feedbackUrl,companyId);
		LOGGER.info("Calling SocialResponder Successfull");
		return "Success";

	}
	
	
	@POST
	@Path("/sendUserFeedBackForTwitter")
	public String sendUserFeedBackForTwitter(@FormParam("postID") String strPostID,@FormParam("sourceType") String sourceType,@FormParam("feedbackUrl") String feedbackUrl,@FormParam("msgToPost") String strPostData,@FormParam("companyId") String companyId) throws DataSourceException  {
	
		String message = "";
		ticketManagementDao = new TicketManagementDao();
		Map<String, Object> tokenMap = ticketManagementDao.getTwitterUserToken();
		
		System.out.println("strPostID : " + strPostID);
		System.out.println("sourceType : " + sourceType);
		System.out.println("feedbackUrl : " + feedbackUrl);
		System.out.println("strPostData : " + strPostData);
		System.out.println("companyId : " + companyId);
		
		
		/*tokenMap.put("app_key", "");
		tokenMap.put("app_secret","");
		tokenMap.put("token", "");
		tokenMap.put("token_secret", "");*/

		//Instantiate a re-usable and thread-safe factory
		TwitterFactory twitterFactory = new TwitterFactory();

		//Instantiate a new Twitter instance
		Twitter twitter = twitterFactory.getInstance();

		//setup OAuth Consumer Credentials
		twitter.setOAuthConsumer((String) tokenMap.get("app_key"), (String) tokenMap.get("app_secret"));

		//setup OAuth Access Token
		twitter.setOAuthAccessToken(new AccessToken((String) tokenMap.get("token"), (String) tokenMap.get("token_secret")));

		String strData = null;
		String authorName = null;
		String postText = null;
		String postId = null;
		
		

		//Instantiate and initialize a new twitter status update
		TicketManagementDao ticketManagementDao2 = new TicketManagementDao();
		Map<String , String> postDetails = ticketManagementDao2.getPostDetails(strPostID,sourceType,companyId);
		postId = strPostID;
		System.out.println("====================::::::::::::" + postId);
		postText = postDetails.get("postText");
		authorName = postDetails.get("authorName");
		
		strData = strPostData;
		
		if(feedbackUrl.length() > 1){
			strData += " " + feedbackUrl;
		}
		
		LOGGER.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\nauthor_name=" + authorName + "\npost_text=" + postText + "\npost=" + postId + "\nstrData=" + strData);
		
		StatusUpdate statusUpdate = new StatusUpdate("@" + authorName + " " + strData);
		//806429288496889856
		statusUpdate.setInReplyToStatusId(Long.parseLong(postId));

		//tweet or update status
		try {
			twitter.updateStatus(statusUpdate);
		
		}catch(TwitterException e) {
			LOGGER.info(e.getErrorCode());
			LOGGER.info(e.getStatusCode());
			LOGGER.info(e.getErrorMessage());
			LOGGER.info(e.getMessage());
			
			message = e.getErrorMessage();
		}
		
	return message;
	}
	
	
}
