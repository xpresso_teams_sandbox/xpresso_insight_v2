package com.abzooba.trackR24.services;

import static com.abzooba.trackR24.util.Constants.ATTRIBUTE_SP_NAME;
import static com.abzooba.trackR24.util.Constants.ATTRIBUTE_SQL;
import static com.abzooba.trackR24.util.Constants.BLANK_STRING;
import static com.abzooba.trackR24.util.Constants.COLUMN_NAMES;
import static com.abzooba.trackR24.util.Constants.ENTITIES_FOR_WORD_CLOUD;
import static com.abzooba.trackR24.util.Constants.JDBC_ANALYTICS_DAO;
import static com.abzooba.trackR24.util.Constants.JDBC_DASHBOARD_DAO;
import static com.abzooba.trackR24.util.Constants.RESULTS;
import static com.abzooba.trackR24.util.Constants.ROWS;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_ACCOUNT_ID;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_ASPECT;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_ASPECT_NAME;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_FROM_DT;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_INTERVAL;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_PRIMARY_COMPANY_ID;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_SELECTED_DURATION;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_SENTIMENT_TYPE;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_SOURCE;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_SOURCE_ID;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_TOPIC;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_TO_DT;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.abzooba.trackR24.dao.DataSourceException;
import com.abzooba.trackR24.dao.FactoryDao;
import com.abzooba.trackR24.dao.JdbcAnalyticsDao;
import com.abzooba.trackR24.dao.JdbcDashboardDao;
import com.abzooba.trackR24.models.Result;
import com.abzooba.trackR24.models.UserInfo;
import com.abzooba.trackR24.util.PropertyHandler;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;

@SuppressWarnings("unchecked")
@Path("/dashboard")
public class DashboardService {

	private static final Logger LOGGER = Logger.getLogger(DashboardService.class);

	private Map<String, Object> staticParameters;
	private Map<String, Object> dynamicParameters;

	@POST
	@Path("/getObjectFromJson")
	@Consumes(MediaType.APPLICATION_JSON)
	public void getObjectFromJson(String userJson) throws IOException  {

		LOGGER.info(">>>>>>>DashboardService ::: Entering getObjectFromJson>>>>>>>>");
		ObjectMapper mapper = new ObjectMapper();
		UserInfo userInfo = mapper.readValue(userJson, UserInfo.class);
		LOGGER.debug(userInfo.getUsersList().get(0).getUserEmail() + "|" + userInfo.getUsersList().get(0).getUserName());
		LOGGER.info(">>>>>>>DashboardService ::: Exiting getObjectFromJson>>>>>>>>>");

	}
	
	/**
	 * 
	 * @Author Name: Ashis Nayak
	 * @Create Date: 10/24/2017
	 * @Last Modified: Urmi Saha: 27-12-2016
	 * @Input: accountId
	 * @Output: Response
	 * @Method Insight: this method calls the onload SP for Top Advocates
	 * @throws DataSourceException 
	 */
	@POST
	@Path("/getTopAdvocates")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTopAdvocates(@FormParam("accountId") int accountId) throws DataSourceException {

		LOGGER.info(">>>>>>>DashboardService ::: Entering getTopAdvocates>>>>>>>>");
		LOGGER.info(">>>>>>>DashboardService ::: getTopAdvocates ::: In Parameters >>>>>>>> accountId :" + accountId);
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_TOP_ADVOCATES"));
		dynamicParameters = new HashMap<>();
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
		JdbcDashboardDao jdbcDD = (JdbcDashboardDao) FactoryDao.getInstance(JDBC_DASHBOARD_DAO);
		Result result = jdbcDD.fetchTopAdvocates(staticParameters, dynamicParameters);
		LOGGER.info(">>>>>>>DashboardService ::: getTopAdvocates ::: result >>>>>>>>" + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>DashboardService ::: Exiting getTopAdvocates>>>>>>>>");
		return responseBuilder.build();

	}
	
	/**
	 * 
	 * @throws DataSourceException 
	 * @Author Name: Urmi Saha
	 * @Create Date: 03-11-2016
	 * @Last Modified: Ashis Nayak: 10/24/2017
	 * @Input: accountId, type, filterValue
	 * @Output: Response
	 * @Method Insight: this method calls the SPs for Daily Overall Sentimental Distribution of Cities/Sources/Contexts
	 * @throws Exception
	 */
	@POST
	@Path("/getOverallSentimentDistribution")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getOverallSentimentDistribution(@FormParam("accountId") int accountId) throws DataSourceException {

		LOGGER.info(">>>>>>>DashboardService ::: Entering getOverallSentimentDistribution>>>>>>>>");
		LOGGER.info(">>>>>>>DashboardService ::: getOverallSentimentDistribution parameters>>>>>>>> accountId: " + accountId);
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		dynamicParameters = new HashMap<>();

		staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_OVERALL_SENTIMENT_DONUT"));
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);

		JdbcDashboardDao jdbcDD = (JdbcDashboardDao) FactoryDao.getInstance(JDBC_DASHBOARD_DAO);
		Result result = jdbcDD.fetchOverallSentimentDistribution(staticParameters, dynamicParameters);
		LOGGER.info(">>>>>>>DashboardService ::: getOverallSentimentDistribution ::: result from fetchOverallSentimentDistribution>>>>>>>>" + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>DashboardService ::: Exiting getOverallSentimentDistribution>>>>>>>>");
		return responseBuilder.build();

	}


	@POST
	@Path("/getTimezoneOffset")
	public Response getTimezoneOffset(@FormParam("accountId") int accountId) throws DataSourceException{
		LOGGER.info(">>>>>>>DashboardService ::: Entering getTimezoneOffset>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_GET_TIMEZONE_OFFSET"));
		dynamicParameters = new HashMap<>();
		LOGGER.debug("accountID ::::: " + accountId);
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);

		JdbcAnalyticsDao jdbcAD = (JdbcAnalyticsDao) FactoryDao.getInstance(JDBC_ANALYTICS_DAO);
		Result result = jdbcAD.fetchSourcesOrContexts(staticParameters, dynamicParameters);
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>DashboardService ::: Exiting getTimezoneOffset>>>>>>>>" + resultObj.toJSONString());
		return responseBuilder.build();
	}

	@POST
	@Path("/getSnapshot")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSnapshot(@FormParam("accountId") int accountId, @FormParam("selectedInterval") String selectedInterval, @FormParam("type") String type) throws DataSourceException {

		LOGGER.info(">>>>>>>DashboardService ::: Entering getDailySnapshot>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		switch (type) {
			case "F":
				staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_DAILY_FEEDBACK_SNAPSHOT"));
				break;
			case "C":
				staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_DAILY_COMPLAINT_SNAPSHOT"));
				break;
			case "E":
				staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_DAILY_ENTITY_SNAPSHOT"));
				break;
			case "TC":
				staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_DAILY_TOPCOMPLAINT_SNAPSHOT"));
				break;
			default:LOGGER.info("NO SP");

		}

		dynamicParameters = new HashMap<>();
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_SELECTED_DURATION"), selectedInterval);

		JdbcDashboardDao jdbcDD = (JdbcDashboardDao) FactoryDao.getInstance(JDBC_DASHBOARD_DAO);
		Result result = jdbcDD.fetchDailySnapshot(staticParameters, dynamicParameters);
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>DashboardService ::: Exiting getSnapshot>>>>>>>>>");
		return responseBuilder.build();

	}
	
	@POST
	@Path("/getSnapshotNew")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSnapshotNew(@FormParam("accountId") int accountId, @FormParam("selectedInterval") String selectedInterval, 
			@FormParam("categories") String categories, @FormParam("from_date") String uiFromDate,  @FormParam("to_date") String uiToDate,
			@FormParam("sourceTypeId") String sourceTypeId) throws DataSourceException, java.text.ParseException{
		
		LOGGER.info(">>>>>>>DashboardService ::: Entering getDailySnapshot>>>>>>>>");
		
		List<Map<String, Object>> snapshotMetaInfoList = null;
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		Result result = null;
		JSONObject allSourcesWithValsJosnObj = null;
		JSONObject eachSnapshotJosnObj = null;
		JSONObject allSnapshotsJosnObj = null;
		JSONObject overallSnapshotJsonObj = null;
		JSONObject finalSnapshotJsonObj =  null;
		String fromDate = null;
		String toDate = null;
		int isCustomDate = 0; // if zero then  no if 1 then  yes
		
		if(selectedInterval.equals("0")) {
			isCustomDate  = 1;
		}
		
		Date date =  null;
		SimpleDateFormat formatCur = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat formatNew = new SimpleDateFormat("yyyy-MM-dd");
		
		if(uiFromDate == null ||  uiFromDate.length()  == 0){
			fromDate = "2017-01-01";
		} else{
			date = formatCur.parse(uiFromDate);
			fromDate = formatNew.format(date);
		}
		
		if(uiToDate == null ||  uiToDate.length()  == 0){
			toDate = "2017-01-01";
		} else{
			date = formatCur.parse(uiToDate);
			toDate = formatNew.format(date);
		}
        
		
		LOGGER.info("Createing Object of the dao class : " + JDBC_DASHBOARD_DAO);
		JdbcDashboardDao jdbcDD = (JdbcDashboardDao) FactoryDao.getInstance(JDBC_DASHBOARD_DAO);
		
		//  for snapshot metainf start.....
		LOGGER.info("Getting snapshot metainfo....");
		staticParameters = new HashMap<>();
		dynamicParameters = new HashMap<>();
		
		if(categories.equals("0")) {
			staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_DASHBORAD_SNAPSHOT_METAINFO_ALL"));
			dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
		} else {
			staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_DASHBORAD_SNAPSHOT_METAINFO_SPECIFIC"));
			dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, Integer.parseInt(categories));
		}
		
		snapshotMetaInfoList = jdbcDD.getSnapshotsMetaInfo(staticParameters, dynamicParameters);
		
		LOGGER.info("Snapshot info list found.");
		LOGGER.info("Number of Snapshots are ===>> " + snapshotMetaInfoList);
		allSnapshotsJosnObj = new JSONObject();
		String snapshotType = "";
		for(Map<String, Object> rowMap : snapshotMetaInfoList) {
			JSONArray reviewColNamesJsonArr =  null;
			JSONArray reviewFeedbackColValuesJsonArr = null;
			JSONArray reviewComplaintColValuesJsonArr = null;
			JSONArray reviewCommentColValuesJsonArr = null;
			String snapshotName = "";
			String snapshotIdTemp = "";
			List<String> sourceNamesList = new ArrayList<>();
			List<String> sourceTypeIdList = new ArrayList<>();
			JSONArray mostComplaintColNamesJsonArr =  null;
			JSONArray mostComplaintColValsJsonArr =  null;
			JSONArray trendingTopicColNamesJsonArr =  null;
			JSONArray trendingTopicColValsJsonArr =  null;
			eachSnapshotJosnObj = new JSONObject();
			
			for(Map.Entry<String, Object> mapEntry : rowMap.entrySet()) {
				
				if(mapEntry.getKey().equalsIgnoreCase("type")) {
					snapshotType = mapEntry.getValue().toString();
					LOGGER.info("snapshotId ==>> " + snapshotType );
				} else if(mapEntry.getKey().equalsIgnoreCase("snapshot_id")) {
					snapshotIdTemp = mapEntry.getValue().toString();
					LOGGER.info("snapshotId ==>> " + snapshotIdTemp );
				} else if(mapEntry.getKey().equalsIgnoreCase("snapshot_display_name")) {
					snapshotName = mapEntry.getValue().toString();
					LOGGER.info("snapShotName ==>> " + snapshotName );
				} else if((mapEntry.getKey().startsWith("query")) && (mapEntry.getValue() != null && String.valueOf(mapEntry.getValue()).length()  > 0)) {

						staticParameters = new HashMap<>();
						dynamicParameters = new HashMap<>();
						dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
						dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_SELECTED_DURATION_NEW"), Integer.parseInt(selectedInterval));
						dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_IS_CUSTOM_DATE"), isCustomDate);
						dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_FROM_DATE"), fromDate);
						dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_TO_DATE"), toDate);
						dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_SOURCE_TYPE_ID"), 0);
						dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_SNAPSHOT_ID"), Integer.parseInt(snapshotIdTemp));
						staticParameters.put(ATTRIBUTE_SP_NAME, mapEntry.getValue());
						result = jdbcDD.fetchDailySnapshotNew(staticParameters, dynamicParameters);
						
						if(mapEntry.getKey().equalsIgnoreCase("query_feedback")) {
							reviewColNamesJsonArr = (JSONArray)result.toJSON().get(COLUMN_NAMES);
							reviewFeedbackColValuesJsonArr = (JSONArray)result.toJSON().get(ROWS);
							for(int  i= 0; i < reviewFeedbackColValuesJsonArr.size(); i++) {
								JSONArray feedbackValuJsonArr = (JSONArray) reviewFeedbackColValuesJsonArr.get(i);
								sourceNamesList.add(String.valueOf(feedbackValuJsonArr.get(0)).toLowerCase());
								sourceTypeIdList.add(String.valueOf(feedbackValuJsonArr.get(feedbackValuJsonArr.size() - 1)));
							}
						}else if(mapEntry.getKey().equalsIgnoreCase("query_complaint")) {
							reviewComplaintColValuesJsonArr = (JSONArray)result.toJSON().get(ROWS);
						} else if(mapEntry.getKey().equalsIgnoreCase("query_comment")) {
							reviewCommentColValuesJsonArr = (JSONArray)result.toJSON().get(ROWS);
						} else if(mapEntry.getKey().equalsIgnoreCase("query_most_complaint")) {
							mostComplaintColNamesJsonArr = (JSONArray)result.toJSON().get(COLUMN_NAMES);
							mostComplaintColValsJsonArr = (JSONArray)result.toJSON().get(ROWS);
						} else if(mapEntry.getKey().equalsIgnoreCase("query_trending_topic")) {
							trendingTopicColNamesJsonArr = (JSONArray)result.toJSON().get(COLUMN_NAMES);
							trendingTopicColValsJsonArr = (JSONArray)result.toJSON().get(ROWS);
						} 
					}
		
			} 
			
			LOGGER.info("Creating json for each source....");
			allSourcesWithValsJosnObj = new JSONObject();
			for(int k = 0; k < sourceNamesList.size(); k++ ) {
				String strSource =  sourceNamesList.get(k);
				String iSourceTypeId =  sourceTypeIdList.get(k);
				JSONObject eachSourceWithValJsonObj = new JSONObject();
				JSONObject reviewsJsonObj = new JSONObject();
				JSONObject userActivityTypeJsonObj = null;  // hold info for feedback,comment, complaint etc
				JSONObject sentimentDistributionJsonObj = new JSONObject();
				
				// taking feedback data
				if(reviewFeedbackColValuesJsonArr != null)  {
					for(int  i= 0; i < reviewFeedbackColValuesJsonArr.size(); i++) {
						JSONArray feedbackValuJsonArr = (JSONArray) reviewFeedbackColValuesJsonArr.get(i);
						String tempSourceName = String.valueOf(feedbackValuJsonArr.get(0));
						String userActivityTypeName = String.valueOf(feedbackValuJsonArr.get(1));
						userActivityTypeJsonObj = new JSONObject(); 
						if(strSource.equalsIgnoreCase(tempSourceName)) {	
							for(int j = 1; j  < feedbackValuJsonArr.size() -1; j++) {
								userActivityTypeJsonObj.put(String.valueOf(reviewColNamesJsonArr.get(j)), String.valueOf(feedbackValuJsonArr.get(j)));
							}
							reviewsJsonObj.put(userActivityTypeName, userActivityTypeJsonObj);
						}
						
					}
				}
				
				
				// taking complaint data 
				if(reviewComplaintColValuesJsonArr != null) {
					for(int  i= 0; i < reviewComplaintColValuesJsonArr.size(); i++) {
						JSONArray complaintValuJsonArr = (JSONArray) reviewComplaintColValuesJsonArr.get(i);
						String tempSourceName = String.valueOf(complaintValuJsonArr.get(0));
						String userActivityTypeName = String.valueOf(complaintValuJsonArr.get(1));
						userActivityTypeJsonObj = new JSONObject();  
						if(strSource.equalsIgnoreCase(tempSourceName)) {
							for(int j = 1; j  < complaintValuJsonArr.size() -1; j++) {
								userActivityTypeJsonObj.put(String.valueOf(reviewColNamesJsonArr.get(j)), String.valueOf(complaintValuJsonArr.get(j)));
							}
							reviewsJsonObj.put(userActivityTypeName, userActivityTypeJsonObj);
						}
						
					}
				}
				
				// taking comment data 
				if(reviewCommentColValuesJsonArr != null) {
					for(int  i= 0; i < reviewCommentColValuesJsonArr.size(); i++) {
						JSONArray commentValuJsonArr = (JSONArray) reviewCommentColValuesJsonArr.get(i);
						String tempSourceName = String.valueOf(commentValuJsonArr.get(0));
						String userActivityTypeName = String.valueOf(commentValuJsonArr.get(1));
						userActivityTypeJsonObj = new JSONObject();  
						if(strSource.equalsIgnoreCase(tempSourceName)) {
							for(int j = 1; j  < commentValuJsonArr.size()-1 ; j++) {
							
								userActivityTypeJsonObj.put(String.valueOf(reviewColNamesJsonArr.get(j)), String.valueOf(commentValuJsonArr.get(j)));
							}
							reviewsJsonObj.put(userActivityTypeName, userActivityTypeJsonObj);
						}
						
					}
				}
				
				// taking most complaint data
				JSONObject mostComPlaintTrendTopicJsonObj = new JSONObject();
				if(mostComplaintColValsJsonArr != null)  {
					for(int  i= 0; i < mostComplaintColValsJsonArr.size(); i++) {
						JSONArray mostComplaintValuJsonArr = (JSONArray) mostComplaintColValsJsonArr.get(i);
						String tempSourceName = String.valueOf(mostComplaintValuJsonArr.get(0));
						if(strSource.equalsIgnoreCase(tempSourceName)) {	
							mostComPlaintTrendTopicJsonObj.put(String.valueOf(mostComplaintColNamesJsonArr.get(1)), String.valueOf(mostComplaintValuJsonArr.get(1)));
						}
						
					}
				}
				
				// setting sendtiment_distribution json objec for for final json
				// column part starts
				// taking most complaint data
				if(mostComplaintColValsJsonArr != null)  {
					for(int  i= 0; i < mostComplaintColValsJsonArr.size(); i++) {
						JSONArray mostComplaintValuJsonArr = (JSONArray) mostComplaintColValsJsonArr.get(i);
						String tempSourceName = String.valueOf(mostComplaintValuJsonArr.get(0));
						if(strSource.equalsIgnoreCase(tempSourceName)) {	
							mostComPlaintTrendTopicJsonObj.put(String.valueOf(mostComplaintColNamesJsonArr.get(1)), String.valueOf(mostComplaintValuJsonArr.get(1)));
						}
						
					}
				}
				
				// taking most trending topic data
				if(trendingTopicColValsJsonArr != null)  {
					for(int  i= 0; i < trendingTopicColValsJsonArr.size(); i++) {
						JSONArray trendingTopicValuJsonArr = (JSONArray) trendingTopicColValsJsonArr.get(i);
						String tempSourceName = String.valueOf(trendingTopicValuJsonArr.get(0));
						if(strSource.equalsIgnoreCase(tempSourceName)) {	
							mostComPlaintTrendTopicJsonObj.put(String.valueOf(trendingTopicColNamesJsonArr.get(1)), String.valueOf(trendingTopicValuJsonArr.get(1)));
						}
						
					}
				}
				// column part ends
				
				// sentiment_graph part starts
				JSONObject sentimentGraphJsonObj = new JSONObject();  // this objec values must be taken from database. for the time being its hard coded.
				sentimentGraphJsonObj.put("icon", "graph-img");
				sentimentGraphJsonObj.put("graph_url", "getSnapshotSentimentChartData");
				
				sentimentGraphJsonObj.put("graph_id", iSourceTypeId);
				sentimentDistributionJsonObj.put("sentiment_graph", sentimentGraphJsonObj);
				//sentiment_graph part ends
				
				// add mostComPlaintTrendTopicJsonObj under sentiment distribution json obj
				sentimentDistributionJsonObj.put("column", mostComPlaintTrendTopicJsonObj);
				
				// adding reviewsJsonObj under sourcejsonobject
				eachSourceWithValJsonObj.put("reviews", reviewsJsonObj);
				eachSourceWithValJsonObj.put("sentimental_distribution", sentimentDistributionJsonObj);
				LOGGER.info("eachSourceWithValJsonObj ==>> " + eachSourceWithValJsonObj);
				// adding the current source info under allSourcesWithValsJosnObj
				allSourcesWithValsJosnObj.put(strSource, eachSourceWithValJsonObj);
				
				eachSnapshotJosnObj.put("source_type_id", snapshotIdTemp);
				eachSnapshotJosnObj.put("sources", allSourcesWithValsJosnObj);
				LOGGER.info(">>>>>>>eachSnapshotJosnObj ===>>> " + eachSnapshotJosnObj);
				
				if(snapshotType != null && snapshotType.equalsIgnoreCase("overall")) {
					overallSnapshotJsonObj = new JSONObject();
					overallSnapshotJsonObj.put(snapshotName, eachSnapshotJosnObj);
				} else {
					allSnapshotsJosnObj.put(snapshotName, eachSnapshotJosnObj);
				}
			}
		}
		
		LOGGER.info(">>>>>>>allSnapshotsJosnObj ===>>> " + overallSnapshotJsonObj);
		LOGGER.info(">>>>>>>allSnapshotsJosnObj ===>>> " + allSnapshotsJosnObj);
		LOGGER.info(">>>>>>>merging both  overallSnapshotJsonObj and allSnapshotsJosnObj into a single jsonobject ");
		finalSnapshotJsonObj = new JSONObject();
		finalSnapshotJsonObj.put("overall_snapshots", overallSnapshotJsonObj);
		finalSnapshotJsonObj.put("all_snapshots", allSnapshotsJosnObj);
		LOGGER.info(">>>>>>>finalSnapshotJsonObjafter merged ===>>> " + finalSnapshotJsonObj);
		resultObj.put(RESULTS, finalSnapshotJsonObj.toJSONString());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>DashboardService ::: Exiting getDailySnapshot>>>>>>>>>");
		return responseBuilder.build();

	}

	@POST
	@Path("/getViralityComparison")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getViralityComparison(@FormParam("accountId") int accountId, @FormParam("startDt") String startDt, @FormParam("endDt") String endDt, @FormParam("hR") int hr, @FormParam("selectedInterval") String selectedInterval) throws DataSourceException {

		LOGGER.info(">>>>>>>DashboardService ::: Entering getViralityComparison>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		if (BLANK_STRING.equalsIgnoreCase(selectedInterval.trim())) {
			staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_VIRALITY_COMPARISON_ONLOAD"));
		} else {
			staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_VIRALITY_COMPARISON"));
		}
		dynamicParameters = new HashMap<>();
		Integer timeInterval = null;
		if ("".equalsIgnoreCase(startDt.trim())) {
			startDt = null;
		}
		if ("".equalsIgnoreCase(endDt.trim())) {
			endDt = null;
		}
		if ("".equalsIgnoreCase(selectedInterval.trim())) {
			selectedInterval = null;
		}
		if (hr != -1) {
			timeInterval = Integer.valueOf(hr);
		}
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
		dynamicParameters.put(SP_IN_PARAM_FROM_DT, startDt);
		dynamicParameters.put(SP_IN_PARAM_TO_DT, endDt);
		dynamicParameters.put(SP_IN_PARAM_INTERVAL, timeInterval);
		dynamicParameters.put(SP_IN_PARAM_SELECTED_DURATION, selectedInterval);

		JdbcDashboardDao jdbcDD = (JdbcDashboardDao) FactoryDao.getInstance(JDBC_DASHBOARD_DAO);
		Result result = jdbcDD.fetchContextSentimentDetails(staticParameters, dynamicParameters);
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>DashboardService ::: Exiting getViralityComparison>>>>>>>>");
		return responseBuilder.build();

	}

	@POST
	@Path("/getContextwiseSentimentDistribution")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getContextwiseSentimentDistribution(@FormParam("account_Id") int accountId, @FormParam("sentiment") String sentiment, @FormParam("company_Id") String companyId) throws DataSourceException{

		LOGGER.info(">>>>>>>DashboardService ::: Entering getContextwiseSentimentDistribution>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_CONTEXTWISE_SENTIMENT_DISTRIBUTION"));
		dynamicParameters = new HashMap<>();
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
		dynamicParameters.put(SP_IN_PARAM_SENTIMENT_TYPE, sentiment);
		dynamicParameters.put(SP_IN_PARAM_PRIMARY_COMPANY_ID, companyId);
		

		JdbcDashboardDao jdbcDD = (JdbcDashboardDao) FactoryDao.getInstance(JDBC_DASHBOARD_DAO);
		Result result = jdbcDD.fetchContextwiseSentimentDistribution(staticParameters, dynamicParameters);
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>DashboardService ::: Exiting getContextwiseSentimentDistribution>>>>>>>>>");
		return responseBuilder.build();

	}
	
	@POST
	@Path("/getBrandComparisonMonthly")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getBrandComparisonMonthly(@FormParam("account_Id") int accountId, @FormParam("topic") int topic, @FormParam("company_Id") String companyId) throws DataSourceException {

		LOGGER.info(">>>>>>>DashboardService ::: Entering getBrandComparisonMonthly>>>>>>>>");
		LOGGER.info(">>>>>>>DashboardService ::: getTopAdvocates ::: In Parameters >>>>>>>> companyId :" + companyId + " accountId : " + accountId);
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_BRAND_COMPARISON_MONTHLY"));
		dynamicParameters = new HashMap<>();
		Integer aspectId = topic == -1 ? null : Integer.valueOf(topic);
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_ASPECT"), aspectId);
		dynamicParameters.put(SP_IN_PARAM_PRIMARY_COMPANY_ID, companyId);

		JdbcDashboardDao jdbcDD = (JdbcDashboardDao) FactoryDao.getInstance(JDBC_DASHBOARD_DAO);
		Result result = jdbcDD.fetchCompetitiveHappinessIndex(staticParameters, dynamicParameters);
		LOGGER.info(">>>>>>>DashboardService ::: getBrandComparisonMonthly ::: result >>>>>>>>" + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>DashboardService ::: Exiting getBrandComparisonMonthly>>>>>>>>>");
		return responseBuilder.build();

	}


	@POST
	@Path("/getContextwiseSentimentDistributionPercentile")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getContextwiseSentimentDistributionPercentile(@FormParam("account_Id") int accountId, @FormParam("sentiment") String sentiment, @FormParam("company_Id") String companyId) throws DataSourceException {

		LOGGER.info(">>>>>>>DashboardService ::: Entering getContextwiseSentimentDistribution>>>>>>>>" + companyId);
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_CONTEXTWISE_SENTIMENT_DISTRIBUTION_PERCENTILE"));
		dynamicParameters = new HashMap<>();
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_SENTIMENT_TYPE"), sentiment);
		dynamicParameters.put(SP_IN_PARAM_PRIMARY_COMPANY_ID, companyId);
		
		JdbcDashboardDao jdbcDD = (JdbcDashboardDao) FactoryDao.getInstance(JDBC_DASHBOARD_DAO);
		Result result = jdbcDD.fetchContextwiseSentimentDistribution(staticParameters, dynamicParameters);
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>DashboardService ::: Exiting getContextwiseSentimentDistribution>>>>>>>>>");
		return responseBuilder.build();

	}
	
	@POST
	@Path("/getSnapshotDropdown")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSnapshotDropdown(@FormParam("accountId") int accountId) throws DataSourceException, ParseException {
		
		LOGGER.info(">>>>>>>DashboardService ::: getSnapshotDropdown() starts>>>>>>>>");
		JSONObject finalFilterDropdownJsonObj = new JSONObject();
		JSONArray allDropdownOptionsJsonArr = new JSONArray();
		JSONObject eachDropdownOptionsJsonObj;
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		List<Map<String, Object>> snapshotMetaInfoList = null;
		staticParameters = new HashMap<>();
		staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_DASHBOARD_SNAPSHOT_DROPDOWN"));
		dynamicParameters = new HashMap<>();
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
		JdbcDashboardDao jdbcDD = (JdbcDashboardDao) FactoryDao.getInstance(JDBC_DASHBOARD_DAO);
		LOGGER.info(">>>>>>>DashboardService ::: getting snapshot id name from db>>>>>>>>");
		snapshotMetaInfoList = jdbcDD.getSnapshotsMetaInfo(staticParameters, dynamicParameters);  // calling getSnapshotsMetaInfo() because  same type of output needed.here we need snapshotid  and only.
		LOGGER.info(">>>>>>>DashboardService ::: snapshot id name found from db.>>>>>>>>");
		LOGGER.info(">>>>>>>DashboardService ::: snapshotMetaInfoList size>>>>>>>> " + snapshotMetaInfoList);
		
		if(snapshotMetaInfoList != null) {
			for(Map<String, Object> rowMap : snapshotMetaInfoList) {
				eachDropdownOptionsJsonObj = new JSONObject();
				for(Map.Entry<String, Object> mapEntry : rowMap.entrySet()) {
					String key = mapEntry.getKey();
					String value  = String.valueOf(mapEntry.getValue());
					if(key.equalsIgnoreCase("value")) {
						eachDropdownOptionsJsonObj.put(key, Integer.parseInt(value));
					} else {
						eachDropdownOptionsJsonObj.put(key, value );
					}
					LOGGER.info(">>>>>>>DashboardService ::: eachDropdownOptionsJsonObj >>>>>>>> " + eachDropdownOptionsJsonObj);
				}
				allDropdownOptionsJsonArr.add(eachDropdownOptionsJsonObj);
			}
		}
		
		LOGGER.info(">>>>>>>DashboardService ::: preparing for allFiltersJsonArr>>>>>>>>");
		
		String strAllFiltersInterval = "[{\"name\": \"daily\",\"value\": 1,\"isCustom\": false}, {\"name\": \"weekly\",\"value\": 2,\"isCustom\": false}, {\"name\": \"monthly\",\"value\": 3,\"isCustom\": false}, {\"name\": \"quarterly\",\"value\": 4,\"isCustom\": false}, {\"name\": \"yearly\",\"value\": 5,\"isCustom\": false}, {\"name\": \"custom\",\"value\": 0,\"isCustom\": true}]";

		Object object=null;
		JSONArray intervalsJsonArr=null;
		JSONParser jsonParser=new JSONParser();
		object=jsonParser.parse(strAllFiltersInterval);
		intervalsJsonArr=(JSONArray) object;
		LOGGER.info("interval josn array :: "+intervalsJsonArr);
		
		finalFilterDropdownJsonObj.put("filters", intervalsJsonArr);
		finalFilterDropdownJsonObj.put("dropdown_options", allDropdownOptionsJsonArr);
		
		LOGGER.info("finalFilterDropdownJsonObj :: "+finalFilterDropdownJsonObj);
		resultObj.put(RESULTS, finalFilterDropdownJsonObj.toJSONString());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>DashboardService ::: Exiting getSnapshotDropdown>>>>>>>>>");
		return responseBuilder.build();

	}
	
	@POST
	@Path("/getSnapshotSentimentChartData")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSnapshotSentimentChartData(@FormParam("accountId") int accountId, @FormParam("selectedInterval") String selectedInterval, 
			@FormParam("categories") String categories, @FormParam("from_date") String uiFromDate,  @FormParam("to_date") String uiToDate,@FormParam("snapshotId") int snapshotId, @FormParam("sourceTypeId") int sourceTypeId) throws java.text.ParseException, DataSourceException{
		
		LOGGER.info(">>>>>>>DashboardService ::: getSnapshotSentimentChartData() starts>>>>>>>>");
		
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		Result result = null;
		String fromDate = null;
		String toDate = null;
		JSONArray colNamesJsonArr = null;
		JSONArray colValsJsonArr = null;
		JSONObject  finalJsonObject = new JSONObject();
		String prevSentimentVal = "";	
		String curSentimentVal = "";
		String prevCompanyName = "";	
		String curCompanyName = "";
		JSONObject sentimentJsonObj = null;
		JSONObject chartJsonObj = null;
		JSONObject jsonJsonObj = null;
		JSONArray catgJsonArr = null;
		JSONArray dataJsonArr = null;
		JSONObject eachCompanyDataJsonObj = null;
		JSONArray  subDataJsonArr = null;
		boolean isAllaspectsFound = false;
		boolean isSentimeentChanged = false;
		JSONArray tempDefualtZeroValJsonArr = new JSONArray();
		boolean istempDefualtZeroValJsonArrValLoaded = false;
		
		int isCustomDate = 0; // if zero then  no if 1 then  yes
		
		if(selectedInterval.equals("0")) {
			isCustomDate  = 1;
		}
		
		Date date =  null;
		SimpleDateFormat formatCur = new SimpleDateFormat("dd-MM-yyyy");
		SimpleDateFormat formatNew = new SimpleDateFormat("yyyy-MM-dd");
		
		if(uiFromDate == null ||  uiFromDate.length()  == 0){
			LOGGER.info("Setting 2017-01-01 as defaults fromDate. it will not be  used if selectedInterval =0 ");
			fromDate = "2017-01-01";
		} else{
			date = formatCur.parse(uiFromDate);
			fromDate = formatNew.format(date);
		}
		
		if(uiToDate == null ||  uiToDate.length()  == 0){
			LOGGER.info("Setting 2017-01-01 as defaults toDate. it will not be  used if selectedInterval =0 ");
			toDate = "2017-01-01";
		} else{
			date = formatCur.parse(uiToDate);
			toDate = formatNew.format(date);
		}
        
        LOGGER.info("Parametrs for SP are :  accountId= " + accountId + " & selectedInterval = " + selectedInterval + " & isCustomDate = " + isCustomDate +
        		" & fromDate = " + fromDate + " & toDate = " + toDate + " & snapshotId = " + snapshotId + " & sourceTypeId = " + sourceTypeId);
		
		LOGGER.info("Createing Object of the dao class : " + JDBC_DASHBOARD_DAO);
		JdbcDashboardDao jdbcDD = (JdbcDashboardDao) FactoryDao.getInstance(JDBC_DASHBOARD_DAO);
		
		staticParameters = new HashMap<>();
		dynamicParameters = new HashMap<>();
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_SELECTED_DURATION_NEW"), Integer.parseInt(selectedInterval));
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_IS_CUSTOM_DATE"), isCustomDate);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_FROM_DATE"), fromDate);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_TO_DATE"), toDate);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_SNAPSHOT_ID"), snapshotId);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_SOURCE_TYPE_ID"), sourceTypeId);
		
		staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_SNAPSHOT_SENTIMENT_CHART"));
		LOGGER.info("Calling SP.....");
		result = jdbcDD.fetchDailySnapshotNew(staticParameters, dynamicParameters);
		
		colNamesJsonArr = (JSONArray)result.toJSON().get(COLUMN_NAMES);
		colValsJsonArr = (JSONArray)result.toJSON().get(ROWS);
		
		if(colValsJsonArr != null && colValsJsonArr.isEmpty()) {
			LOGGER.info("SP resultset is not empty");
			for(int j= 0; j < colValsJsonArr.size(); j++) {
				JSONArray eachRowJsonArr = (JSONArray)colValsJsonArr.get(j);
				for(int i = 0; i < colNamesJsonArr.size(); i++) {
					String colName = (String)colNamesJsonArr.get(i);
					
					if(colName.equalsIgnoreCase("sentiment")) {
						curSentimentVal = (String)eachRowJsonArr.get(i);
						if(!curSentimentVal.equalsIgnoreCase(prevSentimentVal) || (j == colValsJsonArr.size() - 1)) {
							
							if(prevSentimentVal.length() > 0) {
								// preparing for chart json object
								JSONObject tolltipJsonObj = new JSONObject();
								
								JSONArray colorJsonArr = new JSONArray();
								colorJsonArr.add("#3EBEC1");
								colorJsonArr.add("#8edee0");
								
								
								if(chartJsonObj!=null) {
								chartJsonObj.put("type", "bar");
								chartJsonObj.put("y_label", "Sentiment Distribution( " + prevSentimentVal.toUpperCase() + " )");
								chartJsonObj.put("x_label", "");
								chartJsonObj.put("tool_tip_text", "");
								chartJsonObj.put("tooltipFormat", tolltipJsonObj);
								chartJsonObj.put("colors", colorJsonArr);
								chartJsonObj.put("legend", true);
								}
								
								isSentimeentChanged = true;
								
							} else {
								isSentimeentChanged = false;
							}
							
							if(prevSentimentVal.length() == 0) {
								sentimentJsonObj = new JSONObject();
								chartJsonObj =  new JSONObject();
								jsonJsonObj = new JSONObject();
								catgJsonArr = new JSONArray();
								dataJsonArr  = new JSONArray();
								prevSentimentVal = curSentimentVal;
							}
							
						}
					} else if(colName.equalsIgnoreCase("company_name")) {
						curCompanyName = (String)eachRowJsonArr.get(i);
						if(!curCompanyName.equalsIgnoreCase(prevCompanyName) || isSentimeentChanged) {
							if(prevCompanyName.length() > 0) {
								isAllaspectsFound = true;
								eachCompanyDataJsonObj = new JSONObject();
								eachCompanyDataJsonObj.put("name", prevCompanyName);
								// handle no data if all values of subDataJsonArr are 0
								if(isAllaspectsFound && !istempDefualtZeroValJsonArrValLoaded){
										for(int k =  0; k < catgJsonArr.size(); k++){
											// values from db comes as bigdecimal . if type changes then must change type here also
											tempDefualtZeroValJsonArr.add(new BigDecimal(0)); 
										}
										istempDefualtZeroValJsonArrValLoaded = true;
									
								}
								
								if(tempDefualtZeroValJsonArr.containsAll(subDataJsonArr)) {
									eachCompanyDataJsonObj.put("data", new JsonArray());
								} else {
									eachCompanyDataJsonObj.put("data", subDataJsonArr);
								}
								
								dataJsonArr.add(eachCompanyDataJsonObj);
								
							}  
							
							prevCompanyName = curCompanyName;
							subDataJsonArr = new JSONArray();
						}
						
					} else if(colName.equalsIgnoreCase("aspect")) {
						if(!isAllaspectsFound) {
							String aspectName = (String)eachRowJsonArr.get(i);
							if(catgJsonArr!=null) {
							catgJsonArr.add(aspectName);
							}
						} 
					} else if(colName.equalsIgnoreCase("review_count")) {
						BigDecimal counts =  (BigDecimal)eachRowJsonArr.get(i);
						if(subDataJsonArr!=null) {
						subDataJsonArr.add(counts);
						}
						
						if(isSentimeentChanged) {
							jsonJsonObj.put("categories", catgJsonArr);
							jsonJsonObj.put("data", dataJsonArr);
							chartJsonObj.put("json", jsonJsonObj);
							sentimentJsonObj.put("charts", chartJsonObj);
							finalJsonObject.put(prevSentimentVal.toLowerCase(), sentimentJsonObj);
							
							sentimentJsonObj = new JSONObject();
							chartJsonObj =  new JSONObject();
							jsonJsonObj = new JSONObject();
							dataJsonArr  = new JSONArray();
							prevSentimentVal = curSentimentVal;
							
							isSentimeentChanged = false;
						}
					}
				}
				
			}
		} else {
			LOGGER.info("SP resultset is Empty");
		}
			
		
		
		LOGGER.info(">>>>>>>finalJsonObject  ===>>> " + finalJsonObject);
		resultObj.put(RESULTS, finalJsonObject.toJSONString());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>DashboardService ::: Exiting getDailySnapshot>>>>>>>>>");
		return responseBuilder.build();
	}
	
	
	@POST
	@Path("/getOverallSentimentDistributionData")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getOverallSentimentDistributionData(@FormParam("accountId") int accountId,@FormParam("selectedSource") String selectedSource,@FormParam("selectedTopics") String selectedTopics,@FormParam("startDate") String startDate, @FormParam("endDate") String endDate) throws DataSourceException {

		LOGGER.info(">>>>>>>DashboardService ::: Entering getFilterOverallSentimentDistribution>>>>>>>>");
		JSONObject resultObj = new JSONObject() ;
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		dynamicParameters = new HashMap<>();

		staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_DASHBOARD_OVERALL_DISTRIBUTION"));
		
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
		dynamicParameters.put(SP_IN_PARAM_SOURCE, selectedSource);
		dynamicParameters.put(SP_IN_PARAM_TOPIC, selectedTopics);
		dynamicParameters.put(SP_IN_PARAM_FROM_DT, startDate);
		dynamicParameters.put(SP_IN_PARAM_TO_DT, endDate);
		
		
		JdbcDashboardDao jdbcDD = (JdbcDashboardDao) FactoryDao.getInstance(JDBC_DASHBOARD_DAO);
		Result result = jdbcDD.fetchFilteredOverallSentimentDistributionForDashboard(staticParameters, dynamicParameters);
		LOGGER.info(">>>>>>>DashboardService ::: getFilterOverallSentimentDistribution ::: result from fetchFilteredOverallSentimentDistribution>>>>>>>>" + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>DashboardService ::: Exiting getFilterOverallSentimentDistribution>>>>>>>>"+resultObj);
		return responseBuilder.build();

	}
	
	
	@POST
	@Path("/getOverallSentimentDistributionDataDayWise")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getOverallSentimentDistributionDataDayWise(@FormParam("accountId") int accountId,@FormParam("selectedSource") String selectedSource,@FormParam("startDate") String startDate, @FormParam("endDate") String endDate) throws DataSourceException {

		LOGGER.info(">>>>>>>DashboardService ::: Entering getOverallSentimentDistributionDataDayWise>>>>>>>>");
		JSONObject resultObj = new JSONObject() ;
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		dynamicParameters = new HashMap<>();

		staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_DASHBOARD_OVERALL_SENTIMENT_DAYWISE"));
		
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
		dynamicParameters.put(SP_IN_PARAM_SOURCE, selectedSource);
		dynamicParameters.put(SP_IN_PARAM_FROM_DT, startDate);
		dynamicParameters.put(SP_IN_PARAM_TO_DT, endDate);
		
		
		JdbcDashboardDao jdbcDD = (JdbcDashboardDao) FactoryDao.getInstance(JDBC_DASHBOARD_DAO);
		Result result = jdbcDD.fetchOverallSentimentDistributionDataDayWise(staticParameters, dynamicParameters);
		LOGGER.info(">>>>>>>DashboardService ::: getFilterOverallSentimentDistribution ::: result from fetchFilteredOverallSentimentDistribution>>>>>>>>" + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>DashboardService ::: Exiting getFilterOverallSentimentDistribution>>>>>>>>"+resultObj);
		return responseBuilder.build();

	}
	
	@POST
	@Path("/getAspectWiseDistributionForForDashboard")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAspectWiseDistributionForForDashboard(@FormParam("accountId") int accountId,@FormParam("selectedSource") int selectedSource,@FormParam("selectedTopics") String selectedTopics,@FormParam("startDate") String startDate, @FormParam("endDate") String endDate) throws DataSourceException {

		LOGGER.info(">>>>>>>DashboardService ::: Entering getAspectWiseDistributionForForDashboard>>>>>>>>" + selectedSource);
		JSONObject resultObj = new JSONObject() ;
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		dynamicParameters = new HashMap<>();

		staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_ASPECT_WISE_DISTRIBUTION_DASHBOARD"));
		
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
		dynamicParameters.put(SP_IN_PARAM_SOURCE_ID, selectedSource);
		dynamicParameters.put(SP_IN_PARAM_TOPIC, selectedTopics);
		dynamicParameters.put(SP_IN_PARAM_FROM_DT, startDate);
		dynamicParameters.put(SP_IN_PARAM_TO_DT, endDate);
		
		
		JdbcDashboardDao jdbcDD = (JdbcDashboardDao) FactoryDao.getInstance(JDBC_DASHBOARD_DAO);
		Result result = jdbcDD.fetchAspectWiseDistributionForForDashboard(staticParameters, dynamicParameters);
		LOGGER.info(">>>>>>>DashboardService ::: getAspectWiseDistributionForForDashboard ::: result >>>>>>>>" + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>DashboardService ::: Exiting fetchAspectWiseDistributionForForDashboard>>>>>>>>"+resultObj);
		return responseBuilder.build();

	}	
	
	@POST
	@Path("/getSourceAspectWiseSentimentDistribution")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSourceAspectWiseSentimentDistribution(@FormParam("accountId") int accountId,@FormParam("selectedSource") int selectedSource,@FormParam("selectedTopics") String selectedTopics,@FormParam("aspect") String aspectName,@FormParam("startDate") String startDate, @FormParam("endDate") String endDate) throws DataSourceException {

		LOGGER.info(">>>>>>>DashboardService ::: Entering getSourceAspectWiseSentimentDistribution>>>>>>>>" + selectedSource);
		JSONObject resultObj = new JSONObject() ;
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		dynamicParameters = new HashMap<>();

		staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_SOURCE_ASPECT_WISE_SENTIMENT_DISTRIBUTION_DASHBOARD"));
		
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
		dynamicParameters.put(SP_IN_PARAM_SOURCE_ID, selectedSource);
		dynamicParameters.put(SP_IN_PARAM_ASPECT_NAME, aspectName);
		dynamicParameters.put(SP_IN_PARAM_FROM_DT, startDate);
		dynamicParameters.put(SP_IN_PARAM_TO_DT, endDate);
		
		
		JdbcDashboardDao jdbcDD = (JdbcDashboardDao) FactoryDao.getInstance(JDBC_DASHBOARD_DAO);
		Result result = jdbcDD.fetchSourceAspectWiseSentimentDistribution(staticParameters, dynamicParameters);
		LOGGER.info(">>>>>>>DashboardService ::: getSourceAspectWiseSentimentDistribution ::: result >>>>>>>>" + result.toJSON());
		
		resultObj.put(RESULTS, result.toJSON());
		
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>DashboardService ::: Exiting getSourceAspectWiseSentimentDistribution>>>>>>>>"+resultObj);
		return responseBuilder.build();

	}	

	@POST
	@Path("/getSourceAspectWiseSentimenTopEntity")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSourceAspectWiseSentimenTopEntity(@FormParam("accountId") int accountId,@FormParam("selectedSource") int selectedSource,@FormParam("selectedTopics") String selectedTopics,@FormParam("startDate") String startDate, @FormParam("endDate") String endDate,@FormParam("aspect") String strAspectName) throws DataSourceException {

		LOGGER.info(">>>>>>>DashboardService ::: Entering getSourceAspectWiseSentimenTopEntity>>>>>>>>" + selectedSource);
		JSONObject resultObj = new JSONObject() ;
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		dynamicParameters = new HashMap<>();

		staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_SOURCE_ASPECT_WISE_SENTIMENT_DISTRIBUTION_ENTITY_DASHBOARD"));
		
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
		dynamicParameters.put(SP_IN_PARAM_SOURCE_ID, selectedSource);
		dynamicParameters.put(SP_IN_PARAM_TOPIC, selectedTopics);
		dynamicParameters.put(SP_IN_PARAM_FROM_DT, startDate);
		dynamicParameters.put(SP_IN_PARAM_TO_DT, endDate);
		dynamicParameters.put(SP_IN_PARAM_ASPECT, strAspectName);
		
		
		
		JdbcDashboardDao jdbcDD = (JdbcDashboardDao) FactoryDao.getInstance(JDBC_DASHBOARD_DAO);
		Result result = jdbcDD.fetchSourceAspectWiseSentimentTopEntity(staticParameters, dynamicParameters);
		LOGGER.info(">>>>>>>DashboardService ::: getSourceAspectWiseSentimenTopEntity ::: result >>>>>>>>" + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>DashboardService ::: Exiting getSourceAspectWiseSentimenTopEntity>>>>>>>>"+resultObj);
		return responseBuilder.build();

	}
	
	@POST
	@Path("/getSourceAspectWiseSentimenTopFeedback")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSourceAspectWiseSentimenTopFeedback(@FormParam("accountId") int accountId,@FormParam("selectedSource") int selectedSource,@FormParam("selectedTopics") String selectedTopics,@FormParam("startDate") String startDate, @FormParam("endDate") String endDate,@FormParam("aspect") String strAspectName) throws DataSourceException, IOException, ParseException {

		LOGGER.info(">>>>>>>DashboardService ::: Entering getSourceAspectWiseSentimenTopFeedback>>>>>>>>" + selectedSource);
		JSONObject resultObj = new JSONObject() ;
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		dynamicParameters = new HashMap<>();

		staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_SOURCE_ASPECT_WISE_SENTIMENT_DISTRIBUTION_FEEDBACK_DASHBOARD"));
		
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
		dynamicParameters.put(SP_IN_PARAM_SOURCE_ID, selectedSource);
		dynamicParameters.put(SP_IN_PARAM_TOPIC, selectedTopics);
		dynamicParameters.put(SP_IN_PARAM_FROM_DT, startDate);
		dynamicParameters.put(SP_IN_PARAM_TO_DT, endDate);
		dynamicParameters.put(SP_IN_PARAM_ASPECT, strAspectName);
		
		
		
		JdbcDashboardDao jdbcDD = (JdbcDashboardDao) FactoryDao.getInstance(JDBC_DASHBOARD_DAO);
		Result result = jdbcDD.fetchSourceAspectWiseSentimentTopFeedback(staticParameters, dynamicParameters);
		LOGGER.info(">>>>>>>DashboardService ::: getSourceAspectWiseSentimenTopFeedback ::: result >>>>>>>>" + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>DashboardService ::: Exiting getSourceAspectWiseSentimenTopFeedback>>>>>>>>"+resultObj);
		return responseBuilder.build();

	}
	@POST
	@Path("/getAspectDistributionData")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAspectDistributionData(@FormParam("accountId") int accountId,@FormParam("startDate") String startDate, 
			@FormParam("endDate") String endDate) throws DataSourceException {

		LOGGER.info(">>>>>>>DashboardService ::: Entering getAspectDistributionData>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder;
		staticParameters = new HashMap<>();
		dynamicParameters = new HashMap<>();

		staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_DASHBOARD_OVERALL_DISTRIBUTION"));
		
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
		dynamicParameters.put(SP_IN_PARAM_FROM_DT, startDate);
		dynamicParameters.put(SP_IN_PARAM_TO_DT, endDate);
		
		JdbcDashboardDao jdbcDD = (JdbcDashboardDao) FactoryDao.getInstance(JDBC_DASHBOARD_DAO);
		Result result = jdbcDD.fetchFilteredOverallSentimentDistribution(staticParameters, dynamicParameters);
		LOGGER.info(">>>>>>>DashboardService ::: getAspectDistributionData ::: result from fetchFilteredOverallSentimentDistribution>>>>>>>>" + result.toJSON());
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>DashboardService ::: Exiting getAspectDistributionData>>>>>>>>"+resultObj.toJSONString());
		return responseBuilder.build();

	}
	
	
	@POST
	@Path("/getWordCloudData")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getWordCloudData(@FormParam("accountId") int accountId,  @FormParam("sourceId") int sourceId, @FormParam("startDate") String startDate, @FormParam("endDate") String endDate, @FormParam("sentiment") String sentiment) throws DataSourceException,IOException{
		LOGGER.info(">>>>>>>DashboardService ::: Entering getWordCloudData>>>>>>>>");
		ResponseBuilder responseBuilder;
		List<Result> resultListObj = new ArrayList<>();
		ObjectMapper mapper = new ObjectMapper();
		//Result aspectResultObj1 = fetchWordCloudData(accountId, ASPECTS_FOR_WORD_CLOUD, null, sourceId, startDate, endDate, sentiment);
		Result aspectResultObj2 = fetchWordCloudData(accountId, ENTITIES_FOR_WORD_CLOUD, null, sourceId, startDate, endDate, sentiment);
		//resultListObj.add(aspectResultObj1);
		resultListObj.add(aspectResultObj2);
		String wordCloudJson = mapper.writeValueAsString(resultListObj);
		responseBuilder = Response.ok(wordCloudJson);
		LOGGER.info(">>>>>>>DashboardService ::: Exiting getWordCloudData>>>>>>>>");
		return responseBuilder.build();
	}
	
	
	public Result fetchWordCloudData(int accountId, String aspectOrEntityFlag, String selectedContext, int sourceId, String startDt, String endDt, String sentiment) throws DataSourceException {
		LOGGER.info(">>>>>>>DashboardService ::: Entering fetchWordCloudData>>>>>>>>");
		staticParameters = new HashMap<>();
		//staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_WORD_CLOUD_ENTITY"));
		staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_DASHBOARD_WORDCLOUD_ENTITY"));
		/*if (aspectOrEntityFlag.equalsIgnoreCase(PropertyHandler.getPropertyValue("ASPECTS_FOR_WORD_CLOUD"))) {
			staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_WORD_CLOUD_ASPECT"));
		} else {
			staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_WORD_CLOUD_ENTITY"));
		}*/
		dynamicParameters = new HashMap<>();
		Integer sourceID = sourceId == -1 ? null : Integer.valueOf(sourceId);
		dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
		dynamicParameters.put(SP_IN_PARAM_SOURCE, sourceID);
		dynamicParameters.put(SP_IN_PARAM_FROM_DT, ((startDt == null) || (startDt.trim() == "") ? null : startDt));
		dynamicParameters.put(SP_IN_PARAM_TO_DT, ((endDt == null) || (endDt.trim() == "") ? null : endDt));
		dynamicParameters.put(SP_IN_PARAM_SENTIMENT_TYPE, ((sentiment == null) || ("".equalsIgnoreCase(sentiment.trim())) ? null : sentiment));
		if (aspectOrEntityFlag.equalsIgnoreCase(ENTITIES_FOR_WORD_CLOUD)) {
			dynamicParameters.put(SP_IN_PARAM_TOPIC, ((selectedContext == null) || ("".equalsIgnoreCase(selectedContext.trim())) ? null : selectedContext));
		}
		JdbcDashboardDao jdbcAD = (JdbcDashboardDao) FactoryDao.getInstance(JDBC_DASHBOARD_DAO);
		Result result = jdbcAD.fetchEntitiesForWorCloud(staticParameters, dynamicParameters);
		LOGGER.info(">>>>>>>DashboardService ::: Exiting fetchWordCloudData>>>>>>>>");
		return result;
	}
	
	@POST
	@Path("/getSentimentDistributionForAspect")
	@Produces(MediaType.APPLICATION_JSON)
	public Response fetchSentimentDistributionForAspect(@FormParam("accountId") int accountId, @FormParam("selectedSource") int sourceId ,
			@FormParam("selectedTopics") int aspectId,
			@FormParam("startDate")  String startDt, @FormParam("endDate")  String endDt) throws DataSourceException {
		
			LOGGER.info(">>>>>>>DashboardService ::: Entering getSentimentDistributionForAspect>>>>>>>>");
			JSONObject resultObj = new JSONObject();
			
			ResponseBuilder responseBuilder;
			staticParameters = new HashMap<>();
			dynamicParameters = new HashMap<>();
			
			dynamicParameters.put(SP_IN_PARAM_ASPECT, aspectId);
			dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
			dynamicParameters.put(SP_IN_PARAM_SOURCE, sourceId);
			dynamicParameters.put(SP_IN_PARAM_FROM_DT, ((startDt == null) || (startDt.trim() == "") ? null : startDt));
			dynamicParameters.put(SP_IN_PARAM_TO_DT, ((endDt == null) || (endDt.trim() == "") ? null : endDt));
			staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_DASHBOARD_SENTIMENT_DISTRIBUTION_FOR_ASPECT"));
			
			JdbcDashboardDao jdbcDD = (JdbcDashboardDao) FactoryDao.getInstance(JDBC_DASHBOARD_DAO);
			Result result = jdbcDD.fetchEntityAndSentimentForAspect(staticParameters, dynamicParameters);
			
			
			resultObj.put(RESULTS, result.toJSON());
			responseBuilder = Response.ok(resultObj.toJSONString());
			LOGGER.info(">>>>>>>DashboardService ::: Exiting getSentimentDistributionForAspect>>>>>>>>");
			return responseBuilder.build();

		}
	
	
	@POST
	@Path("/getEntitiesForAspect")
	@Produces(MediaType.APPLICATION_JSON)
	public Response fetchEntitiesForAspect(@FormParam("accountId") int accountId, @FormParam("selectedSource") int sourceId ,@FormParam("selectedTopics") int aspectId,
			@FormParam("startDate")  String startDt, @FormParam("endDate")  String endDt) throws DataSourceException {
		
			LOGGER.info(">>>>>>>DashboardService ::: Entering fetchEntitiesForAspect>>>>>>>>");
			JSONObject resultObj = new JSONObject();
			
			ResponseBuilder responseBuilder;
			staticParameters = new HashMap<>();
			dynamicParameters = new HashMap<>();
			
			dynamicParameters.put(SP_IN_PARAM_ASPECT, aspectId);
			dynamicParameters.put(SP_IN_PARAM_ACCOUNT_ID, accountId);
			dynamicParameters.put(SP_IN_PARAM_SOURCE, sourceId);
			dynamicParameters.put(SP_IN_PARAM_FROM_DT, ((startDt == null) || (startDt.trim() == "") ? null : startDt));
			dynamicParameters.put(SP_IN_PARAM_TO_DT, ((endDt == null) || (endDt.trim() == "") ? null : endDt));
			staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_DASHBOARD_ENTITY_DISTRIBUTION_FOR_ASPECT"));
			
			JdbcDashboardDao jdbcDD = (JdbcDashboardDao) FactoryDao.getInstance(JDBC_DASHBOARD_DAO);
			Result result = jdbcDD.fetchEntityAndSentimentForAspect(staticParameters, dynamicParameters);
		
			resultObj.put(RESULTS, result.toJSON());
			responseBuilder = Response.ok(resultObj.toJSONString());
			LOGGER.info(">>>>>>>DashboardService ::: Exiting fetchEntitiesForAspect>>>>>>>>");
			return responseBuilder.build();

		}
	

}
