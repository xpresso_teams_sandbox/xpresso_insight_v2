package com.abzooba.trackR24.services;

import static com.abzooba.trackR24.util.Constants.ATTRIBUTE_SP_NAME;
import static com.abzooba.trackR24.util.Constants.JDBC_COMMON_DAO;
import static com.abzooba.trackR24.util.Constants.JDBC_RATINGS_DAO;
import static com.abzooba.trackR24.util.Constants.SP_INVALID_REPORT_LIST;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_ACCOUNT_ID;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_CITY_ID;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_OWNERSHIP_ID;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_SEARCH_TEXT;
import static com.abzooba.trackR24.util.Constants.SP_NAME;
import static com.abzooba.trackR24.util.Constants.SP_SOURCE_WISE_STORES;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import org.apache.log4j.Logger;
/*import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
*/




import com.abzooba.trackR24.dao.DataSourceConnectionException;
import com.abzooba.trackR24.dao.DataSourceException;
import com.abzooba.trackR24.dao.FactoryDao;
import com.abzooba.trackR24.dao.JdbcCommonDao;
import com.abzooba.trackR24.dao.JdbcRatingsDao;
import com.abzooba.trackR24.models.Result;
import com.abzooba.trackR24.util.PropertyHandler;

@Path("/common")
public class CommonService {

	private static final Logger LOGGER = Logger.getLogger(CommonService.class);

	private Map<String, Object> staticParameters;
	private Map<String, Object> dynamicParameters;

	@POST
	@Path("/overallSentimentUpdater")
	public int overallSentimentUpdater(@FormParam("company_Id") int companyID,@FormParam("sentimentID") String sentimentID, 
			@FormParam("postID") String postID,@FormParam("originalSentiment") String originalSentiment, @FormParam("postText") String postText,
			@FormParam("tabName") String tabName,@FormParam("postDateTime") String postDateTime,@FormParam("inAccountId") String accountId) throws DataSourceException {

		LOGGER.info(">>>>>>>AnalyticsService ::: Entering overallSentimentUpdater>>>>>>>>");
		staticParameters = new HashMap<>();
		dynamicParameters = new HashMap<>();
		
		String overAllSentiment = "Neutral";
		if(sentimentID.equalsIgnoreCase("1")){
			overAllSentiment = "Positive";
		}else if(sentimentID.equalsIgnoreCase("-1")){
			overAllSentiment = "Negative";
		}
		
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_POST_ID"), postID);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_SENTIMENT_TYPE"), overAllSentiment);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_COMPANY"),companyID);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_SENTIMENT_NUMBER"),sentimentID);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_ORIGINAL_SENTIMENT"),originalSentiment);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_POST_TEXT"),postText);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_TAB_NAME"),tabName);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_POST_DATETIME"),postDateTime);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_ACCOUNT_ID"),accountId);
		staticParameters.put(PropertyHandler.getPropertyValue("ATTRIBUTE_SQL"), PropertyHandler.getPropertyValue("SQL_FOR_INSERT_CHANGED_OVERALL_SENTIMENT"));

		JdbcCommonDao jdbcCD = (JdbcCommonDao) FactoryDao.getInstance(PropertyHandler.getPropertyValue("JDBC_COMMON_DAO"));
		int updateStatus = jdbcCD.overallSentimentUpdater(staticParameters, dynamicParameters);
		
		LOGGER.info(">>>>>>>AnalyticsService ::: Exiting overallSentimentUpdater>>>>>>>>"+ updateStatus);
		return updateStatus;

	}

	@POST
	@Path("/invalidDataUpdater")
	public int invalidDataUpdater(@FormParam("postID") String postID) throws DataSourceException {

		LOGGER.info(">>>>>>>AnalyticsService ::: Entering invalidDataUpdater>>>>>>>>");
		staticParameters = new HashMap<>();
		dynamicParameters = new HashMap<>();
		
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_POST_ID"), postID);
		staticParameters.put(PropertyHandler.getPropertyValue("ATTRIBUTE_SQL"), PropertyHandler.getPropertyValue("SQL_FOR_UPDATE_INVALID_DATA"));

		JdbcCommonDao jdbcCD = (JdbcCommonDao) FactoryDao.getInstance(PropertyHandler.getPropertyValue("JDBC_COMMON_DAO"));
		int updateStatus = jdbcCD.invalidDataUpdater(staticParameters, dynamicParameters);
		
		LOGGER.info(">>>>>>>AnalyticsService ::: Exiting invalidDataUpdater>>>>>>>>"+ updateStatus);
		return updateStatus;

	}

	
	public static String checkForNull(String sValue) {
		
		if(sValue==null)
			return null;
		else if("".equalsIgnoreCase(sValue.trim()))
			return null;
		else 
			return sValue;
	}
	
//--------------------------------Add By Jayanta========================
	public Map<String, Object> getInvaildReport() throws DataSourceConnectionException, FileNotFoundException {

		LOGGER.info(">>>>>>>RatingsService ::: Entering getStoreListInExcelFormat>>>>>>>>");
		String spName = null;
		staticParameters = new HashMap<>();
		dynamicParameters = new HashMap<>();
		spName = SP_INVALID_REPORT_LIST;
		LOGGER.info(SP_NAME + spName);
		staticParameters.put(ATTRIBUTE_SP_NAME, spName);

		JdbcCommonDao jdbcCD = (JdbcCommonDao) FactoryDao.getInstance(JDBC_COMMON_DAO);
		Map<String, Object> csvData = jdbcCD.fetchInvalidReport(staticParameters);
		
		LOGGER.info(">>>>>>>RatingsService ::: Leaving getStoreListInExcelFormat>>>>>>>>");
		return csvData;

	}
//----------------------------------------------------------------------	
	
	
	/*public void saveDataToExcel(ResultSet resultSet) throws SQLException, IOException {
		
		LOGGER.info(">>>>>>>>>>CommonService:saveDataToExcel>>>>>>>>>>>>");
		XSSFWorkbook workbook = new XSSFWorkbook(); 
	    XSSFSheet spreadsheet = workbook.createSheet("employe db");
	      
	      XSSFRow row = spreadsheet.createRow(1);
	      XSSFCell cell;
	      cell = row.createCell(1);
	      cell.setCellValue("EMP ID");
	      cell = row.createCell(2);
	      cell.setCellValue("EMP NAME");
	      cell = row.createCell(3);
	      cell.setCellValue("DEG");
	      cell = row.createCell(4);
	      cell.setCellValue("SALARY");
	      cell = row.createCell(5);
	      cell.setCellValue("DEPT");
	      int i = 2;

	      while(resultSet.next()) {
	         row = spreadsheet.createRow(i);
	         cell = row.createCell(1);
	         cell = row.createCell(2);
	         cell.setCellValue(resultSet.getString("ename"));
	         cell = row.createCell(3);
	         cell.setCellValue(resultSet.getString("deg"));
	         cell = row.createCell(4);
	         cell.setCellValue(resultSet.getString("salary"));
	         cell = row.createCell(5);
	         cell.setCellValue(resultSet.getString("dept"));
	         i++;
	      }

	      FileOutputStream out = new FileOutputStream(new File("exceldatabase.xlsx"));
	      workbook.write(out);
	      out.close();
	      System.out.println("exceldatabase.xlsx written successfully");
	   }*/


}
