package com.abzooba.trackR24.services;

import static com.abzooba.trackR24.util.Constants.ATTRIBUTE_SP_NAME;
import static com.abzooba.trackR24.util.Constants.ATTRIBUTE_SQL;
import static com.abzooba.trackR24.util.Constants.JDBC_USER_DAO;
import static com.abzooba.trackR24.util.Constants.MESSAGE;
import static com.abzooba.trackR24.util.Constants.RESULTS;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_PASSWORD;
import static com.abzooba.trackR24.util.Constants.SP_IN_PARAM_USER_NAME;

import java.io.IOException;
import java.net.URI;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriBuilder;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.abzooba.trackR24.dao.DataSourceException;
import com.abzooba.trackR24.dao.FactoryDao;
import com.abzooba.trackR24.dao.HibernateUserDao;
import com.abzooba.trackR24.dao.JdbcUserDao;
import com.abzooba.trackR24.models.Result;
import com.abzooba.trackR24.models.UserInfo;
import com.abzooba.trackR24.models.UserInfoBean;
import com.abzooba.trackR24.util.AdapterHandler;
import com.abzooba.trackR24.util.CommonHandler;
import com.abzooba.trackR24.util.PropertyHandler;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Author Name: Kunal Kashyap
 * Create Date: 21-07-2016
 * Class Insight: This class contains all the service methods for calling the dao layer
 * 				  of all the user related functionalities. 
 */
@Path("/user")
@SuppressWarnings("unchecked")
public class UserService {
	private static final Logger LOGGER = Logger.getLogger(UserService.class);

	private Map<String, Object> staticParameters;
	private Map<String, Object> dynamicParameters;
	Response response;
	URI uri;
	
	/**
	 * Method Insight: This service method is responsible for login functionality
	 * @param request,email,password
	 * @return Response
	 * @throws DataSourceException 
	 * @throws Exception
	 */
	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response login(@Context HttpServletRequest request, @FormParam("email") String email, @FormParam("password") String password) throws DataSourceException {
		LOGGER.info(">>>>>>>UserService ::: Entering LoginService>>>>>>>>");
		HttpSession session;
		staticParameters = new HashMap<>();
		staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_CHECK_LOGIN"));
		dynamicParameters = new HashMap<>();
		dynamicParameters.put(SP_IN_PARAM_USER_NAME, email);
		dynamicParameters.put(SP_IN_PARAM_PASSWORD, password);
		
		JdbcUserDao jdbcUD = (JdbcUserDao) FactoryDao.getInstance(JDBC_USER_DAO);
		ArrayList<String> loginDetails = jdbcUD.loginUser(staticParameters, dynamicParameters);

		if (loginDetails.size() > 1) {
			String concatResult = CommonHandler.concatArrayList(loginDetails);
			session = request.getSession();
			session.setAttribute("User", email);
			uri = UriBuilder.fromPath("../index.jsp").queryParam(MESSAGE, concatResult).build();
		} else {
			String concatResult = loginDetails.get(0);
			uri = UriBuilder.fromPath("../login.jsp").queryParam("wrong", concatResult).build();
		}
		LOGGER.info(">>>>>>>UserService ::: Exiting LoginService>>>>>>>>>");
		return Response.seeOther(uri).build();
	}

	/**
	 * Method Insight: This service method is used for checking the registration confirmation status of user.
	 * @param key
	 * @return Response
	 * @throws DataSourceException 
	 * @throws Exception
	 */
	@GET
	@Path("/mail")
	public Response checkMail(@QueryParam("ck") String key) throws DataSourceException{
		LOGGER.info(">>>>>>>UserService ::: Entering MailService>>>>>>>>");

		staticParameters = new HashMap<>();
		staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_CHECK_MAIL_CONF"));
		dynamicParameters = new HashMap<>();
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_KEY"), key);
		
		JdbcUserDao jdbcUD = (JdbcUserDao) FactoryDao.getInstance(JDBC_USER_DAO);
		ArrayList<String> loginDetails = jdbcUD.loginUser(staticParameters, dynamicParameters);
		String concatResult = CommonHandler.concatArrayList(loginDetails);

		uri = UriBuilder.fromPath("../index.jsp").queryParam(MESSAGE, concatResult).build();
		LOGGER.info(">>>>>>>UserService ::: Exiting MailService>>>>>>>>>");
		return Response.seeOther(uri).build();

	}

	/**
	 * Method Insight: This service method is responsible for calling the dao layer method for user registration.
	 * @param request,inUserName,inUserID,inPassword,inCountryID,inCompanyName,inAccountName,inDesignation,inDomainID,inTimeZoneID
	 * @return Response
	 * @throws DataSourceException 
	 * @throws Exception
	 */
	@POST
	@Path("/inserUserSignUpDetails")
	@Produces(MediaType.APPLICATION_JSON)
	public Response inserUserSignUpDetails(@Context HttpServletRequest request, @FormParam("userName") String inUserName, @FormParam("userId") String inUserID, @FormParam("userPassword") String inPassword, @FormParam("SelectCountry") int inCountryID, @FormParam("userCompany") String inCompanyName, @FormParam("accountName") String inAccountName, @FormParam("userDesignation") String inDesignation, @FormParam("selectdomain") int inDomainID, @FormParam("timezone") int inTimeZoneID) throws DataSourceException{
		LOGGER.info(">>>>>>>UserService ::: Entering inserUserSignUpDetails>>>>>>>>");
		String key = CommonHandler.randomAlphaNumeric(50);
		staticParameters = new HashMap<>();
		staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_INSERT_DATA"));
		dynamicParameters = new HashMap<>();
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_USER_ID"), inUserID);
		dynamicParameters.put(SP_IN_PARAM_USER_NAME, inUserName);
		dynamicParameters.put(SP_IN_PARAM_PASSWORD, inPassword);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_COMPANY_NAME"), inCompanyName);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_DESIGNATION"), inDesignation);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_COUNTRY_ID"), inCountryID);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_DOMAIN_ID"), inDomainID);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_TIMEZONE_ID"), inTimeZoneID);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_ROLE_ID"), 2);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_CONFIRMED_INDEX"), 0);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_USER_TOKEN"), key);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_ACCOUNT_NAME"), inAccountName);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_EXPIRY_INTERVAL"), PropertyHandler.getPropertyValue("SP_IN_PARAM_EXPIRY_INTERVAL_VALUE"));

		String wrongLogin;
		HttpSession session;
		JdbcUserDao jdbcUD = (JdbcUserDao) FactoryDao.getInstance(JDBC_USER_DAO);
		int success = jdbcUD.registerUser(staticParameters, dynamicParameters);
		if (success == 1) {
			try {
				session = request.getSession();
				session.setAttribute("User", inUserID);
				String myurl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
				String data = key.concat("$").concat(myurl).concat("$").concat(inUserName).concat("$").concat(inPassword);
				CommonHandler.sendEmail(inUserID, data, "confirmation");
			} catch (Exception e) {
				LOGGER.error(e.getMessage());
			}
			uri = UriBuilder.fromPath("../activate.jsp").build();
		} else if (success == 0) {
			wrongLogin = PropertyHandler.getPropertyValue("VALIDATION_DUPLICATE_EMAIL");
			uri = UriBuilder.fromPath("../registration.jsp").queryParam("wrongreg", wrongLogin).build();
		}
		LOGGER.info(">>>>>>>UserService ::: Exiting inserUserSignUpDetails>>>>>>>>");
		return Response.seeOther(uri).build();
	}
	
	/**
	 * Method Insight: This service method is responsible for calling the dao layer method for fetching all the static
	 * 				   details of registraion page.
	 * @param type
	 * @return Response
	 * @throws DataSourceException 
	 * @throws Exception
	 */
	@GET
	@Path("/getStaticDetails")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getStaticDetails(@QueryParam("type") String type) throws DataSourceException  {
		LOGGER.info(">>>>>>>UserService ::: Entering getStaticDetails>>>>>>>>");
		JSONObject resultObj = new JSONObject();
		ResponseBuilder responseBuilder = null;
		staticParameters = new HashMap<>();
		if (type.equalsIgnoreCase("Country")) {
			staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_GET_COUNTRY_DETAILS"));
		} else if (type.equalsIgnoreCase("Domain")) {
			staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_GET_DOMAIN_DETAILS"));
		} else if (type.equalsIgnoreCase("Timezone")) {
			staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_GET_TIMEZONE_DETAILS"));
		}
		JdbcUserDao jdbcUD = (JdbcUserDao) FactoryDao.getInstance(JDBC_USER_DAO);
		Result result = jdbcUD.getStaticResults(staticParameters, dynamicParameters);
		resultObj.put(RESULTS, result.toJSON());
		responseBuilder = Response.ok(resultObj.toJSONString());
		LOGGER.info(">>>>>>>UserService ::: Exiting getStaticDetails>>>>>>>>");
		return responseBuilder.build();

	}
	
	/**
	 * Method Insight: This service method is responsible for calling the dao layer method for forgot password functionality.
	 * @param email
	 * @return
	 * @throws DataSourceException 
	 * @throws SQLException 
	 * @throws MessagingException 
	 * @throws AddressException 
	 * @throws Exception
	 */
	@POST
	@Path("/forgotpassword")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response forgotPassword(@FormParam("email") String email) throws DataSourceException, MessagingException {
		LOGGER.info(">>>>>>>UserService ::: Entering forgotPassword>>>>>>>>");
		staticParameters = new HashMap<>();
		staticParameters.put(ATTRIBUTE_SQL, PropertyHandler.getPropertyValue("SQL_FOR_FORGOT_PASSWORD"));
		dynamicParameters = new HashMap<>();
		dynamicParameters.put(SP_IN_PARAM_USER_NAME, email);
		
		JdbcUserDao jdbcUD = (JdbcUserDao) FactoryDao.getInstance(JDBC_USER_DAO);
		String data = jdbcUD.getUpdatePassword(staticParameters, dynamicParameters);

		String wrongpassword;
		if (data == null) {
			wrongpassword = PropertyHandler.getPropertyValue("VALIDATION_WRONG_EMAIL");
			uri = UriBuilder.fromPath("../forgotpassword.jsp").queryParam("wrongpassword", wrongpassword).build();
		} else {
			CommonHandler.sendEmail(email, data, "forgot");
			uri = UriBuilder.fromPath("../mailpassword.jsp").queryParam(MESSAGE, "").build();
		}
		LOGGER.info(">>>>>>>UserService ::: Exiting forgotPassword>>>>>>>>>");
		return Response.seeOther(uri).build();
	}

	/**
	 * Method Insight: This service method is responsible for reset password functionality
	 * @param authDetails,email,oldpassword,newpassword
	 * @return Response
	 * @throws DataSourceException 
	 * @throws Exception
	 */
	@POST
	@Path("/updatepassword")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response updatePassword(@FormParam("adminDetails") String authDetails, @FormParam("adminEmail") String email, @FormParam("oldPassword") String oldpassword, @FormParam("newPassword") String newpassword) throws DataSourceException  {
		LOGGER.info(">>>>>>>UserService ::: Entering updatePassword>>>>>>>>");

		staticParameters = new HashMap<>();
		staticParameters.put(ATTRIBUTE_SP_NAME, PropertyHandler.getPropertyValue("SP_RESET_PASSWORD"));
		dynamicParameters = new HashMap<>();
		dynamicParameters.put(SP_IN_PARAM_USER_NAME, email);
		dynamicParameters.put(PropertyHandler.getPropertyValue("SP_IN_PARAM_OLD_PASSWORD"), oldpassword);
		dynamicParameters.put(SP_IN_PARAM_PASSWORD, newpassword);
		JdbcUserDao jdbcUD = (JdbcUserDao) FactoryDao.getInstance(JDBC_USER_DAO);
		int i = jdbcUD.registerUser(staticParameters, dynamicParameters);
		if (i == 1) {
			uri = UriBuilder.fromPath("../login.jsp").queryParam("resetpassword", i).build();
		} else {
			uri = UriBuilder.fromPath("../index.jsp").queryParam(MESSAGE, authDetails).fragment("/settings/".concat("0")).build();
		}
		LOGGER.info(">>>>>>>UserService ::: Exiting updatePassword>>>>>>>>>");
		return Response.seeOther(uri).build();
	}

	/**
	 * Method Insight: This method is responsible for add info functionality.
	 * @param request,userInfo
	 * @throws JsonParseException,JsonMappingException,IOException,ParseException
	 */
	@POST
	@Path("/adduser")
	@Consumes(MediaType.APPLICATION_JSON)
	public void addUser(@Context HttpServletRequest request, String userInfo) throws  IOException, ParseException {
		LOGGER.info(">>>>>>>AddUserService ::: Entering AddUser>>>>>>>>");

		JSONParser parser = new JSONParser();
		Object obj = parser.parse(userInfo);
		JSONArray array = new JSONArray();
		array.add(obj);
		String adminEmail = null;
		for (int i = 0; i < array.size(); i++) {
			JSONObject obj2 = (JSONObject) array.get(i);
			adminEmail = (String) obj2.get("adminEmail");
		}
		ObjectMapper mapper = new ObjectMapper();
		UserInfo userInfoObj = mapper.readValue(userInfo, UserInfo.class);

		AdapterHandler adapterHandler = new AdapterHandler();
		UserInfoBean userInfoBean = adapterHandler.getBean(userInfoObj);
		HibernateUserDao hibernateUD = (HibernateUserDao) FactoryDao.getInstance(PropertyHandler.getPropertyValue("HIBERNATE_USER_DAO"));
		hibernateUD.addUserInfo(userInfoBean, adminEmail);

	}

	/**
	 * Method Insight: This service method is responsible for fetching user's details in the settings page.
	 * @param adminEmail
	 * @return Response
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonGenerationException 
	 * @throws Exception
	 */
	@POST
	@Path("/fetchUserInfo")
	@Produces(MediaType.APPLICATION_JSON)
	public Response fetchUserInfo(String adminEmail) throws IOException {

		LOGGER.info(">>>>>>>adminEmail :");
		LOGGER.info(">>>>>>>UserService ::: Entering fetchUserInfo>>>>>>>>");
		ResponseBuilder responseBuilder = null;
		
		HibernateUserDao hibernateUD = (HibernateUserDao) FactoryDao.getInstance(PropertyHandler.getPropertyValue("HIBERNATE_USER_DAO"));
		UserInfo userInfo = hibernateUD.fetchUserInfo(adminEmail);
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = mapper.writeValueAsString(userInfo);
		responseBuilder = Response.ok(jsonInString);

		LOGGER.info(">>>>>>>DashboardService ::: Exiting getDailySnapshot>>>>>>>>>");
		return responseBuilder.build();

	}

}
