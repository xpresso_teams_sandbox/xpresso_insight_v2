package com.abzooba.trackR24.services;

public class HeadlineObject {
	private String headline;
	private String publishDatetime;
	private String newsUrl;

	public void setHeadline(String headline) {
		this.headline = headline;
	}

	public String getHeadline() {
		return headline;
	}

	public void setPublishDatetime(String publishDatetime) {
		this.publishDatetime = publishDatetime;
	}

	public String getPublishDatetime() {
		return publishDatetime;
	}

	public String getNewsUrl() {
		return newsUrl;
	}

	public void setNewsUrl(String newsUrl) {
		this.newsUrl = newsUrl;
	}
}

